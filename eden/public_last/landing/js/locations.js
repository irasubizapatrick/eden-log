/*
 * jVector Maps
 */

var markers =
    [{ latLng: [35.861660, 104.195396], name: 'China'},
        { latLng: [-1.940278, 29.873888], name: 'Rwanda'},
        { latLng: [-6.369028, 34.888821], name: 'Tanzania'},
        { latLng: [-13.133897, 27.849333], name: 'Zambia'},
        { latLng: [-3.409225, 29.974677], name: 'Burundi'},
        { latLng: [-13.302212, 34.036726], name: 'Malawi'},
        { latLng: [1.105402, 32.520620], name: 'Uganda'},
        { latLng: [25.267406, 55.292681], name: 'United Arab Emirates'},
        { latLng: [8.052941, 29.518586], name: 'S. Sudan'}];

$(function() {
    "use strict";
    var $jvectormapDiv = $('#jvectormap');
    if ($jvectormapDiv.length && $.fn.vectorMap) {
        $jvectormapDiv.vectorMap({
            map: 'world_mill',
            zoomOnScroll: false,
            hoverOpacity: 0.7,
            regionStyle: {
                initial: {
                    fill: '#e3ecff',
                    "fill-opacity": 1,
                    "stroke-width": 0,
                },
                hover: {
                    fill: '#cfdcf7',
                    "fill-opacity": 1,
                    cursor: 'pointer'
                },
            },
            markerStyle: {
                initial: {
                    fill: '#2761d8',
                    stroke: '#2761d8'
                }
            },
            markers: markers
        });
    }
});