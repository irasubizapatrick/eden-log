
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="/landing/img/favicon.png" type="image/x-icon" />

    <title>Mikumi Freight Forwarders Ltd  </title>

    <link href="/landing/css/font-awesome.min.css" rel="stylesheet">

    <link href="/landing/css/bootstrap.min.css" rel="stylesheet">

    <link href="/landing/vendors/revolution/css/settings.css" rel="stylesheet">
    <link href="/landing/vendors/revolution/css/layers.css" rel="stylesheet">
    <link href="/landing/vendors/revolution/css/navigation.css" rel="stylesheet">
    <link href="/landing/vendors/animate-css/animate.css" rel="stylesheet">

    <link href="/landing/vendors/owl-carousel/assets/owl.carousel.min.css" rel="stylesheet">
    <link href="/landing/vendors/magnific-popup/magnific-popup.css" rel="stylesheet">
    <link href="/landing/css/style.css" rel="stylesheet">

    <!--[if lt IE 9]>
    <script src="/js/html5shiv.min.js"></script>
    <script src="/js/respond.min.js"></script>
    <![endif]-->
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-142579219-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-142579219-1');
    </script>

</head>
<style type="text/css">
    ::-webkit-input-placeholder { /* Edge */
        color: #1d6891 !important;
    }
    .search{
        color: #1d6891 !important;
    }
    .contact
    {
        padding-top: 2%;
    }
    .__cf_email__{
        color: #b8d4df !important;
    }
    #main_slider{
        margin-top: 0px !important;
        margin-bottom: 0px !important;
        height: 560px !important;
    }
</style>

<body>

@include('layouts.page_header')

<section class="main_slider_area">
    <div id="main_slider" class="rev_slider" data-version="5.3.1.6">
        <ul>
            <li class="slide_home" data-index="rs-2972" data-transition="fade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="300" data-rotate="0" data-saveperformance="off" data-title="Creative" data-param1="01" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">

                <img src="/landing/img/home-slider/slider-1.jpg" alt="" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="5" class="rev-slidebg" data-no-retina>

                <div class="slider_text_box">
                    <div class="tp-caption first_text" data-x="['left','left','left','center']" data-y="['top','top','top','top']" data-hoffset="['0','0','0','0']" data-voffset="['250','250','250','140']" data-fontsize="['34','34','34','20']" data-lineheight="['44','44','44','30']" data-width="none" data-height="none" data-whitespace="nowrap" data-type="text" data-responsive_offset="on" data-frames="[{&quot;delay&quot;:10,&quot;speed&quot;:1500,&quot;frame&quot;:&quot;0&quot;,&quot;from&quot;:&quot;y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;&quot;,&quot;mask&quot;:&quot;x:0px;y:0px;s:inherit;e:inherit;&quot;,&quot;to&quot;:&quot;o:1;&quot;,&quot;ease&quot;:&quot;Power2.easeInOut&quot;},{&quot;delay&quot;:&quot;wait&quot;,&quot;speed&quot;:1500,&quot;frame&quot;:&quot;999&quot;,&quot;to&quot;:&quot;y:[175%];&quot;,&quot;mask&quot;:&quot;x:inherit;y:inherit;s:inherit;e:inherit;&quot;,&quot;ease&quot;:&quot;Power2.easeInOut&quot;}]" data-textAlign="['left','left','left','left']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]">Reliable and flexible Logistics</div>
                    <div class="tp-caption secand_text" data-x="['left','left','left','center']" data-y="['top','top','top','top']" data-hoffset="['0','0','0','0']" data-voffset="['315','315','315','190']" data-fontsize="['50','50','50','30']" data-lineheight="['60','60','60','40']" data-width="['730','730','730','400']" data-height="none" data-whitespace="['normal','normal','normal','normal']" data-type="text" data-responsive_offset="on" data-frames="[{&quot;delay&quot;:10,&quot;speed&quot;:1500,&quot;frame&quot;:&quot;0&quot;,&quot;from&quot;:&quot;y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;&quot;,&quot;mask&quot;:&quot;x:0px;y:0px;s:inherit;e:inherit;&quot;,&quot;to&quot;:&quot;o:1;&quot;,&quot;ease&quot;:&quot;Power2.easeInOut&quot;},{&quot;delay&quot;:&quot;wait&quot;,&quot;speed&quot;:1500,&quot;frame&quot;:&quot;999&quot;,&quot;to&quot;:&quot;y:[175%];&quot;,&quot;mask&quot;:&quot;x:inherit;y:inherit;s:inherit;e:inherit;&quot;,&quot;ease&quot;:&quot;Power2.easeInOut&quot;}]" data-textAlign="['left','left','left','center']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[10,10,10,10]" data-paddingleft="[0,0,0,0]" >Personalized cargo services in worldwide commerce</div>
                    <div class="tp-caption slider_btn" data-x="['left','left','left','center']" data-y="['top','top','top','top']" data-hoffset="['0','0','0','0']" data-voffset="['470','470','470','335']" data-fontsize="['16','16','16','16']" data-lineheight="['26','26','26','26']" data-width="none" data-height="none" data-whitespace="nowrap" data-type="text" data-responsive_offset="on" data-frames='[{"from":"y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":150,"ease":"Power2.easeInOut"},{"delay":"wait","speed":300,"to":"opacity:0;","ease":"nothing"}]' data-textAlign="['left','left','left','left']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]"><a class="quote_btn" href="#">KNOW MORE <i class="fa fa-angle-right"></i></a></div>
                    <div class="tp-caption ContentZoom-SmallIcon tp-resizeme rs-parallaxlevel-0" data-x="['left','left','left','center']" data-y="['top','top','top','top']" data-hoffset="['0','0','0','-25']" data-voffset="['190','190','190','80']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_hover="o:1;rX:0;rY:0;rZ:0;z:0;s:300;e:Power1.easeInOut;" data-style_hover="c:rgba(111, 124, 130, 1.00);cursor:pointer;" data-transform_in="opacity:0;s:500;e:Power1.easeInOut;" data-transform_out="opacity:0;s:500;e:Power1.easeInOut;s:500;e:Power1.easeInOut;" data-start="0" data-splitin="none" data-splitout="none" data-actions='[{"event":"click","action":"jumptoslide","slide":"previous","delay":""}]' data-responsive_offset="on"><i class="fa fa-angle-left"></i>
                    </div>
                    <div class="tp-caption ContentZoom-SmallIcon tp-resizeme rs-parallaxlevel-0" data-x="['left','left','left','center']" data-y="['top','top','top','top']" data-hoffset="['50','50','50','25']" data-voffset="['190','190','190','80']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_hover="o:1;rX:0;rY:0;rZ:0;z:0;s:300;e:Power1.easeInOut;" data-style_hover="c:rgba(111, 124, 130, 1.00);cursor:pointer;" data-transform_in="opacity:0;s:500;e:Power1.easeInOut;" data-transform_out="opacity:0;s:500;e:Power1.easeInOut;s:500;e:Power1.easeInOut;" data-start="0" data-splitin="none" data-splitout="none" data-actions='[{"event":"click","action":"jumptoslide","slide":"next","delay":""}]' data-responsive_offset="on"><i class="fa fa-angle-right"></i>
                    </div>
                </div>
            </li>
            <li class="slide_home" data-index="rs-2973" data-transition="fade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="300" data-rotate="0" data-saveperformance="off" data-title="Creative" data-param1="01" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">

                <img src="/landing/img/home-slider/slider-2.jpg" alt="" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="5" class="rev-slidebg" data-no-retina>

                <div class="slider_text_box">
                    <div class="tp-caption first_text" data-x="['left','left','left','center']" data-y="['top','top','top','top']" data-hoffset="['0','0','0','0']" data-voffset="['250','250','250','140']" data-fontsize="['34','34','34','20']" data-lineheight="['44','44','44','30']" data-width="none" data-height="none" data-whitespace="nowrap" data-type="text" data-responsive_offset="on" data-frames="[{&quot;delay&quot;:10,&quot;speed&quot;:1500,&quot;frame&quot;:&quot;0&quot;,&quot;from&quot;:&quot;y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;&quot;,&quot;mask&quot;:&quot;x:0px;y:0px;s:inherit;e:inherit;&quot;,&quot;to&quot;:&quot;o:1;&quot;,&quot;ease&quot;:&quot;Power2.easeInOut&quot;},{&quot;delay&quot;:&quot;wait&quot;,&quot;speed&quot;:1500,&quot;frame&quot;:&quot;999&quot;,&quot;to&quot;:&quot;y:[175%];&quot;,&quot;mask&quot;:&quot;x:inherit;y:inherit;s:inherit;e:inherit;&quot;,&quot;ease&quot;:&quot;Power2.easeInOut&quot;}]" data-textAlign="['left','left','left','left']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]">Reliable and flexible Logistics</div>
                    <div class="tp-caption secand_text" data-x="['left','left','left','center']" data-y="['top','top','top','top']" data-hoffset="['0','0','0','0']" data-voffset="['315','315','315','190']" data-fontsize="['50','50','50','30']" data-lineheight="['60','60','60','40']" data-width="['730','730','730','400']" data-height="none" data-whitespace="['normal','normal','normal','normal']" data-type="text" data-responsive_offset="on" data-frames="[{&quot;delay&quot;:10,&quot;speed&quot;:1500,&quot;frame&quot;:&quot;0&quot;,&quot;from&quot;:&quot;y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;&quot;,&quot;mask&quot;:&quot;x:0px;y:0px;s:inherit;e:inherit;&quot;,&quot;to&quot;:&quot;o:1;&quot;,&quot;ease&quot;:&quot;Power2.easeInOut&quot;},{&quot;delay&quot;:&quot;wait&quot;,&quot;speed&quot;:1500,&quot;frame&quot;:&quot;999&quot;,&quot;to&quot;:&quot;y:[175%];&quot;,&quot;mask&quot;:&quot;x:inherit;y:inherit;s:inherit;e:inherit;&quot;,&quot;ease&quot;:&quot;Power2.easeInOut&quot;}]" data-textAlign="['left','left','left','center']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[10,10,10,10]" data-paddingleft="[0,0,0,0]">Personalized cargo services in worldwide commerce</div>
                    <div class="tp-caption slider_btn" data-x="['left','left','left','center']" data-y="['top','top','top','top']" data-hoffset="['0','0','0','0']" data-voffset="['470','470','470','335']" data-fontsize="['16','16','16','16']" data-lineheight="['26','26','26','26']" data-width="none" data-height="none" data-whitespace="nowrap" data-type="text" data-responsive_offset="on" data-frames='[{"from":"y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":150,"ease":"Power2.easeInOut"},{"delay":"wait","speed":300,"to":"opacity:0;","ease":"nothing"}]' data-textAlign="['left','left','left','left']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]"><a class="quote_btn" href="#">KNOW MORE <i class="fa fa-angle-right"></i></a></div>
                    <div class="tp-caption ContentZoom-SmallIcon tp-resizeme rs-parallaxlevel-0" data-x="['left','left','left','center']" data-y="['top','top','top','top']" data-hoffset="['0','0','0','-25']" data-voffset="['190','190','190','80']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_hover="o:1;rX:0;rY:0;rZ:0;z:0;s:300;e:Power1.easeInOut;" data-style_hover="c:rgba(111, 124, 130, 1.00);cursor:pointer;" data-transform_in="opacity:0;s:500;e:Power1.easeInOut;" data-transform_out="opacity:0;s:500;e:Power1.easeInOut;s:500;e:Power1.easeInOut;" data-start="0" data-splitin="none" data-splitout="none" data-actions='[{"event":"click","action":"jumptoslide","slide":"previous","delay":""}]' data-responsive_offset="on"><i class="fa fa-angle-left"></i>
                    </div>
                    <div class="tp-caption ContentZoom-SmallIcon tp-resizeme rs-parallaxlevel-0" data-x="['left','left','left','center']" data-y="['top','top','top','top']" data-hoffset="['50','50','50','25']" data-voffset="['190','190','190','80']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_hover="o:1;rX:0;rY:0;rZ:0;z:0;s:300;e:Power1.easeInOut;" data-style_hover="c:rgba(111, 124, 130, 1.00);cursor:pointer;" data-transform_in="opacity:0;s:500;e:Power1.easeInOut;" data-transform_out="opacity:0;s:500;e:Power1.easeInOut;s:500;e:Power1.easeInOut;" data-start="0" data-splitin="none" data-splitout="none" data-actions='[{"event":"click","action":"jumptoslide","slide":"next","delay":""}]' data-responsive_offset="on"><i class="fa fa-angle-right"></i>
                    </div>
                </div>
            </li>
        </ul>
    </div>
</section>

<section class="about_area">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="about_left_text">
                    <h3 class="single_title">About Us</h3>
                    <p>
                        Mikumi Logistics Limited, you partner with Tanzania’s premiere logistics service provider. Our core business encompasses integrated logistics, international freight forwarding and supply chain solutions. Our expertise extends from handling merchandise and non-merchandise, to POSM and more.We employ a far-reaching global network that stretches across six continents, and includes the largest distribution network and hub operations in Africa.
                    </p>
                </div>
            </div>
            <div class="col-md-6">
                <div id="jvectormap"></div>
                <img src="">
            </div>
        </div>
    </div>
</section>
<section class="feature_slider_area">
    <div class="container">
        <div class="row feature_row">
            <div class="feature_slider_inner owl-carousel">
                <div class="item">
                    <div class="feature_s_item" style="height: 495px !important;">
                        <img alt="" class="svg social-link" src="landing/img/icon/svg/aeroplane.svg" />
                        <a href="#">
                            <h4>Air Cargo</h4>
                        </a>
                        <p>Mikumi freight offers a global and cost effective solution to all of your air freight needs. Through a worldwide network spanning the four corners of the globe, our teams of cargo professionals provide global services and local expertise to ensure complete door-to-door transportation. Our state-of-the-art technology and network of offices around the world allow total trace ability and visibility of your shipments from the moment a job is booked until it is delivered at the final destination.</p>
                    </div>
                </div>
                <div class="item">
                    <div class="feature_s_item" style="height: 495px !important;">
                        <img alt="" class="svg social-link" src="landing/img/icon/svg/ship.svg" />
                        <a href="#">
                            <h4>Sea Freight</h4>
                        </a>
                        <p>Our sea freight and vessel chartering specialists are ready to guide you through the complexities of global transportation. As part of our door-to-door service, we plan and implement the entire transport chain for your goods. Our direct and close contacts to ship owners around the world are essential in our pursuit to find the right vessel in the right place at a competitive price. Whether your shipment is big or small, personal or commercial, we work with you one-on-one to secure the most efficient and cost-effective see freight solution.</p>
                    </div>
                </div>
                <div class="item">
                    <div class="feature_s_item" style="height: 495px !important;">
                        <img alt="" class="svg social-link" src="landing/img/icon/svg/delivery-truck.svg" />
                        <a href="#">
                            <h4>Road Transport</h4>
                        </a>
                        <p>Mikumi freight provides a full range of rail and road freight services to meet the needs of all types of business and freight, from single-container loads to multiple-container network distribution. Our transport networks provide reliable countrywide coverage, offering you the fastest point-to-point cross-country road and rail transport available. Our experienced shipping agents will arrange for door-to-door collection and delivery of your goods, whether they are containerized, palletized or boxed.</p>
                    </div>
                </div>
                <div class="item">
                    <div class="feature_s_item" style="height: 495px !important;">
                        <img alt="" class="svg social-link" src="landing/img/icon/svg/delivery-truck.svg" />
                        <a href="#">
                            <h4>Pipeline</h4>
                        </a>
                        <p>Natural resources, like crude oil and natural gases, are the raw material for energy that the world consumes. While many forms of transportation are used to move these products to marketplaces; pipelines remain the safest, most efficient and economical way to move these natural resources. This system of transmission serves as a national network to move the energy resources we need from production areas or ports of entry throughout countries to consumers, airports, military bases, population centers and industry every day.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

{{--<section class="our_blog_area">--}}
    {{--<div class="container">--}}
        {{--<div class="main_title">--}}
            {{--<h2>From Our Blog</h2>--}}
        {{--</div>--}}
        {{--<div class="row our_blog_inner">--}}
            {{--<div class="col-md-4">--}}
                {{--<div class="our_blog_item">--}}
                    {{--<div class="our_blog_img">--}}
                        {{--<img src="landing/img/blog/our-blog/our-blog-1.jpg" alt="">--}}
                        {{--<div class="b_date">--}}
                            {{--<h6>JULY</h6>--}}
                            {{--<h5>25</h5>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="our_blog_content">--}}
                        {{--<a href="blog-details.html">--}}
                            {{--<h4>QUALITY CARGO SERVICES AT AFFORDABLE PRICE</h4>--}}
                        {{--</a>--}}
                        {{--<p>Dramatically transition clicks-and-mortar portals before diverse leadership skills. Monotonectally recaptiualize fully tested information after.</p>--}}
                        {{--<h6><a href="#">Frank Martin</a><span>•</span><a href="#">9 Comments</a></h6>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<div class="col-md-4">--}}
                {{--<div class="our_blog_item">--}}
                    {{--<div class="our_blog_img">--}}
                        {{--<img src="landing/img/blog/our-blog/our-blog-2.jpg" alt="">--}}
                        {{--<div class="b_date">--}}
                            {{--<h6>JULY</h6>--}}
                            {{--<h5>12</h5>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="our_blog_content">--}}
                        {{--<a href="blog-details.html">--}}
                            {{--<h4>WHY CHOOSE OUR WAREHOUSING SERVICE?</h4>--}}
                        {{--</a>--}}
                        {{--<p>Dramatically transition clicks-and-mortar portals before diverse leadership skills. Monotonectally recaptiualize fully tested information after.</p>--}}
                        {{--<h6><a href="#">Frank Martin</a><span>•</span><a href="#">9 Comments</a></h6>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<div class="col-md-4">--}}
                {{--<div class="our_blog_item">--}}
                    {{--<div class="our_blog_img">--}}
                        {{--<img src="landing/img/blog/our-blog/our-blog-3.jpg" alt="">--}}
                        {{--<div class="b_date">--}}
                            {{--<h6>June</h6>--}}
                            {{--<h5>07</h5>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="our_blog_content">--}}
                        {{--<a href="blog-details.html">--}}
                            {{--<h4>TOP BENEFITS OF HIRING OUR <br class="b_title_br" />TRUCKING SERVICE</h4>--}}
                        {{--</a>--}}
                        {{--<p>Dramatically transition clicks-and-mortar portals before diverse leadership skills. Monotonectally recaptiualize fully tested information after.</p>--}}
                        {{--<h6><a href="#">Frank Martin</a><span>•</span><a href="#">9 Comments</a></h6>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
{{--</section>--}}



@include('layouts.web_footer')


<script src="/landing/js/jquery-2.2.4.js"></script>

<script src="/landing/js/bootstrap.min.js"></script>

<script src="/landing/vendors/revolution/js/jquery.themepunch.tools.min.js"></script>
<script src="/landing/vendors/revolution/js/jquery.themepunch.revolution.min.js"></script>
<script src="/landing/vendors/revolution/js/extensions/revolution.extension.actions.min.js"></script>
<script src="/landing/vendors/revolution/js/extensions/revolution.extension.video.min.js"></script>
<script src="/landing/vendors/revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
<script src="/landing/vendors/revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>
<script src="/landing/vendors/revolution/js/extensions/revolution.extension.navigation.min.js"></script>

<script src="/landing/vendors/owl-carousel/owl.carousel.min.js"></script>
<script src="/landing/vendors/magnific-popup/jquery.magnific-popup.min.js"></script>

<script src="/landing/vendors/jvectormap/jvectormap.min.js"></script>
<script src="/landing/vendors/jvectormap/jvectormap-worldmill.js"></script>
<script src="/landing/js/locations.js"></script>

<script src="/landing/js/script.js"></script>
</body>
</html>
<!-- Localized -->