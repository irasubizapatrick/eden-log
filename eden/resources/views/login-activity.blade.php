<!DOCTYPE html>
<html>

@include('layouts.admin_modal')
<style>
    .form_size{
        height: calc(3.25rem + 2px) !Important;
    }
</style>
<body>

<!-- Navigation Bar-->
@include('layouts.admin_sidebar')
<!-- End Navigation Bar-->


<div class="wrapper">
    <div class="container-fluid">
        <!-- end page title end breadcrumb -->
    </div><!-- /.modal -->

    <div class="row m-0 p-2">
        <div class="col-sm-12">
            <div class="page-title-box">
                <div class="btn-group pull-right">
                    <ol class="breadcrumb hide-phone p-0 m-0">
                        <li class="breadcrumb-item"><a href="#">Agent List</a></li>
                        <li class="breadcrumb-item active">Dashboard</li>
                    </ol>
                </div>
                <h4 class="page-title">All Logs</h4>
            </div>
        </div>
        <div class="col-12 container" style="margin-top: -3rem;">
            <div class="card m-b-30">
                <div class="card-body">
                    @if (Session::has('message'))
                        <div class="alert alert-success alert-dismissable">
                            <a href="" class="close" data-dismiss="alert" aria-label="close">
                                &times;

                            </a>
                            {{ Session::get('message') }}
                        </div>

                    @endif
                    @if (Session::has('delete'))
                        <div class="alert alert-success alert-dismissable">
                            <a href="" class="close" data-dismiss="alert" aria-label="close">
                                &times;

                            </a>
                            {{ Session::get('delete') }}
                        </div>

                    @endif
                    @if (Session::has('updated'))
                        <div class="alert alert-success alert-dismissable">
                            <a href="" class="close" data-dismiss="alert" aria-label="close">
                                &times;

                            </a>
                            {{ Session::get('updated') }}
                        </div>

                    @endif

                    <table id="datatable-buttons" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                        <tr style="font-size: 13px;background-color: #416b8d !important;color: #fff;">
                            <th>#</th>
                            <th>User</th>
                            <th>Last Login</th>
                            <th>Ip Address</th>
                            <th>Email</th>

                        </tr>
                        </thead>
                        <tbody>
                        <?php $i=1;?>
                        @foreach($loginActivities AS $value)
                            <tr>
                                <td>{{$i++}}</td>
                                <td>{{$value->user->name}}</td>
                                <td>{{$value->created_at}}</td>
                                <td>{{$value->ip_address}}</td>
                                <td>{{$value->user->email}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                </div>
            </div>
        </div> <!-- end col -->
    </div> <!-- end row -->

</div> <!-- end container -->
</div>
<!-- end wrapper -->


<!-- Footer -->
@include('layouts.footer')
<!-- End Footer -->


@include('layouts.javas')

</body>
</html>
<!-- Localized -->