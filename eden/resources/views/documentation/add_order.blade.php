<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    @include('layouts.title')
    <meta content="Admin Dashboard" name="description" />
    <meta content="Themesdesign" name="Mikumi Freight" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <link rel="shortcut icon" href="/assets/images/favicon.ico">


    <link href="/assets/css/bootstrap337.min.css" rel="stylesheet">
    <link href="/assets/plugins/datatables/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
    <link href="/assets/plugins/datatables/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css" />

    <link href="/assets/plugins/datatables/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css" />

    <link href="/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="/assets/css/icons.css" rel="stylesheet" type="text/css" />
    <link href="/assets/css/style.css" rel="stylesheet" type="text/css" />
    {{--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>--}}
    <link href="/assets/css/select2.min.css" rel="stylesheet" type="text/css" />
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $(".Other").change(function(){
                $(this).find("option:selected").each(function(){
                    var  Other = $('.Other').val();

                    if(Other == 'Other')
                    {

                        $('.shipper_other').show();
                    }

                    else
                    {
                        $('.shipper_other').hide();
                    }

                });
            }).change();

        });
    </script>
</head>
<style type="text/css">

    body{
        margin-top:40px;
    }

    .stepwizard-step p {
        margin-top: 10px;
    }

    .stepwizard-row {
        display: table-row;
    }

    .stepwizard {
        display: table;
        width: 100%;
        position: relative;
    }

    .stepwizard-step button[disabled] {
        opacity: 1 !important;
        filter: alpha(opacity=100) !important;
    }

    .stepwizard-row:before {
        top: 14px;
        bottom: 0;
        position: absolute;
        content: " ";
        width: 100%;
        height: 1px;
        background-color: #ccc;
        z-order: 0;

    }

    .stepwizard-step {
        display: table-cell;
        text-align: center;
        position: relative;
    }

    .btn-circle {
        width: 30px;
        height: 30px;
        text-align: center;
        padding: 6px 0;
        font-size: 12px;
        line-height: 1.428571429;
        border-radius: 15px;
    }
    .size{
        height: 34px !important;
    }
    .dt-buttons{
        margin-left: -2% !important;
    }
    .has-submenu{
        padding: 0px 10px 0px 30px;
    }
</style>
<body>

@include('layouts.documentation_sidebar')
<div class="wrapper">
    <div class="container-fluid">
        <div class="row pt-3">
            <div class="col-sm-12">
                <div class="page-title-box" style="padding: 20px 0 !important;">
                    <div class="btn-group pull-right">
                        <ol class="breadcrumb hide-phone p-0 m-0">
                            <li class="breadcrumb-item"><a href="#">Orders</a></li>
                            <li class="breadcrumb-item active">Dashboard</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Add Order</h4>
                </div>
            </div>
            <div class="col-12">
                <div class="card m-b-30">
                    <div class="card-body mt-5">
                        <div class="container">
                            <div class="row">
                                <div class="container">
                                    <div class="stepwizard">
                                        <div class="stepwizard-row setup-panel">
                                            <div class="stepwizard-step">
                                                <a href="#step-1"  class="btn btn-primary btn-circle">1</a>
                                                <p>Step 1</p>
                                            </div>
                                            <div class="stepwizard-step">
                                                <a href="#step-2"  class="btn btn-default btn-circle" disabled="disabled">2</a>
                                                <p>Step 2</p>
                                            </div>
                                            {{--<div class="stepwizard-step">--}}
                                            {{--<a href="#step-3" class="btn btn-default btn-circle" disabled="disabled">3</a>--}}
                                            {{--<p>Step 3</p>--}}
                                            {{--</div>--}}
                                            <div class="stepwizard-step">
                                                <a href="#step-4"  class="btn btn-default btn-circle" disabled="disabled">3</a>
                                                <p>Step 3</p>
                                            </div>
                                        </div>
                                    </div>
                                    <form role="form" class="form-horizontal"  action="/admin_add_order" method="POST" enctype="multipart/form-data">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                        <div class="row setup-content" id="step-1">
                                            <div class="col-xs-12">
                                                <div class="col-md-12">
                                                    <div class="col-md-6">
                                                        <label>  Order Created Date </label>
                                                        <input type="date" class="form-control" name="date" placeholder="Enter Date"  >
                                                        <input type="hidden"  class="form-control" name="current_status" value="Receiving the documents"  >
                                                    </div>
                                                    <div class="col-md-6">
                                                        <label> Bill of lading </label>
                                                        <input type="text" class="form-control" name="bill_lading" placeholder="Enter bill of lading" value="0">
                                                    </div>
                                                    <div class="col-md-6">
                                                        <label> Client Name / Code Consignee </label>
                                                        <input type="text" class="form-control" name="client_name" placeholder="Enter client name" value="0" >
                                                    </div>


                                                    <div class="col-md-6">
                                                        <label> Type Container </label>
                                                        <select type="text" class="form-control size all" id="type"  name="type" required>
                                                            <option value="0"> Select Cargo Type </option>
                                                            <option value="20ft "> 20ft </option>
                                                            <option value="40ft "> 40ft </option>
                                                            <option value="LooseCargo"> Loose Cargo </option>
                                                        </select>
                                                    </div>

                                                    <div class="col-md-3">
                                                        <label>Weight</label>
                                                        <input type="text" class="form-control" name="weight" placeholder="Enter weight" value="0">
                                                    </div>
                                                    <div class="col-md-3">
                                                        <label> Frequency </label>
                                                        <select class="form-control size" name="frequency">
                                                            <option value="KG">KG</option>
                                                            <option value="Tone">Tone </option>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-6" id="extra" style="display: none">
                                                        <label> Loose Cargo Type</label>
                                                        <select type="text" class="form-control cargo_type size " id="cargo" name="loose_type" required>
                                                            <option value="0"> Choose  </option>
                                                            <option value="General "> General Cargo </option>
                                                            <option value="MotorVehicle"> Motor Vehicle </option>
                                                        </select>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <label>  Line/ Vessel </label>
                                                        <input type="text" class="form-control" name="line_vessel" placeholder="Enter line vessel" value="0">
                                                    </div>

                                                    {{--<div class="col-md-6 field_wrapper container" id="chassisN" style="display: none">--}}
                                                    {{--<div class="multi-field-wrapper2 container " >--}}
                                                    {{--<div class="multi-fields2">--}}
                                                    {{--<div class="row multi-field2">--}}
                                                    {{--<input type="hidden" name="count" value="1" />--}}
                                                    {{--<label class="control-label" for="field1">Chassis Number.</label>--}}
                                                    {{--<input type="text" id="projectinput1" class="form-control" name="cont_no[]" value=""  placeholder="Enter Chass" >--}}

                                                    {{--<button type="button" class="remove-field2 btn btn-success "><i class="fa fa-times-circle"></i></button>--}}
                                                    {{--<button class="remove-field2 btn btn-success btn" type="button" style="height: 32px;width: 40px;margin: 1.8em 10px 0px 0px;" >+</button>--}}
                                                    {{--<button  class="remove-field2 btn btn-success btn" type="button" style="height: 32px;width: 40px;margin: 1.8em 10px 0px 0px;">-</button>--}}
                                                    {{--<button type="button" class="remove-field2 btn btn-danger" style="height: 32px;width: 40px;margin: 1.8em 10px 0px 0px;">-</button>--}}
                                                    {{--<button type="button" class="add-field2 btn btn-success" style="height: 32px;width: 40px;margin: 1.8em 10px 0px 0px;">+</button>--}}
                                                    {{--</div>--}}
                                                    {{--</div>--}}
                                                    {{--</div>--}}
                                                    {{--<label class="control-label" for="field1">Chassis no .</label>--}}
                                                    {{--<input type="text" class="form-control" name="cont_no[]" value="" placeholder="Enter no" width="100"/>--}}

                                                    {{--<a  href="javascript:void(0);" class="add_button my-4  btn btn-success btn-lg" title="Add field"><i class=" fa fa-plus"></i> </a>--}}


                                                    {{--</div>--}}
                                                    {{--<div class="col-md-6 field_wrapper container" id="Packages" style="display: none;" >--}}
                                                    {{--<label class="control-label" for="field1">Packages.</label>--}}
                                                    {{--<input type="text" class="form-control" name="cont_no[]" value="" placeholder="Enter no" width="100"/>--}}

                                                    {{--<a  href="javascript:void(0);" class="add_button my-4  btn btn-success btn-lg" title="Add field"><i class=" fa fa-plus"></i> </a>--}}
                                                    {{--</div>--}}
                                                    <div class="col-md-3">
                                                        <label> Eta </label>
                                                        <input type="date" class="form-control" name="eta" placeholder="Enter eta"  >
                                                    </div>
                                                    <div class="col-md-3">
                                                        <label> Ata </label>
                                                        <input type="date" class="form-control" name="ata" placeholder="Enter ata" >
                                                    </div>

                                                    <div class="col-md-6">
                                                        <label> Documents Received Date </label>
                                                        <input type="date" class="form-control" name="document_date_received" placeholder="Enter document date received"  >
                                                    </div>

                                                    <div class="col-md-6">
                                                        <label>   C&F At Destination </label>
                                                        <select class="form-control expenses" name="agent_id"  required>
                                                            <option value=""> --- Choose ---</option>
                                                            @foreach($agents as $agents)
                                                                <option value="{{$agents->id}}">{{$agents->organization_name}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <label> Regime </label>
                                                        <select class="form-control expenses" name="declaration_id" required >
                                                            <option value="">  Select   </option>
                                                            @foreach($declaration_type as $declarations)
                                                                <option value="{{$declarations->id}}">{{$declarations->cargo_declaration_type}}</option>
                                                            @endforeach
                                                        </select>

                                                    </div>
                                                    <div class="col-md-6">
                                                        <label>Country</label>
                                                        <select class="form-control  country" name="country_id">
                                                            <option value=""> Choose </option>
                                                            @foreach($country as $key => $value)
                                                                <option value="{{ $key }}">{{$value}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <label> Destination </label>
                                                        <select class="form-control destination" name="destination" >
                                                            <option value="">Select </option>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <label> Border Exit </label>
                                                        <select class="form-control country" name="border_exist" required>
                                                            <option value=""> Choose</option>
                                                            <option value="Rusumo">Rusumo</option>
                                                            <option value="Kabanga">Kabanga</option>
                                                            <option value="Tunduma">Tunduma</option>
                                                            <option value="Mtukula">Mtukula</option>
                                                        </select>
                                                    </div>


                                                    <div class="col-md-6 field_wrapper container" >
                                                        {{--<div class="multi-field-wrapper2 container" >--}}
                                                        {{--<div class="multi-fields2">--}}
                                                        {{--<div class="row multi-field2">--}}
                                                        {{--<input type="hidden" name="count" value="1" />--}}

                                                        {{--<input type="text" id="projectinput1" class="form-control" name="cont_no[]" required placeholder="Enter Cont no" >--}}

                                                        {{--<button type="button" class="remove-field2 btn btn-success "><i class="fa fa-times-circle"></i></button>--}}
                                                        {{--<button class="remove-field2 btn btn-success btn" type="button" style="height: 32px;width: 40px;margin: 1.8em 10px 0px 0px;" >+</button>--}}
                                                        {{--<button  class="remove-field2 btn btn-success btn" type="button" style="height: 32px;width: 40px;margin: 1.8em 10px 0px 0px;">-</button>--}}
                                                        {{--<button type="button" class="remove-field2 btn btn-danger" style="height: 32px;width: 40px;margin: 1.8em 10px 0px 0px;">-</button>--}}
                                                        {{--<button type="button" class="add-field2 btn btn-success" style="height: 32px;width: 40px;margin: 1.8em 10px 0px 0px;">+</button>--}}
                                                        {{--</div>--}}
                                                        {{--</div>--}}
                                                        {{--</div>--}}


                                                        {{--<div class="field_wrapper">--}}
                                                        {{--<div>--}}
                                                        <label class="control-label" for="field1" id="container" style="float: left;">Cont-no.</label>
                                                        <label class="control-label" for="field1" id="Packages" style="display: none; float: left;">Packages.</label>
                                                        <label class="control-label" for="field1" id="chassisN" style="display: none; float: left;">Chassis Number.</label>
                                                        <input type="text" class="form-control" name="cont_no[]" value="0" placeholder="Enter no" width="100"/>

                                                        <a  href="javascript:void(0);" class="add_button my-4  btn btn-success btn-lg" title="Add field"><i class=" fa fa-plus"></i> </a>
                                                        {{--</div>--}}
                                                    </div>




                                                </div>
                                                <button class="btn btn-primary mx-3 my-4 nextBtn btn-lg pull-right" type="button" >Next</button>
                                                <button class="btn btn-success  my-4 btn-lg pull-right" type="submit" >Save</button>
                                            </div>
                                        </div>
                                        <div class="row setup-content" id="step-2">
                                            <div class="col-xs-12">

                                                <div class="col-md-12">

                                                    <div class="col-md-6">
                                                        <label> Terminal   </label>
                                                        <select class="form-control country" name="terminal" required>
                                                            <option value=""> Choose</option>
                                                            <option value="TICTS">TICTS</option>
                                                            <option value="TPA">TPA</option>
                                                            <option value="AZAM ICD">AZAM ICD</option>
                                                            <option value="JEFAG ICD">JEFAG ICD</option>
                                                            <option value="TRH ICD">TRH ICD</option>
                                                            <option value="KICD">KICD</option>
                                                            <option value="AFRICAN ICD">AFRICAN ICD</option>
                                                            <option value="DICD">DICD</option>
                                                            <option value="GALCO ICD">GALCO ICD</option>
                                                            <option value="AMI ICD">AMI ICD</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-6 shipper">
                                                        <label> Shipping line   </label>
                                                        <select class="form-control country Other" name="shipping_line" required>
                                                            <option value=""> Choose</option>
                                                            <option value="PACIFIC INTERNATIONAL LINES (PIL)">PACIFIC INTERNATIONAL LINES (PIL)</option>
                                                            <option value="TANZANIA SHIPPING (WECU LINE)">TANZANIA SHIPPING (WECU LINE)</option>
                                                            <option value="MSC">MSC</option>
                                                            <option value="CMA CGM">CMA CGM</option>
                                                            <option value="EVERGREEN">EVERGREEN</option>
                                                            <option value="MAERSK LINE">MAERSK LINE</option>
                                                            <option value="SAFMARINE">SAFMARINE</option>
                                                            <option value="SEAFORTH">SEAFORTH</option>
                                                            <option value="COSCO SHIPPING LINE">COSCO SHIPPING LINE</option>
                                                            <option value="IGNAZIO MESSINA & C">IGNAZIO MESSINA & C</option>
                                                            <option value="INCHCAPE SHIPPING SERVICES (HAPAG)">INCHCAPE SHIPPING SERVICES (HAPAG)</option>
                                                            <option value="DIAMOND (GOLD STAR)">DIAMOND (GOLD STAR)</option>
                                                            <option value="ONE (NYK) (WOSAC)">ONE (NYK) (WOSAC)</option>
                                                            <option value="Other">Other</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <label> Shipper   </label>
                                                        <input type="text" class="form-control" name="shipper" placeholder="Enter Shipper Name" value="0">
                                                    </div>
                                                    <div class="col-md-6 shipper_other">
                                                        <label>Shipping Line</label>
                                                        <input type="text" class="form-control" name="shipper_other"  value="" placeholder="Enter Shipping Line">
                                                    </div>



                                                    <div class="col-md-6">
                                                        <label> Cargo  Description / Commodity </label>
                                                        <input type="text" class="form-control" name="commodity" placeholder="Enter commodity" value="0">
                                                    </div>

                                                    <div class="col-md-6">
                                                        <label> Voyager Number </label>
                                                        <input type="text" class="form-control" name="voyager_number" placeholder="Enter Voyage Number" value="0">
                                                    </div>
                                                    <div class="col-md-6">
                                                        <label> Remarks </label>
                                                        <input type="text" class="form-control" name="remarks" placeholder="Enter remarks"  value="0">
                                                    </div>
                                                    <div class="col-md-6">
                                                        <label> Special Instruction </label>
                                                        <input type="text" class="form-control" name="special_instruction" placeholder="Enter Special Instruction"  value="0" >
                                                    </div>
                                                    <div class="col-md-6">
                                                        <label> Invoice Doc </label>
                                                        <input type="file" class="form-control" name="invoice_order" value="0">
                                                    </div>
                                                    <div class="col-md-6">
                                                        <label> Bill of lading Doc  </label>
                                                        <input type="file" class="form-control" name="bl_doc" value="0">
                                                    </div>
                                                    <div class="col-md-6">
                                                        <label> Packing List Doc </label>
                                                        <input type="file" class="form-control" name="package_list_doc" value="0">
                                                    </div>
                                                </div>
                                                <button class="btn btn-primary mx-3 my-4 nextBtn btn-lg pull-right" type="button" >Next</button>
                                                <button class="btn btn-success  my-4 btn-lg pull-right" type="submit" >Save</button>
                                            </div>
                                        </div>

                                    </form>
                                    <div class="row setup-content" id="step-4">
                                        <div class="col-xs-12">
                                            <div class="col-md-12">
                                                <h3>Completed</h3>
                                                <p>You have successfully completed all steps.</p>
                                                {{--<button  type="submit" class="btn btn-success btn-lg pull-right">Finish!</button>--}}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('layouts.footer')

@include('layouts.javas')
<script type="text/javascript">
    $(document).ready(function () {

        var navListItems = $('div.setup-panel div a'),
            allWells = $('.setup-content'),
            allNextBtn = $('.nextBtn');

        allWells.hide();

        navListItems.click(function (e) {
            e.preventDefault();
            var $target = $($(this).attr('href')),
                $item = $(this);

            if (!$item.hasClass('disabled')) {
                navListItems.removeClass('btn-primary').addClass('btn-default');
                $item.addClass('btn-primary');
                allWells.hide();
                $target.show();
                $target.find('input:eq(0)').focus();
            }
        });

        allNextBtn.click(function(){
            var curStep = $(this).closest(".setup-content"),
                curStepBtn = curStep.attr("id"),
                nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
                curInputs = curStep.find("input[type='text'],input[type='url']"),
                isValid = true;

            $(".form-group").removeClass("has-error");
            for(var i=0; i<curInputs.length; i++){
                if (!curInputs[i].validity.valid){
                    isValid = false;
                    $(curInputs[i]).closest(".form-group").addClass("has-error");
                }
            }

            if (isValid)
                nextStepWizard.removeAttr('disabled').trigger('click');
        });

        $('div.setup-panel div a.btn-primary').trigger('click');
    });
</script>

<script>
    $(document).ready(function(){
        $('.size').on('change', function(e) {
            e.preventDefault();
            if (document.getElementById('type').value == 'LooseCargo') {
                document.getElementById('extra').style.display = 'block';
                document.getElementById('container').style.display = 'none';
                document.getElementById('chassisN').style.display = 'block';
                document.getElementById('Packages').style.display = 'none';

            }


            else {
                document.getElementById('container').style.display = 'block';
                document.getElementById('chassisN').style.display = 'none';
                document.getElementById('extra').style.display = 'none';
                document.getElementById('Packages').style.display = 'none';
            }
        });

        $('.cargo_type').on('change', function(e) {
            e.preventDefault();
            if (document.getElementById('cargo').value == 'MotorVehicle') {
                document.getElementById('chassisN').style.display = 'block';
                document.getElementById('Packages').style.display = 'none';
            }
            else {
                document.getElementById('chassisN').style.display = 'none';
                document.getElementById('Packages').style.display = 'block';

            }
        });

        $('.declaration').on('change', function(e) {
            e.preventDefault();
            if (document.getElementById('declaration').value == 'Clear') {
                document.getElementById('amount').style.display = 'none';


            }

            else {
                document.getElementById('amount').style.display = 'block';;
            }
        });
    });


</script>

<script type="text/javascript">
    $(document).ready(function() {


        $('select[name="country_id"]').on('change', function() {
            var stateID = $(this).val();
            if(stateID) {
                $.ajax({
                    url: '/country_name/ajax/'+stateID,
                    type: "GET",
                    dataType: "json",
                    success:function(data) {


                        $('select[name="destination"]').empty();
                        $.each(data, function(key, value) {
                            $('select[name="destination"]').append('<option value="'+ value +'">'+ value +'</option>');
                        });


                    }
                });
            }else{
                $('select[name="destination"]').empty();
            }
        });
    });
</script>

</body>
</html>