
<!DOCTYPE html>
<html>
@include('layouts.admin_view_order')

<style type="text/css">
    .has-submenu{
        padding: 0px 10px 0px 30px !important;
        font-weight: 700 !important;
    }
    .dropdown-menu {
        padding: 7px 50px;
        font-size: 15px;
        box-shadow: 0 2px 31px rgba(0, 0, 0, 0.08);
        border-color: #eff3f6;
        margin-top: 17px;
        min-width: 21rem;
        font-weight: 400;
    }
</style>

<body class="fixed-left">
<div id="wrapper">
    @include('layouts.documentation_sidebar')

    <div class="content-page">
        <div class="content">

            <div class="page-content-wrapper ">

                <div class="container-fluid">

                    @foreach($track as $value)
                        <div class="row pt-3 pb-5" style="padding-top: 5rem !Important;">
                            <div class="col-lg-12">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="col-lg-12">
                                            <h4 class="font-16"><strong> File Number:  {{$value->file_no}}</strong></h4>
                                            <h2 class="mt-0 header-title " >Document Received: {{$value->document_date_received}}</h2>
                                            <h4 class="mt-0 header-title">Bill of lading : <span style="font-size: small">{{$value->bill_lading}}</span> </h4>
                                            <h4 class="mt-0 header-title">Current Status : <span style="font-size: small" class="badge badge-primary">{{$value->current_status}}</span> </h4>

                                        </div>
                                        <div class="col-lg-12">
                                            <img src="/assets/images/slogo.jpeg" class="rounded-circle img-circle  float-right" alt="..." style="margin-top: -8rem; width: 16%;">
                                        </div>

                                        <div class="col-lg-12">
                                            <!-- Nav tabs -->
                                            <ul class="nav nav-tabs" role="tablist">
                                                <li class="nav-item">
                                                    <a class="nav-link active" data-toggle="tab" href="#home" role="tab" style="color: black;"><strong>Consignee</strong></a>
                                                </li>

                                                <li class="nav-item">
                                                    <a class="nav-link tabs" data-toggle="tab" href="#eta" role="tab"> <strong>ETA</strong></a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link tabs" data-toggle="tab" href="#ata" role="tab"><strong> ATA</strong></a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link tabs" data-toggle="tab" href="#shipping_line" role="tab"><strong>Shipping Line </strong></a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link tabs" data-toggle="tab" href="#shipper" role="tab"><strong>Shipper</strong></a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link tabs" data-toggle="tab" href="#vessel" role="tab"><strong>Vessel Name</strong></a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link tabs" data-toggle="tab" href="#documents" role="tab"><strong>Documents</strong></a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link tabs" data-toggle="tab" href="#profile" role="tab"><strong>Others</strong></a>
                                                </li>

                                            </ul>
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="tab-content">
                                                <div class="tab-pane active p-3" id="home" role="tabpanel">
                                                    <p class="font-14 mb-0">
                                                        <strong>Client / Consignee :  </strong> {{$value->client_name}} <br>
                                                    </p>
                                                </div>

                                                <div class="tab-pane p-3" id="eta" role="tabpanel">
                                                    <p class="font-14 mb-0">
                                                        <strong>ETA : </strong>{{$value->eta}}  <br>
                                                    </p>
                                                </div>
                                                <div class="tab-pane p-3" id="ata" role="tabpanel">
                                                    <p class="font-14 mb-0">
                                                        <strong>ATA : </strong>{{$value->ata}}  <br>
                                                    </p>
                                                </div>
                                                <div class="tab-pane p-3" id="shipping_line" role="tabpanel">
                                                    <p class="font-14 mb-0">
                                                        <strong>Shipping Line: </strong>{{$value->shipping_line}}  <br>
                                                        <strong>Shipping Address: </strong>{{$value->shipper_address}}  <br>
                                                    </p>
                                                </div>
                                                <div class="tab-pane p-3" id="vessel" role="tabpanel">
                                                    <p class="font-14 mb-0">
                                                        <strong>Vessel Name: </strong>{{$value->line_vessel}}  <br>
                                                    </p>
                                                </div>
                                                <div class="tab-pane p-3" id="shipper" role="tabpanel">
                                                    <p class="font-14 mb-0">
                                                        <strong>Shipper : </strong>{{$value->shipper}}  <br>
                                                        <strong>Address : </strong>{{$value->shipper_address}}  <br>
                                                    </p>
                                                </div>
                                                <div class="tab-pane p-3" id="declaration" role="tabpanel">
                                                    <p class="font-14 mb-0">
                                                        <strong >Declaration Status: </strong> {{$value->declaration}} <br>
                                                        <strong >Amount : </strong> {{$value->amount}} <br>
                                                    </p>
                                                </div>

                                                <div class="tab-pane p-3" id="profile" role="tabpanel">
                                                    <p class="font-14 mb-0">
                                                        <strong>Cargo Description : </strong> {{$value->cargo_description}} <br>
                                                        <strong>Weight as per  BL : </strong>{{$value->weight}}  <br>
                                                        <strong>Port Charges :</strong>{{$value->port_charge}}  <br>
                                                        <strong>Weight  : </strong>{{$value->weight}} {{$value->frequency}}  <br>
                                                        <strong>Voyage Number  : </strong>{{$value->voyager_number}}  <br>
                                                        <strong>Guart Amount Taken  : </strong>{{number_format($value->guart_amount_taken)}}  <br>
                                                        <strong>Voyage Guart Amount Returned  : </strong>{{number_format($value->guart_amount_returned)}}  TSH <br>
                                                    </p>
                                                </div>
                                                <div class="tab-pane p-3" id="documents" role="tabpanel">
                                                    <p class="font-14 mb-0">
                                                        <strong>Delivery Order :</strong>
                                                        @if($value->delivery_order != '')
                                                            <a href="/DeliveryOrder/{{$value->delivery_order}}"  target="_blank">file</a>

                                                        @else

                                                            <a href="#" style="color: red">no file</a>

                                                        @endif
                                                        <br>
                                                        <strong>Port Charges File</strong>
                                                        @if($value->port_charge_invoice != '')
                                                            <a href="/PortCharges/{{$value->port_charge_invoice}}"  target="_blank">file</a>

                                                        @else

                                                            <a href="#"  style="color: red;">no file</a>

                                                        @endif <br>
                                                        <strong>Invoice Doc </strong>
                                                        @if($value->invoice_order != '')
                                                            <a href="/OrderDoc/{{$value->invoice_order}}"  target="_blank">file</a>

                                                        @else

                                                            <a href="#"  style="color: red;">no file</a>

                                                        @endif <br>
                                                        <strong>Bill of lading Doc </strong>
                                                        @if($value->bl_doc != '')
                                                            <a href="/OrderDoc/{{$value->bl_doc}}"  target="_blank">file</a>

                                                        @else

                                                            <a href="#"  style="color: red">no file</a>

                                                        @endif <br>
                                                        <strong>Packing List Doc</strong>
                                                        @if($value->package_list_doc != '')
                                                            <a href="/OrderDoc/{{$value->package_list_doc}}"  target="_blank">file</a>

                                                        @else

                                                            <a href="#"  style="color: red">no file</a>

                                                        @endif <br>

                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="panel panel-default">
                                                <div class="pt-3">
                                                    <h5 class="panel-title font-20 mx-2"><strong>CARGO LOADING DETAILS </strong></h5>
                                                </div>
                                                <div class="">
                                                    <div class="table-responsive">
                                                        <table class="table ">
                                                            <thead>
                                                            <tr>
                                                                <th>#</th>
                                                                <th><strong>Container no / Chassis no </strong></th>
                                                                <th class="text-center"><strong>Size /  Units</strong></th>
                                                                <th class="text-center"><strong>Transporter</strong>
                                                                </th>
                                                                <th  class="text-center"><strong>Truck / Trailer No</strong>
                                                                </th>
                                                                <th  class="text-center"><strong>Date of Loading</strong></th>
                                                                <th  class="text-center"><strong>Driver</strong></th>
                                                                <th  class="text-center"><strong>Driver Contact</strong></th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            @foreach($containers as $cont)
                                                                <tr>
                                                                    <td>{{$cont->id}}</td>
                                                                    <td >{{$cont->cont_no}}</td>
                                                                    <td class="text-center">{{$cont->type}}</td>
                                                                    <td class="text-center">{{$cont->transporter_name}}</td>
                                                                    <td class="text-center">{{$cont->trucking_number}}</td>
                                                                    <td class="text-center">{{$cont->loading_date}}</td>
                                                                    <td class="text-center">{{$cont->driver_name}}</td>
                                                                    <td class="text-center">{{$cont->driver_contact}}</td>
                                                                </tr>
                                                            @endforeach
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-lg-12">
                                                {{--<address>--}}
                                                {{--Invoice Date : {{$value->invoice_date}} <br>--}}
                                                {{--Invoice By :  {{$value->invoice_by}}<br>--}}
                                                {{--Checked By  :  {{$value->checked_by}}<br>--}}
                                                {{--</address>--}}

                                                <address class="pt-4">
                                                    <strong>Special Instruction  if any : {{$value->special_instruction}} </strong><br>
                                                    Remarks  : {{$value->remarks}} <br>
                                                </address>
                                                @if (Session::has('message'))
                                                    <div class="alert alert-success alert-dismissable">
                                                        <a href="" class="close" data-dismiss="alert" aria-label="close">
                                                            &times;

                                                        </a>
                                                        {{ Session::get('message') }}
                                                    </div>

                                                @endif
                                                @if (Session::has('delete'))
                                                    <div class="alert alert-success alert-dismissable">
                                                        <a href="" class="close" data-dismiss="alert" aria-label="close">
                                                            &times;

                                                        </a>
                                                        {{ Session::get('delete') }}
                                                    </div>

                                                @endif
                                                @if (Session::has('updated'))
                                                    <div class="alert alert-success alert-dismissable">
                                                        <a href="" class="close" data-dismiss="alert" aria-label="close">
                                                            &times;

                                                        </a>
                                                        {{ Session::get('updated') }}
                                                    </div>

                                                @endif
                                                <h4 class="panel-title font-20 pt-5"><strong>* Trucking Updates </strong></h4>

                                                <table class="table table-bordered">
                                                    <thead>
                                                    <tr style="background-color: #8dc3428a; color: #000 !important;">
                                                        <th scope="col">#</th>
                                                        <th>Bill of lading</th>
                                                        <th>Container no / Chassis no </th>
                                                        <th scope="col">Processing Date  </th>
                                                        <th scope="col">Updated Status</th>
                                                        <th>Comments</th>
                                                        <th scope="col"> Done by</th>
                                                        <th>Actions</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody >
                                                    <?php $i=1;?>
                                                    @foreach($status as $updates)
                                                        <tr>
                                                            <th scope="row">{{$i++}}</th>
                                                            <td>{{$updates->bill_lading}}   </td>
                                                            <td>{{$updates->cont_no}}</td>
                                                            <td>{{$updates->processing_date}}   </td>
                                                            <td>{{$updates->processing_status}}</td>
                                                            <td>{{$updates->other_comment}}</td>
                                                            <td>{{$updates->name}} -- {{$updates->updated_at}}</td>
                                                            <td>

                                                                <button data-toggle="modal" data-target="#editleads<?php echo $i;?>" class="pull-left edit  btn-sm  btn btn-primary dlt_sm_table mx-2 mt-2"><i class="fa fa-pencil"></i></button>

                                                                <button data-toggle="modal" data-target="#delete<?php echo $i;?>" class="pull-left edit btn-sm  mx-2 mt-2 btn-danger dlt_sm_table"><i class="fa fa-trash"></i></button>
                                                                <div class="modal fade" id="delete<?php echo $i;?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLarge01" style="display: none;" aria-hidden="true">
                                                                    <div class="modal-dialog modal-lg" role="document">
                                                                        <div class="modal-content">
                                                                            <div class="modal-header">
                                                                                <h5 class="modal-title">Delete </h5>
                                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                    <span aria-hidden="true">×</span>
                                                                                </button>
                                                                            </div>
                                                                            <form class="pull-left" action="/delete/status/{{$updates->id}}" method="POST">
                                                                                <label class="mx-4">Are you sure you want to delete</label>
                                                                                <input type="hidden" name="_method" value="DELETE" />
                                                                                <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                                                                <div class="modal-footer">
                                                                                    <button type="submit" class="btn btn-success">Confirm</button>
                                                                                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                                                                </div>
                                                                            </form>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div id="editleads<?php echo $i;?>" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
                                                                    <div class="modal-dialog modal-lg">
                                                                        <div class="modal-content">
                                                                            <div class="modal-header">
                                                                                <h5 class="modal-title" id="editleads"> Edit Cargo Updates </h5>
                                                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

                                                                            </div>
                                                                            <div class="modal-body">
                                                                                <div class="row">
                                                                                    <form role="form" action="/updates/status/{{$updates->id}}" method="post" enctype="multipart/form-data">
                                                                                        <input type="hidden" name="_method" value="PUT" />
                                                                                        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                                                                        <div class="row container  pt-3">
                                                                                            <div class="col-md-6 ">
                                                                                                <label>Bill of lading</label>
                                                                                                <select class="form-control all " name="ladding_id"  required>
                                                                                                    <option value="{{$updates->ladding_id}}">{{$updates->bill_lading}}</option>

                                                                                                </select>
                                                                                            </div>
                                                                                            <div class="col-md-6">
                                                                                                <label>Container Number</label>
                                                                                                <select name="cont_no" class="form-control form_size">
                                                                                                    <option value="">{{$updates->cont_no}}</option>
                                                                                                </select>
                                                                                            </div>
                                                                                            <div class="col-md-6">
                                                                                                <label>Processing date </label>
                                                                                                <input type="date"  name="processing_date" value="{{$updates->processing_date}}" class="form-control" placeholder="Enter Processing date " required>
                                                                                            </div>
                                                                                            <div class="col-md-6">
                                                                                                <label>Processing status </label>
                                                                                                <select class="form-control" name="processing_status" required>

                                                                                                    <option value="{{$updates->processing_status}}" >{{$updates->processing_status}}</option>
                                                                                                    <option value="Receiving the documents"> Receiving the documents </option>
                                                                                                    <option value="Under shipping line Procedures"> Under shipping line Procedures </option>
                                                                                                    <option value="Under Customs Procedures"> Under Customs Procedures </option>
                                                                                                    <option value="Release Order, Delivery Order Received"> Release Order, Delivery Order Received </option>
                                                                                                    <option value="Lodge Port charges"> Lodge Port charges </option>
                                                                                                    <option value="Pay Port Charges"> Pay Port Charges </option>
                                                                                                    <option value="Loading"> Loading </option>
                                                                                                    <option value="Truck/ Cargo in route Position"> Truck/ Cargo in route Position </option>
                                                                                                    <option value="Cargo at the Boarder"> Cargo at the Boarder</option>
                                                                                                    <option value="Other"> Other</option>

                                                                                                </select>
                                                                                            </div>
                                                                                            <div class="col-md-12">
                                                                                                <label>Any other Comment</label>
                                                                                                <textarea class="form-control" name="other_comment" >
                                                                                            {{$updates->other_comment}}
                                                                                        </textarea>
                                                                                            </div>
                                                                                            <div class="col-md-12 mt-2 pt-5">
                                                                                                <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Cancel</button>
                                                                                                <button type="submit" class="btn btn-primary waves-effect waves-light">Save changes</button>
                                                                                            </div>
                                                                                        </div>
                                                                                    </form>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                @endforeach
                            </div>
                        </div>
                </div>

                @include('layouts.footer')
            </div>
        </div>

@include('layouts.javas')
</body>
</html>