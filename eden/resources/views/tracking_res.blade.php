<!DOCTYPE html>
<html lang="en">
@include('layouts.page_head')
<style>
    @import url("https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css");
    .track_tbl td.track_dot {
        width: 50px;
        position: relative;
        padding: 0;
        text-align: center;
    }
    .track_tbl td.track_dot:after {
        content: "\f111";
        font-family: FontAwesome;
        position: absolute;
        margin-left: -5px;
        top: 15px;
    }
    .track_tbl td.track_dot span.track_line {
        background: #000;
        width: 3px;
        min-height: 50px;
        position: absolute;
        height: 101%;
    }
    .size_th{
        width: 10% !important;
    }
    .track_tbl tbody tr:first-child td.track_dot span.track_line {
        top: 30px;
        min-height: 25px;
    }
    .track_tbl tbody tr:last-child td.track_dot span.track_line {
        top: 0;
        min-height: 25px;
        height: 10%;
    }
    #title{
        color: #000000;
    }
</style>
<body>
@include('layouts.page_header')

{{--<section class="tracking_search_area">--}}
    {{--<div class="container">--}}
        {{--<form role="form" class="form-horizontal" action="/tracking_cargo" method="POST" enctype="multipart/form-data">--}}
            {{--<input type="hidden" name="_token" value="{{ csrf_token() }}" />--}}
            {{--<div class="tracking_search_inner">--}}
                {{--<h2 class="single_title">Track your Shipment</h2>--}}
                {{--<h5>Enter a tracking number or bill of lading, and get tracking results.</h5>--}}
                {{--<div class="row">--}}
                    {{--<div class="col-md-12">--}}
                        {{--<input type="text" class="form-control" name="track" placeholder="Enter Bill of lading ">--}}
                    {{--</div>--}}

                    {{--<strong>Or </strong>--}}
                    {{--<div class="col-md-12" style="margin-top: 2%;">--}}
                        {{--<input type="text" class="form-control" name="cont_no" placeholder="Enter Container Number ">--}}
                    {{--</div>--}}
                    {{--<div class="col-md-4">--}}
                        {{--<button class="btn btn-primary btn-lg" type="submit" style="border-radius: 0 !important; margin-top: 6%;"><i class="fa fa-circle-o-notch" aria-hidden="true"></i> Track</button>--}}
                    {{--</div>--}}
                {{--</div>--}}


            {{--</div>--}}
        {{--</form>--}}
    {{--</div>--}}
{{--</section>--}}
{{--<a href="/tracking_truck" class="btn btn-success">Track</a>--}}
<section>

</section>

<section class="timeline_tracking_area ">
    <div class="container">
        <a href="/tracking_truck" role="button" class="btn btn-success btn-lg" type="submit" style="border-radius: 0 !important; margin-top: 6%;"><i class="fa fa-circle-o-notch" aria-hidden="true"></i> Track</a>

        {{--<div class="row">--}}
            {{--<div class="timeline_tracking_inner ">--}}
                {{--<div class="timeline_tracking_box col-md-6 mx-auto " style="--}}
    {{--margin-top: 9rem;--}}
{{--">--}}
                    {{--<div class="tracking_in tag-delivered" style="--}}
    {{--width: 58rem;margin-left: -13px;">--}}
                        {{--<h4>Results</h4>--}}
                    {{--</div>--}}
                    {{--<div class="tracking_list">--}}
                        {{--<ul>--}}
                            {{--@forelse($track as $truck)--}}
                                {{--<li>--}}
                                    {{--<div class="checkpoint__time"><strong>{{$truck->processing_date}}</strong></div>--}}
                                    {{--<div class="checkpoint__icon delivered"></div>--}}
                                    {{--<div class="checkpoint__content"> <strong>Bill of Lading : {{$truck->bill_lading}}</strong></div>--}}
                                    {{--<div class="row" style="    margin-left: 0%;margin-top: 1%;">--}}
                                        {{--<div class="checkpoint__content">--}}
                                            {{--<h5 style="padding-bottom: 2%;">Containers list</h5>--}}
                                        {{--</div>--}}

                                        {{--@foreach($cont as $container)--}}
                                            {{--<div class="checkpoint__content"><p> {{$container->cont_no}}</p></div>--}}
                                        {{--@endforeach--}}
                                    {{--</div>--}}

                                    {{--<div class="checkpoint__content"><p style="margin-top: 2%;">Status: {{$truck->processing_status}}</p>--}}
                                        {{--<div class="hint" style="margin-top: 1%;"> Description : {{$truck->other_comment}}</div></div>--}}
                                {{--</li>--}}

                            {{--@empty--}}
                                {{--<div class="hint">--}}
                                    {{--<span style="color: red; font-size: large; margin-left: 44rem;"> no information founded</span>--}}

                                {{--</div>--}}


                        {{--</ul>--}}
                    {{--</div>--}}
                    {{--@endforelse--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
        <div class="p-4" style="margin-top: 3%;">
            <h3 style="color: #000000"> Tracking Results</h3>
            <table class="table table-bordered track_tbl" style="margin-top: 3%;">
                <thead>
                <tr id="title">
                    <th >
                        <center>
                           #
                        </center>
                        </th>
                    <th class="size_th">Date</th>
                    <th class="size_th">Bill of lading</th>
                    <th >Container List</th>
                    <th >Status</th>
                    <th >Description</th>
                </tr>
                </thead>
                <tbody>
                <?php $i=1;?>
                @forelse($track AS $truck)
                    <tr class="active">
                        <td class="track_dot" id="title">
                            <span class="track_line"></span>
                        </td>
                        <td id="title">{{$truck->processing_date}}</td>
                        <td id="title">{{$truck->bill_lading}}</td>
                        <td id="title">
                            @foreach($cont as $container)
                            <div class="checkpoint__content"><p> {{$container->cont_no}}</p></div>
                            @endforeach

                        </td>
                        <td id="title">{{$truck->processing_status}}</td>
                        <td id="title"> {{$truck->other_comment}}</td>
                    </tr>
                    @empty
                    <tr>

                            <div class="hint">
                                <span style="color: red; font-size: large; margin-left: 44rem;"> no  results</span>
                            </div>

                    </tr>


                @endforelse
                </tbody>
            </table>
        </div>
    </div>
</section>



<footer class="footer_area">
    <div class="footer_widget_area">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-xs-6">
                    <aside class="f_widget about_widget">
                        {{--<img src="landing/img/footer-logo.png" alt="">--}}
                        <h4 style="color: #ffffff;">Mikumi Freight Forwarders Ltd</h4>
                        <p>Mikumi Freight Forwarders Limited, is a fully licensed private company which engages in the provision of high quality domestic and international freight</p>
                        <ul>
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                            <li><a href="#"><i class="fa fa-pinterest"></i></a></li>
                        </ul>
                    </aside>
                </div>
                <div class="col-md-3 col-xs-6">
                    <aside class="f_widget link_widget">
                        <div class="f_title">
                            <h3>Quick Links</h3>
                        </div>
                        <ul>
                            <li><a href="#">INVESTER RELATIONS</a></li>
                            <li><a href="#">PRESS & MEDIA</a></li>
                            <li><a href="#">COOKIE POLICY</a></li>
                            <li><a href="#">TERMS & CONDITIONS</a></li>
                        </ul>
                    </aside>
                </div>
                <div class="col-md-3 col-xs-6">
                    <aside class="f_widget service_widget">
                        <div class="f_title">
                            <h3>Services</h3>
                        </div>
                        <ul>
                            <li><a href="#">Standard Air Freight Services</a></li>
                            <li><a href="#">Sea Freight Services</a></li>
                            <li><a href="#">Full loads and part loads</a></li>
                            <li><a href="#">Specialized Transport</a></li>
                            <li><a href="http://apps.mikumifreight.com">Application</a></li>
                        </ul>
                    </aside>
                </div>
                <div class="col-md-3 col-xs-6">
                    <aside class="f_widget info_widget">
                        <div class="f_title">
                            <h3>Contact</h3>
                        </div>
                        <div class="contact_details">
                            <p>Harbour View Tower 8th Floor , room 808,  Samora Avenue, Dar es salaam, Tanzania</p>
                            <p>Phone: <a href="tel:+1-(255)-7899">+255 714 307 315</a></p>
                            <p>Fax: <a href="#">+255 222 112 536/37</a></p>
                            <p>Email: <a href="#"><span class="__cf_email__" data-cfemail="eb838e878784ab87848c82989f828898c5888486" >info@mikumifreight.com</span></a></p>
                        </div>
                    </aside>
                </div>
            </div>
        </div>
    </div>
    <div class="footer_copy_right">
        <div class="container">
            <h4>Copyright ©
                <script data-cfasync="false" src="../../cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script><script>
                    document.write(new Date().getFullYear());
                </script>. All rights reserved.</h4>
        </div>
    </div>
</footer>

@include('layouts.page_footer')
</body>
</html>
<!-- Localized -->