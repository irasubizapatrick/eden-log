<!DOCTYPE html>
<html>

@include('layouts.finance_head')
<style type="text/css">
    .form_size{
        height: 34px !important;
    }
    .select2{
        width: 100% !important;
        margin-top: -4rem;
    }
    .dataTables_paginate{
        display: none;
    }
</style>
<body>
<!-- Navigation Bar-->
@include('layouts.sales_sidebar')
<!-- End Navigation Bar-->

<div class="wrapper">
    <div class="container-fluid">

        <!-- end page title end breadcrumb -->
        <div class="modal fade bill_lading"  role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title mt-0" id="myLargeModalLabel">Search by Bill</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body">
                        <form class="form-horizontal"  action="/search/bill/trans" method="POST" enctype="multipart/form-data">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                            <div class="row col-md-12">
                                <div class="col-md-12">
                                    <label>Bill of lading</label>
                                    <select class="form-control form_size " name="ladding_id"  id="search_ladding_id" required>
                                        <option value="">Select Bill of lading</option>
                                        @foreach($ladding as $key => $value)
                                            <option value="{{ $key }}">{{$value}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-12 pt-5 pb-5">
                                    <button type="submit" class="btn btn-primary waves-effect waves-light">Search </button>
                                    <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div><!-- /.modal-content -->
        </div>
        <div class="modal fade bs-example-modal-lg"  role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title mt-0" id="myLargeModalLabel">Assign Transporter to a Container</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body">
                        <form class="form-horizontal"  action="/all_trans" method="POST" enctype="multipart/form-data">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                            <div class="row col-md-12">
                                <div class="col-md-12">
                                    <label>Bill of lading</label>
                                    <select class="form-control form_size " name="ladding_id"  id="ladding_id" required>
                                        <option value="">Select Bill of lading</option>
                                        @foreach($ladding as $key => $value)
                                            <option value="{{ $key }}">{{$value}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-6">
                                    <label>Container Number</label>
                                    <select name="container_id" class="form-control form_size">
                                        <option value="">Container number</option>
                                    </select>
                                </div>
                                <div class="col-md-6">
                                    <label>Transporter Name</label>
                                    <input type="text" class="form-control"  value="0" name="transporter_name" placeholder="Enter Transporter  name" required>
                                </div>
                                <div class="col-md-6 pull-left">
                                    <label>Truck  / Trailer No</label>
                                    <input type="text" class="form-control"   name="trucking_number"  placeholder="Enter Tricking Number " required>
                                </div>
                                <div class="col-md-6 pull-left">
                                    <label>Truck  Horse File</label>
                                    <input type="file" class="form-control"   name="truck_horse_copy"  placeholder="Enter Tricking Number " required>
                                </div>
                                <div class="col-md-6 pull-left">
                                    <label>Truck Trailer File</label>
                                    <input type="file" class="form-control"   name="truck_trailer_copy"  placeholder="Enter Tricking Number " required>
                                </div>
                                <div class="col-md-6 pull-left">
                                    <label>Date of Loading</label>
                                    <input type="date" class="form-control"   name="loading_date"  placeholder="Enter Tricking Number " required>
                                </div>
                                <div class="col-md-6 pull-left">
                                    <input type="hidden" class="form-control" name="role" value="Agent" required>
                                    <input type="hidden" class="form-control" name="user_id" value="{{Auth::user()->id}}" required>
                                </div>
                                <div class="col-md-12 pt-5 pb-5">
                                    <button type="submit" class="btn btn-primary waves-effect waves-light">Save changes</button>
                                    <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row m-0 p-5">
        <div class="col-sm-12 ">
            <div class="page-title-box">
                <div class="btn-group pull-right">
                    <ol class="breadcrumb hide-phone p-0 m-0">
                        <li class="breadcrumb-item"><a href="#">All Transporter List</a></li>
                        <li class="breadcrumb-item active">Dashboard</li>
                    </ol>
                </div>
                <h4 class="page-title">All Container Transporters</h4>
            </div>
        </div>
        <div class="col-12 container" style="margin-top: -3rem;">
            <div class="card m-b-30 ">
                <div class="card-body">
                    <a href="/all_trans" role="button" class="btn btn-success waves-effect waves-light pull-right mx-3"><i class="fa fa-home"></i> <span> All</span></a>

                    <button type="button" class="btn btn-primary waves-effect waves-light pull-right" data-toggle="modal" data-target=".bs-example-modal-lg "> <i class="fa fa-plus"></i> <span>Add Transporter</span></button>

                    <button type="button" class="btn btn-warning waves-effect waves-light pull-right mx-4" data-toggle="modal" data-target=".bill_lading "><i class="fa fa-search"></i> <span>Search </span></button>

                    @if (Session::has('message'))
                        <div class="alert alert-success alert-dismissable">
                            <a href="" class="close" data-dismiss="alert" aria-label="close">
                                &times;

                            </a>
                            {{ Session::get('message') }}
                        </div>

                    @endif
                    @if (Session::has('delete'))
                        <div class="alert alert-success alert-dismissable">
                            <a href="" class="close" data-dismiss="alert" aria-label="close">
                                &times;

                            </a>
                            {{ Session::get('delete') }}
                        </div>

                    @endif
                    @if (Session::has('updated'))
                        <div class="alert alert-success alert-dismissable">
                            <a href="" class="close" data-dismiss="alert" aria-label="close">
                                &times;
                            </a>
                            {{ Session::get('updated') }}
                        </div>
                    @endif
                    <table id="datatable-buttons" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                        <tr style="font-size: 13px;background-color: #416b8d !important;color: #fff;">
                            <th>#</th>
                            <th>Bill of Loading</th>
                            <th>Date of loading</th>
                            <th>Container No</th>
                            <th>Truck  / Trailer No</th>
                            <th>Transporter Name</th>
                            <th>Truck Horse </th>
                            <th>Truck Trailer</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $i=1;?>
                        @foreach($transporter AS $value)
                            <tr>
                                <td>{{$i++}}</td>
                                <td>{{$value->bill_lading}}</td>
                                <td>{{$value->loading_date}}</td>
                                <td>{{$value->container->cont_no}}</td>

                                <td>{{$value->trucking_number}}</td>
                                <td>{{$value->transporter_name}} </td>
                                <td>
                                    @if($value->truck_horse_copy !="")

                                        <a href="/archives/{{$value->truck_horse_copy}}"  target="_blank">file</a>
                                    @else
                                        <a href="#"  style="color: red;">no file</a>
                                    @endif
                                </td>
                                <td>
                                    @if($value->truck_trailer_copy !="")

                                        <a href="/archives/{{$value->truck_trailer_copy}}"  target="_blank">file</a>
                                    @else
                                        <a href="#"  style="color: red;">no file</a>
                                    @endif
                                </td>
                                <td>
                                    <button data-toggle="modal" data-target="#editleads<?php echo $i;?>" class="pull-left edit  btn-lg  btn btn-primary dlt_sm_table mx-2 mt-2"><i class="fa fa-pencil"></i></button>

                                    <button data-toggle="modal" data-target="#delete<?php echo $i;?>" class="pull-left edit  btn-lg  btn btn-danger dlt_sm_table mx-2 mt-2"><i class="fa fa-trash"></i></button>

                                    <div class="modal fade" id="delete<?php echo $i;?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLarge01" style="display: none;" aria-hidden="true">
                                        <div class="modal-dialog modal-lg" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title">Delete </h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">×</span>
                                                    </button>
                                                </div>
                                                <form class="pull-left" action="/all_trans/{{$value->id}}" method="POST">
                                                    <label class="mx-4">Are you sure you want to delete</label>
                                                    <input type="hidden" name="_method" value="DELETE" />
                                                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                                    <div class="modal-footer">
                                                        <button type="submit" class="btn btn-success">Confirm</button>
                                                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="editleads<?php echo $i;?>" class="modal fade" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
                                        <div class="modal-dialog modal-lg">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h4 class="modal-title" id="editleads"> Edit Transporter for this deal</h4>
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

                                                </div>
                                                <div class="modal-body">
                                                    <div class="row">
                                                        <form role="form" action="/all_trans/{{$value->id}}" method="post" enctype="multipart/form-data">
                                                            <input type="hidden" name="_method" value="PUT" />
                                                            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                                            <div class="row col-md-12">
                                                                <div class="col-md-12">
                                                                    <label>Bill of lading</label>
                                                                    <select class="form-control form_size edit_ladding_id" name="ladding_id"  required>
                                                                        <option value="">Select Bill of lading</option>
                                                                        @foreach($ladding as $key => $all)
                                                                            <option value="{{ $key }}">{{$all}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <label>Container Number</label>
                                                                    <select name="container_id" class="form-control form_size mb-2">
                                                                        <option value="">Container number</option>
                                                                    </select>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <label>Transporter Name</label>
                                                                    <input type="text" class="form-control"  value="{{$value->transporter_name}}" name="transporter_name" placeholder="Enter Transporter  name" required>
                                                                </div>
                                                                <div class="col-md-6 pull-left">
                                                                    <label> Truck  / Trailer No</label>
                                                                    <input type="text" class="form-control"  value="{{$value->trucking_number}}"  name="trucking_number"  placeholder="Enter Tricking Number " required>
                                                                </div>
                                                                <div class="col-md-6 pull-left">
                                                                    <label>Truck  Horse File</label>
                                                                    <input type="file" class="form-control"   name="truck_horse_copy"  placeholder="Enter Tricking Number " >
                                                                </div>
                                                                <div class="col-md-6 pull-left">
                                                                    <label>Truck Trailer File</label>
                                                                    <input type="file" class="form-control"   name="truck_trailer_copy"  placeholder="Enter Tricking Number " >
                                                                </div>
                                                                <div class="col-md-6 pull-left">
                                                                    <label>Date of Loading</label>
                                                                    <input type="date" class="form-control"  value="{{$value->loading_date}}"  name="loading_date"  placeholder="Enter Tricking Number " required>
                                                                </div>
                                                                <div class="col-md-12 pt-5 pb-3">
                                                                    <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Close</button>
                                                                    <button type="submit" class="btn btn-primary waves-effect waves-light">Save changes</button>
                                                                </div>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div><!-- /.modal-content -->
                                        </div><!-- /.modal-dialog -->
                                    </div><!-- /.modal -->

                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <span class="float-right ">
                        <?php echo $transporter->appends(Request::all())->fragment('foo')->render(); ?>

                    </span>

                </div>
            </div>
        </div> <!-- end col -->
    </div> <!-- end row -->

</div> <!-- end container -->
</div>
<!-- end wrapper -->

<!-- Footer -->
@include('layouts.footer')
<!-- End Footer -->


@include('layouts.javas')
<script type="text/javascript">
    $(document).ready(function() {
        $('select[name="ladding_id"]').on('change', function() {
            var stateID = $(this).val();
            if(stateID) {
                $.ajax({
                    url: '/add_status/ajax/'+stateID,
                    type: "GET",
                    dataType: "json",
                    success:function(data) {


                        $('select[name="container_id"]').empty();
                        $.each(data, function(key, value) {
                            $('select[name="container_id"]').append('<option value="'+ key +'">'+ value +'</option>');
                        });


                    }
                });
            }else{
                $('select[name="container_id"]').empty();
            }
        });
    });
</script>


</body>
</html>
<!-- Localized -->