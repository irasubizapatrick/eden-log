

<!DOCTYPE html>
<html lang="en">
@include('layouts.page_head')
<body>

@include('layouts.page_header')

<section class="banner_area">
    <div class="container">
        <div class="pull-left">
            <h3>Contact Us</h3>
        </div>
        <div class="pull-right">
            <a href="/">Home</a>
            <a href="/contact_us">Contact</a>
        </div>
    </div>
</section>


<section class="contact_details_area">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-xs-6">
                <h3 class="contact_title">Rwanda</h3>
                <div class="media">
                    <div class="media-left">
                        <i class="fa fa-map-marker"></i>
                    </div>
                    <div class="media-body">
                        <p> Magerwa </p>
                        <p>Gikondo Nyarugenge  </p>
                    </div>
                </div>
                <div class="media mt-0">
                    <div class="media-left">
                        <i class="fa fa-envelope-o"></i>
                    </div>
                    <div class="media-body">
                        <p><a href="../../cdn-cgi/l/email-protection/index.html" data-cfemail="47222a262e2b07223f262a372b226924282a">info@mikumifreight.com</a></p>
                    </div>
                </div>
                <div class="media mt-0">
                    <div class="media-left">
                        <i class="fa fa-phone"></i>
                    </div>
                    <div class="media-body">
                        <p>+255 714 307 315</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-xs-6">
                <h3 class="contact_title">Tanzania</h3>
                <div class="media">
                    <div class="media-left">
                        <i class="fa fa-map-marker"></i>
                    </div>
                    <div class="media-body">
                        <p>Harbour View Tower 8th Floor , room 804, </p>
                        <p>Samora Avenue, Dar es salaam, Tanzania</p>
                    </div>
                </div>
                <div class="media mt-0">
                    <div class="media-left">
                        <i class="fa fa-envelope-o"></i>
                    </div>
                    <div class="media-body">
                        <p><a href="../../cdn-cgi/l/email-protection/index.html"  data-cfemail="791c14181015391c01181409151c571a1614">info@mikumifreight.com</a></p>
                    </div>
                </div>
                <div class="media mt-0">
                    <div class="media-left">
                        <i class="fa fa-phone"></i>
                    </div>
                    <div class="media-body">
                        <p>+255 714 307 315</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-xs-6">
                <h3 class="contact_title">China</h3>
                <div class="media">
                    <div class="media-left">
                        <i class="fa fa-map-marker"></i>
                    </div>
                    <div class="media-body">
                        <p>B - 562, Mallin Street </p>
                        <p>China</p>
                    </div>
                </div>
                <div class="media mt-0">
                    <div class="media-left">
                        <i class="fa fa-envelope-o"></i>
                    </div>
                    <div class="media-body">
                        <p><a href="../../cdn-cgi/l/email-protection/index.html" data-cfemail="2c49414d45406c49544d415c4049024f4341">info@mikumifreight.com</a></p>
                    </div>
                </div>
                <div class="media mt-0">
                    <div class="media-left">
                        <i class="fa fa-phone"></i>
                    </div>
                    <div class="media-body">
                        <p>+ 86 15 546 8975</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<section class="contact_form_area">
    <div class="container">
        <div class="main_title">
            <h5>GET IN TOUCH</h5>
            <h2>Contact Us</h2>
        </div>
        <div class="row contact_form_inner">
            <h3 class="c_inner_title">Send us a message</h3>
            <form class="contact_us_form" action="#" method="post" id="phpcontactform">
                <div class="form-group col-md-6">
                    <input type="text" class="form-control" id="name" name="name" placeholder="Your Name">
                </div>
                <div class="form-group col-md-6">
                    <input type="email" class="form-control" id="email" name="email" placeholder="Email">
                </div>
                <div class="form-group col-md-12">
                    <input type="text" class="form-control" id="subject" name="subject" placeholder="Subject">
                </div>
                <div class="form-group col-md-12">
                    <textarea class="form-control" id="message" name="message" placeholder="Message" rows="1"></textarea>
                </div>
                <div class="form-group col-md-12 button_area">
                    <button type="submit" value="submit your quote" class="btn submit_blue form-control" id="js-contact-btn">Send message <i class="fa fa-angle-right"></i></button>
                    <div id="js-contact-result" data-success-msg="Success, We will get back to you soon" data-error-msg="Oops! Something went wrong"></div>
                </div>
            </form>
        </div>
    </div>
</section>

@include('layouts.web_footer')

@include('layouts.page_footer')
</body>
</html>
<!-- Localized -->