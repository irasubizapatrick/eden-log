<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    @include('layouts.title')
    <meta content="Admin Dashboard" name="description" />
    <meta content="Logistics" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <!-- App Icons -->
    <link rel="shortcut icon" href="/assets/images/favicon.ico">

    <!-- App css -->
    <link href="/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="/assets/css/icons.css" rel="stylesheet" type="text/css" />
    <link href="/assets/css/style.css" rel="stylesheet" type="text/css" />

    <style type="text/css">
        .logo{
            width: 100%;
            height:100%;
        }
    </style>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-142579219-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-142579219-1');
    </script>

</head>


<body>

<!-- Begin page -->
<div class="accountbg"></div>
<div class="wrapper-page">

    <div class="card">
        <div class="card-body">
            <h3 class="text-center mt-0 m-b-15">
                <a href="/" class="logo logo-admin "><img src="assets/images/logo.jpeg" height="150"  alt="logo"></a>
            </h3>
            <h4 class="text-muted text-center font-18"><b>Result</b></h4>

            <div class="row">
                <div class="col-12">
                    <div class="card m-b-30">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <a role="button" class="btn btn-danger pull-right "  href="{{URL::previous()}}" style="margin-left: 1%;margin-top:  5%;"> Get back</a>

                                    <section id="cd-timeline" class="cd-container">
                                        @forelse($track as $value)
                                            <div class="cd-timeline-block">
                                                <div class="cd-timeline-img cd-success">
                                                    <i class="mdi mdi-adjust"></i>
                                                </div> <!-- cd-timeline-img -->

                                                <div class="cd-timeline-content">
                                                    <h3>{{$value->status}}</h3>
                                                    <p class="mb-0 text-muted font-14">{{$value->comment}}</p>
                                                    <span class="cd-date">{{$value->bill_loading}}</span>
                                                    <span class="cd-date">{{$value->updated_at}}</span>
                                                </div> <!-- cd-timeline-content -->
                                            </div> <!-- cd-timeline-block -->
                                        @empty
                                            <div class="alert alert-success" style="width: 96%;">
                                                <strong>Sorry !</strong> No Information Found   .
                                            </div>
                                        @endforelse


                                    </section> <!-- cd-timeline -->
                                </div>
                            </div><!-- Row -->

                        </div>
                    </div>
                </div> <!-- end col -->
            </div> <!-- end row -->


        </div>
    </div>
</div>

<!-- jQuery  -->
<script src="/assets/js/jquery.min.js"></script>
<script src="/assets/js/popper.min.js"></script>
<script src="/assets/js/bootstrap.min.js"></script>
<script src="/assets/js/modernizr.min.js"></script>
<script src="/assets/js/waves.js"></script>
<script src="/assets/js/jquery.slimscroll.js"></script>
<script src="/assets/js/jquery.nicescroll.js"></script>
<script src="/assets/js/jquery.scrollTo.min.js"></script>

<!-- App js -->
<script src="/assets/js/app.js"></script>

</body>
</html>
<!-- Localized -->
