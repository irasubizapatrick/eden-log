<!DOCTYPE html>
<html>

@include('layouts.admin_head')
<style>
    .size{
        height: calc(3.25rem + -10px) !Important;
    }
    .select2{
        width: 100% !important;
        margin-top: -3rem;
    }
    .form_size{
        height: calc(3.25rem + 3px) !Important;
    }
    .dt-buttons{
        margin-left: -2% !important;
    }
</style>
<body>
@include('layouts.agent_sidebar')
<div class="wrapper">
    <div class="container-fluid">
        <div class="modal fade bill_lading"  role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title mt-0" id="myLargeModalLabel">Search by Bill</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body">
                        <form class="form-horizontal"  action="/search/bill/details/im8" method="POST" enctype="multipart/form-data">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                            <div class="row col-md-12">
                                <div class="col-md-12">
                                    <label>Bill of lading</label>
                                    <select class="form-control form_size " name="ladding_id"  id="ladding_id" required>
                                        <option value="">Select Bill of lading</option>
                                        @foreach($ladding as $key => $value)
                                            <option value="{{ $key }}">{{$value}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-12 pt-5 pb-5">
                                    <button type="submit" class="btn btn-primary waves-effect waves-light">Search </button>
                                    <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row m-0 p-0">
        <div class="col-sm-12">
            <div class="page-title-box">
                <div class="btn-group pull-right">
                    <ol class="breadcrumb hide-phone p-0 m-0">
                        <li class="breadcrumb-item"><a href="#"> List</a></li>
                        <li class="breadcrumb-item active">Dashboard</li>
                    </ol>
                </div>
                <h4 class="page-title">All IM8 Documents </h4>
            </div>
        </div>
        <div class="col-12 container" style="margin-top: -3rem;">
            <div class="card m-b-30">
                <div class="card-body">
                    <a href="/declaration/im8" role="button" class="btn btn-success sm waves-effect waves-light pull-right mx-2 button_search"><i class="fa fa-home"></i> <span> All</span></a>

                    <button type="button" class="btn btn-warning waves-effect waves-light pull-right mx-4" data-toggle="modal" data-target=".bill_lading "><i class="fa fa-search"></i><span>Search </span></button>
                    @if (Session::has('message'))
                        <div class="alert alert-success alert-dismissable">
                            <a href="" class="close" data-dismiss="alert" aria-label="close">
                                &times;

                            </a>
                            {{ Session::get('message') }}
                        </div>

                    @endif
                    @if (Session::has('delete'))
                        <div class="alert alert-danger alert-dismissable">
                            <a href="" class="close" data-dismiss="alert" aria-label="close">
                                &times;

                            </a>
                            {{ Session::get('delete') }}
                        </div>

                    @endif
                    @if (Session::has('updated'))
                        <div class="alert alert-success alert-dismissable">
                            <a href="" class="close" data-dismiss="alert" aria-label="close">
                                &times;

                            </a>
                            {{ Session::get('updated') }}
                        </div>

                    @endif

                    <table id="datatable-buttons" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                        <tr style="font-size: 13px;background-color: #416b8d !important;color: #fff;">
                            <th>#</th>
                            <th>Bill of lading</th>
                            <th>Manifest</th>
                            <th>Authorization   </th>
                            <th>Movement Sheet</th>
                            <th>Assessment </th>
                            <th>Release</th>
                            <th>Actions</th>
                        </tr>
                        </thead>


                        <tbody>
                        <?php $i=1;?>
                        @foreach($declaration AS $value)
                            <tr>
                                <td>{{$i++}}</td>
                                <td>{{$value->bill_lading}}</td>
                                <td>{{$value->manifest}}</td>
                                <td>
                                    @if($value->authorization !="")

                                        <a href="/Declaration/{{$value->authorization}}"  target="_blank">file</a>
                                    @else
                                        <a href="#"  style="color: red;">no file</a>
                                    @endif

                                </td>
                                <td>

                                    @if($value->movement_sheet !="")

                                        <a href="/Declaration/{{$value->movement_sheet}}"  target="_blank">file</a>
                                    @else
                                        <a href="#" style="color: red;">no file</a>
                                    @endif
                                </td>

                                <td>

                                    @if($value->assessment_doc !="")

                                        <a href="/Declaration/{{$value->assessment_doc}}"  target="_blank">file</a>
                                    @else
                                        <a href="#"  style="color: red;">no file</a>
                                    @endif

                                </td>
                                <td>

                                    @if($value->release !="")

                                        <a href="/Declaration/{{$value->release}}"  target="_blank">file</a>
                                    @else
                                        <a href="#"  style="color: red;">no file</a>
                                    @endif

                                </td>
                                <td>
                                    <button data-toggle="modal" data-target="#editleads<?php echo $i;?>" class="pull-left edit btn-lg  btn-primary dlt_sm_table mx-2 mt-2"><i class="fa fa-pencil"></i></button>


                                    <div id="editleads<?php echo $i;?>" class="modal fade" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
                                        <div class="modal-dialog modal-lg">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h3 class="modal-title" id="editleads"> Edit Declaration </h3>
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

                                                </div>
                                                <div class="modal-body">
                                                    <div class="row p-5">
                                                        <form role="form-horizontal" action="/declaration/im8/{{$value->id}}" method="post" enctype="multipart/form-data">
                                                            <input type="hidden" name="_method" value="PUT" />
                                                            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                                            <div class="row col-md-12">
                                                                <div class="col-md-6" >
                                                                    <label> Bill of lading</label>
                                                                    <input type="hidden" class="form-control" name="manifest" value="yes">
                                                                    <select class="form-control form_size" name="ladding_id">
                                                                        <option value="{{$value->ladding_id}}"> {{$value->bill_lading}}</option>
                                                                    </select>
                                                                </div>
                                                                <div class="col-md-6" >
                                                                    <label> Authorization </label>
                                                                    <input type="file"  name="authorization" class="form-control" >
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <label>Movement Sheet</label>
                                                                    <input type="file"  name="movement_sheet" class="form-control">
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <label>Release</label>
                                                                    <input type="file"  name="release" class="form-control">
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <label>Assessment</label>
                                                                    <input type="file"  name="assessment_doc" class="form-control">
                                                                </div>
                                                                <div class="col-md-12 mx-auto mt-5 pb-5">
                                                                    <center>
                                                                        <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Close</button>
                                                                        <button type="submit" class="btn btn-primary waves-effect waves-light">Save changes</button>
                                                                    </center>
                                                                </div>

                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </td>

                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>

</div>
</div>
@include('layouts.footer')

@include('layouts.javas')
{{--<script>--}}
    {{--$(document).ready(function(){--}}
        {{--$('.form_size').on('change', function(e) {--}}
            {{--e.preventDefault();--}}
            {{--if (document.getElementById('manifest').value == 'Yes') {--}}
                {{--document.getElementById('doc1').style.display = 'block';--}}
                {{--document.getElementById('doc2').style.display = 'block';--}}
                {{--document.getElementById('doc3').style.display = 'block';--}}
                {{--document.getElementById('doc4').style.display = 'block';--}}
                {{--document.getElementById('doc5').style.display = 'block';--}}

            {{--}--}}


            {{--else {--}}
                {{--document.getElementById('doc1').style.display = 'none';--}}
                {{--document.getElementById('doc2').style.display = 'none';--}}
                {{--document.getElementById('doc3').style.display = 'none';--}}
                {{--document.getElementById('doc4').style.display = 'none';--}}
                {{--document.getElementById('doc5').style.display = 'none';--}}
            {{--}--}}
        {{--});--}}
    {{--});--}}
{{--</script>--}}
</body>
</html>