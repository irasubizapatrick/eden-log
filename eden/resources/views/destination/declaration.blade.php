<!DOCTYPE html>
<html>

@include('layouts.admin_modal')
<style>
    .size{

        width: 360px;

    }
    .has-submenu{
        padding: 0px 10px 0px 10px;
    }
</style>
<body>

@include('layouts.declaration_sidebar')
<div class="wrapper">
    <div class="container-fluid">
        <div class="modal fade bs-example-modal-lg" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h3 class="modal-title mt-0" id="myLargeModalLabel">Add Destination </h3>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body p-5">
                        <form class="form-horizontal"  action="/manage/destination" method="POST" enctype="multipart/form-data">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Country</label>
                                    <select class="form-control size country"  name="country_id" required>
                                        <option value=""> Choose </option>
                                        @foreach($country as $cat)
                                            <option value="{{$cat->id}}"> {{$cat->country_name}}</option>
                                        @endforeach
                                    </select>

                                </div>
                                <div class="col-md-6">
                                    <label>Destination Name</label>
                                    <input type="text" class="form-control" name="destination_name" placeholder="Enter  destination name " required>
                                </div>
                                <div class="col-md-12 mx-auto pb-5 pt-3">
                                    <div class="modal-footer ">
                                        <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Close</button>
                                        <button type="submit" class="btn btn-primary waves-effect waves-light">Save changes</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row m-0 p-2">
        <div class="col-sm-12">
            <div class="page-title-box" style="padding: 60px 0px 46px 0px !important">
                <div class="btn-group pull-right">
                    <ol class="breadcrumb hide-phone p-0 m-0">
                        <li class="breadcrumb-item"><a href="#">All Conversation </a></li>
                        <li class="breadcrumb-item active">Dashboard</li>
                    </ol>
                </div>
                <h4 class="page-title">All Destinations</h4>
            </div>
        </div>
        <div class="col-12 container" style="margin-top: -3rem;">
            <div class="card m-b-30">
                <div class="card-body">
                    <button type="button" class="btn btn-primary waves-effect waves-light pull-right" data-toggle="modal" data-target=".bs-example-modal-lg "><i class="fa fa-plus"></i> <span>Add </span></button>
                    @if (Session::has('message'))
                        <div class="alert alert-success alert-dismissable">
                            <a href="" class="close" data-dismiss="alert" aria-label="close">
                                &times;

                            </a>
                            {{ Session::get('message') }}
                        </div>

                    @endif
                    @if (Session::has('delete'))
                        <div class="alert alert-danger alert-dismissable">
                            <a href="" class="close" data-dismiss="alert" aria-label="close">
                                &times;

                            </a>
                            {{ Session::get('delete') }}
                        </div>

                    @endif
                    @if (Session::has('updated'))
                        <div class="alert alert-updated alert-dismissable">
                            <a href="" class="close" data-dismiss="alert" aria-label="close">
                                &times;

                            </a>
                            {{ Session::get('updated') }}
                        </div>

                    @endif

                    <table id="datatable-buttons" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                        <tr style="font-size: 13px;background-color: #416b8d !important;color: #fff;">
                            <th>#</th>
                            <th>Country</th>
                            <th>Destination</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $i=1;?>
                        @foreach($destination AS $value)
                            <tr>
                                <td>{{$i++}}</td>
                                <td>{{$value->country->country_name}}</td>
                                <td>{{$value->destination_name}}</td>
                                <td>
                                    <button data-toggle="modal" data-target="#editleads<?php echo $i;?>" class="pull-left edit btn-lg  btn-primary dlt_sm_table mx-2 mt-2"><i class="fa fa-pencil"></i></button>
                                    <button data-toggle="modal" data-target="#delete<?php echo $i;?>" class="pull-left edit  btn-lg   btn-danger dlt_sm_table mx-2 mt-2"><i class="fa fa-trash"></i></button>

                                    <div class="modal fade" id="delete<?php echo $i;?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLarge01" style="display: none;" aria-hidden="true">
                                        <div class="modal-dialog modal-lg" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title">Delete </h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">×</span>
                                                    </button>
                                                </div>
                                                <form class="pull-left" action="/manage/destination/{{$value->id}}" method="POST">
                                                    <label class="mx-4">Are you sure you want to delete</label>
                                                    <input type="hidden" name="_method" value="DELETE" />
                                                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                                    <div class="modal-footer">
                                                        <button type="submit" class="btn btn-success">Confirm</button>
                                                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="editleads<?php echo $i;?>" class="modal fade"  role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
                                        <div class="modal-dialog modal-lg">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h3 class="modal-title" id="editleads"> Edit Destination </h3>
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

                                                </div>
                                                <div class="modal-body p-5">
                                                    <div class="row">
                                                        <form role="form" action="/manage/destination/{{$value->id}}" method="post" enctype="multipart/form-data">
                                                            <input type="hidden" name="_method" value="PUT" />
                                                            <input type="hidden" name="_token" value="{{ csrf_token() }}" />

                                                            <div class="col-md-6">
                                                                <label>Country</label>
                                                                <select class="form-control size country"  name="country_id" >
                                                                    <option value="{{$value->country_id}}"> {{$value->country->country_name}} </option>
                                                                    @foreach($country as $cat)
                                                                        <option value="{{$cat->id}}"> {{$cat->country_name}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <label>Destination </label>
                                                                <input type="text" class="form-control"  value="{{$value->destination_name}}" name="destination_name" placeholder="Enter  destination name " required>
                                                            </div>
                                                            <div class="col-md-12 pt-5">
                                                                <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Close</button>
                                                                <button type="submit" class="btn btn-primary waves-effect waves-light">Save changes</button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>
</div>
</div>
@include('layouts.footer')


@include('layouts.javas')
</body>
</html>