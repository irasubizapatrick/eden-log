<!DOCTYPE html>
<html>
@include('layouts.finance_head')
<style>
    .form_size{
        height: calc(3.25rem + 2px) !Important;
    }
    .dt-buttons{
        margin-left: 0% !important;
    }
    .size{
        width: 350px;
    }

    .bl a:hover{
        text-decoration: underline;
        color: #376b8f !important;
    }
</style>
<body>

@include('layouts.shipping_sidebar')
<!-- End Navigation Bar-->
<div class="wrapper">
    <div class="row mx-auto">
        <div class="col-md-12 ">
            <div class="page-title-box mx-auto" >
                <div class="btn-group pull-right">
                    <ol class="breadcrumb hide-phone">
                        <li class="breadcrumb-item"><a href="#">Consignments List</a></li>
                        <li class="breadcrumb-item active">Dashboard</li>
                    </ol>
                </div>
                <h4 class="page-title ">All Consignments</h4>
            </div>
        </div>
        <div class="modal fade transporter" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title mt-0 mx-4" id="myLargeModalLabel">Search all Orders </h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body">
                        <form class="" role="form" action="/all_order/search/border" method="POST" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="row container mx-0 p-3">
                                <div class="col-md-6">
                                    <label>Consignee</label>
                                    <select class="form-control size" id="containerList" name="agent_id" >
                                        <option value=""> Choose</option>
                                        @foreach($agent as $all)
                                            <option value="{{$all->id}}"> {{$all->organization_name}}</option>
                                        @endforeach

                                    </select>
                                </div>
                                <div class="col-md-6">
                                    <label>Current Status</label>
                                    <select class="form-control form_size" name="current_status_one"   >
                                        <option value=""> ---  Choose ---</option>
                                        <option value="Receiving the documents"> Receiving the documents </option>
                                        <option value="Under shipping line Procedures"> Under shipping line Procedures </option>
                                        <option value="Under Customs Procedures"> Under Customs Procedures </option>
                                        <option value="Release Order, Delivery Order Received"> Release Order, Delivery Order Received </option>
                                        <option value="Prepare for T1"> Prepare for T1 </option>
                                        <option value="Lodge Port charges"> Lodge Port charges </option>
                                        <option value="Pay Port Charges"> Pay Port Charges </option>
                                        <option value="Loading"> Loading </option>
                                        <option value="Truck/ Cargo in route Position"> Truck/ Cargo in route Position </option>
                                        <option value="Crossed Border"> Crossed Border </option>
                                        <option value="Other"> Other</option>
                                        <option value="Cargo not crossed the Boarder">  Cargo not crossed the Boarder </option>
                                    </select>
                                </div>
                                <div class="col-md-6">
                                    <label>From</label>
                                    <input type="date" name="date1"  class="form-control">
                                </div>
                                <div class="col-md-6">
                                    <label>To</label>
                                    <input type="date" name="date2"  class="form-control">
                                </div>
                                <div class="col-md-12 pt-5  pb-3" >
                                    <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Close</button>
                                    <button type="submit" class="btn btn-primary waves-effect waves-light">Search</button>
                                </div>

                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xl-12" style="margin-top: -3rem;">

            <div class="card ">
                <div class="card-body col-xl-12">
                    <div class="table-responsive col-xl-12">
                        @if (Session::has('message'))
                            <div class="alert alert-success alert-dismissable">
                                <a href="" class="close" data-dismiss="alert" aria-label="close">
                                    &times;

                                </a>
                                {{ Session::get('message') }}
                            </div>

                        @endif
                        @if (Session::has('delete'))
                            <div class="alert alert-success alert-dismissable">
                                <a href="" class="close" data-dismiss="alert" aria-label="close">
                                    &times;

                                </a>
                                {{ Session::get('delete') }}
                            </div>

                        @endif
                        @if (Session::has('updated'))
                            <div class="alert alert-success alert-dismissable">
                                <a href="" class="close" data-dismiss="alert" aria-label="close">
                                    &times;

                                </a>
                                {{ Session::get('updated') }}
                            </div>

                        @endif
                            <a href="/all_order" role="button" class="btn btn-success sm waves-effect waves-light pull-right mx-2 button_search"><i class="fa fa-home"></i> <span> All Orders</span></a>

                            <button type="button" class="btn btn-primary waves-effect waves-light pull-right mx-2 button_search" data-toggle="modal" data-target=".transporter "><i class="fa fa-search"></i> <span> Search</span></button>

                        <table id="datatable-buttons" class="table table-hover table-striped table-bordered" cellspacing="0" width="100%" >
                            <thead>
                            <tr style="font-size: 13px;background-color: #416b8d !important;color: #fff;">
                                <th>#</th>
                                <th>Date</th>
                                <th>File No</th>
                                <th>Consignee</th>
                                <th>BL</th>
                                <th>Vessel </th>
                                <th> C&F / Agent </th>
                                <th>Shipping Line</th>
                                <th>Shipper</th>
                                <th>Destination</th>
                                <th>Current Status</th>
                                <th>Comment</th>
                                <th>Actions</th>

                            </tr>
                            </thead>
                            <tbody>
                            <?php $i=1;?>
                            @foreach($track AS $value)
                                <tr style="font-size: 14px;">
                                    <td>{{$i++}}</td>
                                    <td>{{$value->date}} </td>
                                    <td>{{$value->file_no}} </td>
                                    <td>{{$value->client_name}} </td>
                                    <td class="bl"> <a href="{{route ('/view/all/container', ['id' =>$value->id])}}"  style="color: black;">{{$value->bill_lading}} </a> </td>
                                    <td>{{$value->line_vessel}}</td>
                                    <td>{{$value->organization_name}}</td>
                                    <td>{{$value->shipping_line}}</td>
                                    <td>{{$value->shipper}}</td>
                                    <th>{{$value->destination}}</th>
                                    <td>

                                        @if($value->current_status == 'Prepare for T1')

                                            <span class="label label-warning"> {{$value->current_status}}</span>
                                        @elseif($value->current_status == 'Crossed Border')

                                            <span class="label label-success"> {{$value->current_status}}</span>


                                        @elseif($value->current_status == 'Receiving the documents')

                                            <span class="label label-primary"> {{$value->current_status}} </span>

                                        @elseif($value->current_status == 'Under Customs Procedures')

                                            <span class="label label-warning"> {{$value->current_status}} </span>
                                        @else
                                            <span class="label label-info"> {{$value->current_status}} </span>
                                        @endif

                                    </td>
                                    <td>
                                        {{$value->other_comment}}
                                    </td>
                                    <td>
                                        <a href="{{route ('edit/order', ['id' =>$value->id])}}" class="pull-left edit  btn-lg  btn btn-primary dlt_sm_table mx-2 mt-2"><i class="fa fa-pencil"></i> </a>
                                        <a href="{{route ('/view/all/container', ['id' =>$value->id])}}" class="pull-left edit  btn-lg  btn btn-success dlt_sm_table mx-2 mt-2"><i class="fa fa-eye"></i> </a>


                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>

    </div>
</div>
@include('layouts.footer')
@include('layouts.javas')
</body>
</html>