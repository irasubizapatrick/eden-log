<!DOCTYPE html>
<html>
@include('layouts.admin_head')
<body>

<!-- Navigation Bar-->
@include('layouts.admin_sidebar')
<!-- End Navigation Bar-->
<div class="wrapper">
    <div class="row mx-auto">
        <div class="col-md-12 ">
            <div class="page-title-box mx-auto" >
                <div class="btn-group pull-right">
                    <ol class="breadcrumb hide-phone">
                        <li class="breadcrumb-item"><a href="#">Orders List</a></li>
                        <li class="breadcrumb-item active">Dashboard</li>
                    </ol>
                </div>
                <h4 class="page-title ">All Orders</h4>
            </div>
        </div>
        <div class="col-xl-12" style="margin-top: -3rem;">
            <div class="card ">
                <div class="card-body col-xl-12">
                    <div class="table-responsive col-xl-12">
                        @if (Session::has('message'))
                            <div class="alert alert-success alert-dismissable">
                                <a href="" class="close" data-dismiss="alert" aria-label="close">
                                    &times;

                                </a>
                                {{ Session::get('message') }}
                            </div>

                        @endif
                        @if (Session::has('delete'))
                            <div class="alert alert-success alert-dismissable">
                                <a href="" class="close" data-dismiss="alert" aria-label="close">
                                    &times;

                                </a>
                                {{ Session::get('delete') }}
                            </div>

                        @endif
                        @if (Session::has('updated'))
                            <div class="alert alert-success alert-dismissable">
                                <a href="" class="close" data-dismiss="alert" aria-label="close">
                                    &times;

                                </a>
                                {{ Session::get('updated') }}
                            </div>

                        @endif

                        <table id="datatable-buttons" class="table table-hover table-striped table-bordered" cellspacing="0" width="100%" >
                            <thead>
                            <tr style="font-size: 13px;background-color: #416b8d !important;color: #fff;">
                                <th>#</th>
                                <th>Date</th>
                                <th>File No</th>
                                <th>Consignee</th>
                                <th>Bill of lading</th>
                                <th> Line/ Vessel </th>
                                <th> C&F / Agent </th>
                                <th>Shipping Line</th>
                                <th>Shipper</th>
                                <th>Invoice</th>
                                <th>Status</th>
                                <th>Actions</th>

                            </tr>
                            </thead>
                            <tbody>
                            <?php $i=1;?>
                            @foreach($track AS $value)
                                <tr style="font-size: 14px;">
                                    <td>{{$i++}}</td>
                                    <td>{{$value->date}} </td>
                                    <td>{{$value->file_no}} </td>
                                    <td>{{$value->client_name}} </td>
                                    <td>{{$value->bill_lading}} </td>
                                    <td>{{$value->line_vessel}}</td>
                                    <td>{{$value->organization_name}}</td>
                                    <td>{{$value->shipping_line}}</td>
                                    <td>{{$value->shipper}}</td>
                                    <td>
                                        @if($value != '')
                                            <a href="#"  target="_blank">no file</a>
                                        @else
                                            <a href="/tax_clearance/{{$value->invoice_order}}"  target="_blank"> file</a>
                                        @endif
                                    </td>
                                    <td>

                                        @if($value->track_status == 'Receiving the documents')

                                            <button class="btn btn-warning btn-lg" style="height: 27px;width: 41px;"> </button>

                                        @elseif($value->track_status == 'Release Order, Delivery Order Received')

                                            <button class="btn btn-success btn-lg" style="height: 27px;width: 41px;"></button>
                                        @elseif($value->track_status == 'Loading')

                                            <button class="btn btn-success btn-lg" style="height: 27px;width: 41px;"></button>

                                        @else
                                            <button class="btn btn-info btn-lg" style="height: 27px;width: 41px;"></button>

                                        @endif
                                    </td>
                                    <td>
                                        <a href="{{route ('edit/order', ['id' =>$value->id])}}" class="pull-left edit  btn-lg  btn btn-primary dlt_sm_table mx-2 mt-2"><i class="fa fa-pencil"></i> </a>

                                        <button data-toggle="modal" data-target="#delete<?php echo $i;?>" class="pull-left edit  btn-lg  btn btn-danger dlt_sm_table mx-2 mt-2""><i class="fa fa-trash"></i></button>
                                        <div class="modal fade" id="delete<?php echo $i;?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLarge01" style="display: none;" aria-hidden="true">
                                            <div class="modal-dialog modal-lg" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title">Delete </h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">×</span>
                                                        </button>
                                                    </div>
                                                    <form class="pull-left" action="/my_orders/{{$value->id}}" method="POST">
                                                        <label class="mx-4">Are you sure you want to delete</label>
                                                        <input type="hidden" name="_method" value="DELETE" />
                                                        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                                        <div class="modal-footer">
                                                            <button type="submit" class="btn btn-success">Confirm</button>
                                                            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>

    </div>
</div>
<!-- end wrapper -->


<!-- Footer -->
@include('layouts.footer')
<!-- End Footer -->


@include('layouts.javas');
</body>
</html>
<!-- Localized -->