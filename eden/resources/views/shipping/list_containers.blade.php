
<!DOCTYPE html>
<html>
@include('layouts.admin_view_order')

<style type="text/css">
    .has-submenu{
        padding: 0px 10px 0px 30px !important;
        font-weight: 700 !important;
    }
    .dropdown-menu {
        padding: 7px 50px;
        font-size: 15px;
        box-shadow: 0 2px 31px rgba(0, 0, 0, 0.08);
        border-color: #eff3f6;
        margin-top: 17px;
        min-width: 21rem;
        font-weight: 400;
    }
</style>

<body class="fixed-left">
<div id="wrapper">
    @include('layouts.shipping_sidebar')

    <div class="content-page">
        <div class="content">

            <div class="page-content-wrapper ">
                <div class="container-fluid">
                    @foreach($track as $value)
                        <div class="row pt-3 pb-5" style="padding-top: 5rem !Important;">
                            <div class="col-lg-12">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="col-lg-12">
                                            <h4 class="font-16"><strong> File Number:  {{$value->file_no}}</strong></h4>
                                            <h2 class="mt-0 header-title " >Document Received: {{$value->document_date_received}}</h2>
                                            <h4 class="mt-0 header-title">Bill of lading : <span style="font-size: small">{{$value->bill_lading}}</span> </h4>
                                            <h4 class="mt-0 header-title">Current Status : <span style="font-size: small" class="badge badge-primary">{{$value->current_status}}</span> </h4>

                                        </div>
                                        <div class="col-lg-12">
                                            <img src="/assets/images/slogo.jpeg" class="rounded-circle img-circle  float-right" alt="..." style="margin-top: -8rem; width: 16%;">
                                        </div>

                                        <div class="col-lg-12">
                                            <ul class="nav nav-tabs" role="tablist">
                                                <li class="nav-item">
                                                    <a class="nav-link active" data-toggle="tab" href="#home" role="tab" style="color: black;"><strong>Consignee</strong></a>
                                                </li>

                                                <li class="nav-item">
                                                    <a class="nav-link tabs" data-toggle="tab" href="#eta" role="tab"> <strong>ETA</strong></a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link tabs" data-toggle="tab" href="#ata" role="tab"><strong> ATA</strong></a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link tabs" data-toggle="tab" href="#shipping_line" role="tab"><strong>Shipping Line </strong></a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link tabs" data-toggle="tab" href="#shipper" role="tab"><strong>Shipper</strong></a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link tabs" data-toggle="tab" href="#vessel" role="tab"><strong>Vessel Name</strong></a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link tabs" data-toggle="tab" href="#documents" role="tab"><strong>Documents</strong></a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link tabs" data-toggle="tab" href="#profile" role="tab"><strong>Others</strong></a>
                                                </li>

                                            </ul>
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="tab-content">
                                                <div class="tab-pane active p-3" id="home" role="tabpanel">
                                                    <p class="font-14 mb-0">
                                                        <strong>Client / Consignee :  </strong> {{$value->client_name}} <br>
                                                    </p>
                                                </div>

                                                <div class="tab-pane p-3" id="eta" role="tabpanel">
                                                    <p class="font-14 mb-0">
                                                        <strong>ETA : </strong>{{$value->eta}}  <br>
                                                    </p>
                                                </div>
                                                <div class="tab-pane p-3" id="ata" role="tabpanel">
                                                    <p class="font-14 mb-0">
                                                        <strong>ATA : </strong>{{$value->ata}}  <br>
                                                    </p>
                                                </div>
                                                <div class="tab-pane p-3" id="shipping_line" role="tabpanel">
                                                    <p class="font-14 mb-0">
                                                        <strong>Shipping Line: </strong>{{$value->shipping_line}}  <br>
                                                        <strong>Shipping Address: </strong>{{$value->shipper_address}}  <br>
                                                    </p>
                                                </div>
                                                <div class="tab-pane p-3" id="vessel" role="tabpanel">
                                                    <p class="font-14 mb-0">
                                                        <strong>Vessel Name: </strong>{{$value->line_vessel}}  <br>
                                                    </p>
                                                </div>
                                                <div class="tab-pane p-3" id="shipper" role="tabpanel">
                                                    <p class="font-14 mb-0">
                                                        <strong>Shipper : </strong>{{$value->shipper}}  <br>
                                                        <strong>Address : </strong>{{$value->shipper_address}}  <br>
                                                    </p>
                                                </div>
                                                <div class="tab-pane p-3" id="declaration" role="tabpanel">
                                                    <p class="font-14 mb-0">
                                                        <strong >Declaration Status: </strong> {{$value->declaration}} <br>
                                                        <strong >Amount : </strong> {{$value->amount}} <br>
                                                    </p>
                                                </div>

                                                <div class="tab-pane p-3" id="profile" role="tabpanel">
                                                    <p class="font-14 mb-0">
                                                        <strong>Commodity : </strong> {{$value->commodity}} <br>
                                                        <strong>Cargo Description : </strong> {{$value->cargo_description}} <br>
                                                        <strong>Weight as per  BL : </strong>{{$value->weight}}  <br>
                                                        <strong>Port Charges :</strong>{{$value->port_charge}}  <br>
                                                        <strong>Weight  : </strong>{{$value->weight}} {{$value->frequency}}  <br>
                                                        <strong>Voyage Number  : </strong>{{$value->voyager_number}}  <br>
                                                        <strong>Guart Amount Taken  : </strong>{{number_format($value->guart_amount_taken)}}  <br>
                                                        <strong>Voyage Guart Amount Returned  : </strong>{{number_format($value->guart_amount_returned)}}  TSH <br>
                                                    </p>
                                                </div>
                                                <div class="tab-pane p-3" id="documents" role="tabpanel">
                                                    <p class="font-14 mb-0">
                                                        <strong>Delivery Order :</strong>
                                                        @if($value->delivery_order != '')
                                                            <a href="/DeliveryOrder/{{$value->delivery_order}}"  target="_blank">file</a>

                                                        @else

                                                            <a href="#" style="color: red">no file</a>

                                                        @endif
                                                        <br>
                                                        <strong>Port Charges File</strong>
                                                        @if($value->port_charge_invoice != '')
                                                            <a href="/PortCharges/{{$value->port_charge_invoice}}"  target="_blank">file</a>

                                                        @else

                                                            <a href="#"  style="color: red;">no file</a>

                                                        @endif <br>
                                                        <strong>Invoice Doc </strong>
                                                        @if($value->invoice_order != '')
                                                            <a href="/OrderDoc/{{$value->invoice_order}}"  target="_blank">file</a>

                                                        @else

                                                            <a href="#"  style="color: red;">no file</a>

                                                        @endif <br>
                                                        <strong>Bill of lading Doc </strong>
                                                        @if($value->bl_doc != '')
                                                            <a href="/OrderDoc/{{$value->bl_doc}}"  target="_blank">file</a>

                                                        @else

                                                            <a href="#"  style="color: red">no file</a>

                                                        @endif <br>
                                                        <strong>Packing List Doc</strong>
                                                        @if($value->package_list_doc != '')
                                                            <a href="/OrderDoc/{{$value->package_list_doc}}"  target="_blank">file</a>

                                                        @else

                                                            <a href="#"  style="color: red">no file</a>

                                                        @endif <br>

                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="panel panel-default">
                                                <div class="pt-3">
                                                    <h5 class="panel-title font-20 mx-2"><strong>Cargo  Details </strong></h5>
                                                </div>
                                                <div class="">
                                                    <div class="table-responsive">
                                                        <table class="table ">
                                                            <thead>
                                                            <tr>
                                                                <th>#</th>
                                                                <th><strong>Container no / Chassis no </strong></th>
                                                                <th class="text-center"><strong>Size /  Units</strong></th>
                                                                <th class="text-center"><strong>Transporter</strong>
                                                                </th>
                                                                <th  class="text-center"><strong>Truck / Trailer No</strong>
                                                                </th>
                                                                <th  class="text-center"><strong>Date of Loading</strong></th>
                                                                <th  class="text-center"><strong>Driver</strong></th>
                                                                <th  class="text-center"><strong>Driver Contact</strong></th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            @foreach($containers as $cont)
                                                                <tr>
                                                                    <td>{{$cont->id}}</td>
                                                                    <td >{{$cont->cont_no}}</td>
                                                                    <td class="text-center">{{$cont->type}}</td>
                                                                    <td class="text-center">{{$cont->transporter_name}}</td>
                                                                    <td class="text-center">{{$cont->trucking_number}}</td>
                                                                    <td class="text-center">{{$cont->loading_date}}</td>
                                                                    <td class="text-center">{{$cont->driver_name}}</td>
                                                                    <td class="text-center">{{$cont->driver_contact}}</td>
                                                                </tr>
                                                            @endforeach
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-lg-12">
                                                {{--<address>--}}
                                                {{--Invoice Date : {{$value->invoice_date}} <br>--}}
                                                {{--Invoice By :  {{$value->invoice_by}}<br>--}}
                                                {{--Checked By  :  {{$value->checked_by}}<br>--}}
                                                {{--</address>--}}

                                                <address class="pt-4">
                                                    <strong>Special Instruction  if any : {{$value->special_instruction}} </strong><br>
                                                    Remarks  : {{$value->remarks}} <br>
                                                </address>
                                                @if (Session::has('message'))
                                                    <div class="alert alert-success alert-dismissable">
                                                        <a href="" class="close" data-dismiss="alert" aria-label="close">
                                                            &times;

                                                        </a>
                                                        {{ Session::get('message') }}
                                                    </div>

                                                @endif
                                                @if (Session::has('delete'))
                                                    <div class="alert alert-success alert-dismissable">
                                                        <a href="" class="close" data-dismiss="alert" aria-label="close">
                                                            &times;

                                                        </a>
                                                        {{ Session::get('delete') }}
                                                    </div>

                                                @endif
                                                @if (Session::has('updated'))
                                                    <div class="alert alert-success alert-dismissable">
                                                        <a href="" class="close" data-dismiss="alert" aria-label="close">
                                                            &times;

                                                        </a>
                                                        {{ Session::get('updated') }}
                                                    </div>

                                                @endif
                                                <h4 class="panel-title font-20 pt-5"><strong>* Trucking Updates </strong></h4>

                                                <table class="table table-bordered">
                                                    <thead>
                                                    <tr style="background-color: #8dc3428a; color: #000 !important;">
                                                        <th scope="col">#</th>
                                                        <th>Bill of lading</th>
                                                        <th>Container no / Chassis no </th>
                                                        <th scope="col">Processing Date  </th>
                                                        <th scope="col">Updated Status</th>
                                                        <th>Comments</th>
                                                        <th scope="col"> Done by</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody >
                                                    <?php $i=1;?>
                                                    @foreach($status as $updates)
                                                        <tr>
                                                            <th scope="row">{{$i++}}</th>
                                                            <td>{{$updates->bill_lading}}   </td>
                                                            <td>{{$updates->cont_no}}</td>
                                                            <td>{{$updates->processing_date}}   </td>
                                                            <td>{{$updates->processing_status}}</td>
                                                            <td>{{$updates->other_comment}}</td>
                                                            <td>{{$updates->name}} -- {{$updates->updated_at}}</td>

                                                        </tr>
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                @endforeach
                            </div>
                        </div>
                </div>

                @include('layouts.footer')
            </div>
        </div>

@include('layouts.javas')
</body>
</html>