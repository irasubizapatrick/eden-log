<!DOCTYPE html>
<html>
@include('layouts.finance_head')
<style>
    .form_size{
        height: calc(3.25rem + 2px) !Important;
    }
</style>
<body>

@include('layouts.documentation_sidebar')
<div class="wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <div class="btn-group pull-right">
                        <ol class="breadcrumb hide-phone p-0 m-0">

                        </ol>


                    </div>
                </div>
            </div>
        </div>
        @if (Session::has('message'))
            <div class="alert alert-success alert-dismissable">
                <a href="" class="close" data-dismiss="alert" aria-label="close">
                    &times;

                </a>
                {{ Session::get('message') }}
            </div>

        @endif
        @if (Session::has('delete'))
            <div class="alert alert-success alert-dismissable">
                <a href="" class="close" data-dismiss="alert" aria-label="close">
                    &times;

                </a>
                {{ Session::get('delete') }}
            </div>

        @endif
        @if (Session::has('updated'))
            <div class="alert alert-success alert-dismissable">
                <a href="" class="close" data-dismiss="alert" aria-label="close">
                    &times;

                </a>
                {{ Session::get('updated') }}
            </div>

        @endif
        @forelse($password as $value)
            <div class="row mx-auto">
                <div class="card m-b-30">
                    <div class="card-body"  >
                        <div class="col-12">
                            <form class="form-horizontal"  action="/update_password/{{$value->id}}" method="POST" enctype="multipart/form-data">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                <div class="row ">
                                    <div class="col-md-12 mt-4" >
                                        <label>Email</label>
                                        <input type="text" class="form-control" value="{{$value->email}}" name="email" placeholder="Enter email " value="">
                                    </div>
                                    <div class="col-md-12 mt-4" >
                                        <label>Password </label>
                                        <input type="password" class="form-control" name="password" placeholder="Enter password " value="">
                                    </div>
                                    <div class="col-md-12 mt-4">
                                        <button type="submit" class="pull-left btn btn-primary waves-effect"></i> Submit</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        @empty

        @endforelse
    </div>
@include('layouts.footer')
@include('layouts.javas')
</body>
</html>