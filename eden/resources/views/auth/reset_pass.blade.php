<!DOCTYPE html>
<html>
@include('layouts.admin_head')
<style>
    .form_size{
        height: calc(3.25rem + 2px) !Important;
    }
</style>
<body>

<!-- Navigation Bar-->
@include('layouts.sales_sidebar')
<!-- End Navigation Bar-->

<div class="wrapper">
    <div class="container-fluid">

        <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <div class="btn-group pull-right">
                        <ol class="breadcrumb hide-phone p-0 m-0">

                        </ol>


                    </div>
                </div>
            </div>
        </div>
        <!-- end page title end breadcrumb -->
        <div class="row mx-auto">
            <div class="card m-b-30">
                <div class="card-body"  >
                    <div class="col-12">
                        <form class="form-horizontal"  action="/change_password" method="POST" enctype="multipart/form-data">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                            <div class="row ">
                                <div class="col-md-12 mt-4" >
                                    <label>Email</label>
                                    <input type="text" class="form-control" name="email" placeholder="Enter email " value="">
                                </div>
                                <div class="col-md-12 mt-4" >
                                    <label>Password </label>
                                    <input type="text" class="form-control" name="password" placeholder="Enter password " value="">
                                </div>
                                <div class="col-md-12 mt-4">
                                    <button type="submit" class="pull-left btn btn-primary waves-effect"></i> Submit</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div> <!-- end col -->
            <!-- end row -->
        </div> <!-- end container -->
    </div>
    <!-- end wrapper -->


    <!-- Footer -->
@include('layouts.footer')
<!-- End Footer -->


@include('layouts.javas')

</body>
</html>
<!-- Localized -->