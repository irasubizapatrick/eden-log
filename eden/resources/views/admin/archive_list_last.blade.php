<!DOCTYPE html>
<html>

@include('layouts.admin_modal')
<style>
    .size{
        height: calc(3.25rem + 2px) !Important;
    }
</style>
<body>
<!-- Navigation Bar-->
@include('layouts.admin_sidebar')
<!-- End Navigation Bar-->
<div class="wrapper">
    <div class="container-fluid">
        <!-- end page title end breadcrumb -->
        <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title mt-0" id="myLargeModalLabel">Upload Document</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>

                    <div class="modal-body">
                        <form class="form-horizontal"  action="/checking_ladding" method="POST" enctype="multipart/form-data">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                            <div class="row col-md-12">
                                <div class="col-md-12">
                                    <label>Select Bill of ladding</label>
                                    <select class="form-control" name="ladding_id" required style="height: 4rem;">
                                        <option value=""> -- Choose --</option>
                                        @foreach($ladding as $test)
                                            <option value="{{$test->id}}">{{$test->bill_lading}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-6 pull-left">
                                    <label> Invoice Document </label>
                                    <input type="file"  name="invoice_doc" class="form-control">
                                </div>
                                <div class="col-md-6">
                                    <label>Shipping Line Charges</label>
                                    <input type="file"  name="shipping_line_charges" class="form-control">
                                </div>
                                <div class="col-md-6">
                                    <label>Port charges</label>
                                    <input type="file"  name="port_charges" class="form-control">
                                </div>
                                <div class="col-md-6">
                                    <label>Customer Payments</label>
                                    <input type="file"  name="customer_payments" class="form-control">
                                </div>
                                <div class="col-md-6 pull-left">
                                    <input type="hidden" class="form-control" name="role" value="Agent" required>
                                    <input type="hidden" class="form-control" name="user_id" value="{{Auth::user()->id}}" required>

                                </div>
                                <div class="col-md-6 mx-auto">
                                    <div class="modal-footer ">
                                        <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Close</button>
                                        <button type="submit" class="btn btn-primary waves-effect waves-light">Save changes</button>
                                    </div>
                                </div>

                            </div>
                        </form>
                    </div>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    <div class="row m-0 p-3">
        <div class="col-sm-12">
            <div class="page-title-box">
                <div class="btn-group pull-right">
                    <ol class="breadcrumb hide-phone p-0 m-0">
                        <li class="breadcrumb-item"><a href="#">Archive List</a></li>
                        <li class="breadcrumb-item active">Dashboard</li>
                    </ol>
                </div>
                <h4 class="page-title">All Document</h4>
            </div>
        </div>
        <div class="col-12 container">
            <div class="card m-b-30">
                <div class="card-body">
                    <button type="button" class="btn btn-success waves-effect waves-light pull-right" data-toggle="modal" data-target=".bs-example-modal-lg "><i class="fa fa-plus"></i> <span>Add</span></button>
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <strong>Whoops!</strong> There were some problems with your input.<br><br>
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    @if (Session::has('message'))
                        <div class="alert alert-success alert-dismissable">
                            <a href="" class="close" data-dismiss="alert" aria-label="close">
                                &times;

                            </a>
                            {{ Session::get('message') }}
                        </div>

                    @endif
                    @if (Session::has('delete'))
                        <div class="alert alert-success alert-dismissable">
                            <a href="" class="close" data-dismiss="alert" aria-label="close">
                                &times;

                            </a>
                            {{ Session::get('delete') }}
                        </div>

                    @endif
                    @if (Session::has('updated'))
                        <div class="alert alert-success alert-dismissable">
                            <a href="" class="close" data-dismiss="alert" aria-label="close">
                                &times;

                            </a>
                            {{ Session::get('updated') }}
                        </div>

                    @endif

                    <table id="datatable-buttons" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                        <tr style="font-size: 13px;background-color: #416b8d !important;color: #fff;">
                            <th>#</th>
                            <th>Bill of lading</th>
                            <th>Invoice </th>
                            <th>Shipping line Charges</th>
                            <th>Port Changes</th>
                            <th>Payment Proof</th>
                            <th>Action</th>
                        </tr>
                        </thead>


                        <tbody>
                        <?php $i=0;?>
                        @foreach($archive AS $value)
                            <tr>
                                <td>{{$i+1}}</td>
                                <td>{{$value->bill_lading}}</td>
                                <td>

                                    <a href="/archives/{{$value->invoice_doc}}"  target="_blank">file</a>

                                </td>
                                <td>
                                    <a href="/archives/{{$value->shipping_line_charges}}"  target="_blank">file</a>

                                </td>

                                <td>
                                    <a href="/archives/{{$value->port_charges}}"  target="_blank">file</a>

                                </td>
                                <td>
                                    <a href="/archives/{{$value->customer_payments}}"  target="_blank">file</a>
                                </td>
                                <td>
                                    <button data-toggle="modal" data-target="#editleads<?php echo $i;?>" class="pull-left edit btn btn-primary dlt_sm_table"><i class="fa fa-pencil"></i></button>
                                    <form class="pull-left" action="/all_agent/{{$value->id}}" method="POST">
                                        <input type="hidden" name="_method" value="DELETE" />
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                        <button  type="submit" class="delete btn btn-danger dlt_sm_table" data-toggle="modal" data-target="#modal-basic>" style="margin-left: 20%;"><i class="fa fa-trash"></i></button>
                                    </form>
                                    <div id="editleads<?php echo $i;?>" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
                                        <div class="modal-dialog modal-lg">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h3 class="modal-title" id="editleads"> Edit documents Details</h3>
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

                                                </div>
                                                <div class="modal-body">
                                                    <div class="row">
                                                        <form role="form-horizontal" action="/checking_ladding/{{$value->id}}" method="post" enctype="multipart/form-data">
                                                            <input type="hidden" name="_method" value="PUT" />
                                                            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                                            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                                            <div class="row col-md-12">
                                                                <div class="col-md-12">
                                                                    <label> Bill of ladding</label>
                                                                    <select class="form-control" name="c" required style="height: 4rem;">
                                                                        <option value="{{$value->ladding_id}}"> {{$value->bill_lading}}</option>
                                                                        @foreach($ladding as $test)
                                                                            <option value="{{$test->id}}">{{$test->bill_lading}}</option>
                                                                        @endforeach
                                                                    </select>

                                                                </div>
                                                                <div class="col-md-6 pull-left">
                                                                    <label> Invoice Document </label>
                                                                    <input type="file"  name="invoice_doc" class="form-control">
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <label>Shipping Line Charges</label>
                                                                    <input type="file"  name="shipping_line_charges" class="form-control">
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <label>Port charges</label>
                                                                    <input type="file"  name="port_charges" class="form-control">
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <label>Customer Payments</label>
                                                                    <input type="file"  name="customer_payments" class="form-control">
                                                                </div>
                                                                <div class="col-md-6 pull-left">
                                                                    <input type="hidden" class="form-control" name="role" value="Agent" required>
                                                                    <input type="hidden" class="form-control" name="user_id" value="{{Auth::user()->id}}" required>

                                                                </div>
                                                                <div class="col-md-6 mx-auto">
                                                                    <div class="modal-footer ">
                                                                        <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Close</button>
                                                                        <button type="submit" class="btn btn-primary waves-effect waves-light">Save changes</button>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div><!-- /.modal-content -->
                                        </div><!-- /.modal-dialog -->
                                    </div><!-- /.modal -->

                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                </div>
            </div>
        </div> <!-- end col -->
    </div> <!-- end row -->

</div> <!-- end container -->
</div>
<!-- end wrapper -->


<!-- Footer -->
@include('layouts.footer')
<!-- End Footer -->


@include('layouts.javas')

</body>
</html>
<!-- Localized -->