<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <title>Mikumi Freight Logistics Application</title>
    <meta content="Admin Dashboard" name="description" />
    <meta content="Themesdesign" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <!-- App Icons -->
    <link rel="shortcut icon" href="/assets/images/favicon.ico">


    <!-- DataTables -->
    {{--<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">--}}
    <link href="/assets/plugins/datatables/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
    <link href="/assets/plugins/datatables/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css" />
    <!-- Responsive datatable examples -->
    <link href="/assets/plugins/datatables/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css" />

    <!-- App css -->
    <link href="/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="/assets/css/icons.css" rel="stylesheet" type="text/css" />
    <link href="/assets/css/style.css" rel="stylesheet" type="text/css" />
</head>
<style type="text/css">

    body{
        margin-top:40px;
    }

    .stepwizard-step p {
        margin-top: 10px;
    }

    .stepwizard-row {
        display: table-row;
    }

    .stepwizard {
        display: table;
        width: 100%;
        position: relative;
    }

    .stepwizard-step button[disabled] {
        opacity: 1 !important;
        filter: alpha(opacity=100) !important;
    }

    .stepwizard-row:before {
        top: 14px;
        bottom: 0;
        position: absolute;
        content: " ";
        width: 100%;
        height: 1px;
        background-color: #ccc;
        z-order: 0;

    }

    .stepwizard-step {
        display: table-cell;
        text-align: center;
        position: relative;
    }

    .btn-circle {
        width: 30px;
        height: 30px;
        text-align: center;
        padding: 6px 0;
        font-size: 12px;
        line-height: 1.428571429;
        border-radius: 15px;
    }
    .size{
        height: 34px !important;
    }
</style>
<body>

<!-- Navigation Bar-->
@include('layouts.admin_sidebar')
<!-- End Navigation Bar-->

<div class="wrapper">
    <div class="container-fluid">
        <div class="row pt-3">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <div class="btn-group pull-right">
                        <ol class="breadcrumb hide-phone p-0 m-0">
                            <li class="breadcrumb-item"><a href="#">Orders</a></li>
                            <li class="breadcrumb-item active">Dashboard</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Add Order</h4>
                </div>
            </div>
            <div class="col-12">
                <div class="card m-b-30">
                    <div class="card-body mt-4">
                        {{--<div class="container">--}}
                        {{--<div class="row">--}}
                        {{--<section>--}}
                        {{--<div class="wizard">--}}
                        {{--<div class="wizard-inner">--}}
                        {{--<div class="connecting-line"></div>--}}
                        {{--<ul class="nav nav-tabs" role="tablist" style="width: 120rem;">--}}

                        {{--<li role="presentation" class="active">--}}
                        {{--<a href="#step1" data-toggle="tab" aria-controls="step1" role="tab" title="Step 1">--}}
                        {{--<span class="round-tab">--}}
                        {{--Step 1--}}
                        {{--</span>--}}
                        {{--</a>--}}
                        {{--</li>--}}

                        {{--<li role="presentation" class="disabled">--}}
                        {{--<a href="#step2" data-toggle="tab" aria-controls="step2" role="tab" title="Step 2">--}}
                        {{--<span class="round-tab">--}}
                        {{--Step 2--}}
                        {{--</span>--}}
                        {{--</a>--}}
                        {{--</li>--}}
                        {{--<li role="presentation" class="disabled">--}}
                        {{--<a href="#complete" data-toggle="tab" aria-controls="complete" role="tab" title="Complete">--}}
                        {{--<span class="round-tab">--}}
                        {{--Finish--}}
                        {{--</span>--}}
                        {{--</a>--}}
                        {{--</li>--}}
                        {{--</ul>--}}
                        {{--</div>--}}
                        {{--<form role="form" class="form-horizontal"  action="/admin_add_order" method="POST" enctype="multipart/form-data">--}}
                        {{--<input type="hidden" name="_token" value="{{ csrf_token() }}" />--}}
                        {{--<div class="tab-content">--}}
                        {{--<div class="tab-pane active" role="tabpanel" id="step1">--}}
                        {{--<div class="col-md-12">--}}
                        {{--<div class="col-md-6">--}}
                        {{--<label>  Order Created Date </label>--}}
                        {{--<input type="date" class="form-control" name="date" placeholder="Enter Date"  >--}}
                        {{--</div>--}}
                        {{--<div class="col-md-6">--}}
                        {{--<label> Bill of lading </label>--}}
                        {{--<input type="text" class="form-control" name="bill_lading" placeholder="Enter bill of lading" value="0">--}}
                        {{--</div>--}}
                        {{--<div class="col-md-6">--}}
                        {{--<label> Client Name / Code Consignee </label>--}}
                        {{--<input type="text" class="form-control" name="client_name" placeholder="Enter client name" value="0" >--}}
                        {{--</div>--}}

                        {{--<div class="col-md-6">--}}
                        {{--<label> Type Container </label>--}}
                        {{--<select type="text" class="form-control size" name="type" required>--}}
                        {{--<option value="0"> Select Cargo Type </option>--}}
                        {{--<option value="20ft "> 20ft </option>--}}
                        {{--<option value="40ft "> 40ft </option>--}}
                        {{--<option value="Loose Cargo "> Loose Cargo </option>--}}
                        {{--</select>--}}
                        {{--</div>--}}
                        {{--<div class="col-md-3">--}}
                        {{--<label>Weight</label>--}}
                        {{--<input type="number" class="form-control" name="weight" placeholder="Enter weight" value="0">--}}
                        {{--</div>--}}
                        {{--<div class="col-md-3">--}}
                        {{--<label> Frequency </label>--}}
                        {{--<select class="form-control size" name="frequency">--}}
                        {{--<option value="KG">KG</option>--}}
                        {{--<option value="Tone">Tone </option>--}}
                        {{--</select>--}}
                        {{--</div>--}}
                        {{--<div class="col-md-6">--}}
                        {{--<label>  Line/ Vessel </label>--}}
                        {{--<input type="text" class="form-control" name="line_vessel" placeholder="Enter line vessel" value="0">--}}
                        {{--</div>--}}
                        {{--<div class="col-md-6">--}}
                        {{--<label> Eta </label>--}}
                        {{--<input type="date" class="form-control" name="eta" placeholder="Enter eta"  value="0">--}}
                        {{--</div>--}}
                        {{--<div class="col-md-6">--}}
                        {{--<label> Ata </label>--}}
                        {{--<input type="date" class="form-control" name="ata" placeholder="Enter ata"  value="0">--}}
                        {{--</div>--}}
                        {{--<div class="col-md-6">--}}
                        {{--<label>  C&F At Destination </label>--}}
                        {{--<select class="form-control size" name="agent_id"   required>--}}
                        {{--<option value="">  Select   </option>--}}
                        {{--@foreach($agents as $agent)--}}
                        {{--<option value="{{$agent->id}}">{{$agent->organization_name}}</option>--}}
                        {{--@endforeach--}}
                        {{--</select>--}}
                        {{--</div>--}}
                        {{--<div class="col-md-6">--}}
                        {{--<label> Documents Received Date </label>--}}
                        {{--<input type="date" class="form-control" name="document_date_received" placeholder="Enter document date received"  >--}}
                        {{--</div>--}}

                        {{--<div class="col-md-6">--}}
                        {{--<label> Destination </label>--}}
                        {{--<input type="text" class="form-control" name="destination" placeholder="Enter destination" value="0">--}}
                        {{--</div>--}}
                        {{--<div class="col-md-6">--}}
                        {{--<input type="hidden" name="count" value="1" />--}}
                        {{--<label class="control-label" for="field1">Cont-no.</label>--}}
                        {{--<div class="controls" id="profs">--}}
                        {{--<div class="input-append">--}}
                        {{--<div id="field">--}}
                        {{--<input autocomplete="off" class="input form-control"  id="field1" name="cont_no[]" type="text" placeholder="Enter cont -no." value="0" data-items="8" style="width: 52rem;"/>--}}
                        {{--<button id="b1" class="btn btn-success btn add-more btn-lg" type="button"  style="margin-left: 95%;margin-top: -6rem;">+</button>--}}
                        {{--</div>--}}
                        {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="multi-field-wrapper2 container">--}}
                        {{--<div class="multi-fields2">--}}
                        {{--<div class="row multi-field2">--}}
                        {{--<input type="hidden" name="count" value="1" />--}}
                        {{--<label class="control-label" for="field1">Cont-no.</label>--}}
                        {{--<input type="text" id="projectinput1" class="form-control" name="cont_no[]" value="0" >--}}

                        {{--<button type="button" class="remove-field2 btn btn-success "><i class="fa fa-times-circle"></i></button>--}}
                        {{--<button class="remove-field2 btn btn-success btn" type="button" style="height: 32px;width: 40px;margin: 1.8em 10px 0px 0px;" >+</button>--}}
                        {{--<button  class="remove-field2 btn btn-success btn" type="button" style="height: 32px;width: 40px;margin: 1.8em 10px 0px 0px;">-</button>--}}
                        {{--<button type="button" class="remove-field2 btn btn-danger" style="height: 32px;width: 40px;margin: 1.8em 10px 0px 0px;">-</button>--}}
                        {{--<button type="button" class="add-field2 btn btn-success" style="height: 32px;width: 40px;margin: 1.8em 10px 0px 0px;">+</button>--}}
                        {{--</div>--}}
                        {{--</div>--}}
                        {{--</div>--}}
                        {{--</div>--}}
                        {{--</div>--}}
                        {{--<ul class="list-inline pull-right p-5">--}}
                        {{--<li><button type="button" class="btn btn-default next-step">Skip</button></li>--}}
                        {{--<li><button type="button" class="btn btn-primary next-step">Click to go next</button></li>--}}
                        {{--<li><button type="submit" class="btn btn-success waves-effect waves-light">Save</button></li>--}}
                        {{--</ul>--}}
                        {{--</div>--}}
                        {{--<div class="tab-pane" role="tabpanel" id="step2">--}}
                        {{--<div class="col-md-12">--}}

                        {{--<div class="col-md-6">--}}
                        {{--<label> Terminal   </label>--}}
                        {{--<input type="text" class="form-control" name="terminal" placeholder="Enter terminal" value="0">--}}
                        {{--</div>--}}
                        {{--<div class="col-md-6">--}}
                        {{--<label> Shipping line   </label>--}}
                        {{--<input type="text" class="form-control" name="shipping_line" placeholder="Enter Shipping line " value="0">--}}
                        {{--</div>--}}

                        {{--<div class="col-md-6">--}}
                        {{--<label> Shipper   </label>--}}
                        {{--<input type="text" class="form-control" name="shipper" placeholder="Enter Shipper Name" value="0">--}}
                        {{--</div>--}}

                        {{--<div class="col-md-6">--}}
                        {{--<label> Cargo  Description / Commodity </label>--}}
                        {{--<input type="text" class="form-control" name="commodity" placeholder="Enter commodity" value="0">--}}
                        {{--</div>--}}
                        {{--<div class="col-md-6">--}}
                        {{--<label> Port Charges </label>--}}
                        {{--<input type="text" class="form-control" name="port_charge" placeholder="Enter port charge" value="0">--}}
                        {{--</div>--}}
                        {{--<div class="col-md-6">--}}
                        {{--<label> Voyager Number </label>--}}
                        {{--<input type="text" class="form-control" name="voyager_number" placeholder="Enter Voyage Number" value="0">--}}
                        {{--</div>--}}
                        {{--<div class="col-md-6">--}}
                        {{--<label> Remarks </label>--}}
                        {{--<input type="text" class="form-control" name="remarks" placeholder="Enter remarks"  value="0">--}}
                        {{--<input type="hidden" class="form-control" name="user_id" value="1" >--}}
                        {{--</div>--}}
                        {{--<div class="col-md-6">--}}
                        {{--<label> Special Instruction </label>--}}
                        {{--<input type="text" class="form-control" name="special_instruction" placeholder="Enter Special Instruction"  value="0" >--}}
                        {{--</div>--}}
                        {{--<div class="col-md-6">--}}
                        {{--<label>File Open BY </label>--}}
                        {{--<input type="text" class="form-control" name="file_opened_by"   value="0" placeholder="Enter Who opened file">--}}
                        {{--</div>--}}

                        {{--<div class="col-md-6">--}}
                        {{--<label> Invoice Doc </label>--}}
                        {{--<input type="file" class="form-control" name="invoice_order" value="0">--}}
                        {{--</div>--}}
                        {{--</div>--}}
                        {{--<ul class="list-inline pull-right p-5">--}}
                        {{--<li><button type="button" class="btn btn-default prev-step">Previous</button></li>--}}
                        {{--<li><button type="button" class="btn btn-primary next-step">Click to go next</button></li>--}}
                        {{--<li><button type="submit" class="btn btn-success waves-effect waves-light">Save</button></li>--}}
                        {{--</ul>--}}
                        {{--</div>--}}
                        {{--<div class="tab-pane" role="tabpanel" id="step3">--}}
                        {{--<div class="col-md-12">--}}
                        {{--<div class="col-md-6">--}}
                        {{--<label>  Interchange </label>--}}
                        {{--<input type="text" class="form-control" name="inter_change" placeholder="Enter inter change"  value="0">--}}
                        {{--</div>--}}
                        {{--<div class="col-md-6">--}}
                        {{--<label> Cont drop off</label>--}}
                        {{--<input type="text" class="form-control" name="cont_drop_off" placeholder="Enter cont drop off"  value="0">--}}
                        {{--</div>--}}
                        {{--</div>--}}
                        {{--<ul class="list-inline pull-right p-5" >--}}
                        {{--<li><button type="button" class="btn btn-default prev-step">Previous</button></li>--}}

                        {{--<li><button type="button" class="btn btn-primary btn-info-full next-step">Click to go next</button></li>--}}
                        {{--<li><button type="submit" class="btn btn-success waves-effect waves-light">Submit</button></li>--}}

                        {{--</ul>--}}
                        {{--</div>--}}
                        {{--<div class="tab-pane" role="tabpanel" id="complete">--}}
                        {{--<h3>Complete</h3>--}}
                        {{--<p>You have successfully completed all steps.</p>--}}
                        {{--<button type="submit" class="btn btn-success waves-effect waves-light">Save</button>--}}
                        {{--</div>--}}
                        {{--<div class="clearfix"></div>--}}
                        {{--</div>--}}
                        {{--</form>--}}
                        {{--</div>--}}
                        {{--</section>--}}
                        {{--</div>--}}
                        {{--</div>--}}

                        <div class="container">
                            <div class="row">
                                <div class="container">
                                    <div class="stepwizard">
                                        <div class="stepwizard-row setup-panel">
                                            <div class="stepwizard-step">
                                                <a href="#step-1" type="button" class="btn btn-primary btn-circle">1</a>
                                                <p>Step 1</p>
                                            </div>
                                            <div class="stepwizard-step">
                                                <a href="#step-2" type="button" class="btn btn-default btn-circle" disabled="disabled">2</a>
                                                <p>Step 2</p>
                                            </div>
                                            <div class="stepwizard-step">
                                                <a href="#step-3" type="button" class="btn btn-default btn-circle" disabled="disabled">3</a>
                                                <p>Step 3</p>
                                            </div>
                                            <div class="stepwizard-step">
                                                <a href="#step-4" type="button" class="btn btn-default btn-circle" disabled="disabled">4</a>
                                                <p>Step 4</p>
                                            </div>
                                        </div>
                                    </div>
                                    <form role="form" class="form-horizontal"  action="/admin_add_order" method="POST" enctype="multipart/form-data">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                        <div class="row setup-content" id="step-1">
                                            <div class="col-xs-12">
                                                <div class="col-md-12">
                                                    <div class="col-md-6">
                                                        <label>  Order Created Date </label>
                                                        <input type="date" class="form-control" name="date" placeholder="Enter Date"  >
                                                    </div>
                                                    <div class="col-md-6">
                                                        <label> Bill of lading </label>
                                                        <input type="text" class="form-control" name="bill_lading" placeholder="Enter bill of lading" value="0">
                                                    </div>
                                                    <div class="col-md-6">
                                                        <label> Client Name / Code Consignee </label>
                                                        <input type="text" class="form-control" name="client_name" placeholder="Enter client name" value="0" >
                                                    </div>

                                                    <div class="col-md-6">
                                                        <label> Type Container </label>
                                                        <select type="text" class="form-control size" name="type" required>
                                                            <option value="0"> Select Cargo Type </option>
                                                            <option value="20ft "> 20ft </option>
                                                            <option value="40ft "> 40ft </option>
                                                            <option value="Loose Cargo "> Loose Cargo </option>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <label>Weight</label>
                                                        <input type="number" class="form-control" name="weight" placeholder="Enter weight" value="0">
                                                    </div>
                                                    <div class="col-md-3">
                                                        <label> Frequency </label>
                                                        <select class="form-control size" name="frequency">
                                                            <option value="KG">KG</option>
                                                            <option value="Tone">Tone </option>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <label>  Line/ Vessel </label>
                                                        <input type="text" class="form-control" name="line_vessel" placeholder="Enter line vessel" value="0">
                                                    </div>
                                                    <div class="col-md-6">
                                                        <label> Eta </label>
                                                        <input type="date" class="form-control" name="eta" placeholder="Enter eta"  value="0">
                                                    </div>
                                                    <div class="col-md-6">
                                                        <label> Ata </label>
                                                        <input type="date" class="form-control" name="ata" placeholder="Enter ata"  value="0">
                                                    </div>
                                                    <div class="col-md-6">
                                                        <label>  C&F At Destination </label>
                                                        <select class="form-control size" name="agent_id"   required>
                                                            <option value="">  Select   </option>
                                                            @foreach($agents as $agent)
                                                                <option value="{{$agent->id}}">{{$agent->organization_name}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <label> Documents Received Date </label>
                                                        <input type="date" class="form-control" name="document_date_received" placeholder="Enter document date received"  >
                                                    </div>

                                                    <div class="col-md-6">
                                                        <label> Destination </label>
                                                        <input type="text" class="form-control" name="destination" placeholder="Enter destination" value="0">
                                                    </div>
                                                    <div class="col-md-6">
                                                        {{--<input type="hidden" name="count" value="1" />--}}
                                                        {{--<label class="control-label" for="field1">Cont-no.</label>--}}
                                                        {{--<div class="controls" id="profs">--}}
                                                        {{--<div class="input-append">--}}
                                                        {{--<div id="field">--}}
                                                        {{--<input autocomplete="off" class="input form-control"  id="field1" name="cont_no[]" type="text" placeholder="Enter cont -no." value="0" data-items="8" style="width: 52rem;"/>--}}
                                                        {{--<button id="b1" class="btn btn-success btn add-more btn-lg" type="button"  style="margin-left: 95%;margin-top: -6rem;">+</button>--}}
                                                        {{--</div>--}}
                                                        {{--</div>--}}
                                                        {{--</div>--}}
                                                        <div class="multi-field-wrapper2 container">
                                                            <div class="multi-fields2">
                                                                <div class="row multi-field2">
                                                                    <input type="hidden" name="count" value="1" />
                                                                    <label class="control-label" for="field1">Cont-no.</label>
                                                                    <input type="text" id="projectinput1" class="form-control" name="cont_no[]" value="0" >

                                                                    {{--<button type="button" class="remove-field2 btn btn-success "><i class="fa fa-times-circle"></i></button>--}}
                                                                    {{--<button class="remove-field2 btn btn-success btn" type="button" style="height: 32px;width: 40px;margin: 1.8em 10px 0px 0px;" >+</button>--}}
                                                                    {{--<button  class="remove-field2 btn btn-success btn" type="button" style="height: 32px;width: 40px;margin: 1.8em 10px 0px 0px;">-</button>--}}
                                                                    <button type="button" class="remove-field2 btn btn-danger" style="height: 32px;width: 40px;margin: 1.8em 10px 0px 0px;">-</button>
                                                                    <button type="button" class="add-field2 btn btn-success" style="height: 32px;width: 40px;margin: 1.8em 10px 0px 0px;">+</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <button class="btn btn-primary mx-3 nextBtn btn-lg pull-right" type="button" >Next</button>
                                                <button class="btn btn-success  btn-lg pull-right" type="submit" >Save</button>
                                            </div>
                                        </div>
                                        <div class="row setup-content" id="step-2">
                                            <div class="col-xs-12">
                                                <div class="col-md-12">

                                                    <div class="col-md-6">
                                                        <label> Terminal   </label>
                                                        <input type="text" class="form-control" name="terminal" placeholder="Enter terminal" value="0">
                                                    </div>
                                                    <div class="col-md-6">
                                                        <label> Shipping line   </label>
                                                        <input type="text" class="form-control" name="shipping_line" placeholder="Enter Shipping line " value="0">
                                                    </div>

                                                    <div class="col-md-6">
                                                        <label> Shipper   </label>
                                                        <input type="text" class="form-control" name="shipper" placeholder="Enter Shipper Name" value="0">
                                                    </div>

                                                    <div class="col-md-6">
                                                        <label> Cargo  Description / Commodity </label>
                                                        <input type="text" class="form-control" name="commodity" placeholder="Enter commodity" value="0">
                                                    </div>
                                                    <div class="col-md-6">
                                                        <label> Port Charges </label>
                                                        <input type="text" class="form-control" name="port_charge" placeholder="Enter port charge" value="0">
                                                    </div>
                                                    <div class="col-md-6">
                                                        <label> Voyager Number </label>
                                                        <input type="text" class="form-control" name="voyager_number" placeholder="Enter Voyage Number" value="0">
                                                    </div>
                                                    <div class="col-md-6">
                                                        <label> Remarks </label>
                                                        <input type="text" class="form-control" name="remarks" placeholder="Enter remarks"  value="0">
                                                        <input type="hidden" class="form-control" name="user_id" value="1" >
                                                    </div>
                                                    <div class="col-md-6">
                                                        <label> Special Instruction </label>
                                                        <input type="text" class="form-control" name="special_instruction" placeholder="Enter Special Instruction"  value="0" >
                                                    </div>
                                                    <div class="col-md-6">
                                                        <label>File Open BY </label>
                                                        <input type="text" class="form-control" name="file_opened_by"   value="0" placeholder="Enter Who opened file">
                                                    </div>

                                                    <div class="col-md-6">
                                                        <label> Invoice Doc </label>
                                                        <input type="file" class="form-control" name="invoice_order" value="0">
                                                    </div>
                                                </div>
                                                <button class="btn btn-primary mx-3 my-4 nextBtn btn-lg pull-right" type="button" >Next</button>
                                                <button class="btn btn-success  my-4 btn-lg pull-right" type="submit" >Save</button>
                                            </div>
                                        </div>

                                    </form>
                                    <div class="row setup-content" id="step-3">
                                        <div class="col-xs-12">
                                            <div class="col-md-12">
                                                <div class="col-md-6">
                                                    <label>  Interchange </label>
                                                    <input type="text" class="form-control" name="inter_change" placeholder="Enter inter change"  value="0">
                                                </div>
                                                <div class="col-md-6">
                                                    <label> Cont drop off</label>
                                                    <input type="text" class="form-control" name="cont_drop_off" placeholder="Enter cont drop off"  value="0">
                                                </div>
                                            </div>
                                            <button class="btn btn-primary mx-3 my-4 nextBtn btn-lg pull-right" type="button" >Next</button>
                                            <button class="btn btn-success  my-4 btn-lg pull-right" type="submit" >Save</button>
                                        </div>
                                    </div>
                                    <div class="row setup-content" id="step-4">
                                        <div class="col-xs-12">
                                            <div class="col-md-12">
                                                <h3>Complete</h3>
                                                <p>You have successfully completed all steps.</p>
                                                <button  type="submit" class="btn btn-success btn-lg pull-right">Finish!</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div> <!-- end col -->
        </div> <!-- end row -->

    </div>
    <!-- end container -->
</div>
<!-- end wrapper -->
<!-- Footer -->
@include('layouts.footer')

@include('layouts.javas')
<script type="text/javascript">
    $(document).ready(function () {

        var navListItems = $('div.setup-panel div a'),
            allWells = $('.setup-content'),
            allNextBtn = $('.nextBtn');

        allWells.hide();

        navListItems.click(function (e) {
            e.preventDefault();
            var $target = $($(this).attr('href')),
                $item = $(this);

            if (!$item.hasClass('disabled')) {
                navListItems.removeClass('btn-primary').addClass('btn-default');
                $item.addClass('btn-primary');
                allWells.hide();
                $target.show();
                $target.find('input:eq(0)').focus();
            }
        });

        allNextBtn.click(function(){
            var curStep = $(this).closest(".setup-content"),
                curStepBtn = curStep.attr("id"),
                nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
                curInputs = curStep.find("input[type='text'],input[type='url']"),
                isValid = true;

            $(".form-group").removeClass("has-error");
            for(var i=0; i<curInputs.length; i++){
                if (!curInputs[i].validity.valid){
                    isValid = false;
                    $(curInputs[i]).closest(".form-group").addClass("has-error");
                }
            }

            if (isValid)
                nextStepWizard.removeAttr('disabled').trigger('click');
        });

        $('div.setup-panel div a.btn-primary').trigger('click');
    });
</script>

</body>
</html>
<!-- Localized -->
