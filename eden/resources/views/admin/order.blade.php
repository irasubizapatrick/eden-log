<!DOCTYPE html>
<html>
@include('layouts.admin_head')
<body>
<!-- Navigation Bar-->
@include('layouts.admin_sidebar')
<!-- End Navigation Bar-->


<div class="wrapper">
    <div class="container-fluid">

        <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <div class="btn-group pull-right">
                        <ol class="breadcrumb hide-phone p-0 m-0">
                        </ol>
                    </div>
                    <h4 class="page-title">Add Order</h4>
                </div>
            </div>
        </div>
        <!-- end page title end breadcrumb -->

        <div class="row">
            <div class="col-12">
                <div class="card m-b-30">
                    <div class="card-body">

                        <form class="form-horizontal"  action="/my_orders" method="POST" enctype="multipart/form-data">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                            <div class="row col-md-12">
                                <div class="col-md-6">
                                    <label> Date </label>
                                    <input type="date" class="form-control" name="date" placeholder="Enter Date" required>
                                </div>
                                <div class="col-md-6">
                                    <label> Consment. ID </label>
                                    <input type="text" class="form-control" name="cons_meant" placeholder="Enter consment. id" required>
                                </div>
                                <div class="col-md-6">
                                    <label> Bill of lading </label>
                                    <input type="text" class="form-control" name="bill_lading" placeholder="Enter bill of lading" required>
                                </div>
                                <div class="col-md-6">
                                    <label> Cont-no. </label>
                                    <input type="text" class="form-control" name="cont_no" placeholder="Enter cont -no." required>
                                </div>
                                <div class="col-md-6">
                                    <label> Type </label>
                                    <input type="text" class="form-control" name="type" placeholder="Enter type" required>
                                </div>
                                <div class="col-md-6">
                                    <label> Qty </label>
                                    <input type="number" class="form-control" name="qty" placeholder="Enter qty" required>
                                </div>
                                <div class="col-md-6">
                                    <label>  Line/ Vessel </label>
                                    <input type="text" class="form-control" name="line_vessel" placeholder="Enter line vessel" required>
                                </div>
                                <div class="col-md-6">
                                    <label> Eta </label>
                                    <input type="text" class="form-control" name="eta" placeholder="Enter eta" required>
                                </div>
                                <div class="col-md-6">
                                    <label>   Consignee </label>
                                    <input type="text" class="form-control" name="consignee" placeholder="Enter consignee" required>
                                </div>
                                <div class="col-md-6">
                                    <label> C&F At Destination </label>
                                    <input type="text" class="form-control" name="cf_destination" placeholder="Enter cf_destination" required>
                                </div>
                                <div class="col-md-6">
                                    <label> Documents Received Date </label>
                                    <input type="date" class="form-control" name="document_date_received" placeholder="Enter document date received" required>
                                </div>
                                <div class="col-md-6">
                                    <label> Destination </label>
                                    <input type="text" class="form-control" name="destination" placeholder="Enter destination" required>
                                </div>
                                <div class="col-md-6">
                                    <label> Terminal   </label>
                                    <input type="text" class="form-control" name="terminal" placeholder="Enter terminal" required>
                                </div>
                                <div class="col-md-6">
                                    <label> Commodity </label>
                                    <input type="text" class="form-control" name="commodity" placeholder="Enter commodity" required>
                                </div>
                                <div class="col-md-6">
                                    <label> Port Charges </label>
                                    <input type="text" class="form-control" name="port_charge" placeholder="Enter port charge" required>
                                </div>
                                <div class="col-md-6">
                                    <label> Loading Date </label>
                                    <input type="date" class="form-control" name="loading_date" placeholder="Enter loading date" required>
                                </div>
                                <div class="col-md-6">
                                    <label> Transporter </label>
                                    <input type="text" class="form-control" name="transporter" placeholder="Enter transporter" required>
                                </div>
                                <div class="col-md-6">
                                    <label> Trick Details </label>
                                    <input type="text" class="form-control" name="truck_details" placeholder="Enter truck details" required>
                                </div>
                                <div class="col-md-6">
                                    <label> Driver's Contact </label>
                                    <input type="number" class="form-control" name="driver_contact" placeholder="Enter driver contact" required>
                                </div>
                                <div class="col-md-6">
                                    <label>  Interchange </label>
                                    <input type="text" class="form-control" name="inter_change" placeholder="Enter inter change" required>
                                </div>
                                <div class="col-md-6">
                                    <label> Cont drop off</label>
                                    <input type="text" class="form-control" name="cont_drop_off" placeholder="Enter cont drop off" required>
                                </div>
                                <div class="col-md-6">
                                    <label> Remarks </label>
                                    <input type="text" class="form-control" name="remarks" placeholder="Enter remarks" required>
                                    <input type="hidden" class="form-control" name="user_id" value="{{Auth::user()->id}}" required>
                                </div>
                                <div class="col-md-6">
                                    <label> Invoice Date </label>
                                    <input type="date" class="form-control" name="invoice_date" placeholder="Enter Date" required>
                                </div>
                                <div class="col-md-6">
                                    <label>  Invoice By  </label>
                                    <input type="text" class="form-control" name="invoice_by" placeholder="Enter Invoice by " required>
                                </div>
                                <div class="col-md-6">
                                    <label> Checked By </label>
                                    <input type="text" class="form-control" name="checked_by" placeholder="Enter Checked By " required>
                                </div>
                                <div class="col-md-6">
                                    <label> Special Instruction </label>
                                    <input type="text" class="form-control" name="special_instruction" placeholder="Enter Special Instruction" required>
                                </div>
                                <div class="col-md-12">
                                    <center>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Reset</button>
                                            <button type="submit" class="btn btn-primary waves-effect waves-light">Save changes</button>
                                        </div>
                                    </center>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div> <!-- end col -->
        </div> <!-- end row -->

    </div> <!-- end container -->
</div>
<!-- end wrapper -->


<!-- Footer -->
@include('layouts.footer')
<!-- End Footer -->


@include('layouts.javas')

</body>
</html>
<!-- Localized -->