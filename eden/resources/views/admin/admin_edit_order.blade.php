<!DOCTYPE html>
<html>
@include('layouts.admin_modal')
<style type="text/css">
    body{
        margin-top:40px;
    }

    .stepwizard-step p {
        margin-top: 10px;
    }

    .stepwizard-row {
        display: table-row;
    }

    .stepwizard {
        display: table;
        width: 100%;
        position: relative;
    }

    .stepwizard-step button[disabled] {
        opacity: 1 !important;
        filter: alpha(opacity=100) !important;
    }

    .stepwizard-row:before {
        top: 14px;
        bottom: 0;
        position: absolute;
        content: " ";
        width: 100%;
        height: 1px;
        background-color: #ccc;
        z-order: 0;

    }

    .stepwizard-step {
        display: table-cell;
        text-align: center;
        position: relative;
    }

    .has-submenu{
        padding: 0px 10px 0px 10px !important;
        font-weight: 700 !important;
    }
    .btn-circle {
        width: 30px;
        height: 30px;
        text-align: center;
        padding: 6px 0;
        font-size: 12px;
        line-height: 1.428571429;
        border-radius: 15px;
    }
    .size{
        height: 34px !important;
    }
</style>
<body>

@include('layouts.admin_sidebar')

<div class="wrapper">
    <div class="container-fluid">
        <div class="row pt-3">
            <div class="col-sm-12">
                <div class="page-title-box" style="padding: 20px 0 !important;">
                    <div class="btn-group pull-right">
                        <ol class="breadcrumb hide-phone p-0 m-0">
                            <li class="breadcrumb-item"><a href="#">Orders</a></li>
                            <li class="breadcrumb-item active">Dashboard</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Edit Details Order</h4>
                </div>
            </div>
            <div class="col-12">
                <div class="card m-b-30">
                    <div class="card-body mt-4">
                        <div class="container">
                            <div class="row">
                                @foreach($tracks as $value)
                                    <div class="container">
                                        <div class="stepwizard">
                                            <div class="stepwizard-row setup-panel">
                                                <div class="stepwizard-step">
                                                    <a href="#step-1"  class="btn btn-primary btn-circle">1</a>
                                                    <p>Step 1</p>
                                                </div>
                                                <div class="stepwizard-step">
                                                    <a href="#step-2" class="btn btn-default btn-circle" disabled="disabled">2</a>
                                                    <p>Step 2</p>
                                                </div>
                                                <div class="stepwizard-step">
                                                    <a href="#step-3"  class="btn btn-default btn-circle" disabled="disabled">3</a>
                                                    <p>Step 3</p>
                                                </div>
                                                <div class="stepwizard-step">
                                                    <a href="#step-4"  class="btn btn-default btn-circle" disabled="disabled">4</a>
                                                    <p>Step 4</p>
                                                </div>
                                                <div class="stepwizard-step">
                                                    <a href="#step-5"  class="btn btn-default btn-circle" disabled="disabled">5</a>
                                                    <p>Step 5</p>
                                                </div>
                                            </div>
                                        </div>
                                        <form role="form" action="/admin_update_order/{{$value->id}}" method="POST" enctype="multipart/form-data">
                                            <input type="hidden" name="_method" value="PUT" />
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                            <div class="row setup-content" id="step-1">
                                                <div class="col-xs-12">
                                                    <div class="col-md-12">
                                                        <div class="col-md-12">
                                                            <div class="col-md-6">
                                                                <label>  Order Created Date </label>
                                                                <input type="date" class="form-control" name="date" placeholder="Enter Date" value="{{$value->date}}"  >
                                                            </div>
                                                            <div class="col-md-6">
                                                                <label> Bill of lading </label>
                                                                <input type="text" class="form-control" name="bill_lading" placeholder="Enter bill of lading" value="{{$value->bill_lading}}">
                                                            </div>
                                                            <div class="col-md-6">
                                                                <label> Client Name / Code Consignee </label>
                                                                <input type="text" class="form-control" name="client_name" placeholder="Enter client name" value="{{$value->client_name}}" >
                                                            </div>

                                                            <div class="col-md-3">
                                                                <label> Type Container </label>
                                                                <input type="text" class="form-control" name="type" placeholder="Ex: 20ft Container" value="{{$value->type}}">
                                                            </div>

                                                            <div class="col-md-3">
                                                                <label> Regime </label>
                                                                <select class="form-control size" name="declaration_id"  >
                                                                    <option value="">  Select   </option>
                                                                    @foreach($declaration_type as $declarations)
                                                                        <option value="{{$declarations->id}}">{{$declarations->declaration->cargo_declaration_type}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <label>Manifest</label>
                                                                <select class="form-control size" name="manifest">
                                                                    <option value=""> Choose </option>
                                                                    <option value="Yes"> Yes </option>
                                                                    <option value="No"> No </option>
                                                                </select>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <label> Weight </label>
                                                                <input type="number" class="form-control" name="weight" value="{{$value->weight}}" placeholder="Enter weight" >
                                                            </div>
                                                            <div class="col-md-6">
                                                                <label> Frequency </label>
                                                                <select class="form-control size" name="frequency">
                                                                    <option value="KG">KG</option>
                                                                    <option value="Tone">Tone </option>
                                                                </select>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <label>  Line/ Vessel </label>
                                                                <input type="text" class="form-control" name="line_vessel" value="{{$value->line_vessel}}"  placeholder="Enter line vessel">
                                                            </div>
                                                            <div class="col-md-3">
                                                                <label> Eta </label>
                                                                <input type="date" class="form-control" name="eta" placeholder="Enter eta"  value="{{$value->eta}}">
                                                            </div>
                                                            <div class="col-md-3">
                                                                <label> Ata </label>
                                                                <input type="date" class="form-control" name="ata" placeholder="Enter ata"  value="{{$value->ata}}">
                                                            </div>
                                                            <div class="col-md-3">
                                                                <label>   C&F At Destination </label>
                                                                <select class="form-control size" name="agent_id"   required>
                                                                    <option value="{{$value->agent_id}}"> {{$value->organization_name}} </option>
                                                                    @foreach($agent as $agents)
                                                                        <option value="{{$agents->id}}">{{$agents->organization_name}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <label> Documents Received Date </label>
                                                                <input type="date" class="form-control" name="document_date_received" value="{{$value->document_date_received}}" placeholder="Enter document date received"  >
                                                            </div>

                                                            <div class="col-md-3">
                                                                <label>Country</label>
                                                                <select class="form-control  country" name="country_id">
                                                                    <option value=""> Choose </option>
                                                                    @foreach($country as $key => $dara)
                                                                        <option value="{{ $key }}">{{$dara->country_name}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <label> Destination </label>
                                                                <select class="form-control destination" name="destination"  >
                                                                    <option value="{{$value->destination}}">{{$value->destination}}</option>
                                                                    @foreach($destination as $dest)
                                                                        <option value="{{$dest->destination_name}}">{{$dest->destination_name}}</option>
                                                                    @endforeach
                                                                </select>
                                                                </select>
                                                            </div>

                                                            <div class="col-md-6">
                                                                <label> Border Exit </label>
                                                                <input type="text" class="form-control" name="border_exist" placeholder="Enter Border Exist" value="{{$value->border_exist}}">
                                                            </div>
                                                            <div class="col-md-6 mt-3" >
                                                                <input type="hidden" name="count" value="1"  />
                                                                @if($value->type == 'LooseCargo')

                                                                    <label class="control-label" > List Chassis-no</label>
                                                                @else

                                                                    <label class="control-label" >   List Cont-no.</label>
                                                                @endif
                                                                <div class="controls" id="profs">
                                                                    <div class="input-append">
                                                                        <div id="fieldgfg">
                                                                            @foreach( $container_loop as $key => $attendance)

                                                                                <input autocomplete="off" class="input form-control"    name="cont_no_id[]"  placeholder="Enter cont -no."  value="{{$attendance->id}}" data-items="8" type="hidden"/>
                                                                                <input autocomplete="off" class="input form-control mt-2"    name="cont_no[]" type="text" placeholder="Enter cont -no."  value="{{$attendance->cont_no}}" data-items="8" />
                                                                            @endforeach
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <button class="btn btn-primary nextBtn mx-3  my-2 btn-lg pull-right" type="button" >Next</button>
                                                        <button class="btn btn-success  btn-lg pull-right my-2 " type="submit" >Save</button>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row setup-content" id="step-2">
                                                <div class="col-xs-12">
                                                    <div class="col-md-12">
                                                        <div class="col-md-12">
                                                            <div class="col-md-6">
                                                                <label> Declaration</label>
                                                                <select class="form-control size declaration" name="declaration" id="declaration" >

                                                                    @if($value->declaration == '')
                                                                        <option value=""> Choose Status </option>
                                                                        <option value="Amendment">Amendment</option>
                                                                        <option value="Storage">Storage</option>
                                                                        <option value="Clear">Clear</option>
                                                                    @else
                                                                        <option value="{{$value->declaration}}">{{$value->declaration}}</option>

                                                                    @endif
                                                                </select>
                                                            </div>
                                                            @if($value->amount == '')
                                                                <div class="col-md-6" id="amount" style="display: none;">
                                                                    @else
                                                                        <div class="col-md-6" id="amount" >
                                                                            @endif
                                                                            <label>Declaration Amount </label>
                                                                            @if($value->amount == '')
                                                                                <input type="text" class="form-control" name="amount" placeholder="Enter Amount" >
                                                                            @else
                                                                                <input type="text" class="form-control" name="amount" value="{{$value->amount}}" placeholder="Enter Amount" >
                                                                            @endif
                                                                        </div>

                                                                        <div class="col-md-6">
                                                                            <label> Terminal   </label>
                                                                            <input type="text" class="form-control" name="terminal" placeholder="Enter terminal" value="{{$value->terminal}}">
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <label> Delivery  Order   </label>
                                                                            <input type="file" class="form-control" name="delivery_order"  value="{{$value->delivery_order}}">
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <label> Shipping Line   </label>
                                                                            <input type="text" class="form-control" name="shipping_line" placeholder="Enter Shipping Name" value="{{$value->shipping_line}}">
                                                                        </div>

                                                                        <div class="col-md-6">
                                                                            <label> Shipper   </label>
                                                                            <input type="text" class="form-control" name="shipper" placeholder="Enter Shipper Name" value="{{$value->shipper}}">
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <label> Cargo  Description / Commodity </label>
                                                                            <input type="text" class="form-control" name="commodity" placeholder="Enter commodity" value="{{$value->commodity}}">
                                                                        </div>


                                                                        <div class="col-md-6">
                                                                            <label> Voyager Number </label>
                                                                            <input type="text" class="form-control" name="voyager_number" placeholder="Enter Voyage Number" value="{{$value->voyager_number}}">
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <label> Port Charges  Amount</label>
                                                                            <input type="text" class="form-control" name="port_charge" placeholder="Enter port charge" value="{{$value->port_charge}}">
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <label> Port Charges  Invoice</label>
                                                                            <input type="file" class="form-control" name="port_charge_invoice" placeholder="Enter port Invoice" value="{{$value->port_charge_invoice}}">
                                                                        </div>


                                                                </div>
                                                                <button class="btn btn-primary mx-3 my-4 nextBtn btn-lg pull-right" type="button" >Next</button>
                                                                <button class="btn btn-success  btn-lg pull-right my-4" type="submit" >Save</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row setup-content" id="step-3">
                                                <div class="col-xs-12">
                                                    <div class="col-md-12">
                                                        <div class="col-md-12">
                                                            <div class="col-md-6">
                                                                <label> Remarks </label>
                                                                <input type="text" class="form-control" name="remarks" placeholder="Enter remarks"  value="{{$value->remarks}}">
                                                                <input type="hidden" class="form-control" name="user_id" value="{{Auth::user()->id}}" >
                                                            </div>
                                                            <div class="col-md-6">
                                                                <label> Special Instruction </label>
                                                                <input type="text" class="form-control" name="special_instruction" placeholder="Enter Special Instruction"  value="{{$value->special_instruction}}" >
                                                            </div>
                                                            <div class="col-md-6">
                                                                <label> Invoice Doc </label>
                                                                <input type="file" class="form-control" name="invoice_order" value="0">
                                                            </div>
                                                            <div class="col-md-6">
                                                                <label> Bill of lading Doc  </label>
                                                                <input type="file" class="form-control" name="bl_doc" value="0">
                                                            </div>
                                                            <div class="col-md-6">
                                                                <label> Package List Doc </label>
                                                                <input type="file" class="form-control" name="package_list_doc" value="0">
                                                            </div>
                                                            <div class="col-md-6">
                                                                <label> Guart Amount Taken   </label>
                                                                <input type="number" class="form-control" name="guart_amount_taken" placeholder="Enter Amount" value="{{$value->guart_amount_taken}}">
                                                            </div>
                                                            <div class="col-md-6">
                                                                <label> Guart Amount   Returned   </label>
                                                                <input type="number" class="form-control" name="guart_amount_returned" placeholder="Enter amount" value="{{$value->guart_amount_returned}}">
                                                            </div>

                                                            <div class="col-md-6">
                                                                <label> Currency  </label>
                                                                <select class="form-control size" name="guart_currency">
                                                                    <option value=""> Choose Currency </option>
                                                                    <option value="TSH">TSH</option>
                                                                    <option value="USD">USD </option>
                                                                </select>
                                                            </div>

                                                            <button class="btn btn-primary mx-3 my-4 nextBtn btn-lg pull-right" type="button" >Next</button>
                                                            <button class="btn btn-success  btn-lg pull-right my-4" type="submit" >Save</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                        <div class="row setup-content" id="step-4">
                                            <div class="col-xs-12">
                                                <div class="col-md-12">
                                                    <div class="col-md-6">
                                                        <form class="form container" method="POST" action="/now" enctype="multipart/form-data">
                                                            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                                            <div class="multi-field-wrapper2 ">
                                                                <div class="multi-fields2 ">
                                                                    <div class="row multi-field2 ">

                                                                        @if($value->type == 'LooseCargo')

                                                                            <label class="control-label" > Add New Chassis-no.</label>
                                                                        @else

                                                                            <label class="control-label" >   Add New Cont-no.</label>
                                                                        @endif
                                                                        <input type="hidden" class="form-control m-0 "   name="ladding_id" value="{{$value->id}}" />
                                                                        <input type="text" id="projectinput1" class="form-control my-5 m" name="cont_no[]" value="0" >
                                                                        <button type="button" class="remove-field2 btn btn-danger" style="height: 32px;width: 40px;margin: -1.2em 10px 0px 0px;">-</button>
                                                                        <button type="button" class="add-field2 btn btn-success" style="height: 32px;width: 40px;margin: -1.2em 10px 0px 0px;">+</button>
                                                                    </div>
                                                                </div>
                                                                <button type="submit" class="my-5  btn btn-warning" style="margin-left: -2rem;">Save New</button>
                                                                <button class="btn btn-primary mx-3 nextBtn btn-lg " type="button" >Next</button>

                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row setup-content" id="step-5">
                                            <div class="col-xs-12">
                                                <div class="col-md-12">
                                                    <h3>Completed</h3>
                                                    <p>You have successfully completed all steps.</p>
                                                    {{--<button  type="submit" class="btn btn-success btn-lg pull-right">Finish!</button>--}}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('layouts.footer')
@include('layouts.javas')
<script type="text/javascript">
    $(document).ready(function () {

        var navListItems = $('div.setup-panel div a'),
            allWells = $('.setup-content'),
            allNextBtn = $('.nextBtn');

        allWells.hide();

        navListItems.click(function (e) {
            e.preventDefault();
            var $target = $($(this).attr('href')),
                $item = $(this);

            if (!$item.hasClass('disabled')) {
                navListItems.removeClass('btn-primary').addClass('btn-default');
                $item.addClass('btn-primary');
                allWells.hide();
                $target.show();
                $target.find('input:eq(0)').focus();
            }
        });

        allNextBtn.click(function(){
            var curStep = $(this).closest(".setup-content"),
                curStepBtn = curStep.attr("id"),
                nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
                curInputs = curStep.find("input[type='text'],input[type='url']"),
                isValid = true;

            $(".form-group").removeClass("has-error");
            for(var i=0; i<curInputs.length; i++){
                if (!curInputs[i].validity.valid){
                    isValid = false;
                    $(curInputs[i]).closest(".form-group").addClass("has-error");
                }
            }

            if (isValid)
                nextStepWizard.removeAttr('disabled').trigger('click');
        });

        $('div.setup-panel div a.btn-primary').trigger('click');
    });
</script>
<script type="text/javascript">
    $(document).ready(function(){
        $('.declaration').on('change', function(e) {
            e.preventDefault();
            if (document.getElementById('declaration').value == 'Clear') {
                document.getElementById('amount').style.display = 'none';

            }
            else {
                document.getElementById('amount').style.display = 'block';;
            }
        });

    });
</script>

<script type="text/javascript">
    $(document).ready(function() {


        $('select[name="country_id"]').on('change', function() {
            var stateID = $(this).val();
            if(stateID) {
                $.ajax({
                    url: '/country_name/ajax/'+stateID,
                    type: "GET",
                    dataType: "json",
                    success:function(data) {


                        $('select[name="destination"]').empty();
                        $.each(data, function(key, value) {
                            $('select[name="destination"]').append('<option value="'+ value +'">'+ value +'</option>');
                        });


                    }
                });
            }else{
                $('select[name="destination"]').empty();
            }
        });
    });
</script>
</body>
</html>
