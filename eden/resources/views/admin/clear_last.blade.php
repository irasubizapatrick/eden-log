<!DOCTYPE html>
<html>

@include('layouts.admin_head')
<style>
    .form_size{
        height: calc(3.25rem + 2px) !Important;
    }
    .page-title-box{
        padding: 30px 0px 20px 0px !important;
    }
    #ladding_id{
        height: calc(3.25rem + 2px) !Important;
    }
</style>
<body>
@include('layouts.admin_sidebar')
<div class="wrapper">
    <div class="container-fluid">
        <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title mt-0" id="myLargeModalLabel">Upload Document</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body">
                        <form class="form-horizontal"  action="/clearance" method="POST" enctype="multipart/form-data">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                            <div class="row col-md-12">

                                <div class="col-md-6">
                                    <label>Bill of lading</label>
                                    <select class="form-control "  name="ladding_id"  id="ladding_id" >
                                        <option value=""> Choose</option>
                                        @foreach($ladding as $key => $all)
                                            <option value="{{ $key }}">{{$all}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-6">
                                    <label>Container Number</label>
                                    <select name="container_id " class="form-control form_size">
                                        <option value="">Container number</option>
                                    </select>
                                </div>
                                <div class="col-md-6">
                                    <label> Date </label>
                                    <input type="date"  name="clearing_date" class="form-control" >
                                </div>
                                <div class="col-md-6 ">
                                    <label>Tanzania Revenue Authority</label>
                                    <input type="file"  name="tra" class="form-control">
                                </div>
                                <div class="col-md-6">
                                    <label>Rwanda Revenue Authority charges</label>
                                    <input type="file"  name="rra" class="form-control">
                                </div>
                                <div class="col-md-6">
                                    <label>Other </label>
                                    <input type="file"  name="other_clearance_doc" class="form-control">
                                </div>
                                <div class="col-md-12 mx-auto pt-5 mb-5">
                                    <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Close</button>
                                    <button type="submit" class="btn btn-primary waves-effect waves-light">Save changes</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row mx-auto">
        <div class="col-sm-12">
            <div class="page-title-box mt-5 ">
                <div class="btn-group pull-right">
                    <ol class="breadcrumb hide-phone">
                        <li class="breadcrumb-item"><a href="#">Tax Clearance List</a></li>
                        <li href="/dashboard" class="breadcrumb-item active">Dashboard</li>
                    </ol>
                </div>
                <h4 class="page-title">All Tax Clearance</h4>
            </div>
        </div>
        <div class="col-12 container">
            <div class="card m-b-30">
                <div class="card-body">
                    <button type="button" class="btn btn-primary waves-effect waves-light pull-right" data-toggle="modal" data-target=".bs-example-modal-lg "><i class="fa fa-plus"></i> <span>Add Customs</span></button>
                    @if (Session::has('message'))
                        <div class="alert alert-success alert-dismissable">
                            <a href="" class="close" data-dismiss="alert" aria-label="close">
                                &times;

                            </a>
                            {{ Session::get('message') }}
                        </div>

                    @endif
                    @if (Session::has('delete'))
                        <div class="alert alert-success alert-dismissable">
                            <a href="" class="close" data-dismiss="alert" aria-label="close">
                                &times;

                            </a>
                            {{ Session::get('delete') }}
                        </div>

                    @endif
                    @if (Session::has('updated'))
                        <div class="alert alert-success alert-dismissable">
                            <a href="" class="close" data-dismiss="alert" aria-label="close">
                                &times;

                            </a>
                            {{ Session::get('updated') }}
                        </div>

                    @endif

                    <table id="datatable-buttons" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                        <tr style="font-size: 13px;background-color: #416b8d !important;color: #fff;">
                            <th>#</th>
                            <th>Date </th>
                            <th>Bill of lading</th>
                            <th>TRA</th>
                            <th>RRA</th>
                            <th>Actions</th>
                        </tr>
                        </thead>


                        <tbody>
                        <?php $i=0;?>
                        @foreach($archive AS $value)
                            <tr>
                                <td>{{$i+1}}</td>

                                <td> {{$value->clearing_date}}</td>
                                <td>{{$value->bill_lading}}</td>
                                <td>
                                    <a href="/tax_clearance/{{$value->tra}}"  target="_blank">file</a>

                                </td>

                                <td>
                                    <a href="/tax_clearance/{{$value->rra}}"  target="_blank">file</a>

                                </td>
                                <td>
                                    <button data-toggle="modal" data-target="#editleads<?php echo $i;?>" class="pull-left edit  btn-lg  btn btn-primary dlt_sm_table mx-2 mt-2"><i class="fa fa-pencil"></i></button>

                                    <button data-toggle="modal" data-target="#delete<?php echo $i;?>" class="pull-left edit  btn-lg  btn btn-danger dlt_sm_table mx-2 mt-2"><i class="fa fa-trash"></i></button>
                                    <div class="modal fade" id="delete<?php echo $i;?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLarge01" style="display: none;" aria-hidden="true">
                                        <div class="modal-dialog modal-lg" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title">Delete </h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">×</span>
                                                    </button>
                                                </div>
                                                <form class="pull-left" action="/clearance/{{$value->id}}" method="POST">
                                                    <label class="mx-4">Are you sure you want to delete</label>
                                                    <input type="hidden" name="_method" value="DELETE" />
                                                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                                    <div class="modal-footer">
                                                        <button type="submit" class="btn btn-success">Confirm</button>
                                                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="editleads<?php echo $i;?>" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
                                        <div class="modal-dialog modal-lg">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h3 class="modal-title" id="editleads"> Edit documents Details</h3>
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

                                                </div>
                                                <div class="modal-body">
                                                    <div class="row">
                                                        <form role="form-horizontal" action="/clearance/{{$value->id}}" method="post" enctype="multipart/form-data">
                                                            <input type="hidden" name="_method" value="PUT" />
                                                            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                                            <div class="row col-md-12">
                                                                <div class="col-md-6">
                                                                    <label> Bill of ladding</label>
                                                                    <select class="form-control" name="c" required style="height: 4rem;">
                                                                        @foreach($ladding as $test)
                                                                            <option value="{{$test->id}}">{{$test->bill_lading}}</option>
                                                                        @endforeach
                                                                    </select>

                                                                </div>
                                                                <div class="col-md-6 pull-left">
                                                                    <label> Date </label>
                                                                    <input type="date"  name="clearing_date" class="form-control" value="{{$value->clearing_date}}">
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <label>Tanzania Revenue Authority</label>
                                                                    <input type="file"  name="tra" class="form-control">
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <label>Rwanda Revenue Authority charges</label>
                                                                    <input type="file"  name="rra" class="form-control">
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <label>Other </label>
                                                                    <input type="file"  name="other_clearance_doc" class="form-control">
                                                                </div>

                                                                <div class="col-md-6 mx-auto">
                                                                    <div class="modal-footer ">
                                                                        <button type="submit" class="btn btn-primary waves-effect waves-light">Save changes</button>
                                                                        <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Close</button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </td>

                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>

</div>
</div>
@include('layouts.footer')
@include('layouts.javas')
<script type="text/javascript">
    $(document).ready(function() {
        $('select[name="ladding_id"]').on('change', function() {
            var stateID = $(this).val();
            if(stateID) {
                $.ajax({
                    url: '/add_status/ajax/'+stateID,
                    type: "GET",
                    dataType: "json",
                    success:function(data) {


                        $('select[name="container_id"]').empty();
                        $.each(data, function(key, value) {
                            $('select[name="container_id"]').append('<option value="'+ key +'">'+ value +'</option>');
                        });


                    }
                });
            }else{
                $('select[name="container_id"]').empty();
            }
        });
    });
</script>
</body>
</html>