<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    @include('layouts.title')
    <meta content="Admin Dashboard" name="description" />
    <meta content="Mikumi" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <!-- App Icons -->
    <link rel="shortcut icon" href="/assets/images/favicon.ico">

    <!-- DataTables -->
    <link href="/assets/plugins/datatables/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
    <link href="/assets/plugins/datatables/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css" />
    <!-- Responsive datatable examples -->
    <link href="/assets/plugins/datatables/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css" />

    <!-- App css -->
    <link href="/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="/assets/css/icons.css" rel="stylesheet" type="text/css" />
    <link href="/assets/css/style.css" rel="stylesheet" type="text/css" />

</head>

<style type="text/css">
    .has-submenu{
        padding: 0px 10px 0px 5px !important;
        font-weight: 700 !important;
    }
</style>
<body>

<!-- Navigation Bar-->
@include('layouts.admin_sidebar')
<!-- End Navigation Bar-->

<div class="wrapper">
    <div class="container-fluid">

        <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <div class="btn-group pull-right">
                        <ol class="breadcrumb hide-phone p-0 m-0">

                        </ol>


                    </div>
                </div>
            </div>
        </div>
        <!-- end page title end breadcrumb -->
        <div class="container-fluid">

            <div class="row">
                <div class="col-12">
                    <div class="card m-b-30">
                        <div class="card-body">

                            <div class="row">
                                <div class="col-md-12">
                                    <a role="button" class="btn btn-danger pull-right "  href="{{URL::previous()}}" style="margin-left: 1%;margin-top:  5%;"> Get back</a>

                                    <section id="cd-timeline" class="cd-container">
                                        @forelse($track as $value)
                                            <div class="cd-timeline-block">
                                                <div class="cd-timeline-img cd-success">
                                                    <i class="mdi mdi-adjust"></i>
                                                </div> <!-- cd-timeline-img -->

                                                <div class="cd-timeline-content">
                                                    <h3>{{$value->status}}</h3>
                                                    <p class="mb-0 text-muted font-14">{{$value->comment}}</p>
                                                    <span class="cd-date">{{$value->bill_loading}}</span>
                                                    <span class="cd-date">{{$value->updated_at}}</span>
                                                </div> <!-- cd-timeline-content -->
                                            </div> <!-- cd-timeline-block -->
                                        @empty
                                            <div class="alert alert-success" style="width: 96%;">
                                                <strong>Sorry !</strong> No Information Found   .
                                            </div>
                                        @endforelse


                                    </section> <!-- cd-timeline -->
                                </div>
                            </div><!-- Row -->

                        </div>
                    </div>
                </div> <!-- end col -->
            </div> <!-- end row -->

        </div>


    </div> <!-- end container -->
</div>
<!-- end wrapper -->


<!-- Footer -->
@include('layouts.footer')
<!-- End Footer -->


@include('layouts.javas')

</body>
</html>
<!-- Localized -->