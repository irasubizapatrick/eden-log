<!DOCTYPE html>
<html>

@include('layouts.admin_modal')
<style>
    .size{
        height: calc(3.25rem + -10px) !Important;
    }
    .select2{
        width: 100% !important;
        margin-top: -3rem;
    }
</style>
<body>
@include('layouts.admin_sidebar')
<div class="wrapper">
    <div class="container-fluid">
        <!-- end page title end breadcrumb -->
        <div class="modal fade bs-example-modal-lg"  role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title mt-0" id="myLargeModalLabel">Create  declaration Cargo type</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body">
                        <form class="form-horizontal"  action="/declaration/type" method="POST" enctype="multipart/form-data">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                            <div class="row col-md-12">
                                <div class="col-md-6 pull-left">
                                    <label> Cargo Declaration type </label>
                                    <input type="text"  name="cargo_declaration_type" placeholder="Enter Cargo type" class="form-control">
                                </div>
                                <div class="col-md-12 mx-auto pt-5 pb-5">
                                        <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Close</button>
                                        <button type="submit" class="btn btn-primary waves-effect waves-light">Save changes</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row m-0 p-0">
        <div class="col-sm-12">
            <div class="page-title-box">
                <div class="btn-group pull-right">
                    <ol class="breadcrumb hide-phone p-0 m-0">
                        <li class="breadcrumb-item"><a href="#">Declaration Cargo type List</a></li>
                        <li class="breadcrumb-item active">Dashboard</li>
                    </ol>
                </div>
                <h4 class="page-title">All Declaration Cargo type </h4>
            </div>
        </div>
        <div class="col-12 container" style="margin-top: -3rem;">
            <div class="card m-b-30">
                <div class="card-body">
                    <button type="button" class="btn btn-primary waves-effect waves-light pull-right" data-toggle="modal" data-target=".bs-example-modal-lg "><i class="fa fa-plus"></i> <span> Add </span></button>
                    @if (Session::has('message'))
                        <div class="alert alert-success alert-dismissable">
                            <a href="" class="close" data-dismiss="alert" aria-label="close">
                                &times;

                            </a>
                            {{ Session::get('message') }}
                        </div>

                    @endif
                    @if (Session::has('delete'))
                        <div class="alert alert-danger alert-dismissable">
                            <a href="" class="close" data-dismiss="alert" aria-label="close">
                                &times;

                            </a>
                            {{ Session::get('delete') }}
                        </div>

                    @endif
                    @if (Session::has('updated'))
                        <div class="alert alert-warning alert-dismissable">
                            <a href="" class="close" data-dismiss="alert" aria-label="close">
                                &times;

                            </a>
                            {{ Session::get('updated') }}
                        </div>

                    @endif

                    <table id="datatable-buttons" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                        <tr style="font-size: 13px;background-color: #416b8d !important;color: #fff;">
                            <th>#</th>
                            <th>Created by </th>
                            <th>Cargo Type </th>
                            <th>Actions</th>
                        </tr>
                        </thead>


                        <tbody>
                        <?php $i=1;?>
                        @foreach($declaration AS $value)
                            <tr>
                                <td>{{$i++}}</td>
                                <td>{{$value->user->name}}</td>
                                <td>{{$value->cargo_declaration_type}}</td>
                                <td>
                                    <button data-toggle="modal" data-target="#editleads<?php echo $i;?>" class="pull-left edit btn-lg  btn-primary dlt_sm_table mx-2 mt-2"><i class="fa fa-pencil"></i></button>

                                    <button data-toggle="modal" data-target="#delete<?php echo $i;?>" class="pull-left edit btn-lg  mx-2 mt-2 btn-danger dlt_sm_table"><i class="fa fa-trash"></i></button>
                                    <div class="modal fade" id="delete<?php echo $i;?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLarge01" style="display: none;" aria-hidden="true">
                                        <div class="modal-dialog modal-lg" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h4 class="modal-title">Delete </h4>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">×</span>
                                                    </button>
                                                </div>
                                                <form class="pull-left" action="/declaration/type/{{$value->id}}" method="POST">
                                                    <label class="mx-4">Are you sure you want to delete</label>
                                                    <input type="hidden" name="_method" value="DELETE" />
                                                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                                    <div class="modal-footer">
                                                        <button type="submit" class="btn btn-success">Confirm</button>
                                                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="editleads<?php echo $i;?>" class="modal fade" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
                                        <div class="modal-dialog modal-lg">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h4 class="modal-title" id="editleads"> Edit Declaration Cargo type </h4>
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

                                                </div>
                                                <div class="modal-body">
                                                    <div class="row">
                                                        <form role="form-horizontal" action="/declaration/type/{{$value->id}}" method="post" enctype="multipart/form-data">
                                                            <input type="hidden" name="_method" value="PUT" />
                                                            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                                            <div class="row col-md-12">
                                                                <div class="col-md-12">
                                                                    <label> Cargo Declaration type </label>
                                                                    <input type="text"  name="cargo_declaration_type" value="{{$value->cargo_declaration_type}}" placeholder="Enter Cargo type" class="form-control">
                                                                </div>
                                                                <div class="col-md-12 mx-auto pt-5 pb-5">
                                                                    <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Close</button>
                                                                    <button type="submit" class="btn btn-primary waves-effect waves-light">Save changes</button>
                                                                </div>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>

</div>
</div>
@include('layouts.footer')
@include('layouts.javas')

</body>
</html>