<!DOCTYPE html>
<html>
@include('layouts.admin_head')
<style type="text/css">
    .has-submenu{
        padding: 0px 10px 0px 33px !important;
    }
</style>

<body>
<!-- Navigation Bar-->
@include('layouts.finance_sidebar')
<!-- End Navigation Bar-->


<div class="wrapper">
    <div class="container-fluid">
        <!-- end page title end breadcrumb -->
        <div class="row pt-5">
            <div class="col-sm-12">
                <div class="page-title-box" style="padding: 35px 0px 20px 0px !important;">
                    <div class="btn-group pull-right">
                        <ol class="breadcrumb hide-phone p-0 m-0">
                            <li class="breadcrumb-item"><a href="/dashboard">Mikumi Freight Forwarders Ltd </a></li>
                            <li class="breadcrumb-item active">Dashboard</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Dashboard</h4>
                </div>
            </div>
            <div class="col-md-6 col-xl-3">
                <a href="/all_order">
                    <div class="mini-stat clearfix bg-white">
                        <span class="mini-stat-icon bg-light"><i class="mdi mdi-truck-delivery text-danger"></i></span>
                        <div class="mini-stat-info text-right text-muted">
                            <span class="counter text-danger">{{$order_count}}</span>
                            Total Orders
                        </div>
                    </div>
                </a>

            </div>

            <div class="col-md-6 col-xl-3">
                <a href="/all_agent">
                    <div class="mini-stat clearfix bg-success">
                        <span class="mini-stat-icon bg-light"><i class="mdi mdi-human text-success"></i></span>
                        <div class="mini-stat-info text-right text-white">
                            <span class="counter text-white">{{$agent_count}}</span>
                            Agents
                        </div>
                    </div>
                </a>
            </div>


            <div class="col-md-6 col-xl-3">
                <a href="/all/consignment/cross">
                    <div class="mini-stat clearfix bg-white">
                        <span class="mini-stat-icon bg-light"><i class="mdi mdi-border-outside text-warning"></i></span>
                        <div class="mini-stat-info text-right text-muted">
                            <span class="counter text-warning"> {{$crossed_count}}</span>
                            Current  Consignment Crossed Border
                        </div>
                    </div>
                </a>

            </div>
            <div class="col-md-6 col-xl-3">
                <a href="/all_order">
                    <div class="mini-stat clearfix bg-info">
                        <span class="mini-stat-icon bg-light"><i class="mdi mdi-currency-usd  text-info"></i></span>
                        <div class="mini-stat-info text-right text-light">
                            <span class="counter text-white">{{$expense_count}}</span>
                            Last 30 Days Consignment
                        </div>
                    </div>
                </a>

            </div>
        </div>
        <div class="row">
            <div class="col-xl-12">
                <div class="card m-b-30">
                    <div class="card-body">
                        <h4 class="mt-0 m-b-15 header-title">All Consignments Analytics </h4>

                        <div class="col-md-6">
                            <figure class="highcharts-figure">
                                <div id="container"></div>
                            </figure>
                        </div>
                        <div class="col-md-6">
                            <figure class="highcharts-figure">
                                <div id="container1"></div>
                            </figure>
                        </div>
                    </div>
                </div>

            </div>

        </div>

        <div class="row">
            <div class="col-xl-12">
                <div class="card m-b-30">
                    <div class="card-body">
                        <h4 class="mt-0 m-b-15 header-title">Recent Consignments </h4>

                        <div class="table-responsive">

                            <table id="datatable-buttons" class="table table-hover mb-0 table-striped table-bordered" cellspacing="0" width="100%" >
                                <thead>
                                <tr style="font-size: 13px;background-color: #416b8d !important;color: #fff;">
                                    <th>#</th>
                                    <th>Date</th>
                                    <th>File No</th>
                                    <th>Consignee</th>
                                    <th>BL</th>
                                    <th> Line/ Vessel </th>
                                    <th> C&F At Destination </th>
                                    <th>Shipping Line</th>
                                    <th>Shipper</th>
                                    <th>Destination</th>
                                    <th>Current Status</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $i=1;?>
                                @foreach($track AS $value)
                                    <tr style="font-size: 13px;">
                                        <td>{{$i++}}</td>
                                        <td>{{$value->date}} </td>
                                        <td>{{$value->file_no}} </td>
                                        <td>{{$value->client_name}}</td>
                                        <td>{{$value->bill_lading}} </td>
                                        <td>{{$value->line_vessel}}</td>
                                        <td>{{$value->organization_name}}</td>
                                        <td>{{$value->shipping_line}}</td>
                                        <td>{{$value->shipper}}</td>
                                        <td>{{$value->destination}}</td>
                                        <td>

                                            @if($value->current_status == 'Prepare for T1')

                                                <span class="label label-warning"> {{$value->current_status}}</span>

                                            @elseif($value->current_status == 'Crossed Border')

                                                <span class="label label-success"> {{$value->current_status}}</span>


                                            @elseif($value->current_status == 'Receiving the documents')

                                                <span class="label label-primary"> {{$value->current_status}} </span>

                                            @elseif($value->current_status == 'Under Customs Procedures')

                                                <span class="label label-warning"> {{$value->current_status}} </span>
                                            @else
                                                <span class="label label-info"> {{$value->current_status}}  </span>

                                            @endif

                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>

                        </div>

                    </div>
                </div>
            </div>

        </div>


        <div class="row">
            <div class="col-xl-12">
                <div class="card m-b-30">
                    <div class="card-body">

                        <div class="col-md-6">
                            <center>
                                <h4 class="mt-0 m-b-15 header-title pb-5 pt-5">Agent   Consignments Analytics  <br><span class="label label-success">File Closed</span> </h4>

                            </center>

                            <div class="table-responsive">

                                <table  class="table table-hover mb-0 table-striped table-bordered" cellspacing="0" width="100%" >
                                    <thead>
                                    <tr style="font-size: 13px;background-color: #416b8d !important;color: #fff;">
                                        <th>#</th>
                                        <th>Agent Name</th>
                                        <th>Phone</th>
                                        <th>No</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $i=1;?>
                                    @foreach($close_file AS $value)
                                        <tr style="font-size: 13px;">
                                            <td>{{$i++}}</td>
                                            <td>{{$value->organization_name}} </td>
                                            <td>{{$value->telephone}} </td>
                                            <td>
                                                @if($value->most  > '20')
                                                    <span class="label label-danger"> {{$value->most}}</span>
                                                @elseif($value->most <='10')
                                                    <span class="label label-success"> {{$value->most}}</span>
                                                @else
                                                    <span class="label label-warning"> {{$value->most}}</span>

                                                @endif


                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>

                            </div>
                        </div>
                        <div class="col-md-6">
                            <center>
                                <h4 class="mt-0 m-b-15 header-title pb-5 pt-5">Agent   Consignments Analytics  <br><span class="label label-warning">Ongoing Consignments</span> </h4>

                            </center>

                            <div class="table-responsive">

                                <table  class="table table-hover mb-0 table-striped table-bordered" cellspacing="0" width="100%" >
                                    <thead>
                                    <tr style="font-size: 13px;background-color: #416b8d !important;color: #fff;">
                                        <th>#</th>
                                        <th>Agent Name</th>
                                        <th>Phone</th>
                                        <th>No</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $i=1;?>
                                    @foreach($not_close_file AS $value)
                                        <tr style="font-size: 13px;">
                                            <td>{{$i++}}</td>
                                            <td>{{$value->organization_name}} </td>
                                            <td>{{$value->telephone}} </td>
                                            <td>
                                                @if($value->most  > '20')
                                                    <span class="label label-danger"> {{$value->most}}</span>
                                                @elseif($value->most <='10')
                                                    <span class="label label-success"> {{$value->most}}</span>
                                                @else
                                                    <span class="label label-warning"> {{$value->most}}</span>

                                                @endif


                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>

        <!-- end row -->

    </div> <!-- end container -->
</div>

@include('layouts.footer')


@include('layouts.javas')
<script type="text/javascript">
    $(document).ready(function () {
        var bar_other       = <?php echo $bar_other[0] ?>;
        var bar_truck       = <?php echo $bar_truck[0] ?>;
        var bar_pay_port    = <?php echo $bar_pay_port[0] ?>;
        var bar_port        = <?php echo $bar_port[0] ?>;
        var bar_under_line  = <?php echo $bar_under_line[0] ?>;
        var bar_received    = <?php echo $bar_received[0] ?>;
        var bar_free        = <?php echo $bar_free[0] ?>;
        var bar_warehouse   = <?php echo $bar_warehouse[0] ?>;
        var bar_unpacking   = <?php echo $bar_unpacking[0] ?>;
        var row_collumn     = <?php echo $row_collumn[0] ?>;
        var bar_cross     = <?php echo $bar_cross[0] ?>;

        Highcharts.chart('container', {
            chart: {
                type: 'column'
            },
            title: {
                text: 'Consignments Analytics  Based on Current Status'
            },
            xAxis: {
                type: 'category',
                labels: {
                    rotation: -45,
                    style: {
                        fontSize: '13px',
                        fontFamily: 'Verdana, sans-serif'
                    }
                }
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'View All'
                }
            },
            legend: {
                enabled: false
            },
            tooltip: {
                pointFormat: 'Consignments: <b>{point.y}</b>'
            },
            series: [{
                name: 'Consignments',
                data: [
                    ['Receiving Documents', bar_received],
                    ['Truck/ Cargo in route Position', bar_truck],
                    ['Pay Port Charges', bar_pay_port],
                    ['Lodge Port charges', bar_port],
                    ['Loading',row_collumn ],
                    ['Under shipping line Procedures',bar_under_line],
                    ['Unloaded ( Warehouse)', bar_warehouse],
                    ['Unpacking',bar_unpacking ],
                    ['Other', bar_other],
                    ['Crossed Border', bar_cross],
                    ['Container Free', bar_free]
                ],
                dataLabels: {
                    enabled: true,
                    rotation: -90,
                    color: '#FFFFFF',
                    align: 'right',
                    format: '{point.y}', // one decimal
                    y: 10, // 10 pixels down from the top
                    style: {
                        fontSize: '13px',
                        fontFamily: 'Verdana, sans-serif'
                    }
                }
            }]
        });



    });

</script>
<script>
    $(document).ready(function () {

        var bar_customs             = <?php echo $bar_customs ?>;
        var bar_t1                  = <?php echo $bar_t1 ?>;
        var bar_loading             = <?php echo $bar_loading ?>;
        var bar_cargo_arrived       = <?php echo $bar_cargo_arrived ?>;
        var bar_crossed             = <?php echo $bar_crossed ?>;
        var file_closed             = <?php echo $file_closed ?>;
        var receiving_doc             = <?php echo $receiving_doc ?>;

        //Another Charts

        // Build the chart
        Highcharts.chart('container1', {
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            title: {
                text: 'Consignments Chart Analytics  '
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point:y}</b>'
            },
            accessibility: {
                point: {
                    valueSuffix: '%'
                }
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false
                    },
                    showInLegend: true
                }
            },
            series: [{
                name: 'Consignments',
                colorByPoint: true,
                data: [{
                    name: 'Under Customs Procedures',
                    y: bar_customs,
                    sliced: true,
                    selected: true
                }, {
                    name: 'Loading',
                    color:"#ffa500",
                    y: bar_loading

                }, {
                    name: 'Prepare T1',
                    color:"#376b8f",
                    y: bar_t1
                }
                    , {
                        name: 'File  Closed',
                        color:"#6ed187",
                        y: file_closed
                    },{
                        name: 'Crossed The  Border',
                        color:"#00bfff",
                        y: bar_crossed
                    }

                    , {
                        name: 'Cargo Arrived',
                        color:"#000",
                        y: bar_cargo_arrived
                    },
                    {
                        name: 'Cargo Receiving Document',
                        color:"#378f6f",
                        y: receiving_doc
                    }]
            }]
        });
    });
</script>


</body>
</html>