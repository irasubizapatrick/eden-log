<!DOCTYPE html>
<html>
@include('layouts.agent_head')
<body>
@include('layouts.agent_sidebar')
<div class="wrapper">
    <div class="container-fluid">

        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box" style="padding: 67px 0px 20px 0px !important;">
                    <div class="btn-group pull-right">
                        <ol class="breadcrumb hide-phone p-0 m-0">
                            <li class="breadcrumb-item"><a href="/dashboard">{{$name}}</a></li>
                            <li class="breadcrumb-item active">Dashboard</li>
                        </ol>
                    </div>
                    <h4 class="page-title">  <span style="color: grey; font-size: large;">Welcome to  Dashboard </span></h4>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 col-xl-3">
                <a href="/my_orders">
                    <div class="mini-stat clearfix bg-white">
                        <span class="mini-stat-icon bg-light"><i class="mdi mdi-truck-delivery text-danger"></i></span>
                        <div class="mini-stat-info text-right text-muted">
                            <span class="counter text-danger">{{$order_count}}</span>
                            Total Orders
                        </div>
                    </div>
                </a>

            </div>

            <div class="col-md-6 col-xl-3">
                <a href="/track/loading">
                    <div class="mini-stat clearfix bg-success">
                        <span class="mini-stat-icon bg-light"><i class="mdi mdi-human text-success"></i></span>
                        <div class="mini-stat-info text-right text-white">
                            <span class="counter text-white">{{$track_loading_count}}</span>
                            Loading Cargo
                        </div>
                    </div>
                </a>
            </div>


            <div class="col-md-6 col-xl-3">
                <a href="/track/cargo">
                    <div class="mini-stat clearfix bg-white">
                        <span class="mini-stat-icon bg-light"><i class="mdi mdi-cube-outline text-warning"></i></span>
                        <div class="mini-stat-info text-right text-muted">
                            <span class="counter text-warning">{{$track_cargo_count}}</span>
                            Crossed Border
                        </div>
                    </div>
                </a>

            </div>
            <div class="col-md-6 col-xl-3">
                <a href="/track/t1">
                    <div class="mini-stat clearfix bg-info">
                        <span class="mini-stat-icon bg-light"><i class="mdi mdi-currency-usd  text-info"></i></span>
                        <div class="mini-stat-info text-right text-light">
                            <span class="counter text-white">{{$prepare_t1_count}}</span>
                            Prepare T1
                        </div>
                    </div>
                </a>

            </div>
        </div>
        <div class="row">
            <div class="col-xl-12">
                <div class="card m-b-30">
                    <div class="card-body">
                        <h4 class="mt-0 m-b-15 header-title">Recent Consignments</h4>

                        <div class="table-responsive">
                            <table id="datatable-buttons" class="table table-hover mb-0 table-striped table-bordered" cellspacing="0" width="100%" >
                                <thead>
                                <tr style="font-size: 13px;background-color: #416b8d !important;color: #fff;">
                                    <th>#</th>
                                    <th>Date</th>
                                    <th> Consignee Name</th>
                                    <th>Bill of lading</th>
                                    <th> Line/ Vessel </th>
                                    <th> C&F At Destination </th>
                                    <th>Shipping Line</th>
                                    <th>Shipper</th>
                                    <th>Destination</th>
                                    <th>Current Status</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $i=1;?>
                                @foreach($track AS $value)
                                    <tr style="font-size: 13px;">
                                        <td>{{$i++}}</td>
                                        <td>{{$value->date}} </td>
                                        <td>{{$value->client_name}}</td>
                                        <td>{{$value->bill_lading}} </td>
                                        <td>{{$value->line_vessel}}</td>
                                        <td>{{$value->organization_name}}</td>
                                        <td>{{$value->shipping_line}}</td>
                                        <td>{{$value->shipper}}</td>
                                        <td>{{$value->destination}}</td>
                                        <td>

                                            @if($value->current_status == 'Prepare for T1')

                                                <span class="label label-warning"> {{$value->current_status}}</span>

                                            @elseif($value->current_status == 'Crossed Border')

                                                <span class="label label-success"> {{$value->current_status}}</span>


                                            @elseif($value->current_status == 'Receiving the documents')

                                                <span class="label label-primary"> {{$value->current_status}} </span>

                                            @elseif($value->current_status == 'Under Customs Procedures')

                                                <span class="label label-warning"> {{$value->current_status}} </span>
                                            @else
                                                <span class="label label-info"> {{$value->current_status}}  </span>

                                            @endif

                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>

                        </div>

                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
@include('layouts.footer')
<script src="/assets/js/jquery.min.js"></script>
<script src="/assets/js/popper.min.js"></script>
<script src="/assets/js/bootstrap.min.js"></script>
<script src="/assets/js/modernizr.min.js"></script>
<script src="/assets/js/waves.js"></script>
<script src="/assets/js/jquery.slimscroll.js"></script>
<script src="/assets/js/jquery.nicescroll.js"></script>
<script src="/assets/js/jquery.scrollTo.min.js"></script>

<script src="/assets/js/initial.js-master/initial.js" type="text/javascript"></script>
<script>
    $('.profile').initial();
</script>
</body>
</html>