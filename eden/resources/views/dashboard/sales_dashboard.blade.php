<!DOCTYPE html>
<html>
@include('layouts.finance_head')

<body>
@include('layouts.sales_sidebar')
<div class="wrapper">
    <div class="container-fluid">
        <!-- end page title end breadcrumb -->
        <div class="row pt-5">
            <div class="col-sm-12">
                <div class="page-title-box" style="padding: 35px 0px 20px 0px !important;">
                    <div class="btn-group pull-right">
                        <ol class="breadcrumb hide-phone p-0 m-0">
                            <li class="breadcrumb-item"><a href="/dashboard">Mikumi Freight Forwarders Ltd </a></li>
                            <li class="breadcrumb-item active">Dashboard</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Dashboard</h4>
                </div>
            </div>
            <div class="col-md-6 col-xl-3">
                <a href="/all_order">
                    <div class="mini-stat clearfix bg-white">
                        <span class="mini-stat-icon bg-light"><i class="mdi mdi-truck-delivery text-danger"></i></span>
                        <div class="mini-stat-info text-right text-muted">
                            <span class="counter text-danger">{{$order_count}}</span>
                            Total Orders
                        </div>
                    </div>
                </a>

            </div>

            <div class="col-md-6 col-xl-3">
                <a href="/all_agent">
                    <div class="mini-stat clearfix bg-success">
                        <span class="mini-stat-icon bg-light"><i class="mdi mdi-human text-success"></i></span>
                        <div class="mini-stat-info text-right text-white">
                            <span class="counter text-white">{{$agent_count}}</span>
                            Agents
                        </div>
                    </div>
                </a>
            </div>


            <div class="col-md-6 col-xl-3">
                <a href="/all_order">
                    <div class="mini-stat clearfix bg-white">
                        <span class="mini-stat-icon bg-light"><i class="mdi mdi-cube-outline text-warning"></i></span>
                        <div class="mini-stat-info text-right text-muted">
                            <span class="counter text-warning">0</span>
                            New Orders
                        </div>
                    </div>
                </a>

            </div>
            <div class="col-md-6 col-xl-3">
                <a href="#">
                    <div class="mini-stat clearfix bg-info">
                        <span class="mini-stat-icon bg-light"><i class="mdi mdi-currency-usd  text-info"></i></span>
                        <div class="mini-stat-info text-right text-light">
                            <span class="counter text-white">Messages</span>
                            0
                        </div>
                    </div>
                </a>

            </div>
        </div>


        <div class="row">
            <div class="col-xl-12">
                <div class="card m-b-30">
                    <div class="card-body">
                        <h4 class="mt-0 m-b-15 header-title">Recent Order</h4>

                        <div class="table-responsive">

                            <table id="datatable-buttons" class="table table-hover mb-0 table-striped table-bordered" cellspacing="0" width="100%" >
                                <thead>
                                <tr style="font-size: 13px;background-color: #416b8d !important;color: #fff;">
                                    <th>#</th>
                                    <th>Date</th>
                                    <th>File No</th>
                                    <th>Client Name /Code Consignee</th>
                                    <th>Bill of lading</th>
                                    <th> Line/ Vessel </th>
                                    <th> C&F At Destination </th>
                                    <th>Shipping Line</th>
                                    <th>Shipper</th>
                                    <th>Destination</th>
                                    {{--<th>View more</th>--}}
                                    {{--<th>Updates</th>--}}
                                </tr>
                                </thead>
                                <tbody>
                                <?php $i=1;?>
                                @foreach($track AS $value)
                                    <tr style="font-size: 13px;">
                                        <td>{{$i++}}</td>
                                        <td>{{$value->date}} </td>
                                        <td>{{$value->file_no}} </td>
                                        <td>{{$value->client_name}}</td>
                                        <td>{{$value->bill_lading}} </td>
                                        <td>{{$value->line_vessel}}</td>
                                        <td>{{$value->organization_name}}</td>
                                        <td>{{$value->shipping_line}}</td>
                                        <td>{{$value->shipper}}</td>
                                        <td>{{$value->destination}}</td>

                                        {{--<td>--}}
                                        {{--<a href="{{route ('order_details', ['id' =>$value->id])}}"  role="button" class="delete btn btn-sm btn-primary  "><i class="mdi mdi-eye"></i> view more</a>--}}

                                        {{--</td>--}}
                                        {{--<td>--}}
                                        {{--<a href="{{route ('order_status', ['id' =>$value->id])}}"  role="button" class="delete btn btn-sm btn-success">Change Status</a>--}}

                                        {{--</td>--}}
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>

                        </div>

                    </div>
                </div>
            </div>

        </div>
        <!-- end row -->

    </div> <!-- end container -->
</div>
<!-- end wrapper -->


<!-- Footer -->
@include('layouts.footer')
<!-- End Footer -->


@include('layouts.dash_footer')

</body>
</html>
<!-- Localized -->