<!DOCTYPE html>
<html>
@include('layouts.finance_head')

<body>
@include('layouts.shipping_sidebar')
<div class="wrapper">
    <div class="container-fluid">
        <div class="row pt-5">
            <div class="col-sm-12">
                <div class="page-title-box" style="padding: 35px 0px 20px 0px !important;">
                    <div class="btn-group pull-right" >
                        <ol class="breadcrumb hide-phone p-0 m-0">
                            <li class="breadcrumb-item"><a href="/dashboard">Mikumi Freight Forwarders Ltd</a></li>
                            <li class="breadcrumb-item active">Dashboard</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Dashboard</h4>
                </div>
            </div>
        </div>


        <div class="row">
            <div class="col-xl-12">
                <div class="card m-b-30">
                    <div class="card-body">
                        <h4 class="mt-0 m-b-15 header-title">All Consignments </h4>

                        <div class="table-responsive">

                            <table id="datatable-buttons" class="table table-hover mb-0 table-striped table-bordered" cellspacing="0" width="100%" >
                                <thead>
                                <tr style="font-size: 13px;background-color: #416b8d !important;color: #fff;">
                                    <th>#</th>
                                    <th>Date</th>
                                    <th>File No</th>
                                    <th> Consignee</th>
                                    <th>Bill of lading</th>
                                    <th> Line/ Vessel </th>
                                    <th> C&F At Destination </th>
                                    <th>Shipping Line</th>
                                    <th>Shipper</th>
                                    <th>Destination</th>
                                    <th>Current Status</th>
                                    {{--<th>Updates</th>--}}
                                </tr>
                                </thead>
                                <tbody>
                                <?php $i=1;?>
                                @foreach($track AS $value)
                                    <tr style="font-size: 13px;">
                                        <td>{{$i++}}</td>
                                        <td>{{$value->date}} </td>
                                        <td>{{$value->file_no}} </td>
                                        <td>{{$value->client_name}}</td>
                                        <td>{{$value->bill_lading}} </td>
                                        <td>{{$value->line_vessel}}</td>
                                        <td>{{$value->organization_name}}</td>
                                        <td>{{$value->shipping_line}}</td>
                                        <td>{{$value->shipper}}</td>
                                        <td>{{$value->destination}}</td>
                                        <td>

                                            @if($value->current_status == 'Prepare for T1')

                                                <span class="label label-warning"> {{$value->current_status}}</span>

                                            @elseif($value->current_status == 'Crossed Border')

                                                <span class="label label-success"> {{$value->current_status}}</span>


                                            @elseif($value->current_status == 'Receiving the documents')

                                                <span class="label label-primary"> {{$value->current_status}} </span>

                                            @elseif($value->current_status == 'Under Customs Procedures')

                                                <span class="label label-warning"> {{$value->current_status}} </span>
                                            @else
                                                <span class="label label-info"> {{$value->current_status}}  </span>

                                            @endif

                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>

                        </div>

                    </div>
                </div>
            </div>

        </div>

    </div>
</div>
@include('layouts.footer')

@include('layouts.dash_footer')

</body>
</html>