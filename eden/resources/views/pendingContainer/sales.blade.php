<!DOCTYPE html>
<html>

@include('layouts.finance_head')
<style type="text/css">
    .form_size{
        height: calc(3.25rem + 2px) !Important;
    }
    .select2{
        width: 100% !important;
        margin-top: -4rem;
    }
    .dataTables_paginate {
        display: none;
    }
</style>
<body>
@include('layouts.sales_sidebar')


<div class="wrapper">
    <div class="container-fluid">


    </div>
    <div class="row m-0  p-3">
        <div class="col-sm-12 ">
            <div class="page-title-box">
                <div class="btn-group pull-right">
                    <ol class="breadcrumb hide-phone p-0 m-0">
                        <li class="breadcrumb-item"><a href="#">All Pending Containers List</a></li>
                        <li class="breadcrumb-item active">Dashboard</li>
                    </ol>
                </div>
                <h6 class="page-title">All Pending Containers  </h6>
            </div>
        </div>
        <div class="col-12 container" style="margin-top: -3rem;">
            <div class="card m-b-30 ">
                <div class="card-body">
                    <div class="modal fade bill_lading"  role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title mt-0" id="myLargeModalLabel">Search By Container </h4>
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                </div>
                                <div class="modal-body">
                                    <form class="form-horizontal"  action="/search/container/pending" method="POST" enctype="multipart/form-data">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                        <div class="row col-md-12">
                                            <div class="col-md-6">
                                                <label>Container Number</label>
                                                <select class="form-control form_size" name="container_id" id="ladding_id">
                                                    <option value="">Container number</option>
                                                    @foreach($cont_bill as $key)
                                                        <option value="{{$key->id}}">{{$key->cont_no}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="col-md-6">
                                                <label>C&F</label>
                                                <select class="form-control form_size edit_ladding_id" name="cf" >
                                                    <option value="">Container number</option>
                                                    @foreach($agent as $all)
                                                        <option value="{{$all->id}}">{{$all->organization_name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>

                                            <div class="col-md-6">
                                                <label>Country</label>
                                                <select class="form-control mt-5  country" name="country_id">
                                                    <option value=""> Choose </option>
                                                    @foreach($country as $key => $value)
                                                        <option value="{{ $key }}">{{$value}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="col-md-6">
                                                <label> Destination </label>
                                                <select class="form-control mt-5  destination" name="destination"  >
                                                    <option value="">Select </option>
                                                </select>
                                                </select>
                                            </div>

                                            <div class="col-md-12 pt-4 pb-4">
                                                <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
                                                <button type="submit" class="btn btn-primary waves-effect waves-light">Search </button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                    <a href="/pending/container" role="button" class="btn btn-success waves-effect waves-light pull-right mx-3"><i class="fa fa-home"></i><span> All</span></a>

                    <button type="button" class="btn btn-warning waves-effect waves-light pull-right mx-4" data-toggle="modal" data-target=".bill_lading "><i class="fa fa-search"></i><span>Search </span></button>

                    @if (Session::has('message'))
                        <div class="alert alert-success alert-dismissable">
                            <a href="" class="close" data-dismiss="alert" aria-label="close">
                                &times;

                            </a>
                            {{ Session::get('message') }}
                        </div>

                    @endif
                    @if (Session::has('delete'))
                        <div class="alert alert-success alert-dismissable">
                            <a href="" class="close" data-dismiss="alert" aria-label="close">
                                &times;

                            </a>
                            {{ Session::get('delete') }}
                        </div>

                    @endif
                    @if (Session::has('updated'))
                        <div class="alert alert-success alert-dismissable">
                            <a href="" class="close" data-dismiss="alert" aria-label="close">
                                &times;

                            </a>
                            {{ Session::get('updated') }}
                        </div>

                    @endif
                    <table id="datatable-buttons" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                        <tr style="font-size: 13px;background-color: #416b8d !important;color: #fff;">
                            <th>#</th>
                            <th>Bill of Loading</th>
                            <th>Container No</th>
                            <th>Status </th>
                            <th>C&F</th>
                            <th>Destination</th>
                            <th>Last Update </th>
                        </tr>
                        </thead>


                        <tbody>
                        <?php $i=1;?>
                        @foreach($pending AS $value)
                            <tr>
                                <td>{{$i++}}</td>

                                <td>{{$value->bill_lading}}</td>
                                <td>{{$value->cont_no}}</td>
                                <td>

                                    @if($value->current_status == 'Prepare for T1')

                                        <span class="label label-warning"> {{$value->current_status}}</span>

                                    @elseif($value->current_status == 'Crossed Border')

                                        <span class="label label-success"> {{$value->current_status}}</span>


                                    @elseif($value->current_status == 'Receiving the documents')

                                        <span class="label label-primary"> {{$value->current_status}} </span>

                                    @elseif($value->current_status == 'Under Customs procedures')

                                        <span class="label label-warning"> {{$value->current_status}} </span>
                                    @else
                                        <span class="label label-info"> {{$value->current_status}}  </span>

                                    @endif

                                </td>
                                <td>
                                    {{$value->organization_name}}
                                </td>
                                <td>
                                    {{$value->destination}}
                                </td>
                                <td>{{$value->updateDate}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <span class="float-right">
                        <?php echo $pending->appends(Request::all())->fragment('foo')->render(); ?>

                     </span>

                </div>
            </div>
        </div>
    </div>
</div>
</div>
@include('layouts.footer')

@include('layouts.javas')

<script type="text/javascript">
    $(document).ready(function() {


        $('select[name="country_id"]').on('change', function() {
            var stateID = $(this).val();
            if(stateID) {
                $.ajax({
                    url: '/country_name/ajax/'+stateID,
                    type: "GET",
                    dataType: "json",
                    success:function(data) {


                        $('select[name="destination"]').empty();
                        $.each(data, function(key, value) {
                            $('select[name="destination"]').append('<option value="'+ value +'">'+ value +'</option>');
                        });


                    }
                });
            }else{
                $('select[name="destination"]').empty();
            }
        });
    });
</script>

</body>
</html>