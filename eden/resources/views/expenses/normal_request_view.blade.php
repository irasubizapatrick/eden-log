<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<style>
    body {
        background: #ffffff;
        margin-top: 120px;
        margin-bottom: 120px;
    }
    .p5{
        padding: 3rem 3rem 0rem 3rem;
    }

    @media print {
        #print_button {
            display: none;
        }
        .print_button {
            display: none;
        }
        letter_header{
            margin-left: 1rem;
        }
        .cargo_name{
            font-weight: 900 !important;
        }
        .company_details{

            margin-left: 82px !important;
            color: black !important;
        }
    }
</style>
<div class="container">
    <div class="row">
        <div class="col-sm-12 p-3">
            <div class="page-title-box">

                <a href="{{URL::previous()}}" class="btn btn-danger print_button"> <i class="fa fa-arrow-left"></i> Back</a>
                <a href="#" id="print_button" class="btn btn-success" onclick="window.print()" >
                    <i class="fa fa-print"></i>
                    Print
                </a>
            </div>
        </div>
        @foreach($expenses as $value)
            <div class="col-12">
                <div class="card">
                    <div class="card-body p-0">
                        <div class="row p5">
                            <div class="col-md-6">
                                <p class="font-weight-bold mb-1">MIKUMI FREIGHT FORWARDERS LTD</p>
                                <p class="mb-1">Harbour view Tower , 8 Floor room no , 808</p>
                                <p class="mb-1">Samora Avenue</p>
                                <p class="mb-1">P.O.BOX  9163</p>
                                <p class="mb-1">Dar Salaam</p>
                                <p class="mb-1">+255 714 3073 15</p>
                            </div>
                            <div class="col-md-6">
                                {{--<img src="http://via.placeholder.com/400x90?text=logo">--}}
                            </div>
                            <div class="col-md-12">
                                <h4 class="text-center font-weight-bold">REQUISITION FORM</h4>
                            </div>
                            <div class="col-md-6 my-3">
                                <p>Date :  {{$value->expenses_date}}</p>
                                <p>Payee : {{$value->payee}}</p>
                            </div>
                        </div>

                        <hr >

                        <div class="row p-3">
                            <div class="col-md-12">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th class="border-0 text-uppercase small font-weight-bold">SN</th>
                                        <th class="border-0 text-uppercase small font-weight-bold">Particulars</th>
                                        <th class="border-0 text-uppercase small font-weight-bold">Quantity</th>
                                        <th class="border-0 text-uppercase small font-weight-bold">Amount  </th>
                                        <th class="border-0 text-uppercase small font-weight-bold">Total</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>1</td>
                                        <td>{{$value->more_info}} </td>
                                        <td>{{$value->quantity_expenses}}</td>
                                        <td>{{$value->amount_requested}} / {{$value->amount_currency}}</td>
                                        <td>{{$value->amount_requested}} / {{$value->amount_currency}}</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="col-md-6 my-5 p5">
                            <h6>Amount in Words  :  {{$value->amount_words}}</h6>
                        </div>
                        <div class="col-md-6 my-5 p5">
                            <h6>Requested By  </h6>
                            <h6>Name {{$value->user->name}}</h6>
                            <h6>Signature ...........</h6>
                            <h6>Date {{$value->created_at}}</h6>
                        </div>

                        <div class="col-md-6 my-5 p5 pb-5">
                            <h6>Approved By  </h6>
                            <h6>Name {{$value->staff->name}}</h6>
                            <h6>Signature ............</h6>
                            <h6>Date {{$value->created_at}}</h6>
                        </div>
                    </div>
                </div>
            </div>

        @endforeach
    </div>
</div>


