<!DOCTYPE html>
<html>
@include('layouts.super_head')
<style type="text/css">
    .form_size {
        height: calc(3.25rem + 2px) !important;
    }
    .size {
        height: calc(1rem + 2px) !important;
    }
    .select2{
        width: 100% !important;
        margin-top: -1rem;
    }
    /*.all{*/
    /*height: calc(4.25rem + 2px) !important*/
    /*}*/
    .has-submenu
    {
        padding: 0px 16px 0px 30px !important;
    }
    .dataTables_paginate{
        display: none !important;
    }

</style>
<body>

<!-- Navigation Bar-->
@include('layouts.super_sidebar')
<!-- End Navigation Bar-->
<div class="wrapper">
    <div class="row mx-auto">
        <div class="col-sm-12 ">
            <div class="page-title-box">
                <div class="btn-group pull-right">
                    <ol class="breadcrumb hide-phone p-0 m-0">
                        <li class="breadcrumb-item"><a href="#">All Driver List</a></li>
                        <li class="breadcrumb-item active">Dashboard</li>
                    </ol>
                </div>
                <h4 class="page-title">All Consignment Expenses  </h4>
            </div>
        </div>
        <div class="col-xl-12" style="margin-top: -3rem;">

            <div class="card ">
                <div class="card-body col-xl-12">
                    <div class="table-responsive col-xl-12">
                        @if (Session::has('message'))
                            <div class="alert alert-success alert-dismissable">
                                <a href="" class="close" data-dismiss="alert" aria-label="close">
                                    &times;

                                </a>
                                {{ Session::get('message') }}
                            </div>

                        @endif
                        @if (Session::has('delete'))
                            <div class="alert alert-success alert-dismissable">
                                <a href="" class="close" data-dismiss="alert" aria-label="close">
                                    &times;

                                </a>
                                {{ Session::get('delete') }}
                            </div>

                        @endif
                        @if (Session::has('updated'))
                            <div class="alert alert-success alert-dismissable">
                                <a href="" class="close" data-dismiss="alert" aria-label="close">
                                    &times;

                                </a>
                                {{ Session::get('updated') }}
                            </div>

                        @endif
                        <div class="modal fade bill_lading"  role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title mt-0 mx-3" id="myLargeModalLabel">Search By Container </h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    </div>
                                    <div class="modal-body">
                                        <form class="form-horizontal"  action="/search/consignment" method="POST" enctype="multipart/form-data">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                            <div class="row col-md-12">

                                                <div class="col-md-6">
                                                    <label>Bill of lading </label>
                                                    <select class="form-control size" name="bill_lading" id="bl">
                                                        <option value="">  Enter Bill of lading  </option>
                                                        @foreach($data as $value)
                                                            <option value="{{$value->id}}">{{$value->bill_lading}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="col-md-6">
                                                    <label>Container Number</label>
                                                    <select class="form-control size" name="container_id" id="search_ladding_id">
                                                        <option value="">Container number</option>
                                                        @foreach($cont_bill as $key)
                                                            <option value="{{$key->id}}">{{$key->cont_no}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="col-md-6">
                                                    <label>Consignee</label>
                                                    <select class="form-control size" name="client_name" id="consignee">
                                                        <option value=""> Choose Consignee</option>
                                                        @foreach($track as $value)
                                                            <option value="{{$value->client_name}}">{{$value->client_name}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="col-md-12 pt-5 pb-5">
                                                    <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
                                                    <button type="submit" class="btn btn-primary waves-effect waves-light">Search </button>
                                                </div>
                                            </div>
                                        </form>

                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="modal fade bs-example-modal-lg"  role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title mt-0" id="myLargeModalLabel"> Request Requisition Form  </h4>
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    </div>
                                    <div class="modal-body">
                                        <form class="form-horizontal"  action="/expenses/form" method="POST" enctype="multipart/form-data">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                            <div class="row container  pt-3">
                                                <div class="col-md-6 ">
                                                    <label>Bill of lading</label>
                                                    <select class="form-control form_size " name="ladding_id" id="ladding_id" required style="margin-top: -8% !important;">
                                                        <option value="">Select Bill of lading</option>
                                                        @foreach($ladding as $key => $value)
                                                            <option value="{{ $key }}">{{$value}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="col-md-6">
                                                    <label>Container no / Chassis no</label>
                                                    <select name="cont_no" class="form-control form_size">
                                                        <option value="">Container number</option>
                                                    </select>
                                                </div>

                                                <div class="col-md-6">
                                                    <label>Date</label>
                                                    <input type="date" name="expenses_date" class="form-control"  required>
                                                </div>
                                                <div class="col-md-6">
                                                    <label>Payee</label>
                                                    <input type="text" name="payee" class="form-control" placeholder="Enter payee name"  required>
                                                </div>
                                                <div class="col-md-6">
                                                    <label>Amount Request</label>
                                                    <input type="number" name="amount_requested" class="form-control" placeholder="Enter amount" required>
                                                </div>
                                                <div class="col-md-6">
                                                    <label>Currency type</label>
                                                    <select name="amount_currency" class="form-control form_size" required>
                                                        <option value=""> Choose</option>
                                                        <option value="TZS"> TZS </option>
                                                        <option value="USD"> USD </option>
                                                    </select>
                                                </div>
                                                <div class="col-md-12">
                                                    <label>Activity  </label>
                                                    <select class="form-control  expenses" name="reason" required>
                                                        <option value=""> Choose</option>
                                                        <option value="Port charges"> Port charges </option>
                                                        <option value="Shipping line charges"> Shipping line charges </option>
                                                        <option value="Container charges/deposit"> Container charges/deposit </option>
                                                        <option value="Transport cost"> Transport cost </option>
                                                        <option value="Stamp / miscellaneous"> Stamp / miscellaneous </option>
                                                        <option value="TANROAD charges"> TANROAD charges </option>
                                                        <option value="TRA charges"> TRA charges </option>
                                                        <option value="Freight and delivery "> Freight and delivery  </option>
                                                        <option value="Fuel"> Fuel </option>
                                                        <option value="Stationary and printing"> Stationary and printing</option>
                                                        <option value="Commissions and fees"> Commissions and fees</option>
                                                        <option value="Facilitation expenses"> Facilitation expenses</option>
                                                        <option value="Insurance charges"> Insurance charges</option>
                                                        <option value="Repair and Maintenance"> Repair and Maintenance</option>
                                                        <option value="Travel expenses"> Travel expenses</option>
                                                        <option value="Chemical Permit"> Chemical Permit </option>
                                                        <option value="Demurage"> Demurage </option>
                                                        <option value="Amendment charges"> Amendment charges </option>
                                                    </select>
                                                </div>
                                                <div class="col-md-12">
                                                    <label>Amount In Words</label>
                                                    <textarea placeholder="Enter amount in words " class="form-control" name="amount_words" required ></textarea>
                                                </div>
                                                <div class="col-md-12">
                                                    <label>Description</label>
                                                    <textarea class="form-control" name="more_info" placeholder="Enter more details " ></textarea>
                                                </div>
                                                <div class="col-md-12 mt-5 mb-5">
                                                    <center>
                                                        <button type="reset" class="btn btn-danger waves-effect" data-dismiss="modal">Close</button>
                                                        <button type="submit" class="btn btn-primary waves-effect waves-light">Submit </button>
                                                    </center>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal fade search"  role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title mt-0 mx-4" id="myLargeModalLabel"> Advanced Search   </h4>
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    </div>
                                    <div class="modal-body">
                                        <form class="form-horizontal"  action="/general/expenses" method="POST" enctype="multipart/form-data">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                            <div class="row container  pt-3">
                                                <div class="col-md-6">
                                                    <label> From</label>
                                                    <input type="date" name="date1" class="form-control"  >
                                                </div>
                                                <div class="col-md-6">
                                                    <label> To </label>
                                                    <input type="date" name="date2" class="form-control"   >
                                                </div>
                                                <div class="col-md-6">
                                                    <label>Activity  </label>
                                                    <select class="form-control  expenses" name="activity"  >
                                                        <option value=""> Choose</option>
                                                        <option value="Port charges"> Port charges </option>
                                                        <option value="Shipping line charges"> Shipping line charges </option>
                                                        <option value="Container charges/deposit"> Container charges/deposit </option>
                                                        <option value="Transport cost"> Transport cost </option>
                                                        <option value="Stamp / miscellaneous"> Stamp / miscellaneous </option>
                                                        <option value="TANROAD charges"> TANROAD charges </option>
                                                        <option value="TRA charges"> TRA charges </option>
                                                        <option value="Freight and delivery "> Freight and delivery  </option>
                                                        <option value="Fuel"> Fuel </option>
                                                        <option value="Stationary and printing"> Stationary and printing</option>
                                                        <option value="Commissions and fees"> Commissions and fees</option>
                                                        <option value="Facilitation expenses"> Facilitation expenses</option>
                                                        <option value="Insurance charges"> Insurance charges</option>
                                                        <option value="Repair and Maintenance"> Repair and Maintenance</option>
                                                        <option value="Travel expenses"> Travel expenses</option>

                                                    </select>
                                                </div>
                                                <div class="col-md-12 mt-5 mb-5">
                                                    <button type="reset" class="btn btn-danger waves-effect" data-dismiss="modal">Close</button>
                                                    <button type="submit" class="btn btn-success waves-effect waves-light">Search </button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <a href="/expenses" role="button" type="button" class="btn btn-success waves-effect waves-light pull-right mx-1" ><i class="fa fa-home" style="color: #ffffff;"></i><span style="color: #ffffff;"> All  </span></a>

                        <button type="button" class="btn btn-primary waves-effect waves-light pull-right mx-3" data-toggle="modal" data-target=".bs-example-modal-lg "><i class="fa fa-plus"></i><span> Add Request  </span></button>
                        <button type="button" class="btn btn-warning waves-effect waves-light pull-right mx-4" data-toggle="modal" data-target=".search "><i class="fa fa-search"></i><span style="color: #ffffff;"> Advanced  Search  </span></button>

                        <button type="button" class="btn btn-info waves-effect waves-light pull-right mx-4" data-toggle="modal" data-target=".bill_lading "><i class="fa fa-search"></i><span>Search </span></button>

                        <table id="datatable-buttons" class="table table-hover table-striped table-bordered" cellspacing="0" width="100%" >
                            <thead>
                            <tr style="font-size: 13px;background-color: #416b8d !important;color: #fff;">
                                <th>#</th>
                                <th>Date</th>
                                <th>BL</th>
                                <th>Cont no</th>
                                <th>Consignee</th>
                                {{--<th>C&F</th>--}}
                                <th>Requested  </th>
                                <th>Payee</th>
                                <th>TZS</th>
                                <th>USD</th>
                                <th>Description</th>
                                <th>Finance </th>
                                <th>Admin </th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $i=1;?>
                            @foreach($expenses_data AS $value)
                                <tr>
                                    <td>{{$i++}}</td>
                                    <td>{{$value->expenses_date}}</td>
                                    <td>{{$value->bill_lading}}</td>
                                    <td>{{$value->cont_no}}</td>
                                    <td>{{$value->client_name}}</td>
{{--                                    <td>{{$value->container->lad->track->agent->organization_name}}</td>--}}
                                    <td>{{$value->user->name}}  </td>
                                    <td>{{$value->payee}}</td>
                                    <td><span class="amountTZS">{{$value->currency_tzs}}</span> </td>
                                    <td> <span class="amountUSD">{{$value->currency_usd}}</span></td>
                                    <td>{{$value->more_info}} / {{$value->reason}}</td>
                                    <td>
                                        @if($value->finance_status == 'rejected')

                                            <span class="label label-danger"> {{$value->finance_status}}</span>
                                        @elseif($value->finance_status == 'pending')

                                            <span class="label label-warning"> {{$value->finance_status}}</span>

                                        @else
                                            <span class="label label-success"> {{$value->finance_status}} </span>

                                        @endif

                                    </td>
                                    <td>

                                        @if($value->super_status == 'wait')

                                            <span class="label label-info"> {{$value->super_status}}</span>
                                        @elseif($value->super_status == 'rejected')

                                            <span class="label label-danger"> {{$value->super_status}}</span>
                                        @elseif($value->super_status == 'pending')

                                            <span class="label label-warning"> {{$value->super_status}}</span>

                                        @else
                                            <span class="label label-success"> {{$value->super_status}} </span>

                                    @endif

                                    <td>
                                        <button data-toggle="modal" data-target="#editleads<?php echo $i;?>" class="pull-left edit  btn-lg  btn btn-primary dlt_sm_table mx-2 mt-2" style="width: 37px"><i class="fa fa-pencil"></i></button>

                                        <div id="editleads<?php echo $i;?>" class="modal fade" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
                                            <div class="modal-dialog modal-lg">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h4 class="modal-title" id="editleads"> Edit Requisition Form</h4>
                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

                                                    </div>
                                                    <div class="modal-body">
                                                        <div class="row">
                                                            <form role="form" action="/expenses/{{$value->id}}" method="post" enctype="multipart/form-data">
                                                                <input type="hidden" name="_method" value="PUT" />
                                                                <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                                                <div class="row col-md-12">
                                                                    <div class="col-md-6">
                                                                        <label>Payee</label>
                                                                        <input type="text" name="payee" value="{{$value->payee}}" class="form-control" placeholder="Enter payee name"  >
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                        <label>Amount Request</label>
                                                                        <input type="number" name="amount_requested" value="{{$value->amount_requested}}" class="form-control" placeholder="Enter amount" >
                                                                    </div>


                                                                    <div class="col-md-6">
                                                                        <label>Currency type</label>
                                                                        <select name="amount_currency" class="form-control form_size" >
                                                                            <option value="{{$value->amount_currency}}"> {{$value->amount_currency}}</option>
                                                                            <option value="TZS"> TZS </option>
                                                                            <option value="USD"> USD </option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                        <label>Status</label>
                                                                        <select name="finance_status" class="form-control form_size" required>


                                                                            @if($value->super_status == 'rejected')
                                                                                <option > canceled by super admin</option>

                                                                            @elseif($value->super_status == 'Authorized')
                                                                                <option > voucher closed</option>
                                                                            @else
                                                                                <option value=""> Choose</option>
                                                                                <option value="pending">pending</option>
                                                                                <option value="rejected"> reject </option>
                                                                                <option value="approved"> Approve </option>
                                                                            @endif
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-md-12">
                                                                        <label>Activity  </label>
                                                                        <select class="form-control expenses" name="reason" required>
                                                                            <option value="{{$value->reason}}"> {{$value->reason}}</option>
                                                                            <option value="Port charges"> Port charges </option>
                                                                            <option value="Shipping line charges"> Shipping line charges </option>
                                                                            <option value="Container charges/deposit"> Container charges/deposit </option>
                                                                            <option value="Transport cost"> Transport cost </option>
                                                                            <option value="Stamp / miscellaneous"> Stamp / miscellaneous </option>
                                                                            <option value="TANROAD charges"> TANROAD charges </option>
                                                                            <option value="TRA charges"> TRA charges </option>
                                                                            <option value="Freight and delivery "> Freight and delivery  </option>
                                                                            <option value="Fuel"> Fuel </option>
                                                                            <option value="Stationary and printing"> Stationary and printing</option>
                                                                            <option value="Commissions and fees"> Commissions and fees</option>
                                                                            <option value="Facilitation expenses"> Facilitation expenses</option>
                                                                            <option value="Insurance charges"> Insurance charges</option>
                                                                            <option value="Repair and Maintenance"> Repair and Maintenance</option>
                                                                            <option value="Travel expenses"> Travel expenses</option>

                                                                        </select>
                                                                    </div>
                                                                    <div class="col-md-12">
                                                                        <label>Amount In Words</label>
                                                                        <textarea placeholder="Enter amount in words "  class="form-control" name="amount_words" required >{{$value->amount_words}}</textarea>
                                                                    </div>
                                                                    <div class="col-md-12 pt-5 pb-5">
                                                                        <center>
                                                                            <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Close</button>
                                                                            <button type="submit" class="btn btn-primary waves-effect waves-light">Save changes</button>
                                                                        </center>
                                                                    </div>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <a href="{{route ('view/request', ['id' =>$value->id])}}"  class="pull-left edit  btn-lg  btn btn-success dlt_sm_table mx-2 mt-2"><i class="fa fa-eye"></i></a>


                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                            <span class="float-right">
                            <?php echo $expenses_data->appends(Request::all())->fragment('foo')->render(); ?>
                    </span>
                    </div>

                </div>
            </div>
        </div>

    </div>
</div>
<!-- end wrapper -->


<!-- Footer -->
@include('layouts.footer')
<!-- End Footer -->


@include('layouts.javas');
<script type="text/javascript">
    $(document).ready(function() {

        $('#ladding_id').select2();

        $('select[name="ladding_id"]').on('change', function() {
            var stateID = $(this).val();
            if(stateID) {
                $.ajax({
                    url: '/add_status/ajax/'+stateID,
                    type: "GET",
                    dataType: "json",
                    success:function(data) {


                        $('select[name="cont_no"]').empty();
                        $.each(data, function(key, value) {
                            $('select[name="cont_no"]').append('<option value="'+ key +'">'+ value +'</option>');
                        });


                    }
                });
            }else{
                $('select[name="cont_no"]').empty();
            }
        });
    });
</script>
</body>
</html>
<!-- Localized -->