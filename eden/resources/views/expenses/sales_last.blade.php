<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    @include('layouts.title')
    <meta content="Admin Dashboard" name="description" />
    <meta content="Mikumi" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <link rel="shortcut icon" href="/assets/images/favicon.ico">

    <link href="/assets/plugins/datatables/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
    <link href="/assets/plugins/datatables/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css" />

    <link href="/assets/plugins/datatables/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css" />

    <link href="/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="/assets/css/icons.css" rel="stylesheet" type="text/css" />
    <link href="/assets/css/style.css" rel="stylesheet" type="text/css" />

    <link href="/assets/css/select2.min.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .all{
            height: calc(2.25rem + 2px) !important;
        }
        .select2{
            margin-top: -4rem;
        }
        .dt-buttons{
            margin-left: -2% !important;
        }
        .has-submenu{
            padding: 0px 10px 0px 10px !important;
        }
        .dropdown-menu>li>a{
            font-weight: 400 !important;
        }
        .dropdown-menu {
            padding: 7px 50px;
            font-size: 15px;
            box-shadow: 0 2px 31px rgba(0, 0, 0, 0.08);
            border-color: #eff3f6;
            margin-top: 17px;
            min-width: 21rem;
        }
    </style>

</head>
<body>
@include('layouts.sales_sidebar')

<div class="wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <div class="btn-group pull-right">
                        <ol class="breadcrumb hide-phone p-0 m-0">

                        </ol>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <div class="card m-b-30">
                    <div class="card-body mx-auto">
                        <h5 class="page-title mx-3">Consignment Requisition Form</h5>
                        @if (Session::has('message'))
                            <div class="alert alert-success alert-dismissable">
                                <a href="" class="close" data-dismiss="alert" aria-label="close">
                                    &times;

                                </a>
                                {{ Session::get('message') }}
                            </div>

                        @endif
                        @if (Session::has('delete'))
                            <div class="alert alert-success alert-dismissable">
                                <a href="" class="close" data-dismiss="alert" aria-label="close">
                                    &times;

                                </a>
                                {{ Session::get('delete') }}
                            </div>

                        @endif
                        @if (Session::has('updated'))
                            <div class="alert alert-success alert-dismissable">
                                <a href="" class="close" data-dismiss="alert" aria-label="close">
                                    &times;
                                </a>
                                {{ Session::get('updated') }}
                            </div>
                        @endif
                        <form class="form-horizontal"  action="/expenses/form" method="POST" enctype="multipart/form-data">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                            <div class="row container  pt-3">
                                <div class="col-md-6 ">
                                    <label>Bill of lading</label>
                                    <select class="form-control all " name="ladding_id" id="ladding_id" required>
                                        <option value="">Select Bill of lading</option>
                                        @foreach($ladding as $key => $value)
                                            <option value="{{ $key }}">{{$value}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-6">
                                    <label>Container no / Chassis no</label>
                                    <select name="cont_no" class="form-control form_size">
                                        <option value="">Container number</option>
                                    </select>
                                </div>

                                <div class="col-md-6">
                                    <label>Date</label>
                                    <input type="date" name="expenses_date" class="form-control"  required>
                                </div>
                                <div class="col-md-6">
                                    <label>Payee</label>
                                    <input type="text" name="payee" class="form-control" placeholder="Enter payee name"  required>
                                </div>
                                <div class="col-md-6">
                                    <label>Amount Request</label>
                                    <input type="number" name="amount_requested" class="form-control" placeholder="Enter amount" required>
                                </div>
                                <div class="col-md-6">
                                    <label>Currency type</label>
                                    <select name="amount_currency" class="form-control" required>
                                        <option value=""> Choose</option>
                                        <option value="TZS"> TZS </option>
                                        <option value="USD"> USD </option>
                                    </select>
                                </div>
                                <div class="col-md-12">
                                    <label>Amount In Words</label>
                                    <textarea placeholder="Enter amount in words " class="form-control" name="amount_words" required ></textarea>
                                </div>
                                <div class="col-md-12">
                                    <label>Description</label>
                                    <textarea class="form-control" name="more_info" placeholder="Enter more details " ></textarea>
                                </div>
                                <div class="col-md-12 mt-5">
                                    <center>
                                        <button type="reset" class="btn btn-danger waves-effect" >Clear</button>
                                        <button type="submit" class="btn btn-primary waves-effect waves-light">Submit </button>
                                    </center>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('layouts.footer')
@include('layouts.javas')


<script type="text/javascript">
    $(document).ready(function() {

        $('#ladding_id').select2();

        $('select[name="ladding_id"]').on('change', function() {
            var stateID = $(this).val();
            if(stateID) {
                $.ajax({
                    url: '/add_status/ajax/'+stateID,
                    type: "GET",
                    dataType: "json",
                    success:function(data) {


                        $('select[name="cont_no"]').empty();
                        $.each(data, function(key, value) {
                            $('select[name="cont_no"]').append('<option value="'+ key +'">'+ value +'</option>');
                        });


                    }
                });
            }else{
                $('select[name="cont_no"]').empty();
            }
        });
    });
</script>

</body>
</html>