<!DOCTYPE html>
<html>

@include('layouts.finance_head')
<link href="/assets/css/select2.min.css" rel="stylesheet" type="text/css" />
<style type="text/css">
    .form_size{
        height: calc(3.25rem + 2px) !Important;
    }
    .select2{
        width: 100% !important;
        margin-top: -3rem;
    }


</style>
<body>
@include('layouts.finance_sidebar')

<div class="wrapper">
    <div class="container-fluid">
        <div class="modal fade bill_lading"  role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title mt-0" id="myLargeModalLabel">Search by Bill</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body">
                        <form class="form-horizontal"  action="/search/bill/driver" method="POST" enctype="multipart/form-data">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                            <div class="row col-md-12">
                                <div class="col-md-12">
                                    <label>Bill of lading</label>
                                    <select class="form-control form_size " name="ladding_id"  id="ladding_id" required>
                                        <option value="">Select Bill of lading</option>
                                        @foreach($ladding as $key => $value)
                                            <option value="{{ $key }}">{{$value}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-12 pt-5 pb-5">
                                    <button type="submit" class="btn btn-primary waves-effect waves-light">Search </button>
                                    <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.modal -->
    <div class="row m-0 p-3">
        <div class="col-sm-12 ">
            <div class="page-title-box">
                <div class="btn-group pull-right">
                    <ol class="breadcrumb hide-phone p-0 m-0">
                        <li class="breadcrumb-item"><a href="#">All Driver List</a></li>
                        <li class="breadcrumb-item active">Dashboard</li>
                    </ol>
                </div>
                <h4 class="page-title">All Office Expenses  </h4>
            </div>
        </div>
        <div class="col-12 container" style="margin-top: -3rem;">
            <div class="card m-b-30 ">
                <div class="card-body">
                    @if (Session::has('message'))
                        <div class="alert alert-success alert-dismissable">
                            <a href="" class="close" data-dismiss="alert" aria-label="close">
                                &times;

                            </a>
                            {{ Session::get('message') }}
                        </div>

                    @endif
                    @if (Session::has('delete'))
                        <div class="alert alert-success alert-dismissable">
                            <a href="" class="close" data-dismiss="alert" aria-label="close">
                                &times;

                            </a>
                            {{ Session::get('delete') }}
                        </div>

                    @endif
                    @if (Session::has('updated'))
                        <div class="alert alert-success alert-dismissable">
                            <a href="" class="close" data-dismiss="alert" aria-label="close">
                                &times;

                            </a>
                            {{ Session::get('updated') }}
                        </div>

                    @endif
                        <div class="modal fade bs-example-modal-lg"  role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title mt-0" id="myLargeModalLabel"> Request Requisition Form  </h4>
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    </div>
                                    <div class="modal-body">
                                        <form class="form-horizontal"  action="/expenses/form/normal" method="POST" enctype="multipart/form-data">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                            <div class="row container  pt-3">
                                                <div class="col-md-6">
                                                    <label>Date</label>
                                                    <input type="date" name="expenses_date" class="form-control"  required>
                                                </div>
                                                <div class="col-md-6">
                                                    <label>Payee</label>
                                                    <input type="text" name="payee" class="form-control" placeholder="Enter payee name"  required>
                                                </div>
                                                <div class="col-md-6">
                                                    <label>Amount Request</label>
                                                    <input type="number" name="amount_requested" class="form-control" placeholder="Enter amount" required>
                                                </div>
                                                <div class="col-md-6">
                                                    <label>Currency type</label>
                                                    <select name="amount_currency" class="form-control form_size" required>
                                                        <option value=""> Choose</option>
                                                        <option value="TZS"> TZS </option>
                                                        <option value="USD"> USD </option>
                                                    </select>
                                                </div>
                                                <div class="col-md-12">
                                                    <label>Activity  </label>
                                                    <select class="form-control form_size" name="reason" required>
                                                        <option value=""> Choose</option>
                                                        <option value="Transport cost"> Transport cost </option>
                                                        <option value="Stamp / miscellaneous"> Stamp / miscellaneous </option>
                                                        <option value="Fuel"> Fuel </option>
                                                        <option value="Stationary and printing"> Stationary and printing</option>
                                                        <option value="Facilitation expenses"> Facilitation expenses</option>
                                                        <option value="License and Permit"> License and Permit</option>
                                                        <option value="Legal and Professional fees">  Legal and Professional fees</option>
                                                        <option value="Meals and Entertainments"> Meals and Entertainments </option>
                                                        <option value="Insurance charges"> Insurance charges</option>
                                                        <option value="Repair and Maintenance"> Repair and Maintenance</option>
                                                        <option value="Travel expenses"> Travel expenses</option>
                                                        <option value="General / Administrative activities  "> General / Administrative activities  </option>
                                                        <option value="NSSF"> NSSF </option>
                                                        <option value="WCF"> WCF </option>
                                                        <option value="TRA"> TRA </option>
                                                        <option value=" Amendment charges">  Amendment charges</option>

                                                    </select>
                                                </div>
                                                <div class="col-md-12">
                                                    <label>Amount In Words</label>
                                                    <textarea placeholder="Enter amount in words " class="form-control" name="amount_words" required ></textarea>
                                                </div>
                                                <div class="col-md-12">
                                                    <label>Description</label>
                                                    <textarea class="form-control" name="more_info" placeholder="Enter more details " ></textarea>
                                                </div>
                                                <div class="col-md-12 mt-5 pb-5">
                                                    <center>
                                                        <button type="reset" class="btn btn-danger waves-effect " data-dismiss="modal" > Cancel </button>
                                                        <button type="submit" class="btn btn-primary waves-effect waves-light">Submit </button>
                                                    </center>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal fade search"  role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title mt-0 mx-4" id="myLargeModalLabel"> Advanced Search   </h4>
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    </div>
                                    <div class="modal-body">
                                        <form class="form-horizontal"  action="/back/expenses" method="POST" enctype="multipart/form-data">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                            <div class="row container  pt-3">
                                                <div class="col-md-6">
                                                    <label> From</label>
                                                    <input type="date" name="date1" class="form-control"  required>
                                                </div>
                                                <div class="col-md-6">
                                                    <label> To </label>
                                                    <input type="date" name="date2" class="form-control"   required>
                                                </div>
                                                <div class="col-md-6">
                                                    <label>Activity  </label>
                                                    <select class="form-control form_size" name="activity"  required>
                                                        <option value=""> Choose</option>
                                                        <option value="Transport cost"> Transport cost </option>
                                                        <option value="Stamp / miscellaneous"> Stamp / miscellaneous </option>
                                                        <option value="Fuel"> Fuel </option>
                                                        <option value="Stationary and printing"> Stationary and printing</option>
                                                        <option value="Facilitation expenses"> Facilitation expenses</option>
                                                        <option value="License and Permit"> License and Permit</option>
                                                        <option value="Legal and Professional fees">  Legal and Professional fees</option>
                                                        <option value="Meals and Entertainments"> Meals and Entertainments </option>
                                                        <option value="Insurance charges"> Insurance charges</option>
                                                        <option value="Repair and Maintenance"> Repair and Maintenance</option>
                                                        <option value="Travel expenses"> Travel expenses</option>
                                                        <option value="General / Administrative activities  "> General / Administrative activities  </option>


                                                    </select>
                                                </div>
                                                <div class="col-md-12 mt-5 mb-5">
                                                    <button type="reset" class="btn btn-danger waves-effect" data-dismiss="modal">Close</button>
                                                    <button type="submit" class="btn btn-success waves-effect waves-light">Search </button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <a href="/expenses" role="button" type="button" class="btn btn-success waves-effect waves-light pull-right mx-1" ><i class="fa fa-search" style="color: #ffffff;"></i><span style="color: #ffffff;"> All  </span></a>
                        <button type="button" class="btn btn-warning waves-effect waves-light pull-right mx-4" data-toggle="modal" data-target=".search "><i class="fa fa-search"></i><span style="color: #ffffff;"> Advanced  Search  </span></button>


                        <button type="button" class="btn btn-primary waves-effect waves-light pull-right" data-toggle="modal" data-target=".bs-example-modal-lg "><i class="fa fa-plus"></i><span> Add Request  </span></button>

                        <table id="datatable-buttons" class="table table-hover table-striped table-bordered" cellspacing="0" width="100%" >
                        <thead>
                        <tr style="font-size: 13px;background-color: #416b8d !important;color: #fff;">
                            <th>#</th>
                            <th>Date</th>
                            <th>Requested  </th>
                            <th>Payee</th>
                            <th>Amount</th>
                            <th>Description</th>
                            <th>Finance </th>
                            <th>Admin </th>
                            <th>Action</th>
                        </tr>
                        </thead>



                        <tbody>
                        <?php $i=1;?>
                        @foreach($expenses_data AS $value)
                            <tr>
                                <td>{{$i++}}</td>
                                <td>{{$value->expenses_date}}</td>
                                <td>{{$value->user->name}}  </td>
                                <td>{{$value->payee}}</td>
                                <td>{{number_format($value->amount_requested)}} {{$value->amount_currency}} </td>
                                <td>{{$value->more_info}} / {{$value->reason}}</td>
                                <td>
                                    @if($value->finance_status == 'wait')

                                        <span class="label label-info"> {{$value->finance_status}}</span>
                                    @elseif($value->finance_status == 'rejected')

                                        <span class="label label-danger"> {{$value->finance_status}}</span>
                                    @elseif($value->finance_status == 'pending')

                                        <span class="label label-warning"> {{$value->finance_status}}</span>

                                    @else
                                        <span class="label label-success"> {{$value->finance_status}} </span>

                                    @endif

                                </td>
                                <td>

                                    @if($value->super_status == 'wait')

                                        <span class="label label-info"> {{$value->super_status}}</span>
                                    @elseif($value->super_status == 'rejected')

                                        <span class="label label-danger"> {{$value->super_status}}</span>
                                    @elseif($value->super_status == 'pending')

                                        <span class="label label-warning"> {{$value->super_status}}</span>

                                    @else
                                        <span class="label label-success"> {{$value->super_status}} </span>

                                @endif

                                <td>
                                    <button data-toggle="modal" data-target="#editleads<?php echo $i;?>" class="pull-left edit  btn-lg  btn btn-primary dlt_sm_table mx-2 mt-2" style="width: 37px !important;"><i class="fa fa-pencil"></i></button>
                                    <button data-toggle="modal" data-target="#delete<?php echo $i;?>" class="pull-left edit  btn-lg  btn btn-danger dlt_sm_table mx-2 mt-2" style="width: 37px"><i class="fa fa-trash"></i></button>
                                    <div class="modal fade" id="delete<?php echo $i;?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLarge01" style="display: none;" aria-hidden="true">
                                        <div class="modal-dialog modal-lg" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title">Delete </h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">×</span>
                                                    </button>
                                                </div>
                                                <form class="pull-left" action="/normal/expenses/{{$value->id}}" method="POST">
                                                    <label class="mx-4">Are you sure you want to delete</label>
                                                    <input type="hidden" name="_method" value="DELETE" />
                                                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                                    <div class="modal-footer">
                                                        <button type="submit" class="btn btn-success">Confirm</button>
                                                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="editleads<?php echo $i;?>" class="modal fade" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
                                        <div class="modal-dialog modal-lg">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h4 class="modal-title" id="editleads"> Edit Requisition Form</h4>
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

                                                </div>
                                                <div class="modal-body">
                                                    <div class="row">
                                                        <form role="form" action="/normal/expenses/{{$value->id}}" method="post" enctype="multipart/form-data">
                                                            <input type="hidden" name="_method" value="PUT" />
                                                            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                                            <div class="row col-md-12">
                                                                <div class="col-md-6">
                                                                    <label>Payee</label>
                                                                    <input type="text" name="payee" value="{{$value->payee}}" class="form-control" placeholder="Enter payee name"  >
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <label>Amount Request</label>
                                                                    <input type="number" name="amount_requested" value="{{$value->amount_requested}}" class="form-control" placeholder="Enter amount" >
                                                                </div>


                                                                <div class="col-md-6">
                                                                    <label>Currency type</label>
                                                                    <select name="amount_currency " class="form-control form_size" >
                                                                        <option value=""> Choose</option>
                                                                        <option value="TZS"> TZS </option>
                                                                        <option value="USD"> USD </option>
                                                                    </select>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <label>Status</label>
                                                                    <select name="finance_status" class="form-control form_size" required>
                                                                        @if($value->super_status == 'rejected')
                                                                            <option > canceled by super admin</option>

                                                                        @elseif($value->super_status == 'Authorized')
                                                                            <option > voucher closed</option>
                                                                        @else
                                                                            <option value="{{$value->finance_status}}"> Choose</option>
                                                                            <option value="pending">pending</option>
                                                                            <option value="rejected"> reject </option>
                                                                            <option value="approved"> Approve </option>
                                                                        @endif
                                                                    </select>
                                                                </div>
                                                                <div class="col-md-12">
                                                                    <label>Activity  </label>
                                                                    <select class="form-control form_size" name="reason" required>
                                                                        <option value="{{$value->reason}}"> {{$value->reason}}</option>
                                                                        <option value="Transport cost"> Transport cost </option>
                                                                        <option value="Stamp / miscellaneous"> Stamp / miscellaneous </option>
                                                                        <option value="Fuel"> Fuel </option>
                                                                        <option value="Stationary and printing"> Stationary and printing</option>
                                                                        <option value="Facilitation expenses"> Facilitation expenses</option>
                                                                        <option value="License and Permit"> License and Permit</option>
                                                                        <option value="Legal and Professional fees">  Legal and Professional fees</option>
                                                                        <option value="Meals and Entertainments"> Meals and Entertainments </option>
                                                                        <option value="Insurance charges"> Insurance charges</option>
                                                                        <option value="Repair and Maintenance"> Repair and Maintenance</option>
                                                                        <option value="Travel expenses"> Travel expenses</option>
                                                                        <option value="General / Administrative activities  "> General / Administrative activities  </option>

                                                                    </select>
                                                                </div>
                                                                <div class="col-md-12">
                                                                    <label>Amount In Words</label>
                                                                    <textarea placeholder="Enter amount in words "  class="form-control" name="amount_words" required >{{$value->amount_words}}</textarea>
                                                                </div>
                                                                <div class="col-md-12">
                                                                    <label>Description</label>
                                                                    <textarea class="form-control" name="more_info" placeholder="Enter more details " >{{$value->more_info}}</textarea>
                                                                </div>
                                                                <div class="col-md-12 pt-5 pb-5">
                                                                    <center>
                                                                        <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Close</button>
                                                                        <button type="submit" class="btn btn-primary waves-effect waves-light">Save changes</button>
                                                                    </center>
                                                                </div>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <a href="{{route ('view/request/normal', ['id' =>$value->id])}}"  class="pull-left edit  btn-lg  btn btn-success dlt_sm_table mx-2 mt-2"><i class="fa fa-eye"></i></a>

                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>

</div>
</div>
@include('layouts.footer')
@include('layouts.javas')
<script type="text/javascript">
    $(document).ready(function() {
        $('select[name="ladding_id"]').on('change', function() {
            var stateID = $(this).val();
            if(stateID) {
                $.ajax({
                    url: '/add_status/ajax/'+stateID,
                    type: "GET",
                    dataType: "json",
                    success:function(data) {


                        $('select[name="container_id"]').empty();
                        $.each(data, function(key, value) {
                            $('select[name="container_id"]').append('<option value="'+ key +'">'+ value +'</option>');
                        });


                    }
                });
            }else{
                $('select[name="container_id"]').empty();
            }
        });
    });
</script>

</body>
</html>