<!DOCTYPE html>
<html>

@include('layouts.finance_head')
<style type="text/css">
    .form_size {
        height: calc(3.25rem + 2px) !important;
    }
    .select2{
        width: 100% !important;
        margin-top: -1rem;
    }
    /*.all{*/
    /*height: calc(4.25rem + 2px) !important*/
    /*}*/


</style>
<body>
@include('layouts.sales_sidebar')

<div class="wrapper">
    <!-- /.modal -->
    <div class="row m-0 p-3">
        <div class="col-sm-12 ">
            <div class="page-title-box">
                <div class="btn-group pull-right">
                    <ol class="breadcrumb hide-phone p-0 m-0">
                        <li class="breadcrumb-item"><a href="#"> List</a></li>
                        <li class="breadcrumb-item active">Dashboard</li>
                    </ol>
                </div>
                <h4 class="page-title"> Expenses  Request </h4>
            </div>
        </div>
        <div class="col-12 container" style="margin-top: -3rem;">
            <div class="card m-b-30 ">
                <div class="card-body">
                    @if (Session::has('message'))
                        <div class="alert alert-success alert-dismissable">
                            <a href="" class="close" data-dismiss="alert" aria-label="close">
                                &times;

                            </a>
                            {{ Session::get('message') }}
                        </div>

                    @endif
                    @if (Session::has('delete'))
                        <div class="alert alert-success alert-dismissable">
                            <a href="" class="close" data-dismiss="alert" aria-label="close">
                                &times;

                            </a>
                            {{ Session::get('delete') }}
                        </div>

                    @endif
                    @if (Session::has('updated'))
                        <div class="alert alert-success alert-dismissable">
                            <a href="" class="close" data-dismiss="alert" aria-label="close">
                                &times;

                            </a>
                            {{ Session::get('updated') }}
                        </div>

                    @endif

                    <div class="modal fade bs-example-modal-lg"  role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title mt-0" id="myLargeModalLabel"> Request Requisition Form  </h4>
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                </div>
                                <div class="modal-body">
                                    <form class="form-horizontal"  action="/expenses/form" method="POST" enctype="multipart/form-data">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                        <div class="row container  pt-3">
                                            <div class="col-md-6 ">
                                                <label>Bill of lading</label>
                                                <select class="form-control all " name="ladding_id" id="ladding_id" required>
                                                    <option value="">Select Bill of lading</option>
                                                    @foreach($ladding as $key => $value)
                                                        <option value="{{ $key }}">{{$value}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="col-md-6">
                                                <label>Container no / Chassis no</label>
                                                <select name="cont_no" class="form-control form_size">
                                                    <option value="">Container number</option>
                                                </select>
                                            </div>

                                            <div class="col-md-6">
                                                <label>Date</label>
                                                <input type="date" name="expenses_date" class="form-control"  required>
                                            </div>
                                            <div class="col-md-6">
                                                <label>Payee</label>
                                                <input type="text" name="payee" class="form-control" placeholder="Enter payee name"  required>
                                            </div>
                                            <div class="col-md-6">
                                                <label>Amount Request</label>
                                                <input type="number" name="amount_requested" class="form-control" placeholder="Enter amount" required>
                                            </div>
                                            <div class="col-md-6">
                                                <label>Currency type</label>
                                                <select name="amount_currency" class="form-control form_size" required>
                                                    <option value=""> Choose</option>
                                                    <option value="TZS"> TZS </option>
                                                    <option value="USD"> USD </option>
                                                </select>
                                            </div>
                                            <div class="col-md-12">
                                                <label>Activity  </label>
                                                <select class="form-control form_size" name="reason" required>
                                                    <option value=""> Choose</option>
                                                    <option value="Port charges"> Port charges </option>
                                                    <option value="Shipping line charges"> Shipping line charges </option>
                                                    <option value="Container charges/deposit"> Container charges/deposit </option>
                                                    <option value="Transport cost"> Transport cost </option>
                                                    <option value="Stamp / miscellaneous"> Stamp / miscellaneous </option>
                                                    <option value="TANROAD charges"> TANROAD charges </option>
                                                    <option value="TRA charges"> TRA charges </option>
                                                    <option value="Freight and delivery "> Freight and delivery  </option>
                                                    <option value="Fuel"> Fuel </option>
                                                    <option value="Stationary and printing"> Stationary and printing</option>
                                                    <option value="Commissions and fees"> Commissions and fees</option>
                                                    <option value="Facilitation expenses"> Facilitation expenses</option>
                                                    <option value="Insurance charges"> Insurance charges</option>
                                                    <option value="Repair and Maintenance"> Repair and Maintenance</option>
                                                    <option value="Travel expenses"> Travel expenses</option>
                                                    <option value="Chemical Permit"> Chemical Permit </option>
                                                    <option value="Demurage"> Demurage </option>
                                                    <option value="Amendment charges"> Amendment charges </option>
                                                </select>
                                            </div>
                                            <div class="col-md-12">
                                                <label>Amount In Words</label>
                                                <textarea placeholder="Enter amount in words " class="form-control" name="amount_words" required ></textarea>
                                            </div>
                                            <div class="col-md-12">
                                                <label>Description</label>
                                                <textarea class="form-control" name="more_info" placeholder="Enter more details " ></textarea>
                                            </div>
                                            <div class="col-md-12 mt-5">
                                                <center>
                                                    <button type="reset" class="btn btn-danger waves-effect" >Clear</button>
                                                    <button type="submit" class="btn btn-primary waves-effect waves-light">Submit </button>
                                                </center>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                    <button type="button" class="btn btn-primary waves-effect waves-light pull-right" data-toggle="modal" data-target=".bs-example-modal-lg "><i class="fa fa-plus"></i><span> Add Request  </span></button>

                    <table id="datatable-buttons" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                        <tr style="font-size: 13px;background-color: #416b8d !important;color: #fff;">
                            <th>#</th>
                            <th>Date</th>
                            <th>Bill of Loading</th>
                            <th>Cont no</th>
                            <th>Consignee</th>
                            <th>C&F</th>
                            <th>Requested  </th>
                            <th>Payee</th>
                            <th>TZS</th>
                            <th>USD</th>
                            <th>Description</th>
                            <th>Finance </th>
                            <th>Admin </th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $i=1;?>
                        @foreach($expenses_data AS $value)
                            <tr>
                                <td>{{$i++}}</td>
                                <td>{{$value->expenses_date}}</td>
                                <td>{{$value->container->lad->bill_lading}}</td>
                                <td>{{$value->container->cont_no}}</td>
                                <td>{{$value->container->lad->track->client_name}}</td>
                                <td>{{$value->container->lad->track->agent->organization_name}}</td>
                                <td>{{$value->user->name}}  </td>
                                <td>{{$value->payee}}</td>
                                <td><span class="amountTZS">{{$value->currency_tzs}}</span> </td>
                                <td> <span class="amountUSD">{{$value->currency_usd}}</span></td>
                                <td>{{$value->more_info}} / {{$value->reason}}</td>
                                <td>
                                    @if($value->finance_status == 'rejected')

                                        <span class="label label-danger"> {{$value->finance_status}}</span>
                                    @elseif($value->finance_status == 'pending')

                                        <span class="label label-warning"> {{$value->finance_status}}</span>

                                    @else
                                        <span class="label label-success"> {{$value->finance_status}} </span>

                                    @endif

                                </td>
                                <td>

                                    @if($value->super_status == 'wait')

                                        <span class="label label-info"> {{$value->super_status}}</span>
                                    @elseif($value->super_status == 'rejected')

                                        <span class="label label-danger"> {{$value->super_status}}</span>
                                    @elseif($value->super_status == 'pending')

                                        <span class="label label-warning"> {{$value->super_status}}</span>

                                    @else
                                        <span class="label label-success"> {{$value->super_status}} </span>

                                @endif

                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>

</div>
</div>
@include('layouts.footer')
@include('layouts.javas')
<script type="text/javascript">
    $(document).ready(function() {

        $('#ladding_id').select2();

        $('select[name="ladding_id"]').on('change', function() {
            var stateID = $(this).val();
            if(stateID) {
                $.ajax({
                    url: '/add_status/ajax/'+stateID,
                    type: "GET",
                    dataType: "json",
                    success:function(data) {


                        $('select[name="cont_no"]').empty();
                        $.each(data, function(key, value) {
                            $('select[name="cont_no"]').append('<option value="'+ key +'">'+ value +'</option>');
                        });


                    }
                });
            }else{
                $('select[name="cont_no"]').empty();
            }
        });
    });
</script>

</body>
</html>