
<html lang="en">

<head>
    <title> Cargo  Trucking</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <meta name="author" content="Free-Template.co" />
    <link rel="shortcut icon" href="/landing/img/favicon.png">

    <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,700%7COswald:400,700" rel="stylesheet">

    <link rel="stylesheet" href="/track/fonts/icomoon/style.css">

    <link rel="stylesheet" href="/track/css/bootstrap.min.css">
    <link rel="stylesheet" href="/track/css/jquery.fancybox.min.css">
    <link rel="stylesheet" href="/track/css/owl.carousel.min.css">
    <link rel="stylesheet" href="/track/css/owl.theme.default.min.css">
    <link rel="stylesheet" href="/track/fonts/flaticon/font/flaticon.css">
    <link rel="stylesheet" href="/track/css/aos.css">

    <!-- MAIN CSS -->
    <link rel="stylesheet" href="/track/css/style.css">

</head>

<body data-spy="scroll" data-target=".site-navbar-target" data-offset="300">



<div class="site-wrap" id="home-section">

    <div class="site-mobile-menu site-navbar-target">
        <div class="site-mobile-menu-header">
            <div class="site-mobile-menu-close mt-3">
                <span class="icon-close2 js-menu-toggle"></span>
            </div>
        </div>
        <div class="site-mobile-menu-body"></div>
    </div>




    <div class="ftco-blocks-cover-1">
        <div class="ftco-cover-1 overlay" style="background-image: url('https://source.unsplash.com/pSyfecRCBQA/1920x780')">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-6">
                        <h1>Track your Shipment</h1>
                        <form role="form" class="form-horizontal" action="/cargo/tracking/results" method="POST" enctype="multipart/form-data">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                            <div class="form-group">
                                <input type="text" class="form-control" name="track" placeholder="Enter your bill of lading number">

                                <p style="color: white;">or</p>

                                <input type="text" class="form-control" name="cont_no" placeholder="Enter your contaniner  number">
                                <button type="submit" class="btn mt-4 btn-primary text-white px-4 " value="Track Now"> Track Now</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
</div>

<script src="/track/js/jquery-3.3.1.min.js"></script>
<script src="/track/js/popper.min.js"></script>
<script src="/track/js/bootstrap.min.js"></script>
<script src="/track/js/owl.carousel.min.js"></script>
<script src="/track/js/jquery.sticky.js"></script>
<script src="/track/js/jquery.waypoints.min.js"></script>
<script src="/track/js/jquery.animateNumber.min.js"></script>
<script src="/track/js/jquery.fancybox.min.js"></script>
<script src="/track/js/jquery.easing.1.3.js"></script>
<script src="/track/js/aos.js"></script>

<script src="/track/js/main.js"></script>


</body>

</html>
