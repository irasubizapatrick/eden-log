<!DOCTYPE html>
<html>

@include('layouts.admin_modal')
<style>
    .size{
        height: calc(3.25rem + 2px) !Important;
    }
    .dt-buttons{
        margin-left: -2% !important;
    }
    .has-submenu{
        padding: 0px 10px 0px 10px;
    }
    #datatable-buttons_paginate{
        display: none !important;
    }

</style>
<body>
@include('layouts.admin_sidebar')
<div class="wrapper">
    <div class="container-fluid">
        <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title mt-0" id="myLargeModalLabel">Add Staff </h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body">
                        <form class="form-horizontal"  action="/staff" method="POST" enctype="multipart/form-data">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                            <div class="row col-md-12">
                                <div class="col-md-6 pull-left">
                                    <label> Staff  Category</label>
                                    <select name="role" class="form-control size"  required>
                                        <option value=""> Choose role</option>
                                        <option value="Sales"> Operations </option>
                                        <option value="Declaration"> Declaration </option>
                                        <option value="Shipping"> Shipping </option>
                                        <option value="Documentation"> Documentation Staff</option>
                                        <option value="Finance"> Finance  </option>
                                    </select>
                                </div>
                                <div class="col-md-6">
                                    <label>Full Name</label>
                                    <input type="text" class="form-control" name="employee_name" placeholder="Enter Full names" required>
                                </div>

                                <div class="col-md-6">
                                    <label>Telephone</label>
                                    <input type="text" class="form-control" name="employee_phone" placeholder="Enter Telephone number" required>
                                </div>
                                <div class="col-md-6 pull-left">
                                    <label>Username</label>
                                    <input type="text" class="form-control" name="name" placeholder="Enter username" required>
                                </div>
                                <div class="col-md-6">
                                    <label>Email</label>
                                    <input type="email" class="form-control" name="employee_email" placeholder="Enter  email" required>
                                </div>
                                <div class="col-md-6">
                                    <label>Password</label>
                                    <input type="password" class="form-control" name="password" placeholder=" ********* " required>
                                </div>

                                <div class="col-md-12 pt-5 pb-5">
                                    <center>
                                        <button type="submit" class="btn btn-primary waves-effect waves-light">Save changes</button>
                                        <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Close</button>

                                    </center>
                                    </div>
                                </div>


                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row m-0 p-2">
        <div class="col-sm-12">
            <div class="page-title-box" style="padding: 60px 0px 46px 0px !important">
                <div class="btn-group pull-right">
                    <ol class="breadcrumb hide-phone p-0 m-0">
                        <li class="breadcrumb-item"><a href="#">Staff List</a></li>
                        <li class="breadcrumb-item active">Dashboard</li>
                    </ol>
                </div>
                <h4 class="page-title">All Staff</h4>
            </div>
        </div>
        <div class="col-12 container" style="margin-top: -3rem;">
            <div class="card m-b-30">
                <div class="card-body">
                    <button type="button" class="btn btn-primary waves-effect waves-light pull-right" data-toggle="modal" data-target=".bs-example-modal-lg "><i class="fa fa-plus"></i> <span>Add Staff</span></button>
                    @if (Session::has('message'))
                        <div class="alert alert-success alert-dismissable">
                            <a href="" class="close" data-dismiss="alert" aria-label="close">
                                &times;

                            </a>
                            {{ Session::get('message') }}
                        </div>

                    @endif
                    @if (Session::has('delete'))
                        <div class="alert alert-success alert-dismissable">
                            <a href="" class="close" data-dismiss="alert" aria-label="close">
                                &times;

                            </a>
                            {{ Session::get('delete') }}
                        </div>

                    @endif
                    @if (Session::has('updated'))
                        <div class="alert alert-success alert-dismissable">
                            <a href="" class="close" data-dismiss="alert" aria-label="close">
                                &times;

                            </a>
                            {{ Session::get('updated') }}
                        </div>

                    @endif

                    <table id="datatable-buttons" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                        <tr style="font-size: 13px;background-color: #416b8d !important;color: #fff;">
                            <th>#</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Role</th>
                            <th>Telephone</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $i=1;?>
                        @foreach($staff AS $value)
                            <tr>
                                <td>{{$i++}}</td>
                                <td>{{$value->employee_name}}</td>
                                <td>{{$value->employee_email}}</td>
                                <td>

                                    @if($value->display_name == 'Sales')

                                        <span class="label label-info">Operations</span>

                                    @elseif($value->display_name == 'Documentation')

                                        <span class="label label-warning">{{$value->display_name}}</span>

                                    @elseif($value->display_name == 'Shipping')

                                        <span class="label label-danger">{{$value->display_name}}</span>

                                    @elseif($value->display_name == 'Finance')

                                        <span class="label label-primary">{{$value->display_name}}</span>
                                    @else

                                        <span class="label label-success">{{$value->display_name}}</span>
                                    @endif

                                </td>

                                <td>{{$value->employee_phone}}</td>
                                <td>
                                    <button data-toggle="modal" data-target="#editleads<?php echo $i;?>" class="pull-left edit btn-lg  btn-primary dlt_sm_table mx-2 mt-2"><i class="fa fa-pencil"></i></button>
                                    <button data-toggle="modal" data-target="#delete<?php echo $i;?>" class="pull-left edit btn-lg  mx-2 mt-2 btn-danger dlt_sm_table"><i class="fa fa-trash"></i></button>
                                    <div class="modal fade" id="delete<?php echo $i;?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLarge01" style="display: none;" aria-hidden="true">
                                        <div class="modal-dialog modal-lg" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h4 class="modal-title">Delete </h4>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">×</span>
                                                    </button>
                                                </div>
                                                <form class="pull-left" action="/staff/{{$value->id}}" method="POST">
                                                    <label class="mx-4">Are you sure you want to delete</label>
                                                    <input type="hidden" name="_method" value="DELETE" />
                                                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                                    <div class="modal-footer">
                                                        <button type="submit" class="btn btn-success">Confirm</button>
                                                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="editleads<?php echo $i;?>" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
                                        <div class="modal-dialog modal-lg">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h4 class="modal-title" id="editleads"> Edit Staff Details</h4>
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

                                                </div>
                                                <div class="modal-body">
                                                    <div class="row">
                                                        <form role="form" action="/staff/{{$value->id}}" method="post" enctype="multipart/form-data">
                                                            <input type="hidden" name="_method" value="PUT" />
                                                            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                                            <div class="row col-md-12">
                                                                <div class="col-md-6 pull-left">
                                                                    <label> Staff  Category</label>
                                                                    <select name="role" class="form-control size"  required>
                                                                        <option value=""> Choose role</option>
                                                                        <option value="Sales"> Operations </option>
                                                                        <option value="Documentation"> Documentation Staff</option>
                                                                        <option value="Shipping"> Shipping </option>
                                                                        <option value="Declaration"> Declaration </option>
                                                                        <option value="Finance"> Finance  </option>
                                                                    </select>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <label>Full Name</label>
                                                                    <input type="text" class="form-control form_size"  value="{{$value->employee_name}}" name="employee_name" placeholder="Enter Full  name" required>
                                                                </div>
                                                                <div class="col-md-6 pull-left">
                                                                    <label>Phone Number</label>
                                                                    <input type="text" class="form-control form_size" value="{{$value->employee_phone}}"  name="employee_phone" placeholder="Enter Phone number" required>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <label>Email</label>
                                                                    <input type="text" class="form-control form_size" value="{{$value->employee_email}}"  name="employee_email" placeholder="Enter Email " required>
                                                                </div>
                                                                <div class="col-md-12 pt-5 pb-5">
                                                                    <center>
                                                                        <button type="submit" class="btn btn-primary waves-effect waves-light">Save changes</button>
                                                                        <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Close</button>
                                                                    </center>
                                                                </div>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <span class="float-right">
                        <?php echo $staff->appends(Request::all())->fragment('foo')->render(); ?>

                    </span>

                </div>
            </div>
        </div>
    </div>

</div>
</div>
@include('layouts.footer')
@include('layouts.javas')
</body>
</html>