<!DOCTYPE html>
<html>
@include('layouts.admin_head')

<style>
    .form_size{
        height: calc(3.25rem + 7px) !Important;
    }
    .select2{
        width: 100% !important;
        margin-top: -6rem !important;
    }
    body {
        font-family: 'Open Sans', sans-serif;
        font-weight: 300;
    }
    .tabs {
        max-width: 640px;
        margin: 0 auto;
        padding: 0 20px;
    }
    #tab-button {
        display: table;
        table-layout: fixed;
        width: 100%;
        margin: 0;
        padding: 0;
        list-style: none;
    }
    #tab-button li {
        display: table-cell;
        width: 20%;
    }
    #tab-button li a {
        display: block;
        padding: .5em;
        background: #eee;
        border: 1px solid #ddd;
        text-align: center;
        color: #000;
        text-decoration: none;
    }
    #tab-button li:not(:first-child) a {
        border-left: none;
    }
    #tab-button li a:hover,
    #tab-button .is-active a {
        border-bottom-color: transparent;
        background: #376a8f;
        color: #fff;

    }
    .tab-contents {
        padding: .5em 2em 1em;
        border: 1px solid #ddd;
    }

    .tab-button-outer {
        display: none;
    }
    .tab-contents {
        margin-top: 20px;
    }
    @media screen and (min-width: 768px) {
        .tab-button-outer {
            position: relative;
            z-index: 2;
            display: block;
        }
        .tab-select-outer {
            display: none;
        }
        .tab-contents {
            position: relative;
            top: -1px;
            margin-top: 0;
            border: none !important;
        }
    }

    .has-submenu{
        padding: 0px 10px 0px 30px !important;
        font-weight: 700 !important;
    }
</style>
<body>

@include('layouts.sales_sidebar')
<div class="wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <div class="btn-group pull-right">
                        <ol class="breadcrumb hide-phone p-0 m-0">

                        </ol>


                    </div>
                </div>
            </div>
        </div>
        <div class="row col-md-6 mx-auto">
            <div class="card m-b-30">
                <div class="card-body " style="width: 65rem;">
                    @if (Session::has('message'))
                        <div class="alert alert-success alert-dismissable">
                            <a href="" class="close" data-dismiss="alert" aria-label="close">
                                &times;
                            </a>
                            {{ Session::get('message') }}
                        </div>

                    @endif
                    @if (Session::has('delete'))
                        <div class="alert alert-danger alert-dismissable">
                            <a href="" class="close" data-dismiss="alert" aria-label="close">
                                &times;

                            </a>
                            {{ Session::get('delete') }}
                        </div>

                    @endif
                    @if (Session::has('updated'))
                        <div class="alert alert-warning alert-dismissable">
                            <a href="" class="close" data-dismiss="alert" aria-label="close">
                                &times;

                            </a>
                            {{ Session::get('updated') }}
                        </div>

                    @endif

                    <div class="tabs">
                        <div class="tab-button-outer">
                            <ul id="tab-button">
                                <li><a href="#tab01">Track By Bill Of lading </a></li>
                                <li><a href="#tab02">Track By  Container no / Chassis no </a></li>
                            </ul>
                        </div>
                        <div id="tab01" class="tab-contents">
                            <div class="col-md-12">
                                <form class="form-horizontal"  action="/find_truck" method="POST" enctype="multipart/form-data">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                    <div class="row" style="margin-top: 5rem;">
                                        <div class="col-md-12">

                                            <select name="bill_lading"  class="form-control form_size text" id="edit_ladding_id" required>
                                                <option value=""> <i class="fa fa-search">  Enter  Bill of lading</i>  </option>
                                                @foreach($ladding as $key => $value)
                                                    <option value="{{ $key }}">{{$value}}</option>
                                                @endforeach
                                            </select>

                                        </div>
                                        <div class="col-md-12 mt-5 pb-5">
                                            <button type="submit" class="pull-left btn btn-lg btn btn-primary waves-effect"> <i class="fa fa-search"></i> Track</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div id="tab02" class="tab-contents">

                            <div class="col-md-12">
                                <form class="form-horizontal"  action="/find_truck" method="POST" enctype="multipart/form-data">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                    <div class="row" style="margin-top: 5rem;">
                                        <div class="col-md-12">
                                            <select name="cont_no"  class="form-control form_size text" id="ladding_id" required>
                                                <option value=""> Enter Container no / Chassis no </option>
                                                @foreach($container as  $value)
                                                    <option value="{{$value->cont_no}}">{{$value->cont_no}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-md-12 mt-5 pb-5">
                                            <button type="submit" class="pull-left btn btn-lg btn btn-primary waves-effect"> <i class="fa fa-search"></i> Track</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('layouts.footer')
    @include('layouts.javas')

    <script type="text/javascript">
        $(document).ready(function() {
            $('select[name="bill_lading"]').on('change', function() {
                var stateID = $(this).val();
                if(stateID) {
                    $.ajax({
                        url: '/tracking/ajax/'+stateID,
                        type: "GET",
                        dataType: "json",
                        success:function(data) {


                            $('select[name="cont_no"]').empty();
                            $.each(data, function(key, value) {
                                $('select[name="cont_no"]').append('<option value="'+ key +'">'+ value +'</option>');
                            });


                        }
                    });
                }else{
                    $('select[name="cont_no"]').empty();
                }
            });
        });
    </script>
    <script type="text/javascript">
        $(function() {
            var $tabButtonItem = $('#tab-button li'),
                $tabSelect = $('#tab-select'),
                $tabContents = $('.tab-contents'),
                activeClass = 'is-active';

            $tabButtonItem.first().addClass(activeClass);
            $tabContents.not(':first').hide();

            $tabButtonItem.find('a').on('click', function(e) {
                var target = $(this).attr('href');

                $tabButtonItem.removeClass(activeClass);
                $(this).parent().addClass(activeClass);
                $tabSelect.val(target);
                $tabContents.hide();
                $(target).show();
                e.preventDefault();
            });

            $tabSelect.on('change', function() {
                var target = $(this).val(),
                    targetSelectNum = $(this).prop('selectedIndex');

                $tabButtonItem.removeClass(activeClass);
                $tabButtonItem.eq(targetSelectNum).addClass(activeClass);
                $tabContents.hide();
                $(target).show();
            });
        });
    </script>
</div>
</body>
</html>