<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    @include('layouts.title')
    <meta content="Admin Dashboard" name="description" />
    <meta content="Mikumi" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <link rel="shortcut icon" href="/assets/images/favicon.ico">

    <link href="/assets/plugins/datatables/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
    <link href="/assets/plugins/datatables/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css" />

    <link href="/assets/plugins/datatables/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css" />

    <link href="/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="/assets/css/icons.css" rel="stylesheet" type="text/css" />
    <link href="/assets/css/style.css" rel="stylesheet" type="text/css" />

    <link href="/assets/css/select2.min.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .all{
            height: calc(2.25rem + 2px) !important;
        }
        .select2{
            margin-top: -4rem;
        }
        .dt-buttons{
            margin-left: -2% !important;
        }
        .has-submenu{
            padding: 0px 10px 0px 30px;
        }
        .dropdown-menu>li>a{
            font-weight: 400 !important;
        }
        .dropdown-menu {
            padding: 7px 50px;
            font-size: 15px;
            box-shadow: 0 2px 31px rgba(0, 0, 0, 0.08);
            border-color: #eff3f6;
            margin-top: 17px;
            min-width: 21rem;
        }
    </style>

</head>
<body>
@include('layouts.sales_sidebar')

<div class="wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <div class="btn-group pull-right">
                        <ol class="breadcrumb hide-phone p-0 m-0">

                        </ol>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <div class="card m-b-30">
                    <div class="card-body mx-auto">
                        <h5 class="page-title mx-3">Add  Status</h5>

                        <form class="form-horizontal"  action="/save_add_status" method="POST" enctype="multipart/form-data">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                            <div class="row container  pt-3">
                                <div class="col-md-6 ">
                                    <label>Bill of lading</label>
                                    <select class="form-control all " name="ladding_id" id="ladding_id" required>
                                        <option value="">Select Bill of lading</option>
                                        @foreach($ladding as $key => $value)
                                            <option value="{{ $key }}">{{$value}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-6">
                                    <label>Container no / Chassis no</label>
                                    <select name="cont_no" class="form-control form_size">
                                        <option value="">Container number</option>
                                    </select>
                                </div>
                                <div class="col-md-6">
                                    <label>Processing date </label>
                                    <input type="date" name="processing_date" class="form-control" placeholder="Enter Processing date " required>
                                </div>
                                <div class="col-md-6">
                                    <label>Processing status </label>
                                    {{--<select class="form-control" name="processing_status" required>--}}
                                    {{--<option value="0"> Select Status</option>--}}
                                    {{--<option value="Receiving the documents"> Receiving the documents </option>--}}
                                    {{--<option value="Under shipping line Procedures"> Under shipping line Procedures </option>--}}
                                    {{--<option value="Under Customs Procedures"> Under Customs Procedures </option>--}}
                                    {{--<option value="Release Order, Delivery Order Received"> Release Order, Delivery Order Received </option>--}}
                                    {{--<option value="Prepare for T1"> Prepare for T1 </option>--}}
                                    {{--<option value="Lodge Port charges"> Lodge Port charges </option>--}}
                                    {{--<option value="Pay Port Charges"> Pay Port Charges </option>--}}
                                    {{--<option value="Loading"> Loading </option>--}}
                                    {{--<option value="Truck/ Cargo in route Position"> Truck/ Cargo in route Position </option>--}}
                                    {{--<option value="Crossed Border"> Crossed Border </option>--}}
                                    {{--<option value="File Closed"> File Closed </option>--}}
                                    {{--<option value="Other"> Other</option>--}}

                                    {{--</select>--}}
                                    <select name="processing_status" class="form-control form_size" required>
                                        <option value=""> Select Status </option>

                                    </select>
                                </div>
                                <div class="col-md-12">
                                    <label>Any other Comment</label>
                                    <textarea class="form-control" name="other_comment" >
                                        no
                                    </textarea>
                                </div>
                                <div class="col-md-12 mt-5">
                                    <center>
                                        <button type="reset" class="btn btn-danger waves-effect" >Clear</button>
                                        <button type="submit" class="btn btn-primary waves-effect waves-light">Save changes</button>
                                    </center>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('layouts.footer')
@include('layouts.javas')


<script type="text/javascript">
    $(document).ready(function() {

        $('#ladding_id').select2();

        $('select[name="ladding_id"]').on('change', function() {
            var stateID = $(this).val();
            if(stateID) {
                $.ajax({
                    url: '/add_status/ajax/'+stateID,
                    type: "GET",
                    dataType: "json",
                    success:function(data) {


                        $('select[name="cont_no"]').empty();
                        $.each(data, function(key, value) {
                            $('select[name="cont_no"]').append('<option value="'+ key +'">'+ value +'</option>');
                        });


                    }
                });
            }else{
                $('select[name="cont_no"]').empty();
            }
        });
    });
</script>
<script type="text/javascript">
    $(document).ready(function() {

        $('#ladding_id').select2();

        $('select[name="ladding_id"]').on('change', function() {
            var stateID = $(this).val();
            if(stateID) {
                $.ajax({
                    url: '/find/status/ajax/'+stateID,
                    type: "GET",
                    dataType: "json",
                    success:function(data) {


                        $('select[name="processing_status"]').empty();
                        $.each(data, function(key, value) {
                            if(value === 'Receiving the documents') {
                                $('select[name="processing_status"]').append('<option value="'+ value +'">'+ value +'</option><option value="Under shipping line procedures">Under shipping line procedures</option>');
                            }
                            if(value === 'Under shipping line procedures') {
                                $('select[name="processing_status"]').append('<option value="'+ value +'">'+ value +'</option><option value="Under customs procedures">Under customs procedures</option><option value="Receiving the documents">Receiving the documents</option><option value="Release order/delivery order">Release order/delivery order</option><option value="Prepare T1. / IM4 Documents">Prepare T1. / IM4 Documents</option><option value="Release / Exit documents">Release / Exit documents</option> ');
                            }
                            if(value === 'Under customs procedures') {
                                $('select[name="processing_status"]').append('<option value="'+ value +'">'+ value +'</option><option value="Receiving the documents">Receiving the documents</option><option value="Under shipping line procedures">Under shipping line procedures</option> <option value="Release order, Delivery order received">Release order, Delivery order received</option>');
                            }
                            if(value === 'Release order, Delivery order received') {
                                $('select[name="processing_status"]').append('<option value="'+ value +'">'+ value +'</option><option value="Receiving the documents">Receiving the documents</option><option value="Under shipping line procedures">Under shipping line procedures</option><option value="Under customs procedures">Under customs procedures</option><option value="Prepare T1. / IM4 Documents">Prepare T1. / IM4 Documents </option>');
                            }
                            if(value === 'Release order/delivery order'){
                                $('select[name="processing_status"]').append('<option value="'+ value +'">'+ value +'</option><option value="Receiving the documents">Receiving the documents</option><option value="Under shipping line procedures">Under shipping line procedures</option><option value="Under customs procedures">Under customs procedures</option><option value="Prepare T1. / IM4 Documents">Prepare T1. / IM4 Documents </option>');
                            }
                            if(value === 'Prepare T1. / IM4 Documents') {
                                $('select[name="processing_status"]').append('<option value="'+ value +'">'+ value +'</option><option value="Receiving the documents">Receiving the documents</option><option value="Under shipping line procedures">Under shipping line procedures</option><option value="Under customs procedures">Under customs procedures</option><option value="Release / Exit documents">Release / Exit documents </option>');
                            }
                            if(value === 'Release / Exit documents') {
                                $('select[name="processing_status"]').append('<option value="'+ value +'">'+ value +'</option><option value="Receiving the documents">Receiving the documents</option><option value="Under shipping line procedures">Under shipping line procedures</option><option value="Under customs procedures">Under customs procedures</option><option value="Prepare T1. / IM4 Documents">Prepare T1. / IM4 Documents </option> <option value="Lodge Port Charges">Lodge Port Charges</option>');

                            }
                            if(value === 'Lodge Port Charges') {
                                $('select[name="processing_status"]').append('<option value="'+ value +'">'+ value +'</option><option value="Receiving the documents">Receiving the documents</option><option value="Under shipping line procedures">Under shipping line procedures</option><option value="Under customs procedures">Under customs procedures</option><option value="Prepare T1. / IM4 Documents">Prepare T1. / IM4 Documents </option> <option value="Release / Exit documents">Release / Exit documents</opton><option value="Pay Port Charges">Pay Port Charges</option>');
                            }
                            if(value === 'Pay Port Charges') {
                                $('select[name="processing_status"]').append('<option value="'+ value +'">'+ value +'</option><option value="Receiving the documents">Receiving the documents</option><option value="Under shipping line procedures">Under shipping line procedures</option><option value="Under customs procedures">Under customs procedures</option><option value="Prepare T1. / IM4 Documents">Prepare T1. / IM4 Documents </option> <option value="Release / Exit documents">Release / Exit documents</opton><option value="Lodge Port Charges">Lodge Port Charges</option><option value="Loading">Loading</option>');

                            }
                            if(value === 'Loading') {
                                $('select[name="processing_status"]').append('<option value="'+ value +'">'+ value +'</option><option value="Receiving the documents">Receiving the documents</option><option value="Under shipping line procedures">Under shipping line procedures</option><option value="Under customs procedures">Under customs procedures</option><option value="Prepare T1. / IM4 Documents">Prepare T1. / IM4 Documents </option> <option value="Release / Exit documents">Release / Exit documents</opton><option value="Lodge Port Charges">Lodge Port Charges</option><option value="Pay Port Charges">Pay Port Charges</option><option value="Truck/ Cargo in route Position">Truck/ Cargo in route Position</option>');
                            }
                            if(value === 'Truck/ Cargo in route Position') {
                                $('select[name="processing_status"]').append('<option value="'+ value +'">'+ value +'</option><option value="Receiving the documents">Receiving the documents</option><option value="Under shipping line procedures">Under shipping line procedures</option><option value="Under customs procedures">Under customs procedures</option><option value="Prepare T1. / IM4 Documents">Prepare T1. / IM4 Documents </option> <option value="Release / Exit documents">Release / Exit documents</opton><option value="Lodge Port Charges">Lodge Port Charges</option><option value="Pay Port Charges">Pay Port Charges</option><option value="Loading">Loading</option><option value="Crossed Border">Crossed Border</option>');
                            }
                            if(value === 'Crossed Border') {
                                $('select[name="processing_status"]').append('<option value="'+ value +'">'+ value +'</option><option value="Receiving the documents">Receiving the documents</option><option value="Under shipping line procedures">Under shipping line procedures</option><option value="Under customs procedures">Under customs procedures</option><option value="Release order, Delivery order received">Release order, Delivery order received</option>><option value="Prepare T1. / IM4 Documents">Prepare T1. / IM4 Documents</option><option value="Release / Exit documents">Release / Exit documents</option><option value="Lodge Port Charges">Lodge Port Charges</option><option value="Pay Port Charges">Pay Port Charges</option><option value="Loading">Loading</option><option value="Truck/ Cargo in route Position">Truck/ Cargo in route Position</option><option value="Other">Other</option>');
                            }
                            if(value === 'Other') {
                                $('select[name="processing_status"]').append('<option value="'+ value +'">'+ value +'</option><option value="Receiving the documents">Receiving the documents</option><option value="Under shipping line procedures">Under shipping line procedures</option><option value="Under customs procedures">Under customs procedures</option><option value="Release order, Delivery order received">Release order, Delivery order received</option>><option value="Prepare T1. / IM4 Documents">Prepare T1. / IM4 Documents</option><option value="Release / Exit documents">Release / Exit documents</option><option value="Lodge Port Charges">Lodge Port Charges</option><option value="Pay Port Charges">Pay Port Charges</option><option value="Loading">Loading</option><option value="Truck/ Cargo in route Position">Truck/ Cargo in route Position</option><option value="Crossed Border">Crossed Border</option><option value="Other">Other</option><option value="Cargo arrived "> Cargo arrived </option>');
                            }
                            if(value === 'Cargo arrived') {
                                $('select[name="processing_status"]').append('<option value="'+ value +'">'+ value +'</option><option value="Receiving the documents">Receiving the documents</option><option value="Under shipping line procedures">Under shipping line procedures</option><option value="Under customs procedures">Under customs procedures</option><option value="Release order, Delivery order received">Release order, Delivery order received</option>><option value="Prepare T1. / IM4 Documents">Prepare T1. / IM4 Documents</option><option value="Release / Exit documents">Release / Exit documents</option><option value="Lodge Port Charges">Lodge Port Charges</option><option value="Pay Port Charges">Pay Port Charges</option><option value="Loading">Loading</option><option value="Truck/ Cargo in route Position">Truck/ Cargo in route Position</option><option value="Crossed Border">Crossed Border</option><option value="Other">Other</option><option value="Cargo offloaded">Cargo offloaded </option>');
                            }
                            if(value === 'Cargo offloaded') {
                                $('select[name="processing_status"]').append('<option value="'+ value +'">'+ value +'</option><option value="Receiving the documents">Receiving the documents</option><option value="Under shipping line procedures">Under shipping line procedures</option><option value="Under customs procedures">Under customs procedures</option><option value="Release order, Delivery order received">Release order, Delivery order received</option>><option value="Prepare T1. / IM4 Documents">Prepare T1. / IM4 Documents</option><option value="Release / Exit documents">Release / Exit documents</option><option value="Lodge Port Charges">Lodge Port Charges</option><option value="Pay Port Charges">Pay Port Charges</option><option value="Loading">Loading</option><option value="Truck/ Cargo in route Position">Truck/ Cargo in route Position</option><option value="Crossed Border">Crossed Border</option><option value="Other">Other</option><option value="Cargo arrived">Cargo arrived</option><option value="Empty container in route to Dar-Port "> Empty container in route to Dar-Port </option>');
                            }
                            if(value === 'Empty container in route to Dar-Port') {
                                $('select[name="processing_status"]').append('<option value="'+ value +'">'+ value +'</option><option value="Receiving the documents">Receiving the documents</option><option value="Under shipping line procedures">Under shipping line procedures</option><option value="Under customs procedures">Under customs procedures</option><option value="Release order, Delivery order received">Release order, Delivery order received</option>><option value="Prepare T1. / IM4 Documents">Prepare T1. / IM4 Documents</option><option value="Release / Exit documents">Release / Exit documents</option><option value="Lodge Port Charges">Lodge Port Charges</option><option value="Pay Port Charges">Pay Port Charges</option><option value="Loading">Loading</option><option value="Truck/ Cargo in route Position">Truck/ Cargo in route Position</option><option value="Crossed Border">Crossed Border</option><option value="Other">Other</option><option value="Cargo arrived">Cargo arrived</option><option value="Cargo offloaded">Cargo offloaded </option><option value="Container drop off">Container drop off</option>');
                            }
                            if(value === 'Container drop off') {
                                $('select[name="processing_status"]').append('<option value="'+ value +'">'+ value +'</option><option value="Receiving the documents">Receiving the documents</option><option value="Under shipping line procedures">Under shipping line procedures</option><option value="Under customs procedures">Under customs procedures</option><option value="Release order, Delivery order received">Release order, Delivery order received</option>><option value="Prepare T1. / IM4 Documents">Prepare T1. / IM4 Documents</option><option value="Release / Exit documents">Release / Exit documents</option><option value="Lodge Port Charges">Lodge Port Charges</option><option value="Pay Port Charges">Pay Port Charges</option><option value="Loading">Loading</option><option value="Truck/ Cargo in route Position">Truck/ Cargo in route Position</option><option value="Crossed Border">Crossed Border</option><option value="Other">Other</option><option value="Cargo arrived">Cargo arrived</option><option value="Empty container in route to Dar-Port "> Empty container in route to Dar-Port </option><option value="File Closed">File Closed</option>');
                            }
                            if(value === 'File Closed') {
                                $('select[name="processing_status"]').append('<option value="'+ value +'">'+ value +'</option><option value="Receiving the documents">Receiving the documents</option><option value="Under shipping line procedures">Under shipping line procedures</option><option value="Under customs procedures">Under customs procedures</option><option value="Release order, Delivery order received">Release order, Delivery order received</option>><option value="Prepare T1. / IM4 Documents">Prepare T1. / IM4 Documents</option><option value="Release / Exit documents">Release / Exit documents</option><option value="Lodge Port Charges">Lodge Port Charges</option><option value="Pay Port Charges">Pay Port Charges</option><option value="Loading">Loading</option><option value="Truck/ Cargo in route Position">Truck/ Cargo in route Position</option><option value="Crossed Border">Crossed Border</option><option value="Other">Other</option><option value="Cargo arrived">Cargo arrived</option><option value="Cargo offloaded">Cargo offloaded</option><option value="Empty container in route to Dar-Port "> Empty container in route to Dar-Port </option><option value="Container drop off">Container drop off</option>');
                            }
                        });


                    }
                });
            }else{
                $('select[name="processing_status"]').empty();
            }
        });
    });
</script>

</body>
</html>