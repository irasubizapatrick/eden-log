<!DOCTYPE html>
<html lang="en">
@include('layouts.page_head')
<style>
    @import url("https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css");
    .track_tbl td.track_dot {
        width: 50px;
        position: relative;
        padding: 0;
        text-align: center;
    }
    .track_tbl td.track_dot:after {
        content: "\f111";
        font-family: FontAwesome;
        position: absolute;
        margin-left: -5px;
        top: 15px;
    }
    .track_tbl td.track_dot span.track_line {
        background: #000;
        width: 3px;
        min-height: 50px;
        position: absolute;
        height: 101%;
    }
    .size_th{
        width: 10% !important;
    }
    .track_tbl tbody tr:first-child td.track_dot span.track_line {
        top: 30px;
        min-height: 25px;
    }
    .track_tbl tbody tr:last-child td.track_dot span.track_line {
        top: 0;
        min-height: 25px;
        height: 10%;
    }
    #title{
        color: #000000;
    }
</style>
<body>
<section>

</section>

<section class="timeline_tracking_area ">
    <div class="container">
        <a href="/cargo/tracking" role="button" class="btn btn-success btn-lg" type="submit" style="border-radius: 0 !important; margin-top: 6%;  background: #ff8b00;border-color: #ff8b00;"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back to Tracking </a>

        <div class="p-4" style="margin-top: 3%;">
            <h3 style="color: #000000"> Your Shipment Tracking Results</h3>
            <table class="table table-bordered track_tbl" style="margin-top: 3%;">
                <thead>
                <tr id="title">
                    <th >
                        <center>
                            #
                        </center>
                    </th>
                    <th class="size_th">Date</th>
                    <th class="size_th">Bill of lading</th>
                    <th >Container List</th>
                    <th >Position</th>
                    <th >Remarks</th>
                </tr>
                </thead>
                <tbody>
                <?php $i=1;?>
                @forelse($track AS $truck)
                    <tr class="active">
                        <td class="track_dot" id="title">
                            <span class="track_line"></span>
                        </td>
                        <td id="title">{{$truck->processing_date}}</td>
                        <td id="title">{{$truck->bill_lading}}</td>
                        <td id="title">
                            @foreach($cont as $container)
                                <div class="checkpoint__content"><p> {{$container->cont_no}}</p></div>
                            @endforeach

                        </td>
                        <td id="title">{{$truck->processing_status}}</td>
                        <td id="title"> {{$truck->other_comment}}</td>
                    </tr>
                @empty
                    <tr>

                        <div class="hint">
                            <span style="color: red; font-size: large; margin-left: 44rem;"> no  results</span>
                        </div>

                    </tr>


                @endforelse
                </tbody>
            </table>
        </div>
    </div>
</section>




@include('layouts.page_footer')
</body>
</html>
<!-- Localized -->