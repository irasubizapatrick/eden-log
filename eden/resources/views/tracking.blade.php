<!DOCTYPE html>
<html lang="en">
@include('layouts.page_head')
<body>
@include('layouts.page_header')

<section class="tracking_search_area">
    <div class="container">
        <form role="form" class="form-horizontal" action="/tracking_cargo" method="POST" enctype="multipart/form-data">
            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
            <div class="tracking_search_inner">
                <h2 class="single_title">Track your Shipment</h2>
                <h5>Enter a tracking number or bill of lading, and get tracking results.</h5>
                <div class="row">
                    <div class="col-md-12">
                            <input type="text" class="form-control" name="track" placeholder="Enter Bill of lading ">
                    </div>

                    <strong>Or </strong>
                    <div class="col-md-12" style="margin-top: 2%;">
                            <input type="text" class="form-control" name="cont_no" placeholder="Enter Container Number ">
                    </div>
                    <div class="col-md-4">
                         <button class="btn btn-success btn-lg" type="submit" style="border-radius: 0 !important; margin-top: 6%;"><i class="fa fa-circle-o-notch" aria-hidden="true"></i> Track</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</section>
<footer class="footer_area">
    <div class="footer_widget_area">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-xs-6">
                    <aside class="f_widget about_widget">
                        {{--<img src="landing/img/footer-logo.png" alt="">--}}
                        <h4 style="color: #ffffff;">Mikumi Freight Forwarders Ltd</h4>
                        <p>Mikumi Freight Forwarders Limited, is a fully licensed private company which engages in the provision of high quality domestic and international freight</p>
                        <ul>
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                            <li><a href="#"><i class="fa fa-pinterest"></i></a></li>
                        </ul>
                    </aside>
                </div>
                <div class="col-md-3 col-xs-6">
                    <aside class="f_widget link_widget">
                        <div class="f_title">
                            <h3>Quick Links</h3>
                        </div>
                        <ul>
                            <li><a href="#">INVESTER RELATIONS</a></li>
                            <li><a href="#">PRESS & MEDIA</a></li>
                            <li><a href="#">COOKIE POLICY</a></li>
                            <li><a href="#">TERMS & CONDITIONS</a></li>
                        </ul>
                    </aside>
                </div>
                <div class="col-md-3 col-xs-6">
                    <aside class="f_widget service_widget">
                        <div class="f_title">
                            <h3>Services</h3>
                        </div>
                        <ul>
                            <li><a href="#">Standard Air Freight Services</a></li>
                            <li><a href="#">Sea Freight Services</a></li>
                            <li><a href="#">Full loads and part loads</a></li>
                            <li><a href="#">Specialized Transport</a></li>
                        </ul>
                    </aside>
                </div>
                <div class="col-md-3 col-xs-6">
                    <aside class="f_widget info_widget">
                        <div class="f_title">
                            <h3>Contact</h3>
                        </div>
                        <div class="contact_details">
                            <p>Harbour View Tower 8th Floor , room 808,  Samora Avenue, Dar es salaam, Tanzania</p>
                            <p>Phone: <a href="tel:+1-(255)-7899">+255 714 307 315</a></p>
                            <p>Fax: <a href="#">+255 222 112 536/37</a></p>
                            <p>Email: <a href="#"><span class="__cf_email__" data-cfemail="eb838e878784ab87848c82989f828898c5888486" >info@mikumifreight.com</span></a></p>
                        </div>
                    </aside>
                </div>
            </div>
        </div>
    </div>
    <div class="footer_copy_right">
        <div class="container">
            <h4>Copyright ©
                <script data-cfasync="false" src="../../cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script><script>
                    document.write(new Date().getFullYear());
                </script>. All rights reserved.</h4>
        </div>
    </div>
</footer>

@include('layouts.page_footer')
</body>
</html>
<!-- Localized -->