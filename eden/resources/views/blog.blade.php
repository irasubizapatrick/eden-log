<!DOCTYPE html>
<html lang="en">
@include('layouts.page_head')
<body>


@include('layouts.page_header')
<section class="banner_area">
    <div class="container">
        <div class="pull-left">
            <h3>Blog</h3>
        </div>
        <div class="pull-right">
            <a href="/">Home</a>
            <a href="/blog">Blog</a>
        </div>
    </div>
</section>
<section class="main_blog_area">
    <div class="container">
        <div class="row main_blog_inner">
            <div class="col-md-9">
                <div class="main_blog_items">
                    <div class="main_blog_item">
                        <div class="main_blog_image">
                            <img src="/landing/img/blog/blog-4.jpg" alt="">
                        </div>
                        <div class="main_blog_text">
                            <a href="#"><h2>How we can capture the market sales</h2></a>
                            <div class="blog_author_area">
                                <a href="#"><i class="fa fa-user"></i>By : <span>Admin</span></a>
                                <a href="#"><i class="fa fa-tag"></i><span>finance</span> / <span>investment</span></a>
                                <a href="#"><i class="fa fa-comments-o"></i>Comments: 5</a>
                            </div>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur velit esse cillum dolore eu ...</p>
                            <a class="more_btn" href="#">Read more</a>
                        </div>
                    </div>
                </div>
                <nav aria-label="Page navigation" class="blog_pagination">
                    <ul class="pagination">
                        <li class="active"><a href="#">1</a></li>
                        <li><a href="#">2</a></li>
                        <li>
                            <a href="#" aria-label="Next">
                                <i class="fa fa-angle-right" aria-hidden="true"></i>
                            </a>
                        </li>
                    </ul>
                </nav>
            </div>
            <div class="col-md-3">
                <div class="sidebar_area">
                    <aside class="r_widget search_widget">
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Enter Search Keywords">
                            <span class="input-group-btn">
                                <button class="btn btn-default" type="button"><i class="fa fa-search"></i></button>
                            </span>
                        </div>
                    </aside>
                    <aside class="r_widget categories_widget">
                        <div class="r_widget_title">
                            <h3>Categories</h3>
                        </div>
                        <ul>
                            <li><a href="#">Legal Consultancy <i class="fa fa-angle-right" aria-hidden="true"></i></a></li>
                            <li><a href="#">Bonds Investiment <i class="fa fa-angle-right" aria-hidden="true"></i></a></li>
                            <li><a href="#">Business <i class="fa fa-angle-right" aria-hidden="true"></i></a></li>
                            <li><a href="#">General Consultancy <i class="fa fa-angle-right" aria-hidden="true"></i></a></li>
                            <li><a href="#">Sales Analysis <i class="fa fa-angle-right" aria-hidden="true"></i></a></li>
                            <li><a href="#">Investment <i class="fa fa-angle-right" aria-hidden="true"></i></a></li>
                        </ul>
                    </aside>
                    <aside class="r_widget recent_widget">
                        <div class="r_widget_title">
                            <h3>Recent News</h3>
                        </div>
                        <div class="recent_inner">
                            <div class="recent_item">
                                <a href="#"><h4>5 tips for control your financial investments.</h4></a>
                                <h5>08 March 2017</h5>
                            </div>
                            <div class="recent_item">
                                <a href="#"><h4>5 tips for control your financial investments.</h4></a>
                                <h5>08 March 2017</h5>
                            </div>
                        </div>
                    </aside>
                </div>
            </div>
        </div>
    </div>
</section>


<footer class="footer_area">
    <div class="footer_widget_area">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-xs-6">
                    <aside class="f_widget about_widget">
                        {{--<img src="landing/img/footer-logo.png" alt="">--}}
                        <h4 style="color: #ffffff;">Mikumi Freight Fowarders Ltd</h4>
                        <p>Mikumi Freight Forwarders Limited, is a fully licensed private company which engages in the provision of high quality domestic and international freight</p>
                        <ul>
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                            <li><a href="#"><i class="fa fa-pinterest"></i></a></li>
                        </ul>
                    </aside>
                </div>
                <div class="col-md-3 col-xs-6">
                    <aside class="f_widget link_widget">
                        <div class="f_title">
                            <h3>Quick Links</h3>
                        </div>
                        <ul>
                            <li><a href="#">INVESTER RELATIONS</a></li>
                            <li><a href="#">PRESS & MEDIA</a></li>
                            <li><a href="#">COOKIE POLICY</a></li>
                            <li><a href="#">TERMS & CONDITIONS</a></li>
                        </ul>
                    </aside>
                </div>
                <div class="col-md-3 col-xs-6">
                    <aside class="f_widget service_widget">
                        <div class="f_title">
                            <h3>Services</h3>
                        </div>
                        <ul>
                            <li><a href="#">Standard Air Freight Services</a></li>
                            <li><a href="#">Sea Freight Services</a></li>
                            <li><a href="#">Full loads and part loads</a></li>
                            <li><a href="#">Specialized Transport</a></li>
                        </ul>
                    </aside>
                </div>
                <div class="col-md-3 col-xs-6">
                    <aside class="f_widget info_widget">
                        <div class="f_title">
                            <h3>Contact</h3>
                        </div>
                        <div class="contact_details">
                            <p>Harbour View Tower 8th Floor , room 808,  Samora Avenue, Dar es salaam, Tanzania</p>
                            <p>Phone: <a href="tel:+1-(255)-7899">+255 714 307 315</a></p>
                            <p>Fax: <a href="#">+255 222 112 536/37</a></p>
                            <p>Email: <a href="#"><span class="__cf_email__" data-cfemail="eb838e878784ab87848c82989f828898c5888486">info@mikumifreight.com</span></a></p>
                        </div>
                    </aside>
                </div>
            </div>
        </div>
    </div>
    <div class="footer_copy_right">
        <div class="container">
            <h4>Copyright ©
                <script data-cfasync="false" src="../../cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script><script>
                    document.write(new Date().getFullYear());
                </script>. All rights reserved.</h4>
        </div>
    </div>
</footer>

@include('layouts.page_footer')
</body>
</html>
<!-- Localized -->