
<!DOCTYPE html>
<html lang="en">
@include('layouts.page_head')

<body>

@include('layouts.page_header')


<section class="banner_area">
    <div class="container">
        <div class="pull-left">
            <h3>About Us</h3>
        </div>
        <div class="pull-right">
            <a href="/">Home</a>
            <a href="/about">About Us</a>
        </div>
    </div>
</section>


<section class="our_about_area">
    <div class="container">
        <div class="row">
            <div class="col-md-5">
                <div class="our_about_image">
                    <img src="/landing/img/about-right-1.jpg" alt="">
                </div>
            </div>
            <div class="col-md-7">
                <div class="our_about_left_content">
                    <h4 class="title"> When you partner with Mikumi Logistics Limited, you partner with Tanzania’s premiere logistics service provider.</h4>
                    <p class="title_desc" id="one_paragraph">Our core business encompasses integrated logistics, international freight forwarding and supply chain solutions. Our expertise extends from handling merchandise and non-merchandise, to POSM and more.The choice of many Top 100 Brands, Mikumi Logistics Limited is Tanzania’s premier logistics service provider.
                    </p>
                </div>
            </div>
        </div>
        <p class="title_desc" id="one_paragraph">
            We employ a far-reaching global network that stretches across six continents, and includes the largest distribution network and hub operations in Africa. Many of Top 100 Brands employ our services across a wide spectrum of industries including fashion & lifestyle, electronics & technology, food & beverage to industrial & material science, automotive, and pharmaceutical & healthcare. As an asset-based organisation, we offer our customers considerable reliability and flexibility in supporting their continuing growth and expansion in Africa and the wider world. Our solution-based mindset and can-do attitude can be found in each and every one of our professional employees and partners across the globe.
            Complete connectivity, accessibility and visibility are provided through our supply chain visibility system.With world-class assets, industry experts and award winning IT systems and process, we deliver your products faster and more cost-effective than anyone else.
        </p>
    </div>
</section>


<section class="global_text_area">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="global_text_item">
                    <h3 class="single_title">Our Global Mission</h3>
                    <p>At Mikumi Logistics, we have a goal-oriented vision that clearly identifies where we’re going and how we’re going to get there. With a view to being the East and Central Africa’s leading provider of logistics solutions we continue to further develop our position as a global market leader in transport and logistics solutions.
                        To achieve this vision and meet our goals, we proactively and constantly look for new possibilities. Therefore, an important part of our vision is to attract and retain the best employees, best technologies in the market.</p>
                </div>
            </div>
            <div class="col-md-6">
                <div class="global_text_item">
                    <h3 class="single_title">We Are Growing Faster</h3>
                    <p>We meet our customers’ demands for a personal and professional service by offering innovative supply chain solutions for global sea, air, and road transportation as well as certain specialist services.</p>
                </div>
            </div>
        </div>
    </div>
</section>

@include('layouts.web_footer')


@include('layouts.page_footer')
</body>
</html>
<!-- Localized -->