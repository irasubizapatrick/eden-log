<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    @include('layouts.title')
    <meta content="Admin Dashboard" name="description" />
    <meta content="Logistics" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <!-- App Icons -->
    <link rel="shortcut icon" href="/assets/images/favicon.ico">

    <!-- App css -->
    <link href="/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="/assets/css/icons.css" rel="stylesheet" type="text/css" />
    <link href="/assets/css/style.css" rel="stylesheet" type="text/css" />

    <style type="text/css">
        .logo{
            width: 100%;
            height:100%;
        }
    </style>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-142579219-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-142579219-1');
    </script>

</head>


<body>

<!-- Begin page -->
<div class="accountbg"></div>
<div class="wrapper-page">

    <div class="card">
        <div class="card-body">
            <h3 class="text-center mt-0 m-b-15">
                <a href="/" class="logo logo-admin "><img src="assets/images/logo.jpeg" height="150"  alt="logo"></a>
            </h3>
            <h4 class="text-muted text-center font-18"><b>Enter Bill of ladding Numberss or Container Number</b></h4>

            <div class="row">
                <div class="col-12">
                <form class="form-horizontal"  action="/find_truck_form" method="POST" enctype="multipart/form-data">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                    <div class="row">
                        <div class="col-md-12">
                            <input type="text" class="form-control" name="order_track_number" placeholder="Enter Trucking Number " >
                        </div>
                        <div class="col-md-12">
                            <input type="text" class="form-control" name="order_track_number" placeholder="Enter Trucking Numbers " >
                        </div>
                        <div class="col-md-12">
                                <div class="modal-footer">
                                    <button type="submit" class="btn btn-primary mx-auto waves-effect waves-light">Submit</button>
                                </div>
                        </div>
                    </div>
                </form>
                </div> <!-- end col -->
            </div> <!-- end row -->

        </div>
    </div>
</div>



<!-- jQuery  -->
<script src="/assets/js/jquery.min.js"></script>
<script src="/assets/js/popper.min.js"></script>
<script src="/assets/js/bootstrap.min.js"></script>
<script src="/assets/js/modernizr.min.js"></script>
<script src="/assets/js/waves.js"></script>
<script src="/assets/js/jquery.slimscroll.js"></script>
<script src="/assets/js/jquery.nicescroll.js"></script>
<script src="/assets/js/jquery.scrollTo.min.js"></script>

<!-- App js -->
<script src="/assets/js/app.js"></script>

</body>
</html>
<!-- Localized -->
