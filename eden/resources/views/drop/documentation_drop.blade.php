<!DOCTYPE html>
<html>

@include('layouts.finance_head')
<style type="text/css">
    .form_size{
        height: calc(3.25rem + 2px) !Important;
    }
    .select2{
        width: 100% !important;
        margin-top: -4rem;
    }
</style>
<body>
@include('layouts.documentation_sidebar')


<div class="wrapper">
    <div class="container-fluid">


    </div>
    <div class="row m-0  p-3">
        <div class="col-sm-12 ">
            <div class="page-title-box">
                <div class="btn-group pull-right">
                    <ol class="breadcrumb hide-phone p-0 m-0">
                        <li class="breadcrumb-item"><a href="#">All Drop Off List</a></li>
                        <li class="breadcrumb-item active">Dashboard</li>
                    </ol>
                </div>
                <h4 class="page-title">All Drop Off </h4>
            </div>
        </div>
        <div class="col-12 container" style="margin-top: -3rem;">
            <div class="card m-b-30 ">
                <div class="card-body">
                    <div class="modal fade bill_lading"  role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title mt-0" id="myLargeModalLabel">Search By Container </h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                </div>
                                <div class="modal-body">
                                    <form class="form-horizontal"  action="/search/bill/drop" method="POST" enctype="multipart/form-data">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                        <div class="row col-md-12">
                                            <div class="col-md-6">
                                                <label>Container Number</label>
                                                <select class="form-control form_size" name="container_id" id="ladding_id">
                                                    <option value="">Container number</option>
                                                    @foreach($cont_bill as $key)
                                                        <option value="{{$key->id}}">{{$key->cont_no}}</option>
                                                    @endforeach

                                                </select>
                                            </div>
                                            <div class="col-md-12 pt-4 pb-4">
                                                <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
                                                <button type="submit" class="btn btn-primary waves-effect waves-light">Search </button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="modal fade bs-example-modal-lg"  role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title mt-0 mx-4" id="myLargeModalLabel">Assign Drop Off to a Container</h4>
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                </div>
                                <div class="modal-body">
                                    <form class="form-horizontal"  action="/all_drop" method="POST" enctype="multipart/form-data">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                        <div class="row col-md-12">

                                            <div class="col-md-6">
                                                <label>Container Number</label>
                                                <select class="form-control form_size " name="container_id" id="containerList"  onchange="getcarDetails(this.value);" required>

                                                    <option value="">Container number</option>
                                                    @foreach($cont_bill as $key)
                                                        <option value="{{$key->id}}">{{$key->cont_no}}</option>
                                                    @endforeach

                                                </select>
                                            </div>
                                            <div class="col-md-6 mt-3">
                                                <label>Bill of lading</label>
                                                <input type="text" class="form-control"  value="0" name="bill_lading" id="Bill"  placeholder="Enter Drop off  name" required>

                                            </div>
                                            <div class="col-md-6">
                                                <label>Drop off Date</label>
                                                <input type="date" class="form-control"  value="0" name="dropOff_date" placeholder="Enter Drop off  name" required>
                                            </div>
                                            <div class="col-md-6">
                                                <label>Drop Off</label>
                                                <input type="text" class="form-control"  value="0" name="drop_name"  placeholder="Enter Drop off  name" required>
                                            </div>
                                            <div class="col-md-6">
                                                <label>Truck Number</label>
                                                <input type="text" class="form-control"   id="Test"  name="trucking_number" placeholder="Enter truck Number  name" required>
                                            </div>
                                            <div class="col-md-6">
                                                <label>Transporter Name</label>
                                                <input type="text" class="form-control"  id="Trans" name="transporter_name" placeholder="Enter transporter name" required>
                                            </div>
                                            <div class="col-md-6">
                                                <label>Interchange  </label>
                                                <input type="file" class="form-control"   id="Text2" name="drop_document" placeholder="Enter transporter name" >
                                            </div>

                                            <div class="col-md-6">
                                                <label>Out Word Document</label>
                                                <input type="file" class="form-control" name="outWord" >
                                            </div>
                                            <div class="col-md-12 mx-auto  pt-5 pb-5">
                                                <center>

                                                    <button type="submit" class="btn btn-primary waves-effect waves-light">Save changes</button>
                                                    <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Close</button>
                                                </center>

                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <a href="/all_drop" role="button" class="btn btn-success waves-effect waves-light pull-right mx-3"><i class="fa fa-home"></i><span> All</span></a>

                    <button type="button" class="btn btn-primary waves-effect waves-light pull-right" data-toggle="modal" data-target=".bs-example-modal-lg "><i class="fa fa-plus"></i><span> Add Drop off</span></button>
                    <button type="button" class="btn btn-warning waves-effect waves-light pull-right mx-4" data-toggle="modal" data-target=".bill_lading "><i class="fa fa-search"></i><span>Search </span></button>

                    @if (Session::has('message'))
                        <div class="alert alert-success alert-dismissable">
                            <a href="" class="close" data-dismiss="alert" aria-label="close">
                                &times;

                            </a>
                            {{ Session::get('message') }}
                        </div>

                    @endif
                    @if (Session::has('delete'))
                        <div class="alert alert-success alert-dismissable">
                            <a href="" class="close" data-dismiss="alert" aria-label="close">
                                &times;

                            </a>
                            {{ Session::get('delete') }}
                        </div>

                    @endif
                    @if (Session::has('updated'))
                        <div class="alert alert-success alert-dismissable">
                            <a href="" class="close" data-dismiss="alert" aria-label="close">
                                &times;

                            </a>
                            {{ Session::get('updated') }}
                        </div>

                    @endif
                    <table id="datatable-buttons" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                        <tr style="font-size: 13px;background-color: #416b8d !important;color: #fff;">
                            <th>#</th>
                            <th>Date</th>
                            <th>Bill of Loading</th>
                            <th>Container No</th>
                            <th>Drop off  Area </th>
                            <th>Truck Number</th>
                            <th>Transporter Name</th>
                            <th>Interchange </th>
                            <th>Out word Doc</th>
                            <th>Action</th>
                        </tr>
                        </thead>


                        <tbody>
                        <?php $i=1;?>
                        @foreach($drop AS $value)
                            <tr>
                                <td>{{$i++}}</td>
                                <td>{{$value->dropOff_date}}</td>
                                <td>{{$value->bill_lading}}</td>
                                <td>{{$value->cont_no}}</td>
                                <td>{{$value->drop_name}}</td>
                                <td>{{$value->trucking_number}}</td>
                                <td>{{$value->transporter_name}}</td>

                                <td>
                                    @if($value->drop_document !="")

                                        <a href="/dropOffDocument/{{$value->drop_document}}"  target="_blank">file</a>
                                    @else
                                        <a href="#"  target="_blank">no file</a>
                                    @endif
                                </td>
                                <td>
                                    @if($value->outWord !="")

                                        <a href="/dropOffDocument/{{$value->outWord}}"  target="_blank">file</a>
                                    @else
                                        <a href="#"  target="_blank" style="color: red">no file</a>
                                    @endif
                                </td>
                                <td>
                                    <button data-toggle="modal" data-target="#editleads<?php echo $i;?>" class="pull-left edit  btn-lg  btn btn-primary dlt_sm_table mx-2 mt-2"><i class="fa fa-pencil"></i></button>

                                    <button data-toggle="modal" data-target="#delete<?php echo $i;?>" class="pull-left edit  btn-lg  btn btn-danger dlt_sm_table mx-2 mt-2"><i class="fa fa-trash"></i></button>
                                    <div class="modal fade" id="delete<?php echo $i;?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLarge01" style="display: none;" aria-hidden="true">
                                        <div class="modal-dialog modal-lg" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h4  class="modal-title mx-2">Delete </h4>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">×</span>
                                                    </button>
                                                </div>
                                                <form class="pull-left" action="/all_drop/{{$value->id}}" method="POST">
                                                    <label class="mx-4">Are you sure you want to delete</label>
                                                    <input type="hidden" name="_method" value="DELETE" />
                                                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                                    <div class="modal-footer pb-4">
                                                        <button type="submit" class="btn btn-success">Confirm</button>
                                                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="editleads<?php echo $i;?>" class="modal fade"  role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
                                        <div class="modal-dialog modal-lg">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h4 class="modal-title mx-2" id="editleads"> Edit Drop Off for this deal</h4>
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

                                                </div>
                                                <div class="modal-body">
                                                    <div class="row">
                                                        <form role="form" action="/all_drop/{{$value->id}}" method="post" enctype="multipart/form-data">
                                                            <input type="hidden" name="_method" value="PUT" />
                                                            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                                            <div class="row col-md-12">
                                                                <div class="col-md-6">
                                                                    <label>Drop off Date</label>
                                                                    <input type="date" class="form-control"  value="{{$value->dropOff_date}}" name="dropOff_date" placeholder="Enter Drop off  name" required>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <label>Container Number</label>
                                                                    <select class="form-control form_size" name="container_id" id="edit_ladding_id"  onchange="getcarDetails(this.value);" required>

                                                                        <option value="{{$value->container_id}}">{{$value->cont_no}}</option>
                                                                        @foreach($cont_bill as $key)
                                                                            <option value="{{$key->id}}">{{$key->cont_no}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                                <div class="col-md-6 mt-3">
                                                                    <label>Bill of lading</label>
                                                                    <label>Bill of lading</label>
                                                                    <input type="text" class="form-control"  value="{{$value->bill_lading}}" name="bill_lading" id="BilEdit"  placeholder="Enter Drop off  name" required>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <label>Drop Off</label>
                                                                    <input type="text" class="form-control"  value="{{$value->drop_name}}" name="drop_name"  placeholder="Enter Drop off  name" required>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <label>Truck Number</label>
                                                                    <input type="text" class="form-control"   id="Test"  value="{{$value->trucking_number}}" name="trucking_number" placeholder="Enter truck Number  name" required>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <label>Transporter Name</label>
                                                                    <input type="text" class="form-control"  id="Trans" value="{{$value->transporter_name}}" name="transporter_name" placeholder="Enter transporter name" required>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <label>Interchange </label>
                                                                    <input type="file" class="form-control"   id="Text2" value="{{$value->drop_document}}" name="drop_document" placeholder="Enter transporter name" >
                                                                </div>


                                                                <div class="col-md-6">
                                                                    <label>Out Word Document</label>
                                                                    <input type="file" class="form-control" name="outWord" >
                                                                </div>
                                                                <div class="col-md-6 mx-auto pt-4 pb-4">
                                                                    <center>
                                                                        <button type="submit" class="btn btn-primary waves-effect waves-light">Save changes</button>
                                                                        <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Close</button>
                                                                    </center>
                                                                </div>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <span class="float-right">
                        <?php echo $driver->appends(Request::all())->fragment('foo')->render(); ?>

                     </span>

                </div>
            </div>
        </div>
    </div>
</div>
</div>
@include('layouts.footer')

@include('layouts.javas')
<script type="text/javascript">
    $(document).ready(function() {
        $('select[name="container_id"]').on('change', function() {
            var stateID = $(this).val();
            if(stateID) {
                $.ajax({
                    url: '/add_drop/ajax/'+stateID,
                    type: "GET",
                    dataType: "json",
                    success:function(data) {


                        $('select[name="ladding_id"]').empty();
                        $.each(data, function(key, value) {
                            $('select[name="ladding_id"]').append('<option value="'+ key +'">'+ value +'</option>');
                        });


                    }
                });
            }else{
                $('select[name="container_id"]').empty();
            }
        });
    });
</script>
<script type="text/javascript">
    var getcarDetails = function (id) {
        var str = '/cont_no/'+ id;
        $.ajax({
            url: str,
            type: 'get',
            async: false,
            success: function (data) {
                console.log(data);
                document.getElementById('Test').value = data.trucking_number;
                document.getElementById('Trans').value = data.transporter_name;
                document.getElementById('Bill').value = data.bill_lading;
                document.getElementById('BilEdit').value = data.bill_lading;

            }
        });

    };
</script>

</body>
</html>