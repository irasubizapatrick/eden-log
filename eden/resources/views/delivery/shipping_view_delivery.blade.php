<!DOCTYPE html>
<html>

@include('layouts.finance_head')
<style>

    @media print {
        #print_button {
            display: none;
        }
        .print_button {
            display: none;
        }
        letter_header{
            margin-left: 1rem;
        }
        .cargo_name{
            font-weight: 900 !important;
        }
        .company_details{

            margin-left: 82px !important;
            color: black !important;
        }
    }

    .receipt-content .logo a:hover {
        text-decoration: none;
        color: #7793C4;
    }

    .receipt-content .invoice-wrapper {
        background: #FFF;
        box-shadow: 0px 0px 1px #CCC;
        padding: 40px 40px 60px;
        border-radius: 4px;
    }

    .receipt-content .invoice-wrapper .payment-details span {
        color: #A9B0BB;
        display: block;
    }
    .receipt-content .invoice-wrapper .payment-details a {
        display: inline-block;
        margin-top: 5px;
    }

    .receipt-content .invoice-wrapper .line-items .print a {
        display: inline-block;
        border: 1px solid #9CB5D6;
        padding: 13px 13px;
        border-radius: 5px;
        color: #708DC0;
        font-size: 13px;
        -webkit-transition: all 0.2s linear;
        -moz-transition: all 0.2s linear;
        -ms-transition: all 0.2s linear;
        -o-transition: all 0.2s linear;
        transition: all 0.2s linear;
    }

    .receipt-content .invoice-wrapper .line-items .print a:hover {
        text-decoration: none;
        border-color: #333;
        color: #333;
    }

    .receipt-content {
        background: #ECEEF4;
    }
    @media (min-width: 1200px) {
        .receipt-content .container {width: 900px; }
    }

    .receipt-content .logo {
        text-align: center;
        margin-top: 50px;
    }

    .receipt-content .logo a {
        font-family: Myriad Pro, Lato, Helvetica Neue, Arial;
        font-size: 36px;
        letter-spacing: .1px;
        color: #555;
        font-weight: 300;
        -webkit-transition: all 0.2s linear;
        -moz-transition: all 0.2s linear;
        -ms-transition: all 0.2s linear;
        -o-transition: all 0.2s linear;
        transition: all 0.2s linear;
    }

    .receipt-content .invoice-wrapper .intro {
        line-height: 25px;
        color: #444;
    }

    .receipt-content .invoice-wrapper .payment-info {
        margin-top: 25px;
        padding-top: 15px;
    }

    .receipt-content .invoice-wrapper .payment-info span {
        color: #A9B0BB;
    }

    .receipt-content .invoice-wrapper .payment-info strong {
        display: block;
        color: #444;
        margin-top: 3px;
    }

    @media (max-width: 767px) {
        .receipt-content .invoice-wrapper .payment-info .text-right {
            text-align: left;
            margin-top: 20px; }
    }
    .receipt-content .invoice-wrapper .payment-details {
        margin-top: 30px;
        padding-top: 20px;
        line-height: 22px;
    }


    @media (max-width: 767px) {
        .receipt-content .invoice-wrapper .payment-details .text-right {
            text-align: left;
            margin-top: 20px; }
    }
    .receipt-content .invoice-wrapper .line-items {
        margin-top: 40px;
    }
    .receipt-content .invoice-wrapper .line-items .headers {
        color: #A9B0BB;
        font-size: 13px;
        letter-spacing: .3px;
        border-bottom: 2px solid #EBECEE;
        padding-bottom: 4px;
    }
    .receipt-content .invoice-wrapper .line-items .items {
        margin-top: 8px;
        border-bottom: 2px solid #EBECEE;
        padding-bottom: 8px;
    }
    .receipt-content .invoice-wrapper .line-items .items .item {
        padding: 10px 0;
        color: #696969;
        font-size: 15px;
    }
    @media (max-width: 767px) {
        .receipt-content .invoice-wrapper .line-items .items .item {
            font-size: 13px; }
    }
    .receipt-content .invoice-wrapper .line-items .items .item .amount {
        letter-spacing: 0.1px;
        color: #84868A;
        font-size: 16px;
    }
    @media (max-width: 767px) {
        .receipt-content .invoice-wrapper .line-items .items .item .amount {
            font-size: 13px; }
    }

    .receipt-content .invoice-wrapper .line-items .total {
        margin-top: 30px;
    }

    .receipt-content .invoice-wrapper .line-items .total .extra-notes {
        float: left;
        width: 40%;
        text-align: left;
        font-size: 13px;
        color: #7A7A7A;
        line-height: 20px;
    }

    @media (max-width: 767px) {
        .receipt-content .invoice-wrapper .line-items .total .extra-notes {
            width: 100%;
            margin-bottom: 30px;
            float: none; }
    }

    .receipt-content .invoice-wrapper .line-items .total .extra-notes strong {
        display: block;
        margin-bottom: 5px;
        color: #454545;
    }

    .receipt-content .invoice-wrapper .line-items .total .field {
        margin-bottom: 7px;
        font-size: 14px;
        color: #555;
    }

    .receipt-content .invoice-wrapper .line-items .total .field.grand-total {
        margin-top: 10px;
        font-size: 16px;
        font-weight: 500;
    }

    .receipt-content .invoice-wrapper .line-items .total .field.grand-total span {
        color: #20A720;
        font-size: 16px;
    }

    .receipt-content .invoice-wrapper .line-items .total .field span {
        display: inline-block;
        margin-left: 20px;
        min-width: 85px;
        color: #84868A;
        font-size: 15px;
    }

    .receipt-content .invoice-wrapper .line-items .print {
        margin-top: 50px;
        text-align: center;
    }

    .titles{
        background-color: none !important;
        border: 1px solid black;
        text-align:left;
    }


    .receipt-content .invoice-wrapper .line-items .print a i {
        margin-right: 3px;
        font-size: 14px;
    }

    .receipt-content .footer {
        margin-top: 40px;
        margin-bottom: 110px;
        text-align: center;
        font-size: 12px;
        color: #969CAD;
    }
    div[class^="col-md-6"], div[class*=" tocolor-"] {
        background-color: #eee;
        border: 1px solid black;
        text-align:left;
    }
</style>
<body>
<div class="wrapper">
    <div class="row m-0 p-0">
        <div class="col-sm-12">
            <div class="page-title-box">

                <a href="{{URL::previous()}}" class="btn btn-danger print_button " > <i class="fa fa-arrow-left"></i> Back</a>
                <a href="#" id="print_button" class="btn btn-success" onclick="window.print()" >
                    <i class="fa fa-print"></i>
                    Print this receipt
                </a>
            </div>
        </div>
        <div class="col-12 container" style="margin-top: -3rem;">
            <div class="card m-b-30">
                <div class="receipt-content">
                    <div class="container bootstrap snippet">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="invoice-wrapper" style="margin-top: -15rem;">
                                    <div class="payment-details">
                                        <div class="row" style="margin-top: -6rem;border-bottom: 2px solid #80808038;padding-bottom: 2rem;">
                                            <div class="col-sm-6 float-left">
                                                <img src="/assets/images/slogo.jpeg" alt="..." style="width: 60%;margin-left: -17px;">
                                            </div>
                                            <div class="col-sm-6 " style="margin-top: -9rem;margin-left: 459px;color: black;">
                                                <i class="fa fa-map-marker company_details"> Harbour View Tower | 8th Floor, room 808 <br>P.O.Box 9163, Samora Avenue , Dar es salaam, Tanzania</i><br>
                                                <i class="fa fa-phone company_details" style="zcolor: black;font-weight: 600;"> (+255) 22 2112536/37 </i> & +255 714 307 315</i><br>
                                                <i class="fa fa-envelope company_details"> info@mikumifreight.com</i>
                                            </div>
                                        </div>
                                    </div>
                                    <h1 class="text-center mt-5" style="font-size: xx-large;">DELIVERY NOTE</h1>
                                    @foreach($delivery as $value)
                                        <h3 style="margin-left: -2%!important;     font-size: x-large;">CONSIGNEE ADDRESS</h3>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <p class="pull-left">
                                                    <strong style="font-size: large;">    Name:           </strong> {{$value->client_name}} <br>
                                                    <strong style="font-size: large;">    Telephone:      </strong> {{$value->telephone}} <br>
                                                    <strong style="font-size: large;">    Destination:    </strong> {{$value->destination}}
                                                </p>
                                            </div>
                                            <div class="col-md-6">
                                                <p class="pull-left">
                                                    <strong style="font-size: large;">    Loading Date:          </strong> {{$value->loading_date}} <br>

                                                    <strong style="font-size: large;">    Ref:      </strong> {{$value->file_no}} <br>
                                                </p>
                                            </div>
                                        </div>
                                        <br>
                                        <div class="row titles">
                                            <div class="col-md-6">
                                                <h3 class="cargo_name" style="margin-left: -2%!important;font-size: x-large;">CARGO DETAILS</h3>
                                            </div>
                                            <div class="col-md-6">
                                                <h3 style="margin-left: -2%!important; font-size: larger; font-size: x-large;" class="cargo_name">TRANSPORTER / DRIVER'S DETAILS</h3>
                                            </div>
                                        </div>
                                        <br>
                                        <div class="row">

                                            <div class="col-md-6">
                                                <p class="pull-left">
                                                    <strong style="font-size: large;">    BL:  </strong> {{$value->bill_lading}} <br> <br>
                                                    <strong style="font-size: large;">    Container No:  </strong> {{$value->cont_no}} / {{$value->type}} <br> <br>
                                                    <strong style="font-size: large;">    Packages:      </strong> {{$value->packages}} <br> <br>
                                                    <strong style="font-size: large;">    Container seal:</strong> {{$value->container_seal}} <br> <br>
                                                    <strong style="font-size: large;">    Loading Place : </strong>  {{$value->terminal}} <br> <br>
                                                    <strong style="font-size: large;">    Destination:    </strong> {{$value->destination}}  <br> <br>
                                                    <strong style="font-size: large;">    Description :   </strong> {{$value->commodity}} <br> <br>
                                                    <strong style="font-size: large;">    Remarks :   </strong> {{$value->remarks}} <br> <br>
                                                </p>
                                            </div>
                                            <div class="col-md-6">
                                                <p class="pull-left">
                                                    <strong>    Truck No:           </strong> {{$value->trucking_number}} <br> <br>
                                                    <strong>    Driver Full Name:      </strong> {{$value->driver_name}} <br> <br>
                                                    <strong>    Driver Contact:    </strong> {{$value->driver_contact}} <br> <br>
                                                    <strong>    Driver Licence :    </strong>  {{$value->driver_license}} <br> <br>
                                                    <strong>    Driver Passport:    </strong> {{$value->driver_passport}}  <br> <br>
                                                    <strong>    Transporter Name :    </strong> {{$value->transporter_name}} <br>
                                                </p>
                                            </div>
                                        </div>
                                        <br>
                                        <div class="row titles">
                                            <div class="col-md-6">
                                                <h3 style="margin-left: -2%!important; font-size: x-large;">SHIPPER/CARRIER'S DETAILS</h3>
                                            </div>
                                            <div class="col-md-6">
                                                <h3 style="margin-left: -2%!important;     font-size: x-large;">
                                                    CLIENT IN CHARGE</h3>
                                            </div>
                                        </div>
                                        <br>
                                        <div class="row">

                                            <div class="col-md-6">
                                                <p class="pull-left">
                                                    <strong style="font-size: large;">    Shipper Name:           </strong> {{$value->shipper}} <br> <br>
                                                    <strong style="font-size: large;">    Instructions:      </strong> {{$value->special_instruction}} <br> <br>
                                                    <strong style="font-size: large;">    Shipping Line:    </strong> {{$value->shipping_line}} <br> <br>
                                                </p>
                                            </div>
                                            <div class="col-md-6">
                                                <p class="pull-left">
                                                    <strong style="font-size: large;">    Name:           </strong> {{$value->organization_name}} <br> <br>
                                                    <strong style="font-size: large;">    Phone:      </strong> {{$value->telephone}} <br> <br>
                                                    <strong style="font-size: large;">    Remarks:    </strong> {{$value->remarks}}
                                                </p>
                                            </div>
                                        </div>
                                        <br>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <p class="pull-left">
                                                    <strong style="font-size: large;">    Driver:  </strong>   {{$value->driver_name}}  <br> <br>
                                                </p>
                                            </div>
                                            <div class="col-md-6">
                                                <p class="pull-left">
                                                    <strong style="font-size: large;">    Signature: </strong>  <br> <br>
                                                    <strong style="font-size: large;">    Date:  </strong> {{$value->loading_date}}  <br> <br>
                                                </p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <p class="pull-left">
                                                    <strong style="font-size: large;">    Client in charge:  </strong>    <br> <br>
                                                </p>
                                            </div>
                                            <div class="col-md-6">
                                                <p class="pull-left">
                                                    <strong style="font-size: large;">  Signature: </strong>  <br> <br>
                                                    <strong style="font-size: large;">  Date:  </strong> {{$value->loading_date}} <br> <br>
                                                </p>
                                            </div>
                                            <div class="col-md-6">
                                                <p class="pull-left">
                                                    <strong style="font-size: large;">  Recipient: </strong>   {{$value->recipient_name}}  <br> <br>
                                                </p>
                                            </div>
                                            <div class="col-md-6">
                                                <p class="pull-left">
                                                    <strong style="font-size: large;">    Signature: </strong>  <br> <br>
                                                    <strong style="font-size: large;">    Date: </strong>  {{$value->recipient_date}}  <br> <br>
                                                </p>
                                            </div>
                                        </div>
                                </div>
                                @endforeach

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
@include('layouts.javas')
</body>
</html>