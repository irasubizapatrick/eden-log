<!DOCTYPE html>

<html>
@include('layouts.admin_head')
<style>

    @media print {
        #print_button {
            display: none;
        }
        .print_button {
            display: none;
        }
        letter_header{
            margin-left: 1rem;
        }
        .page {

        }
    }

    .receipt-content .logo a:hover {
        text-decoration: none;
        color: #7793C4;
    }

    .receipt-content .invoice-wrapper {
        background: #FFF;
        box-shadow: 0px 0px 1px #CCC;
        padding: 40px 40px 60px;
        border-radius: 4px;
    }

    .receipt-content .invoice-wrapper .payment-details span {
        color: #A9B0BB;
        display: block;
    }
    .receipt-content .invoice-wrapper .payment-details a {
        display: inline-block;
        margin-top: 5px;
    }

    .receipt-content .invoice-wrapper .line-items .print a {
        display: inline-block;
        border: 1px solid #9CB5D6;
        padding: 13px 13px;
        border-radius: 5px;
        color: #708DC0;
        font-size: 13px;
        -webkit-transition: all 0.2s linear;
        -moz-transition: all 0.2s linear;
        -ms-transition: all 0.2s linear;
        -o-transition: all 0.2s linear;
        transition: all 0.2s linear;
    }

    .receipt-content .invoice-wrapper .line-items .print a:hover {
        text-decoration: none;
        border-color: #333;
        color: #333;
    }

    .receipt-content {
        background: #ECEEF4;
    }
    @media (min-width: 1200px) {
        .receipt-content .container {width: 900px; }
    }

    .receipt-content .logo {
        text-align: center;
        margin-top: 50px;
    }

    .receipt-content .logo a {
        font-family: Myriad Pro, Lato, Helvetica Neue, Arial;
        font-size: 36px;
        letter-spacing: .1px;
        color: #555;
        font-weight: 300;
        -webkit-transition: all 0.2s linear;
        -moz-transition: all 0.2s linear;
        -ms-transition: all 0.2s linear;
        -o-transition: all 0.2s linear;
        transition: all 0.2s linear;
    }

    .receipt-content .invoice-wrapper .intro {
        line-height: 25px;
        color: #444;
    }

    .receipt-content .invoice-wrapper .payment-info {
        margin-top: 25px;
        padding-top: 15px;
    }

    .receipt-content .invoice-wrapper .payment-info span {
        color: #A9B0BB;
    }

    .receipt-content .invoice-wrapper .payment-info strong {
        display: block;
        color: #444;
        margin-top: 3px;
    }

    @media (max-width: 767px) {
        .receipt-content .invoice-wrapper .payment-info .text-right {
            text-align: left;
            margin-top: 20px; }
    }
    .receipt-content .invoice-wrapper .payment-details {
        margin-top: 30px;
        padding-top: 20px;
        line-height: 22px;
    }


    @media (max-width: 767px) {
        .receipt-content .invoice-wrapper .payment-details .text-right {
            text-align: left;
            margin-top: 20px; }
    }
    .receipt-content .invoice-wrapper .line-items {
        margin-top: 40px;
    }
    .receipt-content .invoice-wrapper .line-items .headers {
        color: #A9B0BB;
        font-size: 13px;
        letter-spacing: .3px;
        border-bottom: 2px solid #EBECEE;
        padding-bottom: 4px;
    }
    .receipt-content .invoice-wrapper .line-items .items {
        margin-top: 8px;
        border-bottom: 2px solid #EBECEE;
        padding-bottom: 8px;
    }
    .receipt-content .invoice-wrapper .line-items .items .item {
        padding: 10px 0;
        color: #696969;
        font-size: 15px;
    }
    @media (max-width: 767px) {
        .receipt-content .invoice-wrapper .line-items .items .item {
            font-size: 13px; }
    }
    .receipt-content .invoice-wrapper .line-items .items .item .amount {
        letter-spacing: 0.1px;
        color: #84868A;
        font-size: 16px;
    }
    @media (max-width: 767px) {
        .receipt-content .invoice-wrapper .line-items .items .item .amount {
            font-size: 13px; }
    }

    .receipt-content .invoice-wrapper .line-items .total {
        margin-top: 30px;
    }

    .receipt-content .invoice-wrapper .line-items .total .extra-notes {
        float: left;
        width: 40%;
        text-align: left;
        font-size: 13px;
        color: #7A7A7A;
        line-height: 20px;
    }

    @media (max-width: 767px) {
        .receipt-content .invoice-wrapper .line-items .total .extra-notes {
            width: 100%;
            margin-bottom: 30px;
            float: none; }
    }

    .receipt-content .invoice-wrapper .line-items .total .extra-notes strong {
        display: block;
        margin-bottom: 5px;
        color: #454545;
    }

    .receipt-content .invoice-wrapper .line-items .total .field {
        margin-bottom: 7px;
        font-size: 14px;
        color: #555;
    }

    .receipt-content .invoice-wrapper .line-items .total .field.grand-total {
        margin-top: 10px;
        font-size: 16px;
        font-weight: 500;
    }

    .receipt-content .invoice-wrapper .line-items .total .field.grand-total span {
        color: #20A720;
        font-size: 16px;
    }

    .receipt-content .invoice-wrapper .line-items .total .field span {
        display: inline-block;
        margin-left: 20px;
        min-width: 85px;
        color: #84868A;
        font-size: 15px;
    }

    .receipt-content .invoice-wrapper .line-items .print {
        margin-top: 50px;
        text-align: center;
    }

    .titles{
        background-color: none !important;
        border: 1px solid black;
        text-align:left;
    }


    .receipt-content .invoice-wrapper .line-items .print a i {
        margin-right: 3px;
        font-size: 14px;
    }

    .receipt-content .footer {
        margin-top: 40px;
        margin-bottom: 110px;
        text-align: center;
        font-size: 12px;
        color: #969CAD;
    }
    div[class^="col-md-6"], div[class*=" tocolor-"] {
        background-color: #eee;
        border: 1px solid black;
        text-align:left;
    }
    @page {
        size: A4;
    }
</style>
<body>
<div class="wrapper">
    <div class="row m-0 p-0">
        <div class="col-sm-12">
            <div class="page-title-box">
                <a href="{{URL::previous()}}" class="btn btn-danger print_button " > <i class="fa fa-arrow-left"></i> Back</a>
                <a href="#" id="print_button" class="btn btn-success" onclick="window.print()" >
                    <i class="fa fa-print"></i>
                    Print this receipt
                </a>
            </div>
        </div>
        <div class="col-12 container" style="margin-top: -3rem;">
            <div class="card m-b-30">
                <div class="receipt-content">
                    <div class="container bootstrap snippet">
                        <div class="row ">
                            <div class="col-md-12">
                                <div class="invoice-wrapper ">
                                    <div class="payment-details">
                                        <div class="row" style="margin-top: -6rem;border-bottom: 2px solid #80808038;padding-bottom: 3rem;">
                                            <div class="col-sm-6 float-left">
                                                <img src="/assets/images/slogo.jpeg" class="" alt="..." style=" width: 60%;">
                                            </div>
                                            <div class="col-sm-6" style="margin-top: 2rem;">
                                                <i class="fa fa-map-marker"> Harbour View Tower | 8th Floor, room 808 <br>P.O.Box 9163, Samora Avenue , Dar es salaam, Tanzania</i><br>
                                                <i class="fa fa-phone"> (+255) 22 2112536/37 </i> & +255 714 307 315</i><br>
                                                <i class="fa fa-envelope"> info@mikumifreight.com</i>
                                            </div>
                                        </div>
                                    </div>
                                    <h1 class="text-center mt-5">DELIVERY NOTE</h1>
                                    @foreach($delivery as $value)
                                        <h3 style="margin-left: -2%!important">CONSIGNEE ADDRESS</h3>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <p class="pull-left">
                                                    <strong>    Name:           </strong> {{$value->container->lad->track->client_name}} <br>
                                                    <strong>    Telephone:      </strong> {{$value->container->lad->track->agent->telephone}} <br>
                                                    <strong>    Destination:    </strong> {{$value->container->lad->track->destination}}
                                                </p>
                                            </div>
                                            <div class="col-md-6">
                                                <p class="pull-left">
                                                    <strong>    Loading Date::          </strong> {{$value->transporter->loading_date}} <br>

                                                    <strong>    Ref:      </strong> {{$value->container->lad->track->file_no}} <br>
                                                </p>
                                            </div>
                                        </div>
                                        <br>
                                        <div class="row titles">
                                            <div class="col-md-6">
                                                <h3 style="margin-left: -2%!important">CARGO DETAILS</h3>
                                            </div>
                                            <div class="col-md-6">
                                                <h3 style="margin-left: -2%!important">TRANSPORTER / DRIVER'S DETAILS</h3>
                                            </div>
                                        </div>
                                        <br>
                                        <div class="row">

                                            <div class="col-md-6">
                                                <p class="pull-left">
                                                    <strong>    BL:  </strong> {{$value->container->lad->bill_lading}} <br>
                                                    <strong>    Container No:  </strong> {{$value->container->cont_no}} / {{$value->container->lad->track->type}} <br> <br>
                                                    <strong>    Packages:      </strong> {{$value->packages}} <br> <br>
                                                    <strong>    Container seal:</strong> {{$value->container_seal}} <br> <br>
                                                    <strong>    Loading Place : </strong>  {{$value->container->lad->track->terminal}} <br> <br>
                                                    <strong>    Destination:    </strong> {{$value->container->lad->track->destination}}  <br> <br>
                                                    <strong>    Description :   </strong> {{$value->container->lad->track->commodity}} <br> <br>
                                                    <strong>    Remarks :   </strong> {{$value->container->lad->track->remarks}} <br> <br>
                                                </p>
                                            </div>
                                            <div class="col-md-6">
                                                <p class="pull-left">
                                                    <strong>    Truck No:           </strong> {{$value->transporter->trucking_number}} <br> <br>
                                                    <strong>    Driver Full Name:      </strong> {{$value->driver->driver_name}} <br> <br>
                                                    <strong>    Driver Contact:    </strong> {{$value->driver->driver_contact}} <br> <br>
                                                    <strong>    Driver Licence :    </strong>  {{$value->driver->driver_license}} <br> <br>
                                                    <strong>    Driver Passport:    </strong> {{$value->driver->driver_passport}}  <br> <br>
                                                    <strong>    Transporter Name :    </strong> {{$value->transporter->transporter_name}} <br>
                                                </p>
                                            </div>
                                        </div>
                                        <br>
                                        <div class="row titles">
                                            <div class="col-md-6">
                                                <h3 style="margin-left: -2%!important">SHIPPER/CARRIER'S DETAILS</h3>
                                            </div>
                                            <div class="col-md-6">
                                                <h3 style="margin-left: -2%!important">
                                                    AGENT IN CHARGE</h3>
                                            </div>
                                        </div>
                                        <br>
                                        <div class="row">

                                            <div class="col-md-6">
                                                <p class="pull-left">
                                                    <strong>    Shipper Name:           </strong> {{$value->container->lad->track->shipper}} <br> <br>
                                                    <strong>    Instructions:      </strong> {{$value->container->lad->track->special_instruction}} <br> <br>
                                                    <strong>    Shipping Line:    </strong> {{$value->container->lad->track->shipping_line}} <br> <br>
                                                </p>
                                            </div>
                                            <div class="col-md-6">
                                                <p class="pull-left">
                                                    <strong>    Name:           </strong> {{$value->container->lad->track->agent->organization_name}} <br> <br>
                                                    <strong>    Phone:      </strong> {{$value->container->lad->track->agent->telephone}} <br> <br>
                                                    <strong>    Remarks:    </strong> {{$value->container->lad->track->remarks}}
                                                </p>
                                            </div>
                                        </div>
                                        <br>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <p class="pull-left">
                                                    <strong>    Driver:  </strong>   {{$value->driver->driver_name}}  <br> <br>
                                                </p>
                                            </div>
                                            <div class="col-md-6">
                                                <p class="pull-left">
                                                    <strong>    Signature: </strong>  <br> <br>
                                                    <strong>    Date:  </strong> {{$value->driver->created_at}}  <br> <br>
                                                </p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <p class="pull-left">
                                                    <strong>    Agent in charge:  </strong>    <br> <br>
                                                </p>
                                            </div>
                                            <div class="col-md-6">
                                                <p class="pull-left">
                                                    <strong>  Signature: </strong>  <br> <br>
                                                    <strong>  Date:  </strong> {{$value->transporter->loading_date}} <br> <br>
                                                </p>
                                            </div>
                                            <div class="col-md-6">
                                                <p class="pull-left">
                                                    <strong>  Receipt: </strong>   {{$value->recipient_name}}  <br> <br>
                                                </p>
                                            </div>
                                            <div class="col-md-6">
                                                <p class="pull-left">
                                                    <strong>    Signature: </strong> {{$value->recipient_date}}  <br> <br>
                                                    <strong>    Date: </strong>   <br> <br>
                                                </p>
                                            </div>
                                        </div>

                                @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
@include('layouts.javas')
</body>
</html>