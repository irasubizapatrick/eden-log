<!DOCTYPE html>
<html>

@include('layouts.super_head')
<style>
    .size{
        height: calc(3.25rem + -1px) !important;
    }
    .select2{
        width: 100% !important;
        margin-top: -3rem;
    }
    .dataTables_paginate{
        display: none !important;
    }
</style>
<body>
@include('layouts.super_sidebar')
<div class="wrapper">
    <div class="container-fluid">
        <div class="modal fade bs-example-modal-lg"  role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title mt-0" id="myLargeModalLabel">Generate Delivery Note</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body">
                        <div class="row p-5">
                            <form class="form-horizontal"  action="/delivery/note" method="POST" enctype="multipart/form-data">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                <div class="row col-md-12">
                                    <div class="col-md-6">
                                        <label>Container Number</label>
                                        <select class="form-control size" name="container_id" id="ladding_id"  onchange="getcarDetails(this.value);" required>

                                            <option value="">Choose  Container number</option>
                                            @foreach($cont_bill as $value)
                                                <option value="{{$value->container_id}}">{{$value->container->cont_no}}</option>
                                            @endforeach

                                        </select>
                                    </div>
                                    <div class="col-md-6 mt-3">
                                        <label>Bill of lading</label>
                                        <input type="text" class="form-control"  value="0" name="bill_lading"readonly id="Bill"  placeholder="Enter Drop off  name" required>

                                    </div>
                                    <div class="col-md-6">
                                        <label> Transporter Name</label>
                                        <input type="text" class="form-control"  value="0" readonly name="transporter_name" id="transporter"  placeholder="Enter Drop off  name" required>

                                    </div>
                                    <div class="col-md-6">
                                        <label> Driver Name</label>
                                        <input type="text" class="form-control"  value="0" readonly name="driver_name" id="driver"  placeholder="Enter Drop off  name" required>

                                    </div>
                                    <div class="col-md-6">
                                        <label> Trailer / Truck No</label>
                                        <input type="text" class="form-control"   value="0" readonly name="trucking_number" id="truck"  placeholder="Enter Drop off  name" required>

                                    </div>


                                    <div class="col-md-6">
                                        <label>Recipient Name</label>
                                        <input type="text"  name="recipient_name" value="0" class="form-control" placeholder="Enter Recipient Name">
                                    </div>
                                    <div class="col-md-6">
                                        <label>Loading Date</label>
                                        <input type="date" value="0"    name="loading_date" id="all" class="form-control" placeholder="Enter Recipient date ">
                                    </div>
                                    <div class="col-md-6">
                                        <label>Recipient Date</label>
                                        <input type="date"  name="recipient_date" value="0"  class="form-control" placeholder="Enter Recipient date ">
                                    </div>

                                    <div class="col-md-6">
                                        <label>Packages</label>
                                        <input type="text"  name="packages" value="0" class="form-control" placeholder="Enter packages  ">
                                    </div>
                                    <div class="col-md-6">
                                        <label>Container Seal </label>
                                        <input type="text"  name="container_seal" value="0" class="form-control" placeholder="Enter container seal  ">
                                    </div>
                                    <div class="col-md-12 pt-4">
                                        <center>
                                            <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-primary waves-effect waves-light">Save changes</button>
                                        </center>
                                    </div>

                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row m-0 p-0">
        <div class="col-sm-12">
            <div class="page-title-box">
                <div class="btn-group pull-right">
                    <ol class="breadcrumb hide-phone p-0 m-0">
                        <li class="breadcrumb-item"><a href="#">Delivery note List</a></li>
                        <li class="breadcrumb-item active">Dashboard</li>
                    </ol>
                </div>
                <h4 class="page-title">Delivery Note</h4>
            </div>

            <div class="modal fade bill_lading"  role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title mt-0" id="myLargeModalLabel">Search By Container </h5>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        </div>
                        <div class="modal-body">
                            <form class="form-horizontal"  action="/search/delivery/note" method="POST" enctype="multipart/form-data">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                <div class="row col-md-12">
                                    <div class="col-md-6">
                                        <label>Container Number</label>
                                        <select class="form-control size" name="container_id" id="search_ladding_id">
                                            <option value="">Container number</option>
                                            @foreach($cont_bill as $key)
                                                <option value="{{$key->container->id}}">{{$key->container->cont_no}}</option>
                                            @endforeach

                                        </select>
                                    </div>
                                    <div class="col-md-12 pt-4 pb-4">
                                        <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
                                        <button type="submit" class="btn btn-primary waves-effect waves-light">Search </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 container" style="margin-top: -3rem;">
            <div class="card m-b-30">
                <div class="card-body">
                    <a href="/delivery/note" role="button" type="button" class="btn btn-success waves-effect waves-light pull-right mx-2" ><i class="fa fa-home"></i> <span>All</span></a>
                    <button type="button" class="btn btn-primary waves-effect waves-light pull-right" data-toggle="modal" data-target=".bs-example-modal-lg "><i class="fa fa-plus"></i> <span>Add New</span></button>
                    <button type="button" class="btn btn-warning waves-effect waves-light pull-right mx-4" data-toggle="modal" data-target=".bill_lading "><i class="fa fa-search"></i><span>Search </span></button>


                    @if (Session::has('message'))
                        <div class="alert alert-success alert-dismissable">
                            <a href="" class="close" data-dismiss="alert" aria-label="close">
                                &times;

                            </a>
                            {{ Session::get('message') }}
                        </div>

                    @endif
                    @if (Session::has('delete'))
                        <div class="alert alert-success alert-dismissable">
                            <a href="" class="close" data-dismiss="alert" aria-label="close">
                                &times;

                            </a>
                            {{ Session::get('delete') }}
                        </div>

                    @endif
                    @if (Session::has('updated'))
                        <div class="alert alert-success alert-dismissable">
                            <a href="" class="close" data-dismiss="alert" aria-label="close">
                                &times;

                            </a>
                            {{ Session::get('updated') }}
                        </div>

                    @endif

                    <table id="datatable-buttons" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                        <tr style="font-size: 13px;background-color: #416b8d !important;color: #fff;">
                            <th>#</th>
                            <th>BL</th>
                            <th>Loading Date</th>
                            <th>Cont/ Chassis No</th>
                            <th>Driver Name</th>
                            <th>Transporter Name</th>
                            <th>Truck No</th>
                            <th>Cont Seal</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $i=1;?>
                        @foreach($delivery AS $value)
                            <tr>
                                <td>{{$i++}}</td>
                                <td>{{$value->bill_lading}}</td>

                                <td>{{$value->loading_date}}</td>
                                <td>{{$value->cont_no}}</td>
                                <td>{{$value->driver_name}}  </td>
                                <td>{{$value->transporter_name}} </td>
                                <td>{{$value->trucking_number}}</td>
                                <td>{{$value->container_seal}}</td>
                                <td>
                                    <button data-toggle="modal" data-target="#editleads<?php echo $i;?>" class="pull-left edit  btn-lg  btn btn-primary dlt_sm_table mx-2 mt-2"><i class="fa fa-pencil"></i></button>
                                    <button data-toggle="modal" data-target="#delete<?php echo $i;?>" class="pull-left edit  btn-lg  btn btn-danger dlt_sm_table mx-2 mt-2"><i class="fa fa-trash"></i></button>

                                    <div class="modal fade" id="delete<?php echo $i;?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLarge01" style="display: none;" aria-hidden="true">
                                        <div class="modal-dialog modal-lg" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title">Delete </h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">×</span>
                                                    </button>
                                                </div>
                                                <form class="pull-left" action="/delivery/note/{{$value->id}}" method="POST">
                                                    <label class="mx-4">Are you sure you want to delete</label>
                                                    <input type="hidden" name="_method" value="DELETE" />
                                                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                                    <div class="modal-footer">
                                                        <button type="submit" class="btn btn-success">Confirm</button>
                                                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="editleads<?php echo $i;?>" class="modal fade" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
                                        <div class="modal-dialog modal-lg">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h4 class="modal-title" id="editleads"> Edit Delivery Note</h4>
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

                                                </div>
                                                <div class="modal-body">
                                                    <div class="row p-5">
                                                        <form role="form" action="/delivery/note/{{$value->id}}" method="post" enctype="multipart/form-data">
                                                            <input type="hidden" name="_method" value="PUT" />
                                                            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                                            <div class="row col-md-12">
                                                                <div class="col-md-6 " style="margin-top: -1rem;">
                                                                    <label>Bill of lading</label>
                                                                    <select class="form-control all size" name="ladding_id" id="edit_ladding_id" required>
                                                                        <option value="{{$value->container_id}}">{{$value->bill_lading}}</option>
                                                                        @foreach($ladding as $key => $all)
                                                                            <option value="{{ $key }}">{{$all}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <label>Container No / Chassis No</label>
                                                                    <select name="container_id"  class="form-control size">
                                                                        <option value="{{$value->container_id}}"> {{$value->cont_no}} </option>
                                                                        <option value="">Container number</option>
                                                                    </select>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <label> Transporter Name</label>
                                                                    <select class="form-control size" name="transporter_id">
                                                                        <option value="{{$value->transporter_id}}"> {{$value->transporter_name}}</option>
                                                                        @foreach($transporter as $trans)
                                                                            <option value="{{$trans->id}}">{{$trans->transporter_name}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <label> Driver Name</label>
                                                                    <select class="form-control size" name="driver_id">
                                                                        <option value="{{$value->driver_id}}">{{$value->driver_name}}</option>
                                                                        @foreach($drivers  as $driver)
                                                                            <option value="{{$driver->id}}">{{$driver->driver_name}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <label>Loading Date</label>
                                                                    <input type="date"  name="loading_date"  value="{{$value->loading_date}}"  class="form-control" placeholder="Enter Recipient date ">
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <label>Recipient Name</label>
                                                                    <input type="text"  name="recipient_name"  value="{{$value->recipient_name}}"  class="form-control" placeholder="Enter Recipient Name">
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <label>Recipient Date</label>
                                                                    <input type="date"  name="recipient_date"  value="{{$value->recipient_date}}"  class="form-control" placeholder="Enter Recipient date ">
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <label>Packages</label>
                                                                    <input type="text"  name="packages" class="form-control"  value="{{$value->packages}}"  placeholder="Enter packages ">
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <label>Container Seal </label>
                                                                    <input type="text"  name="container_seal" class="form-control"  value="{{$value->container_seal}}"  placeholder="Enter container seal  ">
                                                                </div>
                                                                <div class="col-md-12 pt-4">
                                                                    <center>
                                                                        <button type="submit" class="btn btn-primary waves-effect waves-light">Save changes</button>
                                                                        <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Close</button>
                                                                    </center>
                                                                </div>
                                                            </div>

                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <a href="{{route ('view/delivery', ['id' =>$value->id])}}"  class="pull-left edit  btn-lg  btn btn-success dlt_sm_table mx-2 mt-2"><i class="fa fa-eye"></i></a>
                                </td>

                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <span class="float-right">
                        <?php echo $delivery->appends(Request::all())->fragment('foo')->render(); ?>

                    </span>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

@include('layouts.footer')
@include('layouts.javas')
<script type="text/javascript">
    $(document).ready(function() {

        $('#ladding_id').select2();

        $('select[name="ladding_id"]').on('change', function() {
            var stateID = $(this).val();
            if(stateID) {
                $.ajax({
                    url: '/add_status/ajax/'+stateID,
                    type: "GET",
                    dataType: "json",
                    success:function(data) {


                        $('select[name="container_id"]').empty();
                        $.each(data, function(key, value) {
                            $('select[name="container_id"]').append('<option value="'+ key +'">'+ value +'</option>');
                        });


                    }
                });
            }else{
                $('select[name="container_id"]').empty();
            }
        });

        $('#edit_ladding_id').select2();

        $('select[name="ladding_id"]').on('change', function() {
            var stateID = $(this).val();
            if(stateID) {
                $.ajax({
                    url: '/add_status/ajax/'+stateID,
                    type: "GET",
                    dataType: "json",
                    success:function(data) {


                        $('select[name="container_id"]').empty();
                        $.each(data, function(key, value) {
                            $('select[name="container_id"]').append('<option value="'+ key +'">'+ value +'</option>');
                        });


                    }
                });
            }else{
                $('select[name="container_id"]').empty();
            }
        });
    });
</script>

<script type="text/javascript">
    var getcarDetails = function (id) {
        var str = '/cont_number/'+ id;
        $.ajax({
            url: str,
            type: 'get',
            async: false,
            success: function (data) {
                console.log(data);
                document.getElementById('transporter').value = data.transporter_name;
                document.getElementById('driver').value = data.driver_name;
                document.getElementById('Bill').value = data.bill_lading;
                document.getElementById('truck').value = data.trucking_number;
                document.getElementById('all').value = data.loading_date;
            }
        });

    };
</script>
</body>
</html>