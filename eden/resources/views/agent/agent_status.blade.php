<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    @include('layouts.title')
    <meta content="Admin Dashboard" name="description" />
    <meta content="Mikumi" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <!-- App Icons -->
    <link rel="shortcut icon" href="/assets/images/favicon.ico">

    <!-- DataTables -->
    <link href="/assets/plugins/datatables/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
    <link href="/assets/plugins/datatables/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css" />
    <!-- Responsive datatable examples -->
    <link href="/assets/plugins/datatables/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css" />

    <!-- App css -->
    <link href="/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="/assets/css/icons.css" rel="stylesheet" type="text/css" />
    <link href="/assets/css/style.css" rel="stylesheet" type="text/css" />
    <link href="/assets/css/select2.min.css" rel="stylesheet" type="text/css" />


</head>

<style>
    .has-submenu{
        padding: 0px 10px 0px 10px;
    }
    #topnav .navigation-menu > li {
        display: inline-block;
        position: relative;
    }
</style>


<body>

<!-- Navigation Bar-->
@include('layouts.agent_sidebar')
<!-- End Navigation Bar-->


<div class="wrapper">
    <div class="container-fluid">

        <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <div class="btn-group pull-right">
                        <ol class="breadcrumb hide-phone p-0 m-0">

                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <!-- end page title end breadcrumb -->

        <div class="row">
            <div class="col-12">
                <div class="card m-b-30">
                    <div class="card-body mx-auto">
                        <h5 class="page-title mx-3">Add  Status</h5>

                        <form class="form-horizontal"  action="/save_add_status" method="POST" enctype="multipart/form-data">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                            <div class="row container  pt-3">
                                <div class="col-md-6">
                                    <label>Bill of lading</label>
                                    <select class="form-control " name="ladding_id" id="ladding_id" required>
                                        <option value="">Select Bill of lading</option>
                                        @foreach($ladding as $key => $value)
                                            <option value="{{ $key }}">{{$value}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-6 mt-3">
                                    <label>Container Number</label>
                                    <select name="cont_no" class="form-control form_size">
                                        <option value="">Container number</option>
                                    </select>
                                </div>
                                <div class="col-md-6">
                                    <label>Processing date </label>
                                    <input type="date" name="processing_date" class="form-control" placeholder="Enter Processing date " required>
                                </div>
                                <div class="col-md-6
">
                                    <label>Processing status </label>
                                    <select class="form-control" name="processing_status" required>
                                        <option value="0"> Select Status</option>
                                        <option value="Cargo Crossed border"> Cargo Crossed border </option>
                                        <option value="Unpacking"> Unpacking </option>
                                        <option value="Cargo Arrived"> Cargo Arrived </option>
                                        <option value="Unloaded ( Warehouse)"> Unloaded ( Warehouse)</option>
                                        <option value="Container Free"> Container Free</option>
                                        
                                    </select>
                                </div>
                                <div class="col-md-12">
                                    <label>Any other Comment</label>
                                    <textarea class="form-control" name="other_comment" >
                                        no
                                    </textarea>
                                </div>
                                <div class="col-md-12 mt-5">
                                    <center>
                                        <button type="reset" class="btn btn-danger waves-effect" data-dismiss="modal">Clear</button>
                                        <button type="submit" class="btn btn-primary waves-effect waves-light">Save changes</button>
                                    </center>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div> <!-- end col -->
        </div> <!-- end row -->


    </div> <!-- end container -->
</div>
<!-- end wrapper -->


<!-- Footer -->
@include('layouts.footer')
<!-- End Footer -->


@include('layouts.javas')
<script type="text/javascript">
    $(document).ready(function() {
        $('select[name="ladding_id"]').on('change', function() {
            var stateID = $(this).val();
            if(stateID) {
                $.ajax({
                    url: '/add_status/ajax/'+stateID,
                    type: "GET",
                    dataType: "json",
                    success:function(data) {


                        $('select[name="cont_no"]').empty();
                        $.each(data, function(key, value) {
                            $('select[name="cont_no"]').append('<option value="'+ key +'">'+ value +'</option>');
                        });


                    }
                });
            }else{
                $('select[name="cont_no"]').empty();
            }
        });
    });
</script>

</body>
</html>
<!-- Localized -->