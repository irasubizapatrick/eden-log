<!DOCTYPE html>
<html>
@include('layouts.admin_head')
<style>
    #datatable-buttons_filter{
        display: none !important;
    }
</style>
<style>
    .form_size{
        width: 334px !important;
    }

    @media print {


    }
</style>
<body>

@include('layouts.agent_sidebar')

<div class="wrapper">
    <div class="row m-0">
        <div class="col-sm-12">
            <div class="page-title-box" style="padding: 58px 0px 19px 0px !important">
                <div class="btn-group pull-right">
                    <ol class="breadcrumb hide-phone p-0 m-0">
                        <li class="breadcrumb-item"><a href="#">Orders List</a></li>
                        <li class="breadcrumb-item active">Dashboard</li>
                    </ol>
                </div>
                <h4 class="page-title">All Orders</h4>
            </div>
        </div>
        <div class="col-xl-12" >
            <div class="card">

                <div class="modal fade period"  role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title mt-0 mx-4" id="myLargeModalLabel">Search Based </h4>
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            </div>
                            <div class="modal-body">
                                <form class="" role="form" action="/sortOrders" method="POST" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                    <div class="row container mx-0 p-3">
                                        <div class="col-md-6">
                                            <label>Container Number</label>
                                            <select class="form-control form_size" name="container_id" id="vessel" >
                                                <option value=""> Choose Container</option>
                                                @foreach($container_no as $value)
                                                    <option value="{{$value->id}}">{{$value->cont_no}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        {{--<div class="col-md-6">--}}
                                            {{--<label>From</label>--}}
                                            {{--<input type="date" name="date1" value="" class="form-control" >--}}
                                        {{--</div>--}}
                                        {{--<div class="col-md-6" >--}}
                                            {{--<label>To</label>--}}
                                            {{--<input type="date" name="date2"   value="" class="form-control" >--}}
                                        {{--</div>--}}
                                        <div class="col-md-12  pt-5  pb-3" >
                                            <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-primary waves-effect waves-light">Search</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div><!-- /.modal-content -->
                </div>

                <div class="card-body">
                    <button type="button" class="btn btn-primary waves-effect waves-light pull-right mx-2" data-toggle="modal" data-target=".period "><i class="fa fa-search"></i> <span>Advanced Search </span></button>

                    <a type="button" role="button" class="btn btn-success waves-effect waves-light pull-right mx-2" href="/reports"><i class="fa fa-home"></i> <span>All Orders </span></a>
                    {{--<button type="button" class="btn btn-success waves-effect waves-light pull-right mx-2" data-toggle="modal" data-target=".destinations"><i class="fa fa-search"></i> <span>Destinations </span></button>--}}
                    {{--<button type="button" class="btn btn-primary waves-effect waves-light pull-right mx-2" data-toggle="modal" data-target=".consignee"><i class="fa fa-search"></i> <span>Consignee </span></button>--}}

                    <div class="table-responsive mt-4">
                        @if (Session::has('message'))
                            <div class="alert alert-success alert-dismissable">
                                <a href="" class="close" data-dismiss="alert" aria-label="close">
                                    &times;

                                </a>
                                {{ Session::get('message') }}
                            </div>

                        @endif
                        @if (Session::has('delete'))
                            <div class="alert alert-success alert-dismissable">
                                <a href="" class="close" data-dismiss="alert" aria-label="close">
                                    &times;

                                </a>
                                {{ Session::get('delete') }}
                            </div>

                        @endif
                        @if (Session::has('updated'))
                            <div class="alert alert-success alert-dismissable">
                                <a href="" class="close" data-dismiss="alert" aria-label="close">
                                    &times;

                                </a>
                                {{ Session::get('updated') }}
                            </div>

                        @endif
                            <table id="datatable-buttons" class=" table table-hover table-striped table-bordered" cellspacing="0" width="100%" >
                                <thead>
                                <tr style="font-size: 13px;background-color: #416b8d !important;color: #fff;">
                                    <th>#</th>
                                    <th>Date</th>
                                    <th>BL</th>
                                    {{--<th>Cont </th>--}}
                                    <th>Consignee</th>
                                    <th>ETA</th>
                                    <th> C&F </th>
                                    <th>Shipping Line</th>
                                    <th>Destination</th>
                                    <th>Status</th>


                                </tr>
                                </thead>
                                <tbody>
                                <?php $i=1;?>
                                @foreach($track AS $value)
                                    <tr style="font-size: 12px;">
                                        <td>{{$i++}}</td>
                                        <td>{{$value->document_date_received}} </td>
                                        <td>{{$value->bill_lading}} </td>
{{--                                        <td>{{$value->cont_no}}</td>--}}
                                        <td>{{$value->client_name}} </td>
                                        <td>{{$value->eta}}</td>
                                        <td>{{$value->organization_name}}</td>
                                        <td>{{$value->shipping_line}}</td>
                                        <td>{{$value->destination}}</td>
                                        <td>

                                            @if($value->current_status == 'Prepare for T1')

                                                <span class="label label-danger"> {{$value->current_status}}</span>
                                            @elseif($value->current_status == 'Crossed Border')

                                                <span class="label label-success"> {{$value->current_status}}</span>

                                            @elseif($value->current_status == 'Loading')

                                                <span class="label label-danger"> {{$value->current_status}}</span>



                                            @elseif($value->current_status == 'Receiving the documents')

                                                <span class="label label-primary"> {{$value->current_status}} </span>

                                            @elseif($value->current_status == 'Under Customs Procedures')

                                                <span class="label label-warning"> {{$value->current_status}} </span>
                                            @else
                                                <span class="label label-info"> {{$value->current_status}} </span>
                                            @endif

                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>

                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
</div>
@include('layouts.footer')
@include('layouts.javas')

</body>
</html>