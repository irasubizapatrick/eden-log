
<!DOCTYPE html>
<html>
@include('layouts.admin_view_order')

<body class="fixed-left">
<!-- Begin page -->
<div id="wrapper">

@include('layouts.agent_sidebar')

    <div class="content-page">
        <div class="content">
            <div class="topbar">
                <nav class="navbar-custom">

                    <ul class="list-inline menu-left mb-0">
                        <li class="list-inline-item">
                            <button type="button" class="button-menu-mobile open-left waves-effect">
                                <i class="ion-navicon"></i>
                            </button>
                        </li>
                        <li class="hide-phone list-inline-item app-search">
                            <h3 class="page-title">Details</h3>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </nav>
            </div>
            <div class="page-content-wrapper ">

                <div class="container-fluid">

                        @forelse($track as $value)
                        <div class="row pt-3 pb-5 mt-4">
                            <div class="col-lg-12">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="col-lg-12">
                                            <h2 class="mt-0 header-title " >Document Received: {{$value->date}}</h2>
                                            <h4 class="mt-0 header-title">Bill of ladding</h4>
                                            <p class="text-muted m-b-30 font-14">{{$value->bill_lading}}</p>
                                        </div>
                                        <div class="col-lg-12">
                                            <img src="/assets/images/slogo.jpeg" class="rounded-circle img-circle  float-right" alt="..." style="margin-top: -8rem;width: 22%;">
                                        </div>

                                        <div class="col-lg-12">
                                            <!-- Nav tabs -->
                                            <ul class="nav nav-tabs" role="tablist">
                                                <li class="nav-item">
                                                    <a class="nav-link active" data-toggle="tab" href="#home" role="tab" style="color: black;"><strong>Consignee</strong></a>
                                                </li>

                                                <li class="nav-item">
                                                    <a class="nav-link tabs" data-toggle="tab" href="#eta" role="tab"> <strong>ETA</strong></a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link tabs" data-toggle="tab" href="#ata" role="tab"><strong> ATA</strong></a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link tabs" data-toggle="tab" href="#shipping_line" role="tab"><strong>Shipping Line </strong></a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link tabs" data-toggle="tab" href="#shipper" role="tab"><strong>Shipper</strong></a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link tabs" data-toggle="tab" href="#vessel" role="tab"><strong>Vessel Name</strong></a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link tabs" data-toggle="tab" href="#driver" role="tab"><strong>Drivers Details</strong></a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link tabs" data-toggle="tab" href="#documents" role="tab"><strong>Documents</strong></a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link tabs" data-toggle="tab" href="#profile" role="tab"><strong>Others</strong></a>
                                                </li>


                                            </ul>
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="tab-content">
                                                <div class="tab-pane active p-3" id="home" role="tabpanel">
                                                    <p class="font-14 mb-0">
                                                        <strong>Client / Consignee :  </strong> {{$value->client_name}} <br>
                                                    </p>
                                                </div>

                                                <div class="tab-pane p-3" id="eta" role="tabpanel">
                                                    <p class="font-14 mb-0">
                                                        <strong>ETA : </strong>{{$value->eta}}  <br>
                                                    </p>
                                                </div>
                                                <div class="tab-pane p-3" id="ata" role="tabpanel">
                                                    <p class="font-14 mb-0">
                                                        <strong>ATA : </strong>{{$value->ata}}  <br>
                                                    </p>
                                                </div>
                                                <div class="tab-pane p-3" id="shipping_line" role="tabpanel">
                                                    <p class="font-14 mb-0">
                                                        <strong>Shipping Line: </strong>{{$value->shipping_line}}  <br>
                                                    </p>
                                                </div>
                                                <div class="tab-pane p-3" id="vessel" role="tabpanel">
                                                    <p class="font-14 mb-0">
                                                        <strong>Vessel Name: </strong>{{$value->line_vessel}}  <br>
                                                    </p>
                                                </div>
                                                <div class="tab-pane p-3" id="shipper" role="tabpanel">
                                                    <p class="font-14 mb-0">
                                                        <strong>Shipper : </strong>{{$value->shipper}}  <br>
                                                        <strong>Address : </strong> {{$value->shipper_address}}
                                                    </p>
                                                </div>

                                                <div class="tab-pane p-3" id="profile" role="tabpanel">
                                                    <p class="font-14 mb-0">
                                                        <strong >Border Exist : </strong> {{$value->border_exist}} <br>
                                                        <strong >Cargo Description : </strong> {{$value->cargo_description}} <br>
                                                        <strong>Commodity : </strong> {{$value->commodity}}
                                                        <strong>Terminal : </strong> {{$value->terminal}}
                                                        <strong>Weight as per  BL : </strong>{{$value->weight}}  <br>
                                                        <strong>Port of Loading :</strong>{{$value->port_charge}}  <br>
                                                        <strong>Voyage Number  : </strong>{{$value->voyager_number}}  <br>

                                                    </p>
                                                </div>
                                                <div class="tab-pane p-3" id="documents" role="tabpanel">
                                                    <p class="font-14 mb-0">
                                                        <strong>Port Charges File Doc : </strong>
                                                        @if($value->port_charge_invoice != '')
                                                            <a href="/PortCharges/{{$value->port_charge_invoice}}"  target="_blank">file</a>

                                                        @else

                                                            <a href="#"  style="color: red;">no file</a>

                                                        @endif <br>
                                                        <strong>Invoice Doc : </strong>
                                                        @if($value->invoice_order != '')
                                                            <a href="/OrderDoc/{{$value->invoice_order}}"  target="_blank">file</a>

                                                        @else

                                                            <a href="#"  style="color: red;">no file</a>

                                                        @endif <br>
                                                        <strong>Bill of lading Doc : </strong>
                                                        @if($value->bl_doc != '')
                                                            <a href="/OrderDoc/{{$value->bl_doc}}"  target="_blank">file</a>

                                                        @else

                                                            <a href="#"  style="color: red">no file</a>

                                                        @endif <br>
                                                        <strong>Packing List Doc :</strong>
                                                        @if($value->package_list_doc != '')
                                                            <a href="/OrderDoc/{{$value->package_list_doc}}"  target="_blank">file</a>

                                                        @else

                                                            <a href="#"  style="color: red">no file</a>

                                                        @endif <br>
                                                    </p>
                                                </div>
                                                <div class="tab-pane p-3" id="driver" role="tabpanel">
                                                    @foreach($driver as $driv)
                                                        <p class="font-14 mb-0">
                                                            <strong >Name: </strong> {{$driv->driver_name}} <br>
                                                            <strong>Contact : </strong>{{$driv->driver_contact}}  <br>
                                                            <strong>Passport : </strong>{{$driv->driver_passport}}  <br>
                                                            <strong>License : </strong>{{$driv->driver_license}}  <br>
                                                        </p>
                                                    @endforeach
                                                </div>

                                            </div>
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="panel panel-default">
                                                <div class="pt-3">
                                                    <h5 class="panel-title font-20 mx-2"><strong>CARGO LOADING DETAILS </strong></h5>
                                                </div>
                                                <div class="">
                                                    <div class="table-responsive">
                                                        <table class="table ">
                                                            <thead>
                                                            <tr>
                                                                <th><strong># </strong></th>
                                                                <th><strong>Container Number </strong></th>
                                                                <th class="text-center"><strong>Size /  Units</strong></th>
                                                                <th class="text-center"><strong>Weight</strong>
                                                                </th>
                                                                <th class="text-center"><strong>Transporter Name</strong>
                                                                </th>
                                                                <th  class="text-center"><strong>Truck / Trailer No</strong>
                                                                </th>
                                                                <th  class="text-center"><strong>Date of Loading</strong></th>
                                                                <th class="text-center">
                                                                    <strong>Driver Name</strong>
                                                                </th>
                                                                <th class="text-center">
                                                                    <strong> Driver Contact</strong>
                                                                </th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            <?php $i=1?>
                                                            @foreach($containers as $cont)
                                                                <tr>
                                                                    <td>{{$cont->id}}</td>
                                                                    <td >{{$cont->cont_no}}</td>
                                                                    <td class="text-center">{{$cont->type}}</td>
                                                                    <td class="text-center">{{$cont->weight}}</td>
                                                                    <td class="text-center">{{$cont->transporter_name}}</td>
                                                                    <td class="text-center">{{$cont->trucking_number}}</td>
                                                                    <td class="text-center">{{$cont->loading_date}}</td>
                                                                    <td class="text-center">{{$cont->driver_name}}</td>
                                                                    <td class="text-center">{{$cont->driver_contact}}</td>
                                                                </tr>
                                                            @endforeach
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-lg-12">
                                                {{--<address>--}}
                                                {{--Invoice Date : {{$value->invoice_date}} <br>--}}
                                                {{--Invoice By :  {{$value->invoice_by}}<br>--}}
                                                {{--Checked By  :  {{$value->checked_by}}<br>--}}
                                                {{--</address>--}}

                                                <address class="pt-4">
                                                    <strong>Special Instruction  if any : {{$value->special_instruction}} </strong><br>
                                                    Remarks  : {{$value->remarks}} <br>
                                                </address>
                                                <h4 class="panel-title font-20 pt-5"><strong>* Trucking Updates </strong></h4>

                                                <table class="table table-bordered">
                                                    <thead>
                                                    <tr style="background-color: #8dc3428a; color: #000 !important;">
                                                        <th scope="col">#</th>
                                                        <th>BL</th>
                                                        <th>Container no</th>
                                                        <th scope="col">Processing Date  </th>
                                                        <th scope="col">Updated Status</th>
                                                        <th>More</th>
                                                        <th scope="col"> Updated by</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody >
                                                    <?php $i=1;?>
                                                    @foreach($status as $updates)
                                                        <tr>
                                                            <th scope="row">{{$i++}}</th>
                                                            <td>{{$updates->bill_lading}}</td>
                                                            <td>{{$updates->cont_no}}</td>
                                                            <td>{{$updates->processing_date}}   </td>
                                                            <td>{{$updates->processing_status}}</td>
                                                            <td>{{$updates->other_comment}}</td>
                                                            <td>{{$updates->name}} -- {{$updates->updated_at}}</td>

                                                        </tr>
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @empty
                                    <div class="row pt-3 pb-5 mt-4">
                                        <div class="col-md-6 mx-auto" >
                                                <center>
                                                    <div class="alert alert-success alert-dismissable">
                                                           <span class="mx-auto" style="text-align: center; color: red;">No results founded</span>
                                                    </div>
                                                </center>

                                            </div>
                                    </div>
                                @endforelse
                            </div>
                        </div>
                </div>
                @include('layouts.footer')
            </div>
        </div>

@include('layouts.javas')
    </div>
</div>
</body>
</html>