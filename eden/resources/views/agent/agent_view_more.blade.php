
<!DOCTYPE html>
<html>
@include('layouts.admin_view_order')

<body class="fixed-left">


<!-- Begin page -->
<div id="wrapper">

    <!-- ========== Left Sidebar Start ========== -->
@include('layouts.admin_sidebar_view_order')
<!-- Left Sidebar End -->

    <!-- Start right Content here -->

    <div class="content-page">
        <!-- Start content -->
        <div class="content">

            <!-- Top Bar Start -->
            <div class="topbar">

                <nav class="navbar-custom">

                    <ul class="list-inline float-right mb-0">
                        <li class="list-inline-item dropdown notification-list">
                            <a class="nav-link dropdown-toggle arrow-none waves-effect" data-toggle="dropdown" href="#" role="button"
                               aria-haspopup="false" aria-expanded="false">
                                <i class="mdi mdi-email-outline noti-icon"></i>
                                <span class="badge badge-danger noti-icon-badge">5</span>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right dropdown-arrow dropdown-menu-lg">
                                <!-- item-->
                                <div class="dropdown-item noti-title">
                                    <h5><span class="badge badge-danger float-right">745</span>Messages</h5>
                                </div>

                                <!-- item-->
                                <a href="javascript:void(0);" class="dropdown-item notify-item">
                                    <div class="notify-icon"><img src="assets/images/users/avatar-2.jpg" alt="user-img" class="img-fluid rounded-circle" /> </div>
                                    <p class="notify-details"><b>Charles M. Jones</b><small class="text-muted">Dummy text of the printing and typesetting industry.</small></p>
                                </a>

                                <!-- item-->
                                <a href="javascript:void(0);" class="dropdown-item notify-item">
                                    <div class="notify-icon"><img src="assets/images/users/avatar-3.jpg" alt="user-img" class="img-fluid rounded-circle" /> </div>
                                    <p class="notify-details"><b>Thomas J. Mimms</b><small class="text-muted">You have 87 unread messages</small></p>
                                </a>

                                <!-- item-->
                                <a href="javascript:void(0);" class="dropdown-item notify-item">
                                    <div class="notify-icon"><img src="assets/images/users/avatar-4.jpg" alt="user-img" class="img-fluid rounded-circle" /> </div>
                                    <p class="notify-details"><b>Luis M. Konrad</b><small class="text-muted">It is a long established fact that a reader will</small></p>
                                </a>

                                <!-- All-->
                                <a href="javascript:void(0);" class="dropdown-item notify-item">
                                    View All
                                </a>

                            </div>
                        </li>



                    </ul>

                    <ul class="list-inline menu-left mb-0">
                        <li class="list-inline-item">
                            <button type="button" class="button-menu-mobile open-left waves-effect">
                                <i class="ion-navicon"></i>
                            </button>
                        </li>
                        <li class="hide-phone list-inline-item app-search">
                            <h3 class="page-title">Details</h3>
                        </li>
                    </ul>

                    <div class="clearfix"></div>

                </nav>

            </div>
            <!-- Top Bar End -->

            <div class="page-content-wrapper ">

                <div class="container-fluid">

                    @foreach($track as $value)
                        <div class="row pt-3 pb-5">
                            <div class="col-lg-12">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="col-lg-12">
                                            <h4 class="font-16"><strong> File Number:  {{$value->file_no}}</strong></h4>
                                            <h2 class="mt-0 header-title " >Document Received: {{$value->document_date_received}}</h2>
                                            <h4 class="mt-0 header-title">Bill of ladding</h4>
                                            <p class="text-muted m-b-30 font-14">{{$value->bill_lading}}</p>
                                        </div>
                                        <div class="col-lg-12">
                                            <img src="/assets/images/slogo.jpeg" class="rounded-circle img-circle  float-right" alt="..." style="margin-top: -11rem;width: 22%;">
                                        </div>

                                        <div class="col-lg-12">
                                            <!-- Nav tabs -->
                                            <ul class="nav nav-tabs" role="tablist">
                                                <li class="nav-item">
                                                    <a class="nav-link active" data-toggle="tab" href="#home" role="tab" style="color: black;"><strong>Consignee</strong></a>
                                                </li>

                                                <li class="nav-item">
                                                    <a class="nav-link" data-toggle="tab" href="#eta" role="tab"> <strong>ETA</strong></a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" data-toggle="tab" href="#ata" role="tab"><strong> ATA</strong></a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" data-toggle="tab" href="#shipper" role="tab"><strong>Shipper</strong></a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" data-toggle="tab" href="#vesell" role="tab"><strong>Vessel </strong></a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" data-toggle="tab" href="#driver" role="tab"><strong>Driver</strong></a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" data-toggle="tab" href="#profile" role="tab"><strong>Others</strong></a>
                                                </li>

                                            </ul>
                                        </div>
                                        <div class="col-lg-12">
                                            <!-- Tab panes -->
                                            <div class="tab-content">
                                                <div class="tab-pane active p-3" id="home" role="tabpanel">
                                                    <p class="font-14 mb-0">
                                                        <strong>Client / Consignee :  </strong> {{$value->client_name}} <br>
                                                    </p>
                                                </div>

                                                <div class="tab-pane p-3" id="eta" role="tabpanel">
                                                    <p class="font-14 mb-0">
                                                        <strong>ETA : </strong>{{$value->eta}}  <br>
                                                    </p>
                                                </div>
                                                <div class="tab-pane p-3" id="ata" role="tabpanel">
                                                    <p class="font-14 mb-0">
                                                        <strong>ATA : </strong>{{$value->ata}}  <br>
                                                    </p>
                                                </div>
                                                <div class="tab-pane p-3" id="shipper" role="tabpanel">
                                                    <p class="font-14 mb-0">
                                                        <strong>Shipper : </strong>{{$value->shipper}}  <br>
                                                    </p>
                                                </div>
                                                <div class="tab-pane p-3" id="vesell" role="tabpanel">
                                                    <p class="font-14 mb-0">
                                                        <strong>Vessel Name: </strong>{{$value->line_vessel}}  <br>
                                                    </p>
                                                </div>
                                                <div class="tab-pane p-3" id="profile" role="tabpanel">
                                                    <p class="font-14 mb-0">
                                                        <strong >Cargo Description : </strong> {{$value->cargo_description}} <br>
                                                        <strong>Weight as per  BL : </strong>{{$value->weight}}  <br>
                                                        <strong>Port of Loading :</strong>{{$value->port_charge}}  <br>
                                                        <strong>Voyage Number  : </strong>{{$value->voyager_number}}  <br>
                                                    </p>
                                                </div>
                                                <div class="tab-pane p-3" id="driver" role="tabpanel">
                                                    @foreach($driver as $driv)
                                                        <p class="font-14 mb-0">
                                                            <strong >Name: </strong> {{$driv->driver_name}} <br>
                                                            <strong>Contact : </strong>{{$driv->driver_contact}}  <br>
                                                        </p>
                                                    @endforeach
                                                </div>

                                            </div>
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="panel panel-default">
                                                <div class="p-2">
                                                    <h3 class="panel-title font-20"><strong>CARGO LOADING DETAILS </strong></h3>
                                                </div>
                                                <div class="">
                                                    <div class="table-responsive">
                                                        <table class="table ">
                                                            <thead>
                                                            <tr>
                                                                <th><strong>Container Number </strong></th>
                                                                <th class="text-center"><strong>Size /  Units</strong></th>
                                                                <th class="text-center"><strong>Weight</strong>
                                                                </th>
                                                                <th class="text-center"><strong>Transporter</strong>
                                                                </th>
                                                                <th  class="text-center"><strong>Truck / Trailer No</strong>
                                                                </th>
                                                                <th  class="text-center"><strong>Date of Loading</strong></th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            @foreach($containers as $cont)
                                                                <tr>
                                                                    <td >{{$cont->cont_no}}</td>
                                                                    <td class="text-center">{{$cont->type}}</td>
                                                                    <td class="text-center">{{$cont->line_vessel}}</td>
                                                                    <td class="text-center">{{$cont->transporter_name}}</td>
                                                                    <td class="text-center">{{$cont->trucking_number}}</td>
                                                                    <td class="text-center">{{$cont->loading_date}}</td>
                                                                </tr>
                                                            @endforeach
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-lg-12">
                                                {{--<address>--}}
                                                {{--Invoice Date : {{$value->invoice_date}} <br>--}}
                                                {{--Invoice By :  {{$value->invoice_by}}<br>--}}
                                                {{--Checked By  :  {{$value->checked_by}}<br>--}}
                                                {{--</address>--}}
                                                <address>
                                                    <strong>Special Instruction  if any : {{$value->special_instruction}} </strong><br>
                                                    Remarks  : {{$value->remarks}} <br>
                                                </address>
                                            </div>
                                        </div>
                                    </div> <!-- end col -->
                                </div> <!-- end row -->

                                @endforeach
                            </div><!-- container -->


                        </div> <!-- Page content Wrapper -->

                </div> <!-- content -->

                @include('layouts.footer')

            </div>
            <!-- End Right content here -->

        </div>
        <!-- END wrapper -->


@include('layouts.javas')
</body>
</html>
<!-- Localized -->