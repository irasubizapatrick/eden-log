
<!DOCTYPE html>
<html>
@include('layouts.admin_view_order')

<body class="fixed-left">
<!-- Begin page -->
<div id="wrapper">

    <!-- ========== Left Sidebar Start ========== -->
@include('layouts.admin_sidebar')
<!-- Left Sidebar End -->

    <!-- Start right Content here -->

    <div class="content-page">
        <!-- Start content -->
        <div class="content">

            <!-- Top Bar Start -->
            <!-- Top Bar End -->

            <div class="page-content-wrapper ">

                <div class="container-fluid">


                        <div class="row pt-3 pb-5" style="padding-top: 5rem !Important;">
                            <div class="col-lg-12">
                                <div class="card">
                                    <div class="card-body">

                                        <div class="col-lg-12">
                                            <img src="/assets/images/slogo.jpeg" class="rounded-circle img-circle  float-right" alt="..." style=" width: 22%;">
                                        </div>

                                        <div class="col-lg-12">
                                            <!-- Nav tabs -->
                                         <h4 class="mx-5">Chat Messages</h4>
                                        </div>
                                        <form class="form-horizontal"  action="/reply" method="POST" enctype="multipart/form-data">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                        <div class="col-lg-12">
                                            <!-- Tab panes -->
                                            <div class="tab-content">
                                                <div class="tab-pane active p-3" id="home" role="tabpanel">
                                                    <div class="col-md-6 col-xl-8 pl-md-3 px-lg-auto px-0">

                                                        <div class="chat-message">
                                                            @foreach($message_status as $value)
                                                            <ul class="list-unstyled chat">
                                                                <li class="d-flex justify-content-between mb-4">

                                                                    <div class="chat-body white p-3 ml-2 z-depth-1">
                                                                        <div class="col-md-12">  <div class="header">
                                                                                <img data-name="{{$value->user->name}}" class="rounded-circle img-circle profile" alt="..."style="width:  45px;height: 45px; margin-top: -11px;">
                                                                                <small class="primary-font">{{$value->user->name}}</small>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-12">
                                                                            <hr class="w-100">
                                                                            <p class="mb-0">
                                                                                <label>Response: </label>
                                                                                {{$value->message_responses}}
                                                                            </p>
                                                                            <small class="pull-right  float-right"><i class="far fa-clock"></i>Created  Date : {{$value->created_at}}</small>
                                                                        </div>
                                                                    </div>
                                                                    <input type="hidden" name="message_id" value="{{$value->message->id}}">
                                                                </li>
                                                                @endforeach
                                                                <div class="col-md-6">
                                                                    <div class="form-group basic-textarea">
                                                                        <textarea class="form-control pl-2 my-0" name="message_responses" id="exampleFormControlTextarea2" rows="3" placeholder="Type your response here..."></textarea>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-12 mt-2">
                                                                    <a  href="{{URL::previous()}}" role="button" class="btn btn-danger waves-effect" data-dismiss="modal"> <i class="fa fa-arrow-left"></i> Get Back</a>
                                                                    <button type="submit" class="btn btn-primary waves-effect waves-light"> <i class="fa fa-paper-plane"></i> Send</button>
                                                                </div>
                                                            </ul>
                                                        </div>

                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </form>
                                    </div> <!-- end col -->
                                </div> <!-- end row -->
                            </div><!-- container -->


                        </div> <!-- Page content Wrapper -->

                </div> <!-- content -->

                @include('layouts.footer')

            </div>
            <!-- End Right content here -->

        </div>
        <!-- END wrapper -->


@include('layouts.javas')
</body>
</html>
<!-- Localized -->