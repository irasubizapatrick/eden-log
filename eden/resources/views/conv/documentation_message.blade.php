<!DOCTYPE html>
<html>

@include('layouts.admin_modal')
<style>
    .size{
        height: calc(3.25rem + 2px) !Important;
    }
    .has-submenu{
        padding: 0px 10px 0px 30px;
    }
</style>
<body>

@include('layouts.documentation_sidebar')
<div class="wrapper">
    <div class="container-fluid">
        <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h3 class="modal-title mt-0" id="myLargeModalLabel">Add Message </h3>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body">
                        <form class="form-horizontal"  action="/conversation" method="POST" enctype="multipart/form-data">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Agent Name</label>
                                    <select class="form-control size" name="agent_id" >
                                        <option value="">--- Select Agent ---</option>
                                        @foreach($agent as $agent_value)
                                            <option value="{{$agent_value->id}}">{{$agent_value->organization_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-6">
                                    <label>Message Title</label>
                                    <input type="text" class="form-control" name="message_title" placeholder="Enter  Message title" required>
                                </div>

                                <div class="col-md-12">
                                    <label>Message Description</label>
                                    <textarea type="text" class="form-control" name="message_description" required >
                                    </textarea>
                                </div>
                                <div class="col-md-6 mx-auto">
                                    <div class="modal-footer ">
                                        <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Close</button>
                                        <button type="submit" class="btn btn-primary waves-effect waves-light">Save changes</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row m-0 p-2">
        <div class="col-sm-12">
            <div class="page-title-box" style="padding: 60px 0px 46px 0px !important">
                <div class="btn-group pull-right">
                    <ol class="breadcrumb hide-phone p-0 m-0">
                        <li class="breadcrumb-item"><a href="#">All Conversation </a></li>
                        <li class="breadcrumb-item active">Dashboard</li>
                    </ol>
                </div>
                <h4 class="page-title">All Conversation</h4>
            </div>
        </div>
        <div class="col-12 container" style="margin-top: -3rem;">
            <div class="card m-b-30">
                <div class="card-body">
                    <button type="button" class="btn btn-primary waves-effect waves-light pull-right" data-toggle="modal" data-target=".bs-example-modal-lg "><i class="fa fa-plus"></i> <span>Add Message</span></button>
                    @if (Session::has('message'))
                        <div class="alert alert-success alert-dismissable">
                            <a href="" class="close" data-dismiss="alert" aria-label="close">
                                &times;

                            </a>
                            {{ Session::get('message') }}
                        </div>

                    @endif
                    @if (Session::has('delete'))
                        <div class="alert alert-success alert-dismissable">
                            <a href="" class="close" data-dismiss="alert" aria-label="close">
                                &times;

                            </a>
                            {{ Session::get('delete') }}
                        </div>

                    @endif
                    @if (Session::has('updated'))
                        <div class="alert alert-success alert-dismissable">
                            <a href="" class="close" data-dismiss="alert" aria-label="close">
                                &times;

                            </a>
                            {{ Session::get('updated') }}
                        </div>

                    @endif

                    <table id="datatable-buttons" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                        <tr style="font-size: 13px;background-color: #416b8d !important;color: #fff;">
                            <th>#</th>
                            <th>Agent</th>
                            <th>Subject Title </th>
                            <th>Message </th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $i=1;?>
                        @foreach($message AS $value)
                            <tr>
                                <td>{{$i++}}</td>
                                <td>{{$value->agent->organization_name}}</td>
                                <td>{{$value->message_title}}</td>
                                <td>{{$value->message_description}}</td>
                                <td>
                                    <button data-toggle="modal" data-target="#editleads<?php echo $i;?>" class="pull-left edit btn-lg  btn-primary dlt_sm_table mx-2 mt-2"><i class="fa fa-pencil"></i></button>
                                    <a href="{{route ('ViewConversation',['id' =>$value->id])}}" role="button"  class="pull-left edit btn-lg  btn-success  mx-2 mt-2"><span> <i class="fa fa-comment"></i> reply </span></a>
                                    <div id="response<?php echo $i;?>" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
                                        <div class="modal-dialog modal-lg">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                </div>
                                                <div class="modal-body">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <h4>Response </h4>
                                                            <p>{{$value->message_description}}</p>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="reply<?php echo $i;?>" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
                                        <div class="modal-dialog modal-lg">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h3 class="modal-title" id="editleads"> Add Response</h3>
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                </div>
                                                <div class="modal-body">
                                                    <form class="form-horizontal"  action="/reply" method="POST" enctype="multipart/form-data">
                                                        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                                        <div class="card grey lighten-3 chat-room">
                                                            <div class="card-body">

                                                                <!-- Grid row -->
                                                                <div class="row px-lg-2 px-2">
                                                                    <!-- Grid column -->
                                                                    <div class="col-md-6 col-xl-8 pl-md-3 px-lg-auto px-0">

                                                                        <div class="chat-message">

                                                                            <ul class="list-unstyled chat">
                                                                                <li class="d-flex justify-content-between mb-4">

                                                                                    <div class="chat-body white p-3 ml-2 z-depth-1">
                                                                                        <div class="header">
                                                                                            <strong class="primary-font">{{$value->user_id}}</strong>
                                                                                            <small class="pull-right text-muted"><i class="far fa-clock"></i> 12 mins ago</small>
                                                                                        </div>
                                                                                        <hr class="w-100">
                                                                                        <p class="mb-0">
                                                                                            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut
                                                                                            labore et dolore magna aliqua.
                                                                                        </p>
                                                                                    </div>
                                                                                </li>
                                                                                <li class="white">
                                                                                    <div class="form-group basic-textarea">
                                                                                        <textarea class="form-control pl-2 my-0" name="message_responses" id="exampleFormControlTextarea2" rows="3" placeholder="Type your response here..."></textarea>
                                                                                        <input type="hidden" name="agent_id" value="{{$value->agent_id}}">
                                                                                        <input type="hidden" name="message_id" value="{{$value->id}}">

                                                                                    </div>
                                                                                </li>
                                                                                <div class="col-md-12 mt-2">
                                                                                    <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Close</button>
                                                                                    <button type="submit" class="btn btn-primary waves-effect waves-light">Send</button>
                                                                                </div>
                                                                            </ul>

                                                                        </div>

                                                                    </div>
                                                                    <!-- Grid column -->

                                                                </div>
                                                                <!-- Grid row -->

                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="editleads<?php echo $i;?>" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
                                        <div class="modal-dialog modal-lg">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h3 class="modal-title" id="editleads"> Edit Message Details</h3>
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

                                                </div>
                                                <div class="modal-body">
                                                    <div class="row">
                                                        <form role="form" action="/conversation/{{$value->id}}" method="post" enctype="multipart/form-data">
                                                            <input type="hidden" name="_method" value="PUT" />
                                                            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                                            <div class="col-md-6">
                                                                <label>Agent Name</label>
                                                                <select class="form-control size" name="agent_id" >
                                                                    @foreach($agent as $agent_value)
                                                                        <option value="{{$agent_value->id}}">{{$agent_value->organization_name}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <label>Message Title</label>
                                                                <input type="text" class="form-control"  value="{{$value->message_title}}" name="message_title" placeholder="Enter  Message title" required>
                                                            </div>

                                                            <div class="col-md-12">
                                                                <label>Message Description</label>
                                                                <textarea type="text" class="form-control" name="message_description" required >
                                                                        {{$value->message_description}}
                                                                    </textarea>
                                                            </div>
                                                            <div class="col-md-12 pt-5">
                                                                <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Close</button>
                                                                <button type="submit" class="btn btn-primary waves-effect waves-light">Save changes</button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>
</div>
</div>
@include('layouts.footer')

@include('layouts.javas')
</body>
</html>