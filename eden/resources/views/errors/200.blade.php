<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    @include('layouts.title')
    <meta content="Admin Dashboard" name="description" />
    <meta content="Logistics" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <!-- App Icons -->
    <link rel="shortcut icon" href="/landing/img/favicon.png">

    <!-- App css -->
    <link href="/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="/assets/css/icons.css" rel="stylesheet" type="text/css" />
    <link href="/assets/css/style.css" rel="stylesheet" type="text/css" />

    <style type="text/css">
        .logo{
            width: 100%;
            height:100%;
        }
        .body:before{
            content:'';
            position: absolute;
            width: 100%;
            height: 100%;
            background: rgba(0,0,0,.55);
            top: 0px;
            opacity: 0.9;
            z-index: 1;"
        }

    </style>
</head>
<body class="body">
<center>
    <div class="row">
        <!-- Begin page -->
        <div class="accountbg"></div>
        <div class="wrapper-page">

            <div class="card login_page">
                <div class="card-body">
                    <h3 class="text-center mt-0 m-b-15">
                        <a href="http://mikumifreight.com/" class="logo logo-admin"><img src="/assets/images/logo.png" height="auto"  width="250" alt="logo"></a>
                    </h3>

                    <div class="p-3">
                        <h3 class="pt-4">200</h3>
                        <h4 class="p-5">Success</h4>
                    </div>

                </div>
            </div>
        </div>
    </div>
</center>




<!-- jQuery  -->
<script src="/assets/js/jquery.min.js"></script>
<script src="/assets/js/popper.min.js"></script>
<script src="/assets/js/bootstrap.min.js"></script>
<script src="/assets/js/modernizr.min.js"></script>
<script src="/assets/js/waves.js"></script>
<script src="/assets/js/jquery.slimscroll.js"></script>
<script src="/assets/js/jquery.nicescroll.js"></script>
<script src="/assets/js/jquery.scrollTo.min.js"></script>


</body>
</html>
<!-- Localized -->
