<!DOCTYPE html>
<html>
@include('layouts.admin_head')
<body>

<!-- Navigation Bar-->
@include('layouts.admin_sidebar')
<!-- End Navigation Bar-->
<div class="wrapper">
    <div class="row mx-auto">
        <div class="col-md-12 ">
            <div class="page-title-box mx-auto">
                <div class="btn-group pull-right">
                    <ol class="breadcrumb hide-phone">
                        <li class="breadcrumb-item"><a href="#">Orders List</a></li>
                        <li class="breadcrumb-item active">Dashboard</li>
                    </ol>
                </div>
                <h4 class="page-title ">All Orders</h4>
            </div>
        </div>
        <div class="col-xl-12">
            <div class="card ">
                <div class="card-body col-xl-12">
                    <div class="table-responsive col-xl-12">
                        @if (Session::has('message'))
                            <div class="alert alert-success alert-dismissable">
                                <a href="" class="close" data-dismiss="alert" aria-label="close">
                                    &times;

                                </a>
                                {{ Session::get('message') }}
                            </div>

                        @endif
                        @if (Session::has('delete'))
                            <div class="alert alert-success alert-dismissable">
                                <a href="" class="close" data-dismiss="alert" aria-label="close">
                                    &times;

                                </a>
                                {{ Session::get('delete') }}
                            </div>

                        @endif
                        @if (Session::has('updated'))
                            <div class="alert alert-success alert-dismissable">
                                <a href="" class="close" data-dismiss="alert" aria-label="close">
                                    &times;

                                </a>
                                {{ Session::get('updated') }}
                            </div>

                        @endif

                        <table id="datatable-buttons" class="table table-hover table-striped table-bordered" cellspacing="0" width="100%" >
                            <thead>
                            <tr style="font-size: 13px;background-color: #416b8d !important;color: #fff;">
                                <th>#</th>
                                <th>Date</th>
                                <th>File No</th>
                                <th>Consignee</th>
                                <th>Bill of lading</th>
                                <th>Cont No</th>
                                <th> Line/ Vessel </th>
                                <th> C&F At Destination / Agent </th>
                                <th>Shipping Line</th>
                                <th>Shipper</th>
                                <th>Invoice</th>

                            </tr>
                            </thead>
                            <tbody>
                            <?php $i=1;?>
                            @foreach($track AS $value)
                                <tr style="font-size: 14px;">
                                    <td>{{$i++}}</td>
                                    <td>{{$value->date}} </td>
                                    <td>{{$value->file_no}} </td>
                                    <td>{{$value->client_name}} </td>
                                    <td>{{$value->bill_lading}} </td>
                                    <td>{{$value->cont_no}}</td>
                                    <td>{{$value->line_vessel}}</td>
                                    <td>{{$value->organization_name}}</td>
                                    <td>{{$value->shipping_line}}</td>
                                    <td>{{$value->shipper}}</td>
                                    <td>
                                        @if($value != '')
                                            <a href="#"  target="_blank">no file</a>
                                        @else
                                            <a href="/tax_clearance/{{$value->invoice_order}}"  target="_blank"> file</a>
                                        @endif
                                    </td>

                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>

    </div>
</div>
<!-- end wrapper -->


<!-- Footer -->
@include('layouts.footer')
<!-- End Footer -->


<!-- jQuery  -->
<script src="/assets/js/jquery.min.js"></script>
<script src="/assets/js/popper.min.js"></script>
<script src="/assets/js/bootstrap.min.js"></script>
<script src="/assets/js/modernizr.min.js"></script>
<script src="/assets/js/waves.js"></script>
<script src="/assets/js/jquery.slimscroll.js"></script>
<script src="/assets/js/jquery.nicescroll.js"></script>
<script src="/assets/js/jquery.scrollTo.min.js"></script>

<!-- Required datatable js -->
<script src="/assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="/assets/plugins/datatables/dataTables.bootstrap4.min.js"></script>
<!-- Buttons examples -->
<script src="/assets/plugins/datatables/dataTables.buttons.min.js"></script>
<script src="/assets/plugins/datatables/buttons.bootstrap4.min.js"></script>
<script src="/assets/plugins/datatables/jszip.min.js"></script>
<script src="/assets/plugins/datatables/pdfmake.min.js"></script>
<script src="/assets/plugins/datatables/vfs_fonts.js"></script>
<script src="/assets/plugins/datatables/buttons.html5.min.js"></script>
<script src="/assets/plugins/datatables/buttons.print.min.js"></script>
<script src="/assets/plugins/datatables/buttons.colVis.min.js"></script>
<!-- Responsive examples -->
<script src="/assets/plugins/datatables/dataTables.responsive.min.js"></script>
<script src="/assets/plugins/datatables/responsive.bootstrap4.min.js"></script>

<!-- Datatable init js -->
<script src="/assets/pages/datatables.init.js"></script>

<script src="/assets/js/initial.js-master/initial.js" type="text/javascript"></script>
<script>
    $('.profile').initial();
</script>

</body>
</html>
<!-- Localized -->