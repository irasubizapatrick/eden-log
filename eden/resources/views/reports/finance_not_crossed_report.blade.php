<!DOCTYPE html>
<html>
@include('layouts.finance_head')
<style>
    .form_size{
        height: calc(3.25rem + 2px) !Important;
    }
    .bl a:hover{
        text-decoration: underline;
        color: #376b8f !important;
    }
    .size{
        width: 350px;
    }

</style>
<body>

<!-- Navigation Bar-->
@include('layouts.finance_sidebar')
<!-- End Navigation Bar-->
<div class="wrapper">
    <div class="row mx-auto">
        <div class="col-md-12 ">
            <div class="page-title-box mx-auto" >
                <div class="btn-group pull-right">
                    <ol class="breadcrumb hide-phone">
                        <li class="breadcrumb-item"><a href="#">Orders List</a></li>
                        <li class="breadcrumb-item active">Dashboard</li>
                    </ol>
                </div>
                <h4 class="page-title ">Daily List of Consignments Not Crossed Border </h4>
            </div>
        </div>
        <div class="modal fade transporter"  role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header pt-4 pb-4">
                        <center>
                            <h4 class="modal-title mt-0 mx-4" id="myLargeModalLabel">Daily Reports  of Consignment Not Crossed Boarder</h4>

                        </center>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body">
                        <form class="" role="form" action="/daily-report-updated-not-crossed" method="POST" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="row container mx-0 p-3">
                                <div class="col-md-6">
                                    <label>Consignee</label>
                                    <select class="form-control size consignee"  name="client_name" >
                                        <option value=""> Choose</option>
                                        @foreach($track as $cons)
                                            <option value="{{$cons->client_name}}"> {{$cons->client_name}}</option>
                                        @endforeach

                                    </select>
                                </div>
                                <div class="col-md-6">
                                    <label>Agent</label>
                                    <select class="form-control size agent"  name="agent_id" >
                                        <option value=""> Choose</option>
                                        @foreach($track as $all)
                                            <option value="{{$all->agent_id}}"> {{$all->organization_name}}</option>
                                        @endforeach

                                    </select>
                                </div>

                                <div class="col-md-12 pt-5  pb-4" >
                                    <center>
                                        <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Close</button>
                                        <button type="submit" class="btn btn-primary waves-effect waves-light">Search</button>
                                    </center>

                                </div>

                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xl-12" style="margin-top: -3rem;">

            <div class="card ">
                <div class="card-body col-xl-12">

                    <div class="table-responsive col-xl-12">
                        <a href="/daily/report/not/crossed" role="button" class="btn btn-success sm waves-effect waves-light pull-right mx-2 button_search"><i class="fa fa-home"></i> <span> Daily Report </span></a>

                        <button type="button" class="btn btn-primary sm waves-effect waves-light pull-right mx-2 button_search" data-toggle="modal" data-target=".transporter "><i class="fa fa-search"></i> <span>Advanced Search</span></button>

                        @if (Session::has('message'))
                            <div class="alert alert-success alert-dismissable">
                                <a href="" class="close" data-dismiss="alert" aria-label="close">
                                    &times;

                                </a>
                                {{ Session::get('message') }}
                            </div>

                        @endif
                        @if (Session::has('delete'))
                            <div class="alert alert-success alert-dismissable">
                                <a href="" class="close" data-dismiss="alert" aria-label="close">
                                    &times;

                                </a>
                                {{ Session::get('delete') }}
                            </div>

                        @endif
                        @if (Session::has('updated'))
                            <div class="alert alert-success alert-dismissable">
                                <a href="" class="close" data-dismiss="alert" aria-label="close">
                                    &times;

                                </a>
                                {{ Session::get('updated') }}
                            </div>

                        @endif


                        <table id="datatable-buttons" class="table table-hover table-striped table-bordered" cellspacing="0" width="100%" >
                            <thead>
                            <tr style="font-size: 13px;background-color: #416b8d !important;color: #fff;">
                                <th>#</th>
                                <th>Date</th>
                                <th>File No</th>
                                <th>Consignee</th>
                                <th>BL</th>
                                <th>Vessel </th>
                                <th> C&F / Agent </th>
                                <th>Shipping Line</th>
                                <th>Shipper</th>
                                <th>Destination</th>
                                <th>Current Status</th>
                                <th>Comment</th>

                            </tr>
                            </thead>
                            <tbody>
                            <?php $i=1;?>
                            @foreach($track AS $value)
                                <tr style="font-size: 14px;">
                                    <td>{{$i++}}</td>
                                    <td>{{$value->date}} </td>
                                    <td>{{$value->file_no}} </td>
                                    <td>{{$value->client_name}} </td>
                                    <td class="bl"> <a href="{{route ('/view/all/container', ['id' =>$value->id])}}"  style="color: black;">{{$value->bill_lading}} </a> </td>
                                    <td>{{$value->line_vessel}}</td>
                                    <td>{{$value->organization_name}}</td>
                                    <td>{{$value->shipping_line}}</td>
                                    <td>{{$value->shipper}}</td>
                                    <th>{{$value->destination}}</th>
                                    <td>

                                        @if($value->current_status == 'Prepare for T1')

                                            <span class="label label-warning"> {{$value->current_status}}</span>

                                        @elseif($value->current_status == 'Crossed Border')

                                            <span class="label label-success"> {{$value->current_status}}</span>


                                        @elseif($value->current_status == 'Receiving the documents')

                                            <span class="label label-primary"> {{$value->current_status}} </span>

                                        @elseif($value->current_status == 'Under Customs Procedures')

                                            <span class="label label-warning"> {{$value->current_status}} </span>
                                        @else
                                            <span class="label label-info"> {{$value->current_status}}  </span>

                                        @endif

                                    </td>
                                    <td>
                                        {{$value->other_comment}}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                    </div>

                </div>
            </div>
        </div>

    </div>
</div>
<!-- end wrapper -->


<!-- Footer -->
@include('layouts.footer')
<!-- End Footer -->


@include('layouts.javas');
</body>
</html>
<!-- Localized -->