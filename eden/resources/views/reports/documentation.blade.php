<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    @include('layouts.title')
    <meta content="Admin Dashboard" name="description" />
    <meta content="Mikumi" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <!-- App Icons -->
    <link rel="shortcut icon" href="/landing/img/favicon.png">
    <script src="/assets/js/jquery.js"></script>


    <!-- DataTables -->
    <link href="/assets/css/bootstrap337.min.css" rel="stylesheet">
    <link href="/assets/plugins/datatables/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
    <link href="/assets/plugins/datatables/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css" />
    <!-- Responsive datatable examples -->
    <link href="/assets/plugins/datatables/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css" />

    <!-- App css -->
    <link href="/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="/assets/css/icons.css" rel="stylesheet" type="text/css" />
    <link href="/assets/css/style.css" rel="stylesheet" type="text/css" />
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600" rel="stylesheet">
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">

    <link href="/assets/css/report_select_box.css" rel="stylesheet" type="text/css" />
</head>
<style>
    .dt-buttons{
        margin-left: -2% !important;
    }
    .has-submenu{
        padding: 0px 10px 0px 30px;
    }
    #datatable-buttons_filter{
        display: none !important;
    }
    .form_size{
        height: calc(3.25rem + 2px) !Important;
    }
    /*.button_search{*/
    /*height: 4rem;*/
    /*}*/
</style>
<body>

@include('layouts.documentation_sidebar')

<div class="wrapper">
    <div class="row m-0">
        <div class="col-sm-12">
            <div class="page-title-box" style="padding: 58px 0px 19px 0px !important">
                <div class="btn-group pull-right">
                    <ol class="breadcrumb hide-phone p-0 m-0">
                        <li class="breadcrumb-item"><a href="#">Orders List</a></li>
                        <li class="breadcrumb-item active">Dashboard</li>
                    </ol>
                </div>
                <h4 class="page-title">All Orders Reports</h4>
            </div>
        </div>
        <div class="col-xl-12" >
            <div class="card">

                <div class="modal fade period"  role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title mt-0 mx-4" id="myLargeModalLabel">Search Based on Period </h4>
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            </div>
                            <div class="modal-body">
                                <form class="" role="form" action="/sortOrders" method="POST" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                    <div class="row container mx-0 p-3">
                                        <div class="col-md-6">
                                            <label>From</label>
                                            <input type="date" name="date1" value="" class="form-control" >
                                        </div>
                                        <div class="col-md-6" >
                                            <label>To</label>
                                            <input type="date" name="date2"   value="" class="form-control" >
                                        </div>
                                        <div class="col-md-12  pt-5  pb-3" >
                                            <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-primary waves-effect waves-light">Search</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div><!-- /.modal-content -->
                </div>
                <div class="modal fade destinations"  role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title mt-0 mx-4" id="destinations">Search Based on Destinations</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            </div>
                            <div class="modal-body">
                                <form class="" role="form" action="/sortDestination" method="POST" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                    <div class="row container mx-0 p-3">

                                        <div class="col-md-6">
                                            <label>Country</label>
                                            <select class="form-control  country" name="country_id">
                                                <option value=""> Choose </option>
                                                @foreach($country as $key => $value)
                                                    <option value="{{ $key }}">{{$value}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-md-6">
                                            <label> Destination </label>
                                            <select class="form-control destination" name="destination"  >
                                                <option value="">Select </option>
                                            </select>
                                            </select>
                                        </div>
                                        <div class="col-md-12  pt-5  pb-3" >
                                            <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-primary waves-effect waves-light">Search</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal fade vessel"  role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title mt-0 mx-4" id="destinations">Search Based on Line /Vessel</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            </div>
                            <div class="modal-body">
                                <form class="" role="form" action="/sort/Vessel" method="POST" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                    <div class="row container mx-0 p-3">
                                        <div class="col-md-6">
                                            <select class="form-control form_size" name="line_vessel" id="vessel"  required>
                                                <option value=""> Select Vessel</option>
                                                @foreach($destiny_line as $desk)
                                                    <option value="{{$desk[0]->line_vessel}}"> {{$desk[0]->line_vessel}}</option>
                                                @endforeach
                                                {{--<option value="OTHER">OTHER</option>--}}
                                            </select>
                                        </div>
                                        <div class="col-md-12  pt-5  pb-3" >
                                            <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-primary waves-effect waves-light">Search</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal fade cf"  role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title mt-0 mx-4" id="myLargeModalLabel">Search By Clearing at Destination </h4>
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            </div>
                            <div class="modal-body">
                                <form class="" role="form" action="/Report/Consignee" method="POST" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                    <div class="row container mx-0 p-3">
                                        <div class="col-md-6">
                                            <select class="form-control form_size" name="consignee_id" id="edit_ladding_id4"   required>
                                                <option value="">Select C&A at Destination</option>
                                                @foreach($agent as $age)
                                                    <option value="{{$age->id}}}">{{$age->organization_name}}</option>
                                                @endforeach
                                            </select>

                                        </div>
                                        <div class="col-md-12 pt-5  pb-3" >
                                            <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-primary waves-effect waves-light">Search</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal fade driver"  role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title mt-0 mx-4" id="myLargeModalLabel">Search By Driver </h4>
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            </div>
                            <div class="modal-body">
                                <form class="" role="form" action="/sort/Driver" method="POST" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                    <div class="row container mx-0 p-3">
                                        <div class="col-md-6">
                                            <select class="form-control form_size" name="driver_name" id="edit_ladding_id3"  required>
                                                <option value="">Select Driver Name</option>
                                                @foreach($driver as $drivers)
                                                    <option value="{{$drivers[0]->driver_name}}">{{$drivers[0]->driver_name}}</option>
                                                @endforeach
                                                <option value="OTHER">OTHER</option>
                                            </select>
                                        </div>
                                        <div class="col-md-12 pt-5  pb-3" >
                                            <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-primary waves-effect waves-light">Search</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal fade transporter"  role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title mt-0 mx-4" id="myLargeModalLabel">Search By Transporter </h4>
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            </div>
                            <div class="modal-body">
                                <form class="" role="form" action="/Report/Transporter" method="POST" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                    <div class="row container mx-0 p-3">
                                        <div class="col-md-6">
                                            <select class="form-control form_size" name="transporter_name" id="edit_ladding_id2"  required>
                                                <option value="">Select Transporter Name</option>
                                                @foreach($transporter as $transport)
                                                    <option value="{{$transport[0]->transporter_name}}">{{$transport[0]->transporter_name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-md-12 pt-5  pb-3" >
                                            <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-primary waves-effect waves-light">Search</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal fade consigneed"  role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title mt-0 mx-4" id="destinations">Search Based on Consignee</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            </div>
                            <div class="modal-body">
                                <form class="" role="form" action="/sort/Client" method="POST" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                    <div class="row container mx-0 p-3">
                                        <div class="col-md-6">
                                            <select class="form-control form_size" name="client_name" id="edit_ladding_id1"  required>
                                                <option value=""> Select Consignee</option>
                                                @foreach($destiny_client as $desk)
                                                    <option value="{{$desk[0]->client_name}}"> {{$desk[0]->client_name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-md-12  pt-5  pb-3" >
                                            <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-primary waves-effect waves-light">Search</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card-body">
                    <button type="button" class="btn btn-primary waves-effect waves-light pull-right mx-2 button_search" data-toggle="modal" data-target=".driver "><i class="fa fa-search"></i> <span>Drivers</span></button>
                    <button type="button" class="btn btn-primary waves-effect waves-light pull-right mx-2 button_search" data-toggle="modal" data-target=".transporter "><i class="fa fa-search"></i> <span>Transporter</span></button>
                    <button type="button" class="btn btn-primary waves-effect waves-light pull-right mx-2 button_search" data-toggle="modal" data-target=".period "><i class="fa fa-search"></i> <span>Period</span></button>
                    <button type="button" class="btn btn-primary waves-effect waves-light pull-right mx-2 button_search" data-toggle="modal" data-target=".destinations"><i class="fa fa-search"></i> <span>Destinations </span></button>
                    <button type="button" class="btn btn-primary waves-effect waves-light pull-right mx-2 button_search" data-toggle="modal" data-target=".consigneed "><i class="fa fa-search"></i> <span>Consignee</span></button>
                    <button type="button" class="btn btn-primary btn-lg waves-effect waves-light pull-right mx-2  button_search"  data-toggle="modal" data-target=".cf"><i class="fa fa-search"></i> <span>C&A at Destination </span></button>
                    <button  type="button" class="btn btn-primary btn-lg waves-effect waves-light pull-right mx-2   button_search"  data-toggle="modal" data-target=".vessel"><i class="fa fa-search"></i> <span>Line / Vessel</span></button>

                    <a  href="/reports" type="button" role="button"  class="btn btn-success btn-lg waves-effect waves-light pull-right mx-2   button_search" ><i class="fa fa-home"></i> <span >All Orders</span></a>

                    <div class="table-responsive mt-4">
                        @if (Session::has('message'))
                            <div class="alert alert-success alert-dismissable">
                                <a href="" class="close" data-dismiss="alert" aria-label="close">
                                    &times;

                                </a>
                                {{ Session::get('message') }}
                            </div>

                        @endif
                        @if (Session::has('delete'))
                            <div class="alert alert-success alert-dismissable">
                                <a href="" class="close" data-dismiss="alert" aria-label="close">
                                    &times;

                                </a>
                                {{ Session::get('delete') }}
                            </div>

                        @endif
                        @if (Session::has('updated'))
                            <div class="alert alert-success alert-dismissable">
                                <a href="" class="close" data-dismiss="alert" aria-label="close">
                                    &times;

                                </a>
                                {{ Session::get('updated') }}
                            </div>

                        @endif
                        <table id="datatable-buttons" class=" table table-hover table-striped table-bordered" cellspacing="0" width="100%" >
                            <thead>
                            <tr style="font-size: 13px;background-color: #416b8d !important;color: #fff;">
                                <th>#</th>
                                <th>Date</th>
                                <th>BL</th>
                                <th>Consignee</th>
                                <th>ETA</th>
                                <th> C&F </th>
                                <th>Shipping Line</th>
                                <th>Destination</th>
                                <th>Status</th>


                            </tr>
                            </thead>
                            <tbody>
                            <?php $i=1;?>
                            @foreach($track AS $value)
                                <tr style="font-size: 12px;">
                                    <td>{{$i++}}</td>
                                    <td>{{$value->document_date_received}} </td>
                                    <td>{{$value->bill_lading}} </td>
                                    <td>{{$value->client_name}} </td>
                                    <td>{{$value->eta}}</td>
                                    <td>{{$value->organization_name}}</td>
                                    <td>{{$value->shipping_line}}</td>
                                    <td>{{$value->destination}}</td>
                                    <td>

                                        @if($value->current_status == 'Prepare for T1')

                                            <span class="label label-danger"> {{$value->current_status}}</span>
                                        @elseif($value->current_status == 'Crossed Border')

                                            <span class="label label-success"> {{$value->current_status}}</span>
                                        @elseif($value->current_status == 'Loading')

                                            <span class="label label-danger"> {{$value->current_status}}</span>



                                        @elseif($value->current_status == 'Receiving the documents')

                                            <span class="label label-primary"> {{$value->current_status}} </span>

                                        @elseif($value->current_status == 'Under Customs Procedures')

                                            <span class="label label-warning"> {{$value->current_status}} </span>
                                        @elseif($value->current_status == '')

                                            <span class="label label-default"> no update </span>
                                        @else
                                            <span class="label label-info"> {{$value->current_status}} </span>
                                        @endif

                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
</div>
@include('layouts.footer')
@include('layouts.javas')
<script type="text/javascript">
    $(document).ready(function() {


        $('select[name="country_id"]').on('change', function() {
            var stateID = $(this).val();
            if(stateID) {
                $.ajax({
                    url: '/country_name/ajax/'+stateID,
                    type: "GET",
                    dataType: "json",
                    success:function(data) {


                        $('select[name="destination"]').empty();
                        $.each(data, function(key, value) {
                            $('select[name="destination"]').append('<option value="'+ value +'">'+ value +'</option>');
                        });


                    }
                });
            }else{
                $('select[name="destination"]').empty();
            }
        });
    });
</script>


</body>
</html>