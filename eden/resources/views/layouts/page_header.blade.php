
<header class="main_header_area">
    <div class="header_top">
        <div class="container">
            <div class="pull-left">
                <a href="/"><img src="/landing/img/mikumi.jpeg" alt="" height="100"></a>
            </div>
            <div class="pull-right contact">
                <div class="header_c_text">
                    <h5>Call us</h5>
                    <h4>+255 714 307 315</h4>
                </div>
                <div class="header_c_text">
                    <h5>Working Days</h5>
                    <h4>Mon - Fri </h4>
                </div>
                <div class="header_c_text">
                    <h5>Email Us</h5>
                    <h4><a href="../../cdn-cgi/l/email-protection/index.html" class="__cf_email__" style="color: #0b0b0b !important;">info@mikumifreight.com</a></h4>
                </div>
                <div class="header_c_text">
                    <a class="quote_btn" href="/tracking_truck"> Cargo Tracking</a>
                </div>
            </div>
        </div>
    </div>
    <div class="main_menu_area">
        <nav class="navbar navbar-default">
            <div class="container">

                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>

                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li class="active"><a href="/">Home</a></li>
                        <li><a href="/about_us">About Us</a></li>
                        <li><a href="/our_service">Our Services</a></li>
                        <li><a href="/tracking_truck">Cargo tracking</a></li>
                        <li><a href="#">Blog</a></li>
                        <li><a href="contact_us">Contact us</a></li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        <li>
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="Search here...">
                                <span class="input-group-btn" >
                                <button class="btn btn-default search" type="button"><i class="fa fa-search" aria-hidden="true"></i></button>
                                </span>
                            </div>
                        </li>
                    </ul>
                </div>

            </div>

        </nav>
    </div>
</header>
