<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="/landing/img/favicon.png" type="image/x-icon" />

    <title>Mikumi Freight Forwarders Ltd  </title>

    <link href="/landing/css/font-awesome.min.css" rel="stylesheet">

    <link href="/landing/css/bootstrap.min.css" rel="stylesheet">

    <link href="/landing/vendors/owl-carousel/assets/owl.carousel.min.css" rel="stylesheet">
    <link href="/landing/css/style.css" rel="stylesheet">

</head>
<style type="text/css">
    ::-webkit-input-placeholder { /* Edge */
        color: #1d6891 !important;
    }
    .search{
        color: #1d6891 !important;
    }
    .contact
    {
        padding-top: 2%;
    }
    .__cf_email__{
        color:  #b8d4df !important;
    }
    .title{
        font-size: x-large;
        font-family: 'Rubik', sans-serif;
        color: #202e39;
        line-height: 38px;
        text-transform: none;
        font-weight: lighter;

    }
    .title_desc{
        font-size: 18px !important;
        font-family: 'Rubik', sans-serif!important;
        color: #202e39 !important;
        line-height: 2.4!important;
        text-transform: none !important;
    }
    #one_paragraph{
        padding-top: 3rem;
        font-weight: lighter;
    }
</style>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-142579219-1"></script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-142579219-1');
</script>
