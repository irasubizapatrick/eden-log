<header id="topnav">
    <div class="navbar-custom">
        <div class="container-fluid">
            <div id="navigation">
                <ul class="navigation-menu">

                    <li class="has-submenu">
                        <a href="/dashboard"><i class="ti-home mx-3"></i>Dashboard</a>
                    </li>

                    <li class="has-submenu">
                        <div class="dropdown">
                            <a class=" dropdown-toggle admin_submenu"  data-toggle="dropdown"> <i class="ti ti-package mx-3"></i> Manage  Consignment
                            </a>
                            <ul class="dropdown-menu">
                                <div class="dropdown-divider"></div>
                                <li>
                                    <a href="/add_orders">Create Consignment</a>
                                </li>
                                <div class="dropdown-divider"></div>
                                <li>
                                    <a href="/all_trans">Assign Container  to Transporter</a>
                                </li>
                                <div class="dropdown-divider"></div>
                                <li>
                                    <a href="/all_driver">Assign Container  to Driver</a>
                                </li>

                                <div class="dropdown-divider"></div>
                                <li>
                                    <a href="/archive_doc">Archives</a>
                                </li>
                                <div class="dropdown-divider"></div>

                                <li>
                                    <a href="/pending/container">Pending Containers</a>
                                </li>
                                <div class="dropdown-divider"></div>
                                <li>
                                    <a href="/all_drop">Drop off of Container</a>
                                </li>
                                <li >
                                    <a href="/clearance">   Customs Release </a>
                                </li>
                                <div class="dropdown-divider"></div>
                                <li>
                                    <a href="/all_order">All orders</a>
                                </li>
                                <div class="dropdown-divider"></div>
                            </ul>
                        </div>
                    </li>
                    <li class="has-submenu">
                        <div class="dropdown">
                            <a class=" dropdown-toggle admin_submenu"  data-toggle="dropdown"> <i class="ti ti-truck mx-3"></i> Manage Cargo
                            </a>
                            <ul class="dropdown-menu">
                                <div class="dropdown-divider"></div>
                                <li>
                                    <a href="/tracking">Cargo Tracking</a>
                                </li>
                                <div class="dropdown-divider"></div>
                                <li>
                                    <a href="/add_status">Cargo Updates</a>
                                </li>
                                <div class="dropdown-divider"></div>
                            </ul>
                        </div>
                    </li>
                    <li class="has-submenu">
                        <div class="dropdown">
                            <a class=" dropdown-toggle admin_submenu"  data-toggle="dropdown"> <i class="ti ti-file mx-3"></i> Requisitions
                            </a>
                            <ul class="dropdown-menu">
                                <div class="dropdown-divider"></div>
                                <li>
                                    <a href="/requisition_form">Consignee Expenses
                                        @if($expenses !='')
                                            <span style="color: red;">( {{$expenses}} )</span>
                                        @else
                                            <span style="color: red;">( 0 )</span>
                                        @endif

                                    </a>
                                </li>
                                <div class="dropdown-divider"></div>
                                <li>
                                    <a href="/requisition/form/normal">Normal  Expenses

                                        @if($normal !='')
                                            <span style="color: red;">( {{$normal}} )</span>
                                        @else
                                            <span style="color: red;">( 0 )</span>
                                        @endif

                                    </a>
                                </li>
                                <div class="dropdown-divider"></div>
                            </ul>
                        </div>
                    </li>
                    <li class="has-submenu">
                        <div class="dropdown">
                            <a class=" dropdown-toggle admin_submenu"  data-toggle="dropdown"> <i class="ti ti-files mx-3"></i> Declarations
                            </a>
                            <ul class="dropdown-menu">

                                <div class="dropdown-divider"></div>
                                <li>
                                    <a href="/declaration/im7">  IM7 </a>
                                </li>
                                <div class="dropdown-divider"></div>
                                <li>
                                    <a href="/declaration/im4">  IM4 </a>
                                </li>
                                <div class="dropdown-divider"></div>
                                <li>
                                    <a href="/declaration/im8">  IM8 </a>
                                </li>
                                <div class="dropdown-divider"></div>

                                {{--<li >--}}
                                {{--<a href="/declaration/type">   Cargo Type </a>--}}
                                {{--</li>--}}
                            </ul>
                        </div>
                    </li>
                    <li class="has-submenu">
                        <a href="/conversation"><i class="fa fa-comment mx-3"></i>Messages</a>
                    </li>

                    <li class="has-submenu">
                        <div class="dropdown">
                            <a class=" dropdown-toggle admin_submenu"  data-toggle="dropdown"> <i class="fa fa-file"></i> Reports
                            </a>
                            <ul class="dropdown-menu">
                                <div class="dropdown-divider"></div>
                                <li>
                                    <a href="/daily/report/not/crossed"> Daily   Report </a>
                                </li>
                                <div class="dropdown-divider"></div>
                                <li>
                                    <a href="/new/consignment">Daily New Consignment </a>
                                </li>
                                <div class="dropdown-divider"></div>
                                <li>
                                    <a href="/reports">General Report</a>
                                </li>
                                <div class="dropdown-divider"></div>
                            </ul>
                        </div>
                    </li>
                    <li class="list-inline-item dropdown notification-list has-submenu pull-right">

                        <a class="nav-link dropdown-toggle arrow-none waves-effect nav-user" data-toggle="dropdown" href="#" role="button"
                           aria-haspopup="false" aria-expanded="false">
                            <img data-name="{{Auth::user()->name}}" class="rounded-circle img-circle profile" alt="..."style="width:  45px;height: 45px; margin-top: -11px;">
                        </a>
                        <div class="dropdown-menu dropdown-menu-right profile-dropdown ">

                            <a class="dropdown-item" href="#">Hi,  {{Auth::user()->name}}</a>
                            {{--<a class="dropdown-item" href="#"><i class="dripicons-user text-muted"></i> Profile</a>--}}
                            <a class="dropdown-item" href="/change_password">  Password</a>
                            <a class="dropdown-item" href="{{ url('/logout') }}"onclick="event.preventDefault();document.getElementById('logout-form').submit();" class="mega-menu" data-close="true">
                                <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                </form>
                                <i class="dripicons-exit text-muted"></i> Logout</a>
                        </div>
                    </li>

                </ul>
            </div>
        </div>
    </div>
</header>