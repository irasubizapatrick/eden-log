
<!-- jQuery  -->
<script src="/assets/js/jquery.min.js"></script>
{{--<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>--}}
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="/assets/js/bootstrap.min.js"></script>
<script src="/assets/js/slim.min.js"></script>



<script src="/assets/js/popper.min.js"></script>
<script src="/assets/js/modernizr.min.js"></script>
<script src="/assets/js/waves.js"></script>
<script src="/assets/js/jquery.slimscroll.js"></script>
<script src="/assets/js/jquery.nicescroll.js"></script>
<script src="/assets/js/jquery.scrollTo.min.js"></script>
<!-- Required datatable js -->
<script src="/assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="/assets/plugins/datatables/dataTables.bootstrap4.min.js"></script>
<!-- Buttons examples -->
<script src="/assets/plugins/datatables/dataTables.buttons.min.js"></script>
{{--<script src="http://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.js"></script>--}}
<script src="/assets/plugins/datatables/buttons.bootstrap4.min.js"></script>
<script src="/assets/plugins/datatables/jszip.min.js"></script>
<script src="/assets/plugins/datatables/pdfmake.min.js"></script>
<script src="/assets/plugins/datatables/vfs_fonts.js"></script>
<script src="/assets/plugins/datatables/buttons.html5.min.js"></script>
<script src="/assets/plugins/datatables/buttons.print.min.js"></script>
<script src="/assets/plugins/datatables/buttons.colVis.min.js"></script>
<!-- Responsive examples -->

{{--<script src="/assets/plugins/datatables/dataTables.responsive.min.js"></script>--}}
{{--<script src="/assets/plugins/datatables/responsive.bootstrap4.min.js"></script>--}}
<script src="/assets/js/select2-data.js"></script>
{{--<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.bundle.min.js"></script>--}}

<!-- Datatable init js -->
<script src="/assets/js/bootstrap.bundle.min.js"></script>
<script src="/assets/pages/datatables.init.js"></script>

<!-- App js -->
<script src="/assets/js/initial.js-master/initial.js" type="text/javascript"></script>
<script>
    $('.profile').initial();
</script>


<script type="text/javascript">
    $(document).ready(function () {
        $('.agent').select2();
        $('.consignee').select2();
        $('#ubwato').select2();
        $('#vessel').select2();
        $('#containerList').select2();
        $('#driver_ladding_id').select2();
        $('#search_ladding_id').select2();
        $('#search_container').select2();
        $('#driver_name').select2();
        $('#driver_license').select2();
        $('#driver_passport').select2();

        $('#bl').select2();
        $('#consignee').select2();
        $('.country').select2();




        $('.edit_ladding_id').select2();
        $('#transporter').select2();
        $('#ladding_id').select2();
        $('#container_id').select2();
        $('.edit_ladding_id').select2();
        $('.expenses').select2();
        $('#search').select2();
        $('#archive_search').select2();
        $('#edit_ladding_id').select2();
        $('#edit_ladding_id1').select2();
        $('#edit_ladding_id2').select2();
        $('#edit_ladding_id3').select2();
        $('#edit_ladding_id4').select2();
        $('#edit_ladding_id5').select2();
        $('#search_ladding_id').select2();
        $('.destination').select2();


        $('#search_truck').select2();



        $('#destination').select2();

        //Initialize tooltips
        $('.nav-tabs > li a[title]').tooltip();

        //Wizard
        $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {

            var $target = $(e.target);

            if ($target.parent().hasClass('disabled')) {
                return false;
            }
        });

        $(".next-step").click(function (e) {

            var $active = $('.wizard .nav-tabs li.active');
            $active.next().removeClass('disabled');
            nextTab($active);

        });
        $(".prev-step").click(function (e) {

            var $active = $('.wizard .nav-tabs li.active');
            prevTab($active);

        });


    });

    function nextTab(elem) {
        $(elem).next().find('a[data-toggle="tab"]').click();
    }
    function prevTab(elem) {
        $(elem).prev().find('a[data-toggle="tab"]').click();
    }
</script>
{{--<script type="text/javascript">--}}
    {{--$(document).ready(function(){--}}
        {{--// Add Container as variables--}}
        {{--var next = 1;--}}
        {{--$(".add-more").click(function(e){--}}
            {{--e.preventDefault();--}}
            {{--var addto = "#field" + next;--}}
            {{--var addRemove = "#field" + (next);--}}
            {{--next = next + 1;--}}
            {{--var newIn = '<input  autocomplete="off" class="input form-control" id="field' + next + '" name="cont_no[]"  placeholder="Enter Cont no" type="text" data-items="8"></form>';--}}
            {{--var newInput = $(newIn);--}}
            {{--var removeBtn = '<button id="remove' + (next - 1) + '" class="btn btn-danger  btn-lg  " style="width: 4rem;">-</button></div><div id="field">';--}}
            {{--var removeButton = $(removeBtn);--}}
            {{--$(addto).after(newInput);--}}
            {{--$(addRemove).after(removeButton);--}}
            {{--$("#field" + next).attr('data-source',$(addto).attr('data-source'));--}}
            {{--$("#count").val(next);--}}

            {{--$('.remove-me').click(function(e){--}}
                {{--e.preventDefault();--}}
                {{--var fieldNum = this.id.charAt(this.id.length-1);--}}
                {{--var fieldID = "#field" + fieldNum;--}}
                {{--$(this).remove();--}}
                {{--$(fieldID).remove();--}}
            {{--});--}}
        {{--});--}}

        {{--// Add driver name--}}

    {{--});--}}
{{--</script>--}}


<script type="text/javascript">
    $(document).ready(function(){
        var maxField = 10; //Input fields increment limitation
        var addButton = $('.add_button'); //Add button selector
        var wrapper = $('.field_wrapper'); //Input field wrapper
        var fieldHTML = '<div><input type="text" name="cont_no[]" class="form-control" placeholder="Enter no"  value=""/><a href="javascript:void(0);" class="remove_button btn btn-danger mt-4 btn-lg" ><i class="fa fa-minus"/></a></div>'; //New input field html
        var x = 1; //Initial field counter is 1

        //Once add button is clicked
        $(addButton).click(function(){
            //Check maximum number of input fields
            if(x < maxField){
                x++; //Increment field counter
                $(wrapper).append(fieldHTML); //Add field html
            }
        });

        //Once remove button is clicked
        $(wrapper).on('click', '.remove_button', function(e){
            e.preventDefault();
            $(this).parent('div').remove(); //Remove field html
            x--; //Decrement field counter
        });


    });
    $(document).ready(function() {
        $('#example').DataTable( {

            dom: 'Bfrtip',
            buttons: [
                {
                    extend: 'colvis',
                    postfixButtons: [ 'colvisRestore' ]
                }
            ],
            columnDefs: [
                {
                    targets: -1,
                    visible: true
                }
            ]
        } );
    } );

</script>

<script type="text/javascript">
    $(document).ready(function()
    {
        $('.multi-field-wrapper').each(function() {
            var $wrapper = $('.multi-fields', this);
            $(".add-field", $(this)).click(function(e) {
                $('.multi-field:first-child', $wrapper).clone(true).appendTo($wrapper).find('input').focus();
            });
            $('.multi-field .remove-field', $wrapper).click(function() {
                if ($('.multi-field', $wrapper).length > 1)
                    $(this).parent('.multi-field').remove();
            });
        });
    });
    $(document).ready(function()
    {
        $('.multi-field-wrapper2').each(function() {
            var $wrapper = $('.multi-fields2', this);
            $(".add-field2", $(this)).click(function(e) {
                $('.multi-field2:first-child', $wrapper).clone(true).appendTo($wrapper).find('input').focus();
            });
            $('.multi-field2 .remove-field2', $wrapper).click(function() {
                if ($('.multi-field2', $wrapper).length > 1)
                    $(this).parent('.multi-field2').remove();
            });
        });
    });
</script>
