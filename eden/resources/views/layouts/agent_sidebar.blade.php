<header id="topnav">
    <!-- end topbar-main -->

    <!-- MENU Start -->
    <div class="navbar-custom">
        <div class="container-fluid">
            <div id="navigation">
                <ul class="navigation-menu">

                    <li class="has-submenu">
                        <a href="/dashboard"><i class="ti-home"></i>Dashboard</a>
                    </li>
                    <li class="has-submenu">
                        <a href="/add_orders"><i class="ti ti-package"></i>Add Orders</a>
                    </li>
                    <li class="has-submenu">
                        <a href="/my_orders"><i class="ti ti-package"></i>All Consignments</a>
                    </li>

                    <li class="has-submenu">
                        <div class="dropdown">
                            <a class=" dropdown-toggle admin_submenu"  data-toggle="dropdown"> <i class="ti ti-truck"></i> Manage Cargo
                            </a>
                            <ul class="dropdown-menu">
                                <div class="dropdown-divider"></div>
                                <li>
                                    <a href="/tracking">Cargo Tracking</a>
                                </li>
                                <div class="dropdown-divider"></div>
                                <li>
                                    <a href="/add_status">Cargo Updates</a>
                                </li>
                                <div class="dropdown-divider"></div>
                            </ul>
                        </div>
                    </li>
                    <li class="has-submenu">
                        <div class="dropdown">
                            <a class=" dropdown-toggle admin_submenu"  data-toggle="dropdown"> <i class="ti ti-files mx-3"></i> Declarations
                            </a>
                            <ul class="dropdown-menu">

                                <div class="dropdown-divider"></div>
                                <li>
                                    <a href="/declaration/im7">  IM7 </a>
                                </li>
                                <div class="dropdown-divider"></div>
                                <li>
                                    <a href="/declaration/im4">  IM4 </a>
                                </li>
                                <div class="dropdown-divider"></div>
                                <li>
                                    <a href="/declaration/im8">  IM8 </a>
                                </li>
                                <div class="dropdown-divider"></div>

                                {{--<li >--}}
                                {{--<a href="/declaration/type">   Cargo Type </a>--}}
                                {{--</li>--}}
                            </ul>
                        </div>
                    </li>


                    <li class="has-submenu">
                        <a href="/conversation"><i class="fa fa-comment"></i>Messages</a>
                    </li>
                    <li class="has-submenu">
                        <a href="/reports"><i class="fa fa-file"></i> Reports</a>
                    </li>

                    <li class="list-inline-item dropdown notification-list has-submenu pull-right" >

                        <a class="nav-link dropdown-toggle arrow-none waves-effect nav-user" data-toggle="dropdown" href="#" role="button"
                           aria-haspopup="false" aria-expanded="false">
                            @foreach($profile as $value)

                                @if($value->agent_logo !='')
                                    <img src="/agent/{{$value->agent_logo}}" class="rounded-circle img-circle " alt="..." style="width:  45px;height: 45px; margin-top: -11px;">
                                @else
                                    <img data-name="{{Auth::user()->name}}" class="rounded-circle img-circle profile" alt="..."style="width:  45px;height: 45px; margin-top: -11px;">
                                @endif
                            @endforeach
                        </a>
                        <div class="dropdown-menu dropdown-menu-right profile-dropdown " style="position: absolute;transform: translate3d(-75px, 74px, 0px);top: -32px !important;left: -31px !important;will-change: transform;padding: 12px 0px;">

                            <a class="dropdown-item" href="#">Hi,  {{Auth::user()->name}}</a>
                            <a class="dropdown-item" href="/change_password"> <i class="dripicons-gear"></i> Password</a>
                            <a class="dropdown-item" href="{{ url('/logout') }}"onclick="event.preventDefault();document.getElementById('logout-form').submit();" class="mega-menu" data-close="true">
                                <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                </form>
                                <i class="dripicons-exit text-muted"></i> Logout</a>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</header>