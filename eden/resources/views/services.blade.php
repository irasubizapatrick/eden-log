

<!DOCTYPE html>
<html lang="en">
@include('layouts.page_head')
<body>


@include('layouts.page_header')

<section class="banner_area">
    <div class="container">
        <div class="pull-left">
            <h3>Our Services</h3>
        </div>
        <div class="pull-right">
            <a href="/">Home</a>
            <a href="/our_service">Services</a>
        </div>
    </div>
</section>


<section class="service_area">
    <div class="container">
        <div class="row service_list_inner">
            <div class="col-md-4">
                <div class="left_s_list">
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="active"><a href="#">Ground Shipping</a></li>
                        <li><a href="#">Large Projects</a></li>
                        <li><a href="#">International Air Freight</a></li>
                        <li><a href="#">Contract Logistics</a></li>
                        <li><a href="#">Warehousing</a></li>
                        <li><a href="#">Consulting</a></li>
                        <li><a href="#">Warehousing</a></li>
                        <li><a href="#">Consulting</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-8">
                <div class="row service_list_item_inner">
                    <div class="col-md-6 col-xs-6">
                        <div class="image_s_list">
                            <img src="/landing/img/services_thumb1.jpg" alt="">
                            <h3 class="single_title">Ground Shipping</h3>
                            <p>We have a wide experience in overland industry specific logistic solutions like pharmaceutical logistics, retail and automotive logistics by train or road.</p>
                            <a class="more_btn" href="#">Read MORE <i class="fa fa-angle-right"></i></a>
                        </div>
                    </div>
                    <div class="col-md-6 col-xs-6">
                        <div class="image_s_list">
                            <img src="/landing/img/services_thumb2.jpg" alt="">
                            <h3 class="single_title">Large Projects</h3>
                            <p>We bring your goods safely to worldwide destinations with our great sea fright services. We offer LLC and FLC shipments that are fast and effective with no delays.</p>
                            <a class="more_btn" href="#">Read MORE <i class="fa fa-angle-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<section class="question_contact_area">
    <div class="container">
        <div class="pull-left">
            <h4>Have you question or need any help for work consultant</h4>
        </div>
        <div class="pull-right">
            <a class="more_btn" href="/contact_us">Contact Us</a>
        </div>
    </div>
</section>
@include('layouts.web_footer')

@include('layouts.page_footer')
</body>
</html>
<!-- Localized -->