<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();


});


/**
 *
 * Api Route
 */



Route::group(['middleware' => 'api','prefix' => 'auth'], function () {

    Route::post('/login', 'AuthController@login');
    Route::post('/logout', 'AuthController@logout');
    Route::post('/refresh', 'AuthController@refresh');


});


Route::group(['middleware' => 'jwt.auth'], function () {



      /*
       |--------------------------------------------------------------------------
       | User Profile Api   Controller
       |--------------------------------------------------------------------------
      */
            Route::get('/user-profile','DashboardController@GetUser')->name('user-profile');

       /*
       |--------------------------------------------------------------------------
       | All Orders   Api   Controller
       |--------------------------------------------------------------------------
      */

            Route::get('/all/orders','OrderController@ApiOder')->name('all/orders');

        /*
       |--------------------------------------------------------------------------
       | Track Get Data  Api Controller
       |--------------------------------------------------------------------------
      */

            Route::get('/track/all/data','OrderController@trackingAPi')->name('track/all/data');


        /*
         |--------------------------------------------------------------------------
         | Search   Api Controller using BL
         |--------------------------------------------------------------------------
        */

            Route::post('/truck/order','OrderController@find_truck_Api')->name('truck/order');

        /*
        |--------------------------------------------------------------------------
        | Update Order  Controller using
        |--------------------------------------------------------------------------
       */

            Route::post('/update/status','OrderController@save_add_status_api')->name('update/status');


});



