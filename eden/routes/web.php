<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
         return view('auth.login');
//   return view('welcome');
    });
    /*
    |--------------------------------------------------------------------------
    | Start of CMS  Controller
    |--------------------------------------------------------------------------
    */

    Route::get('/about_us','CmsController@about_us')->name('about_us');

    Route::get('/our_service','CmsController@our_service')->name('our_service');

    Route::any('/tracking_truck','CmsController@tracking')->name('tracking_truck');

    Route::any('/cargo/tracking','CmsController@track_page')->name('cargo/tracking');

    Route::any('/cargo/tracking/results','CmsController@track_page_results')->name('/cargo/tracking/results');

    Route::get('/blog','CmsController@blog')->name('blog');

    Route::get('/contact_us','CmsController@contact_us')->name('contact_us');

    Route::any('/tracking_cargo','CmsController@tracking_cargo')->name('tracking_cargo');

    /*
    |--------------------------------------------------------------------------
    | End of CMS  Controller
    |--------------------------------------------------------------------------
    */
    Route::get('/tracking_form','OrderController@tracking_form')->name('tracking_form');

//    Route::post('/find_truck_form','OrderController@find_truck_form')->name('find_truck_form');

    Route::get('/dashboard', ['middleware' => 'please_login',  function (){
        return view('auth.login');
    }]);

    Route::get('logout ',function (){
        Auth::logout();
        return redirect('/login');
    });

    Auth::routes();

    Route::group(['middleware'=> 'auth'], function() {

        Route::get('/dashboard', 'DashboardController@index');

        Route::get('/change_password','DashboardController@change_pass')->name('change_password');

        Route::any('/update_password/{id}','DashboardController@update_password')->name('update_password');

        Route::get('/all/consignment/cross','DashboardController@CrossedBorder')->name('all/consignment/cross');

        Route::get('/track/t1','DashboardController@track_t1')->name('track/t1');

        Route::get('/track/loading','DashboardController@track_loading')->name('track/loading');

        Route::get('track/cargo','DashboardController@track_cargo')->name('track/cargo');
        /*
        |--------------------------------------------------------------------------
        | Agent Controller
        |--------------------------------------------------------------------------
        */

        Route::resource('/all_agent','AgentController');

        /*
        |--------------------------------------------------------------------------
        | Order   Controller
        |--------------------------------------------------------------------------
        */
        Route::resource('/my_orders','OrderController');

        Route::post('/admin_add_order','OrderController@admin_add_order')->name('admin_add_order');

        Route::get('/all_order','OrderController@all_order')->name('all_order');

        Route::get('/add_orders','OrderController@add_orders')->name('add_orders');

        Route::get('country_name/ajax/{id}',array('as'=>'count.ajax','uses'=>'OrderController@FindDestination'));

        Route::get('/order_details/{id}','OrderController@order_details')->name('order_details');

        Route::post('/all_order/search/border','OrderController@AllSearchOrder')->name('/all_order/search/border');

        Route::get('/view/all/container/{id}','OrderController@ListContainer')->name('/view/all/container');

//        Route::put('/update_add_status/{id}','OrderController@update_add_status')->name('update_add_status');
//
//        Route::any('/delete_add_status/{id)','OrderController@delete_add_status')->name('delete_add_status');

        /*
        |--------------------------------------------------------------------------
        | Order Status  Controller
        |--------------------------------------------------------------------------
        */
        Route::get('/order_status/{id}','OrderController@order_status')->name('order_status');

        Route::get('/add_status',array('as'=>'add_status','uses'=>'OrderController@admin_add_status'))->name('add_status');

        Route::get('add_status/ajax/{id}',array('as'=>'add_status.ajax','uses'=>'OrderController@admin_add_statusAjax'));


        Route::get('find/status/ajax/{id}',array('as'=>'add_status.ajax','uses'=>'OrderController@findStatus'));

        Route::get('add_status/delivery/{id}',array('as'=>'add_status.ajax','uses'=>'OrderController@admin_add_DeliveryAjax'));


        Route::get('add_drop/ajax/{id}',array('as'=>'add_drop.ajax','uses'=>'OrderController@admin_drop_statusAjax'));


        Route::post('/save_add_status','OrderController@save_add_status')->name('save_add_status');

        Route::put('admin_update_order/{id}','OrderController@admin_update_order')->name('admin_update_order');

        Route::get('edit/order/{id}','OrderController@admin_edit_order')->name('edit/order');

        Route::any('delete/container/{id}','OrderController@DeleteContainer')->name('delete/container');




        /*
        |--------------------------------------------------------------------------
        |Tracking   Controller
        |--------------------------------------------------------------------------
        */

        Route::any('/find_truck','OrderController@find_truck')->name('find_truck');

        Route::get('/tracking',array('as'=>'tracking','uses'=>'OrderController@tracking'))->name('tracking');

        Route::get('tracking/ajax/{id}',array('as'=>'tracking.ajax','uses'=>'OrderController@trackingAjax'));

        Route::put('/updates/status/{id}','OrderController@updateStatus')->name('updates/status');

        Route::any('/delete/status/{id}','OrderController@deleteStatus')->name('deleteStatus');

        Route::post('/now','OrderController@now')->name('now');


        /*
        |--------------------------------------------------------------------------
        | Driver  Controller
        |--------------------------------------------------------------------------
        */
        Route::resource('/all_driver','DriverController');

        Route::any('/search/bill/driver','DriverController@searchBlDriver')->name('search/bill/driver');


        /*
        |--------------------------------------------------------------------------
        | Transporter   Controller
        |--------------------------------------------------------------------------
        */

        Route::resource('/all_trans','TransporterController');

        Route::any('/search/bill/trans','TransporterController@searchBl')->name('search/bill/trans');

        /*
        |--------------------------------------------------------------------------
        | Drop-0ff  Controller
        |--------------------------------------------------------------------------
        */

        Route::resource('/all_drop','DropController');

        Route::get('/cont_no/{id?}','DropController@DropDetails');

        Route::any('/search/bill/drop','DropController@searchBlDrop')->name('search/bill/drop');

        Route::get('/pending/container','DropController@PendingContainer')->name('pending/container');

        Route::any('/search/container/pending','DropController@SearchPendingC')->name('search/container/pending');

        /*
        |--------------------------------------------------------------------------
        | Archive   Controller
        |--------------------------------------------------------------------------
        */
        Route::resource('/archive_doc','ArchiveController');

        /*
        |--------------------------------------------------------------------------
        | Staff   Controller
        |--------------------------------------------------------------------------
        */
        Route::resource('/staff','SalesController');

        /*
        |--------------------------------------------------------------------------
        | Clearance Documents  Controller
        |--------------------------------------------------------------------------
        */
        Route::resource('/clearance','ClearanceController');

        /*
        |--------------------------------------------------------------------------
        | Payment Documents - Invoice -  Controller
        |--------------------------------------------------------------------------
        */
        Route::resource('/payments_app','PaymentsController');


        /*
        |--------------------------------------------------------------------------
        | Reports  Controller
        |--------------------------------------------------------------------------
        */

        Route::resource('reports','ReportsController');

        Route::get('/agent_reports/{id}','ReportsController@agent_view_order')->name('agent_reports');

        Route::post('/sortOrders', 'ReportsController@sortOrders');

        Route::post('/sortDestination','ReportsController@sortDestination');

        Route::post('/Report/Consignee','ReportsController@sortConsignee')->name('Report/Consignee');

        Route::post('/Report/Transporter','ReportsController@ReportTransporter')->name('Report/Transporter');

        Route::post('/sort/Vessel','ReportsController@sortVessel')->name('sort/Vessel');

        Route::post('/sort/Client','ReportsController@sortClient')->name('sort/Client');

        Route::post('/sort/Driver','ReportsController@sortDriver')->name('sort/Driver');

        Route::get('/daily/report/not/crossed','ReportsController@DailyReportNotCrossed')->name('daily/report/not/crossed');

        Route::any('/daily-report-updated-not-crossed','ReportsController@SearchDaily')->name('daily-report-updated-not-crossed');

        Route::get('/new/consignment','ReportsController@NewConsignment')->name('/new/consignment');



        /*
        |--------------------------------------------------------------------------
        | Login Activity   Controller
        |--------------------------------------------------------------------------
        */

        Route::get('/login-activity', 'LoginActivityController@index')->middleware('auth');

        /*
        |--------------------------------------------------------------------------
        | Message  Controller
        |--------------------------------------------------------------------------
        */

        Route::resource('/conversation','MessageController');
        Route::post('/reply','MessageController@reply')->name('reply');
        Route::any('/ViewConversation/{id}','MessageController@ViewConversation')->name('ViewConversation');

        /*
        |--------------------------------------------------------------------------
        | Delivery   Controller
        |--------------------------------------------------------------------------
        */
        Route::resource('/delivery/note','DeliveryController');

        Route::get('/view/delivery/{id}','DeliveryController@ViewDelivery')->name('view/delivery');

        Route::get('/trans/{id?}','DeliveryController@ContainerDetails');

        Route::any('/search/delivery/note','DeliveryController@searchDeliveryN')->name('search/delivery/note');

        Route::get('/cont_number/{id?}','DeliveryController@GetMoreDetails');
        /*
        |--------------------------------------------------------------------------
        | Declaration type   Controller
        |--------------------------------------------------------------------------
      */
        Route::resource('/declaration/type','DeclarationTypeController');

        /*
        |--------------------------------------------------------------------------
        | Bill of lading and declaration  type   Controller
        |--------------------------------------------------------------------------
      */

        Route::resource('/declaration/bl','BlDeclarationController');

        Route::resource('declaration/im4','Im4Controller');

        Route::any('/search/bill/details/im4','Im4Controller@searchBlIM4')->name('search/bill/details/im4');

        Route::resource('declaration/im7','Im7Controller');

        Route::any('/search/bill/details/im7','Im7Controller@searchBlIM7')->name('search/bill/details/im7');

        Route::resource('declaration/im8','Im8Controller');

        Route::any('/search/bill/details/im8','Im8Controller@searchBlIM8')->name('search/bill/details/im8');

        /*
        |--------------------------------------------------------------------------
        | Expenses   Controller
        |--------------------------------------------------------------------------
        */

        Route::resource('/expenses','VoucherController');

        Route::get('/requisition_form','VoucherController@RequestAmount')->name('requisition_form');

        Route::get('/view/request/{id}','VoucherController@ViewRequest')->name('view/request');

        Route::any('/expenses/form','VoucherController@ExpensesForm')->name('expenses/form');

        Route::any('/general/expenses','VoucherController@ExpensesSort')->name('general/expenses');

        Route::any('/search/consignment','VoucherController@ExpenseConsign')->name('search/consignment');




        /*
        |--------------------------------------------------------------------------
        | Normal Expenses   Controller
        |--------------------------------------------------------------------------
        */
        Route::resource('/normal/expenses','NormalExpensesController');

        Route::get('/requisition/form/normal','NormalExpensesController@RequestAmountNormal')->name('requisition/form/normal');

        Route::post('/expenses/form/normal','NormalExpensesController@ExpensesFormNormal')->name('expenses/form/normal');

        Route::get('/view/request/normal/{id}','NormalExpensesController@ViewRequestNormal')->name('view/request/normal');

        Route::post('/back/expenses','NormalExpensesController@ExpensesSortback')->name('back/expenses');



        /*
        |--------------------------------------------------------------------------
        | Destination  Controller
        |--------------------------------------------------------------------------
        */

        Route::resource('/manage/destination/country','CountryController');

        Route::resource('/manage/destination','DestinationController');


    });
