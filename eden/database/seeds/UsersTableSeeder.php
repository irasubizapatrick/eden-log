<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();
        User::create(['name' =>'Admin','email' => 'edenlogistics@gmail.com' , 'password' => Hash::make('password')]);

    }
}
