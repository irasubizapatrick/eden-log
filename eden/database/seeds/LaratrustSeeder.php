<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class LaratrustSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return  void
     */
    public function run()
    {
        $this->command->info('Truncating User, Role and Permission tables');
        $this->truncateLaratrustTables();

        $config = config('laratrust_seeder.role_structure');
        $userPermission = config('laratrust_seeder.permission_structure');
        $mapPermission = collect(config('laratrust_seeder.permissions_map'));

        $Adminrole = \App\Role::create([
            'name' => 'admin',
            'display_name' => 'Mikumi',
            'description' => 'Admin'
        ]);

        $Managerrole = \App\Role::create([
            'name' => 'agent',
            'display_name' => 'Agent',
            'description' => 'Agent'
        ]);

        $Employeerole = \App\Role::create([
            'name' => 'sales',
            'display_name' => 'Sales',
            'description' => 'Sales'
        ]);


        $userAdmin = \App\User::create([
            'name' => 'admin',
            'email' => 'admin@log.com',
            'password' => bcrypt('password')
        ]);

        $userAdmin->attachRole($Adminrole);


        $userManager = \App\User::create([
            'name' => 'agent',
            'email' => 'agent@log.com',
            'password' => bcrypt('password')
        ]);
        $userManager->attachRole($Managerrole);


        $userEmployee = \App\User::create([
            'name' => 'sales',
            'email' => 'sales@log.com',
            'password' => bcrypt('password')
        ]);
        $userEmployee->attachRole($Employeerole);

    }

    /**
     * Truncates all the laratrust tables and the users table
     *
     * @return    void
     */
    public function truncateLaratrustTables()
    {
        Schema::disableForeignKeyConstraints();
        DB::table('permission_role')->truncate();
        DB::table('permission_user')->truncate();
        DB::table('role_user')->truncate();
        \App\User::truncate();
        \App\Role::truncate();
        \App\Permission::truncate();
        Schema::enableForeignKeyConstraints();
    }
}
