<?php

use Illuminate\Database\Seeder;


class NewUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $Superrole = \App\Role::create([
            'name' => 'super',
            'display_name' => 'Super',
            'description' => 'Super'
        ]);


        $Documentrole = \App\Role::create([
            'name' => 'documentation',
            'display_name' => 'Documentation',
            'description' => 'Documentation'
        ]);


        $Shippingrole = \App\Role::create([
            'name' => 'shipping',
            'display_name' => 'Shipping',
            'description' => 'Shipping'
        ]);

        $Finance = \App\Role::create([
            'name' => 'finance',
            'display_name' => 'Finance',
            'description' => 'Finance'
        ]);

        $userAdmin = \App\User::create([
            'name' => 'super',
            'email' => 'super@mikumifreight.com',
            'password' => bcrypt('password')
        ]);

        $userAdmin->attachRole($Superrole);


    }

}
