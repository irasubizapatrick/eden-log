<?php

use App\Role;
use Illuminate\Database\Seeder;

class DeclarationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $Declaration = Role::create([
            'name' => 'declaration',
            'display_name' => 'Declaration',
            'description' => 'Declaration'
        ]);
        $Newrole = Role::create([
            'name' => 'client',
            'display_name' => 'Client',
            'description' => 'Client'
        ]);

        $Companyrole = Role::create([
            'name' => 'company',
            'display_name' => 'Company',
            'description' => 'Company'
        ]);

    }
}
