<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCurrencyToTracksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tracks', function (Blueprint $table) {
            $table->string('guart_amount_taken')->nullable();
            $table->string('guart_amount_returned')->nullable();
            $table->string('rwa01')->nullable();
            $table->string('tzdl')->nullable();
            $table->string('t1_reference')->nullable();
            $table->string('document_reference')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tracks', function (Blueprint $table) {
            $table->dropColumn('guart_amount_taken');
            $table->dropColumn('guart_amount_returned');
            $table->dropColumn('rwa01');
            $table->dropColumn('tzdl');
            $table->dropColumn('t1_reference');
            $table->dropColumn('document_reference');
        });
    }
}

