<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddContainerNoToClearancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('clearances', function (Blueprint $table) {
            $table->integer('container_id')->unsigned()->index();
            $table->foreign('container_id')->references('id')->on('containers')->onDelete('cascade');
            $table->string('other_clearance_doc')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('clearances', function (Blueprint $table) {
            $table->dropColumn('container_id');
            $table->dropColumn('other_clearance_doc');
        });
    }
}
