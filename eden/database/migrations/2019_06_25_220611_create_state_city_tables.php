<?php


use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
class CreateStateCityTables extends Migration
{
    public function up()
    {
        Schema::create('demo_state', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->timestamps();
        });


        Schema::create('demo_cities', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('state_id')->unsigned()->index();
            $table->foreign('state_id')->references('id')->on('demo_state')->onDelete('cascade');
            $table->string('capital');
            $table->timestamps();
        });
    }
    public function down()
    {
        Schema::drop('demo_cities');
        Schema::drop('demo_state');
    }
}