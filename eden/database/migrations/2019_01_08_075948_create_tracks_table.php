<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTracksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tracks', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->index();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->integer('agent_id')->unsigned()->index();
            $table->foreign('agent_id')->references('id')->on('agents')->onDelete('cascade');
            $table->string('client_name')->default('0');
            $table->string('file_no');
            $table->string('date')->nullable();
            $table->string('type')->default(0);
            $table->string('weight')->default(0);
            $table->string('frequency')->default(0);
            $table->string('file_opened_by')->default('0');
            $table->string('line_vessel')->default(0);
            $table->string('eta')->nullable();
            $table->string('document_date_received')->nullable();
            $table->string('destination')->default(0);
            $table->string('terminal')->default(0);
            $table->string('commodity')->default(0);
            $table->string('port_charge')->default(0);
            $table->string('remarks')->default(0);
            $table->string('special_instruction')->default('0');
            $table->string('voyager_number')->default('0');
            $table->string('shipper')->default(0);
            $table->string('shipping_line')->default('0');
            $table->string('ata')->nullable();
            $table->string('invoice_order')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tracks');
    }
}
