<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDmcReferenceToBlDeclarationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bl_declarations', function (Blueprint $table) {
            $table->string('whz_reference')->nullable();
            $table->string('t1_im7_reference')->nullable();
            $table->string('c2_im7_reference')->nullable();
            $table->string('dmc_im7_reference')->nullable();
            $table->string('exist_note_reference')->nullable();
            $table->string('release_reference')->nullable();
            $table->string('c2_im4_reference')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bl_declarations', function (Blueprint $table) {
            $table->dropColumn('whz_reference');
            $table->dropColumn('t1_im7_reference');
            $table->dropColumn('c2_im7_reference');
            $table->dropColumn('dmc_im7_reference');
            $table->dropColumn('exist_note_reference');
            $table->dropColumn('release_reference');
            $table->dropColumn('c2_im4_reference');
        });
    }
}
