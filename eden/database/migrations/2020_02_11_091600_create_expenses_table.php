<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExpensesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('expenses', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->index();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->integer('staff_id')->unsigned()->index();
            $table->foreign('staff_id')->references('id')->on('users')->onDelete('cascade');
            $table->integer('container_id')->unsigned()->index();
            $table->foreign('container_id')->references('id')->on('containers')->onDelete('cascade');
            $table->string('expenses_date')->nullable();
            $table->string('amount_requested')->nullable();
            $table->string('amount_words')->nullable();
            $table->string('amount_currency')->nullable();
            $table->string('payee')->nullable();
            $table->string('more_info')->nullable();
            $table->string('finance_status')->default('pending');
            $table->string('super_status')->default('pending');
            $table->string('quantity_expenses')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('expenses');
    }
}
