<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTruckingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('truckings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->index();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->integer('ladding_id')->unsigned()->index();
            $table->foreign('ladding_id')->references('id')->on('laddings')->onDelete('cascade');
            $table->integer('cont_id')->unsigned()->index();
            $table->foreign('cont_id')->references('id')->on('containers')->onDelete('cascade');
            $table->string('processing_status')->default('document sent');
            $table->string('processing_date')->nullable();
            $table->string('other_comment')->default('0');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('truckings');
    }
}

