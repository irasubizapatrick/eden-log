<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDelieveriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('delieveries', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->index();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->integer('container_id')->unsigned()->index();
            $table->foreign('container_id')->references('id')->on('containers')->onDelete('cascade');
            $table->integer('driver_id')->unsigned()->index();
            $table->foreign('driver_id')->references('id')->on('drivers')->onDelete('cascade');
            $table->integer('transporter_id')->unsigned()->index();
            $table->foreign('transporter_id')->references('id')->on('transporters')->onDelete('cascade');
            $table->string('recipient_name')->nullable();
            $table->string('recipient_date')->nullable();
            $table->string('packages')->nullable();
            $table->string('container_seal')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('delieveries');
    }
}
