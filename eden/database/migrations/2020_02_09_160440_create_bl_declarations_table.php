<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlDeclarationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bl_declarations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->index();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->integer('ladding_id')->unsigned()->index();
            $table->foreign('ladding_id')->references('id')->on('laddings')->onDelete('cascade');
            $table->integer('declaration_id')->unsigned()->index();
            $table->foreign('declaration_id')->references('id')->on('declaration_types')->onDelete('cascade');
            $table->string('manifest')->nullable();
            $table->string('t1')->nullable();
            $table->string('whz')->nullable();
            $table->string('t1_verified')->nullable();
            $table->string('exist_note')->nullable();
            $table->string('assessment_doc')->nullable();
            $table->string('c2')->nullable();
            $table->string('authorization')->nullable();
            $table->string('movement_sheet')->nullable();
            $table->string('release')->nullable();
            $table->string('declaration_other')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bl_declarations');
    }
}
