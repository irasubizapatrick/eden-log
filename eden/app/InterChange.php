<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class InterChange extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $table = 'inter_changes';
    protected $fillable = ['id', 'user_id', 'container_id', 'interchange_name'];
}


