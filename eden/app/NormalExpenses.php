<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class NormalExpenses extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $table = 'normal_expenses';
    protected $fillable = ['id', 'user_id', 'staff_id','expenses_date',
        'amount_requested','amount_words','amount_currency','payee','more_info','finance_status','super_status','quantity_expenses','reason'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function staff(){
        return $this->belongsTo('App\User','staff_id');
    }

    public function user(){
        return $this->belongsTo('App\User','user_id');
    }


}
