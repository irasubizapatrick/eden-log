<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Track extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $table = 'tracks';
    protected $fillable = ['id', 'file_no','user_id', 'date','type','client_name'
        ,'weight','line_vessel','eta','agent_id','document_date_received','destination','terminal',
        'commodity','port_charge','remarks','invoice_order','frequency','weight',
        'file_opened_by','special_instruction','voyager_number','shipper','ata','shipping_line','track_status','amount','declaration'
        ,'delivery_order','port_charge_invoice','shipper_address','current_status','bl_doc','package_list_doc','border_exist'
        ,'guart_amount_taken','guart_amount_returned','rwa01','tzdl','t1_reference','document_reference','other_comment'];
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     *
     */

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function lad()
    {
        return $this->hasMany('App\Ladding');
    }

    public function agent()
    {
        return $this->belongsTo('App\Agent','agent_id');
    }

    public function container()
    {
        return $this->belongsTo('App\Container');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function transporter()
    {
        return $this->belongsTo('App\Transporter');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function drop()
    {
        return $this->belongsTo('App\Drop');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function inter()
    {
        return $this->belongsTo('App\InterChange');
    }
}

