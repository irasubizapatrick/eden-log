<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Drop extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $table = 'drops';
    protected $fillable = ['id', 'user_id', 'container_id', 'drop_name','trucking_number','transporter_name','dropOff_date','drop_document','outWord'];
}


