<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Ladding extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $table = 'laddings';
    protected $fillable = ['id', 'user_id','bill_lading','track_id'];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function track(){
        return $this->belongsTo('App\Track','track_id');
    }

}


