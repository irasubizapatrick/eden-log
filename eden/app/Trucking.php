<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Trucking extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $table = 'truckings';
    protected $fillable = ['id', 'user_id', 'ladding_id','cont_id','processing_status', 'processing_date','other_comment'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function cont()
    {
        return $this->belongsTo('App\Container','cont_id');
    }
}
