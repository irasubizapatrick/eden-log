<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Container extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $table = 'containers';
    protected $fillable = ['id', 'user_id','ladding_id','cont_no'];

    public function lad(){
        return $this->belongsTo('App\Ladding','ladding_id');
    }

    public function xxx()
    {
        return $this->belongsToMany('Ladding','containers')->withPivot('cont_no');
    }

    public function xxx2()
    {
        return $this->belongsToMany('Ladding','containers')->withPivot('cont_no');
    }


}
