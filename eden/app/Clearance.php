<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Clearance extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $table = 'clearances';
    protected $fillable = ['id', 'user_id','ladding_id','tra','rra','clearing_date','container_id','other_clearance_doc'];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function container(){
        return $this->belongsTo('App\Container','container_id');
    }

}
