<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Agent extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $table = 'agents';
    protected $fillable = ['id', 'user_id', 'company_id','organization_name','contact_person','telephone','country','address'];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\User','user_id');
    }
}

