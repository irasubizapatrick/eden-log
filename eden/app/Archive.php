<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Archive extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $table = 'archives';
    protected $fillable = ['id', 'user_id','ladding_id','invoice_doc','shipping_line_charges','port_charges','customer_payments','other'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function lad()
    {
        return $this->belongsTo('App\Ladding', 'ladding_id');
    }
}
