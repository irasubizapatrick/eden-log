<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MessageStatus extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $table = 'message_statuses';
    protected $fillable = ['id', 'user_id', 'message_id', 'message_responses'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function message()
    {
        return $this->belongsTo('App\Message', 'message_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
}

