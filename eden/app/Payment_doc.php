<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Payment_doc extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $table = 'payment_docs';
    protected $fillable = ['id', 'user_id','ladding_id','invoice_date','invoice_doc','delivery_note'];
}