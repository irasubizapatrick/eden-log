<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Delievery extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $table = 'delieveries';
    protected $fillable = ['id', 'user_id', 'container_id', 'driver_id', 'transporter_id','recipient_name','recipient_date','packages','container_seal'];

    public function container(){
        return $this->belongsTo('App\Container','container_id');
    }

    public function driver(){
        return $this->belongsTo('App\Driver','driver_id');
    }

    public function transporter(){
        return $this->belongsTo('App\Transporter','transporter_id');
    }

}


