<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LoginActivity extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $table = 'login_activities';
    protected $fillable = ['id','user_id','user_agent', 'ip_address'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     *
     */

    public function user()
    {
        return $this->belongsTo('App\User','user_id');
    }
}