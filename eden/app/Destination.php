<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Destination extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $table = 'destinations';
    protected $fillable = ['id', 'destination_name','country_id'];


    public function country(){
        return $this->belongsTo('App\Country','country_id');
    }

}
