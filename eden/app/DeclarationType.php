<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DeclarationType extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $table = 'declaration_types';
    protected $fillable = ['id', 'user_id','cargo_declaration_type'];

    public function user(){
        return $this->belongsTo('App\User','user_id');
    }


}