<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Message extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $table = 'messages';
    protected $fillable = ['id', 'user_id','agent_id','message_title','message_description'];

    public function agent()
    {
        return $this->belongsTo('App\Agent', 'agent_id');
    }

}