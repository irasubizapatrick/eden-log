<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Driver extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $table = 'drivers';
    protected $fillable = ['id', 'user_id', 'container_id', 'driver_name', 'driver_contact','driver_passport','driver_license','driving_copy','passport_copy','movement_sheet'];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function container()
    {
        return $this->belongsTo('App\Container','container_id');
    }
}