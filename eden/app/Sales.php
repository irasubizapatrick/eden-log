<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Sales extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $table = 'sales';
    protected $fillable = ['id','company_id','employee_name', 'user_id', 'employee_phone', 'employee_email'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     *
     */

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
}