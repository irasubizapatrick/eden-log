<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Transporter extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $table = 'transporters';
    protected $fillable = ['id', 'user_id', 'container_id', 'transporter_name','trucking_number','loading_date','truck_horse_copy','truck_trailer_copy'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function container()
    {
        return $this->belongsTo('App\Container','container_id');
    }
}
