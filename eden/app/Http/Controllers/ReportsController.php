<?php

namespace App\Http\Controllers;
use App\Agent;
use App\Container;
use App\Destination;
use App\Driver;
use App\Expenses;
use App\NormalExpenses;
use App\Transporter;
use App\User;
use Carbon\Carbon;
use DB;
use App\Track;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Collection;
use Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
class ReportsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user_id = Auth::id();

        $from = '2021';
        $to = '2030';
        $user   =Auth::user();
        $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
        $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
        $destination = Destination::all();
        $country = DB::table("countries")->pluck("country_name","id");
        if($user->hasRole('admin'))
        {
            $track   = Track::select('tracks.*','bill_lading','organization_name')
                ->join('laddings','laddings.track_id','=','tracks.id')
                ->join('agents','agents.id','=','tracks.agent_id')
                ->whereBetween(DB::raw('YEAR(tracks.created_at)'),[$from, $to])
                ->latest('tracks.created_at')
                ->get();

            $destiny_line = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->line_vessel;
            });
            $destiny_client = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->client_name;
            });
            $destiny_dest = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->destination;
            });
            $agent          = Agent::with('user')->get();
            $transporter = Transporter::select('transporters.*','cont_no','bill_lading')
                ->join('containers','containers.id','=','transporters.container_id')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->whereBetween(DB::raw('YEAR(transporters.created_at)'),[$from, $to])
                ->orderByDesc("created_at")
                ->get()->groupBy(function($item) {
                    return $item->transporter_name;
                });

            $driver = Driver::select('drivers.*','cont_no','bill_lading')
                ->join('containers','containers.id','=','drivers.container_id')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->whereBetween(DB::raw('YEAR(drivers.created_at)'),[$from, $to])
                ->orderByDesc("created_at")
                ->get()->groupBy(function($item) {
                    return $item->driver_name;
                });

            return view('reports.reports', compact('destination','country','expenses','normal','track','destiny_client','destiny_dest','destiny_line','agent','transporter','driver'));
        }
        elseif($user->hasRole('super'))
        {
            $expenses = Expenses::with('container')->where('finance_status','LIKE','pending')->count();
            $normal = NormalExpenses::where('finance_status','LIKE','pending')->count();

            $track   = Track::select('tracks.*','bill_lading','organization_name','loading_date','cont_no','transporter_name','driver_name','trucking_number','driver_license','driver_contact','driver_passport')
                ->join('laddings','laddings.track_id','=','tracks.id')
                ->join('containers','containers.ladding_id','=','laddings.id')
                ->join('agents','agents.id','=','tracks.agent_id')
                ->join('transporters','transporters.container_id','=','containers.id')
                ->join('drivers','drivers.container_id','=','containers.id')
                ->latest('tracks.created_at')
                ->get();

            $destiny_line = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->line_vessel;
            });
            $destiny_client = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->client_name;
            });
            $destiny_dest = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->destination;
            });
            $agent          = Agent::with('user')->get();
            $transporter = Transporter::select('transporters.*','cont_no','bill_lading')
                ->join('containers','containers.id','=','transporters.container_id')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->orderByDesc("created_at")
                ->get()->groupBy(function($item) {
                    return $item->transporter_name;
                });

            $driver = Driver::select('drivers.*','cont_no','bill_lading')
                ->join('containers','containers.id','=','drivers.container_id')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->orderByDesc("created_at")
                ->get()->groupBy(function($item) {
                    return $item->driver_name;
                });

            return view('reports.super_reports', compact('expenses','normal','track','destiny_client','destiny_dest','destiny_line','agent','transporter','driver'));
        }
        elseif($user->hasRole('finance'))
        {
            $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
            $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

            $track   = Track::select('tracks.*','bill_lading','organization_name','cont_no','loading_date','transporter_name','driver_name','trucking_number','driver_license','driver_contact','driver_passport')
                ->join('laddings','laddings.track_id','=','tracks.id')
                ->join('containers','containers.ladding_id','=','laddings.id')
                ->join('agents','agents.id','=','tracks.agent_id')
                ->join('transporters','transporters.container_id','=','containers.id')
                ->join('drivers','drivers.container_id','=','containers.id')
                ->whereBetween(DB::raw('YEAR(tracks.created_at)'),[$from, $to])
                ->latest('tracks.created_at')
                ->get();

            $destiny_line = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->line_vessel;
            });
            $destiny_client = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->client_name;
            });
            $destiny_dest = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->destination;
            });
            $agent          = Agent::with('user')->get();
            $transporter = Transporter::select('transporters.*','cont_no','bill_lading')
                ->join('containers','containers.id','=','transporters.container_id')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->whereBetween(DB::raw('YEAR(transporters.created_at)'),[$from, $to])
                ->orderByDesc("created_at")
                ->get()->groupBy(function($item) {
                    return $item->transporter_name;
                });

            $driver = Driver::select('drivers.*','cont_no','bill_lading')
                ->join('containers','containers.id','=','drivers.container_id')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->whereBetween(DB::raw('YEAR(drivers.created_at)'),[$from, $to])
                ->orderByDesc("created_at")
                ->get()->groupBy(function($item) {
                    return $item->driver_name;
                });

            return view('reports.finance_reports', compact('destination','country','expenses','normal','track','destiny_client','destiny_dest','destiny_line','agent','transporter','driver'));
        }
        elseif($user->hasRole('sales'))
        {
            $track   = Track::select('tracks.*','bill_lading','organization_name','cont_no','loading_date','transporter_name','driver_name','trucking_number','driver_license','driver_contact','driver_passport')
                ->join('laddings','laddings.track_id','=','tracks.id')
                ->join('containers','containers.ladding_id','=','laddings.id')
                ->join('agents','agents.id','=','tracks.agent_id')
                ->join('transporters','transporters.container_id','=','containers.id')
                ->join('drivers','drivers.container_id','=','containers.id')
                ->whereBetween(DB::raw('YEAR(tracks.created_at)'),[$from, $to])
                ->latest('tracks.created_at')
                ->get();


            $destiny_line = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->line_vessel;
            });
            $destiny_client = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->client_name;
            });
            $destiny_dest = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->destination;
            });
            $agent          = Agent::with('user')->get();
            $transporter = Transporter::select('transporters.*','cont_no','bill_lading')
                ->join('containers','containers.id','=','transporters.container_id')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->whereBetween(DB::raw('YEAR(transporters.created_at)'),[$from, $to])
                ->orderByDesc("created_at")
                ->get()->groupBy(function($item) {
                    return $item->transporter_name;
                });

            $driver = Driver::select('drivers.*','cont_no','bill_lading')
                ->join('containers','containers.id','=','drivers.container_id')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->whereBetween(DB::raw('YEAR(drivers.created_at)'),[$from, $to])
                ->orderByDesc("created_at")
                ->get()->groupBy(function($item) {
                    return $item->driver_name;
                });

            return view('reports.sales_reports', compact('destination','country','expenses','normal','track','destiny_client','destiny_dest','destiny_line','agent','transporter','driver'));
        }
        elseif($user->hasRole('shipping'))
        {
            $track   = Track::select('tracks.*','bill_lading','organization_name','cont_no','loading_date','transporter_name','driver_name','trucking_number','driver_license','driver_contact','driver_passport')
                ->join('laddings','laddings.track_id','=','tracks.id')
                ->join('containers','containers.ladding_id','=','laddings.id')
                ->join('agents','agents.id','=','tracks.agent_id')
                ->join('transporters','transporters.container_id','=','containers.id')
                ->join('drivers','drivers.container_id','=','containers.id')
                ->whereBetween(DB::raw('YEAR(tracks.created_at)'),[$from, $to])
                ->latest('tracks.created_at')
                ->get();


            $destiny_line = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->line_vessel;
            });
            $destiny_client = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->client_name;
            });
            $destiny_dest = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->destination;
            });
            $agent          = Agent::with('user')->get();
            $transporter = Transporter::select('transporters.*','cont_no','bill_lading')
                ->join('containers','containers.id','=','transporters.container_id')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->whereBetween(DB::raw('YEAR(transporters.created_at)'),[$from, $to])
                ->orderByDesc("created_at")
                ->get()->groupBy(function($item) {
                    return $item->transporter_name;
                });

            $driver = Driver::select('drivers.*','cont_no','bill_lading')
                ->join('containers','containers.id','=','drivers.container_id')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->whereBetween(DB::raw('YEAR(drivers.created_at)'),[$from, $to])
                ->orderByDesc("created_at")
                ->get()->groupBy(function($item) {
                    return $item->driver_name;
                });

            return view('reports.shipping', compact('destination','country','expenses','normal','track','destiny_client','destiny_dest','destiny_line','agent','transporter','driver'));
        }
        elseif($user->hasRole('declaration'))
        {
            $track   = Track::select('tracks.*','bill_lading','organization_name','cont_no','loading_date','transporter_name','driver_name','trucking_number','driver_license','driver_contact','driver_passport')
                ->join('laddings','laddings.track_id','=','tracks.id')
                ->join('containers','containers.ladding_id','=','laddings.id')
                ->join('agents','agents.id','=','tracks.agent_id')
                ->join('transporters','transporters.container_id','=','containers.id')
                ->join('drivers','drivers.container_id','=','containers.id')
                ->whereBetween(DB::raw('YEAR(tracks.created_at)'),[$from, $to])
                ->latest('tracks.created_at')
                ->get();


            $destiny_line = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->line_vessel;
            });
            $destiny_client = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->client_name;
            });
            $destiny_dest = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->destination;
            });
            $agent          = Agent::with('user')->get();
            $transporter = Transporter::select('transporters.*','cont_no','bill_lading')
                ->join('containers','containers.id','=','transporters.container_id')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->whereBetween(DB::raw('YEAR(transporters.created_at)'),[$from, $to])
                ->orderByDesc("created_at")
                ->get()->groupBy(function($item) {
                    return $item->transporter_name;
                });

            $driver = Driver::select('drivers.*','cont_no','bill_lading')
                ->join('containers','containers.id','=','drivers.container_id')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->whereBetween(DB::raw('YEAR(drivers.created_at)'),[$from, $to])
                ->orderByDesc("created_at")
                ->get()->groupBy(function($item) {
                    return $item->driver_name;
                });


            return view('reports.declaration', compact('destination','country','expenses','normal','track','destiny_client','destiny_dest','destiny_line','agent','transporter','driver'));
        }
        elseif($user->hasRole('documentation'))
        {
            $track   = Track::select('tracks.*','bill_lading','organization_name','cont_no','loading_date','transporter_name','driver_name','trucking_number','driver_license','driver_contact','driver_passport')
                ->join('laddings','laddings.track_id','=','tracks.id')
                ->join('containers','containers.ladding_id','=','laddings.id')
                ->join('agents','agents.id','=','tracks.agent_id')
                ->join('transporters','transporters.container_id','=','containers.id')
                ->join('drivers','drivers.container_id','=','containers.id')
                ->whereBetween(DB::raw('YEAR(tracks.created_at)'),[$from, $to])
                ->latest('tracks.created_at')
                ->get();


            $destiny_line = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->line_vessel;
            });
            $destiny_client = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->client_name;
            });
            $destiny_dest = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->destination;
            });
            $agent          = Agent::with('user')->get();
            $transporter = Transporter::select('transporters.*','cont_no','bill_lading')
                ->join('containers','containers.id','=','transporters.container_id')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->whereBetween(DB::raw('YEAR(transporters.created_at)'),[$from, $to])
                ->orderByDesc("created_at")
                ->get()->groupBy(function($item) {
                    return $item->transporter_name;
                });

            $driver = Driver::select('drivers.*','cont_no','bill_lading')
                ->join('containers','containers.id','=','drivers.container_id')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->whereBetween(DB::raw('YEAR(drivers.created_at)'),[$from, $to])
                ->orderByDesc("created_at")
                ->get()->groupBy(function($item) {
                    return $item->driver_name;
                });


            return view('reports.documentation', compact('destination','country','expenses','normal','track','destiny_client','destiny_dest','destiny_line','agent','transporter','driver'));
        }
        else if($user->hasRole('agent'))

        {


            $user_id = Auth::id();
            $profile = User::where('id','=',$user_id)->get();
            $track   = Track::select('tracks.*','bill_lading','organization_name','cont_no')
                            ->join('laddings','laddings.track_id','=','tracks.id')
                            ->join('containers','containers.ladding_id','=','laddings.id')
                            ->join('agents','agents.id','=','tracks.agent_id')
//                            ->leftjoin('transporters','transporters.container_id','=','containers.id')
//                            ->leftjoin('drivers','drivers.container_id','=','containers.id')
                            ->where('agents.user_id','=',Auth::id())
                            ->whereBetween(DB::raw('YEAR(tracks.created_at)'),[$from, $to])
                            ->latest('tracks.created_at')
                            ->get();

//            $container_no   = Track::select('tracks.*','bill_lading','organization_name','cont_no')
//                                    ->join('laddings','laddings.track_id','=','tracks.id')
//                                    ->join('containers','containers.ladding_id','=','laddings.id')
//                                    ->join('agents','agents.id','=','tracks.agent_id')
////                                    ->leftjoin('transporters','transporters.container_id','=','containers.id')
////                                    ->leftjoin('drivers','drivers.container_id','=','containers.id')
//                                    ->where('agents.user_id','=',Auth::id())
//                                    ->whereBetween(DB::raw('YEAR(tracks.created_at)'),[$from, $to])
//                                    ->latest('tracks.created_at')
//                                    ->get();

            $container_no   = Container::select('containers.*')
                                ->join('laddings','laddings.id','=','containers.ladding_id')
                                ->join('tracks','tracks.id','=','laddings.track_id')
                                ->join('agents','agents.id','=','tracks.agent_id')
                                ->where('agents.user_id','=',Auth::id())
                                ->whereBetween(DB::raw('YEAR(tracks.created_at)'),[$from, $to])
                                ->latest('tracks.created_at')
                                ->get();

            $destinations = Track::select('tracks.*','bill_lading','organization_name')
                        ->join('laddings','laddings.track_id','=','tracks.id')
                        ->join('agents','agents.id','=','tracks.agent_id')
                        ->whereBetween(DB::raw('YEAR(tracks.created_at)'),[$from, $to])
                        ->latest("tracks.created_at")
                        ->get();


            $agent = Agent::with('user')->get();
            return view('agent.reports', compact('destination','country','track','container_no','destinations','agent','profile'));
        }
        else
        {
            return JsonResponse::create(['error' => 'access-denied'],401);
        }
    }


    /**
     * @param Request $request
     * @return static
     *
     */
    public  function sortDestination(Request $request)
    {
        $from = '2021';
        $to = '2030';
        $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
        $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
        $destination = Destination::all();
        $country = DB::table("countries")->pluck("country_name","id");
        $desk = $request->get('destination');
        $user = Auth::user();
        if ($user->hasRole('admin')){


            $destiny_line = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->line_vessel;
            });
            $destiny_client = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->client_name;
            });
            $destiny_dest = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->destination;
            });
            $track   = Track::select('tracks.*','bill_lading','organization_name')
                            ->join('laddings','laddings.track_id','=','tracks.id')
                            ->join('agents','agents.id','=','tracks.agent_id')
                            ->latest('tracks.document_date_received')
                            ->where('tracks.destination','LIKE','%'.$desk.'%')
                            ->whereBetween(DB::raw('YEAR(tracks.created_at)'),[$from, $to])
                            ->get();
            $agent          = Agent::with('user')->get();
            $transporter = Transporter::select('transporters.*','cont_no','bill_lading')
                                ->join('containers','containers.id','=','transporters.container_id')
                                ->join('laddings','laddings.id','=','containers.ladding_id')
                                ->whereBetween(DB::raw('YEAR(transporters.created_at)'),[$from, $to])
                                ->orderByDesc("created_at")
                                ->get()->groupBy(function($item) {
                                    return $item->transporter_name;
                                });

            $driver = Driver::select('drivers.*','cont_no','bill_lading')
                            ->join('containers','containers.id','=','drivers.container_id')
                            ->join('laddings','laddings.id','=','containers.ladding_id')
                            ->whereBetween(DB::raw('YEAR(drivers.created_at)'),[$from, $to])
                            ->orderByDesc("created_at")
                            ->get()->groupBy(function($item) {
                                return $item->driver_name;
                            });

            return view('reports.reports',compact('destination','country','expenses','normal','track','destiny_client','destiny_dest','destiny_line','agent','transporter','driver'));
        }
        elseif ($user->hasRole('super')){


            $destiny_line = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->line_vessel;
            });
            $destiny_client = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->client_name;
            });
            $destiny_dest = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->destination;
            });
            $track   = Track::select('tracks.*','bill_lading','loading_date','organization_name','cont_no','transporter_name','driver_name','trucking_number','driver_license','driver_contact','driver_passport')
                ->join('laddings','laddings.track_id','=','tracks.id')
                ->join('containers','containers.ladding_id','=','laddings.id')
                ->join('agents','agents.id','=','tracks.agent_id')
                ->join('transporters','transporters.container_id','=','containers.id')
                ->join('drivers','drivers.container_id','=','containers.id')
                ->latest('tracks.document_date_received')
                ->where('tracks.destination', 'like', '%' . $desk . '%')
                ->get();
            $agent          = Agent::with('user')->get();
            $transporter = Transporter::select('transporters.*','cont_no','bill_lading')
                ->join('containers','containers.id','=','transporters.container_id')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->orderByDesc("created_at")
                ->get()->groupBy(function($item) {
                    return $item->transporter_name;
                });

            $driver = Driver::select('drivers.*','cont_no','bill_lading')
                ->join('containers','containers.id','=','drivers.container_id')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->orderByDesc("created_at")
                ->get()->groupBy(function($item) {
                    return $item->driver_name;
                });

            return view('reports.super_reports',compact('destination','country','expenses','normal','track','destiny_client','destiny_dest','destiny_line','agent','transporter','driver'));
        }
        elseif ($user->hasRole('finance')){


            $destiny_line = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->line_vessel;
            });
            $destiny_client = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->client_name;
            });
            $destiny_dest = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->destination;
            });
            $track   = Track::select('tracks.*','bill_lading','loading_date','organization_name','cont_no','transporter_name','driver_name','trucking_number','driver_license','driver_contact','driver_passport')
                ->join('laddings','laddings.track_id','=','tracks.id')
                ->join('containers','containers.ladding_id','=','laddings.id')
                ->join('agents','agents.id','=','tracks.agent_id')
                ->join('transporters','transporters.container_id','=','containers.id')
                ->join('drivers','drivers.container_id','=','containers.id')
                ->latest('tracks.document_date_received')
                ->where('tracks.destination', 'like', '%' . $desk . '%')
                ->whereBetween(DB::raw('YEAR(tracks.created_at)'),[$from, $to])
                ->get();
            $agent          = Agent::with('user')->get();
            $transporter = Transporter::select('transporters.*','cont_no','bill_lading')
                ->join('containers','containers.id','=','transporters.container_id')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->whereBetween(DB::raw('YEAR(transporters.created_at)'),[$from, $to])
                ->orderByDesc("created_at")
                ->get()->groupBy(function($item) {
                    return $item->transporter_name;
                });

            $driver = Driver::select('drivers.*','cont_no','bill_lading')
                ->join('containers','containers.id','=','drivers.container_id')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->whereBetween(DB::raw('YEAR(drivers.created_at)'),[$from, $to])
                ->orderByDesc("created_at")
                ->get()->groupBy(function($item) {
                    return $item->driver_name;
                });

            return view('reports.finance_reports',compact('destination','country','destination','country','expenses','normal','track','destiny_client','destiny_dest','destiny_line','agent','transporter','driver'));
        }
        elseif ($user->hasRole('sales')){



            $destiny_line = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->line_vessel;
            });
            $destiny_client = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->client_name;
            });
            $destiny_dest = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->destination;
            });
            $track   = Track::select('tracks.*','bill_lading','loading_date','organization_name','cont_no','transporter_name','driver_name','trucking_number','driver_license','driver_contact','driver_passport')
                ->join('laddings','laddings.track_id','=','tracks.id')
                ->join('containers','containers.ladding_id','=','laddings.id')
                ->join('agents','agents.id','=','tracks.agent_id')
                ->join('transporters','transporters.container_id','=','containers.id')
                ->join('drivers','drivers.container_id','=','containers.id')
                ->latest('tracks.document_date_received')
                ->where('tracks.destination', 'like', '%' . $desk . '%')
                ->whereBetween(DB::raw('YEAR(tracks.created_at)'),[$from, $to])
                ->get();
            $agent          = Agent::with('user')->get();
            $transporter = Transporter::select('transporters.*','cont_no','bill_lading')
                ->join('containers','containers.id','=','transporters.container_id')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->whereBetween(DB::raw('YEAR(transporters.created_at)'),[$from, $to])
                ->orderByDesc("created_at")
                ->get()->groupBy(function($item) {
                    return $item->transporter_name;
                });

            $driver = Driver::select('drivers.*','cont_no','bill_lading')
                ->join('containers','containers.id','=','drivers.container_id')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->whereBetween(DB::raw('YEAR(drivers.created_at)'),[$from, $to])
                ->orderByDesc("created_at")
                ->get()->groupBy(function($item) {
                    return $item->driver_name;
                });

            return view('reports.sales_reports',compact('destination','country','expenses','normal','track','destiny_client','destiny_dest','destiny_line','agent','transporter','driver'));
        }
        elseif ($user->hasRole('shipping')){



            $destiny_line = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->line_vessel;
            });
            $destiny_client = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->client_name;
            });
            $destiny_dest = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->destination;
            });
            $track   = Track::select('tracks.*','bill_lading','loading_date','organization_name','cont_no','transporter_name','driver_name','trucking_number','driver_license','driver_contact','driver_passport')
                ->join('laddings','laddings.track_id','=','tracks.id')
                ->join('containers','containers.ladding_id','=','laddings.id')
                ->join('agents','agents.id','=','tracks.agent_id')
                ->join('transporters','transporters.container_id','=','containers.id')
                ->join('drivers','drivers.container_id','=','containers.id')
                ->latest('tracks.document_date_received')
                ->where('tracks.destination', 'like', '%' . $desk . '%')
                ->whereBetween(DB::raw('YEAR(tracks.created_at)'),[$from, $to])
                ->get();
            $agent          = Agent::with('user')->get();
            $transporter = Transporter::select('transporters.*','cont_no','bill_lading')
                ->join('containers','containers.id','=','transporters.container_id')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->whereBetween(DB::raw('YEAR(transporters.created_at)'),[$from, $to])
                ->orderByDesc("created_at")
                ->get()->groupBy(function($item) {
                    return $item->transporter_name;
                });

            $driver = Driver::select('drivers.*','cont_no','bill_lading')
                ->join('containers','containers.id','=','drivers.container_id')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->whereBetween(DB::raw('YEAR(drivers.created_at)'),[$from, $to])
                ->orderByDesc("created_at")
                ->get()->groupBy(function($item) {
                    return $item->driver_name;
                });

            return view('reports.shipping',compact('destination','country','expenses','normal','track','destiny_client','destiny_dest','destiny_line','agent','transporter','driver'));
        }
        elseif ($user->hasRole('declaration')){



            $destiny_line = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->line_vessel;
            });
            $destiny_client = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->client_name;
            });
            $destiny_dest = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->destination;
            });
            $track   = Track::select('tracks.*','bill_lading','loading_date','organization_name','cont_no','transporter_name','driver_name','trucking_number','driver_license','driver_contact','driver_passport')
                ->join('laddings','laddings.track_id','=','tracks.id')
                ->join('containers','containers.ladding_id','=','laddings.id')
                ->join('agents','agents.id','=','tracks.agent_id')
                ->join('transporters','transporters.container_id','=','containers.id')
                ->join('drivers','drivers.container_id','=','containers.id')
                ->latest('tracks.document_date_received')
                ->where('tracks.destination','LIKE',$desk)
                ->whereBetween(DB::raw('YEAR(tracks.created_at)'),[$from, $to])
                ->get();
            $agent          = Agent::with('user')->get();
            $transporter = Transporter::select('transporters.*','cont_no','bill_lading')
                ->join('containers','containers.id','=','transporters.container_id')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->whereBetween(DB::raw('YEAR(transporters.created_at)'),[$from, $to])
                ->orderByDesc("created_at")
                ->get()->groupBy(function($item) {
                    return $item->transporter_name;
                });

            $driver = Driver::select('drivers.*','cont_no','bill_lading')
                ->join('containers','containers.id','=','drivers.container_id')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->whereBetween(DB::raw('YEAR(drivers.created_at)'),[$from, $to])
                ->orderByDesc("created_at")
                ->get()->groupBy(function($item) {
                    return $item->driver_name;
                });

            return view('reports.declaration',compact('destination','country','expenses','normal','track','destiny_client','destiny_dest','destiny_line','agent','transporter','driver'));
        }
        elseif ($user->hasRole('documentation')){


            $destiny_line = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->line_vessel;
            });
            $destiny_client = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->client_name;
            });
            $destiny_dest = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->destination;
            });
            $track   = Track::select('tracks.*','bill_lading','loading_date','organization_name','cont_no','transporter_name','driver_name','trucking_number','driver_license','driver_contact','driver_passport')
                ->join('laddings','laddings.track_id','=','tracks.id')
                ->join('containers','containers.ladding_id','=','laddings.id')
                ->join('agents','agents.id','=','tracks.agent_id')
                ->join('transporters','transporters.container_id','=','containers.id')
                ->join('drivers','drivers.container_id','=','containers.id')
                ->latest('tracks.document_date_received')
                ->where('tracks.destination','LIKE',$desk)
                ->whereBetween(DB::raw('YEAR(tracks.created_at)'),[$from, $to])
                ->get();
            $agent          = Agent::with('user')->get();
            $transporter = Transporter::select('transporters.*','cont_no','bill_lading')
                ->join('containers','containers.id','=','transporters.container_id')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->whereBetween(DB::raw('YEAR(transporters.created_at)'),[$from, $to])
                ->orderByDesc("created_at")
                ->get()->groupBy(function($item) {
                    return $item->transporter_name;
                });

            $driver = Driver::select('drivers.*','cont_no','bill_lading')
                ->join('containers','containers.id','=','drivers.container_id')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->whereBetween(DB::raw('YEAR(drivers.created_at)'),[$from, $to])
                ->orderByDesc("created_at")
                ->get()->groupBy(function($item) {
                    return $item->driver_name;
                });

            return view('reports.documentation',compact('destination','country','expenses','normal','track','destiny_client','destiny_dest','destiny_line','agent','transporter','driver'));
        }
        elseif ($user->hasRole('agent'))
        {


            $user_id = Auth::id();
            $profile = User::where('id','=',$user_id)->get();
            $track   = Track::select('tracks.*','bill_lading','organization_name','transporter_name','driver_name','trucking_number','driver_license','driver_contact','driver_passport')
                ->join('laddings','laddings.track_id','=','tracks.id')
                ->join('containers','containers.ladding_id','=','laddings.id')
                ->join('agents','agents.id','=','tracks.agent_id')
                ->join('transporters','transporters.container_id','=','containers.id')
                ->join('drivers','drivers.container_id','=','containers.id')
                ->latest('tracks.document_date_received')
                ->whereBetween(DB::raw('YEAR(tracks.created_at)'),[$from, $to])
                ->where('tracks.destination','LIKE',$desk)
                ->get();
            $agent          = Agent::with('user')->get();

            $transporter = Transporter::select('transporters.*','cont_no','bill_lading')
                ->join('containers','containers.id','=','transporters.container_id')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->whereBetween(DB::raw('YEAR(transporters.created_at)'),[$from, $to])
                ->orderByDesc("created_at")
                ->get()->groupBy(function($item) {
                    return $item->transporter_name;
                });

            $driver = Driver::select('drivers.*','cont_no','bill_lading')
                ->join('containers','containers.id','=','drivers.container_id')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->whereBetween(DB::raw('YEAR(drivers.created_at)'),[$from, $to])
                ->orderByDesc("created_at")
                ->get()->groupBy(function($item) {
                    return $item->driver_name;
                });

            return view('agent.reports',compact('destination','country','track','agent','transporter','driver','profile'));
        }
        else{
            return JsonResponse::create(['error' => 'access-denied'],401);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|static
     */
    public function sortConsignee(Request $request)
    {
        $from = '2021';
        $to = '2030';
        $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
        $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
        $destination = Destination::all();
        $country = DB::table("countries")->pluck("country_name","id");
        $consignee = $request->get('consignee_id');
        $user = Auth::user();
        if ($user->hasRole('admin')){

            $destiny_line = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->line_vessel;
            });
            $destiny_client = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->client_name;
            });
            $destiny_dest = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->destination;
            });
            $track   = Track::select('tracks.*','bill_lading','loading_date','organization_name','cont_no','transporter_name','driver_name','trucking_number','driver_license','driver_contact','driver_passport')
                ->join('laddings','laddings.track_id','=','tracks.id')
                ->join('agents','agents.id','=','tracks.agent_id')
                ->join('containers','containers.ladding_id','=','laddings.id')
                ->join('transporters','transporters.container_id','=','containers.id')
                ->join('drivers','drivers.container_id','=','containers.id')
                ->latest('tracks.document_date_received')
                ->whereBetween(DB::raw('YEAR(tracks.created_at)'),[$from, $to])
                ->where('agents.id','=',$consignee)
                ->get();

            $transporter = Transporter::select('transporters.*','cont_no','bill_lading')
                ->join('containers','containers.id','=','transporters.container_id')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->whereBetween(DB::raw('YEAR(transporters.created_at)'),[$from, $to])
                ->orderByDesc("created_at")
                ->get()->groupBy(function($item) {
                    return $item->transporter_name;
                });

            $driver = Driver::select('drivers.*','cont_no','bill_lading')
                ->join('containers','containers.id','=','drivers.container_id')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->whereBetween(DB::raw('YEAR(drivers.created_at)'),[$from, $to])
                ->orderByDesc("created_at")
                ->get()->groupBy(function($item) {
                    return $item->driver_name;
                });

            $agent          = Agent::with('user')->get();
            return view('reports.reports',compact('destination','country','expenses','normal','track','agent','destiny_client','destiny_dest','destiny_line','transporter','driver'));
        }
        elseif ($user->hasRole('finance')){

            $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
            $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

            $destiny_line = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->line_vessel;
            });
            $destiny_client = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->client_name;
            });
            $destiny_dest = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->destination;
            });
            $track   = Track::select('tracks.*','bill_lading','loading_date','organization_name','cont_no','transporter_name','driver_name','trucking_number','driver_license','driver_contact','driver_passport')
                ->join('laddings','laddings.track_id','=','tracks.id')
                ->join('agents','agents.id','=','tracks.agent_id')
                ->join('containers','containers.ladding_id','=','laddings.id')
                ->join('transporters','transporters.container_id','=','containers.id')
                ->join('drivers','drivers.container_id','=','containers.id')
                ->latest('tracks.document_date_received')
                ->whereBetween(DB::raw('YEAR(tracks.created_at)'),[$from, $to])
                ->where('agents.id','=',$consignee)
                ->get();

            $transporter = Transporter::select('transporters.*','cont_no','bill_lading')
                ->join('containers','containers.id','=','transporters.container_id')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->whereBetween(DB::raw('YEAR(transporters.created_at)'),[$from, $to])
                ->orderByDesc("created_at")
                ->get()->groupBy(function($item) {
                    return $item->transporter_name;
                });

            $driver = Driver::select('drivers.*','cont_no','bill_lading')
                ->join('containers','containers.id','=','drivers.container_id')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->whereBetween(DB::raw('YEAR(drivers.created_at)'),[$from, $to])
                ->orderByDesc("created_at")
                ->get()->groupBy(function($item) {
                    return $item->driver_name;
                });

            $agent          = Agent::with('user')->get();
            return view('reports.finance_reports',compact('destination','country','expenses','normal','track','agent','destiny_client','destiny_dest','destiny_line','transporter','driver'));
        }
        elseif ($user->hasRole('super')){
            $expenses = Expenses::with('container')->where('finance_status','LIKE','pending')->count();

            $normal = NormalExpenses::where('finance_status','LIKE','pending')->count();

            $destiny_line = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->line_vessel;
            });
            $destiny_client = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->client_name;
            });
            $destiny_dest = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->destination;
            });
            $track   = Track::select('tracks.*','bill_lading','loading_date','organization_name','cont_no','transporter_name','driver_name','trucking_number','driver_license','driver_contact','driver_passport')
                ->join('laddings','laddings.track_id','=','tracks.id')
                ->join('agents','agents.id','=','tracks.agent_id')
                ->join('containers','containers.ladding_id','=','laddings.id')
                ->join('transporters','transporters.container_id','=','containers.id')
                ->join('drivers','drivers.container_id','=','containers.id')
                ->latest('tracks.document_date_received')
                ->where('agents.id','=',$consignee)
                ->get();

            $transporter = Transporter::select('transporters.*','cont_no','bill_lading')
                ->join('containers','containers.id','=','transporters.container_id')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->orderByDesc("created_at")
                ->get()->groupBy(function($item) {
                    return $item->transporter_name;
                });

            $driver = Driver::select('drivers.*','cont_no','bill_lading')
                ->join('containers','containers.id','=','drivers.container_id')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->orderByDesc("created_at")
                ->get()->groupBy(function($item) {
                    return $item->driver_name;
                });

            $agent          = Agent::with('user')->get();
            return view('reports.super_reports',compact('destination','country','expenses','normal','track','agent','destiny_client','destiny_dest','destiny_line','transporter','driver'));
        }
        elseif ($user->hasRole('sales')){

            $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
            $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

            $destiny_line = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->line_vessel;
            });
            $destiny_client = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->client_name;
            });
            $destiny_dest = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->destination;
            });
            $track   = Track::select('tracks.*','bill_lading','loading_date','organization_name','cont_no','transporter_name','driver_name','trucking_number','driver_license','driver_contact','driver_passport')
                ->join('laddings','laddings.track_id','=','tracks.id')
                ->join('agents','agents.id','=','tracks.agent_id')
                ->join('containers','containers.ladding_id','=','laddings.id')
                ->join('transporters','transporters.container_id','=','containers.id')
                ->join('drivers','drivers.container_id','=','containers.id')
                ->latest('tracks.document_date_received')
                ->whereBetween(DB::raw('YEAR(tracks.created_at)'),[$from, $to])
                ->where('agents.id','=',$consignee)
                ->get();

            $transporter = Transporter::select('transporters.*','cont_no','bill_lading')
                ->join('containers','containers.id','=','transporters.container_id')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->whereBetween(DB::raw('YEAR(transporters.created_at)'),[$from, $to])
                ->orderByDesc("created_at")
                ->get()->groupBy(function($item) {
                    return $item->transporter_name;
                });

            $driver = Driver::select('drivers.*','cont_no','bill_lading')
                ->join('containers','containers.id','=','drivers.container_id')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->whereBetween(DB::raw('YEAR(drivers.created_at)'),[$from, $to])
                ->orderByDesc("created_at")
                ->get()->groupBy(function($item) {
                    return $item->driver_name;
                });

            $agent          = Agent::with('user')->get();
            return view('reports.sales_reports',compact('destination','country','expenses','normal','track','agent','destiny_client','destiny_dest','destiny_line','transporter','driver'));
        }
        elseif ($user->hasRole('shipping')){

            $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
            $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

            $destiny_line = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->line_vessel;
            });
            $destiny_client = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->client_name;
            });
            $destiny_dest = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->destination;
            });
            $track   = Track::select('tracks.*','bill_lading','loading_date','organization_name','cont_no','transporter_name','driver_name','trucking_number','driver_license','driver_contact','driver_passport')
                ->join('laddings','laddings.track_id','=','tracks.id')
                ->join('agents','agents.id','=','tracks.agent_id')
                ->join('containers','containers.ladding_id','=','laddings.id')
                ->join('transporters','transporters.container_id','=','containers.id')
                ->join('drivers','drivers.container_id','=','containers.id')
                ->latest('tracks.document_date_received')
                ->whereBetween(DB::raw('YEAR(tracks.created_at)'),[$from, $to])
                ->where('agents.id','=',$consignee)
                ->get();

            $transporter = Transporter::select('transporters.*','cont_no','bill_lading')
                ->join('containers','containers.id','=','transporters.container_id')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->whereBetween(DB::raw('YEAR(transporters.created_at)'),[$from, $to])
                ->orderByDesc("created_at")
                ->get()->groupBy(function($item) {
                    return $item->transporter_name;
                });

            $driver = Driver::select('drivers.*','cont_no','bill_lading')
                ->join('containers','containers.id','=','drivers.container_id')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->whereBetween(DB::raw('YEAR(drivers.created_at)'),[$from, $to])
                ->orderByDesc("created_at")
                ->get()->groupBy(function($item) {
                    return $item->driver_name;
                });

            $agent          = Agent::with('user')->get();
            return view('reports.shipping',compact('destination','country','expenses','normal','track','agent','destiny_client','destiny_dest','destiny_line','transporter','driver'));
        }
        elseif ($user->hasRole('declaration')){

            $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
            $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

            $destiny_line = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->line_vessel;
            });
            $destiny_client = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->client_name;
            });
            $destiny_dest = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->destination;
            });
            $track   = Track::select('tracks.*','bill_lading','loading_date','organization_name','cont_no','transporter_name','driver_name','trucking_number','driver_license','driver_contact','driver_passport')
                ->join('laddings','laddings.track_id','=','tracks.id')
                ->join('agents','agents.id','=','tracks.agent_id')
                ->join('containers','containers.ladding_id','=','laddings.id')
                ->join('transporters','transporters.container_id','=','containers.id')
                ->join('drivers','drivers.container_id','=','containers.id')
                ->latest('tracks.document_date_received')
                ->whereBetween(DB::raw('YEAR(tracks.created_at)'),[$from, $to])
                ->where('agents.id','=',$consignee)
                ->get();

            $transporter = Transporter::select('transporters.*','cont_no','bill_lading')
                ->join('containers','containers.id','=','transporters.container_id')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->whereBetween(DB::raw('YEAR(transporters.created_at)'),[$from, $to])
                ->orderByDesc("created_at")
                ->get()->groupBy(function($item) {
                    return $item->transporter_name;
                });

            $driver = Driver::select('drivers.*','cont_no','bill_lading')
                ->join('containers','containers.id','=','drivers.container_id')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->whereBetween(DB::raw('YEAR(drivers.created_at)'),[$from, $to])
                ->orderByDesc("created_at")
                ->get()->groupBy(function($item) {
                    return $item->driver_name;
                });

            $agent          = Agent::with('user')->get();
            return view('reports.declaration',compact('destination','country','expenses','normal','track','agent','destiny_client','destiny_dest','destiny_line','transporter','driver'));
        }
        elseif ($user->hasRole('documentation')){

            $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
            $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

            $destiny_line = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->line_vessel;
            });
            $destiny_client = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->client_name;
            });
            $destiny_dest = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->destination;
            });
            $track   = Track::select('tracks.*','bill_lading','loading_date','organization_name','cont_no','transporter_name','driver_name','trucking_number','driver_license','driver_contact','driver_passport')
                ->join('laddings','laddings.track_id','=','tracks.id')
                ->join('agents','agents.id','=','tracks.agent_id')
                ->join('containers','containers.ladding_id','=','laddings.id')
                ->join('transporters','transporters.container_id','=','containers.id')
                ->join('drivers','drivers.container_id','=','containers.id')
                ->latest('tracks.document_date_received')
                ->whereBetween(DB::raw('YEAR(tracks.created_at)'),[$from, $to])
                ->where('agents.id','=',$consignee)
                ->get();

            $transporter = Transporter::select('transporters.*','cont_no','bill_lading')
                ->join('containers','containers.id','=','transporters.container_id')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->whereBetween(DB::raw('YEAR(transporters.created_at)'),[$from, $to])
                ->orderByDesc("created_at")
                ->get()->groupBy(function($item) {
                    return $item->transporter_name;
                });

            $driver = Driver::select('drivers.*','cont_no','bill_lading')
                ->join('containers','containers.id','=','drivers.container_id')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->whereBetween(DB::raw('YEAR(drivers.created_at)'),[$from, $to])
                ->orderByDesc("created_at")
                ->get()->groupBy(function($item) {
                    return $item->driver_name;
                });

            $agent          = Agent::with('user')->get();
            return view('reports.documentation',compact('destination','country','expenses','normal','track','agent','destiny_client','destiny_dest','destiny_line','transporter','driver'));
        }
        elseif ($user->hasRole('agent'))
        {
            $user_id = Auth::id();
            $profile = User::where('id','=',$user_id)->get();

            $destiny_line = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->line_vessel;
            });
            $destiny_client = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->client_name;
            });
            $destiny_dest = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->destination;
            });
            $track   = Track::select('tracks.*','bill_lading','organization_name','transporter_name','cont_no','driver_name','trucking_number','driver_license','driver_contact','driver_passport')
                ->join('laddings','laddings.track_id','=','tracks.id')
                ->join('containers','containers.ladding_id','=','laddings.id')
                ->join('transporters','transporters.container_id','=','containers.id')
                ->join('drivers','drivers.container_id','=','containers.id')
                ->join('agents','agents.id','=','tracks.agent_id')
                ->latest('tracks.document_date_received')
                ->whereBetween(DB::raw('YEAR(tracks.created_at)'),[$from, $to])
                ->where('agents.id','=',$consignee)
                ->get();
            $agent          = Agent::with('user')->get();

            $transporter = Transporter::select('transporters.*','cont_no','bill_lading')
                ->join('containers','containers.id','=','transporters.container_id')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->whereBetween(DB::raw('YEAR(transporters.created_at)'),[$from, $to])
                ->orderByDesc("created_at")
                ->get()->groupBy(function($item) {
                    return $item->transporter_name;
                });

            $driver = Driver::select('drivers.*','cont_no','bill_lading')
                ->join('containers','containers.id','=','drivers.container_id')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->whereBetween(DB::raw('YEAR(drivers.created_at)'),[$from, $to])
                ->orderByDesc("created_at")
                ->get()->groupBy(function($item) {
                    return $item->driver_name;
                });

            $destiny_all = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->destination;
            });
            return view('agent.reports',compact('destination','country','profile','track','agent','destiny_client','destiny_dest','destiny_line','transporter','driver','destiny_all'));
        }
        else{
            return JsonResponse::create(['error' => 'access-denied'],401);
        }
    }
    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|string
     *
     */
    public function sortOrders(Request $request)
    {
        $date_from = '2021';
        $date_to = '2030';

        $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'),[$date_from, $date_to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
        $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'),[$date_from, $date_to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
        $destination = Destination::all();
        $country = DB::table("countries")->pluck("country_name","id");

        $user   =Auth::user();
        if($user->hasRole('admin'))
        {
            $transporter = Transporter::select('transporters.*','cont_no','bill_lading')
                ->join('containers','containers.id','=','transporters.container_id')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->whereBetween(DB::raw('YEAR(transporters.created_at)'),[$date_from, $date_to])
                ->orderByDesc("created_at")
                ->get()->groupBy(function($item) {
                    return $item->transporter_name;
                });

            $driver = Driver::select('drivers.*','cont_no','bill_lading')
                ->join('containers','containers.id','=','drivers.container_id')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->whereBetween(DB::raw('YEAR(drivers.created_at)'),[$date_from, $date_to])
                ->orderByDesc("created_at")
                ->get()->groupBy(function($item) {
                    return $item->driver_name;
                });


            $destiny_line = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->line_vessel;
            });
            $destiny_client = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->client_name;
            });
            $destiny_dest = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->destination;
            });

            $from   =  $request->input('date1');
            $to     =  $request->input('date2');

            /**
             *
             * If you select From one date  to another Date
             **/

            if(($from  !="") AND ($to !=""))
            {

                $track   = Track::select('tracks.*','bill_lading','organization_name','loading_date','transporter_name','cont_no','driver_name','trucking_number','driver_license','driver_contact','driver_passport')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('containers','containers.ladding_id','=','laddings.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->join('transporters','transporters.container_id','=','containers.id')
                    ->join('drivers','drivers.container_id','=','containers.id')
                    ->latest('tracks.document_date_received')
                    ->whereBetween(DB::raw('YEAR(tracks.created_at)'),[$date_from, $date_to])
                    ->where('tracks.document_date_received','>=', $from)
                    ->where('tracks.document_date_received','<=', $to)
                    ->get();
                $agent          = Agent::with('user')->get();

                return view('reports.reports',compact('destination','country','destination','country','expenses','normal','track','agent','destiny_client','destiny_dest','destiny_line','transporter','driver'));
            }
            /**
             *
             * If you select From one date only
             **/

            else  if(($from  !="") AND ($to ==""))
            {

                $track   = Track::select('tracks.*','bill_lading','organization_name','loading_date','transporter_name','cont_no','driver_name','trucking_number','driver_license','driver_contact','driver_passport')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('containers','containers.ladding_id','=','laddings.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->join('transporters','transporters.container_id','=','containers.id')
                    ->join('drivers','drivers.container_id','=','containers.id')
                    ->latest('tracks.document_date_received')
                    ->whereBetween(DB::raw('YEAR(tracks.created_at)'),[$date_from, $date_to])
                    ->where('tracks.document_date_received','>=', $from)
                    ->get();
                $agent          = Agent::with('user')->get();
                return view('reports.reports',compact('destination','country','expenses','normal','track','agent','destiny_client','destiny_dest','destiny_line','transporter','driver'));
            }

            /**
             *
             * If you select To the last date
             *
             **/

            else  if(($from  =="") AND ($to !=""))
            {

                $track = Track::select('tracks.*')
                    ->whereBetween(DB::raw('YEAR(tracks.created_at)'),[$date_from, $date_to])
                    ->where('tracks.document_date_received','<=', $to)
                    ->get();
                $agent          = Agent::with('user')->get();
                return view('reports.reports',compact('destination','country','expenses','normal','track','agent','transporter','destiny_client','destiny_dest','destiny_line','driver'));
            }
            /**
             *
             * If you don't select anything
             *
             **/

            else
            {
                $track = Track::select('tracks.*')
                    ->whereBetween(DB::raw('YEAR(tracks.created_at)'),[$date_from, $date_to])
                    ->get();
                $agent          = Agent::with('user')->get();
                return view('reports.reports',compact('destination','country','expenses','normal','track','agent','destiny_client','destiny_dest','destiny_line','transporter','driver'));
            }
        }
        elseif($user->hasRole('super'))
        {
            $expenses = Expenses::with('container')->where('finance_status','LIKE','pending')->count();

            $normal = NormalExpenses::where('finance_status','LIKE','pending')->count();

            $transporter = Transporter::select('transporters.*','cont_no','bill_lading')
                ->join('containers','containers.id','=','transporters.container_id')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->orderByDesc("created_at")
                ->get()->groupBy(function($item) {
                    return $item->transporter_name;
                });

            $driver = Driver::select('drivers.*','cont_no','bill_lading')
                ->join('containers','containers.id','=','drivers.container_id')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->orderByDesc("created_at")
                ->get()->groupBy(function($item) {
                    return $item->driver_name;
                });


            $destiny_line = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->line_vessel;
            });
            $destiny_client = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->client_name;
            });
            $destiny_dest = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->destination;
            });

            $from   =  $request->input('date1');
            $to     =  $request->input('date2');

            /**
             *
             * If you select From one date  to another Date
             **/

            if(($from  !="") AND ($to !=""))
            {

                $track   = Track::select('tracks.*','bill_lading','organization_name','loading_date','transporter_name','cont_no','driver_name','trucking_number','driver_license','driver_contact','driver_passport')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('containers','containers.ladding_id','=','laddings.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->join('transporters','transporters.container_id','=','containers.id')
                    ->join('drivers','drivers.container_id','=','containers.id')
                    ->latest('tracks.document_date_received')
                    ->where('tracks.document_date_received','>=', $from)
                    ->where('tracks.document_date_received','<=', $to)
                    ->get();
                $agent          = Agent::with('user')->get();

                return view('reports.super_reports',compact('destination','country','expenses','normal','track','agent','destiny_client','destiny_dest','destiny_line','transporter','driver'));
            }
            /**
             *
             * If you select From one date only
             **/

            else  if(($from  !="") AND ($to ==""))
            {

                $track   = Track::select('tracks.*','bill_lading','organization_name','loading_date','transporter_name','cont_no','driver_name','trucking_number','driver_license','driver_contact','driver_passport')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('containers','containers.ladding_id','=','laddings.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->join('transporters','transporters.container_id','=','containers.id')
                    ->join('drivers','drivers.container_id','=','containers.id')
                    ->latest('tracks.document_date_received')
                    ->where('tracks.document_date_received','>=', $from)
                    ->get();
                $agent          = Agent::with('user')->get();
                return view('reports.super_reports',compact('destination','country','expenses','normal','track','agent','destiny_client','destiny_dest','destiny_line','transporter','driver'));
            }

            /**
             *
             * If you select To the last date
             *
             **/

            else  if(($from  =="") AND ($to !=""))
            {

                $track = Track::select('tracks.*')
                    ->where('date' ,'<=', $to)
                    ->get();
                $agent          = Agent::with('user')->get();
                return view('reports.super_reports',compact('destination','country','expenses','normal','track','agent','transporter','destiny_client','destiny_dest','destiny_line','driver'));
            }
            /**
             *
             * If you don't select anything
             *
             **/

            else
            {
                $track = Track::select('tracks.*')
                    ->get();
                $agent          = Agent::with('user')->get();
                return view('reports.super_reports',compact('destination','country','expenses','normal','track','agent','destiny_client','destiny_dest','destiny_line','transporter','driver'));
            }
        }
        elseif($user->hasRole('sales'))
        {
            $transporter = Transporter::select('transporters.*','cont_no','bill_lading')
                ->join('containers','containers.id','=','transporters.container_id')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->whereBetween(DB::raw('YEAR(transporters.created_at)'),[$date_from, $date_to])
                ->orderByDesc("created_at")
                ->get()->groupBy(function($item) {
                    return $item->transporter_name;
                });

            $driver = Driver::select('drivers.*','cont_no','bill_lading')
                ->join('containers','containers.id','=','drivers.container_id')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->whereBetween(DB::raw('YEAR(drivers.created_at)'),[$date_from, $date_to])
                ->orderByDesc("created_at")
                ->get()->groupBy(function($item) {
                    return $item->driver_name;
                });


            $destiny_line = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->line_vessel;
            });
            $destiny_client = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->client_name;
            });
            $destiny_dest = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->destination;
            });

            $from   =  $request->input('date1');
            $to     =  $request->input('date2');

            /**
             *
             * If you select From one date  to another Date
             **/

            if(($from  !="") AND ($to !=""))
            {

                $track   = Track::select('tracks.*','bill_lading','organization_name','loading_date','transporter_name','cont_no','driver_name','trucking_number','driver_license','driver_contact','driver_passport')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('containers','containers.ladding_id','=','laddings.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->join('transporters','transporters.container_id','=','containers.id')
                    ->join('drivers','drivers.container_id','=','containers.id')
                    ->latest('tracks.document_date_received')
                    ->whereBetween(DB::raw('YEAR(tracks.created_at)'),[$date_from, $date_to])
                    ->where('tracks.document_date_received','>=', $from)
                    ->where('tracks.document_date_received','<=', $to)
                    ->get();
                $agent          = Agent::with('user')->get();

                return view('reports.sales_reports',compact('destination','country','expenses','normal','track','agent','destiny_client','destiny_dest','destiny_line','transporter','driver'));
            }
            /**
             *
             * If you select From one date only
             **/

            else  if(($from  !="") AND ($to ==""))
            {

                $track   = Track::select('tracks.*','bill_lading','organization_name','loading_date','transporter_name','cont_no','driver_name','trucking_number','driver_license','driver_contact','driver_passport')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('containers','containers.ladding_id','=','laddings.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->join('transporters','transporters.container_id','=','containers.id')
                    ->join('drivers','drivers.container_id','=','containers.id')
                    ->latest('tracks.document_date_received')
                    ->whereBetween(DB::raw('YEAR(tracks.created_at)'),[$date_from, $date_to])
                    ->where('tracks.document_date_received','>=', $from)
                    ->get();
                $agent          = Agent::with('user')->get();
                return view('reports.sales_reports',compact('destination','country','expenses','normal','track','agent','destiny_client','destiny_dest','destiny_line','transporter','driver'));
            }

            /**
             *
             * If you select To the last date
             *
             **/

            else  if(($from  =="") AND ($to !=""))
            {

                $track = Track::select('tracks.*')
                    ->whereBetween(DB::raw('YEAR(tracks.created_at)'),[$date_from, $date_to])
                    ->where('date' ,'<=', $to)
                    ->get();
                $agent          = Agent::with('user')->get();
                return view('reports.sales_reports',compact('destination','country','expenses','normal','track','agent','transporter','destiny_client','destiny_dest','destiny_line','driver'));
            }
            /**
             *
             * If you don't select anything
             *
             **/

            else
            {
                $track = Track::select('tracks.*')
                    ->whereBetween(DB::raw('YEAR(tracks.created_at)'),[$date_from, $date_to])
                    ->get();
                $agent          = Agent::with('user')->get();
                return view('reports.sales_reports',compact('destination','country','expenses','normal','track','agent','destiny_client','destiny_dest','destiny_line','transporter','driver'));
            }
        }
        elseif($user->hasRole('finance'))
        {
            $expenses = Expenses::with('container')->where('finance_status','LIKE','pending')->count();

            $normal = NormalExpenses::where('finance_status','LIKE','pending')->count();

            $transporter = Transporter::select('transporters.*','cont_no','bill_lading')
                ->join('containers','containers.id','=','transporters.container_id')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->whereBetween(DB::raw('YEAR(transporters.created_at)'),[$date_from, $date_to])
                ->orderByDesc("created_at")
                ->get()->groupBy(function($item) {
                    return $item->transporter_name;
                });

            $driver = Driver::select('drivers.*','cont_no','bill_lading')
                ->join('containers','containers.id','=','drivers.container_id')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->whereBetween(DB::raw('YEAR(drivers.created_at)'),[$date_from, $date_to])
                ->orderByDesc("created_at")
                ->get()->groupBy(function($item) {
                    return $item->driver_name;
                });


            $destiny_line = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->line_vessel;
            });
            $destiny_client = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->client_name;
            });
            $destiny_dest = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->destination;
            });

            $from   =  $request->input('date1');
            $to     =  $request->input('date2');

            /**
             *
             * If you select From one date  to another Date
             **/

            if(($from  !="") AND ($to !=""))
            {

                $track   = Track::select('tracks.*','bill_lading','organization_name','loading_date','transporter_name','cont_no','driver_name','trucking_number','driver_license','driver_contact','driver_passport')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('containers','containers.ladding_id','=','laddings.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->join('transporters','transporters.container_id','=','containers.id')
                    ->join('drivers','drivers.container_id','=','containers.id')
                    ->latest('tracks.document_date_received')
                    ->whereBetween(DB::raw('YEAR(tracks.created_at)'),[$date_from, $date_to])
                    ->where('tracks.document_date_received','>=', $from)
                    ->where('tracks.document_date_received','<=', $to)
                    ->get();
                $agent          = Agent::with('user')->get();

                return view('reports.finance_reports',compact('destination','country','expenses','normal','track','agent','destiny_client','destiny_dest','destiny_line','transporter','driver'));
            }
            /**
             *
             * If you select From one date only
             **/

            else  if(($from  !="") AND ($to ==""))
            {

                $track   = Track::select('tracks.*','bill_lading','organization_name','loading_date','transporter_name','cont_no','driver_name','trucking_number','driver_license','driver_contact','driver_passport')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('containers','containers.ladding_id','=','laddings.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->join('transporters','transporters.container_id','=','containers.id')
                    ->join('drivers','drivers.container_id','=','containers.id')
                    ->latest('tracks.document_date_received')
                    ->whereBetween(DB::raw('YEAR(tracks.created_at)'),[$date_from, $date_to])
                    ->where('tracks.document_date_received','>=', $from)
                    ->get();
                $agent          = Agent::with('user')->get();
                return view('reports.finance_reports',compact('destination','country','expenses','normal','track','agent','destiny_client','destiny_dest','destiny_line','transporter','driver'));
            }

            /**
             *
             * If you select To the last date
             *
             **/

            else  if(($from  =="") AND ($to !=""))
            {

                $track = Track::select('tracks.*')
                    ->whereBetween(DB::raw('YEAR(tracks.created_at)'),[$date_from, $date_to])
                    ->where('date' ,'<=', $to)
                    ->get();
                $agent          = Agent::with('user')->get();
                return view('reports.finance_reports',compact('destination','country','expenses','normal','track','agent','transporter','destiny_client','destiny_dest','destiny_line','driver'));
            }
            /**
             *
             * If you don't select anything
             *
             **/

            else
            {
                $track = Track::select('tracks.*')
                    ->whereBetween(DB::raw('YEAR(tracks.created_at)'),[$date_from, $date_to])
                    ->get();
                $agent          = Agent::with('user')->get();
                return view('reports.finance_reports',compact('destination','country','expenses','normal','track','agent','destiny_client','destiny_dest','destiny_line','transporter','driver'));
            }
        }
        elseif($user->hasRole('shipping'))
        {
            $transporter = Transporter::select('transporters.*','cont_no','bill_lading')
                ->join('containers','containers.id','=','transporters.container_id')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->whereBetween(DB::raw('YEAR(transporters.created_at)'),[$date_from, $date_to])
                ->orderByDesc("created_at")
                ->get()->groupBy(function($item) {
                    return $item->transporter_name;
                });

            $driver = Driver::select('drivers.*','cont_no','bill_lading')
                ->join('containers','containers.id','=','drivers.container_id')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->whereBetween(DB::raw('YEAR(drivers.created_at)'),[$date_from, $date_to])
                ->get()->groupBy(function($item) {
                    return $item->driver_name;
                });


            $destiny_line = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->line_vessel;
            });
            $destiny_client = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->client_name;
            });
            $destiny_dest = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->destination;
            });

            $from   =  $request->input('date1');
            $to     =  $request->input('date2');

            /**
             *
             * If you select From one date  to another Date
             **/

            if(($from  !="") AND ($to !=""))
            {

                $track   = Track::select('tracks.*','bill_lading','organization_name','loading_date','transporter_name','cont_no','driver_name','trucking_number','driver_license','driver_contact','driver_passport')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('containers','containers.ladding_id','=','laddings.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->join('transporters','transporters.container_id','=','containers.id')
                    ->join('drivers','drivers.container_id','=','containers.id')
                    ->latest('tracks.document_date_received')
                    ->whereBetween(DB::raw('YEAR(tracks.created_at)'),[$date_from, $date_to])
                    ->where('tracks.document_date_received','>=', $from)
                    ->where('tracks.document_date_received','<=', $to)
                    ->get();
                $agent          = Agent::with('user')->get();

                return view('reports.shipping',compact('destination','country','expenses','normal','track','agent','destiny_client','destiny_dest','destiny_line','transporter','driver'));
            }
            /**
             *
             * If you select From one date only
             **/

            else  if(($from  !="") AND ($to ==""))
            {

                $track   = Track::select('tracks.*','bill_lading','organization_name','loading_date','transporter_name','cont_no','driver_name','trucking_number','driver_license','driver_contact','driver_passport')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('containers','containers.ladding_id','=','laddings.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->join('transporters','transporters.container_id','=','containers.id')
                    ->join('drivers','drivers.container_id','=','containers.id')
                    ->latest('tracks.document_date_received')
                    ->whereBetween(DB::raw('YEAR(tracks.created_at)'),[$date_from, $date_to])
                    ->where('tracks.document_date_received','>=', $from)
                    ->get();
                $agent          = Agent::with('user')->get();
                return view('reports.shipping',compact('destination','country','expenses','normal','track','agent','destiny_client','destiny_dest','destiny_line','transporter','driver'));
            }

            /**
             *
             * If you select To the last date
             *
             **/

            else  if(($from  =="") AND ($to !=""))
            {

                $track = Track::select('tracks.*')
                    ->whereBetween(DB::raw('YEAR(tracks.created_at)'),[$date_from, $date_to])
                    ->where('date' ,'<=', $to)
                    ->get();
                $agent          = Agent::with('user')->get();
                return view('reports.shipping',compact('destination','country','expenses','normal','track','agent','transporter','destiny_client','destiny_dest','destiny_line','driver'));
            }
            /**
             *
             * If you don't select anything
             *
             **/

            else
            {
                $track = Track::select('tracks.*')
                    ->whereBetween(DB::raw('YEAR(tracks.created_at)'),[$date_from, $date_to])
                    ->get();
                $agent          = Agent::with('user')->get();
                return view('reports.shipping',compact('destination','country','expenses','normal','track','agent','destiny_client','destiny_dest','destiny_line','transporter','driver'));
            }
        }
        elseif($user->hasRole('declaration'))
        {
            $transporter = Transporter::select('transporters.*','cont_no','bill_lading')
                ->join('containers','containers.id','=','transporters.container_id')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->whereBetween(DB::raw('YEAR(transporters.created_at)'),[$date_from, $date_to])
                ->orderByDesc("created_at")
                ->get()->groupBy(function($item) {
                    return $item->transporter_name;
                });

            $driver = Driver::select('drivers.*','cont_no','bill_lading')
                ->join('containers','containers.id','=','drivers.container_id')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->whereBetween(DB::raw('YEAR(drivers.created_at)'),[$date_from, $date_to])
                ->orderByDesc("created_at")
                ->get()->groupBy(function($item) {
                    return $item->driver_name;
                });


            $destiny_line = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->line_vessel;
            });
            $destiny_client = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->client_name;
            });
            $destiny_dest = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->destination;
            });

            $from   =  $request->input('date1');
            $to     =  $request->input('date2');

            /**
             *
             * If you select From one date  to another Date
             **/

            if(($from  !="") AND ($to !=""))
            {

                $track   = Track::select('tracks.*','bill_lading','organization_name','loading_date','transporter_name','cont_no','driver_name','trucking_number','driver_license','driver_contact','driver_passport')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('containers','containers.ladding_id','=','laddings.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->join('transporters','transporters.container_id','=','containers.id')
                    ->join('drivers','drivers.container_id','=','containers.id')
                    ->latest('tracks.document_date_received')
                    ->whereBetween(DB::raw('YEAR(tracks.created_at)'),[$date_from, $date_to])
                    ->where('tracks.document_date_received','>=', $from)
                    ->where('tracks.document_date_received','<=', $to)
                    ->get();
                $agent          = Agent::with('user')->get();

                return view('reports.declaration',compact('destination','country','expenses','normal','track','agent','destiny_client','destiny_dest','destiny_line','transporter','driver'));
            }
            /**
             *
             * If you select From one date only
             **/

            else  if(($from  !="") AND ($to ==""))
            {

                $track   = Track::select('tracks.*','bill_lading','organization_name','loading_date','transporter_name','cont_no','driver_name','trucking_number','driver_license','driver_contact','driver_passport')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('containers','containers.ladding_id','=','laddings.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->join('transporters','transporters.container_id','=','containers.id')
                    ->join('drivers','drivers.container_id','=','containers.id')
                    ->latest('tracks.document_date_received')
                    ->whereBetween(DB::raw('YEAR(tracks.created_at)'),[$date_from, $date_to])
                    ->where('tracks.document_date_received','>=', $from)
                    ->get();
                $agent          = Agent::with('user')->get();
                return view('reports.declaration',compact('destination','country','expenses','normal','track','agent','destiny_client','destiny_dest','destiny_line','transporter','driver'));
            }

            /**
             *
             * If you select To the last date
             *
             **/

            else  if(($from  =="") AND ($to !=""))
            {

                $track = Track::select('tracks.*')
                    ->whereBetween(DB::raw('YEAR(tracks.created_at)'),[$date_from, $date_to])
                    ->where('tracks.document_date_received','<=', $to)
                    ->get();
                $agent          = Agent::with('user')->get();
                return view('reports.declaration',compact('destination','country','expenses','normal','track','agent','transporter','destiny_client','destiny_dest','destiny_line','driver'));
            }
            /**
             *
             * If you don't select anything
             *
             **/

            else
            {
                $track = Track::select('tracks.*')
                    ->whereBetween(DB::raw('YEAR(tracks.created_at)'),[$date_from, $date_to])
                    ->get();
                $agent          = Agent::with('user')->get();
                return view('reports.declaration',compact('destination','country','expenses','normal','track','agent','destiny_client','destiny_dest','destiny_line','transporter','driver'));
            }
        }
        elseif($user->hasRole('documentation'))
        {
            $transporter = Transporter::select('transporters.*','cont_no','bill_lading')
                ->join('containers','containers.id','=','transporters.container_id')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->whereBetween(DB::raw('YEAR(transporters.created_at)'),[$date_from, $date_to])
                ->orderByDesc("created_at")
                ->get()->groupBy(function($item) {
                    return $item->transporter_name;
                });

            $driver = Driver::select('drivers.*','cont_no','bill_lading')
                ->join('containers','containers.id','=','drivers.container_id')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->whereBetween(DB::raw('YEAR(drivers.created_at)'),[$date_from, $date_to])
                ->orderByDesc("created_at")
                ->get()->groupBy(function($item) {
                    return $item->driver_name;
                });


            $destiny_line = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->line_vessel;
            });
            $destiny_client = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->client_name;
            });
            $destiny_dest = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->destination;
            });

            $from   =  $request->input('date1');
            $to     =  $request->input('date2');

            /**
             *
             * If you select From one date  to another Date
             **/

            if(($from  !="") AND ($to !=""))
            {

                $track   = Track::select('tracks.*','bill_lading','organization_name','loading_date','transporter_name','cont_no','driver_name','trucking_number','driver_license','driver_contact','driver_passport')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('containers','containers.ladding_id','=','laddings.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->join('transporters','transporters.container_id','=','containers.id')
                    ->join('drivers','drivers.container_id','=','containers.id')
                    ->latest('tracks.document_date_received')
                    ->whereBetween(DB::raw('YEAR(tracks.created_at)'),[$date_from, $date_to])
                    ->where('tracks.document_date_received','>=', $from)
                    ->where('tracks.document_date_received','<=', $to)
                    ->get();
                $agent          = Agent::with('user')->get();

                return view('reports.documentation',compact('destination','country','expenses','normal','track','agent','destiny_client','destiny_dest','destiny_line','transporter','driver'));
            }
            /**
             *
             * If you select From one date only
             **/

            else  if(($from  !="") AND ($to ==""))
            {

                $track   = Track::select('tracks.*','bill_lading','organization_name','loading_date','transporter_name','cont_no','driver_name','trucking_number','driver_license','driver_contact','driver_passport')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('containers','containers.ladding_id','=','laddings.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->join('transporters','transporters.container_id','=','containers.id')
                    ->join('drivers','drivers.container_id','=','containers.id')
                    ->latest('tracks.document_date_received')
                    ->whereBetween(DB::raw('YEAR(tracks.created_at)'),[$date_from, $date_to])
                    ->where('tracks.document_date_received','>=', $from)
                    ->get();
                $agent          = Agent::with('user')->get();
                return view('reports.documentation',compact('destination','country','expenses','normal','track','agent','destiny_client','destiny_dest','destiny_line','transporter','driver'));
            }

            /**
             *
             * If you select To the last date
             *
             **/

            else  if(($from  =="") AND ($to !=""))
            {

                $track = Track::select('tracks.*')
                    ->whereBetween(DB::raw('YEAR(tracks.created_at)'),[$date_from, $date_to])
                    ->where('tracks.document_date_received','<=', $to)
                    ->get();
                $agent          = Agent::with('user')->get();
                return view('reports.documentation',compact('destination','country','expenses','normal','track','agent','transporter','destiny_client','destiny_dest','destiny_line','driver'));
            }
            /**
             *
             * If you don't select anything
             *
             **/

            else
            {
                $track = Track::select('tracks.*')
                    ->whereBetween(DB::raw('YEAR(tracks.created_at)'),[$date_from, $date_to])
                    ->get();
                $agent          = Agent::with('user')->get();
                return view('reports.documentation',compact('destination','country','expenses','normal','track','agent','destiny_client','destiny_dest','destiny_line','transporter','driver'));
            }
        }
        elseif ($user->hasRole('agent'))
        {


            $container_id = $request->get('container_id');
            $from   =  $request->input('date1');
            $to     =  $request->input('date2');
//            /**
//             *
//             * If you select From one date  to another Date
//             **/
//
//            if(($from  !="") AND ($to !="" ) )
//            {
//
//                $track   = Track::select('tracks.*','bill_lading','organization_name')
//                    ->join('laddings','laddings.track_id','=','tracks.id')
//                    ->join('agents','agents.id','=','tracks.agent_id')
//                      ->latest('tracks.document_date_received')
//                    ->where('date' ,'>=', $from)
//                    ->where('date', '<=', $to)
//                    ->where('agents.user_id','=',Auth::id())
//                    ->get();
//
//                $agent          = Agent::with('user')->get();
//                $user_id = Auth::id();
//                $profile = User::where('id','=',$user_id)->get();
//                return view('agent.reports',compact('track','agent','destiny','transporter','profile'));
//            }
//            /**
//             *
//             * If you select From one date only
//             **/
//
//            else  if(($from  !="") AND ($to ==""))
//            {
//
//                $track   = Track::select('tracks.*','bill_lading')
//                    ->join('laddings','laddings.track_id','=','tracks.id')
//                    ->join('agents','agents.id','=','tracks.agent_id')
//                      ->latest('tracks.document_date_received')
//                    ->where('date' ,'>=', $from)
//                    ->where('agents.user_id','=',Auth::id())
//                    ->get();
//                $agent          = Agent::with('user')->get();
//                $user_id = Auth::id();
//                $profile = User::where('id','=',$user_id)->get();
//                return view('agent.reports',compact('track','agent','destiny','transporter','profile'));
//            }
//
//            /**
//             *
//             * If you select To the last date
//             *
//             **/
//
//            else  if(($from  =="") AND ($to !=""))
//            {
//
//                $track = Track::select('tracks.*','bill_lading')
//                    ->join('laddings','laddings.track_id','=','tracks.id')
//                    ->join('agents','agents.id','=','tracks.agent_id')
//                    ->where('date' ,'<=', $to)
//                    ->where('agents.user_id','=',Auth::id())
//                      ->latest('tracks.document_date_received')
//                    ->get();
//                $agent          = Agent::with('user')->get();
//
//                $user_id = Auth::id();
//                $profile = User::where('id','=',$user_id)->get();
//                return view('agent.reports',compact('track','agent','transporter','destiny','profile'));
//            }
//            /**
//             *
//             * If you don't select anything
//             *
//             **/
//
//            else
//            {
//
//                $track = Track::select('tracks.*','bill_lading')
//                    ->join('laddings','laddings.track_id','=','tracks.id')
//                    ->join('agents','agents.id','=','tracks.agent_id')
//                    ->where('agents.user_id','=',Auth::id())
//                    ->get();
//                $agent          = Agent::with('user')->get();
//                $user_id = Auth::id();
//                $profile = User::where('id','=',$user_id)->get();
//                return view('agent.reports',compact('track','agent','transporter','destiny','profile','profile'));
//            }


            $user_id = Auth::id();
            $profile = User::where('id','=',$user_id)->get();
            $track   = Track::select('tracks.*','bill_lading','organization_name','cont_no')
                            ->join('laddings','laddings.track_id','=','tracks.id')
                            ->join('containers','containers.ladding_id','=','laddings.id')
                            ->join('agents','agents.id','=','tracks.agent_id')
                            ->where('containers.id','=',$container_id)
//                            ->whereBetween(DB::raw('YEAR(tracks.created_at)'),[$from, $to])
                            ->latest('tracks.created_at')
                            ->get();


            $container_no   = Container::select('containers.*')
                            ->join('laddings','laddings.id','=','containers.ladding_id')
                            ->join('tracks','tracks.id','=','laddings.track_id')
                            ->join('agents','agents.id','=','tracks.agent_id')
                            ->where('agents.user_id','=',Auth::id())
                            ->whereBetween(DB::raw('YEAR(tracks.created_at)'),[$date_from, $date_to])
                            ->latest('tracks.created_at')
                            ->get();

            $destinations = Track::select('tracks.*','bill_lading','organization_name')
                            ->join('laddings','laddings.track_id','=','tracks.id')
                            ->join('agents','agents.id','=','tracks.agent_id')
                            ->whereBetween(DB::raw('YEAR(tracks.created_at)'),[$date_from, $date_to])
                            ->latest("tracks.created_at")
                            ->get();


            $agent = Agent::with('user')->get();
            return view('agent.reports', compact('destination','country','track','container_no','destinations','agent','profile'));


        }
        else{
            return JsonResponse::create(['error' => 'access-denied'],401);
        }

    }


    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|static
     */
    public function ReportTransporter(Request $request)
    {
        $destination = Destination::all();
        $country = DB::table("countries")->pluck("country_name","id");
        $from = '2021';
        $to = '2030';
        $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
        $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

        $desk = $request->get('transporter_name');
        $user = Auth::user();
        if ($user->hasRole('admin'))
        {
            $destiny_line = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->line_vessel;
            });
            $destiny_client = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->client_name;
            });
            $destiny_dest = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->destination;
            });
            $track      = Track::select('tracks.*','bill_lading','organization_name','loading_date','transporter_name','driver_name','cont_no','trucking_number','driver_license','driver_contact','driver_passport')
                ->join('laddings','laddings.track_id','=','tracks.id')
                ->join('agents','agents.id','=','tracks.agent_id')
                ->join('containers','containers.ladding_id','=','laddings.id')
                ->join('transporters','transporters.container_id','=','containers.id')
                ->join('drivers','drivers.container_id','=','containers.id')
                ->latest('tracks.document_date_received')
                ->where('transporters.transporter_name','LIKE',$desk)
                ->whereBetween(DB::raw('YEAR(tracks.created_at)'),[$from, $to])
                ->get();



            $transporter = Transporter::select('transporters.*','cont_no','bill_lading')
                ->join('containers','containers.id','=','transporters.container_id')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->whereBetween(DB::raw('YEAR(transporters.created_at)'),[$from, $to])
                ->orderByDesc("created_at")
                ->get()->groupBy(function($item) {
                    return $item->transporter_name;
                });

            $driver = Driver::select('drivers.*','cont_no','bill_lading')
                ->join('containers','containers.id','=','drivers.container_id')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->whereBetween(DB::raw('YEAR(drivers.created_at)'),[$from, $to])
                ->orderByDesc("created_at")
                ->get()->groupBy(function($item) {
                    return $item->driver_name;
                });

            $agent          = Agent::with('user')->get();

            return view('reports.reports',compact('destination','country','expenses','normal','track','driver','agent','destiny_client','destiny_dest','destiny_line','transporter'));
        }
        else  if ($user->hasRole('finance'))
        {
            $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
            $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

            $destiny_line = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->line_vessel;
            });
            $destiny_client = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->client_name;
            });
            $destiny_dest = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->destination;
            });
            $track      = Track::select('tracks.*','bill_lading','organization_name','loading_date','transporter_name','driver_name','cont_no','trucking_number','driver_license','driver_contact','driver_passport')
                ->join('laddings','laddings.track_id','=','tracks.id')
                ->join('agents','agents.id','=','tracks.agent_id')
                ->join('containers','containers.ladding_id','=','laddings.id')
                ->join('transporters','transporters.container_id','=','containers.id')
                ->join('drivers','drivers.container_id','=','containers.id')
                ->latest('tracks.document_date_received')
                ->whereBetween(DB::raw('YEAR(tracks.created_at)'),[$from, $to])
                ->where('transporters.transporter_name','LIKE',$desk)
                ->get();

            $transporter = Transporter::select('transporters.*','cont_no','bill_lading')
                ->join('containers','containers.id','=','transporters.container_id')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->whereBetween(DB::raw('YEAR(transporters.created_at)'),[$from, $to])
                ->orderByDesc("created_at")
                ->get()->groupBy(function($item) {
                    return $item->transporter_name;
                });

            $driver = Driver::select('drivers.*','cont_no','bill_lading')
                ->join('containers','containers.id','=','drivers.container_id')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->whereBetween(DB::raw('YEAR(drivers.created_at)'),[$from, $to])
                ->orderByDesc("created_at")
                ->get()->groupBy(function($item) {
                    return $item->driver_name;
                });

            $agent          = Agent::with('user')->get();

            return view('reports.finance_reports',compact('destination','country','expenses','normal','track','driver','agent','destiny_client','destiny_dest','destiny_line','transporter'));
        }
        elseif ($user->hasRole('super'))
        {
            $expenses = Expenses::with('container')->where('finance_status','LIKE','pending')->count();

            $normal = NormalExpenses::where('finance_status','LIKE','pending')->count();

            $destiny_line = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->line_vessel;
            });
            $destiny_client = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->client_name;
            });
            $destiny_dest = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->destination;
            });
            $track      = Track::select('tracks.*','bill_lading','organization_name','loading_date','transporter_name','driver_name','cont_no','trucking_number','driver_license','driver_contact','driver_passport')
                ->join('laddings','laddings.track_id','=','tracks.id')
                ->join('agents','agents.id','=','tracks.agent_id')
                ->join('containers','containers.ladding_id','=','laddings.id')
                ->join('transporters','transporters.container_id','=','containers.id')
                ->join('drivers','drivers.container_id','=','containers.id')
                ->latest('tracks.document_date_received')
                ->where('transporters.transporter_name','LIKE',$desk)
                ->get();

            $transporter = Transporter::select('transporters.*','cont_no','bill_lading')
                ->join('containers','containers.id','=','transporters.container_id')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->orderByDesc("created_at")
                ->get()->groupBy(function($item) {
                    return $item->transporter_name;
                });

            $driver = Driver::select('drivers.*','cont_no','bill_lading')
                ->join('containers','containers.id','=','drivers.container_id')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->orderByDesc("created_at")
                ->get()->groupBy(function($item) {
                    return $item->driver_name;
                });

            $agent          = Agent::with('user')->get();

            return view('reports.super_reports',compact('destination','country','expenses','normal','track','driver','agent','destiny_client','destiny_dest','destiny_line','transporter'));
        }
        elseif ($user->hasRole('sales'))
        {
            $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
            $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

            $destiny_line = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->line_vessel;
            });
            $destiny_client = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->client_name;
            });
            $destiny_dest = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->destination;
            });
            $track      = Track::select('tracks.*','bill_lading','organization_name','loading_date','transporter_name','driver_name','cont_no','trucking_number','driver_license','driver_contact','driver_passport')
                ->join('laddings','laddings.track_id','=','tracks.id')
                ->join('agents','agents.id','=','tracks.agent_id')
                ->join('containers','containers.ladding_id','=','laddings.id')
                ->join('transporters','transporters.container_id','=','containers.id')
                ->join('drivers','drivers.container_id','=','containers.id')
                ->latest('tracks.document_date_received')
                ->whereBetween(DB::raw('YEAR(tracks.created_at)'),[$from, $to])
                ->where('transporters.transporter_name','LIKE',$desk)
                ->get();

            $transporter = Transporter::select('transporters.*','cont_no','bill_lading')
                ->join('containers','containers.id','=','transporters.container_id')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->whereBetween(DB::raw('YEAR(transporters.created_at)'),[$from, $to])
                ->orderByDesc("created_at")
                ->get()->groupBy(function($item) {
                    return $item->transporter_name;
                });

            $driver = Driver::select('drivers.*','cont_no','bill_lading')
                ->join('containers','containers.id','=','drivers.container_id')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->whereBetween(DB::raw('YEAR(drivers.created_at)'),[$from, $to])
                ->orderByDesc("created_at")
                ->get()->groupBy(function($item) {
                    return $item->driver_name;
                });

            $agent          = Agent::with('user')->get();

            return view('reports.sales_reports',compact('destination','country','expenses','normal','track','driver','agent','destiny_client','destiny_dest','destiny_line','transporter'));
        }
        elseif ($user->hasRole('shipping'))
        {
            $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
            $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

            $destiny_line = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->line_vessel;
            });
            $destiny_client = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->client_name;
            });
            $destiny_dest = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->destination;
            });
            $track      = Track::select('tracks.*','bill_lading','organization_name','loading_date','transporter_name','driver_name','cont_no','trucking_number','driver_license','driver_contact','driver_passport')
                ->join('laddings','laddings.track_id','=','tracks.id')
                ->join('agents','agents.id','=','tracks.agent_id')
                ->join('containers','containers.ladding_id','=','laddings.id')
                ->join('transporters','transporters.container_id','=','containers.id')
                ->join('drivers','drivers.container_id','=','containers.id')
                ->latest('tracks.document_date_received')
                ->whereBetween(DB::raw('YEAR(tracks.created_at)'),[$from, $to])
                ->where('transporters.transporter_name','LIKE',$desk)
                ->get();

            $transporter = Transporter::select('transporters.*','cont_no','bill_lading')
                ->join('containers','containers.id','=','transporters.container_id')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->whereBetween(DB::raw('YEAR(transporters.created_at)'),[$from, $to])
                ->orderByDesc("created_at")
                ->get()->groupBy(function($item) {
                    return $item->transporter_name;
                });

            $driver = Driver::select('drivers.*','cont_no','bill_lading')
                ->join('containers','containers.id','=','drivers.container_id')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->whereBetween(DB::raw('YEAR(drivers.created_at)'),[$from, $to])
                ->orderByDesc("created_at")
                ->get()->groupBy(function($item) {
                    return $item->driver_name;
                });

            $agent          = Agent::with('user')->get();

            return view('reports.shipping',compact('destination','country','expenses','normal','track','driver','agent','destiny_client','destiny_dest','destiny_line','transporter'));
        }
        elseif ($user->hasRole('declaration'))
        {
            $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
            $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

            $destiny_line = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->line_vessel;
            });
            $destiny_client = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->client_name;
            });
            $destiny_dest = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->destination;
            });
            $track      = Track::select('tracks.*','bill_lading','organization_name','loading_date','transporter_name','driver_name','cont_no','trucking_number','driver_license','driver_contact','driver_passport')
                ->join('laddings','laddings.track_id','=','tracks.id')
                ->join('agents','agents.id','=','tracks.agent_id')
                ->join('containers','containers.ladding_id','=','laddings.id')
                ->join('transporters','transporters.container_id','=','containers.id')
                ->join('drivers','drivers.container_id','=','containers.id')
                ->latest('tracks.document_date_received')
                ->whereBetween(DB::raw('YEAR(tracks.created_at)'),[$from, $to])
                ->where('transporters.transporter_name','LIKE',$desk)
                ->get();

            $transporter = Transporter::select('transporters.*','cont_no','bill_lading')
                ->join('containers','containers.id','=','transporters.container_id')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->whereBetween(DB::raw('YEAR(transporters.created_at)'),[$from, $to])
                ->orderByDesc("created_at")
                ->get()->groupBy(function($item) {
                    return $item->transporter_name;
                });

            $driver = Driver::select('drivers.*','cont_no','bill_lading')
                ->join('containers','containers.id','=','drivers.container_id')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->whereBetween(DB::raw('YEAR(drivers.created_at)'),[$from, $to])
                ->orderByDesc("created_at")
                ->get()->groupBy(function($item) {
                    return $item->driver_name;
                });

            $agent          = Agent::with('user')->get();

            return view('reports.declaration',compact('destination','country','expenses','normal','track','driver','agent','destiny_client','destiny_dest','destiny_line','transporter'));
        }
        elseif ($user->hasRole('documentation'))
        {
            $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
            $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

            $destiny_line = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->line_vessel;
            });
            $destiny_client = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->client_name;
            });
            $destiny_dest = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->destination;
            });
            $track      = Track::select('tracks.*','bill_lading','organization_name','loading_date','transporter_name','driver_name','cont_no','trucking_number','driver_license','driver_contact','driver_passport')
                ->join('laddings','laddings.track_id','=','tracks.id')
                ->join('agents','agents.id','=','tracks.agent_id')
                ->join('containers','containers.ladding_id','=','laddings.id')
                ->join('transporters','transporters.container_id','=','containers.id')
                ->join('drivers','drivers.container_id','=','containers.id')
                ->latest('tracks.document_date_received')
                ->whereBetween(DB::raw('YEAR(tracks.created_at)'),[$from, $to])
                ->where('transporters.transporter_name','LIKE',$desk)
                ->get();

            $transporter = Transporter::select('transporters.*','cont_no','bill_lading')
                ->join('containers','containers.id','=','transporters.container_id')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->whereBetween(DB::raw('YEAR(transporters.created_at)'),[$from, $to])
                ->orderByDesc("created_at")
                ->get()->groupBy(function($item) {
                    return $item->transporter_name;
                });

            $driver = Driver::select('drivers.*','cont_no','bill_lading')
                ->join('containers','containers.id','=','drivers.container_id')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->whereBetween(DB::raw('YEAR(drivers.created_at)'),[$from, $to])
                ->orderByDesc("created_at")
                ->get()->groupBy(function($item) {
                    return $item->driver_name;
                });

            $agent          = Agent::with('user')->get();

            return view('reports.documentation',compact('destination','country','expenses','normal','track','driver','agent','destiny_client','destiny_dest','destiny_line','transporter'));
        }
        elseif ($user->hasRole('agent'))
        {
            $track   = Track::select('tracks.*','bill_lading','organization_name','loading_date','transporter_name','driver_name','cont_no','trucking_number','driver_license','driver_contact','driver_passport')
                ->join('laddings','laddings.track_id','=','tracks.id')
                ->join('agents','agents.id','=','tracks.agent_id')
                ->join('containers','containers.ladding_id','=','laddings.id')
                ->join('transporters','transporters.container_id','=','containers.id')
                ->join('drivers','drivers.container_id','=','containers.id')
                ->latest('tracks.document_date_received')
                ->where('transporters.transporter_name','LIKE',$desk)
                ->whereBetween(DB::raw('YEAR(tracks.created_at)'),[$from, $to])
                ->get();
            $agent   = Agent::with('user')->get();

            $transporter = Transporter::select('transporters.*','cont_no','bill_lading')
                ->join('containers','containers.id','=','transporters.container_id')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->whereBetween(DB::raw('YEAR(transporters.created_at)'),[$from, $to])
                ->orderByDesc("created_at")
                ->get()->groupBy(function($item) {
                    return $item->transporter_name;
                });

            $driver = Driver::select('drivers.*','cont_no','bill_lading')
                ->join('containers','containers.id','=','drivers.container_id')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->whereBetween(DB::raw('YEAR(drivers.created_at)'),[$from, $to])
                ->orderByDesc("created_at")
                ->get()->groupBy(function($item) {
                    return $item->driver_name;
                });

            return view('agent.reports',compact('destination','country','track','driver','agent','transporter'));
        }
        else{
            return JsonResponse::create(['error' => 'access-denied'],401);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|static
     */
    public function sortDriver(Request $request)
    {
        $destination = Destination::all();
        $country = DB::table("countries")->pluck("country_name","id");
        $from = '2021';
        $to = '2030';
        $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
        $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

        $desk = $request->get('driver_name');
        $user = Auth::user();
        if ($user->hasRole('admin'))
        {
            $destiny_line = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->line_vessel;
            });
            $destiny_client = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->client_name;
            });
            $destiny_dest = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->destination;
            });
            $track      = Track::select('tracks.*','bill_lading','organization_name','loading_date','transporter_name','cont_no','driver_name','trucking_number','driver_license','driver_contact','driver_passport')
                ->join('laddings','laddings.track_id','=','tracks.id')
                ->join('agents','agents.id','=','tracks.agent_id')
                ->join('containers','containers.ladding_id','=','laddings.id')
                ->leftjoin('transporters','transporters.container_id','=','containers.id')
                ->leftjoin('drivers','drivers.container_id','=','containers.id')
                ->whereBetween(DB::raw('YEAR(tracks.created_at)'),[$from, $to])
                ->where('drivers.driver_name','LIKE',$desk)
                ->latest('tracks.document_date_received')
                ->get();

            $transporter = Transporter::select('transporters.*','cont_no','bill_lading')
                ->join('containers','containers.id','=','transporters.container_id')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->whereBetween(DB::raw('YEAR(transporters.created_at)'),[$from, $to])
                ->orderByDesc("created_at")
                ->get()->groupBy(function($item) {
                    return $item->transporter_name;
                });

            $driver = Driver::select('drivers.*','cont_no','bill_lading')
                ->join('containers','containers.id','=','drivers.container_id')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->whereBetween(DB::raw('YEAR(drivers.created_at)'),[$from, $to])
                ->orderByDesc("created_at")
                ->get()->groupBy(function($item) {
                    return $item->driver_name;
                });

            $agent          = Agent::with('user')->get();

            return view('reports.reports',compact('destination','country','expenses','normal','track','agent','destiny_client','destiny_dest','destiny_line','transporter','driver'));
        }
        elseif ($user->hasRole('finance'))
        {
            $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
            $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

            $destiny_line = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->line_vessel;
            });
            $destiny_client = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->client_name;
            });
            $destiny_dest = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->destination;
            });
            $track      = Track::select('tracks.*','bill_lading','organization_name','loading_date','transporter_name','cont_no','driver_name','trucking_number','driver_license','driver_contact','driver_passport')
                ->join('laddings','laddings.track_id','=','tracks.id')
                ->join('agents','agents.id','=','tracks.agent_id')
                ->join('containers','containers.ladding_id','=','laddings.id')
                ->join('transporters','transporters.container_id','=','containers.id')
                ->join('drivers','drivers.container_id','=','containers.id')
                ->latest('tracks.document_date_received')
                ->whereBetween(DB::raw('YEAR(tracks.created_at)'),[$from, $to])
                ->where('drivers.driver_name','LIKE',$desk)
                ->get();

            $transporter = Transporter::select('transporters.*','cont_no','bill_lading')
                ->join('containers','containers.id','=','transporters.container_id')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->whereBetween(DB::raw('YEAR(transporters.created_at)'),[$from, $to])
                ->orderByDesc("created_at")
                ->get()->groupBy(function($item) {
                    return $item->transporter_name;
                });

            $driver = Driver::select('drivers.*','cont_no','bill_lading')
                ->join('containers','containers.id','=','drivers.container_id')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->whereBetween(DB::raw('YEAR(drivers.created_at)'),[$from, $to])
                ->orderByDesc("created_at")
                ->get()->groupBy(function($item) {
                    return $item->driver_name;
                });

            $agent          = Agent::with('user')->get();

            return view('reports.finance_reports',compact('destination','country','expenses','normal','track','agent','destiny_client','destiny_dest','destiny_line','transporter','driver'));
        }
        elseif ($user->hasRole('super'))
        {
            $expenses = Expenses::with('container')->where('finance_status','LIKE','pending')->count();

            $normal = NormalExpenses::where('finance_status','LIKE','pending')->count();

            $destiny_line = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->line_vessel;
            });
            $destiny_client = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->client_name;
            });
            $destiny_dest = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->destination;
            });
            $track      = Track::select('tracks.*','bill_lading','organization_name','loading_date','transporter_name','cont_no','driver_name','trucking_number','driver_license','driver_contact','driver_passport')
                ->join('laddings','laddings.track_id','=','tracks.id')
                ->join('agents','agents.id','=','tracks.agent_id')
                ->join('containers','containers.ladding_id','=','laddings.id')
                ->join('transporters','transporters.container_id','=','containers.id')
                ->join('drivers','drivers.container_id','=','containers.id')
                ->latest('tracks.document_date_received')
                ->where('drivers.driver_name','LIKE',$desk)
                ->get();

            $transporter = Transporter::select('transporters.*','cont_no','bill_lading')
                ->join('containers','containers.id','=','transporters.container_id')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->orderByDesc("created_at")
                ->get()->groupBy(function($item) {
                    return $item->transporter_name;
                });

            $driver = Driver::select('drivers.*','cont_no','bill_lading')
                ->join('containers','containers.id','=','drivers.container_id')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->orderByDesc("created_at")
                ->get()->groupBy(function($item) {
                    return $item->driver_name;
                });

            $agent          = Agent::with('user')->get();

            return view('reports.super_reports',compact('destination','country','expenses','normal','track','agent','destiny_client','destiny_dest','destiny_line','transporter','driver'));
        }
        elseif ($user->hasRole('sales'))
        {
            $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
            $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

            $destiny_line = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->line_vessel;
            });
            $destiny_client = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->client_name;
            });
            $destiny_dest = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->destination;
            });
            $track      = Track::select('tracks.*','bill_lading','organization_name','loading_date','transporter_name','cont_no','driver_name','trucking_number','driver_license','driver_contact','driver_passport')
                ->join('laddings','laddings.track_id','=','tracks.id')
                ->join('agents','agents.id','=','tracks.agent_id')
                ->join('containers','containers.ladding_id','=','laddings.id')
                ->join('transporters','transporters.container_id','=','containers.id')
                ->join('drivers','drivers.container_id','=','containers.id')
                ->latest('tracks.document_date_received')
                ->whereBetween(DB::raw('YEAR(tracks.created_at)'),[$from, $to])
                ->where('drivers.driver_name','LIKE',$desk)
                ->get();

            $transporter = Transporter::select('transporters.*','cont_no','bill_lading')
                ->join('containers','containers.id','=','transporters.container_id')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->whereBetween(DB::raw('YEAR(transporters.created_at)'),[$from, $to])
                ->orderByDesc("created_at")
                ->get()->groupBy(function($item) {
                    return $item->transporter_name;
                });

            $driver = Driver::select('drivers.*','cont_no','bill_lading')
                ->join('containers','containers.id','=','drivers.container_id')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->whereBetween(DB::raw('YEAR(drivers.created_at)'),[$from, $to])
                ->orderByDesc("created_at")
                ->get()->groupBy(function($item) {
                    return $item->driver_name;
                });

            $agent          = Agent::with('user')->get();

            return view('reports.sales_reports',compact('destination','country','expenses','normal','track','agent','destiny_client','destiny_dest','destiny_line','transporter','driver'));
        }
        elseif ($user->hasRole('shipping'))
        {
            $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
            $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

            $destiny_line = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->line_vessel;
            });
            $destiny_client = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->client_name;
            });
            $destiny_dest = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->destination;
            });
            $track      = Track::select('tracks.*','bill_lading','organization_name','loading_date','transporter_name','cont_no','driver_name','trucking_number','driver_license','driver_contact','driver_passport')
                ->join('laddings','laddings.track_id','=','tracks.id')
                ->join('agents','agents.id','=','tracks.agent_id')
                ->join('containers','containers.ladding_id','=','laddings.id')
                ->join('transporters','transporters.container_id','=','containers.id')
                ->join('drivers','drivers.container_id','=','containers.id')
                ->latest('tracks.document_date_received')
                ->whereBetween(DB::raw('YEAR(tracks.created_at)'),[$from, $to])
                ->where('drivers.driver_name','LIKE',$desk)
                ->get();

            $transporter = Transporter::select('transporters.*','cont_no','bill_lading')
                ->join('containers','containers.id','=','transporters.container_id')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->whereBetween(DB::raw('YEAR(transporters.created_at)'),[$from, $to])
                ->orderByDesc("created_at")
                ->get()->groupBy(function($item) {
                    return $item->transporter_name;
                });

            $driver = Driver::select('drivers.*','cont_no','bill_lading')
                ->join('containers','containers.id','=','drivers.container_id')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->whereBetween(DB::raw('YEAR(drivers.created_at)'),[$from, $to])
                ->orderByDesc("created_at")
                ->get()->groupBy(function($item) {
                    return $item->driver_name;
                });

            $agent          = Agent::with('user')->get();

            return view('reports.shipping',compact('destination','country','expenses','normal','track','agent','destiny_client','destiny_dest','destiny_line','transporter','driver'));
        }
        elseif ($user->hasRole('declaration'))
        {
            $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
            $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

            $destiny_line = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->line_vessel;
            });
            $destiny_client = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->client_name;
            });
            $destiny_dest = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->destination;
            });
            $track      = Track::select('tracks.*','bill_lading','organization_name','loading_date','transporter_name','cont_no','driver_name','trucking_number','driver_license','driver_contact','driver_passport')
                ->join('laddings','laddings.track_id','=','tracks.id')
                ->join('agents','agents.id','=','tracks.agent_id')
                ->join('containers','containers.ladding_id','=','laddings.id')
                ->join('transporters','transporters.container_id','=','containers.id')
                ->join('drivers','drivers.container_id','=','containers.id')
                ->latest('tracks.document_date_received')
                ->whereBetween(DB::raw('YEAR(tracks.created_at)'),[$from, $to])
                ->where('drivers.driver_name','LIKE',$desk)
                ->get();

            $transporter = Transporter::select('transporters.*','cont_no','bill_lading')
                ->join('containers','containers.id','=','transporters.container_id')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->whereBetween(DB::raw('YEAR(transporters.created_at)'),[$from, $to])
                ->orderByDesc("created_at")
                ->get()->groupBy(function($item) {
                    return $item->transporter_name;
                });

            $driver = Driver::select('drivers.*','cont_no','bill_lading')
                ->join('containers','containers.id','=','drivers.container_id')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->whereBetween(DB::raw('YEAR(drivers.created_at)'),[$from, $to])
                ->orderByDesc("created_at")
                ->get()->groupBy(function($item) {
                    return $item->driver_name;
                });

            $agent          = Agent::with('user')->get();

            return view('reports.declaration',compact('destination','country','expenses','normal','track','agent','destiny_client','destiny_dest','destiny_line','transporter','driver'));
        }
        elseif ($user->hasRole('documentation'))
        {
            $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
            $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

            $destiny_line = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->line_vessel;
            });
            $destiny_client = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->client_name;
            });
            $destiny_dest = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->destination;
            });
            $track      = Track::select('tracks.*','bill_lading','organization_name','loading_date','transporter_name','cont_no','driver_name','trucking_number','driver_license','driver_contact','driver_passport')
                ->join('laddings','laddings.track_id','=','tracks.id')
                ->join('agents','agents.id','=','tracks.agent_id')
                ->join('containers','containers.ladding_id','=','laddings.id')
                ->join('transporters','transporters.container_id','=','containers.id')
                ->join('drivers','drivers.container_id','=','containers.id')
                ->latest('tracks.document_date_received')
                ->whereBetween(DB::raw('YEAR(tracks.created_at)'),[$from, $to])
                ->where('drivers.driver_name','LIKE',$desk)
                ->get();

            $transporter = Transporter::select('transporters.*','cont_no','bill_lading')
                ->join('containers','containers.id','=','transporters.container_id')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->whereBetween(DB::raw('YEAR(transporters.created_at)'),[$from, $to])
                ->orderByDesc("created_at")
                ->get()->groupBy(function($item) {
                    return $item->transporter_name;
                });

            $driver = Driver::select('drivers.*','cont_no','bill_lading')
                ->join('containers','containers.id','=','drivers.container_id')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->whereBetween(DB::raw('YEAR(drivers.created_at)'),[$from, $to])
                ->orderByDesc("created_at")
                ->get()->groupBy(function($item) {
                    return $item->driver_name;
                });

            $agent          = Agent::with('user')->get();

            return view('reports.documentation',compact('destination','country','expenses','normal','track','agent','destiny_client','destiny_dest','destiny_line','transporter','driver'));
        }
        elseif ($user->hasRole('agent'))
        {
            $track   = Track::select('tracks.*','bill_lading','organization_name','loading_date','transporter_name','driver_name','cont_no','trucking_number','driver_license','driver_contact','driver_passport')
                ->join('laddings','laddings.track_id','=','tracks.id')
                ->join('agents','agents.id','=','tracks.agent_id')
                ->join('transporters','transporters.container_id','=','containers.id')
                ->join('drivers','drivers.container_id','=','containers.id')
                ->latest('tracks.document_date_received')
                ->whereBetween(DB::raw('YEAR(tracks.created_at)'),[$from, $to])
                ->where('drivers.driver_name','LIKE',$desk)
                ->get();

            $agent   = Agent::with('user')->get();

            $transporter = Transporter::select('transporters.*','cont_no','bill_lading')
                ->join('containers','containers.id','=','transporters.container_id')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->whereBetween(DB::raw('YEAR(transporters.created_at)'),[$from, $to])
                ->orderByDesc("created_at")
                ->get()->groupBy(function($item) {
                    return $item->transporter_name;
                });

            $driver = Driver::select('drivers.*','cont_no','bill_lading')
                ->join('containers','containers.id','=','drivers.container_id')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->whereBetween(DB::raw('YEAR(drivers.created_at)'),[$from, $to])
                ->orderByDesc("created_at")
                ->get()->groupBy(function($item) {
                    return $item->driver_name;
                });

            return view('agent.reports',compact('destination','country','track','agent','transporter','driver'));
        }
        else{
            return JsonResponse::create(['error' => 'access-denied'],401);
        }
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|static
     * Reports Controller per agent
     */


    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|static
     */
    public  function sortClient(Request $request)
    {
        $destination = Destination::all();
        $country = DB::table("countries")->pluck("country_name","id");
        $from = '2021';
        $to = '2030';
        $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
        $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

        $desk = $request->get('client_name');
        $user = Auth::user();
        if ($user->hasRole('admin')){

            $destiny_line = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->line_vessel;
            });
            $destiny_client = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->client_name;
            });
            $destiny_dest = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->destination;
            });
            $track   = Track::select('tracks.*','bill_lading','organization_name')
                ->join('laddings','laddings.track_id','=','tracks.id')
                ->join('containers','containers.ladding_id','=','laddings.id')
                ->join('agents','agents.id','=','tracks.agent_id')
//                ->innerjoin('transporters','transporters.container_id','=','containers.id')
//                ->innerjoin('drivers','drivers.container_id','=','containers.id')
                ->latest('tracks.document_date_received')
                ->whereBetween(DB::raw('YEAR(tracks.created_at)'),[$from, $to])
                ->where('tracks.client_name','LIKE',$desk)
                ->get();
            $agent          = Agent::with('user')->get();
            $transporter = Transporter::select('transporters.*','cont_no','bill_lading')
                ->join('containers','containers.id','=','transporters.container_id')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->whereBetween(DB::raw('YEAR(transporters.created_at)'),[$from, $to])
                ->orderByDesc("created_at")
                ->get()->groupBy(function($item) {
                    return $item->transporter_name;
                });

            $driver = Driver::select('drivers.*','cont_no','bill_lading')
                ->join('containers','containers.id','=','drivers.container_id')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->whereBetween(DB::raw('YEAR(drivers.created_at)'),[$from, $to])
                ->orderByDesc("created_at")
                ->get()->groupBy(function($item) {
                    return $item->driver_name;
                });

            return view('reports.reports',compact('destination','country','expenses','normal','track','agent','destiny_client','destiny_dest','destiny_line','transporter','driver'));
        }
        else if ($user->hasRole('finance')){

            $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
            $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

            $destiny_line = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->line_vessel;
            });
            $destiny_client = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->client_name;
            });
            $destiny_dest = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->destination;
            });
//            $track   = Track::select('tracks.*','bill_lading','organization_name','loading_date','transporter_name','cont_no','driver_name','trucking_number','driver_license','driver_contact','driver_passport')
            $track   = Track::select('tracks.*','bill_lading','organization_name')
                ->join('laddings','laddings.track_id','=','tracks.id')
                ->join('containers','containers.ladding_id','=','laddings.id')
//                ->join('agents','agents.id','=','tracks.agent_id')
//                ->join('transporters','transporters.container_id','=','containers.id')
                ->join('drivers','drivers.container_id','=','containers.id')
                ->latest('tracks.document_date_received')
                ->whereBetween(DB::raw('YEAR(tracks.created_at)'),[$from, $to])
                ->where('tracks.client_name','LIKE',$desk)
                ->get();
            $agent          = Agent::with('user')->get();
            $transporter = Transporter::select('transporters.*','cont_no','bill_lading')
                ->join('containers','containers.id','=','transporters.container_id')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->whereBetween(DB::raw('YEAR(transporters.created_at)'),[$from, $to])
                ->orderByDesc("created_at")
                ->get()->groupBy(function($item) {
                    return $item->transporter_name;
                });

            $driver = Driver::select('drivers.*','cont_no','bill_lading')
                ->join('containers','containers.id','=','drivers.container_id')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->whereBetween(DB::raw('YEAR(drivers.created_at)'),[$from, $to])
                ->orderByDesc("created_at")
                ->get()->groupBy(function($item) {
                    return $item->driver_name;
                });

            return view('reports.finance_reports',compact('destination','country','expenses','normal','track','agent','destiny_client','destiny_dest','destiny_line','transporter','driver'));
        }
        elseif ($user->hasRole('super')){

            $expenses = Expenses::with('container')->where('finance_status','LIKE','pending')->count();

            $normal = NormalExpenses::where('finance_status','LIKE','pending')->count();

            $destiny_line = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->line_vessel;
            });
            $destiny_client = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->client_name;
            });
            $destiny_dest = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->destination;
            });
            $track   = Track::select('tracks.*','bill_lading','organization_name')
                ->join('laddings','laddings.track_id','=','tracks.id')
//                ->join('containers','containers.ladding_id','=','laddings.id')
                ->join('agents','agents.id','=','tracks.agent_id')
//                ->join('transporters','transporters.container_id','=','containers.id')
//                ->join('drivers','drivers.container_id','=','containers.id')
                ->latest('tracks.document_date_received')
                ->where('tracks.client_name','LIKE',$desk)
                ->get();
            $agent          = Agent::with('user')->get();
            $transporter = Transporter::select('transporters.*','cont_no','bill_lading')
                ->join('containers','containers.id','=','transporters.container_id')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->orderByDesc("created_at")
                ->get()->groupBy(function($item) {
                    return $item->transporter_name;
                });

            $driver = Driver::select('drivers.*','cont_no','bill_lading')
                ->join('containers','containers.id','=','drivers.container_id')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->orderByDesc("created_at")
                ->get()->groupBy(function($item) {
                    return $item->driver_name;
                });

            return view('reports.super_reports',compact('destination','country','expenses','normal','track','agent','destiny_client','destiny_dest','destiny_line','transporter','driver'));
        }
        elseif ($user->hasRole('sales')){

            $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
            $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

            $destiny_line = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->line_vessel;
            });
            $destiny_client = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->client_name;
            });
            $destiny_dest = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->destination;
            });
            $track   = Track::select('tracks.*','bill_lading','organization_name')
                ->join('laddings','laddings.track_id','=','tracks.id')
//                ->join('containers','containers.ladding_id','=','laddings.id')
                ->join('agents','agents.id','=','tracks.agent_id')
//                ->join('transporters','transporters.container_id','=','containers.id')
//                ->join('drivers','drivers.container_id','=','containers.id')
                ->latest('tracks.document_date_received')
                ->whereBetween(DB::raw('YEAR(tracks.created_at)'),[$from, $to])
                ->where('tracks.client_name','LIKE',$desk)
                ->get();
            $agent          = Agent::with('user')->get();
            $transporter = Transporter::select('transporters.*','cont_no','bill_lading')
                ->join('containers','containers.id','=','transporters.container_id')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->whereBetween(DB::raw('YEAR(transporters.created_at)'),[$from, $to])
                ->orderByDesc("created_at")
                ->get()->groupBy(function($item) {
                    return $item->transporter_name;
                });

            $driver = Driver::select('drivers.*','cont_no','bill_lading')
                ->join('containers','containers.id','=','drivers.container_id')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->whereBetween(DB::raw('YEAR(drivers.created_at)'),[$from, $to])
                ->orderByDesc("created_at")
                ->get()->groupBy(function($item) {
                    return $item->driver_name;
                });
            return view('reports.sales_reports',compact('destination','country','expenses','normal','track','agent','destiny_client','destiny_dest','destiny_line','transporter','driver'));
        }
        elseif ($user->hasRole('shipping')){

            $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
            $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

            $destiny_line = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->line_vessel;
            });
            $destiny_client = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->client_name;
            });
            $destiny_dest = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->destination;
            });
            $track   = Track::select('tracks.*','bill_lading','organization_name')
                ->join('laddings','laddings.track_id','=','tracks.id')
//                ->join('containers','containers.ladding_id','=','laddings.id')
                ->join('agents','agents.id','=','tracks.agent_id')
//                ->join('transporters','transporters.container_id','=','containers.id')
//                ->join('drivers','drivers.container_id','=','containers.id')
                ->latest('tracks.document_date_received')
                ->whereBetween(DB::raw('YEAR(tracks.created_at)'),[$from, $to])
                ->where('tracks.client_name','LIKE',$desk)
                ->get();
            $agent          = Agent::with('user')->get();
            $transporter = Transporter::select('transporters.*','cont_no','bill_lading')
                ->join('containers','containers.id','=','transporters.container_id')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->whereBetween(DB::raw('YEAR(transporters.created_at)'),[$from, $to])
                ->orderByDesc("created_at")
                ->get()->groupBy(function($item) {
                    return $item->transporter_name;
                });

            $driver = Driver::select('drivers.*','cont_no','bill_lading')
                ->join('containers','containers.id','=','drivers.container_id')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->whereBetween(DB::raw('YEAR(drivers.created_at)'),[$from, $to])
                ->orderByDesc("created_at")
                ->get()->groupBy(function($item) {
                    return $item->driver_name;
                });

            return view('reports.shipping',compact('destination','country','expenses','normal','track','agent','destiny_client','destiny_dest','destiny_line','transporter','driver'));
        }
        elseif ($user->hasRole('declaration')){

            $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
            $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

            $destiny_line = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->line_vessel;
            });
            $destiny_client = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->client_name;
            });
            $destiny_dest = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->destination;
            });
            $track   = Track::select('tracks.*','bill_lading','organization_name')
                ->join('laddings','laddings.track_id','=','tracks.id')
//                ->join('containers','containers.ladding_id','=','laddings.id')
                ->join('agents','agents.id','=','tracks.agent_id')
//                ->join('transporters','transporters.container_id','=','containers.id')
//                ->join('drivers','drivers.container_id','=','containers.id')
                ->latest('tracks.document_date_received')
                ->whereBetween(DB::raw('YEAR(tracks.created_at)'),[$from, $to])
                ->where('tracks.client_name','LIKE',$desk)
                ->get();
            $agent          = Agent::with('user')->get();
            $transporter = Transporter::select('transporters.*','cont_no','bill_lading')
                ->join('containers','containers.id','=','transporters.container_id')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->whereBetween(DB::raw('YEAR(transporters.created_at)'),[$from, $to])
                ->orderByDesc("created_at")
                ->get()->groupBy(function($item) {
                    return $item->transporter_name;
                });

            $driver = Driver::select('drivers.*','cont_no','bill_lading')
                ->join('containers','containers.id','=','drivers.container_id')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->whereBetween(DB::raw('YEAR(drivers.created_at)'),[$from, $to])
                ->orderByDesc("created_at")
                ->get()->groupBy(function($item) {
                    return $item->driver_name;
                });

            return view('reports.declaration',compact('destination','country','expenses','normal','track','agent','destiny_client','destiny_dest','destiny_line','transporter','driver'));
        }
        elseif ($user->hasRole('documentation')){

            $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
            $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

            $destiny_line = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->line_vessel;
            });
            $destiny_client = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->client_name;
            });
            $destiny_dest = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->destination;
            });
            $track   = Track::select('tracks.*','bill_lading','organization_name')
                ->join('laddings','laddings.track_id','=','tracks.id')
//                ->join('containers','containers.ladding_id','=','laddings.id')
                ->join('agents','agents.id','=','tracks.agent_id')
//                ->join('transporters','transporters.container_id','=','containers.id')
//                ->join('drivers','drivers.container_id','=','containers.id')
                ->latest('tracks.document_date_received')
                ->whereBetween(DB::raw('YEAR(tracks.created_at)'),[$from, $to])
                ->where('tracks.client_name','LIKE',$desk)
                ->get();
            $agent          = Agent::with('user')->get();
            $transporter = Transporter::select('transporters.*','cont_no','bill_lading')
                ->join('containers','containers.id','=','transporters.container_id')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->whereBetween(DB::raw('YEAR(transporters.created_at)'),[$from, $to])
                ->orderByDesc("created_at")
                ->get()->groupBy(function($item) {
                    return $item->transporter_name;
                });

            $driver = Driver::select('drivers.*','cont_no','bill_lading')
                ->join('containers','containers.id','=','drivers.container_id')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->whereBetween(DB::raw('YEAR(drivers.created_at)'),[$from, $to])
                ->orderByDesc("created_at")
                ->get()->groupBy(function($item) {
                    return $item->driver_name;
                });

            return view('reports.documentation',compact('destination','country','expenses','normal','track','agent','destiny_client','destiny_dest','destiny_line','transporter','driver'));
        }
        elseif ($user->hasRole('agent'))
        {


            $track   = Track::select('tracks.*','bill_lading','organization_name')
                ->join('laddings','laddings.track_id','=','tracks.id')
//                ->join('containers','containers.ladding_id','=','laddings.id')
                ->join('agents','agents.id','=','tracks.agent_id')
//                ->join('transporters','transporters.container_id','=','containers.id')
//                ->join('drivers','drivers.container_id','=','containers.id')
                ->latest('tracks.document_date_received')
                ->whereBetween(DB::raw('YEAR(tracks.created_at)'),[$from, $to])
                ->where('tracks.client_name','LIKE',$desk)
                ->get();
            $agent          = Agent::with('user')->get();

            $transporter = Transporter::select('transporters.*','cont_no','bill_lading')
                ->join('containers','containers.id','=','transporters.container_id')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->whereBetween(DB::raw('YEAR(transporters.created_at)'),[$from, $to])
                ->orderByDesc("created_at")
                ->get()->groupBy(function($item) {
                    return $item->transporter_name;
                });

            $driver = Driver::select('drivers.*','cont_no','bill_lading')
                ->join('containers','containers.id','=','drivers.container_id')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->whereBetween(DB::raw('YEAR(drivers.created_at)'),[$from, $to])
                ->orderByDesc("created_at")
                ->get()->groupBy(function($item) {
                    return $item->driver_name;
                });

            return view('agent.reports',compact('destination','country','track','agent','transporter','driver'));
        }
        else{
            return JsonResponse::create(['error' => 'access-denied'],401);
        }
    }


    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|static
     */
    public  function sortVessel(Request $request)
    {
        $destination = Destination::all();
        $country = DB::table("countries")->pluck("country_name","id");
        $from = '2021';
        $to = '2030';
        $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
        $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

        $desk = $request->get('line_vessel');

        $user = Auth::user();
        if ($user->hasRole('admin')){


            $destiny_line = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->line_vessel;
            });
            $destiny_client = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->client_name;
            });
            $destiny_dest = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->destination;
            });
            $track   = Track::select('tracks.*','bill_lading','organization_name','loading_date','transporter_name','driver_name','cont_no','trucking_number','driver_license','driver_contact','driver_passport')
                ->join('laddings','laddings.track_id','=','tracks.id')
                ->join('containers','containers.ladding_id','=','laddings.id')
                ->join('agents','agents.id','=','tracks.agent_id')
                ->join('transporters','transporters.container_id','=','containers.id')
                ->join('drivers','drivers.container_id','=','containers.id')
                ->latest('tracks.document_date_received')
                ->whereBetween(DB::raw('YEAR(tracks.created_at)'),[$from, $to])
                ->where('tracks.line_vessel','LIKE',$desk)
                ->get();



            $agent          = Agent::with('user')->get();
            $transporter = Transporter::select('transporters.*','cont_no','bill_lading')
                ->join('containers','containers.id','=','transporters.container_id')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->whereBetween(DB::raw('YEAR(transporters.created_at)'),[$from, $to])
                ->orderByDesc("created_at")
                ->get()->groupBy(function($item) {
                    return $item->transporter_name;
                });


            $driver = Driver::select('drivers.*','cont_no','bill_lading')
                ->join('containers','containers.id','=','drivers.container_id')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->whereBetween(DB::raw('YEAR(drivers.created_at)'),[$from, $to])
                ->orderByDesc("created_at")
                ->get()->groupBy(function($item) {
                    return $item->driver_name;
                });

            return view('reports.reports',compact('destination','country','expenses','normal','track','agent','destiny_client','destiny_dest','destiny_line','transporter','driver'));
        }
        elseif ($user->hasRole('finance')){

            $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
            $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

            $destiny_line = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->line_vessel;
            });
            $destiny_client = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->client_name;
            });
            $destiny_dest = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->destination;
            });
            $track   = Track::select('tracks.*','bill_lading','organization_name','loading_date','transporter_name','driver_name','cont_no','trucking_number','driver_license','driver_contact','driver_passport')
                ->join('laddings','laddings.track_id','=','tracks.id')
                ->join('containers','containers.ladding_id','=','laddings.id')
                ->join('agents','agents.id','=','tracks.agent_id')
                ->join('transporters','transporters.container_id','=','containers.id')
                ->join('drivers','drivers.container_id','=','containers.id')
                ->latest('tracks.document_date_received')
                ->whereBetween(DB::raw('YEAR(tracks.created_at)'),[$from, $to])
                ->where('tracks.line_vessel','LIKE',$desk)
                ->get();



            $agent          = Agent::with('user')->get();
            $transporter = Transporter::select('transporters.*','cont_no','bill_lading')
                ->join('containers','containers.id','=','transporters.container_id')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->whereBetween(DB::raw('YEAR(transporters.created_at)'),[$from, $to])
                ->orderByDesc("created_at")
                ->get()->groupBy(function($item) {
                    return $item->transporter_name;
                });


            $driver = Driver::select('drivers.*','cont_no','bill_lading')
                ->join('containers','containers.id','=','drivers.container_id')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->whereBetween(DB::raw('YEAR(drivers.created_at)'),[$from, $to])
                ->orderByDesc("created_at")
                ->get()->groupBy(function($item) {
                    return $item->driver_name;
                });

            return view('reports.finance_reports',compact('destination','country','expenses','normal','track','agent','destiny_client','destiny_dest','destiny_line','transporter','driver'));
        }
        else if ($user->hasRole('super')){

            $expenses = Expenses::with('container')->where('finance_status','LIKE','pending')->count();

            $normal = NormalExpenses::where('finance_status','LIKE','pending')->count();

            $destiny_line = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->line_vessel;
            });
            $destiny_client = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->client_name;
            });
            $destiny_dest = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->destination;
            });
            $track   = Track::select('tracks.*','bill_lading','organization_name','loading_date','transporter_name','driver_name','cont_no','trucking_number','driver_license','driver_contact','driver_passport')
                ->join('laddings','laddings.track_id','=','tracks.id')
                ->join('containers','containers.ladding_id','=','laddings.id')
                ->join('agents','agents.id','=','tracks.agent_id')
                ->join('transporters','transporters.container_id','=','containers.id')
                ->join('drivers','drivers.container_id','=','containers.id')
                ->latest('tracks.document_date_received')
                ->where('tracks.line_vessel','LIKE',$desk)
                ->get();

            $agent          = Agent::with('user')->get();
            $transporter = Transporter::select('transporters.*','cont_no','bill_lading')
                ->join('containers','containers.id','=','transporters.container_id')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->orderByDesc("created_at")
                ->get()->groupBy(function($item) {
                    return $item->transporter_name;
                });

            $driver = Driver::select('drivers.*','cont_no','bill_lading')
                ->join('containers','containers.id','=','drivers.container_id')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->orderByDesc("created_at")
                ->get()->groupBy(function($item) {
                    return $item->driver_name;
                });

            return view('reports.super_reports',compact('destination','country','expenses','normal','track','agent','destiny_client','destiny_dest','destiny_line','transporter','driver'));
        }
        else if ($user->hasRole('sales')){


            $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
            $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

            $destiny_line = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->line_vessel;
            });
            $destiny_client = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->client_name;
            });
            $destiny_dest = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->destination;
            });
            $track   = Track::select('tracks.*','bill_lading','organization_name','loading_date','transporter_name','driver_name','cont_no','trucking_number','driver_license','driver_contact','driver_passport')
                ->join('laddings','laddings.track_id','=','tracks.id')
                ->join('containers','containers.ladding_id','=','laddings.id')
                ->join('agents','agents.id','=','tracks.agent_id')
                ->join('transporters','transporters.container_id','=','containers.id')
                ->join('drivers','drivers.container_id','=','containers.id')
                ->latest('tracks.document_date_received')
                ->whereBetween(DB::raw('YEAR(tracks.created_at)'),[$from, $to])
                ->where('tracks.line_vessel','LIKE',$desk)
                ->get();



            $agent          = Agent::with('user')->get();
            $transporter = Transporter::select('transporters.*','cont_no','bill_lading')
                ->join('containers','containers.id','=','transporters.container_id')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->whereBetween(DB::raw('YEAR(transporters.created_at)'),[$from, $to])
                ->orderByDesc("created_at")
                ->get()->groupBy(function($item) {
                    return $item->transporter_name;
                });


            $driver = Driver::select('drivers.*','cont_no','bill_lading')
                ->join('containers','containers.id','=','drivers.container_id')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->whereBetween(DB::raw('YEAR(drivers.created_at)'),[$from, $to])
                ->orderByDesc("created_at")
                ->get()->groupBy(function($item) {
                    return $item->driver_name;
                });
            return view('reports.sales_reports',compact('destination','country','expenses','normal','track','agent','destiny_client','destiny_dest','destiny_line','transporter','driver'));
        }
        else if ($user->hasRole('shipping')){


            $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
            $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

            $destiny_line = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->line_vessel;
            });
            $destiny_client = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->client_name;
            });
            $destiny_dest = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->destination;
            });
            $track   = Track::select('tracks.*','bill_lading','organization_name','loading_date','transporter_name','driver_name','cont_no','trucking_number','driver_license','driver_contact','driver_passport')
                ->join('laddings','laddings.track_id','=','tracks.id')
                ->join('containers','containers.ladding_id','=','laddings.id')
                ->join('agents','agents.id','=','tracks.agent_id')
                ->join('transporters','transporters.container_id','=','containers.id')
                ->join('drivers','drivers.container_id','=','containers.id')
                ->latest('tracks.document_date_received')
                ->whereBetween(DB::raw('YEAR(tracks.created_at)'),[$from, $to])
                ->where('tracks.line_vessel','LIKE',$desk)
                ->get();



            $agent          = Agent::with('user')->get();
            $transporter = Transporter::select('transporters.*','cont_no','bill_lading')
                ->join('containers','containers.id','=','transporters.container_id')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->whereBetween(DB::raw('YEAR(transporters.created_at)'),[$from, $to])
                ->orderByDesc("created_at")
                ->get()->groupBy(function($item) {
                    return $item->transporter_name;
                });


            $driver = Driver::select('drivers.*','cont_no','bill_lading')
                ->join('containers','containers.id','=','drivers.container_id')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->whereBetween(DB::raw('YEAR(drivers.created_at)'),[$from, $to])
                ->orderByDesc("created_at")
                ->get()->groupBy(function($item) {
                    return $item->driver_name;
                });

            return view('reports.shipping',compact('destination','country','expenses','normal','track','agent','destiny_client','destiny_dest','destiny_line','transporter','driver'));
        }
        else if ($user->hasRole('declaration')){


            $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
            $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

            $destiny_line = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->line_vessel;
            });
            $destiny_client = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->client_name;
            });
            $destiny_dest = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->destination;
            });
            $track   = Track::select('tracks.*','bill_lading','organization_name','loading_date','transporter_name','driver_name','cont_no','trucking_number','driver_license','driver_contact','driver_passport')
                ->join('laddings','laddings.track_id','=','tracks.id')
                ->join('containers','containers.ladding_id','=','laddings.id')
                ->join('agents','agents.id','=','tracks.agent_id')
                ->join('transporters','transporters.container_id','=','containers.id')
                ->join('drivers','drivers.container_id','=','containers.id')
                ->latest('tracks.document_date_received')
                ->whereBetween(DB::raw('YEAR(tracks.created_at)'),[$from, $to])
                ->where('tracks.line_vessel','LIKE',$desk)
                ->get();



            $agent          = Agent::with('user')->get();
            $transporter = Transporter::select('transporters.*','cont_no','bill_lading')
                ->join('containers','containers.id','=','transporters.container_id')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->whereBetween(DB::raw('YEAR(transporters.created_at)'),[$from, $to])
                ->orderByDesc("created_at")
                ->get()->groupBy(function($item) {
                    return $item->transporter_name;
                });


            $driver = Driver::select('drivers.*','cont_no','bill_lading')
                ->join('containers','containers.id','=','drivers.container_id')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->whereBetween(DB::raw('YEAR(drivers.created_at)'),[$from, $to])
                ->orderByDesc("created_at")
                ->get()->groupBy(function($item) {
                    return $item->driver_name;
                });

            return view('reports.declaration',compact('destination','country','expenses','normal','track','agent','destiny_client','destiny_dest','destiny_line','transporter','driver'));
        }
        else if ($user->hasRole('documentation')){


            $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
            $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

            $destiny_line = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->line_vessel;
            });
            $destiny_client = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->client_name;
            });
            $destiny_dest = Track::orderBy('created_at')->get()->groupBy(function($item) {
                return $item->destination;
            });
            $track   = Track::select('tracks.*','bill_lading','organization_name','loading_date','transporter_name','driver_name','cont_no','trucking_number','driver_license','driver_contact','driver_passport')
                ->join('laddings','laddings.track_id','=','tracks.id')
                ->join('containers','containers.ladding_id','=','laddings.id')
                ->join('agents','agents.id','=','tracks.agent_id')
                ->join('transporters','transporters.container_id','=','containers.id')
                ->join('drivers','drivers.container_id','=','containers.id')
                ->latest('tracks.document_date_received')
                ->whereBetween(DB::raw('YEAR(tracks.created_at)'),[$from, $to])
                ->where('tracks.line_vessel','LIKE',$desk)
                ->get();



            $agent          = Agent::with('user')->get();
            $transporter = Transporter::select('transporters.*','cont_no','bill_lading')
                ->join('containers','containers.id','=','transporters.container_id')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->whereBetween(DB::raw('YEAR(transporters.created_at)'),[$from, $to])
                ->orderByDesc("created_at")
                ->get()->groupBy(function($item) {
                    return $item->transporter_name;
                });


            $driver = Driver::select('drivers.*','cont_no','bill_lading')
                ->join('containers','containers.id','=','drivers.container_id')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->whereBetween(DB::raw('YEAR(drivers.created_at)'),[$from, $to])
                ->orderByDesc("created_at")
                ->get()->groupBy(function($item) {
                    return $item->driver_name;
                });

            return view('reports.documentation',compact('destination','country','expenses','normal','track','agent','destiny_client','destiny_dest','destiny_line','transporter','driver'));
        }
        elseif ($user->hasRole('agent'))
        {


            $track   = Track::select('tracks.*','bill_lading','organization_name','loading_date','transporter_name','cont_no','driver_name','trucking_number','driver_license','driver_contact','driver_passport')
                ->join('laddings','laddings.track_id','=','tracks.id')
                ->join('containers','containers.ladding_id','=','laddings.id')
                ->join('agents','agents.id','=','tracks.agent_id')
                ->join('transporters','transporters.container_id','=','containers.id')
                ->join('drivers','drivers.container_id','=','containers.id')
                ->latest('tracks.document_date_received')
                ->whereBetween(DB::raw('YEAR(tracks.created_at)'),[$from, $to])
                ->where('tracks.line_vessel','LIKE',$desk)
                ->get();
            $agent          = Agent::with('user')->get();

            $transporter = Transporter::select('transporters.*','cont_no','bill_lading')
                ->join('containers','containers.id','=','transporters.container_id')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->whereBetween(DB::raw('YEAR(transporters.created_at)'),[$from, $to])
                ->orderByDesc("created_at")
                ->get()->groupBy(function($item) {
                    return $item->transporter_name;
                });

            $driver = Driver::select('drivers.*','cont_no','bill_lading')
                ->join('containers','containers.id','=','drivers.container_id')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->whereBetween(DB::raw('YEAR(drivers.created_at)'),[$from, $to])
                ->orderByDesc("created_at")
                ->get()->groupBy(function($item) {
                    return $item->driver_name;
                });

            return view('agent.reports',compact('destination','country','track','agent','transporter','driver'));
        }
        else{
            return JsonResponse::create(['error' => 'access-denied'],401);
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|static
     */
    public function agent_view_order($id)
    {
        $from = '2021';
        $to = '2030';
        $expenses = Expenses::with('container')->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
        $normal = NormalExpenses::where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

        $user   =Auth::user();

        if($user->hasRole('admin'))
        {
            $track   = Track::select('tracks.*','bill_lading','organization_name')
                ->join('laddings','laddings.track_id','=','tracks.id')
                ->join('agents','agents.id','=','tracks.agent_id')
                ->where('tracks.agent_id','=',$id)
                ->latest('tracks.document_date_received')
                ->get();
            $agent          = Agent::with('user')->get();
            return view('order.list_order', compact('expenses','normal','track','agent'));
        }
        elseif($user->hasRole('super'))
        {
            $expenses = Expenses::with('container')->where('finance_status','LIKE','pending')->count();

            $normal = NormalExpenses::where('finance_status','LIKE','pending')->count();

            $track   = Track::select('tracks.*','bill_lading','organization_name')
                ->join('laddings','laddings.track_id','=','tracks.id')
                ->join('agents','agents.id','=','tracks.agent_id')
                ->where('tracks.agent_id','=',$id)
                ->latest('tracks.document_date_received')
                ->get();
            $agent          = Agent::with('user')->get();
            return view('order.super_list_order', compact('expenses','normal','track','agent'));
        }
        elseif($user->hasRole('sales'))
        {
            $track   = Track::select('tracks.*','bill_lading','organization_name')
                ->join('laddings','laddings.track_id','=','tracks.id')
                ->join('agents','agents.id','=','tracks.agent_id')
                ->where('tracks.agent_id','=',$id)
                ->latest('tracks.document_date_received')
                ->get();
            $agent          = Agent::with('user')->get();
            return view('staff.sales_list_order', compact('expenses','normal','track','agent'));
        }
        elseif($user->hasRole('finance'))
        {
            $expenses = Expenses::with('container')->where('finance_status','LIKE','pending')->count();

            $normal = NormalExpenses::where('finance_status','LIKE','pending')->count();

            $track   = Track::select('tracks.*','bill_lading','organization_name')
                ->join('laddings','laddings.track_id','=','tracks.id')
                ->join('agents','agents.id','=','tracks.agent_id')
                ->where('tracks.agent_id','=',$id)
                ->orderByDesc("tracks.created_at")
                ->get();
            $agent          = Agent::with('user')->get();
            return view('finance.agent_list_order', compact('expenses','normal','track','agent'));
        }
        elseif($user->hasRole('documentation'))
        {
            $track   = Track::select('tracks.*','bill_lading','organization_name')
                ->join('laddings','laddings.track_id','=','tracks.id')
                ->join('agents','agents.id','=','tracks.agent_id')
                ->where('tracks.agent_id','=',$id)
                ->latest('tracks.document_date_received')
                ->get();
            $agent          = Agent::with('user')->get();
            return view('documentation.documentation_list_order', compact('expenses','normal','track','agent'));
        }
        else
        {
            return JsonResponse::create(['error' => 'access-denied'],401);
        }
    }


    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|static
     * daily report report for consigment not crossed border
     */

    public function DailyReportNotCrossed()
    {
        $from = '2021';
        $to = '2030';
        $user   =Auth::user();

        if($user->hasRole('admin'))
        {



            $last_30 =Carbon::today()->subDays(10)->format('Y-m-d H:i:s');

            $now    = Carbon::today()->format('Y-m-d H:i:s');

            $agent  =Agent::latest()->get();

            $track   = Track::select('tracks.*','bill_lading','organization_name')
                        ->join('laddings','laddings.track_id','=','tracks.id')
                        ->join('agents','agents.id','=','tracks.agent_id')
                        ->where('tracks.current_status', 'NOT LIKE', '%'.'Crossed Border'.'%')
                        ->where('tracks.current_status', 'NOT LIKE', '%'.'File Closed'.'%')
//                        ->whereBetween('tracks.updated_at',array($last_30, $now))
                        ->whereBetween(DB::raw('YEAR(tracks.created_at)'),[$from, $to])
                        ->latest('tracks.created_at')
                        ->get();


            $consignee  = Track::all();

            $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
            $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

            return view('reports.admin_not_crossed_report', compact('expenses','normal','track','agent','consignee'));

        }
        elseif ($user->hasRole('super'))
        {


        $last_30 =Carbon::today()->subDays(60)->format('Y-m-d H:i:s');

        $now    = Carbon::today()->format('Y-m-d H:i:s');

        $agent  =Agent::latest()->get();

        $track   = Track::select('tracks.*','bill_lading','organization_name')
            ->join('laddings','laddings.track_id','=','tracks.id')
            ->join('agents','agents.id','=','tracks.agent_id')
            ->where('tracks.current_status', 'NOT LIKE', '%'.'Crossed Border'.'%')
            ->where('tracks.current_status', 'NOT LIKE', '%'.'File Closed'.'%')
//            ->whereBetween('tracks.updated_at',array($last_30, $now))
            ->latest('tracks.created_at')
            ->get();


        $consignee  = Track::all();

            $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
            $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

            return view('reports.super_not_crossed_report', compact('expenses','normal','track','agent','consignee'));

        }
        elseif ($user->hasRole('documentation'))
        {


            $last_30 =Carbon::today()->subDays(60)->format('Y-m-d H:i:s');

            $now    = Carbon::today()->format('Y-m-d H:i:s');

            $agent  =Agent::latest()->get();

            $track   = Track::select('tracks.*','bill_lading','organization_name')
                ->join('laddings','laddings.track_id','=','tracks.id')
                ->join('agents','agents.id','=','tracks.agent_id')
                ->where('tracks.current_status', 'NOT LIKE', '%'.'Crossed Border'.'%')
                ->where('tracks.current_status', 'NOT LIKE', '%'.'File Closed'.'%')
//                ->whereBetween('tracks.updated_at',array($last_30, $now))
                ->whereBetween(DB::raw('YEAR(tracks.created_at)'),[$from, $to])
                ->latest('tracks.created_at')
                ->get();


            $consignee  = Track::all();

            $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
            $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

            return view('reports.documentation_not_crossed_report', compact('expenses','normal','track','agent','consignee'));

        }
        elseif ($user->hasRole('declaration'))
        {


            $last_30 =Carbon::today()->subDays(60)->format('Y-m-d H:i:s');

            $now    = Carbon::today()->format('Y-m-d H:i:s');

            $agent  =Agent::latest()->get();

            $track   = Track::select('tracks.*','bill_lading','organization_name')
                ->join('laddings','laddings.track_id','=','tracks.id')
                ->join('agents','agents.id','=','tracks.agent_id')
                ->where('tracks.current_status', 'NOT LIKE', '%'.'Crossed Border'.'%')
                ->where('tracks.current_status', 'NOT LIKE', '%'.'File Closed'.'%')
//                ->whereBetween('tracks.updated_at',array($last_30, $now))
                ->whereBetween(DB::raw('YEAR(tracks.created_at)'),[$from, $to])
                ->latest('tracks.created_at')
                ->get();


            $consignee  = Track::all();

            $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
            $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

            return view('reports.declaration_not_crossed_report', compact('expenses','normal','track','agent','consignee'));

        }
        elseif ($user->hasRole('shipping'))
        {


            $last_30 =Carbon::today()->subDays(60)->format('Y-m-d H:i:s');

            $now    = Carbon::today()->format('Y-m-d H:i:s');

            $agent  =Agent::latest()->get();

            $track   = Track::select('tracks.*','bill_lading','organization_name')
                ->join('laddings','laddings.track_id','=','tracks.id')
                ->join('agents','agents.id','=','tracks.agent_id')
                ->where('tracks.current_status', 'NOT LIKE', '%'.'Crossed Border'.'%')
                ->where('tracks.current_status', 'NOT LIKE', '%'.'File Closed'.'%')
//                ->whereBetween('tracks.updated_at',array($last_30, $now))
                ->whereBetween(DB::raw('YEAR(tracks.created_at)'),[$from, $to])
                ->latest('tracks.created_at')
                ->get();


            $consignee  = Track::all();

            $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
            $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

            return view('reports.shipping_not_crossed_report', compact('expenses','normal','track','agent','consignee'));

        }
        elseif ($user->hasRole('sales'))
        {


            $last_30 =Carbon::today()->subDays(60)->format('Y-m-d H:i:s');

            $now    = Carbon::today()->format('Y-m-d H:i:s');

            $agent  =Agent::latest()->get();

            $track   = Track::select('tracks.*','bill_lading','organization_name')
                ->join('laddings','laddings.track_id','=','tracks.id')
                ->join('agents','agents.id','=','tracks.agent_id')
                ->where('tracks.current_status', 'NOT LIKE', '%'.'Crossed Border'.'%')
                ->where('tracks.current_status', 'NOT LIKE', '%'.'File Closed'.'%')
//                ->whereBetween('tracks.updated_at',array($last_30, $now))
                ->whereBetween(DB::raw('YEAR(tracks.created_at)'),[$from, $to])
                ->latest('tracks.created_at')
                ->get();


            $consignee  = Track::all();

            $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
            $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

            return view('reports.sales_not_crossed_report', compact('expenses','normal','track','agent','consignee'));

        }
        elseif ($user->hasRole('finance'))
        {


            $last_30 =Carbon::today()->subDays(60)->format('Y-m-d H:i:s');

            $now    = Carbon::today()->format('Y-m-d H:i:s');

            $agent  =Agent::latest()->get();

            $track   = Track::select('tracks.*','bill_lading','organization_name')
                ->join('laddings','laddings.track_id','=','tracks.id')
                ->join('agents','agents.id','=','tracks.agent_id')
                ->where('tracks.current_status', 'NOT LIKE', '%'.'Crossed Border'.'%')
                ->where('tracks.current_status', 'NOT LIKE', '%'.'File Closed'.'%')
//                ->whereBetween('tracks.updated_at',array($last_30, $now))
                ->whereBetween(DB::raw('YEAR(tracks.created_at)'),[$from, $to])
                ->latest('tracks.created_at')
                ->get();


            $consignee  = Track::all();
            $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
            $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

            return view('reports.finance_not_crossed_report', compact('expenses','normal','track','agent','consignee'));

        }
        else
        {
            return JsonResponse::create(['error' => 'access-denied'],401);
        }

    }


    /**
     * @param Request $request
     * @return static
     *
     */
    public function SearchDaily(Request $request)
    {

        $from = '2021';
        $to = '2030';
        $user   =Auth::user();
        $client_name = $request->get('client_name');
        $agent_id    = $request->get('agent_id');

        if($user->hasRole('admin'))
        {
            if ($client_name !='' AND $agent_id != '')
            {

                $last_30 =Carbon::today()->subDays(1)->format('Y-m-d H:i:s');

                $now    = Carbon::today()->addDay(1)->format('Y-m-d H:i:s');

                $agent  =Agent::latest()->get();

                $track   = Track::select('tracks.*','bill_lading','organization_name')
                            ->join('laddings','laddings.track_id','=','tracks.id')
                            ->join('agents','agents.id','=','tracks.agent_id')
                            ->where('tracks.current_status','NOT LIKE','Crossed Border')
                            ->where('tracks.current_status','NOT LIKE','File Closed')
                            ->whereBetween('tracks.updated_at',array($last_30, $now))
                            ->where('tracks.client_name','LIKE',$client_name)
                            ->where('tracks.agent_id','=',$agent_id)
                            ->latest('tracks.created_at')
                            ->get();

                $consignee  = Track::all();
                $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
                $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

                return view('reports.admin_not_crossed_report', compact('expenses','normal','track','agent','consignee'));

            }
            else if ($client_name != ''  AND $agent_id =='')
            {

                $last_30 =Carbon::today()->subDays(1)->format('Y-m-d H:i:s');

                $now    = Carbon::today()->addDay(1)->format('Y-m-d H:i:s');

                $agent  =Agent::latest()->get();

                $track   = Track::select('tracks.*','bill_lading','organization_name')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('tracks.current_status','NOT LIKE','Crossed Border')
                    ->where('tracks.current_status','NOT LIKE','File Closed')
                    ->whereBetween('tracks.updated_at',array($last_30, $now))
                    ->where('tracks.client_name','LIKE',$client_name)
                    ->latest('tracks.created_at')
                    ->get();

                $consignee  = Track::all();

                $expenses   = Expenses::with('container')->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
                $normal     = NormalExpenses::where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

                return view('reports.admin_not_crossed_report', compact('expenses','normal','track','agent','consignee'));

            }
            else if ($client_name == ''  AND ($agent_id !=''))
            {


                $last_30 =Carbon::today()->subDays(1)->format('Y-m-d H:i:s');

                $now    = Carbon::today()->addDay(1)->format('Y-m-d H:i:s');

                $agent  =Agent::latest()->get();

                $track   = Track::select('tracks.*','bill_lading','organization_name')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('tracks.current_status','NOT LIKE','Crossed Border')
                    ->where('tracks.current_status','NOT LIKE','File Closed')
                    ->whereBetween('tracks.updated_at',array($last_30, $now))
                    ->where('tracks.agent_id','=',$agent_id)
                    ->latest('tracks.created_at')
                    ->get();

                $consignee  = Track::all();

                $expenses   = Expenses::with('container')->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
                $normal     = NormalExpenses::where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

                return view('reports.admin_not_crossed_report', compact('expenses','normal','track','agent','consignee'));

            }
            else
            {
                $last_30 =Carbon::today()->subDays(1)->format('Y-m-d H:i:s');

                $now    = Carbon::today()->addDay(1)->format('Y-m-d H:i:s');

                $agent  =Agent::latest()->get();

                $track   = Track::select('tracks.*','bill_lading','organization_name')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('tracks.current_status','NOT LIKE','Crossed Border')
                    ->where('tracks.current_status','NOT LIKE','File Closed')
                    ->whereBetween('tracks.updated_at',array($last_30, $now))
                    ->latest('tracks.created_at')
                    ->get();


                $consignee  = Track::all();

                $expenses   = Expenses::with('container')->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
                $normal     = NormalExpenses::where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

                return view('reports.admin_not_crossed_report', compact('expenses','normal','track','agent','consignee'));

            }

        }
        else if($user->hasRole('super'))
        {
            if ($client_name !='' AND $agent_id != '')
            {

                $last_30 =Carbon::today()->subDays(1)->format('Y-m-d H:i:s');

                $now    = Carbon::today()->addDay(1)->format('Y-m-d H:i:s');

                $agent  =Agent::latest()->get();

                $track   = Track::select('tracks.*','bill_lading','organization_name')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('tracks.current_status','NOT LIKE','Crossed Border')
                    ->where('tracks.current_status','NOT LIKE','File Closed')
                    ->whereBetween('tracks.updated_at',array($last_30, $now))
                    ->where('tracks.client_name','LIKE',$client_name)
                    ->where('tracks.agent_id','=',$agent_id)
                    ->latest('tracks.created_at')
                    ->get();

                $consignee  = Track::all();

                $expenses   = Expenses::with('container')->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
                $normal     = NormalExpenses::where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

                return view('reports.super_not_crossed_report', compact('expenses','normal','track','agent','consignee'));

            }
            elseif ($client_name != ''  AND $agent_id =='')
            {

                $last_30 =Carbon::today()->subDays(1)->format('Y-m-d H:i:s');

                $now    = Carbon::today()->addDay(1)->format('Y-m-d H:i:s');

                $agent  =Agent::latest()->get();

                $track   = Track::select('tracks.*','bill_lading','organization_name')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('tracks.current_status','NOT LIKE','Crossed Border')
                    ->where('tracks.current_status','NOT LIKE','File Closed')
                    ->whereBetween('tracks.updated_at',array($last_30, $now))
                    ->where('tracks.client_name','LIKE',$client_name)
                    ->latest('tracks.created_at')
                    ->get();

                $consignee  = Track::all();

                $expenses   = Expenses::with('container')->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
                $normal     = NormalExpenses::where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

                return view('reports.super_not_crossed_report', compact('expenses','normal','track','agent','consignee'));

            }
            elseif ($client_name == ''  AND ($agent_id !=''))
            {


                $last_30 =Carbon::today()->subDays(1)->format('Y-m-d H:i:s');

                $now    = Carbon::today()->addDay(1)->format('Y-m-d H:i:s');

                $agent  =Agent::latest()->get();

                $track   = Track::select('tracks.*','bill_lading','organization_name')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('tracks.current_status','NOT LIKE','Crossed Border')
                    ->where('tracks.current_status','NOT LIKE','File Closed')
                    ->whereBetween('tracks.updated_at',array($last_30, $now))
                    ->where('tracks.agent_id','=',$agent_id)
                    ->latest('tracks.created_at')
                    ->get();

                $consignee  = Track::all();

                $expenses   = Expenses::with('container')->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
                $normal     = NormalExpenses::where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

                return view('reports.super_not_crossed_report', compact('expenses','normal','track','agent','consignee'));

            }
            else
            {
                $last_30 =Carbon::today()->subDays(1)->format('Y-m-d H:i:s');

                $now    = Carbon::today()->addDay(1)->format('Y-m-d H:i:s');

                $agent  =Agent::latest()->get();

                $track   = Track::select('tracks.*','bill_lading','organization_name')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('tracks.current_status','NOT LIKE','Crossed Border')
                    ->where('tracks.current_status','NOT LIKE','File Closed')
                    ->whereBetween('tracks.updated_at',array($last_30, $now))
                    ->latest('tracks.created_at')
                    ->get();


                $consignee  = Track::all();

                $expenses   = Expenses::with('container')->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
                $normal     = NormalExpenses::where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

                return view('reports.super_not_crossed_report', compact('expenses','normal','track','agent','consignee'));

            }

        }
        else if($user->hasRole('documentation'))
        {
            if ($client_name !='' AND $agent_id != '')
            {

                $last_30 =Carbon::today()->subDays(1)->format('Y-m-d H:i:s');

                $now    = Carbon::today()->addDay(1)->format('Y-m-d H:i:s');

                $agent  =Agent::latest()->get();

                $track   = Track::select('tracks.*','bill_lading','organization_name')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('tracks.current_status','NOT LIKE','Crossed Border')
                    ->where('tracks.current_status','NOT LIKE','File Closed')
                    ->whereBetween('tracks.updated_at',array($last_30, $now))
                    ->where('tracks.client_name','LIKE',$client_name)
                    ->where('tracks.agent_id','=',$agent_id)
                    ->latest('tracks.created_at')
                    ->get();

                $consignee  = Track::all();

                $expenses   = Expenses::with('container')->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
                $normal     = NormalExpenses::where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

                return view('reports.documentation_not_crossed_report', compact('expenses','normal','track','agent','consignee'));

            }
            else if ($client_name != ''  AND $agent_id =='')
            {

                $last_30 =Carbon::today()->subDays(1)->format('Y-m-d H:i:s');

                $now    = Carbon::today()->addDay(1)->format('Y-m-d H:i:s');

                $agent  =Agent::latest()->get();

                $track   = Track::select('tracks.*','bill_lading','organization_name')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('tracks.current_status','NOT LIKE','Crossed Border')
                    ->where('tracks.current_status','NOT LIKE','File Closed')
                    ->whereBetween('tracks.updated_at',array($last_30, $now))
                    ->where('tracks.client_name','LIKE',$client_name)
                    ->latest('tracks.created_at')
                    ->get();

                $consignee  = Track::all();

                $expenses   = Expenses::with('container')->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
                $normal     = NormalExpenses::where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

                return view('reports.documentation_not_crossed_report', compact('expenses','normal','track','agent','consignee'));

            }
            else if ($client_name == ''  AND ($agent_id !=''))
            {


                $last_30 =Carbon::today()->subDays(1)->format('Y-m-d H:i:s');

                $now    = Carbon::today()->addDay(1)->format('Y-m-d H:i:s');

                $agent  =Agent::latest()->get();

                $track   = Track::select('tracks.*','bill_lading','organization_name')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('tracks.current_status','NOT LIKE','Crossed Border')
                    ->where('tracks.current_status','NOT LIKE','File Closed')
                    ->whereBetween('tracks.updated_at',array($last_30, $now))
                    ->where('tracks.agent_id','=',$agent_id)
                    ->latest('tracks.created_at')
                    ->get();

                $consignee  = Track::all();

                $expenses   = Expenses::with('container')->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
                $normal     = NormalExpenses::where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

                return view('reports.documentation_not_crossed_report', compact('expenses','normal','track','agent','consignee'));

            }
            else
            {
                $last_30 =Carbon::today()->subDays(1)->format('Y-m-d H:i:s');

                $now    = Carbon::today()->addDay(1)->format('Y-m-d H:i:s');

                $agent  =Agent::latest()->get();

                $track   = Track::select('tracks.*','bill_lading','organization_name')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('tracks.current_status','NOT LIKE','Crossed Border')
                    ->where('tracks.current_status','NOT LIKE','File Closed')
                    ->whereBetween('tracks.updated_at',array($last_30, $now))
                    ->latest('tracks.created_at')
                    ->get();


                $consignee  = Track::all();

                $expenses   = Expenses::with('container')->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
                $normal     = NormalExpenses::where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

                return view('reports.documentation_not_crossed_report', compact('expenses','normal','track','agent','consignee'));

            }

        }
        else if($user->hasRole('declaration'))
        {
            if ($client_name !='' AND $agent_id != '')
            {

                $last_30 =Carbon::today()->subDays(1)->format('Y-m-d H:i:s');

                $now    = Carbon::today()->addDay(1)->format('Y-m-d H:i:s');

                $agent  =Agent::latest()->get();

                $track   = Track::select('tracks.*','bill_lading','organization_name')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('tracks.current_status','NOT LIKE','Crossed Border')
                    ->where('tracks.current_status','NOT LIKE','File Closed')
                    ->whereBetween('tracks.updated_at',array($last_30, $now))
                    ->where('tracks.client_name','LIKE',$client_name)
                    ->where('tracks.agent_id','=',$agent_id)
                    ->latest('tracks.created_at')
                    ->get();

                $consignee  = Track::all();

                $expenses   = Expenses::with('container')->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
                $normal     = NormalExpenses::where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

                return view('reports.declaration_not_crossed_report', compact('expenses','normal','track','agent','consignee'));

            }
            else if ($client_name != ''  AND $agent_id =='')
            {

                $last_30 =Carbon::today()->subDays(1)->format('Y-m-d H:i:s');

                $now    = Carbon::today()->addDay(1)->format('Y-m-d H:i:s');

                $agent  =Agent::latest()->get();

                $track   = Track::select('tracks.*','bill_lading','organization_name')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('tracks.current_status','NOT LIKE','Crossed Border')
                    ->where('tracks.current_status','NOT LIKE','File Closed')
                    ->whereBetween('tracks.updated_at',array($last_30, $now))
                    ->where('tracks.client_name','LIKE',$client_name)
                    ->latest('tracks.created_at')
                    ->get();

                $consignee  = Track::all();

                $expenses   = Expenses::with('container')->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
                $normal     = NormalExpenses::where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

                return view('reports.declaration_not_crossed_report', compact('expenses','normal','track','agent','consignee'));

            }
            else if ($client_name == ''  AND ($agent_id !=''))
            {


                $last_30 =Carbon::today()->subDays(1)->format('Y-m-d H:i:s');

                $now    = Carbon::today()->addDay(1)->format('Y-m-d H:i:s');

                $agent  =Agent::latest()->get();

                $track   = Track::select('tracks.*','bill_lading','organization_name')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('tracks.current_status','NOT LIKE','Crossed Border')
                    ->where('tracks.current_status','NOT LIKE','File Closed')
                    ->whereBetween('tracks.updated_at',array($last_30, $now))
                    ->where('tracks.agent_id','=',$agent_id)
                    ->latest('tracks.created_at')
                    ->get();

                $consignee  = Track::all();

                $expenses   = Expenses::with('container')->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
                $normal     = NormalExpenses::where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

                return view('reports.declaration_not_crossed_report', compact('expenses','normal','track','agent','consignee'));

            }
            else
            {
                $last_30 =Carbon::today()->subDays(1)->format('Y-m-d H:i:s');

                $now    = Carbon::today()->addDay(1)->format('Y-m-d H:i:s');

                $agent  =Agent::latest()->get();

                $track   = Track::select('tracks.*','bill_lading','organization_name')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('tracks.current_status','NOT LIKE','Crossed Border')
                    ->where('tracks.current_status','NOT LIKE','File Closed')
                    ->whereBetween('tracks.updated_at',array($last_30, $now))
                    ->latest('tracks.created_at')
                    ->get();


                $consignee  = Track::all();

                $expenses   = Expenses::with('container')->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
                $normal     = NormalExpenses::where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

                return view('reports.declaration_not_crossed_report', compact('expenses','normal','track','agent','consignee'));

            }

        }
        else if($user->hasRole('shipping'))
        {
            if ($client_name !='' AND $agent_id != '')
            {

                $last_30 =Carbon::today()->subDays(1)->format('Y-m-d H:i:s');

                $now    = Carbon::today()->addDay(1)->format('Y-m-d H:i:s');

                $agent  =Agent::latest()->get();

                $track   = Track::select('tracks.*','bill_lading','organization_name')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('tracks.current_status','NOT LIKE','Crossed Border')
                    ->where('tracks.current_status','NOT LIKE','File Closed')
                    ->whereBetween('tracks.updated_at',array($last_30, $now))
                    ->where('tracks.client_name','LIKE',$client_name)
                    ->where('tracks.agent_id','=',$agent_id)
                    ->latest('tracks.created_at')
                    ->get();

                $consignee  = Track::all();

                $expenses   = Expenses::with('container')->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
                $normal     = NormalExpenses::where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

                return view('reports.shipping_not_crossed_report', compact('expenses','normal','track','agent','consignee'));

            }
            else if ($client_name != ''  AND $agent_id =='')
            {

                $last_30 =Carbon::today()->subDays(1)->format('Y-m-d H:i:s');

                $now    = Carbon::today()->addDay(1)->format('Y-m-d H:i:s');

                $agent  =Agent::latest()->get();

                $track   = Track::select('tracks.*','bill_lading','organization_name')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('tracks.current_status','NOT LIKE','Crossed Border')
                    ->where('tracks.current_status','NOT LIKE','File Closed')
                    ->whereBetween('tracks.updated_at',array($last_30, $now))
                    ->where('tracks.client_name','LIKE',$client_name)
                    ->latest('tracks.created_at')
                    ->get();

                $consignee  = Track::all();

                $expenses   = Expenses::with('container')->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
                $normal     = NormalExpenses::where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

                return view('reports.shipping_not_crossed_report', compact('expenses','normal','track','agent','consignee'));

            }
            else if ($client_name == ''  AND ($agent_id !=''))
            {


                $last_30 =Carbon::today()->subDays(1)->format('Y-m-d H:i:s');

                $now    = Carbon::today()->addDay(1)->format('Y-m-d H:i:s');

                $agent  =Agent::latest()->get();

                $track   = Track::select('tracks.*','bill_lading','organization_name')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('tracks.current_status','NOT LIKE','Crossed Border')
                    ->where('tracks.current_status','NOT LIKE','File Closed')
                    ->whereBetween('tracks.updated_at',array($last_30, $now))
                    ->where('tracks.agent_id','=',$agent_id)
                    ->latest('tracks.created_at')
                    ->get();

                $consignee  = Track::all();

                $expenses   = Expenses::with('container')->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
                $normal     = NormalExpenses::where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

                return view('reports.shipping_not_crossed_report', compact('expenses','normal','track','agent','consignee'));

            }
            else
            {
                $last_30 =Carbon::today()->subDays(1)->format('Y-m-d H:i:s');

                $now    = Carbon::today()->addDay(1)->format('Y-m-d H:i:s');

                $agent  =Agent::latest()->get();

                $track   = Track::select('tracks.*','bill_lading','organization_name')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('tracks.current_status','NOT LIKE','Crossed Border')
                    ->where('tracks.current_status','NOT LIKE','File Closed')
                    ->whereBetween('tracks.updated_at',array($last_30, $now))
                    ->latest('tracks.created_at')
                    ->get();


                $consignee  = Track::all();

                $expenses   = Expenses::with('container')->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
                $normal     = NormalExpenses::where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

                return view('reports.shipping_not_crossed_report', compact('expenses','normal','track','agent','consignee'));

            }

        }
        else if($user->hasRole('sales'))
        {
            if ($client_name !='' AND $agent_id != '')
            {

                $last_30 =Carbon::today()->subDays(1)->format('Y-m-d H:i:s');

                $now    = Carbon::today()->addDay(1)->format('Y-m-d H:i:s');

                $agent  =Agent::latest()->get();

                $track   = Track::select('tracks.*','bill_lading','organization_name')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('tracks.current_status','NOT LIKE','Crossed Border')
                    ->where('tracks.current_status','NOT LIKE','File Closed')
                    ->whereBetween('tracks.updated_at',array($last_30, $now))
                    ->where('tracks.client_name','LIKE',$client_name)
                    ->where('tracks.agent_id','=',$agent_id)
                    ->latest('tracks.created_at')
                    ->get();

                $consignee  = Track::all();

                $expenses   = Expenses::with('container')->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
                $normal     = NormalExpenses::where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

                return view('reports.sales_not_crossed_report', compact('expenses','normal','track','agent','consignee'));

            }
            else if ($client_name != ''  AND $agent_id =='')
            {

                $last_30 =Carbon::today()->subDays(1)->format('Y-m-d H:i:s');

                $now    = Carbon::today()->addDay(1)->format('Y-m-d H:i:s');

                $agent  =Agent::latest()->get();

                $track   = Track::select('tracks.*','bill_lading','organization_name')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('tracks.current_status','NOT LIKE','Crossed Border')
                    ->where('tracks.current_status','NOT LIKE','File Closed')
                    ->whereBetween('tracks.updated_at',array($last_30, $now))
                    ->where('tracks.client_name','LIKE',$client_name)
                    ->latest('tracks.created_at')
                    ->get();

                $consignee  = Track::all();

                $expenses   = Expenses::with('container')->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
                $normal     = NormalExpenses::where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

                return view('reports.sales_not_crossed_report', compact('expenses','normal','track','agent','consignee'));

            }
            else if ($client_name == ''  AND ($agent_id !=''))
            {


                $last_30 =Carbon::today()->subDays(1)->format('Y-m-d H:i:s');

                $now    = Carbon::today()->addDay(1)->format('Y-m-d H:i:s');

                $agent  =Agent::latest()->get();

                $track   = Track::select('tracks.*','bill_lading','organization_name')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('tracks.current_status','NOT LIKE','Crossed Border')
                    ->where('tracks.current_status','NOT LIKE','File Closed')
                    ->whereBetween('tracks.updated_at',array($last_30, $now))
                    ->where('tracks.agent_id','=',$agent_id)
                    ->latest('tracks.created_at')
                    ->get();

                $consignee  = Track::all();

                $expenses   = Expenses::with('container')->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
                $normal     = NormalExpenses::where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

                return view('reports.sales_not_crossed_report', compact('expenses','normal','track','agent','consignee'));

            }
            else
            {
                $last_30 =Carbon::today()->subDays(1)->format('Y-m-d H:i:s');

                $now    = Carbon::today()->addDay(1)->format('Y-m-d H:i:s');

                $agent  =Agent::latest()->get();

                $track   = Track::select('tracks.*','bill_lading','organization_name')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('tracks.current_status','NOT LIKE','Crossed Border')
                    ->where('tracks.current_status','NOT LIKE','File Closed')
                    ->whereBetween('tracks.updated_at',array($last_30, $now))
                    ->latest('tracks.created_at')
                    ->get();


                $consignee  = Track::all();

                $expenses   = Expenses::with('container')->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
                $normal     = NormalExpenses::where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

                return view('reports.sales_not_crossed_report', compact('expenses','normal','track','agent','consignee'));

            }

        }
        else if($user->hasRole('finance'))
        {
            if ($client_name !='' AND $agent_id != '')
            {

                $last_30 =Carbon::today()->subDays(1)->format('Y-m-d H:i:s');

                $now    = Carbon::today()->addDay(1)->format('Y-m-d H:i:s');

                $agent  =Agent::latest()->get();

                $track   = Track::select('tracks.*','bill_lading','organization_name')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('tracks.current_status','NOT LIKE','Crossed Border')
                    ->where('tracks.current_status','NOT LIKE','File Closed')
                    ->whereBetween('tracks.updated_at',array($last_30, $now))
                    ->where('tracks.client_name','LIKE',$client_name)
                    ->where('tracks.agent_id','=',$agent_id)
                    ->latest('tracks.created_at')
                    ->get();

                $consignee  = Track::all();

                $expenses   = Expenses::with('container')->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
                $normal     = NormalExpenses::where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

                return view('reports.finance_not_crossed_report', compact('expenses','normal','track','agent','consignee'));

            }
            else if ($client_name != ''  AND $agent_id =='')
            {

                $last_30 =Carbon::today()->subDays(1)->format('Y-m-d H:i:s');

                $now    = Carbon::today()->addDay(1)->format('Y-m-d H:i:s');

                $agent  =Agent::latest()->get();

                $track   = Track::select('tracks.*','bill_lading','organization_name')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('tracks.current_status','NOT LIKE','Crossed Border')
                    ->where('tracks.current_status','NOT LIKE','File Closed')
                    ->whereBetween('tracks.updated_at',array($last_30, $now))
                    ->where('tracks.client_name','LIKE',$client_name)
                    ->latest('tracks.created_at')
                    ->get();

                $consignee  = Track::all();

                $expenses   = Expenses::with('container')->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
                $normal     = NormalExpenses::where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

                return view('reports.finance_not_crossed_report', compact('expenses','normal','track','agent','consignee'));

            }
            else if ($client_name == ''  AND ($agent_id !=''))
            {


                $last_30 =Carbon::today()->subDays(1)->format('Y-m-d H:i:s');

                $now    = Carbon::today()->addDay(1)->format('Y-m-d H:i:s');

                $agent  =Agent::latest()->get();

                $track   = Track::select('tracks.*','bill_lading','organization_name')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('tracks.current_status','NOT LIKE','Crossed Border')
                    ->where('tracks.current_status','NOT LIKE','File Closed')
                    ->whereBetween('tracks.updated_at',array($last_30, $now))
                    ->where('tracks.agent_id','=',$agent_id)
                    ->latest('tracks.created_at')
                    ->get();

                $consignee  = Track::all();

                $expenses   = Expenses::with('container')->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
                $normal     = NormalExpenses::where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

                return view('reports.finance_not_crossed_report', compact('expenses','normal','track','agent','consignee'));

            }
            else
            {
                $last_30 =Carbon::today()->subDays(1)->format('Y-m-d H:i:s');

                $now    = Carbon::today()->addDay(1)->format('Y-m-d H:i:s');

                $agent  =Agent::latest()->get();

                $track   = Track::select('tracks.*','bill_lading','organization_name')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('tracks.current_status','NOT LIKE','Crossed Border')
                    ->where('tracks.current_status','NOT LIKE','File Closed')
                    ->whereBetween('tracks.updated_at',array($last_30, $now))
                    ->latest('tracks.created_at')
                    ->get();


                $consignee  = Track::all();

                $expenses   = Expenses::with('container')->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
                $normal     = NormalExpenses::where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

                return view('reports.finance_not_crossed_report', compact('expenses','normal','track','agent','consignee'));

            }

        }

        else
        {
            return JsonResponse::create(['error' => 'access-denied'],401);
        }

    }


    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|static
     *
     */
    public function NewConsignment()
    {

        $from = '2021';
        $to = '2030';
        $user   =Auth::user();

        if($user->hasRole('admin'))
        {

            $last_30 =Carbon::today()->subDays(1)->format('Y-m-d H:i:s');

            $now    = Carbon::today()->addDay()->format('Y-m-d H:i:s');
            $agent =Agent::latest()->get();
            $track   = Track::select('tracks.*','bill_lading','organization_name')
                        ->join('laddings','laddings.track_id','=','tracks.id')
                        ->join('agents','agents.id','=','tracks.agent_id')
                        ->whereBetween('tracks.created_at',array($last_30, $now))
                        ->orderByDesc("tracks.created_at")
                        ->get();
            $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
            $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

            return view('order.list_order', compact('expenses','normal','track','agent'));
        }
        else if($user->hasRole('super'))
        {
            $last_30 =Carbon::today()->subDays(1)->format('Y-m-d H:i:s');

            $now    = Carbon::today()->format('Y-m-d H:i:s');
            $agent =Agent::latest()->get();
            $track   = Track::select('tracks.*','bill_lading','organization_name')
                        ->join('laddings','laddings.track_id','=','tracks.id')
                        ->join('agents','agents.id','=','tracks.agent_id')
                        ->whereBetween('tracks.created_at',array($last_30, $now))
                        ->orderByDesc("tracks.created_at")
                        ->get();
            $expenses = Expenses::with('container')->where('finance_status','LIKE','pending')->count();

            $normal = NormalExpenses::where('finance_status','LIKE','pending')->count();

            return view('order.super_list_order', compact('expenses','normal','track','agent'));
        }
        else if($user->hasRole('documentation'))
        {
            $last_30 =Carbon::today()->subDays(1)->format('Y-m-d H:i:s');

            $now    = Carbon::today()->format('Y-m-d H:i:s');
            $agent =Agent::latest()->get();
            $track   = Track::select('tracks.*','bill_lading','organization_name')
                        ->join('laddings','laddings.track_id','=','tracks.id')
                        ->join('agents','agents.id','=','tracks.agent_id')
                        ->whereBetween('tracks.created_at',array($last_30, $now))
                        ->orderByDesc("tracks.created_at")
                        ->get();
            $expenses = Expenses::with('container')->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
            $normal = NormalExpenses::where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

            return view('order.documentation_list_order', compact('expenses','normal','track','agent'));
        }
        else if($user->hasRole('declaration'))
        {
            $last_30 =Carbon::today()->subDays(1)->format('Y-m-d H:i:s');

            $now    = Carbon::today()->format('Y-m-d H:i:s');
            $agent =Agent::latest()->get();
            $track   = Track::select('tracks.*','bill_lading','organization_name')
                        ->join('laddings','laddings.track_id','=','tracks.id')
                        ->join('agents','agents.id','=','tracks.agent_id')
                        ->whereBetween('tracks.created_at',array($last_30, $now))
                        ->orderByDesc("tracks.created_at")
                        ->get();
            $expenses = Expenses::with('container')->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
            $normal = NormalExpenses::where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

            return view('order.declaration_list_order', compact('expenses','normal','track','agent'));
        }
        else if($user->hasRole('shipping'))
        {
            $last_30 =Carbon::today()->subDays(1)->format('Y-m-d H:i:s');

            $now    = Carbon::today()->format('Y-m-d H:i:s');
            $agent =Agent::latest()->get();
            $track   = Track::select('tracks.*','bill_lading','organization_name')
                        ->join('laddings','laddings.track_id','=','tracks.id')
                        ->join('agents','agents.id','=','tracks.agent_id')
                        ->whereBetween('tracks.created_at',array($last_30, $now))
                        ->orderByDesc("tracks.created_at")
                        ->get();
            $expenses = Expenses::with('container')->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
            $normal = NormalExpenses::where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

            return view('order.shipping_list_order', compact('expenses','normal','track','agent'));
        }
        elseif ($user->hasRole('sales'))
        {
            $last_30 =Carbon::today()->subDays(1)->format('Y-m-d H:i:s');

            $now    = Carbon::today()->format('Y-m-d H:i:s');
            $agent =Agent::latest()->get();
            $track   = Track::select('tracks.*','bill_lading','organization_name')
                        ->join('laddings','laddings.track_id','=','tracks.id')
                        ->join('agents','agents.id','=','tracks.agent_id')
                        ->whereBetween('tracks.created_at',array($last_30, $now))
                        ->orderByDesc("tracks.created_at")
                        ->get();
            $expenses = Expenses::with('container')->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
            $normal = NormalExpenses::where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

            return view('staff.sales_list_order', compact('expenses','normal','track','agent'));
        }
        elseif ($user->hasRole('finance'))
        {

            $last_30 =Carbon::today()->subDays(1)->format('Y-m-d H:i:s');

            $now    = Carbon::today()->format('Y-m-d H:i:s');
            $agent =Agent::latest()->get();
            $track   = Track::select('tracks.*','bill_lading','organization_name')
                        ->join('laddings','laddings.track_id','=','tracks.id')
                        ->join('agents','agents.id','=','tracks.agent_id')
                        ->whereBetween('tracks.created_at',array($last_30, $now))
                        ->orderByDesc("tracks.created_at")
                        ->get();
            $expenses = Expenses::with('container')->where('finance_status','LIKE','pending')->count();

            $normal = NormalExpenses::where('finance_status','LIKE','pending')->count();

            return view('finance.finance_list_order', compact('expenses','normal','track','agent'));
        }
        else
        {
            return JsonResponse::create(['error' => 'access-denied'],401);
        }

    }
}
