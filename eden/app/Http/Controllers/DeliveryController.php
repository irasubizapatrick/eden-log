<?php

namespace App\Http\Controllers;

use App\Container;
use App\Delievery;
use App\Driver;
use App\Expenses;
use App\Ladding;
use App\NormalExpenses;
use App\Track;
use App\Transporter;
use App\Trucking;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Session;
use DB;
use Auth;


class DeliveryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user   =Auth::user();
        if($user->hasRole('admin'))
        {
            $from = '2021';
            $to = '2030';

            $cont_bill = Transporter::with('container')->whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->latest()->get();


            $delivery = Delievery::select('delieveries.*','bill_lading','loading_date','cont_no','driver_name','transporter_name','trucking_number')
                ->join('containers','containers.id','=','delieveries.container_id')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->join('drivers','drivers.id','=','delieveries.driver_id')
                ->join('transporters','transporters.id','=','delieveries.transporter_id')
                ->whereBetween(DB::raw('YEAR(delieveries.created_at)'),[$from, $to])
                ->latest()->paginate(10);

            $ladding = DB::table("laddings")->whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->latest('laddings.created_at')->pluck("bill_lading", "id");
            $drivers = Driver::whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->get();
            $transporter = Transporter::whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->get();
            $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
            $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

            return view('delivery.delivery', compact('expenses','normal','delivery', 'ladding', 'drivers', 'transporter','cont_bill'));

        }
        else if($user->hasRole('super')) {
            $cont_bill = Transporter::with('container')->latest()->get();


            $delivery = Delievery::select('delieveries.*','bill_lading','loading_date','cont_no','driver_name','transporter_name','trucking_number')
                ->join('containers','containers.id','=','delieveries.container_id')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->join('drivers','drivers.id','=','delieveries.driver_id')
                ->join('transporters','transporters.id','=','delieveries.transporter_id')
                ->latest()->paginate(10);
            $ladding = DB::table("laddings")->latest('laddings.created_at')->pluck("bill_lading", "id");
            $drivers = Driver::all();
            $transporter = Transporter::all();
            $expenses = Expenses::with('container')->where('finance_status','LIKE','pending')->count();

            $normal = NormalExpenses::where('finance_status','LIKE','pending')->count();

            return view('delivery.super_delivery', compact('expenses','normal','delivery', 'ladding', 'drivers', 'transporter','cont_bill'));

        }
        else if($user->hasRole('shipping')) {
            $from = '2021';
            $to = '2030';

            $cont_bill = Transporter::with('container')->whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->latest()->get();


            $delivery = Delievery::select('delieveries.*','bill_lading','loading_date','cont_no','driver_name','transporter_name','trucking_number')
                ->join('containers','containers.id','=','delieveries.container_id')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->join('drivers','drivers.id','=','delieveries.driver_id')
                ->join('transporters','transporters.id','=','delieveries.transporter_id')
                ->whereBetween(DB::raw('YEAR(delieveries.created_at)'),[$from, $to])
                ->latest()->paginate(10);

            $ladding = DB::table("laddings")->whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->latest('laddings.created_at')->pluck("bill_lading", "id");
            $drivers = Driver::whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->get();
            $transporter = Transporter::whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->get();
            $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
            $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

            return view('delivery.shipping_delivery', compact('expenses','normal','delivery', 'ladding', 'drivers', 'transporter','cont_bill'));

        }
        else if($user->hasRole('sales'))
        {
            $from = '2021';
            $to = '2030';

            $cont_bill = Transporter::with('container')->whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->latest()->get();


            $delivery = Delievery::select('delieveries.*','bill_lading','loading_date','cont_no','driver_name','transporter_name','trucking_number')
                ->join('containers','containers.id','=','delieveries.container_id')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->join('drivers','drivers.id','=','delieveries.driver_id')
                ->join('transporters','transporters.id','=','delieveries.transporter_id')
                ->whereBetween(DB::raw('YEAR(delieveries.created_at)'),[$from, $to])
                ->latest()->paginate(10);

            $ladding = DB::table("laddings")->whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->latest('laddings.created_at')->pluck("bill_lading", "id");
            $drivers = Driver::whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->get();
            $transporter = Transporter::whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->get();
            $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
            $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

            return view('delivery.sales_delivery', compact('expenses','normal','delivery', 'ladding', 'cont_bill','drivers', 'transporter'));

        }
        else if($user->hasRole('finance'))
        {
            $from = '2021';
            $to = '2030';

            $cont_bill = Transporter::with('container')->whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->latest()->get();


            $delivery = Delievery::select('delieveries.*','bill_lading','loading_date','cont_no','driver_name','transporter_name','trucking_number')
                ->join('containers','containers.id','=','delieveries.container_id')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->join('drivers','drivers.id','=','delieveries.driver_id')
                ->join('transporters','transporters.id','=','delieveries.transporter_id')
                ->whereBetween(DB::raw('YEAR(delieveries.created_at)'),[$from, $to])
                ->latest()->paginate(10);

            $ladding = DB::table("laddings")->whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->latest('laddings.created_at')->pluck("bill_lading", "id");
            $drivers = Driver::whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->get();
            $transporter = Transporter::whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->get();
            $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
            $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

            return view('delivery.finance_delivery', compact('expenses','normal','delivery', 'ladding', 'cont_bill','drivers', 'transporter'));

        }
        else
        {
            return JsonResponse::create(['error' => 'access-denied'],401);
        }
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function ViewDelivery(Request $request, $id)
    {
        $user   =Auth::user();
        if($user->hasRole('admin')) {
            $delivery = Delievery::select('delieveries.*','bill_lading','loading_date','cont_no','driver_name','transporter_name','trucking_number','file_no','telephone','terminal','trucking_number','driver_name','driver_contact','driver_license','driver_passport','client_name','destination','shipper','special_instruction','shipping_line','organization_name','telephone','remarks','commodity','type')
                ->join('containers','containers.id','=','delieveries.container_id')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->join('tracks','tracks.id','=','laddings.track_id')
                ->join('agents','agents.id','=','tracks.agent_id')
                ->join('drivers','drivers.id','=','delieveries.driver_id')
                ->join('transporters','transporters.id','=','delieveries.transporter_id')
                ->where('delieveries.id','=',$id)
                ->get();
            $expenses = Expenses::with('container')->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
            $normal = NormalExpenses::where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

            return view('delivery.view_delivery', compact('expenses','normal','delivery'));
        }
        elseif($user->hasRole('super')) {
            $delivery = Delievery::select('delieveries.*','bill_lading','loading_date','cont_no','driver_name','transporter_name','trucking_number','file_no','telephone','terminal','trucking_number','driver_name','driver_contact','driver_license','driver_passport','client_name','destination','shipper','special_instruction','shipping_line','organization_name','telephone','remarks','commodity','type')
                ->join('containers','containers.id','=','delieveries.container_id')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->join('tracks','tracks.id','=','laddings.track_id')
                ->join('agents','agents.id','=','tracks.agent_id')
                ->join('drivers','drivers.id','=','delieveries.driver_id')
                ->join('transporters','transporters.id','=','delieveries.transporter_id')
                ->where('delieveries.id','=',$id)
                ->get();
            $expenses = Expenses::with('container')->where('finance_status','LIKE','pending')->count();

            $normal = NormalExpenses::where('finance_status','LIKE','pending')->count();

            return view('delivery.super_view_delivery', compact('expenses','normal','delivery'));
        }
        elseif($user->hasRole('shipping')) {
            $delivery = Delievery::select('delieveries.*','bill_lading','loading_date','cont_no','driver_name','transporter_name','trucking_number','file_no','telephone','terminal','trucking_number','driver_name','driver_contact','driver_license','driver_passport','client_name','destination','shipper','special_instruction','shipping_line','organization_name','telephone','remarks','commodity','type')
                ->join('containers','containers.id','=','delieveries.container_id')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->join('tracks','tracks.id','=','laddings.track_id')
                ->join('agents','agents.id','=','tracks.agent_id')
                ->join('drivers','drivers.id','=','delieveries.driver_id')
                ->join('transporters','transporters.id','=','delieveries.transporter_id')
                ->where('delieveries.id','=',$id)
                ->get();
            $expenses = Expenses::with('container')->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
            $normal = NormalExpenses::where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

            return view('delivery.shipping_view_delivery', compact('expenses','normal','delivery'));
        }
        else if($user->hasRole('sales'))
        {
            $delivery = Delievery::select('delieveries.*','bill_lading','loading_date','cont_no','driver_name','transporter_name','trucking_number','file_no','telephone','terminal','trucking_number','driver_name','driver_contact','driver_license','driver_passport','client_name','destination','shipper','special_instruction','shipping_line','organization_name','telephone','remarks','commodity','type')
                ->join('containers','containers.id','=','delieveries.container_id')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->join('tracks','tracks.id','=','laddings.track_id')
                ->join('agents','agents.id','=','tracks.agent_id')
                ->join('drivers','drivers.id','=','delieveries.driver_id')
                ->join('transporters','transporters.id','=','delieveries.transporter_id')
                ->where('delieveries.id','=',$id)
                ->get();
            $expenses = Expenses::with('container')->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
            $normal = NormalExpenses::where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

            return view('delivery.sales_view_delivery', compact('expenses','normal','delivery'));
        }
        else if($user->hasRole('finance'))
        {
            $delivery = Delievery::select('delieveries.*','bill_lading','loading_date','cont_no','driver_name','transporter_name','trucking_number','file_no','telephone','terminal','trucking_number','driver_name','driver_contact','driver_license','driver_passport','client_name','destination','shipper','special_instruction','shipping_line','organization_name','telephone','remarks','commodity','type')
                ->join('containers','containers.id','=','delieveries.container_id')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->join('tracks','tracks.id','=','laddings.track_id')
                ->join('agents','agents.id','=','tracks.agent_id')
                ->join('drivers','drivers.id','=','delieveries.driver_id')
                ->join('transporters','transporters.id','=','delieveries.transporter_id')
                ->where('delieveries.id','=',$id)
                ->get();
            $expenses = Expenses::with('container')->where('finance_status','LIKE','pending')->count();

            $normal = NormalExpenses::where('finance_status','LIKE','pending')->count();

            return view('delivery.finance_view', compact('expenses','normal','delivery'));
        }
        else
        {
            return JsonResponse::create(['error' => 'access-denied'],401);
        }
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|static
     *
     */
    public function searchDeliveryN(Request $request)
    {

        $get_bl = $request->get('container_id');
        $user   =Auth::user();
        if($user->hasRole('admin')) {

            $from = '2021';
            $to = '2030';
            $cont_bill = Transporter::with('container')->whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->latest()->get();


            $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
            $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

            $delivery = Delievery::select('delieveries.*','bill_lading','loading_date','cont_no','driver_name','transporter_name','trucking_number')
                ->join('containers','containers.id','=','delieveries.container_id')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->join('drivers','drivers.id','=','delieveries.driver_id')
                ->join('transporters','transporters.id','=','delieveries.transporter_id')
                ->whereBetween(DB::raw('YEAR(delieveries.created_at)'),[$from, $to])
                ->where('containers.id', '=', $get_bl)
                ->latest()->paginate(10);


            $ladding = DB::table("laddings")->latest('laddings.created_at')->pluck("bill_lading", "id");
            $drivers = Driver::whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->get();
            $transporter = Transporter::whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->get();
            return view('delivery.delivery', compact('expenses','normal','delivery', 'ladding', 'drivers', 'transporter','cont_bill'));

        }
        else if($user->hasRole('super')) {
            $cont_bill = Transporter::with('container')->latest()->get();
            $expenses = Expenses::with('container')->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
            $normal = NormalExpenses::where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();


            $delivery = Delievery::select('delieveries.*','bill_lading','loading_date','cont_no','driver_name','transporter_name','trucking_number')
                ->join('containers','containers.id','=','delieveries.container_id')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->join('drivers','drivers.id','=','delieveries.driver_id')
                ->join('transporters','transporters.id','=','delieveries.transporter_id')
                ->where('containers.id', '=', $get_bl)
                ->latest()->paginate(10);


            $ladding = DB::table("laddings")->latest('laddings.created_at')->pluck("bill_lading", "id");
            $drivers = Driver::all();
            $transporter = Transporter::all();
            return view('delivery.super_delivery', compact('expenses','normal','delivery', 'ladding', 'drivers', 'transporter','cont_bill'));

        }
        else if($user->hasRole('shipping')) {
            $from = '2021';
            $to = '2030';
            $cont_bill = Transporter::with('container')->whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->latest()->get();


            $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
            $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

            $delivery = Delievery::select('delieveries.*','bill_lading','loading_date','cont_no','driver_name','transporter_name','trucking_number')
                ->join('containers','containers.id','=','delieveries.container_id')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->join('drivers','drivers.id','=','delieveries.driver_id')
                ->join('transporters','transporters.id','=','delieveries.transporter_id')
                ->whereBetween(DB::raw('YEAR(delieveries.created_at)'),[$from, $to])
                ->where('containers.id', '=', $get_bl)
                ->latest()->paginate(10);


            $ladding = DB::table("laddings")->latest('laddings.created_at')->pluck("bill_lading", "id");
            $drivers = Driver::whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->get();
            $transporter = Transporter::whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->get();
            return view('delivery.shipping_delivery', compact('expenses','normal','delivery', 'ladding', 'drivers', 'transporter','cont_bill'));

        }
        else if($user->hasRole('sales'))
        {
            $from = '2021';
            $to = '2030';
            $cont_bill = Transporter::with('container')->whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->latest()->get();


            $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
            $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

            $delivery = Delievery::select('delieveries.*','bill_lading','loading_date','cont_no','driver_name','transporter_name','trucking_number')
                ->join('containers','containers.id','=','delieveries.container_id')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->join('drivers','drivers.id','=','delieveries.driver_id')
                ->join('transporters','transporters.id','=','delieveries.transporter_id')
                ->whereBetween(DB::raw('YEAR(delieveries.created_at)'),[$from, $to])
                ->where('containers.id', '=', $get_bl)
                ->latest()->paginate(10);


            $ladding = DB::table("laddings")->latest('laddings.created_at')->pluck("bill_lading", "id");
            $drivers = Driver::whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->get();
            $transporter = Transporter::whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->get();
            return view('delivery.sales_delivery', compact('expenses','normal','delivery', 'ladding', 'cont_bill','drivers', 'transporter'));

        }
        else if($user->hasRole('finance'))
        {
            $from = '2021';
            $to = '2030';
            $cont_bill = Transporter::with('container')->whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->latest()->get();


            $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
            $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

            $delivery = Delievery::select('delieveries.*','bill_lading','loading_date','cont_no','driver_name','transporter_name','trucking_number')
                ->join('containers','containers.id','=','delieveries.container_id')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->join('drivers','drivers.id','=','delieveries.driver_id')
                ->join('transporters','transporters.id','=','delieveries.transporter_id')
                ->whereBetween(DB::raw('YEAR(delieveries.created_at)'),[$from, $to])
                ->where('containers.id', '=', $get_bl)
                ->latest()->paginate(10);


            $ladding = DB::table("laddings")->latest('laddings.created_at')->pluck("bill_lading", "id");
            $drivers = Driver::whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->get();
            $transporter = Transporter::whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->get();
            return view('delivery.finance_delivery', compact('expenses','normal','delivery', 'ladding', 'cont_bill','drivers', 'transporter'));

        }
        else
        {
            return JsonResponse::create(['error' => 'access-denied'],401);
        }
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        $container_no = $request->get('container_id');

        $transporter_id = Transporter::where('container_id','=',$container_no)->first();
        $driver_id      = Driver::where('container_id','=',$container_no)->first();

        $delivery= Delievery::where('container_id','=',$container_no)->first();

        $ladding = Container::where('id','=',$container_no)->first()->ladding_id;

//        return response()->json($ladding);
        $user = Auth::user();
        if ($user->hasRole('admin') || $user->hasRole('sales') ||  $user->hasRole('shipping')) {
            if (!$delivery)
            {
                $user_id = Auth::id();

                $request->merge(['user_id' => $user_id,'driver_id'=>$driver_id->id, 'transporter_id' =>$transporter_id->id]);
                $all_data = Delievery::create($request->all());
                if($all_data->save());


                //

                $current_status = 'Loading';
                $user_id =Auth::id();
                $cont_id = $request->get('container_id');
                $processing_date    = $request->get('loading_date');
                $request->merge(['user_id' => $user_id , 'cont_id' =>$cont_id , 'ladding_id' =>$ladding, 'processing_status' => $current_status ,'processing_date' =>$processing_date]);
                $order =  Trucking::create($request->all());
                if ($order->save());

                $track_id = Ladding::where('id','=',$ladding)->first()->track_id;

                $request->merge(['current_status' => $current_status,'other_comment' =>$order->other_comment]);
                $order = Track::findOrFail($track_id);
                $data = $request->all();
                $order->update($data);
                Session::flash('delete', 'Order  status created successfully');
                return redirect('/all_order');
                //
                Session::flash('updated', 'Delivery Generated Successful');
                return redirect('/delivery/note');

            }
            else
            {
                return view('delivery.exist');
            }


        }
        else
        {
            return JsonResponse::create(['error' => 'access-denied'], 401);
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Database\Eloquent\Model|null|static
     */
    public function ContainerDetails($id)
    {
        return  Container::select('containers.*','transporter_name','bill_lading')
                        ->join('transporters','transporters.container_id','=','containers.id')
                        ->join('laddings','laddings.id','=','containers.ladding_id')
                        ->where('containers.id','=',$id);

    }


    public function GetMoreDetails($id)
    {
        return  Container::select('containers.*','trucking_number','transporter_name','bill_lading','driver_name','loading_date')
            ->join('laddings','laddings.id','=','containers.ladding_id')
            ->join('transporters','transporters.container_id','=','containers.id')
            ->join('drivers','drivers.container_id','=','containers.id')
            ->where('transporters.deleted_at', NULL)
            ->where('drivers.deleted_at', NULL)
            ->where('containers.id','=',$id)
            ->first();
    }



    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $user = Auth::user();
        if ($user->hasRole('admin') || $user->hasRole('sales') || $user->hasRole('shipping')) {
            $transport_id = $request->get('transporter_id');
            $user_id = Auth::id();
            $request->merge(['user_id' => $user_id,]);
            $order = Delievery::findOrFail($id);
            $all_data = $request->all();
            if($order->update($all_data));
            $transporter = Transporter::findOrFail($transport_id);
            $transporter->update($all_data);
            Session::flash('updated', 'Delivery Updated Successful');
            return redirect('/delivery/note');
        } else {
            return JsonResponse::create(['error' => 'access-denied'], 401);
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Exception
     */
    public function destroy($id)
    {

        $user = Auth::user();
        if ($user->hasRole('admin') || $user->hasRole('sales')  || $user->hasRole('shipping')) {
            $training = Delievery::findOrfail($id);
            $training->delete();

            Session::flash('delete', 'Delivery Deleted Successful');
            return redirect('/delivery/note');
        }

    }
}
