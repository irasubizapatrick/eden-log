<?php

namespace App\Http\Controllers;

use App\Container;
use App\Driver;
use App\Expenses;
use App\NormalExpenses;
use App\Track;
use App\Trucking;
use Illuminate\Http\JsonResponse;
use Session;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DriverController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user   =Auth::user();
        if($user->hasRole('admin'))
        {
            $from = '2021';
            $to = '2030';

            $ladding = DB::table("laddings")->whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->latest('laddings.created_at')->pluck("bill_lading","id");
            $bill   =Container::whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->get();


            $driver_data    =Driver::orderby('created_at','DESC')->get();
            $driver = Driver::Select('drivers.*','bill_lading','cont_no')
                            ->join('containers','containers.id','=','drivers.container_id')
                            ->join('laddings','laddings.id','=','containers.ladding_id')
                            ->whereBetween(DB::raw('YEAR(drivers.created_at)'), [$from, $to])
                            ->orderByDesc("drivers.created_at")->paginate(10);

            $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
            $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

            return view('driver.list_driver',compact('expenses','normal','bill','driver','ladding','driver_data'));
        }
        elseif($user->hasRole('super'))
        {
            $ladding = DB::table("laddings")->latest('laddings.created_at')->pluck("bill_lading","id");
            $bill   =Container::all();
            $driver_data    =Driver::orderby('created_at','DESC')->get();
            $driver = Driver::Select('drivers.*','bill_lading','cont_no')
                        ->join('containers','containers.id','=','drivers.container_id')
                        ->join('laddings','laddings.id','=','containers.ladding_id')
                        ->orderByDesc("drivers.created_at")->paginate(10);
            $expenses = Expenses::with('container')->where('finance_status','LIKE','pending')->count();

            $normal = NormalExpenses::where('finance_status','LIKE','pending')->count();

            return view('driver.super_list_driver',compact('expenses','normal','bill','driver','ladding','driver_data'));
        }
        elseif($user->hasRole('shipping'))
        {
            $from = '2021';
            $to = '2030';
            $ladding = DB::table("laddings")->whereBetween(DB::raw('YEAR(laddings.created_at)'), [$from, $to])->latest('laddings.created_at')->pluck("bill_lading","id");
            $bill   =Container::all();
            $driver_data    =Driver::whereBetween(DB::raw('YEAR(drivers.created_at)'), [$from, $to])->orderby('created_at','DESC')->get();
            $driver = Driver::Select('drivers.*','bill_lading','cont_no')
                        ->join('containers','containers.id','=','drivers.container_id')
                        ->join('laddings','laddings.id','=','containers.ladding_id')
                        ->whereBetween(DB::raw('YEAR(drivers.created_at)'), [$from, $to])
                        ->orderByDesc("drivers.created_at")->paginate(10);

            $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
            $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

            return view('driver.shipping_list_driver',compact('expenses','normal','bill','driver','ladding','driver_data'));
        }
        elseif($user->hasRole('declaration'))
        {
            $from = '2021';
            $to = '2030';
            $ladding = DB::table("laddings")->whereBetween(DB::raw('YEAR(laddings.created_at)'), [$from, $to])->latest('laddings.created_at')->pluck("bill_lading","id");
            $bill   =Container::all();
            $driver_data    =Driver::whereBetween(DB::raw('YEAR(drivers.created_at)'), [$from, $to])->orderby('created_at','DESC')->get();
            $driver = Driver::Select('drivers.*','bill_lading','cont_no')
                ->join('containers','containers.id','=','drivers.container_id')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->whereBetween(DB::raw('YEAR(drivers.created_at)'), [$from, $to])
                ->orderByDesc("drivers.created_at")->paginate(10);

            $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
            $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

            return view('driver.declaration_list_driver',compact('expenses','normal','bill','driver','ladding','driver_data'));
        }
        elseif($user->hasRole('finance'))
        {
            $from = '2021';
            $to = '2030';
            $ladding = DB::table("laddings")->whereBetween(DB::raw('YEAR(laddings.created_at)'), [$from, $to])->latest('laddings.created_at')->pluck("bill_lading","id");
            $bill   =Container::all();
            $driver_data    =Driver::whereBetween(DB::raw('YEAR(drivers.created_at)'), [$from, $to])->orderby('created_at','DESC')->get();
            $driver = Driver::Select('drivers.*','bill_lading','cont_no')
                ->join('containers','containers.id','=','drivers.container_id')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->whereBetween(DB::raw('YEAR(drivers.created_at)'), [$from, $to])
                ->orderByDesc("drivers.created_at")->paginate(10);

            $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
            $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

            return view('driver.finance_list_driver',compact('expenses','normal','bill','driver','ladding','driver_data'));
        }
        elseif($user->hasRole('sales'))
        {
            $from = '2021';
            $to = '2030';
            $ladding = DB::table("laddings")->whereBetween(DB::raw('YEAR(laddings.created_at)'), [$from, $to])->latest('laddings.created_at')->pluck("bill_lading","id");
            $bill   =Container::all();
            $driver_data    =Driver::whereBetween(DB::raw('YEAR(drivers.created_at)'), [$from, $to])->orderby('created_at','DESC')->get();
            $driver = Driver::Select('drivers.*','bill_lading','cont_no')
                ->join('containers','containers.id','=','drivers.container_id')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->whereBetween(DB::raw('YEAR(drivers.created_at)'), [$from, $to])
                ->orderByDesc("drivers.created_at")->paginate(10);

            $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
            $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

            return view('driver.operation_list_drivers',compact('expenses','normal','bill','driver','ladding','driver_data'));
        }
 
        else
        {
            return JsonResponse::create(['error' => 'access-denied'],401);
        }

    }

    /**
     *
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */



    public function store(Request $request)
    {

        $user   =Auth::user();
        if($user->hasRole('admin') ||  $user->hasRole('sales') || $user->hasRole('finance') || $user->hasRole('shipping') ||  $user->hasRole('declaration'))
        {
        $user_id = Auth::id();
        $request->merge(['user_id' => $user_id]);
        $order =  Driver::create($request->all());

        $driving_copy          = $request->file('driving_copy');
        $passport_copy         = $request->file('passport_copy');
        $movement_sheet        = $request->file('movement_sheet');


        if(isset($driving_copy)) {
            if (isset($driving_copy)) {
                $extension2 = $request->file('driving_copy')->getClientOriginalExtension();
            }
            $sha = 'driving_copy' . md5(time());
            if(isset($extension2)){
                $yellow_card_file = date('Y').$sha . "gfgsgf." . $extension2;
            }
            $destination_path1 = 'archives/';
            if (empty($destination_path1)) {
                File::makeDirectory($destination_path1, 0775, true, true);
            }
            if(isset($yellow_card_file)){
                $order->driving_copy = $yellow_card_file;
                $request->file('driving_copy')->move($destination_path1, $yellow_card_file);
                $order->save();
            }
        }

        if(isset($passport_copy)) {
            if (isset($passport_copy)) {
                $extension2 = $request->file('passport_copy')->getClientOriginalExtension();
            }
            $sha = 'passport_copy' . md5(time());
            if(isset($extension2)){
                $filename4 = date('Y').$sha . "gfgsgf." . $extension2;
            }
            $destination_path1 = 'archives/';
            if (empty($destination_path1)) {
                File::makeDirectory($destination_path1, 0775, true, true);
            }
            if(isset($filename4)){
                $order->passport_copy = $filename4;
                $request->file('passport_copy')->move($destination_path1, $filename4);
                $order->save();
            }

        }
        if(isset($movement_sheet)) {
                if (isset($movement_sheet)) {
                    $extension2 = $request->file('movement_sheet')->getClientOriginalExtension();
                }
                $sha = 'movement_sheet' . md5(time());
                if(isset($extension2)){
                    $filename5 = date('Y').$sha . "gfgsgf." . $extension2;
                }
                $destination_path1 = 'archives/';
                if (empty($destination_path1)) {
                    File::makeDirectory($destination_path1, 0775, true, true);
                }
                if(isset($filename5)){
                    $order->movement_sheet = $filename5;
                    $request->file('movement_sheet')->move($destination_path1, $filename5);
                    $order->save();
                }

            }

        $all_data =  $request->all();

        $all_data['driving_copy']               = !empty($yellow_card_file) ? $yellow_card_file  : $order->driving_copy;
        $all_data['passport_copy']              = !empty($filename4) ? $filename4        : $order->passport_copy;
        $all_data['movement_sheet']             = !empty($filename5) ? $filename5        : $order->movement_sheet;
        $order->update($all_data);
        Session::flash('message', 'Driver Assigned to Container   Successful ');
        return redirect('/all_driver');
        }
        else
        {
            return JsonResponse::create(['error' => 'access-denied'],401);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function searchBlDriver(Request $request )
    {


        $driver_name        = $request->get('driver_name');
        $driver_license     = $request->get('driver_license');
        $driver_passport    = $request->get('driver_passport');
        $get_bl             = $request->get('ladding_id');
        $from = '2021';
        $to = '2030';
        $user   =Auth::user();
        if($user->hasRole('admin'))
        {

            if($get_bl !='')
            {
                $ladding = DB::table("laddings")->whereBetween(DB::raw('YEAR(laddings.created_at)'), [$from, $to])->pluck("bill_lading","id");
                $driver_data    =Driver::whereBetween(DB::raw('YEAR(drivers.created_at)'), [$from, $to])->orderby('created_at','DESC')->get();
                $bill   =Container::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->get();

                $driver = Driver::select('drivers.*','cont_no','bill_lading')
                            ->join('containers','containers.id','=','drivers.container_id')
                            ->join('laddings','laddings.id','=','containers.ladding_id')
                             ->whereBetween(DB::raw('YEAR(drivers.created_at)'), [$from, $to])
                            ->where('laddings.id','=',$get_bl)
                            ->paginate(10);
                $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
                $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

                return view('driver.list_driver',compact('expenses','normal','bill','driver','ladding','driver_data'));
            }
            elseif ($driver_name !='')
            {
                $ladding = DB::table("laddings")->whereBetween(DB::raw('YEAR(laddings.created_at)'), [$from, $to])->pluck("bill_lading","id");
                $driver_data    =Driver::whereBetween(DB::raw('YEAR(drivers.created_at)'), [$from, $to])->orderby('created_at','DESC')->get();
                $bill   =Container::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->get();

                $driver = Driver::select('drivers.*','cont_no','bill_lading')
                            ->join('containers','containers.id','=','drivers.container_id')
                            ->join('laddings','laddings.id','=','containers.ladding_id')
                            ->whereBetween(DB::raw('YEAR(drivers.created_at)'),[$from, $to])
                            ->where('drivers.id','=',$driver_name)
                            ->paginate(10);
                $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
                $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

                return view('driver.list_driver',compact('expenses','normal','bill','driver','ladding','driver_data'));
            }
            elseif ($driver_passport !='')
            {
                $ladding = DB::table("laddings")->whereBetween(DB::raw('YEAR(laddings.created_at)'), [$from, $to])->pluck("bill_lading","id");
                $driver_data    =Driver::whereBetween(DB::raw('YEAR(drivers.created_at)'), [$from, $to])->orderby('created_at','DESC')->get();
                $bill   =Container::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->get();

                $driver = Driver::select('drivers.*','cont_no','bill_lading')
                            ->join('containers','containers.id','=','drivers.container_id')
                            ->join('laddings','laddings.id','=','containers.ladding_id')
                            ->whereBetween(DB::raw('YEAR(drivers.created_at)'),[$from, $to])
                            ->where('drivers.id','=',$driver_passport)
                            ->paginate(10);
                $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
                $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

                return view('driver.list_driver',compact('expenses','normal','bill','driver','ladding','driver_data'));
            }
            elseif ($driver_license !='')
            {
                $ladding = DB::table("laddings")->whereBetween(DB::raw('YEAR(laddings.created_at)'), [$from, $to])->pluck("bill_lading","id");
                $driver_data    =Driver::whereBetween(DB::raw('YEAR(drivers.created_at)'), [$from, $to])->orderby('created_at','DESC')->get();
                $bill   =Container::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->get();

                $driver = Driver::select('drivers.*','cont_no','bill_lading')
                            ->join('containers','containers.id','=','drivers.container_id')
                            ->join('laddings','laddings.id','=','containers.ladding_id')
                            ->whereBetween(DB::raw('YEAR(drivers.created_at)'),[$from, $to])
                            ->where('drivers.id','=',$driver_license)
                            ->paginate(10);
                $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
                $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

                return view('driver.list_driver',compact('expenses','normal','bill','driver','ladding','driver_data'));
            }

        }
        elseif($user->hasRole('shipping'))
        {

            if($get_bl !='')
            {
                $driver_data    =Driver::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->orderby('created_at','DESC')->get();
                $ladding = DB::table("laddings")->whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->pluck("bill_lading","id");

                $bill   =Container::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->get();

                $driver = Driver::select('drivers.*','cont_no','bill_lading')
                    ->join('containers','containers.id','=','drivers.container_id')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->whereBetween(DB::raw('YEAR(drivers.created_at)'), [$from, $to])
                    ->where('laddings.id','=',$get_bl)
                    ->paginate(10);
                $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
                $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

                return view('driver.shipping_list_driver',compact('expenses','normal','bill','driver','ladding','driver_data'));
            }
            elseif ($driver_name !='')
            {
                $driver_data    =Driver::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->orderby('created_at','DESC')->get();
                $ladding = DB::table("laddings")->whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->pluck("bill_lading","id");

                $bill   =Container::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->get();

                $driver = Driver::select('drivers.*','cont_no','bill_lading')
                    ->join('containers','containers.id','=','drivers.container_id')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->whereBetween(DB::raw('YEAR(drivers.created_at)'), [$from, $to])
                    ->where('drivers.id','=',$driver_name)
                    ->paginate(10);
                $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
                $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

                return view('driver.shipping_list_driver',compact('expenses','normal','bill','driver','ladding','driver_data'));
            }
            elseif ($driver_passport !='')
            {
                $driver_data    =Driver::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->orderby('created_at','DESC')->get();
                $ladding = DB::table("laddings")->whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->pluck("bill_lading","id");

                $bill   =Container::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->get();
                $driver = Driver::select('drivers.*','cont_no','bill_lading')
                    ->join('containers','containers.id','=','drivers.container_id')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->whereBetween(DB::raw('YEAR(drivers.created_at)'), [$from, $to])
                    ->where('drivers.id','=',$driver_passport)
                    ->paginate(10);
                $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
                $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

                return view('driver.shipping_list_driver',compact('expenses','normal','bill','driver','ladding','driver_data'));
            }
            elseif ($driver_license !='')
            {
                $driver_data    =Driver::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->orderby('created_at','DESC')->get();
                $ladding = DB::table("laddings")->whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->pluck("bill_lading","id");

                $bill   =Container::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->get();
                $driver = Driver::select('drivers.*','cont_no','bill_lading')
                    ->join('containers','containers.id','=','drivers.container_id')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->whereBetween(DB::raw('YEAR(drivers.created_at)'), [$from, $to])
                    ->where('drivers.id','=',$driver_license)
                    ->paginate(10);
                $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
                $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

                return view('driver.shipping_list_driver',compact('expenses','normal','bill','driver','ladding','driver_data'));
            }

        }
        elseif($user->hasRole('declaration'))
        {

            if($get_bl !='')
            {
                $driver_data    =Driver::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->orderby('created_at','DESC')->get();
                $ladding = DB::table("laddings")->whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->pluck("bill_lading","id");

                $bill   =Container::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->get();
                $driver = Driver::select('drivers.*','cont_no','bill_lading')
                    ->join('containers','containers.id','=','drivers.container_id')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->whereBetween(DB::raw('YEAR(drivers.created_at)'), [$from, $to])
                    ->where('laddings.id','=',$get_bl)
                    ->paginate(10);
                $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
                $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

                return view('driver.declaration_list_driver',compact('expenses','normal','bill','driver','ladding','driver_data'));
            }
            elseif ($driver_name !='')
            {
                $driver_data    =Driver::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->orderby('created_at','DESC')->get();
                $ladding = DB::table("laddings")->whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->pluck("bill_lading","id");

                $bill   =Container::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->get();

                $driver = Driver::select('drivers.*','cont_no','bill_lading')
                    ->join('containers','containers.id','=','drivers.container_id')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->whereBetween(DB::raw('YEAR(drivers.created_at)'), [$from, $to])
                    ->where('drivers.id','=',$driver_name)
                    ->paginate(10);
                $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
                $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

                return view('driver.declaration_list_driver',compact('expenses','normal','bill','driver','ladding','driver_data'));
            }
            elseif ($driver_passport !='')
            {
                $driver_data    =Driver::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->orderby('created_at','DESC')->get();
                $ladding = DB::table("laddings")->whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->pluck("bill_lading","id");

                $bill   =Container::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->get();
                $driver = Driver::select('drivers.*','cont_no','bill_lading')
                    ->join('containers','containers.id','=','drivers.container_id')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->whereBetween(DB::raw('YEAR(drivers.created_at)'), [$from, $to])
                    ->where('drivers.id','=',$driver_passport)
                    ->paginate(10);
                $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
                $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

                return view('driver.declaration_list_driver',compact('expenses','normal','bill','driver','ladding','driver_data'));
            }
            elseif ($driver_license !='')
            {
                $driver_data    =Driver::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->orderby('created_at','DESC')->get();
                $ladding = DB::table("laddings")->whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->pluck("bill_lading","id");

                $bill   =Container::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->get();
                $driver = Driver::select('drivers.*','cont_no','bill_lading')
                    ->join('containers','containers.id','=','drivers.container_id')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->whereBetween(DB::raw('YEAR(drivers.created_at)'), [$from, $to])
                    ->where('drivers.id','=',$driver_license)
                    ->paginate(10);
                $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
                $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

                return view('driver.declaration_list_driver',compact('expenses','normal','bill','driver','ladding','driver_data'));
            }

        }
        elseif($user->hasRole('finance'))
        {

            if($get_bl !='')
            {
                $driver_data    =Driver::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->orderby('created_at','DESC')->get();
                $ladding = DB::table("laddings")->whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->pluck("bill_lading","id");

                $bill   =Container::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->get();
                $driver = Driver::select('drivers.*','cont_no','bill_lading')
                    ->join('containers','containers.id','=','drivers.container_id')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->whereBetween(DB::raw('YEAR(drivers.created_at)'), [$from, $to])
                    ->where('laddings.id','=',$get_bl)
                    ->paginate(10);
                $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
                $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

                return view('driver.finance_list_driver',compact('expenses','normal','bill','driver','ladding','driver_data'));
            }
            elseif ($driver_name !='')
            {
                $driver_data    =Driver::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->orderby('created_at','DESC')->get();
                $ladding = DB::table("laddings")->whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->pluck("bill_lading","id");

                $bill   =Container::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->get();
                $driver = Driver::select('drivers.*','cont_no','bill_lading')
                    ->join('containers','containers.id','=','drivers.container_id')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->whereBetween(DB::raw('YEAR(drivers.created_at)'), [$from, $to])
                    ->where('drivers.id','=',$driver_name)
                    ->paginate(10);
                $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
                $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

                return view('driver.finance_list_driver',compact('expenses','normal','bill','driver','ladding','driver_data'));
            }
            elseif ($driver_passport !='')
            {
                $driver_data    =Driver::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->orderby('created_at','DESC')->get();
                $ladding = DB::table("laddings")->whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->pluck("bill_lading","id");

                $bill   =Container::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->get();
                $driver = Driver::select('drivers.*','cont_no','bill_lading')
                    ->join('containers','containers.id','=','drivers.container_id')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->whereBetween(DB::raw('YEAR(drivers.created_at)'), [$from, $to])
                    ->where('drivers.id','=',$driver_passport)
                    ->paginate(10);
                $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
                $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

                return view('driver.finance_list_driver',compact('expenses','normal','bill','driver','ladding','driver_data'));
            }
            elseif ($driver_license !='')
            {
                $driver_data    =Driver::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->orderby('created_at','DESC')->get();
                $ladding = DB::table("laddings")->whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->pluck("bill_lading","id");

                $bill   =Container::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->get();
                $driver = Driver::select('drivers.*','cont_no','bill_lading')
                    ->join('containers','containers.id','=','drivers.container_id')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->whereBetween(DB::raw('YEAR(drivers.created_at)'), [$from, $to])
                    ->where('drivers.id','=',$driver_license)
                    ->paginate(10);
                $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
                $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

                return view('driver.finance_list_driver',compact('expenses','normal','bill','driver','ladding','driver_data'));
            }

        }
        elseif($user->hasRole('sales'))
        {

            if($get_bl !='')
            {
                $driver_data    =Driver::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->orderby('created_at','DESC')->get();
                $ladding = DB::table("laddings")->whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->pluck("bill_lading","id");

                $bill   =Container::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->get();
                $driver = Driver::select('drivers.*','cont_no','bill_lading')
                    ->join('containers','containers.id','=','drivers.container_id')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->whereBetween(DB::raw('YEAR(drivers.created_at)'), [$from, $to])
                    ->where('laddings.id','=',$get_bl)
                    ->paginate(10);
                $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
                $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

                return view('driver.operation_list_drivers',compact('expenses','normal','bill','driver','ladding','driver_data'));
            }
            elseif ($driver_name !='')
            {
                $driver_data    =Driver::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->orderby('created_at','DESC')->get();
                $ladding = DB::table("laddings")->whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->pluck("bill_lading","id");

                $bill   =Container::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->get();
                $driver = Driver::select('drivers.*','cont_no','bill_lading')
                    ->join('containers','containers.id','=','drivers.container_id')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->whereBetween(DB::raw('YEAR(drivers.created_at)'), [$from, $to])
                    ->where('drivers.id','=',$driver_name)
                    ->paginate(10);
                $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
                $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

                return view('driver.operation_list_drivers',compact('expenses','normal','bill','driver','ladding','driver_data'));
            }
            elseif ($driver_passport !='')
            {
                $driver_data    =Driver::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->orderby('created_at','DESC')->get();
                $ladding = DB::table("laddings")->whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->pluck("bill_lading","id");

                $bill   =Container::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->get();
                $driver = Driver::select('drivers.*','cont_no','bill_lading')
                    ->join('containers','containers.id','=','drivers.container_id')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->whereBetween(DB::raw('YEAR(drivers.created_at)'), [$from, $to])
                    ->where('drivers.id','=',$driver_passport)
                    ->paginate(10);
                $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
                $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

                return view('driver.operation_list_drivers',compact('expenses','normal','bill','driver','ladding','driver_data'));
            }
            elseif ($driver_license !='')
            {
                $driver_data    =Driver::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->orderby('created_at','DESC')->get();
                $ladding = DB::table("laddings")->whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->pluck("bill_lading","id");

                $bill   =Container::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->get();
                $driver = Driver::select('drivers.*','cont_no','bill_lading')
                    ->join('containers','containers.id','=','drivers.container_id')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->whereBetween(DB::raw('YEAR(drivers.created_at)'), [$from, $to])
                    ->where('drivers.id','=',$driver_license)
                    ->paginate(10);
                $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
                $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

                return view('driver.operation_list_drivers',compact('expenses','normal','bill','driver','ladding','driver_data'));
            }

        }
        else
        {
            return JsonResponse::create(['error' => 'access-denied'],401);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


    public function update(Request $request, $id)
    {
        $user   =Auth::user();
        if($user->hasRole('admin') || $user->hasRole('sales') || $user->hasRole('super') || $user->hasRole('shipping') || $user->hasRole('declaration') )
        {
        $user_id = Auth::id();

        $order =  Driver::findOrFail($id);

        $customer_payments         = $request->file('driving_copy');
        $other                     = $request->file('other');
        $movement_sheet            = $request->file('movement_sheet');

        if(isset($customer_payments)) {
            if (isset($customer_payments)) {
                $extension5 = $request->file('driving_copy')->getClientOriginalExtension();
            }
            $sha = 'driving_copy' . md5(time());
            if(isset($extension5)){
                $filename6 = date('Y').$sha . "gfgsgf." . $extension5;
            }
            $port_charges_path = 'archives/';
            if (empty($customer_payments)) {
                File::makeDirectory($customer_payments, 0775, true, true);
            }
            if(isset($filename6)){
                $order->driving_copy = $filename6;
                $request->file('driving_copy')->move($port_charges_path, $filename6);
                $order->save();
            }
        }
        if(isset($other)) {
            if (isset($other)) {
                $extension6 = $request->file('passport_copy')->getClientOriginalExtension();
            }
            $sha = 'passport_copy' . md5(time());
            if(isset($extension6)){
                $filename4 = date('Y').$sha . "gfgsgf." . $extension6;
            }
            $port_charges_path = 'archives/';
            if (empty($other)) {
                File::makeDirectory($other, 0775, true, true);
            }
            if(isset($filename4)){
                $order->passport_copy = $filename4;
                $request->file('passport_copy')->move($port_charges_path, $filename4);
                $order->save();
            }

        }
        if(isset($movement_sheet)) {
            if (isset($movement_sheet)) {
                $extension6 = $request->file('movement_sheet')->getClientOriginalExtension();
            }
            $sha = 'movement_sheet' . md5(time());
            if(isset($extension6)){
                $filename5 = date('Y').$sha . "gfgsgf." . $extension6;
            }
            $port_charges_path = 'archives/';
            if (empty($movement_sheet)) {
                File::makeDirectory($movement_sheet, 0775, true, true);
            }
            if(isset($filename5)){
                $order->movement_sheet = $filename5;
                $request->file('movement_sheet')->move($port_charges_path, $filename5);
                $order->save();
            }

        }

        $request->merge(['user_id' => $user_id]);
        $all_data =  $request->all();

        $all_data['driving_copy']          = !empty($filename6) ? $filename6  : $order->driving_copy;
        $all_data['passport_copy']         = !empty($filename4) ? $filename4  : $order->passport_copy;
        $all_data['movement_sheet']        = !empty($filename5) ? $filename5  : $order->movement_sheet;

        $order->update($all_data);
        Session::flash('updated', 'Driver Updated  Successful ');
        return redirect('/all_driver');
        }
        else
        {
            return JsonResponse::create(['error' => 'access-denied'],401);
        }
    }


    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Exception
     */
    public function destroy($id)
    {

        $user   =Auth::user();
        if($user->hasRole('admin')|| $user->hasRole('sales') || $user->hasRole('shipping') || $user->hasRole('super') || $user->hasRole('declaration'))
        {
            $training =Driver::findOrfail($id);
            $training->delete();

            Session::flash('delete', 'Driver Deleted  Successful ');
            return redirect('/all_driver');
        }

    }
}