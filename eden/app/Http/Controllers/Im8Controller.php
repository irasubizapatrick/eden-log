<?php

namespace App\Http\Controllers;

use App\BlDeclaration;
use App\Expenses;
use App\Ladding;
use App\NormalExpenses;
use App\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\DB;
use Session;
use File;

class Im8Controller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user   =Auth::user();
        if($user->hasRole('admin')) {

            $from = '2021';
            $to = '2030';
            $ladding = DB::table("laddings")->whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->latest()->pluck("bill_lading","id");
            $declaration = BlDeclaration::Select('bl_declarations.*','bill_lading')
                            ->join('declaration_types','declaration_types.id','LIKE','bl_declarations.declaration_id')
                            ->join('laddings','laddings.id','=','bl_declarations.ladding_id')
                            ->where('declaration_types.cargo_declaration_type','LIKE','IM8')
                            ->whereBetween(DB::raw('YEAR(bl_declarations.created_at)'),[$from, $to])
                            ->get();
            $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
            $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

            return view('declaration.im8', compact('expenses','normal','declaration','ladding'));
        }
        elseif($user->hasRole('super')) {

            $ladding = DB::table("laddings")->latest()->pluck("bill_lading","id");
            $declaration = BlDeclaration::Select('bl_declarations.*','bill_lading')
                            ->join('declaration_types','declaration_types.id','LIKE','bl_declarations.declaration_id')
                            ->join('laddings','laddings.id','=','bl_declarations.ladding_id')
                            ->where('declaration_types.cargo_declaration_type','LIKE','IM8')
                            ->get();
            $expenses = Expenses::with('container')->where('finance_status','LIKE','pending')->count();

            $normal = NormalExpenses::where('finance_status','LIKE','pending')->count();

            return view('declaration.super_im8', compact('expenses','normal','declaration','ladding'));
        }
        elseif($user->hasRole('declaration')) {

            $ladding = DB::table("laddings")->latest()->pluck("bill_lading","id");
            $declaration = BlDeclaration::Select('bl_declarations.*','bill_lading')
                                ->join('declaration_types','declaration_types.id','LIKE','bl_declarations.declaration_id')
                                ->join('laddings','laddings.id','=','bl_declarations.ladding_id')
                                ->where('declaration_types.cargo_declaration_type','LIKE','IM8')
                                ->get();
            $expenses = Expenses::with('container')->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
            $normal = NormalExpenses::where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

            return view('declaration.declaration_im8', compact('expenses','normal','declaration','ladding'));
        }
        elseif($user->hasRole('finance')) {

            $ladding = DB::table("laddings")->latest()->pluck("bill_lading","id");
            $declaration = BlDeclaration::Select('bl_declarations.*','bill_lading')
                            ->join('declaration_types','declaration_types.id','LIKE','bl_declarations.declaration_id')
                            ->join('laddings','laddings.id','=','bl_declarations.ladding_id')
                            ->where('declaration_types.cargo_declaration_type','LIKE','IM8')
                            ->get();
            $expenses = Expenses::with('container')->where('finance_status','LIKE','pending')->count();

            $normal = NormalExpenses::where('finance_status','LIKE','pending')->count();

            return view('declaration.finance_im8', compact('expenses','normal','declaration','ladding'));
        }
        else  if($user->hasRole('agent')) {

            $ladding = DB::table("laddings")->join('tracks','tracks.id','=','laddings.track_id')
                            ->join('agents','agents.id','=','tracks.agent_id')
                            ->where('agents.user_id','=',Auth::id())
                            ->pluck("bill_lading","laddings.id");
            $profile = User::where('id','=',Auth::id())->get();
            $lading      = Ladding::all();
            $declaration = BlDeclaration::Select('bl_declarations.*','bill_lading')
                                ->join('declaration_types','declaration_types.id','LIKE','bl_declarations.declaration_id')
                                ->join('laddings','laddings.id','=','bl_declarations.ladding_id')
                                ->join('tracks','tracks.id','=','laddings.track_id')
                                ->join('agents','agents.id','=','tracks.agent_id')
                                ->where('agents.user_id','=',Auth::id())
                                ->where('declaration_types.cargo_declaration_type','LIKE','IM8')
                                ->latest()
                                ->get();
            $expenses = Expenses::with('container')->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
            $normal = NormalExpenses::where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

            return view('declaration.agent_im8', compact('expenses','normal','declaration','lading','profile','ladding'));
        }
        else
        {
            return JsonResponse::create(['error' => 'access-denied'],401);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|static
     */

    public function searchBlIM8(Request $request )
    {

        $user   =Auth::user();
        $get_bl = $request->get('ladding_id');
        if($user->hasRole('admin')) {

            $ladding = DB::table("laddings")->latest()->pluck("bill_lading","id");
            $lading      = Ladding::all();
            $declaration = BlDeclaration::Select('bl_declarations.*','bill_lading')
                            ->join('declaration_types','declaration_types.id','LIKE','bl_declarations.declaration_id')
                            ->join('laddings','laddings.id','=','bl_declarations.ladding_id')
                            ->where('declaration_types.cargo_declaration_type','LIKE','IM8')
                            ->where('laddings.id','=',$get_bl)
                            ->get();
            $expenses = Expenses::with('container')->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
            $normal = NormalExpenses::where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

            return view('declaration.im8', compact('expenses','normal','declaration','lading','ladding'));
        }
        elseif($user->hasRole('super'))
        {

            $ladding = DB::table("laddings")->latest()->pluck("bill_lading","id");
            $lading      = Ladding::all();
            $declaration = BlDeclaration::Select('bl_declarations.*','bill_lading')
                            ->join('declaration_types','declaration_types.id','LIKE','bl_declarations.declaration_id')
                            ->join('laddings','laddings.id','=','bl_declarations.ladding_id')
                            ->where('declaration_types.cargo_declaration_type','LIKE','IM8')
                            ->where('laddings.id','=',$get_bl)
                            ->get();
            $expenses = Expenses::with('container')->where('finance_status','LIKE','pending')->count();

            $normal = NormalExpenses::where('finance_status','LIKE','pending')->count();

            return view('declaration.super_im8', compact('expenses','normal','declaration','lading','ladding'));
        }
        elseif($user->hasRole('finance'))
        {
            $ladding = DB::table("laddings")->latest()->pluck("bill_lading","id");
            $lading      = Ladding::all();
            $declaration = BlDeclaration::Select('bl_declarations.*','bill_lading')
                            ->join('declaration_types','declaration_types.id','LIKE','bl_declarations.declaration_id')
                            ->join('laddings','laddings.id','=','bl_declarations.ladding_id')
                            ->where('declaration_types.cargo_declaration_type','LIKE','IM8')
                            ->where('laddings.id','=',$get_bl)
                            ->get();
            $expenses = Expenses::with('container')->where('finance_status','LIKE','pending')->count();

            $normal = NormalExpenses::where('finance_status','LIKE','pending')->count();

            return view('declaration.finance_im8', compact('expenses','normal','declaration','lading','ladding'));
        }
        elseif($user->hasRole('declaration'))
        {
            $ladding = DB::table("laddings")->latest()->pluck("bill_lading","id");
            $lading      = Ladding::all();
            $declaration = BlDeclaration::Select('bl_declarations.*','bill_lading')
                            ->join('declaration_types','declaration_types.id','LIKE','bl_declarations.declaration_id')
                            ->join('laddings','laddings.id','=','bl_declarations.ladding_id')
                            ->where('declaration_types.cargo_declaration_type','LIKE','IM8')
                            ->where('laddings.id','=',$get_bl)
                            ->get();
            $expenses = Expenses::with('container')->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
            $normal = NormalExpenses::where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

            return view('declaration.declaration_im8', compact('expenses','normal','declaration','lading','ladding'));
        }
        elseif($user->hasRole('agent'))
        {

            $ladding = DB::table("laddings")->join('tracks','tracks.id','=','laddings.track_id')
                            ->join('agents','agents.id','=','tracks.agent_id')
                            ->where('agents.user_id','=',Auth::id())
                            ->pluck("bill_lading","laddings.id");
            $profile = User::where('id','=',Auth::id())->get();
            $lading      = Ladding::all();
            $declaration = BlDeclaration::Select('bl_declarations.*','bill_lading')
                                ->join('declaration_types','declaration_types.id','LIKE','bl_declarations.declaration_id')
                                ->join('laddings','laddings.id','=','bl_declarations.ladding_id')
                                ->join('tracks','tracks.id','=','laddings.track_id')
                                ->join('agents','agents.id','=','tracks.agent_id')
                                ->where('agents.user_id','=',Auth::id())
                                ->where('declaration_types.cargo_declaration_type','LIKE','IM8')
                                ->where('laddings.id','=',$get_bl)
                                ->latest()
                                ->get();
            $expenses = Expenses::with('container')->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
            $normal = NormalExpenses::where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

            return view('declaration.agent_im8', compact('expenses','normal','declaration','lading','profile','ladding'));
        }
        else
        {
            return JsonResponse::create(['error' => 'access-denied'],401);
        }
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user_id = Auth::id();
        $request->merge(['user_id' => $user_id]);
        $order =  BlDeclaration::findOrFail($id);


        $authorization              = $request->file('authorization');
        $assessment                 = $request->file('assessment_doc');
        $movement_sheet             = $request->file('movement_sheet');
        $release                    = $request->file('release');
        $c2                         = $request->file('c2');


        if(isset($c2)) {
            if (isset($c2)) {
                $extension = $request->file('c2')->getClientOriginalExtension();
            }
            $sha = 'c2' . md5(time());
            if(isset($extension)){
                $filename1 = date('Y').$sha . "sgfs." . $extension;
            }
            $destination_path = 'Declaration/';
            if (empty($c2)) {
                File::makeDirectory($c2, 0775, true, true);
            }
            if(isset($filename1)){
                $order->c2 =  $filename1;
                $request->file('c2')->move($destination_path, $filename1);
                $order->save();
            }
        }

        if(isset($movement_sheet)) {
            if (isset($movement_sheet)) {
                $extension = $request->file('movement_sheet')->getClientOriginalExtension();
            }
            $sha = 'movement_sheet' . md5(time());
            if(isset($extension)){
                $filename3 = date('Y').$sha . "sgfs." . $extension;
            }
            $destination_path = 'Declaration/';
            if (empty($movement_sheet)) {
                File::makeDirectory($movement_sheet, 0775, true, true);
            }
            if(isset($filename3)){
                $order->movement_sheet =  $filename3;
                $request->file('movement_sheet')->move($destination_path, $filename3);
                $order->save();
            }
        }

        if(isset($assessment)) {
            if (isset($assessment)) {
                $extension = $request->file('assessment_doc')->getClientOriginalExtension();
            }
            $sha = 'assessment_doc' . md5(time());
            if(isset($extension)){
                $filename2 = date('Y').$sha . "sgfs." . $extension;
            }
            $destination_path = 'Declaration/';
            if (empty($assessment)) {
                File::makeDirectory($assessment, 0775, true, true);
            }
            if(isset($filename2)){
                $order->assessment_doc =  $filename2;
                $request->file('assessment_doc')->move($destination_path, $filename2);
                $order->save();
            }
        }

        if(isset($release)) {
            if (isset($release)) {
                $extension = $request->file('release')->getClientOriginalExtension();
            }
            $sha = 'release' . md5(time());
            if(isset($extension)){
                $filename4 = date('Y').$sha . "sgfs." . $extension;
            }
            $destination_path = 'Declaration/';
            if (empty($release)) {
                File::makeDirectory($release, 0775, true, true);
            }
            if(isset($filename4)){
                $order->release =  $filename4;
                $request->file('release')->move($destination_path, $filename4);
                $order->save();
            }
        }

        if(isset($authorization)) {
            if (isset($authorization)) {
                $extension = $request->file('authorization')->getClientOriginalExtension();
            }
            $sha = 'authorization' . md5(time());
            if(isset($extension)){
                $filename5 = date('Y').$sha . "sgfs." . $extension;
            }
            $destination_path = 'Declaration/';
            if (empty($release)) {
                File::makeDirectory($release, 0775, true, true);
            }
            if(isset($filename5)){
                $order->authorization =  $filename5;
                $request->file('authorization')->move($destination_path, $filename5);
                $order->save();
            }
        }

        $all_data =  $request->all();

        $all_data['c2']                         = !empty($filename1) ? $filename1        : $order->c2;
        $all_data['authorization']              = !empty($filename5) ? $filename5        : $order->authorization;
        $all_data['movement_sheet']             = !empty($filename3) ? $filename3        : $order->movement_sheet;
        $all_data['assessment_doc']             = !empty($filename2) ? $filename2        : $order->assessment_doc;
        $all_data['release']                    = !empty($filename4) ? $filename4        : $order->release;
        $order->update($all_data);
        Session::flash('message', 'Declaration updated');
        return  redirect('/declaration/im8');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
