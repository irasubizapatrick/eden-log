<?php

namespace App\Http\Controllers;

use App\Archive;
use App\Clearance;
use App\Expenses;
use App\Ladding;
use App\NormalExpenses;
use App\Payment_doc;
use Auth;
use DB;
use Faker\Provider\ar_SA\Payment;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\File;

use Session;
use Illuminate\Http\Request;

class ClearanceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user   =Auth::user();
        if($user->hasRole('admin'))
        {
            $from = '2021';
            $to = '2030';

            $ladding = DB::table("laddings")->whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->pluck("bill_lading","id");
            $archive  = Clearance::select('clearances.*','bill_lading')
                        ->join('laddings','laddings.id','=','clearances.ladding_id')
                        ->join('tracks','tracks.id','=','laddings.track_id')
                        ->whereBetween(DB::raw('YEAR(clearances.created_at)'), [$from, $to])
                        ->get();
            $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
            $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();


            return view('admin.clearence_list',compact('expenses','normal','archive','ladding'));
        }
        else if($user->hasRole('super'))
        {
            $ladding = DB::table("laddings")->pluck("bill_lading","id");
            $archive  = Clearance::select('clearances.*','bill_lading')
                ->join('laddings','laddings.id','=','clearances.ladding_id')
                ->join('tracks','tracks.id','=','laddings.track_id')
                ->get();
            $expenses = Expenses::with('container')->where('finance_status','LIKE','pending')->count();

            $normal = NormalExpenses::where('finance_status','LIKE','pending')->count();


            return view('admin.super_clearence_list',compact('expenses','normal','archive','ladding'));
        }
     
        else if($user->hasRole('declaration'))
        {
            $from = '2021';
            $to = '2030';
            $ladding = DB::table("laddings")->whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->pluck("bill_lading","id");
            $archive  = Clearance::select('clearances.*','bill_lading')
                ->join('laddings','laddings.id','=','clearances.ladding_id')
                ->join('tracks','tracks.id','=','laddings.track_id')
                ->whereBetween(DB::raw('YEAR(clearances.created_at)'), [$from, $to])
                ->get();
            $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
            $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();


            return view('declaration.clearence_list',compact('expenses','normal','archive','ladding'));
        }
        else if($user->hasRole('finance'))
        {
            $from = '2021';
            $to = '2030';
            $ladding = DB::table("laddings")->whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->pluck("bill_lading","id");
            $archive  = Clearance::select('clearances.*','bill_lading')
                ->join('laddings','laddings.id','=','clearances.ladding_id')
                ->join('tracks','tracks.id','=','laddings.track_id')
                ->whereBetween(DB::raw('YEAR(clearances.created_at)'), [$from, $to])
                ->get();
            $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
            $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

            return view('finance.clearence_list',compact('expenses','normal','archive','ladding'));
        }
        else
        {
            return JsonResponse::create(['error' => 'access-denied'],401);
        }
    }

    /**
     *
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user_id = Auth::id();
        $request->merge(['user_id' => $user_id]);
        $order =  Clearance::create($request->all());


        $invoice_doc                = $request->file('tra');
        $shipping_line_charges      = $request->file('rra');
        $other_clearance_doc        = $request->file('other_clearance_doc');

        if(isset($invoice_doc)) {
            if (isset($invoice_doc)) {
                $extension1 = $request->file('tra')->getClientOriginalExtension();
            }
            $sha = 'tra' . md5(time());
            if(isset($extension1)){
                $filename1 = date('Y').$sha . "sgfs." . $extension1;
            }
            $destination_path = 'tax_clearance/';
            if (empty($invoice_doc)) {
                File::makeDirectory($invoice_doc, 0775, true, true);
                // File::makeDirectory($destination_path, $mode = 0777, true, true);
            }
            if(isset($filename1)){
                $order->tra =  $filename1;
                $request->file('tra')->move($destination_path, $filename1);
                $order->save();
            }

        }

        if(isset($shipping_line_charges)) {
            if (isset($shipping_line_charges)) {
                $shipping_line_ex = $request->file('rra')->getClientOriginalExtension();
            }
            $sha = 'rra' . md5(time().date("y"));
            if(isset($shipping_line_charges)){
                $filename5 = date('Y').$sha . "fghjm." . $shipping_line_ex;
            }
            $shipping_line_path = 'tax_clearance/';
            if (empty($shipping_line_charges)) {
                File::makeDirectory($shipping_line_charges, 0775, true, true);
            }
            if(isset($filename5)){
                $order->rra = $filename5;
                $request->file('rra')->move($shipping_line_path, $filename5);
                $order->save();
            }
        }
        if(isset($other_clearance_doc)) {
            if (isset($other_clearance_doc)) {
                $shipping_line_ex = $request->file('other_clearance_doc')->getClientOriginalExtension();
            }
            $sha = 'other_clearance_doc' . md5(time().date("y"));
            if(isset($other_clearance_doc)){
                $filename6 = date('Y').$sha . "fghjm." . $shipping_line_ex;
            }
            $shipping_line_path = 'tax_clearance/';
            if (empty($other_clearance_doc)) {
                File::makeDirectory($shipping_line_charges, 0775, true, true);
            }
            if(isset($filename6)){
                $order->other_clearance_doc = $filename6;
                $request->file('other_clearance_doc')->move($shipping_line_path, $filename5);
                $order->save();
            }
        }

        $all_data =  $request->all();

        $all_data['tra']      = !empty($filename1) ? $filename1                : $order->tra;
        $all_data['rra']      = !empty($filename5) ? $filename5                : $order->rra;
        $all_data['other_clearance_doc']      = !empty($filename6) ? $filename6                : $order->other_clearance_doc;
        $order->update($all_data);
        Session::flash('message', 'Document  Uploaded Successful  ');
        return  redirect('/clearance');
    }
    /**
     * @param $id
     */
    public function show($id)
    {
        //
    }

    /**
     * @param $id
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user_id = Auth::id();
        $request->merge(['user_id' => $user_id]);
        $order =  Clearance::findOrFail($id);


        $invoice_doc                = $request->file('tra');
        $shipping_line_charges      = $request->file('rra');
        $other_clearance_doc        = $request->file('other_clearance_doc');


        if(isset($invoice_doc)) {
            if (isset($invoice_doc)) {
                $extension1 = $request->file('tra')->getClientOriginalExtension();
            }
            $sha = 'tra' . md5(time());
            if(isset($extension1)){
                $filename1 = date('Y').$sha . "sgfs." . $extension1;
            }
            $destination_path = 'tax_clearance/';
            if (empty($invoice_doc)) {
                File::makeDirectory($invoice_doc, 0775, true, true);
                // File::makeDirectory($destination_path, $mode = 0777, true, true);
            }
            if(isset($filename1)){
                $order->tra =  $filename1;
                $request->file('tra')->move($destination_path, $filename1);
                $order->save();
            }

        }
        if(isset($shipping_line_charges)) {
            if (isset($shipping_line_charges)) {
                $shipping_line_ex = $request->file('rra')->getClientOriginalExtension();
            }
            $sha = 'rra' . md5(time().date("y"));
            if(isset($shipping_line_charges)){
                $filename5 = date('Y').$sha . "fghjm." . $shipping_line_ex;
            }
            $shipping_line_path = 'tax_clearance/';
            if (empty($shipping_line_charges)) {
                File::makeDirectory($shipping_line_charges, 0775, true, true);
            }
            if(isset($filename5)){
                $order->rra = $filename5;
                $request->file('rra')->move($shipping_line_path, $filename5);
                $order->save();
            }
        }

        if(isset($other_clearance_doc)) {
            if (isset($other_clearance_doc)) {
                $shipping_line_ex = $request->file('other_clearance_doc')->getClientOriginalExtension();
            }
            $sha = 'other_clearance_doc' . md5(time().date("y"));
            if(isset($other_clearance_doc)){
                $filename6 = date('Y').$sha . "fghjm." . $shipping_line_ex;
            }
            $shipping_line_path = 'tax_clearance/';
            if (empty($other_clearance_doc)) {
                File::makeDirectory($other_clearance_doc, 0775, true, true);
            }
            if(isset($filename6)){
                $order->other_clearance_doc = $filename6;
                $request->file('other_clearance_doc')->move($shipping_line_path, $filename6);
                $order->save();
            }
        }

        $all_data =  $request->all();

        $all_data['tra']                        = !empty($filename1) ? $filename1                : $order->tra;
        $all_data['rra']                        = !empty($filename5) ? $filename5                : $order->rra;
        $all_data['other_clearance_doc']        = !empty($filename6) ? $filename6                : $order->other_clearance_doc;
        $order->update($all_data);
        Session::flash('message', 'Document  Updated Successful  ');
        return  redirect('/clearance');
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Exception
     */
    public function destroy($id)
    {
        $training =Clearance::findOrfail($id);
        $training->delete();

        Session::flash('delete', 'Documents  Deleted  Successful ');
        return redirect('/clearance');
    }
}