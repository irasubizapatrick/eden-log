<?php

namespace App\Http\Controllers;

use App\Agent;
use App\BlDeclaration;
use App\Container;
use App\Country;
use App\DeclarationType;
use App\Destination;
use App\Driver;
use App\Expenses;
use App\Ladding;
use App\NormalExpenses;
use App\Transporter;
use App\Trucking;
use App\User;
use Carbon\Carbon;
use DB;
use App\Track;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Input;
use Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;
use Tymon\JWTAuth\Facades\JWTAuth;

use Validator;


class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user   =Auth::user();
        if($user->hasRole('agent')) {


            $from = '2021';
            $to = '2030';
            $user_id = Auth::id();
            $profile = User::where('id','=',$user_id)->get();
            $track = Track::select('tracks.*', 'bill_lading', 'organization_name', 'contact_person')
                            ->join('laddings', 'laddings.track_id', '=', 'tracks.id')
                            ->join('agents', 'agents.id', '=', 'tracks.agent_id')
                            ->where('agents.user_id','=',$user_id)
                            ->whereBetween(DB::raw('YEAR(tracks.created_at)'),[$from, $to])
                            ->latest("tracks.created_at")
                            ->get();

            $agent    = Agent::all()->where('user_id','',$user_id);

            return view('order.my_oder', compact('track', 'user_id','profile','agent'));
        }
        else
        {
            return JsonResponse::create(['error' => 'access-denied'],401);
        }
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|static
     *
     * all orders
     */

    public function ApiOder()
    {

        $user = JWTAuth::parseToken()->authenticate();

        if($user->hasRole('admin')) {

            $from = '2021';
            $to = '2030';
            $track = DB::table('tracks')
                ->select('tracks.id','bill_lading','client_name','file_no','destination','eta','ata','shipping_line','current_status')
                ->join('laddings', 'laddings.track_id', '=', 'tracks.id')
                ->join('agents', 'agents.id', '=', 'tracks.agent_id')
                ->whereBetween(DB::raw('YEAR(tracks.created_at)'),[$from, $to])
                ->orderByDesc("tracks.created_at")
                ->get();
            return response()->json(['orders'=>$track], 200);

        }
        elseif($user->hasRole('super'))
        {

            $track = DB::table('tracks')
                ->select('tracks.id','bill_lading','client_name','file_no','destination','eta','ata','shipping_line','track_status')
                ->join('laddings', 'laddings.track_id', '=', 'tracks.id')
                ->join('agents', 'agents.id', '=', 'tracks.agent_id')
                ->orderByDesc("tracks.created_at")
                ->get();
            return response()->json(['orders'=>$track], 200);

        }
        elseif($user->hasRole('documentation')) {

            $from = '2021';
            $to = '2030';
            $track = DB::table('tracks')
                ->select('tracks.id','bill_lading','client_name','file_no','destination','eta','ata','shipping_line','track_status')
                ->join('laddings', 'laddings.track_id', '=', 'tracks.id')
                ->join('agents', 'agents.id', '=', 'tracks.agent_id')
                ->whereBetween(DB::raw('YEAR(tracks.created_at)'),[$from, $to])
                ->orderByDesc("tracks.created_at")
                ->get();
            return response()->json(['orders'=>$track], 200);

        }
        else if($user->hasRole('declaration')) {

            $from = '2021';
            $to = '2030';
            $track = DB::table('tracks')
                ->select('tracks.id','bill_lading','client_name','file_no','destination','eta','ata','shipping_line','track_status')
                ->join('laddings', 'laddings.track_id', '=', 'tracks.id')
                ->join('agents', 'agents.id', '=', 'tracks.agent_id')
                ->whereBetween(DB::raw('YEAR(tracks.created_at)'),[$from, $to])
                ->orderByDesc("tracks.created_at")
                ->get();
            return response()->json(['orders'=>$track], 200);

        }
        else if($user->hasRole('shipping')) {

            $from = '2021';
            $to = '2030';
            $track = DB::table('tracks')
                ->select('tracks.id','bill_lading','client_name','file_no','destination','eta','ata','shipping_line','track_status')
                ->join('laddings', 'laddings.track_id', '=', 'tracks.id')
                ->join('agents', 'agents.id', '=', 'tracks.agent_id')
                ->whereBetween(DB::raw('YEAR(tracks.created_at)'),[$from, $to])
                ->orderByDesc("tracks.created_at")
                ->get();
            return response()->json(['orders'=>$track], 200);

        }
        else if($user->hasRole('sales')) {

            $from = '2021';
            $to = '2030';
            $track = DB::table('tracks')
                ->select('tracks.id','bill_lading','client_name','file_no','destination','eta','ata','shipping_line','track_status')
                ->join('laddings', 'laddings.track_id', '=', 'tracks.id')
                ->join('agents', 'agents.id', '=', 'tracks.agent_id')
                ->whereBetween(DB::raw('YEAR(tracks.created_at)'),[$from, $to])
                ->orderByDesc("tracks.created_at")
                ->get();
            return response()->json(['orders'=>$track], 200);

        }
        else if($user->hasRole('finance')) {

            $from = '2021';
            $to = '2030';
            $track = DB::table('tracks')
                ->select('tracks.id','bill_lading','client_name','file_no','destination','eta','ata','shipping_line','track_status')
                ->join('laddings', 'laddings.track_id', '=', 'tracks.id')
                ->join('agents', 'agents.id', '=', 'tracks.agent_id')
                ->whereBetween(DB::raw('YEAR(tracks.created_at)'),[$from, $to])
                ->orderByDesc("tracks.created_at")
                ->get();
            return response()->json(['orders'=>$track], 200);

        }
        else
        {
            return JsonResponse::create(['error' => 'access-denied'],401);
        }
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|static
     *
     */

    public function all_order()
    {
        $user   =Auth::user();

        if($user->hasRole('admin'))
        {
            $from = '2021';
            $to = '2030';

            $agent =Agent::latest()->get();
            $track   = Track::select('tracks.*','bill_lading','organization_name')
                ->join('laddings','laddings.track_id','=','tracks.id')
                ->join('agents','agents.id','=','tracks.agent_id')
                ->whereBetween(DB::raw('YEAR(tracks.created_at)'),[$from, $to])
                ->orderByDesc("tracks.created_at")
                ->get();
            $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
            $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

            return view('order.list_order', compact('expenses','normal','track','agent'));
        }
        else if($user->hasRole('super'))
        {
            $agent =Agent::latest()->get();
            $track   = Track::select('tracks.*','bill_lading','organization_name')
                ->join('laddings','laddings.track_id','=','tracks.id')
                ->join('agents','agents.id','=','tracks.agent_id')
                ->orderByDesc("tracks.created_at")
                ->get();
            $expenses = Expenses::with('container')->where('finance_status','LIKE','pending')->count();

            $normal = NormalExpenses::where('finance_status','LIKE','pending')->count();

            return view('order.super_list_order', compact('expenses','normal','track','agent'));
        }
        else if($user->hasRole('documentation'))
        {
            $from = '2021';
            $to = '2030';

            $agent =Agent::latest()->get();
            $track   = Track::select('tracks.*','bill_lading','organization_name')
                ->join('laddings','laddings.track_id','=','tracks.id')
                ->join('agents','agents.id','=','tracks.agent_id')
                ->whereBetween(DB::raw('YEAR(tracks.created_at)'),[$from, $to])
                ->orderByDesc("tracks.created_at")
                ->get();
            $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
            $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

            return view('order.documentation_list_order', compact('expenses','normal','track','agent'));
        }
        else if($user->hasRole('declaration'))
        {
            $from = '2021';
            $to = '2030';

            $agent =Agent::latest()->get();
            $track   = Track::select('tracks.*','bill_lading','organization_name')
                ->join('laddings','laddings.track_id','=','tracks.id')
                ->join('agents','agents.id','=','tracks.agent_id')
                ->whereBetween(DB::raw('YEAR(tracks.created_at)'),[$from, $to])
                ->orderByDesc("tracks.created_at")
                ->get();
            $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
            $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

            return view('order.declaration_list_order', compact('expenses','normal','track','agent'));
        }
        else if($user->hasRole('shipping'))
        {
            $from = '2021';
            $to = '2030';

            $agent =Agent::latest()->get();
            $track   = Track::select('tracks.*','bill_lading','organization_name')
                ->join('laddings','laddings.track_id','=','tracks.id')
                ->join('agents','agents.id','=','tracks.agent_id')
                ->whereBetween(DB::raw('YEAR(tracks.created_at)'),[$from, $to])
                ->orderByDesc("tracks.created_at")
                ->get();
            $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
            $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

            return view('order.shipping_list_order', compact('expenses','normal','track','agent'));
        }
        elseif ($user->hasRole('sales'))
        {

            $from = '2021';
            $to = '2030';

            $agent =Agent::latest()->get();
            $track   = Track::select('tracks.*','bill_lading','organization_name')
                ->join('laddings','laddings.track_id','=','tracks.id')
                ->join('agents','agents.id','=','tracks.agent_id')
                ->whereBetween(DB::raw('YEAR(tracks.created_at)'),[$from, $to])
                ->orderByDesc("tracks.created_at")
                ->get();
            $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
            $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

            return view('staff.sales_list_order', compact('expenses','normal','track','agent'));
        }
        elseif ($user->hasRole('finance'))
        {

            $from = '2021';
            $to = '2030';

            $agent =Agent::latest()->get();
            $track   = Track::select('tracks.*','bill_lading','organization_name')
                ->join('laddings','laddings.track_id','=','tracks.id')
                ->join('agents','agents.id','=','tracks.agent_id')
                ->whereBetween(DB::raw('YEAR(tracks.created_at)'),[$from, $to])
                ->orderByDesc("tracks.created_at")
                ->get();
            $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
            $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

            return view('finance.finance_list_order', compact('expenses','normal','track','agent'));
        }
        else
        {
            return JsonResponse::create(['error' => 'access-denied'],401);
        }

    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|static
     */

    Public function AllSearchOrder(Request $request){

        $date_from = '2021';
        $date_to = '2030';
        $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'),[$date_from, $date_to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
        $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'),[$date_from, $date_to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

        $user   =Auth::user();

        if($user->hasRole('admin'))
        {

            $from   =  $request->input('date1');
            $to     =  $request->input('date2');
            $agent_id  =  $request->get('agent_id');
            $current_status_one = $request->input('current_status_one');

            if(($current_status_one !="") AND ($from  !="") AND ($to !="") AND ($agent_id != ''))
            {

                $agent =Agent::all();
                $track   = Track::select('tracks.*','bill_lading','organization_name')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('current_status','LIKE',$current_status_one)
                    ->where('tracks.id','=',$agent_id)
                    ->where('tracks.document_date_received','>=', $from)
                    ->where('tracks.document_date_received','<=', $to)
                    ->orderByDesc("tracks.created_at")
                    ->get();

                return view('order.list_order', compact('expenses','normal','track','agent'));

            }

            /**
             *
             * If you select From one date only
             **/

            else  if(($current_status_one !="") AND ($from  !="") AND ($to !="") AND ($agent_id ==''))
            {

                $agent = Agent::all();

                $track   = Track::select('tracks.*','bill_lading','organization_name')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('current_status','LIKE',$current_status_one)
                    ->where('tracks.document_date_received' ,'>=', $from)
                    ->where('tracks.document_date_received' ,'<=', $to)
                    ->orderByDesc("tracks.created_at")
                    ->get();
                return view('order.list_order', compact('expenses','normal','track','agent'));
            }

            else  if(($current_status_one !="") AND ($from  !="") AND ($to =="") AND ($agent_id ==''))
            {

                $agent = Agent::all();
                $track   = Track::select('tracks.*','bill_lading','organization_name')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('current_status','LIKE',$current_status_one)
                    ->where('tracks.document_date_received' ,'>=', $from)
                    ->orderByDesc("tracks.created_at")
                    ->get();

                return view('order.list_order', compact('expenses','normal','track','agent'));
            }
            else  if(($current_status_one !="") AND ($from  =="") AND ($to =="") AND ($agent_id ==''))
            {
                $agent  = Agent::all();

                $track   = Track::select('tracks.*','bill_lading','organization_name')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('current_status','LIKE',$current_status_one)
                    ->whereBetween(DB::raw('YEAR(tracks.created_at)'),[$date_from, $date_to])
                    ->orderByDesc("tracks.created_at")
                    ->get();
                return view('order.list_order', compact('expenses','normal','track','agent'));


            }
            elseif(($current_status_one =="") AND ($from  !="") AND ($to !="") AND ($agent_id != ''))
            {

                $agent =Agent::all();
                $track   = Track::select('tracks.*','bill_lading','organization_name')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('current_status','LIKE',$current_status_one)
                    ->where('tracks.id','=',$agent_id)
                    ->where('tracks.document_date_received' ,'>=', $from)
                    ->where('tracks.document_date_received', '<=', $to)
                    ->orderByDesc("tracks.created_at")
                    ->get();

                return view('order.list_order', compact('expenses','normal','track','agent'));


            }
            elseif(($current_status_one =="") AND ($from  !="") AND ($to !="") AND ($agent_id ==''))
            {


                $agent =Agent::all();
                $track   = Track::select('tracks.*','bill_lading','organization_name')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('current_status','LIKE',$current_status_one)
                    ->where('tracks.document_date_received' ,'>=', $from)
                    ->where('tracks.document_date_received', '<=', $to)
                    ->orderByDesc("tracks.created_at")
                    ->get();

                return view('order.list_order', compact('expenses','normal','track','agent'));

            }
            elseif(($current_status_one =="") AND ($from  !="") AND ($to =="") AND ($agent_id ==''))
            {

                $agent =Agent::all();
                $track   = Track::select('tracks.*','bill_lading','organization_name')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('tracks.document_date_received' ,'>=', $from)
                    ->orderByDesc("tracks.created_at")
                    ->get();

                return view('order.list_order', compact('expenses','normal','track','agent'));

            }
            elseif(($current_status_one =="") AND ($from  =="") AND ($to !="") AND ($agent_id != ''))
            {

                $agent =Agent::all();
                $track   = Track::select('tracks.*','bill_lading','organization_name')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('tracks.id','=',$agent_id)
                    ->where('tracks.document_date_received', '<=', $to)
                    ->orderByDesc("tracks.created_at")
                    ->get();

                return view('order.list_order', compact('expenses','normal','track','agent'));

            }
            elseif(($current_status_one =="") AND ($from  =="") AND ($to !="") AND ($agent_id == ''))
            {


                $agent =Agent::all();
                $track   = Track::select('tracks.*','bill_lading','organization_name')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('tracks.document_date_received', '<=', $to)
                    ->orderByDesc("tracks.created_at")
                    ->get();
                return view('order.list_order', compact('expenses','normal','track','agent'));

            }
            elseif(($current_status_one =="") AND ($from  =="") AND ($to =="") AND ($agent_id != ''))
            {

                $agent =Agent::all();
                $track   = Track::select('tracks.*','bill_lading','organization_name')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('tracks.id','=',$agent_id)
                    ->whereBetween(DB::raw('YEAR(tracks.created_at)'),[$date_from, $date_to])
                    ->orderByDesc("tracks.created_at")
                    ->get();

                return view('order.list_order', compact('expenses','normal','track','agent'));

            }
            elseif(($current_status_one !="") AND ($from  =="") AND ($to !="") AND ($agent_id != ''))
            {

                $agent =Agent::all();
                $track   = Track::select('tracks.*','bill_lading','organization_name')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('current_status','LIKE',$current_status_one)
                    ->where('tracks.id','=',$agent_id)
                    ->where('tracks.document_date_received', '<=', $to)
                    ->orderByDesc("tracks.created_at")
                    ->get();

                return view('order.list_order', compact('expenses','normal','track','agent'));

            }
            elseif(($current_status_one !="") AND ($from  !="") AND ($to =="") AND ($agent_id != ''))
            {

                $agent =Agent::all();
                $track   = Track::select('tracks.*','bill_lading','organization_name')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('current_status','LIKE',$current_status_one)
                    ->where('tracks.id','=',$agent_id)
                    ->where('tracks.document_date_received' ,'>=', $from)
                    ->orderByDesc("tracks.created_at")
                    ->get();

                return view('order.list_order', compact('expenses','normal','track','agent'));

            }
            elseif(($current_status_one !="") AND ($from  =="") AND ($to !="") AND ($agent_id == ''))
            {

                $agent =Agent::all();
                $track   = Track::select('tracks.*','bill_lading','organization_name')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('current_status','LIKE',$current_status_one)
                    ->where('tracks.document_date_received', '<=', $to)
                    ->orderByDesc("tracks.created_at")
                    ->get();

                return view('order.list_order', compact('expenses','normal','track','agent'));

            }
            elseif(($current_status_one =="") AND ($from  !="") AND ($to =="") AND ($agent_id != ''))
            {

                $agent =Agent::all();
                $track   = Track::select('tracks.*','bill_lading','organization_name')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('tracks.id','=',$agent_id)
                    ->where('tracks.document_date_received' ,'>=', $from)
                    ->orderByDesc("tracks.created_at")
                    ->get();

                return view('order.list_order', compact('expenses','normal','track','agent'));

            }
            elseif(($current_status_one !="") AND ($from  =="") AND ($to =="") AND ($agent_id != ''))
            {

                $agent =Agent::all();
                $track   = Track::select('tracks.*','bill_lading','organization_name')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('current_status','LIKE',$current_status_one)
                    ->where('tracks.id','=',$agent_id)
                    ->whereBetween(DB::raw('YEAR(tracks.created_at)'),[$date_from, $date_to])
                    ->orderByDesc("tracks.created_at")
                    ->get();

                return view('order.list_order', compact('expenses','normal','track','agent'));

            }
            else
            {
                $agent =Agent::all();
                $track   = Track::select('tracks.*','bill_lading','organization_name')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->whereBetween(DB::raw('YEAR(tracks.created_at)'),[$date_from, $date_to])
                    ->orderByDesc("tracks.created_at")
                    ->get();

                return view('order.list_order', compact('expenses','normal','track','agent'));
            }


            /**
             *
             * If you select To the last date
             *
             **/

        }
        elseif($user->hasRole('super'))
        {

            $expenses = Expenses::with('container')->where('finance_status','LIKE','pending')->count();

            $normal = NormalExpenses::where('finance_status','LIKE','pending')->count();

            $from   =  $request->input('date1');
            $to     =  $request->input('date2');
            $agent_id  =  $request->get('agent_id');
            $current_status_one = $request->input('current_status_one');

            if(($current_status_one !="") AND ($from  !="") AND ($to !="") AND ($agent_id != ''))
            {

                $agent =Agent::all();
                $track   = Track::select('tracks.*','bill_lading','organization_name')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('current_status','LIKE',$current_status_one)
                    ->where('tracks.id','=',$agent_id)
                    ->where('date' ,'>=', $from)
                    ->where('date', '<=', $to)
                    ->orderByDesc("tracks.created_at")
                    ->get();
                return view('order.super_list_order', compact('expenses','normal','track','agent'));

            }
            else  if(($current_status_one !="") AND ($from  !="") AND ($to !="") AND ($agent_id ==''))
            {

                $agent = Agent::all();

                $track   = Track::select('tracks.*','bill_lading','organization_name')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('current_status','LIKE',$current_status_one)
                    ->where('date' ,'>=', $from)
                    ->where('date' ,'<=', $to)
                    ->orderByDesc("tracks.created_at")
                    ->get();
                return view('order.super_list_order', compact('expenses','normal','track','agent'));
            }

            else  if(($current_status_one !="") AND ($from  !="") AND ($to =="") AND ($agent_id ==''))
            {

                $agent = Agent::all();
                $track   = Track::select('tracks.*','bill_lading','organization_name')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('current_status','LIKE',$current_status_one)
                    ->where('date' ,'>=', $from)
                    ->orderByDesc("tracks.created_at")
                    ->get();
                return view('order.super_list_order', compact('expenses','normal','track','agent'));
            }
            else  if(($current_status_one !="") AND ($from  =="") AND ($to =="") AND ($agent_id ==''))
            {


                $agent  = Agent::all();
                $track   = Track::select('tracks.*','bill_lading','organization_name')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('current_status','LIKE',$current_status_one)
                    ->orderByDesc("tracks.created_at")
                    ->get();
                return view('order.super_list_order', compact('expenses','normal','track','agent'));


            }
            elseif(($current_status_one =="") AND ($from  !="") AND ($to !="") AND ($agent_id != ''))
            {

                $agent =Agent::all();
                $track   = Track::select('tracks.*','bill_lading','organization_name')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('current_status','LIKE',$current_status_one)
                    ->where('tracks.id','=',$agent_id)
                    ->where('date' ,'>=', $from)
                    ->where('date', '<=', $to)
                    ->orderByDesc("tracks.created_at")
                    ->get();
                return view('order.super_list_order', compact('expenses','normal','track','agent'));


            }
            elseif(($current_status_one =="") AND ($from  !="") AND ($to !="") AND ($agent_id ==''))
            {


                $agent =Agent::all();
                $track   = Track::select('tracks.*','bill_lading','organization_name')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('current_status','LIKE',$current_status_one)
                    ->where('date' ,'>=', $from)
                    ->where('date', '<=', $to)
                    ->orderByDesc("tracks.created_at")
                    ->get();
                return view('order.super_list_order', compact('expenses','normal','track','agent'));

            }
            elseif(($current_status_one =="") AND ($from  !="") AND ($to =="") AND ($agent_id ==''))
            {

                $agent =Agent::all();
                $track   = Track::select('tracks.*','bill_lading','organization_name')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('date' ,'>=', $from)
                    ->orderByDesc("tracks.created_at")
                    ->get();
                return view('order.super_list_order', compact('expenses','normal','track','agent'));

            }
            elseif(($current_status_one =="") AND ($from  =="") AND ($to !="") AND ($agent_id != ''))
            {

                $agent =Agent::all();
                $track   = Track::select('tracks.*','bill_lading','organization_name')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('tracks.id','=',$agent_id)
                    ->where('date', '<=', $to)
                    ->orderByDesc("tracks.created_at")
                    ->get();
                return view('order.super_list_order', compact('expenses','normal','track','agent'));

            }
            elseif(($current_status_one =="") AND ($from  =="") AND ($to !="") AND ($agent_id == ''))
            {

                $agent =Agent::all();
                $track   = Track::select('tracks.*','bill_lading','organization_name')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('date', '<=', $to)
                    ->orderByDesc("tracks.created_at")
                    ->get();
                return view('order.super_list_order', compact('expenses','normal','track','agent'));

            }
            elseif(($current_status_one =="") AND ($from  =="") AND ($to =="") AND ($agent_id != ''))
            {

                $agent =Agent::all();
                $track   = Track::select('tracks.*','bill_lading','organization_name')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('tracks.id','=',$agent_id)
                    ->orderByDesc("tracks.created_at")
                    ->get();
                return view('order.super_list_order', compact('expenses','normal','track','agent'));

            }
            elseif(($current_status_one !="") AND ($from  =="") AND ($to !="") AND ($agent_id != ''))
            {

                $agent =Agent::all();
                $track   = Track::select('tracks.*','bill_lading','organization_name')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('current_status','LIKE',$current_status_one)
                    ->where('tracks.id','=',$agent_id)
                    ->where('date', '<=', $to)
                    ->orderByDesc("tracks.created_at")
                    ->get();
                return view('order.super_list_order', compact('expenses','normal','track','agent'));

            }
            elseif(($current_status_one !="") AND ($from  !="") AND ($to =="") AND ($agent_id != ''))
            {

                $agent =Agent::all();
                $track   = Track::select('tracks.*','bill_lading','organization_name')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('current_status','LIKE',$current_status_one)
                    ->where('tracks.id','=',$agent_id)
                    ->where('date' ,'>=', $from)
                    ->orderByDesc("tracks.created_at")
                    ->get();
                return view('order.super_list_order', compact('expenses','normal','track','agent'));

            }
            elseif(($current_status_one !="") AND ($from  =="") AND ($to !="") AND ($agent_id == ''))
            {

                $agent =Agent::all();
                $track   = Track::select('tracks.*','bill_lading','organization_name')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('current_status','LIKE',$current_status_one)
                    ->where('date', '<=', $to)
                    ->orderByDesc("tracks.created_at")
                    ->get();

                return view('order.super_list_order', compact('expenses','normal','track','agent'));

            }
            elseif(($current_status_one =="") AND ($from  !="") AND ($to =="") AND ($agent_id != ''))
            {

                $agent =Agent::all();
                $track   = Track::select('tracks.*','bill_lading','organization_name')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('tracks.id','=',$agent_id)
                    ->where('date' ,'>=', $from)
                    ->orderByDesc("tracks.created_at")
                    ->get();

                return view('order.super_list_order', compact('expenses','normal','track','agent'));

            }
            elseif(($current_status_one !="") AND ($from  =="") AND ($to =="") AND ($agent_id != ''))
            {

                $agent =Agent::all();
                $track   = Track::select('tracks.*','bill_lading','organization_name')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('current_status','LIKE',$current_status_one)
                    ->where('tracks.id','=',$agent_id)
                    ->orderByDesc("tracks.created_at")
                    ->get();

                return view('order.super_list_order', compact('expenses','normal','track','agent'));

            }
            else
            {
                $agent =Agent::all();
                $track   = Track::select('tracks.*','bill_lading','organization_name')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->orderByDesc("tracks.created_at")
                    ->get();

                return view('order.super_list_order', compact('expenses','normal','track','agent'));
            }





            /**
             *
             * If you select To the last date
             *
             **/

        }
        elseif($user->hasRole('documentation'))
        {


            $from   =  $request->input('date1');
            $to     =  $request->input('date2');
            $agent_id  =  $request->get('agent_id');
            $current_status_one = $request->input('current_status_one');

            if(($current_status_one !="") AND ($from  !="") AND ($to !="") AND ($agent_id != ''))
            {


                $agent =Agent::all();
                $track   = Track::select('tracks.*','bill_lading','organization_name')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('current_status','LIKE',$current_status_one)
                    ->where('tracks.id','=',$agent_id)
                    ->where('tracks.document_date_received','>=', $from)
                    ->where('date', '<=', $to)
                    ->orderByDesc("tracks.created_at")
                    ->get();
                return view('order.documentation_list_order', compact('expenses','normal','track','agent'));

            }
            else  if(($current_status_one !="") AND ($from  !="") AND ($to !="") AND ($agent_id ==''))
            {

                $agent = Agent::all();


                $track   = Track::select('tracks.*','bill_lading','organization_name')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('current_status','LIKE',$current_status_one)
                    ->where('tracks.document_date_received','>=', $from)
                    ->where('tracks.document_date_received','<=', $to)
                    ->orderByDesc("tracks.created_at")
                    ->get();
                return view('order.documentation_list_order', compact('expenses','normal','track','agent'));
            }

            else  if(($current_status_one !="") AND ($from  !="") AND ($to =="") AND ($agent_id ==''))
            {


                $agent = Agent::all();
                $track   = Track::select('tracks.*','bill_lading','organization_name')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('current_status','LIKE',$current_status_one)
                    ->where('tracks.document_date_received','>=', $from)
                    ->orderByDesc("tracks.created_at")
                    ->get();
                return view('order.documentation_list_order', compact('expenses','normal','track','agent'));
            }
            else  if(($current_status_one !="") AND ($from  =="") AND ($to =="") AND ($agent_id ==''))
            {



                $agent  = Agent::all();
                $track   = Track::select('tracks.*','bill_lading','organization_name')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('current_status','LIKE',$current_status_one)
                    ->orderByDesc("tracks.created_at")
                    ->whereBetween(DB::raw('YEAR(tracks.created_at)'),[$date_from, $date_to])
                    ->get();
                return view('order.documentation_list_order', compact('expenses','normal','track','agent'));


            }
            elseif(($current_status_one =="") AND ($from  !="") AND ($to !="") AND ($agent_id != ''))
            {


                $agent =Agent::all();
                $track   = Track::select('tracks.*','bill_lading','organization_name')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('current_status','LIKE',$current_status_one)
                    ->where('tracks.id','=',$agent_id)
                    ->where('tracks.document_date_received','>=', $from)
                    ->where('date', '<=', $to)
                    ->orderByDesc("tracks.created_at")
                    ->get();
                return view('order.documentation_list_order', compact('expenses','normal','track','agent'));


            }
            elseif(($current_status_one =="") AND ($from  !="") AND ($to !="") AND ($agent_id ==''))
            {


                $agent =Agent::all();
                $track   = Track::select('tracks.*','bill_lading','organization_name')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('current_status','LIKE',$current_status_one)
                    ->where('tracks.document_date_received','>=', $from)
                    ->where('date', '<=', $to)
                    ->orderByDesc("tracks.created_at")
                    ->get();
                return view('order.documentation_list_order', compact('expenses','normal','track','agent'));

            }
            elseif(($current_status_one =="") AND ($from  !="") AND ($to =="") AND ($agent_id ==''))
            {

                $agent =Agent::all();
                $track   = Track::select('tracks.*','bill_lading','organization_name')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('tracks.document_date_received','>=', $from)
                    ->orderByDesc("tracks.created_at")
                    ->get();


                return view('order.documentation_list_order', compact('expenses','normal','track','agent'));

            }
            elseif(($current_status_one =="") AND ($from  =="") AND ($to !="") AND ($agent_id != ''))
            {



                $agent =Agent::all();
                $track   = Track::select('tracks.*','bill_lading','organization_name')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('tracks.id','=',$agent_id)
                    ->where('date', '<=', $to)
                    ->orderByDesc("tracks.created_at")
                    ->get();
                return view('order.documentation_list_order', compact('expenses','normal','track','agent'));

            }
            elseif(($current_status_one =="") AND ($from  =="") AND ($to !="") AND ($agent_id == ''))
            {

                $agent =Agent::all();
                $track   = Track::select('tracks.*','bill_lading','organization_name')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('date', '<=', $to)
                    ->orderByDesc("tracks.created_at")
                    ->get();


                return view('order.documentation_list_order', compact('expenses','normal','track','agent'));

            }
            elseif(($current_status_one =="") AND ($from  =="") AND ($to =="") AND ($agent_id != ''))
            {



                $agent =Agent::all();
                $track   = Track::select('tracks.*','bill_lading','organization_name')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('tracks.id','=',$agent_id)
                    ->orderByDesc("tracks.created_at")
                    ->get();
                return view('order.documentation_list_order', compact('expenses','normal','track','agent'));

            }
            elseif(($current_status_one !="") AND ($from  =="") AND ($to !="") AND ($agent_id != ''))
            {

                $agent =Agent::all();
                $track   = Track::select('tracks.*','bill_lading','organization_name')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('current_status','LIKE',$current_status_one)
                    ->where('tracks.id','=',$agent_id)
                    ->where('date', '<=', $to)
                    ->orderByDesc("tracks.created_at")
                    ->get();


                return view('order.documentation_list_order', compact('expenses','normal','track','status','agent'));

            }
            elseif(($current_status_one !="") AND ($from  !="") AND ($to =="") AND ($agent_id != ''))
            {

                $agent =Agent::all();
                $track   = Track::select('tracks.*','bill_lading','organization_name')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('current_status','LIKE',$current_status_one)
                    ->where('tracks.id','=',$agent_id)
                    ->where('tracks.document_date_received','>=', $from)
                    ->orderByDesc("tracks.created_at")
                    ->get();


                return view('order.documentation_list_order', compact('expenses','normal','track','agent'));

            }
            elseif(($current_status_one !="") AND ($from  =="") AND ($to !="") AND ($agent_id == ''))
            {

                $agent =Agent::all();
                $track   = Track::select('tracks.*','bill_lading','organization_name')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('current_status','LIKE',$current_status_one)
                    ->where('date', '<=', $to)
                    ->orderByDesc("tracks.created_at")
                    ->get();


                return view('order.documentation_list_order', compact('expenses','normal','track','agent'));

            }
            elseif(($current_status_one =="") AND ($from  !="") AND ($to =="") AND ($agent_id != ''))
            {

                $agent =Agent::all();
                $track   = Track::select('tracks.*','bill_lading','organization_name')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('tracks.id','=',$agent_id)
                    ->where('tracks.document_date_received','>=', $from)
                    ->orderByDesc("tracks.created_at")
                    ->get();


                return view('order.documentation_list_order', compact('expenses','normal','track','agent'));

            }
            elseif(($current_status_one !="") AND ($from  =="") AND ($to =="") AND ($agent_id != ''))
            {

                $agent =Agent::all();
                $track   = Track::select('tracks.*','bill_lading','organization_name')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('current_status','LIKE',$current_status_one)
                    ->where('tracks.id','=',$agent_id)
                    ->whereBetween(DB::raw('YEAR(tracks.created_at)'),[$date_from, $date_to])
                    ->orderByDesc("tracks.created_at")
                    ->get();


                return view('order.documentation_list_order', compact('expenses','normal','track','agent'));

            }
            else
            {
                $agent =Agent::all();
                $track   = Track::select('tracks.*','bill_lading','organization_name')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->whereBetween(DB::raw('YEAR(tracks.created_at)'),[$from, $to])
                    ->whereBetween(DB::raw('YEAR(tracks.created_at)'),[$date_from, $date_to])
                    ->orderByDesc("tracks.created_at")
                    ->get();


                return view('order.documentation_list_order', compact('expenses','normal','track','agent'));
            }





            /**
             *
             * If you select To the last date
             *
             **/

        }
        elseif($user->hasRole('sales'))
        {


            $from   =  $request->input('date1');
            $to     =  $request->input('date2');
            $agent_id  =  $request->get('agent_id');
            $current_status_one = $request->input('current_status_one');

            if(($current_status_one !="") AND ($from  !="") AND ($to !="") AND ($agent_id != ''))
            {


                $agent =Agent::all();
                $track   = Track::select('tracks.*','bill_lading','organization_name')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('current_status','LIKE',$current_status_one)
                    ->where('tracks.id','=',$agent_id)
                    ->where('tracks.document_date_received','>=', $from)
                    ->where('date', '<=', $to)
                    ->orderByDesc("tracks.created_at")
                    ->get();
                return view('staff.sales_list_order', compact('expenses','normal','track','agent'));

            }
            else  if(($current_status_one !="") AND ($from  !="") AND ($to !="") AND ($agent_id ==''))
            {

                $agent = Agent::all();

                $track   = Track::select('tracks.*','bill_lading','organization_name')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('current_status','LIKE',$current_status_one)
                    ->where('tracks.document_date_received','>=', $from)
                    ->where('tracks.document_date_received','<=', $to)
                    ->orderByDesc("tracks.created_at")
                    ->get();
                return view('staff.sales_list_order', compact('expenses','normal','track','agent'));
            }

            else  if(($current_status_one !="") AND ($from  !="") AND ($to =="") AND ($agent_id ==''))
            {

                $agent = Agent::all();
                $track   = Track::select('tracks.*','bill_lading','organization_name')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('current_status','LIKE',$current_status_one)
                    ->where('tracks.document_date_received','>=', $from)
                    ->orderByDesc("tracks.created_at")
                    ->get();
                return view('staff.sales_list_order', compact('expenses','normal','track','agent'));
            }
            else  if(($current_status_one !="") AND ($from  =="") AND ($to =="") AND ($agent_id ==''))
            {


                $agent  = Agent::all();
                $track   = Track::select('tracks.*','bill_lading','organization_name')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('current_status','LIKE',$current_status_one)
                    ->orderByDesc("tracks.created_at")
                    ->whereBetween(DB::raw('YEAR(tracks.created_at)'),[$date_from, $date_to])
                    ->get();
                return view('staff.sales_list_order', compact('expenses','normal','track','agent'));


            }
            elseif(($current_status_one =="") AND ($from  !="") AND ($to !="") AND ($agent_id != ''))
            {

                $agent =Agent::all();
                $track   = Track::select('tracks.*','bill_lading','organization_name')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('current_status','LIKE',$current_status_one)
                    ->where('tracks.id','=',$agent_id)
                    ->where('tracks.document_date_received','>=', $from)
                    ->where('date', '<=', $to)
                    ->orderByDesc("tracks.created_at")
                    ->get();
                return view('staff.sales_list_order', compact('expenses','normal','track','agent'));


            }
            elseif(($current_status_one =="") AND ($from  !="") AND ($to !="") AND ($agent_id ==''))
            {

                $agent =Agent::all();
                $track   = Track::select('tracks.*','bill_lading','organization_name')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('current_status','LIKE',$current_status_one)
                    ->where('tracks.document_date_received','>=', $from)
                    ->where('date', '<=', $to)
                    ->orderByDesc("tracks.created_at")
                    ->get();
                return view('staff.sales_list_order', compact('expenses','normal','track','agent'));

            }
            elseif(($current_status_one =="") AND ($from  !="") AND ($to =="") AND ($agent_id ==''))
            {
                $agent =Agent::all();
                $track   = Track::select('tracks.*','bill_lading','organization_name')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('tracks.document_date_received','>=', $from)
                    ->orderByDesc("tracks.created_at")
                    ->get();
                return view('staff.sales_list_order', compact('expenses','normal','track','agent'));

            }
            elseif(($current_status_one =="") AND ($from  =="") AND ($to !="") AND ($agent_id != ''))
            {

                $agent =Agent::all();
                $track   = Track::select('tracks.*','bill_lading','organization_name')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('tracks.id','=',$agent_id)
                    ->where('date', '<=', $to)
                    ->orderByDesc("tracks.created_at")
                    ->get();
                return view('staff.sales_list_order', compact('expenses','normal','track','agent'));

            }
            elseif(($current_status_one =="") AND ($from  =="") AND ($to !="") AND ($agent_id == ''))
            {

                $agent =Agent::all();
                $track   = Track::select('tracks.*','bill_lading','organization_name')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('date', '<=', $to)
                    ->orderByDesc("tracks.created_at")
                    ->get();

                return view('staff.sales_list_order', compact('expenses','normal','track','agent'));

            }
            elseif(($current_status_one =="") AND ($from  =="") AND ($to =="") AND ($agent_id != ''))
            {

                $agent =Agent::all();
                $track   = Track::select('tracks.*','bill_lading','organization_name')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('tracks.id','=',$agent_id)
                    ->orderByDesc("tracks.created_at")
                    ->get();

                return view('staff.sales_list_order', compact('expenses','normal','track','agent'));

            }
            elseif(($current_status_one !="") AND ($from  =="") AND ($to !="") AND ($agent_id != ''))
            {

                $agent =Agent::all();
                $track   = Track::select('tracks.*','bill_lading','organization_name')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('current_status','LIKE',$current_status_one)
                    ->where('tracks.id','=',$agent_id)
                    ->where('date', '<=', $to)
                    ->orderByDesc("tracks.created_at")
                    ->get();

                return view('staff.sales_list_order', compact('expenses','normal','track','agent'));

            }
            elseif(($current_status_one !="") AND ($from  !="") AND ($to =="") AND ($agent_id != ''))
            {

                $agent =Agent::all();
                $track   = Track::select('tracks.*','bill_lading','organization_name')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('current_status','LIKE',$current_status_one)
                    ->where('tracks.id','=',$agent_id)
                    ->where('tracks.document_date_received','>=', $from)
                    ->orderByDesc("tracks.created_at")
                    ->get();

                return view('staff.sales_list_order', compact('expenses','normal','track','agent'));

            }
            elseif(($current_status_one !="") AND ($from  =="") AND ($to !="") AND ($agent_id == ''))
            {

                $agent =Agent::all();
                $track   = Track::select('tracks.*','bill_lading','organization_name')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('current_status','LIKE',$current_status_one)
                    ->where('date', '<=', $to)
                    ->orderByDesc("tracks.created_at")
                    ->get();

                return view('staff.sales_list_order', compact('expenses','normal','track','agent'));

            }
            elseif(($current_status_one =="") AND ($from  !="") AND ($to =="") AND ($agent_id != ''))
            {

                $agent =Agent::all();
                $track   = Track::select('tracks.*','bill_lading','organization_name')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('tracks.id','=',$agent_id)
                    ->where('tracks.document_date_received','>=', $from)
                    ->orderByDesc("tracks.created_at")
                    ->get();

                return view('staff.sales_list_order', compact('expenses','normal','track','agent'));

            }
            elseif(($current_status_one !="") AND ($from  =="") AND ($to =="") AND ($agent_id != ''))
            {

                $agent =Agent::all();
                $track   = Track::select('tracks.*','bill_lading','organization_name')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('current_status','LIKE',$current_status_one)
                    ->where('tracks.id','=',$agent_id)
                    ->orderByDesc("tracks.created_at")
                    ->get();

                return view('staff.sales_list_order', compact('expenses','normal','track','agent'));

            }
            else
            {
                $agent =Agent::all();
                $track   = Track::select('tracks.*','bill_lading','organization_name')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->whereBetween(DB::raw('YEAR(tracks.created_at)'),[$date_from, $date_to])
                    ->orderByDesc("tracks.created_at")
                    ->get();

                return view('staff.sales_list_order', compact('expenses','normal','track','agent'));
            }





            /**
             *
             * If you select To the last date
             *
             **/

        }
        elseif($user->hasRole('finance'))
        {

            $expenses = Expenses::with('container')->where('finance_status','LIKE','pending')->count();

            $normal = NormalExpenses::where('finance_status','LIKE','pending')->count();

            $from   =  $request->input('date1');
            $to     =  $request->input('date2');
            $agent_id  =  $request->get('agent_id');
            $current_status_one = $request->input('current_status_one');

            if(($current_status_one !="") AND ($from  !="") AND ($to !="") AND ($agent_id != ''))
            {


                $agent =Agent::all();
                $track   = Track::select('tracks.*','bill_lading','organization_name')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('current_status','LIKE',$current_status_one)
                    ->where('tracks.id','=',$agent_id)
                    ->where('tracks.document_date_received','>=', $from)
                    ->where('date', '<=', $to)
                    ->orderByDesc("tracks.created_at")
                    ->get();
                return view('finance.finance_list_order', compact('expenses','normal','track','agent'));

            }
            else  if(($current_status_one !="") AND ($from  !="") AND ($to !="") AND ($agent_id ==''))
            {

                $agent = Agent::all();

                $track   = Track::select('tracks.*','bill_lading','organization_name')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('current_status','LIKE',$current_status_one)
                    ->where('tracks.document_date_received','>=', $from)
                    ->where('tracks.document_date_received','<=', $to)
                    ->orderByDesc("tracks.created_at")
                    ->get();
                return view('finance.finance_list_order', compact('expenses','normal','track','agent'));
            }

            else  if(($current_status_one !="") AND ($from  !="") AND ($to =="") AND ($agent_id ==''))
            {

                $agent = Agent::all();
                $track   = Track::select('tracks.*','bill_lading','organization_name')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('current_status','LIKE',$current_status_one)
                    ->where('tracks.document_date_received','>=', $from)
                    ->orderByDesc("tracks.created_at")
                    ->get();
                return view('finance.finance_list_order', compact('expenses','normal','track','agent'));
            }
            else  if(($current_status_one !="") AND ($from  =="") AND ($to =="") AND ($agent_id ==''))
            {


                $agent  = Agent::all();
                $track   = Track::select('tracks.*','bill_lading','organization_name')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('current_status','LIKE',$current_status_one)
                    ->whereBetween(DB::raw('YEAR(tracks.created_at)'),[$date_from, $date_to])
                    ->orderByDesc("tracks.created_at")
                    ->get();
                return view('finance.finance_list_order', compact('expenses','normal','track','agent'));


            }
            elseif(($current_status_one =="") AND ($from  !="") AND ($to !="") AND ($agent_id != ''))
            {

                $agent =Agent::all();
                $track   = Track::select('tracks.*','bill_lading','organization_name')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('current_status','LIKE',$current_status_one)
                    ->where('tracks.id','=',$agent_id)
                    ->where('tracks.document_date_received','>=', $from)
                    ->where('date', '<=', $to)
                    ->orderByDesc("tracks.created_at")
                    ->get();
                return view('finance.finance_list_order', compact('expenses','normal','track','agent'));


            }
            elseif(($current_status_one =="") AND ($from  !="") AND ($to !="") AND ($agent_id ==''))
            {

                $agent =Agent::all();
                $track   = Track::select('tracks.*','bill_lading','organization_name')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('current_status','LIKE',$current_status_one)
                    ->where('tracks.document_date_received','>=', $from)
                    ->where('date', '<=', $to)
                    ->orderByDesc("tracks.created_at")
                    ->get();
                return view('finance.finance_list_order', compact('expenses','normal','track','agent'));

            }
            elseif(($current_status_one =="") AND ($from  !="") AND ($to =="") AND ($agent_id ==''))
            {

                $agent =Agent::all();
                $track   = Track::select('tracks.*','bill_lading','organization_name')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('tracks.document_date_received','>=', $from)
                    ->orderByDesc("tracks.created_at")
                    ->get();
                return view('finance.finance_list_order', compact('expenses','normal','track','agent'));

            }
            elseif(($current_status_one =="") AND ($from  =="") AND ($to !="") AND ($agent_id != ''))
            {

                $agent =Agent::all();
                $track   = Track::select('tracks.*','bill_lading','organization_name')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('tracks.id','=',$agent_id)
                    ->where('date', '<=', $to)
                    ->orderByDesc("tracks.created_at")
                    ->get();

                return view('finance.finance_list_order', compact('expenses','normal','track','agent'));

            }
            elseif(($current_status_one =="") AND ($from  =="") AND ($to !="") AND ($agent_id == ''))
            {

                $agent =Agent::all();
                $track   = Track::select('tracks.*','bill_lading','organization_name')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('date', '<=', $to)
                    ->orderByDesc("tracks.created_at")
                    ->get();

                return view('finance.finance_list_order', compact('expenses','normal','track','agent'));

            }
            elseif(($current_status_one =="") AND ($from  =="") AND ($to =="") AND ($agent_id != ''))
            {

                $agent =Agent::all();
                $track   = Track::select('tracks.*','bill_lading','organization_name')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('tracks.id','=',$agent_id)
                    ->orderByDesc("tracks.created_at")
                    ->get();

                return view('finance.finance_list_order', compact('expenses','normal','track','agent'));

            }
            elseif(($current_status_one !="") AND ($from  =="") AND ($to !="") AND ($agent_id != ''))
            {

                $agent =Agent::all();
                $track   = Track::select('tracks.*','bill_lading','organization_name')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('current_status','LIKE',$current_status_one)
                    ->where('tracks.id','=',$agent_id)
                    ->where('date', '<=', $to)
                    ->orderByDesc("tracks.created_at")
                    ->get();

                return view('finance.finance_list_order', compact('expenses','normal','track','agent'));

            }
            elseif(($current_status_one !="") AND ($from  !="") AND ($to =="") AND ($agent_id != ''))
            {

                $agent =Agent::all();
                $track   = Track::select('tracks.*','bill_lading','organization_name')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('current_status','LIKE',$current_status_one)
                    ->where('tracks.id','=',$agent_id)
                    ->where('tracks.document_date_received','>=', $from)
                    ->orderByDesc("tracks.created_at")
                    ->get();

                return view('finance.finance_list_order', compact('expenses','normal','track','agent'));

            }
            elseif(($current_status_one !="") AND ($from  =="") AND ($to !="") AND ($agent_id == ''))
            {


                $agent =Agent::all();
                $track   = Track::select('tracks.*','bill_lading','organization_name')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('current_status','LIKE',$current_status_one)
                    ->where('date', '<=', $to)
                    ->orderByDesc("tracks.created_at")
                    ->get();
                return view('finance.finance_list_order', compact('expenses','normal','track','agent'));

            }
            elseif(($current_status_one =="") AND ($from  !="") AND ($to =="") AND ($agent_id != ''))
            {


                $agent =Agent::all();
                $track   = Track::select('tracks.*','bill_lading','organization_name')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('tracks.id','=',$agent_id)
                    ->where('tracks.document_date_received','>=', $from)
                    ->orderByDesc("tracks.created_at")
                    ->get();
                return view('finance.finance_list_order', compact('expenses','normal','track','agent'));

            }
            elseif(($current_status_one !="") AND ($from  =="") AND ($to =="") AND ($agent_id != ''))
            {


                $agent =Agent::all();
                $track   = Track::select('tracks.*','bill_lading','organization_name')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('current_status','LIKE',$current_status_one)
                    ->where('tracks.id','=',$agent_id)
                    ->orderByDesc("tracks.created_at")
                    ->get();
                return view('finance.finance_list_order', compact('expenses','normal','track','agent'));

            }
            else
            {
                $agent =Agent::all();
                $track   = Track::select('tracks.*','bill_lading','organization_name')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->whereBetween(DB::raw('YEAR(tracks.created_at)'),[$date_from, $date_to])
                    ->orderByDesc("tracks.created_at")
                    ->get();

                return view('finance.finance_list_order', compact('expenses','normal','track','agent'));
            }





            /**
             *
             * If you select To the last date
             *
             **/

        }
        elseif($user->hasRole('declaration'))
        {


            $from   =  $request->input('date1');
            $to     =  $request->input('date2');
            $agent_id  =  $request->get('agent_id');
            $current_status_one = $request->input('current_status_one');

            if(($current_status_one !="") AND ($from  !="") AND ($to !="") AND ($agent_id != ''))
            {

                $agent =Agent::all();
                $track   = Track::select('tracks.*','bill_lading','organization_name')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('current_status','LIKE',$current_status_one)
                    ->where('tracks.id','=',$agent_id)
                    ->where('tracks.document_date_received','>=', $from)
                    ->where('date', '<=', $to)
                    ->orderByDesc("tracks.created_at")
                    ->get();
                return view('order.declaration_list_order', compact('expenses','normal','track','agent'));

            }
            else  if(($current_status_one !="") AND ($from  !="") AND ($to !="") AND ($agent_id ==''))
            {

                $agent = Agent::all();

                $track   = Track::select('tracks.*','bill_lading','organization_name')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('current_status','LIKE',$current_status_one)
                    ->where('tracks.document_date_received','>=', $from)
                    ->where('tracks.document_date_received','<=', $to)
                    ->orderByDesc("tracks.created_at")
                    ->get();
                return view('order.declaration_list_order', compact('expenses','normal','track','agent'));
            }

            else  if(($current_status_one !="") AND ($from  !="") AND ($to =="") AND ($agent_id ==''))
            {

                $agent = Agent::all();
                $track   = Track::select('tracks.*','bill_lading','organization_name')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('current_status','LIKE',$current_status_one)
                    ->where('tracks.document_date_received','>=', $from)
                    ->orderByDesc("tracks.created_at")
                    ->get();
                return view('order.declaration_list_order', compact('expenses','normal','track','agent'));
            }
            else  if(($current_status_one !="") AND ($from  =="") AND ($to =="") AND ($agent_id ==''))
            {

                $agent  = Agent::all();
                $track   = Track::select('tracks.*','bill_lading','organization_name')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('current_status','LIKE',$current_status_one)
                    ->whereBetween(DB::raw('YEAR(tracks.created_at)'),[$date_from, $date_to])
                    ->orderByDesc("tracks.created_at")
                    ->get();
                return view('order.declaration_list_order', compact('expenses','normal','track','agent'));


            }
            elseif(($current_status_one =="") AND ($from  !="") AND ($to !="") AND ($agent_id != ''))
            {

                $agent =Agent::all();
                $track   = Track::select('tracks.*','bill_lading','organization_name')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('current_status','LIKE',$current_status_one)
                    ->where('tracks.id','=',$agent_id)
                    ->where('tracks.document_date_received','>=', $from)
                    ->where('date', '<=', $to)
                    ->orderByDesc("tracks.created_at")
                    ->get();
                return view('order.declaration_list_order', compact('expenses','normal','track','agent'));


            }
            elseif(($current_status_one =="") AND ($from  !="") AND ($to !="") AND ($agent_id ==''))
            {

                $agent =Agent::all();
                $track   = Track::select('tracks.*','bill_lading','organization_name')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('current_status','LIKE',$current_status_one)
                    ->where('tracks.document_date_received','>=', $from)
                    ->where('date', '<=', $to)
                    ->orderByDesc("tracks.created_at")
                    ->get();
                return view('order.declaration_list_order', compact('expenses','normal','track','agent'));

            }
            elseif(($current_status_one =="") AND ($from  !="") AND ($to =="") AND ($agent_id ==''))
            {

                $agent =Agent::all();
                $track   = Track::select('tracks.*','bill_lading','organization_name')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('tracks.document_date_received','>=', $from)
                    ->orderByDesc("tracks.created_at")
                    ->get();
                return view('order.declaration_list_order', compact('expenses','normal','track','agent'));

            }
            elseif(($current_status_one =="") AND ($from  =="") AND ($to !="") AND ($agent_id != ''))
            {

                $agent =Agent::all();
                $track   = Track::select('tracks.*','bill_lading','organization_name')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('tracks.id','=',$agent_id)
                    ->where('date', '<=', $to)
                    ->orderByDesc("tracks.created_at")
                    ->get();
                return view('order.declaration_list_order', compact('expenses','normal','track','agent'));

            }
            elseif(($current_status_one =="") AND ($from  =="") AND ($to !="") AND ($agent_id == ''))
            {

                $agent =Agent::all();
                $track   = Track::select('tracks.*','bill_lading','organization_name')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('date', '<=', $to)
                    ->orderByDesc("tracks.created_at")
                    ->get();
                return view('order.declaration_list_order', compact('expenses','normal','track','agent'));

            }
            elseif(($current_status_one =="") AND ($from  =="") AND ($to =="") AND ($agent_id != ''))
            {

                $agent =Agent::all();
                $track   = Track::select('tracks.*','bill_lading','organization_name')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('tracks.id','=',$agent_id)
                    ->orderByDesc("tracks.created_at")
                    ->get();
                return view('order.declaration_list_order', compact('expenses','normal','track','agent'));

            }
            elseif(($current_status_one !="") AND ($from  =="") AND ($to !="") AND ($agent_id != ''))
            {

                $agent =Agent::all();
                $track   = Track::select('tracks.*','bill_lading','organization_name')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('current_status','LIKE',$current_status_one)
                    ->where('tracks.id','=',$agent_id)
                    ->where('date', '<=', $to)
                    ->orderByDesc("tracks.created_at")
                    ->get();
                return view('order.declaration_list_order', compact('expenses','normal','track','agent'));

            }
            elseif(($current_status_one !="") AND ($from  !="") AND ($to =="") AND ($agent_id != ''))
            {

                $agent =Agent::all();
                $track   = Track::select('tracks.*','bill_lading','organization_name')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('current_status','LIKE',$current_status_one)
                    ->where('tracks.id','=',$agent_id)
                    ->where('tracks.document_date_received','>=', $from)
                    ->orderByDesc("tracks.created_at")
                    ->get();
                return view('order.declaration_list_order', compact('expenses','normal','track','agent'));

            }
            elseif(($current_status_one !="") AND ($from  =="") AND ($to !="") AND ($agent_id == ''))
            {

                $agent =Agent::all();
                $track   = Track::select('tracks.*','bill_lading','organization_name')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('current_status','LIKE',$current_status_one)
                    ->where('date', '<=', $to)
                    ->orderByDesc("tracks.created_at")
                    ->get();
                return view('order.declaration_list_order', compact('expenses','normal','track','agent'));

            }
            elseif(($current_status_one =="") AND ($from  !="") AND ($to =="") AND ($agent_id != ''))
            {

                $agent =Agent::all();
                $track   = Track::select('tracks.*','bill_lading','organization_name')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('tracks.id','=',$agent_id)
                    ->where('tracks.document_date_received','>=', $from)
                    ->orderByDesc("tracks.created_at")
                    ->get();
                return view('order.declaration_list_order', compact('expenses','normal','track','agent'));

            }
            elseif(($current_status_one !="") AND ($from  =="") AND ($to =="") AND ($agent_id != ''))
            {

                $agent =Agent::all();
                $track   = Track::select('tracks.*','bill_lading','organization_name')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('current_status','LIKE',$current_status_one)
                    ->where('tracks.id','=',$agent_id)
                    ->whereBetween(DB::raw('YEAR(tracks.created_at)'),[$date_from, $date_to])
                    ->orderByDesc("tracks.created_at")
                    ->get();
                return view('order.declaration_list_order', compact('expenses','normal','track','agent'));

            }
            else
            {

                $agent =Agent::all();
                $track   = Track::select('tracks.*','bill_lading','organization_name')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->whereBetween(DB::raw('YEAR(tracks.created_at)'),[$date_from, $date_to])
                    ->orderByDesc("tracks.created_at")
                    ->get();
                return view('order.declaration_list_order', compact('expenses','normal','track','agent'));
            }





            /**
             *
             * If you select To the last date
             *
             **/

        }
        elseif($user->hasRole('shipping'))
        {


            $from   =  $request->input('date1');
            $to     =  $request->input('date2');
            $agent_id  =  $request->get('agent_id');
            $current_status_one = $request->input('current_status_one');

            if(($current_status_one !="") AND ($from  !="") AND ($to !="") AND ($agent_id != ''))
            {

                $agent =Agent::all();
                $track   = Track::select('tracks.*','bill_lading','organization_name')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('current_status','LIKE',$current_status_one)
                    ->where('tracks.id','=',$agent_id)
                    ->where('tracks.document_date_received','>=', $from)
                    ->where('date', '<=', $to)
                    ->orderByDesc("tracks.created_at")
                    ->get();
                return view('order.shipping_list_order', compact('expenses','normal','track','agent'));

            }
            else  if(($current_status_one !="") AND ($from  !="") AND ($to !="") AND ($agent_id ==''))
            {
                $agent = Agent::all();

                $track   = Track::select('tracks.*','bill_lading','organization_name')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('current_status','LIKE',$current_status_one)
                    ->where('tracks.document_date_received','>=', $from)
                    ->where('tracks.document_date_received','<=', $to)
                    ->orderByDesc("tracks.created_at")
                    ->get();
                return view('order.shipping_list_order', compact('expenses','normal','track','agent'));
            }

            else  if(($current_status_one !="") AND ($from  !="") AND ($to =="") AND ($agent_id ==''))
            {

                $agent = Agent::all();
                $track   = Track::select('tracks.*','bill_lading','organization_name')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('current_status','LIKE',$current_status_one)
                    ->where('tracks.document_date_received','>=', $from)
                    ->orderByDesc("tracks.created_at")
                    ->get();
                return view('order.shipping_list_order', compact('expenses','normal','track','agent'));
            }
            else  if(($current_status_one !="") AND ($from  =="") AND ($to =="") AND ($agent_id ==''))
            {


                $agent  = Agent::all();
                $track   = Track::select('tracks.*','bill_lading','organization_name')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('current_status','LIKE',$current_status_one)
                    ->whereBetween(DB::raw('YEAR(tracks.created_at)'),[$date_from, $date_to])
                    ->orderByDesc("tracks.created_at")
                    ->get();
                return view('order.shipping_list_order', compact('expenses','normal','track','agent'));


            }
            elseif(($current_status_one =="") AND ($from  !="") AND ($to !="") AND ($agent_id != ''))
            {

                $agent =Agent::all();
                $track   = Track::select('tracks.*','bill_lading','organization_name')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('current_status','LIKE',$current_status_one)
                    ->where('tracks.id','=',$agent_id)
                    ->where('tracks.document_date_received','>=', $from)
                    ->where('date', '<=', $to)
                    ->orderByDesc("tracks.created_at")
                    ->get();
                return view('order.shipping_list_order', compact('expenses','normal','track','agent'));


            }
            elseif(($current_status_one =="") AND ($from  !="") AND ($to !="") AND ($agent_id ==''))
            {

                $agent =Agent::all();
                $track   = Track::select('tracks.*','bill_lading','organization_name')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('current_status','LIKE',$current_status_one)
                    ->where('tracks.document_date_received','>=', $from)
                    ->where('date', '<=', $to)
                    ->orderByDesc("tracks.created_at")
                    ->get();
                return view('order.shipping_list_order', compact('expenses','normal','track','agent'));

            }
            elseif(($current_status_one =="") AND ($from  !="") AND ($to =="") AND ($agent_id ==''))
            {

                $agent =Agent::all();
                $track   = Track::select('tracks.*','bill_lading','organization_name')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('tracks.document_date_received','>=', $from)
                    ->orderByDesc("tracks.created_at")
                    ->get();
                return view('order.shipping_list_order', compact('expenses','normal','track','agent'));

            }
            elseif(($current_status_one =="") AND ($from  =="") AND ($to !="") AND ($agent_id != ''))
            {

                $agent =Agent::all();
                $track   = Track::select('tracks.*','bill_lading','organization_name')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('tracks.id','=',$agent_id)
                    ->where('date', '<=', $to)
                    ->orderByDesc("tracks.created_at")
                    ->get();
                return view('order.shipping_list_order', compact('expenses','normal','track','agent'));

            }
            elseif(($current_status_one =="") AND ($from  =="") AND ($to !="") AND ($agent_id == ''))
            {


                $agent =Agent::all();
                $track   = Track::select('tracks.*','bill_lading','organization_name')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('date', '<=', $to)
                    ->orderByDesc("tracks.created_at")
                    ->get();
                return view('order.shipping_list_order', compact('expenses','normal','track','agent'));

            }
            elseif(($current_status_one =="") AND ($from  =="") AND ($to =="") AND ($agent_id != ''))
            {


                $agent =Agent::all();
                $track   = Track::select('tracks.*','bill_lading','organization_name')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('tracks.id','=',$agent_id)
                    ->orderByDesc("tracks.created_at")
                    ->get();
                return view('order.shipping_list_order', compact('expenses','normal','track','agent'));

            }
            elseif(($current_status_one !="") AND ($from  =="") AND ($to !="") AND ($agent_id != ''))
            {


                $agent =Agent::all();
                $track   = Track::select('tracks.*','bill_lading','organization_name')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('current_status','LIKE',$current_status_one)
                    ->where('tracks.id','=',$agent_id)
                    ->where('date', '<=', $to)
                    ->orderByDesc("tracks.created_at")
                    ->get();
                return view('order.shipping_list_order', compact('expenses','normal','track','agent'));

            }
            elseif(($current_status_one !="") AND ($from  !="") AND ($to =="") AND ($agent_id != ''))
            {

                $agent =Agent::all();
                $track   = Track::select('tracks.*','bill_lading','organization_name')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('current_status','LIKE',$current_status_one)
                    ->where('tracks.id','=',$agent_id)
                    ->where('tracks.document_date_received','>=', $from)
                    ->orderByDesc("tracks.created_at")
                    ->get();
                return view('order.shipping_list_order', compact('expenses','normal','track','agent'));

            }
            elseif(($current_status_one !="") AND ($from  =="") AND ($to !="") AND ($agent_id == ''))
            {

                $agent =Agent::all();
                $track   = Track::select('tracks.*','bill_lading','organization_name')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('current_status','LIKE',$current_status_one)
                    ->where('date', '<=', $to)
                    ->orderByDesc("tracks.created_at")
                    ->get();
                return view('order.shipping_list_order', compact('expenses','normal','track','agent'));

            }
            elseif(($current_status_one =="") AND ($from  !="") AND ($to =="") AND ($agent_id != ''))
            {


                $agent =Agent::all();
                $track   = Track::select('tracks.*','bill_lading','organization_name')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('tracks.id','=',$agent_id)
                    ->where('tracks.document_date_received','>=', $from)
                    ->orderByDesc("tracks.created_at")
                    ->get();
                return view('order.shipping_list_order', compact('expenses','normal','track','agent'));

            }
            elseif(($current_status_one !="") AND ($from  =="") AND ($to =="") AND ($agent_id != ''))
            {


                $agent =Agent::all();
                $track   = Track::select('tracks.*','bill_lading','organization_name')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('current_status','LIKE',$current_status_one)
                    ->where('tracks.id','=',$agent_id)
                    ->orderByDesc("tracks.created_at")
                    ->get();
                return view('order.shipping_list_order', compact('expenses','normal','track','agent'));

            }
            else
            {

                $agent =Agent::all();
                $track   = Track::select('tracks.*','bill_lading','organization_name')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->whereBetween(DB::raw('YEAR(tracks.created_at)'),[$from, $to])
                    ->orderByDesc("tracks.created_at")
                    ->get();
                return view('order.shipping_list_order', compact('expenses','normal','track','agent'));
            }





            /**
             *
             * If you select To the last date
             *
             **/

        }
        elseif($user->hasRole('agent'))
        {


            $user_id = Auth::id();
            $from   =  $request->input('date1');
            $to     =  $request->input('date2');
            $agent_id  =  $request->get('agent_id');
            $current_status_one = $request->input('current_status_one');
            $profile = User::where('id','=',$user_id)->get();

            if(($current_status_one !="") AND ($from  !="") AND ($to !="") AND ($agent_id != ''))
            {



                $agent =Agent::all();
                $track   = Track::select('tracks.*','bill_lading','organization_name')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('current_status','LIKE',$current_status_one)
                    ->where('tracks.id','=',$agent_id)
                    ->where('tracks.document_date_received','>=', $from)
                    ->where('date', '<=', $to)
                    ->where('agents.user_id','=',$user_id)
                    ->orderByDesc("tracks.created_at")
                    ->get();
                return view('order.my_oder', compact('track','agent','profile'));

            }
            else  if(($current_status_one !="") AND ($from  !="") AND ($to !="") AND ($agent_id ==''))
            {

                $agent = Agent::all();

                $track   = Track::select('tracks.*','bill_lading','organization_name')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('current_status','LIKE',$current_status_one)
                    ->where('tracks.document_date_received','>=', $from)
                    ->where('tracks.document_date_received','<=', $to)
                    ->where('agents.user_id','=',$user_id)
                    ->orderByDesc("tracks.created_at")
                    ->get();
                return view('order.my_oder', compact('track','agent','profile'));
            }

            else  if(($current_status_one !="") AND ($from  !="") AND ($to =="") AND ($agent_id ==''))
            {

                $agent = Agent::all();
                $track   = Track::select('tracks.*','bill_lading','organization_name')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('current_status','LIKE',$current_status_one)
                    ->where('tracks.document_date_received','>=', $from)
                    ->where('agents.user_id','=',$user_id)
                    ->orderByDesc("tracks.created_at")
                    ->get();
                return view('order.my_oder', compact('track','agent','profile'));
            }
            else  if(($current_status_one !="") AND ($from  =="") AND ($to =="") AND ($agent_id ==''))
            {


                $agent  = Agent::all();
                $track   = Track::select('tracks.*','bill_lading','organization_name')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('current_status','LIKE',$current_status_one)
                    ->whereBetween(DB::raw('YEAR(tracks.created_at)'),[$date_from, $date_to])
                    ->orderByDesc("tracks.created_at")
                    ->where('agents.user_id','=',$user_id)
                    ->get();
                return view('order.my_oder', compact('track','agent','profile'));


            }
            elseif(($current_status_one =="") AND ($from  !="") AND ($to !="") AND ($agent_id != ''))
            {

                $agent =Agent::all();
                $track   = Track::select('tracks.*','bill_lading','organization_name')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('current_status','LIKE',$current_status_one)
                    ->where('tracks.id','=',$agent_id)
                    ->where('tracks.document_date_received','>=', $from)
                    ->where('date', '<=', $to)
                    ->where('agents.user_id','=',$user_id)
                    ->orderByDesc("tracks.created_at")
                    ->get();
                return view('order.my_oder', compact('track','agent','profile'));


            }
            elseif(($current_status_one =="") AND ($from  !="") AND ($to !="") AND ($agent_id ==''))
            {

                $agent =Agent::all();
                $track   = Track::select('tracks.*','bill_lading','organization_name')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('current_status','LIKE',$current_status_one)
                    ->where('tracks.document_date_received','>=', $from)
                    ->where('date', '<=', $to)
                    ->where('agents.user_id','=',$user_id)
                    ->orderByDesc("tracks.created_at")
                    ->get();
                return view('order.my_oder', compact('track','agent','profile'));

            }
            elseif(($current_status_one =="") AND ($from  !="") AND ($to =="") AND ($agent_id ==''))
            {

                $agent =Agent::all();
                $track   = Track::select('tracks.*','bill_lading','organization_name')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('tracks.document_date_received','>=', $from)
                    ->where('agents.user_id','=',$user_id)
                    ->orderByDesc("tracks.created_at")
                    ->get();
                return view('order.my_oder', compact('track','agent'));

            }
            elseif(($current_status_one =="") AND ($from  =="") AND ($to !="") AND ($agent_id != ''))
            {

                $agent =Agent::all();
                $track   = Track::select('tracks.*','bill_lading','organization_name')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('tracks.id','=',$agent_id)
                    ->where('date', '<=', $to)
                    ->where('agents.user_id','=',$user_id)
                    ->orderByDesc("tracks.created_at")
                    ->get();
                return view('order.my_oder', compact('track','agent','profile'));

            }
            elseif(($current_status_one =="") AND ($from  =="") AND ($to !="") AND ($agent_id == ''))
            {

                $agent =Agent::all();
                $track   = Track::select('tracks.*','bill_lading','organization_name')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('date', '<=', $to)
                    ->where('agents.user_id','=',$user_id)
                    ->orderByDesc("tracks.created_at")
                    ->get();
                return view('order.my_oder', compact('track','agent','profile'));

            }
            elseif(($current_status_one =="") AND ($from  =="") AND ($to =="") AND ($agent_id != ''))
            {

                $agent =Agent::all();
                $track   = Track::select('tracks.*','bill_lading','organization_name')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('tracks.id','=',$agent_id)
                    ->where('agents.user_id','=',$user_id)
                    ->orderByDesc("tracks.created_at")
                    ->get();
                return view('order.my_oder', compact('track','agent','profile'));

            }
            elseif(($current_status_one !="") AND ($from  =="") AND ($to !="") AND ($agent_id != ''))
            {

                $agent =Agent::all();
                $track   = Track::select('tracks.*','bill_lading','organization_name')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('current_status','LIKE',$current_status_one)
                    ->where('tracks.id','=',$agent_id)
                    ->where('date', '<=', $to)
                    ->where('agents.user_id','=',$user_id)
                    ->orderByDesc("tracks.created_at")
                    ->get();
                return view('order.my_oder', compact('track','agent'));

            }
            elseif(($current_status_one !="") AND ($from  !="") AND ($to =="") AND ($agent_id != ''))
            {

                $agent =Agent::all();
                $track   = Track::select('tracks.*','bill_lading','organization_name')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('current_status','LIKE',$current_status_one)
                    ->where('tracks.id','=',$agent_id)
                    ->where('tracks.document_date_received','>=', $from)
                    ->where('agents.user_id','=',$user_id)
                    ->orderByDesc("tracks.created_at")
                    ->get();
                return view('order.my_oder', compact('track','agent','profile'));

            }
            elseif(($current_status_one !="") AND ($from  =="") AND ($to !="") AND ($agent_id == ''))
            {

                $agent =Agent::all();
                $track   = Track::select('tracks.*','bill_lading','organization_name')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('current_status','LIKE',$current_status_one)
                    ->where('date', '<=', $to)
                    ->where('agents.user_id','=',$user_id)
                    ->orderByDesc("tracks.created_at")
                    ->get();
                return view('order.my_oder', compact('track','agent','profile'));

            }
            elseif(($current_status_one =="") AND ($from  !="") AND ($to =="") AND ($agent_id != ''))
            {

                $agent =Agent::all();
                $track   = Track::select('tracks.*','bill_lading','organization_name')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('tracks.id','=',$agent_id)
                    ->where('tracks.document_date_received','>=', $from)
                    ->where('agents.user_id','=',$user_id)
                    ->orderByDesc("tracks.created_at")
                    ->get();
                return view('order.my_oder', compact('track','agent','profile'));

            }
            elseif(($current_status_one !="") AND ($from  =="") AND ($to =="") AND ($agent_id != ''))
            {

                $agent =Agent::all();
                $track   = Track::select('tracks.*','bill_lading','organization_name')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('current_status','LIKE',$current_status_one)
                    ->where('tracks.id','=',$agent_id)
                    ->where('agents.user_id','=',$user_id)
                    ->orderByDesc("tracks.created_at")
                    ->get();
                return view('order.my_oder', compact('track','agent','profile'));

            }
            else
            {
                $agent =Agent::all();
                $track   = Track::select('tracks.*','bill_lading','organization_name')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->orderByDesc("tracks.created_at")
                    ->whereBetween(DB::raw('YEAR(tracks.created_at)'),[$from, $to])
                    ->where('agents.user_id','=',$user_id)
                    ->get();
                return view('order.my_oder', compact('track','agent','profile'));
            }





            /**
             *
             * If you select To the last date
             *
             **/

        }
        else
        {
            return JsonResponse::create(['error' => 'access-denied'],401);
        }
    }

    /**
     * @param $id
     * @return string
     *
     */

    public function FindDestination($id)
    {


        $service = Destination::where('country_id',$id)->pluck('destination_name','id');


        return json_encode($service);

    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     *
     * return adding order form
     *admin_add_status
     */
    public function add_orders()
    {
        $from = '2021';
        $to = '2030';
        $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
        $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
        $destination = Destination::all();
        $country = DB::table("countries")->pluck("country_name","id");
        $user   =Auth::user();
        if($user->hasRole('admin'))
        {

            $agents = Agent::all();
            $declaration_type = DeclarationType::all();
            return view('admin.admin_add_order',compact('country','normal','expenses','agents','declaration_type','destination'));
        }
        elseif($user->hasRole('super'))
        {
            $expenses = Expenses::with('container')->where('finance_status','LIKE','pending')->count();

            $normal = NormalExpenses::where('finance_status','LIKE','pending')->count();

            $agents = Agent::all();
            $declaration_type = DeclarationType::all();
            return view('admin.super_add_order',compact('country','normal','expenses','agents','declaration_type','destination'));
        }
        elseif($user->hasRole('sales'))
        {
            $agents = Agent::all();
            $declaration_type = DeclarationType::all();
            return view('staff.staff_add_order',compact('country','normal','expenses','agents','declaration_type','destination'));
        }
        elseif ($user->hasRole('agent'))
        {
            $user_id = Auth::id();
            $profile = User::where('id','=',$user_id)->get();
            $agents = Agent::where('user_id','=',Auth::id())->get();
            $declaration_type = DeclarationType::all();
            $destination    = Destination::all();
            return view('order.add_order',compact('country','declaration_type','agents','profile','destination'));

        }
        elseif ($user->hasRole('documentation'))
        {
            $agents = Agent::all();
            $declaration_type = DeclarationType::all();
            return view('documentation.add_order',compact('country','normal','expenses','declaration_type','agents','destination'));

        }
        elseif ($user->hasRole('finance'))
        {
            $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
            $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

            $agents = Agent::all();
            $declaration_type = DeclarationType::all();
            return view('finance.add_order',compact('country','normal','expenses','declaration_type','agents','destination'));

        }
        elseif ($user->hasRole('declaration'))
        {
            $agents = Agent::all();
            $declaration_type = DeclarationType::all();
            return view('declaration.add_order',compact('country','normal','expenses','declaration_type','agents','destination'));

        }
        elseif ($user->hasRole('shipping'))
        {
            $agents = Agent::all();
            $declaration_type = DeclarationType::all();
            return view('shipping.add_order',compact('country','normal','expenses','declaration_type','agents','destination'));

        }
        else
        {
            return JsonResponse::create(['error' => 'access-denied'],401);
        }
    }


    /**
     *
     */
    public function create()
    {
        //
    }

    /**
     * @param Request $request
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * @param Request $request
     * @return JsonResponse
     *
     * Admin Add order in the database
     */
    public function admin_add_order(Request $request)
    {
        $shipping = $request->get('shipping_line');
        $shipper_other = $request->get('shipper_other');
        if($shipping != 'Other')
        {
            $user   =Auth::user();
            if($user->hasRole('admin') || $user->hasRole('sales') || $user->hasRole('super')  ||  $user->hasRole('documentation') ||  $user->hasRole('shipping') || ($user->hasRole('declaration')) || ($user->hasRole('finance')))
            {
                $user_id        = Auth::id();
                $request->merge(['user_id' => $user_id ,'file_no' => 'MIK-'.rand(0,999999).'/21']);
                $bill = $request->get('bill_lading');
                $user = Ladding::where('bill_lading', '=',$bill)->first();
                $declaration_id = $request->get('declaration_id');

                if ($user === null)
                {
                    $track =  Track::create($request->all());

                    $contract_file = $request->file('delivery_order');
                    $port_charge_invoice = $request->file('port_charge_invoice');
                    $bl_doc = $request->file('bl_doc');
                    $package_list_doc   = $request->file('package_list_doc');
                    $invoice_order  =$request->file('invoice_order');


                    if (isset($bl_doc)) {
                        if (isset($bl_doc)) {
                            $extension1 = $request->file('bl_doc')->getClientOriginalExtension();
                        }
                        $sha = 'bl_doc' . md5(time());
                        if (isset($extension1)) {
                            $filename1 = date('Y') . $sha . "sgfs." . $extension1;
                        }
                        $destination_path = 'OrderDoc/';
                        if (empty($bl_doc)) {
                            File::makeDirectory($bl_doc, 0775, true, true);
                        }
                        if (isset($filename1)) {
                            $track->bl_doc = $filename1;
                            $request->file('bl_doc')->move($destination_path, $filename1);
                            $track->save();
                        }
                    }
                    if (isset($invoice_order)) {
                        if (isset($invoice_order)) {
                            $extension1 = $request->file('invoice_order')->getClientOriginalExtension();
                        }
                        $sha = 'invoice_order' . md5(time());
                        if (isset($extension1)) {
                            $filename1 = date('Y') . $sha . "sgfs." . $extension1;
                        }
                        $destination_path = 'OrderDoc/';
                        if (empty($invoice_order)) {
                            File::makeDirectory($invoice_order, 0775, true, true);
                        }
                        if (isset($filename1)) {
                            $track->invoice_order = $filename1;
                            $request->file('invoice_order')->move($destination_path, $filename1);
                            $track->save();
                        }
                    }
                    if (isset($package_list_doc)) {
                        if (isset($package_list_doc)) {
                            $extension1 = $request->file('package_list_doc')->getClientOriginalExtension();
                        }
                        $sha = 'package_list_doc' . md5(time());
                        if (isset($extension1)) {
                            $filename1 = date('Y') . $sha . "sgfs." . $extension1;
                        }
                        $destination_path = 'OrderDoc/';
                        if (empty($package_list_doc)) {
                            File::makeDirectory($package_list_doc, 0775, true, true);
                        }
                        if (isset($filename1)) {
                            $track->package_list_doc = $filename1;
                            $request->file('package_list_doc')->move($destination_path, $filename1);
                            $track->save();
                        }
                    }
                    if (isset($contract_file)) {
                        if (isset($contract_file)) {
                            $extension1 = $request->file('delivery_order')->getClientOriginalExtension();
                        }
                        $sha = 'delivery_order' . md5(time());
                        if (isset($extension1)) {
                            $filename1 = date('Y') . $sha . "sgfs." . $extension1;
                        }
                        $destination_path = 'DeliveryOrder/';
                        if (empty($contract_file)) {
                            File::makeDirectory($contract_file, 0775, true, true);
                            // File::makeDirectory($destination_path, $mode = 0777, true, true);
                        }
                        if (isset($filename1)) {
                            $track->delivery_order = $filename1;
                            $request->file('delivery_order')->move($destination_path, $filename1);
                            $track->save();
                        }
                    }


                    if (isset($port_charge_invoice)) {
                        if (isset($port_charge_invoice)) {
                            $extension1 = $request->file('port_charge_invoice')->getClientOriginalExtension();
                        }
                        $sha = 'port_charge_invoice' . md5(time());
                        if (isset($extension1)) {
                            $filename2 = date('Y') . $sha . "sgfs." . $extension1;
                        }
                        $destination_path = 'PortCharges/';
                        if (empty($port_charge_invoice)) {
                            File::makeDirectory($port_charge_invoice, 0775, true, true);
                        }
                        if (isset($filename2)) {
                            $track->port_charge_invoice = $filename2;
                            $request->file('port_charge_invoice')->move($destination_path, $filename2);
                            $track->save();
                        }
                    }
                    $all_data = $request->all();
                    $all_data['delivery_order']         = !empty($filename1) ? $filename1 : $track->delivery_order;
                    $all_data['port_charge_invoice']    = !empty($filename2) ? $filename2 : $track->port_charge_invoice;
                    $all_data['bl_doc']                 = !empty($filename1) ? $filename1 : $track->bl_doc;
                    $all_data['package_list_doc']       = !empty($filename1) ? $filename1 : $track->package_list_doc;
                    $all_data['invoice_order']          = !empty($filename1) ? $filename1 : $track->invoice_order;
                    $lading                = new Ladding;
                    $lading->bill_lading   =$request->get('bill_lading');
                    $lading->track_id      =$track->id;
                    $lading->user_id       =$user_id;

                    if ($lading->save())
                        $lad_id = $lading->id;
                    {

                        $request->merge(['user_id' => $user_id ,'declaration_id' => $declaration_id , 'ladding_id' =>$lad_id]);
                        $declaration_data =  BlDeclaration::create($request->all());
                        $declaration_data->save();
                    }

                }
                else
                {
                    return JsonResponse::create(['error' => 'bill of lading exist this order can not be created '],401);
                }

                if($lading->save())
                {


                    $user_id        = Auth::id();
                    $count = count($request->input('cont_no'));
                    $lading_id  = $lading->id;
                    for ($i=0; $i<$count; $i++)
                    {
                        $data[] = array('cont_no' => $request->input('cont_no')[$i],
                            'user_id'       => $user_id,
                            'ladding_id'    => $lading_id,
                            'created_at'    =>Carbon::now());
                    }

                    DB::table('containers')->insert($data);
                    $request->merge(['user_id' => $user_id ,'declaration_id' => $declaration_id , 'ladding_id' =>$lading_id]);
                    Session::flash('message', 'Order created successfully');
                    return redirect('/all_order');
                }

                else
                {
                    return  JsonResponse::create(['error' => 'access-denied'],401);
                }
            }
            elseif ($user->hasRole('agent'))
            {
                $user_id        = Auth::id();
                $get_agent      = Agent::whereuser_id($user_id)->first()->id;
                $request->merge(['user_id' => $user_id ,'agent_id' =>$get_agent,'file_no' => 'MIK-'.rand(0,999999).'/20']);
                $bill = $request->get('bill_lading');
                $user = Ladding::where('bill_lading', '=',$bill)->first();
                $declaration_id = $request->get('declaration_id');

                if ($user === null)
                {
                    $track =  Track::create($request->all());

                    $port_charge_invoice = $request->file('port_charge_invoice');
                    $bl_doc = $request->file('bl_doc');
                    $package_list_doc   = $request->file('package_list_doc');
                    $invoice_order  =$request->file('invoice_order');


                    if (isset($bl_doc)) {
                        if (isset($bl_doc)) {
                            $extension1 = $request->file('bl_doc')->getClientOriginalExtension();
                        }
                        $sha = 'bl_doc' . md5(time());
                        if (isset($extension1)) {
                            $filename1 = date('Y') . $sha . "sgfs." . $extension1;
                        }
                        $destination_path = 'OrderDoc/';
                        if (empty($bl_doc)) {
                            File::makeDirectory($bl_doc, 0775, true, true);
                        }
                        if (isset($filename1)) {
                            $track->bl_doc = $filename1;
                            $request->file('bl_doc')->move($destination_path, $filename1);
                            $track->save();
                        }
                    }
                    if (isset($invoice_order)) {
                        if (isset($invoice_order)) {
                            $extension1 = $request->file('invoice_order')->getClientOriginalExtension();
                        }
                        $sha = 'invoice_order' . md5(time());
                        if (isset($extension1)) {
                            $filename1 = date('Y') . $sha . "sgfs." . $extension1;
                        }
                        $destination_path = 'OrderDoc/';
                        if (empty($invoice_order)) {
                            File::makeDirectory($invoice_order, 0775, true, true);
                        }
                        if (isset($filename1)) {
                            $track->invoice_order = $filename1;
                            $request->file('invoice_order')->move($destination_path, $filename1);
                            $track->save();
                        }
                    }
                    if (isset($package_list_doc)) {
                        if (isset($package_list_doc)) {
                            $extension1 = $request->file('package_list_doc')->getClientOriginalExtension();
                        }
                        $sha = 'package_list_doc' . md5(time());
                        if (isset($extension1)) {
                            $filename1 = date('Y') . $sha . "sgfs." . $extension1;
                        }
                        $destination_path = 'OrderDoc/';
                        if (empty($package_list_doc)) {
                            File::makeDirectory($package_list_doc, 0775, true, true);
                        }
                        if (isset($filename1)) {
                            $track->package_list_doc = $filename1;
                            $request->file('package_list_doc')->move($destination_path, $filename1);
                            $track->save();
                        }
                    }



                    $all_data = $request->all();
                    $all_data['bl_doc']                 = !empty($filename1) ? $filename1 : $track->bl_doc;
                    $all_data['package_list_doc']       = !empty($filename1) ? $filename1 : $track->package_list_doc;
                    $all_data['invoice_order']          = !empty($filename1) ? $filename1 : $track->invoice_order;
                    $lading                = new Ladding;
                    $lading->bill_lading   =$request->get('bill_lading');
                    $lading->track_id      =$track->id;
                    $lading->user_id       =$user_id;

                    if ($lading->save())
                        $lad_id = $lading->id;
                    {

                        $request->merge(['user_id' => $user_id ,'declaration_id' => $declaration_id , 'ladding_id' =>$lad_id]);
                        $declaration_data =  BlDeclaration::create($request->all());
                        $declaration_data->save();
                    }

                }
                else
                {
                    return JsonResponse::create(['error' => 'bill of lading exist this order can not be created '],401);
                }

                if($lading->save())
                {


                    $user_id        = Auth::id();
                    $count = count($request->input('cont_no'));
                    $lading_id  = $lading->id;
                    for ($i=0; $i<$count; $i++)
                    {
                        $data[] = array('cont_no' => $request->input('cont_no')[$i],
                            'user_id'       => $user_id,
                            'ladding_id'    => $lading_id);
                    }

                    DB::table('containers')->insert($data);
                    $request->merge(['user_id' => $user_id ,'declaration_id' => $declaration_id , 'ladding_id' =>$lading_id]);
                    Session::flash('message', 'Order created successfully');
                    return redirect('/my_orders');
                }

                else
                {
                    return  JsonResponse::create(['error' => 'access-denied'],401);
                }
//            $invoice_order_file      = $request->file('invoice_order');
//
//
//                $bill = $request->get('bill_lading');
//                $user = Ladding::where('bill_lading', '=',$bill)->first();
//                if ($user === null)
//                {
//                    $track =  Track::create($request->all());
//                    $lading                = new Ladding;
//                    $lading->bill_lading   =$request->get('bill_lading');
//                    $lading->track_id      =$track->id;
//                    $lading->user_id       =$user_id;
//
//                    if(isset($invoice_order_file)) {
//                        if (isset($invoice_order_file)) {
//                            $extension = $request->file('invoice_order')->getClientOriginalExtension();
//                        }
//                        $sha = 'rra' . md5(time().date("y"));
//                        if(isset($extension)){
//                            $filename = date('Y').$sha . "fghjm." . $extension;
//                        }
//                        $shipping_line_path = 'tax_clearance/';
//                        if (empty($invoice_order_file)) {
//                            File::makeDirectory($invoice_order_file, 0775, true, true);
//                        }
//                        if(isset($filename)){
//                            $track->invoice_order = $filename;
//                            $request->file('invoice_order')->move($shipping_line_path, $filename);
//                            $track->save();
//                            $all_data =  $request->all();
//
//                            $all_data['invoice_order']      = !empty($filename1) ? $filename1                : $track->invoice_order;
//                            $track->update($all_data);
//                        }
//                    }
//                }
//                else
//                {
//                    return JsonResponse::create(['error' => 'bill of lading exist this order can not be created'],401);
//                }
//
//                if ($lading->save())
//                {
//                    $cont_no  = '';
//                    $request->merge(['user_id' => $user_id , 'ladding_id' => $lading->id, 'cont_no' =>$cont_no]);
//                    $container =  Container::create($request->all());
//                    $container->save();
//                }
//                Session::flash('message', 'Order created successfully');
//                return redirect('/my_orders');

            }
            else
            {
                return JsonResponse::create(['error' => 'access-denied'],401);
            }
        }
        else
        {
            $user   =Auth::user();
            if($user->hasRole('admin') || $user->hasRole('sales') || $user->hasRole('super')  ||  $user->hasRole('documentation') ||  $user->hasRole('shipping') || ($user->hasRole('declaration')) || ($user->hasRole('finance')))
            {
                $user_id        = Auth::id();
                $request->merge(['user_id' => $user_id ,'shipping_line'=>$shipper_other ,'file_no' => 'MIK-'.rand(0,999999).'/21']);
                $bill = $request->get('bill_lading');
                $user = Ladding::where('bill_lading', '=',$bill)->first();
                $declaration_id = $request->get('declaration_id');

                if ($user === null)
                {
                    $track =  Track::create($request->all());

                    $contract_file = $request->file('delivery_order');
                    $port_charge_invoice = $request->file('port_charge_invoice');
                    $bl_doc = $request->file('bl_doc');
                    $package_list_doc   = $request->file('package_list_doc');
                    $invoice_order  =$request->file('invoice_order');


                    if (isset($bl_doc)) {
                        if (isset($bl_doc)) {
                            $extension1 = $request->file('bl_doc')->getClientOriginalExtension();
                        }
                        $sha = 'bl_doc' . md5(time());
                        if (isset($extension1)) {
                            $filename1 = date('Y') . $sha . "sgfs." . $extension1;
                        }
                        $destination_path = 'OrderDoc/';
                        if (empty($bl_doc)) {
                            File::makeDirectory($bl_doc, 0775, true, true);
                        }
                        if (isset($filename1)) {
                            $track->bl_doc = $filename1;
                            $request->file('bl_doc')->move($destination_path, $filename1);
                            $track->save();
                        }
                    }
                    if (isset($invoice_order)) {
                        if (isset($invoice_order)) {
                            $extension1 = $request->file('invoice_order')->getClientOriginalExtension();
                        }
                        $sha = 'invoice_order' . md5(time());
                        if (isset($extension1)) {
                            $filename1 = date('Y') . $sha . "sgfs." . $extension1;
                        }
                        $destination_path = 'OrderDoc/';
                        if (empty($invoice_order)) {
                            File::makeDirectory($invoice_order, 0775, true, true);
                        }
                        if (isset($filename1)) {
                            $track->invoice_order = $filename1;
                            $request->file('invoice_order')->move($destination_path, $filename1);
                            $track->save();
                        }
                    }
                    if (isset($package_list_doc)) {
                        if (isset($package_list_doc)) {
                            $extension1 = $request->file('package_list_doc')->getClientOriginalExtension();
                        }
                        $sha = 'package_list_doc' . md5(time());
                        if (isset($extension1)) {
                            $filename1 = date('Y') . $sha . "sgfs." . $extension1;
                        }
                        $destination_path = 'OrderDoc/';
                        if (empty($package_list_doc)) {
                            File::makeDirectory($package_list_doc, 0775, true, true);
                        }
                        if (isset($filename1)) {
                            $track->package_list_doc = $filename1;
                            $request->file('package_list_doc')->move($destination_path, $filename1);
                            $track->save();
                        }
                    }
                    if (isset($contract_file)) {
                        if (isset($contract_file)) {
                            $extension1 = $request->file('delivery_order')->getClientOriginalExtension();
                        }
                        $sha = 'delivery_order' . md5(time());
                        if (isset($extension1)) {
                            $filename1 = date('Y') . $sha . "sgfs." . $extension1;
                        }
                        $destination_path = 'DeliveryOrder/';
                        if (empty($contract_file)) {
                            File::makeDirectory($contract_file, 0775, true, true);
                            // File::makeDirectory($destination_path, $mode = 0777, true, true);
                        }
                        if (isset($filename1)) {
                            $track->delivery_order = $filename1;
                            $request->file('delivery_order')->move($destination_path, $filename1);
                            $track->save();
                        }
                    }


                    if (isset($port_charge_invoice)) {
                        if (isset($port_charge_invoice)) {
                            $extension1 = $request->file('port_charge_invoice')->getClientOriginalExtension();
                        }
                        $sha = 'port_charge_invoice' . md5(time());
                        if (isset($extension1)) {
                            $filename2 = date('Y') . $sha . "sgfs." . $extension1;
                        }
                        $destination_path = 'PortCharges/';
                        if (empty($port_charge_invoice)) {
                            File::makeDirectory($port_charge_invoice, 0775, true, true);
                        }
                        if (isset($filename2)) {
                            $track->port_charge_invoice = $filename2;
                            $request->file('port_charge_invoice')->move($destination_path, $filename2);
                            $track->save();
                        }
                    }
                    $all_data = $request->all();
                    $all_data['delivery_order']         = !empty($filename1) ? $filename1 : $track->delivery_order;
                    $all_data['port_charge_invoice']    = !empty($filename2) ? $filename2 : $track->port_charge_invoice;
                    $all_data['bl_doc']                 = !empty($filename1) ? $filename1 : $track->bl_doc;
                    $all_data['package_list_doc']       = !empty($filename1) ? $filename1 : $track->package_list_doc;
                    $all_data['invoice_order']          = !empty($filename1) ? $filename1 : $track->invoice_order;
                    $lading                = new Ladding;
                    $lading->bill_lading   =$request->get('bill_lading');
                    $lading->track_id      =$track->id;
                    $lading->user_id       =$user_id;

                    if ($lading->save())
                        $lad_id = $lading->id;
                    {

                        $request->merge(['user_id' => $user_id ,'declaration_id' => $declaration_id , 'ladding_id' =>$lad_id]);
                        $declaration_data =  BlDeclaration::create($request->all());
                        $declaration_data->save();
                    }

                }
                else
                {
                    return JsonResponse::create(['error' => 'bill of lading exist this order can not be created '],401);
                }

                if($lading->save())
                {


                    $user_id        = Auth::id();
                    $count = count($request->input('cont_no'));
                    $lading_id  = $lading->id;
                    for ($i=0; $i<$count; $i++)
                    {
                        $data[] = array('cont_no' => $request->input('cont_no')[$i],
                            'user_id'       => $user_id,
                            'ladding_id'    => $lading_id,
                            'created_at'    =>Carbon::now());
                    }

                    DB::table('containers')->insert($data);
                    $request->merge(['user_id' => $user_id ,'declaration_id' => $declaration_id , 'ladding_id' =>$lading_id]);
                    Session::flash('message', 'Order created successfully');
                    return redirect('/all_order');
                }

                else
                {
                    return  JsonResponse::create(['error' => 'access-denied'],401);
                }
            }
            elseif ($user->hasRole('agent'))
            {
                $user_id        = Auth::id();
                $get_agent      = Agent::whereuser_id($user_id)->first()->id;
                $request->merge(['user_id' => $user_id ,'agent_id' =>$get_agent,'file_no' => 'MIK-'.rand(0,999999).'/20']);
                $bill = $request->get('bill_lading');
                $user = Ladding::where('bill_lading', '=',$bill)->first();
                $declaration_id = $request->get('declaration_id');

                if ($user === null)
                {
                    $track =  Track::create($request->all());

                    $port_charge_invoice = $request->file('port_charge_invoice');
                    $bl_doc = $request->file('bl_doc');
                    $package_list_doc   = $request->file('package_list_doc');
                    $invoice_order  =$request->file('invoice_order');


                    if (isset($bl_doc)) {
                        if (isset($bl_doc)) {
                            $extension1 = $request->file('bl_doc')->getClientOriginalExtension();
                        }
                        $sha = 'bl_doc' . md5(time());
                        if (isset($extension1)) {
                            $filename1 = date('Y') . $sha . "sgfs." . $extension1;
                        }
                        $destination_path = 'OrderDoc/';
                        if (empty($bl_doc)) {
                            File::makeDirectory($bl_doc, 0775, true, true);
                        }
                        if (isset($filename1)) {
                            $track->bl_doc = $filename1;
                            $request->file('bl_doc')->move($destination_path, $filename1);
                            $track->save();
                        }
                    }
                    if (isset($invoice_order)) {
                        if (isset($invoice_order)) {
                            $extension1 = $request->file('invoice_order')->getClientOriginalExtension();
                        }
                        $sha = 'invoice_order' . md5(time());
                        if (isset($extension1)) {
                            $filename1 = date('Y') . $sha . "sgfs." . $extension1;
                        }
                        $destination_path = 'OrderDoc/';
                        if (empty($invoice_order)) {
                            File::makeDirectory($invoice_order, 0775, true, true);
                        }
                        if (isset($filename1)) {
                            $track->invoice_order = $filename1;
                            $request->file('invoice_order')->move($destination_path, $filename1);
                            $track->save();
                        }
                    }
                    if (isset($package_list_doc)) {
                        if (isset($package_list_doc)) {
                            $extension1 = $request->file('package_list_doc')->getClientOriginalExtension();
                        }
                        $sha = 'package_list_doc' . md5(time());
                        if (isset($extension1)) {
                            $filename1 = date('Y') . $sha . "sgfs." . $extension1;
                        }
                        $destination_path = 'OrderDoc/';
                        if (empty($package_list_doc)) {
                            File::makeDirectory($package_list_doc, 0775, true, true);
                        }
                        if (isset($filename1)) {
                            $track->package_list_doc = $filename1;
                            $request->file('package_list_doc')->move($destination_path, $filename1);
                            $track->save();
                        }
                    }



                    $all_data = $request->all();
                    $all_data['bl_doc']                 = !empty($filename1) ? $filename1 : $track->bl_doc;
                    $all_data['package_list_doc']       = !empty($filename1) ? $filename1 : $track->package_list_doc;
                    $all_data['invoice_order']          = !empty($filename1) ? $filename1 : $track->invoice_order;
                    $lading                = new Ladding;
                    $lading->bill_lading   =$request->get('bill_lading');
                    $lading->track_id      =$track->id;
                    $lading->user_id       =$user_id;

                    if ($lading->save())
                        $lad_id = $lading->id;
                    {

                        $request->merge(['user_id' => $user_id ,'declaration_id' => $declaration_id , 'ladding_id' =>$lad_id]);
                        $declaration_data =  BlDeclaration::create($request->all());
                        $declaration_data->save();
                    }

                }
                else
                {
                    return JsonResponse::create(['error' => 'bill of lading exist this order can not be created '],401);
                }

                if($lading->save())
                {


                    $user_id        = Auth::id();
                    $count = count($request->input('cont_no'));
                    $lading_id  = $lading->id;
                    for ($i=0; $i<$count; $i++)
                    {
                        $data[] = array('cont_no' => $request->input('cont_no')[$i],
                            'user_id'       => $user_id,
                            'ladding_id'    => $lading_id);
                    }

                    DB::table('containers')->insert($data);
                    $request->merge(['user_id' => $user_id ,'declaration_id' => $declaration_id , 'ladding_id' =>$lading_id]);
                    Session::flash('message', 'Order created successfully');
                    return redirect('/my_orders');
                }

                else
                {
                    return  JsonResponse::create(['error' => 'access-denied'],401);
                }
//            $invoice_order_file      = $request->file('invoice_order');
//
//
//                $bill = $request->get('bill_lading');
//                $user = Ladding::where('bill_lading', '=',$bill)->first();
//                if ($user === null)
//                {
//                    $track =  Track::create($request->all());
//                    $lading                = new Ladding;
//                    $lading->bill_lading   =$request->get('bill_lading');
//                    $lading->track_id      =$track->id;
//                    $lading->user_id       =$user_id;
//
//                    if(isset($invoice_order_file)) {
//                        if (isset($invoice_order_file)) {
//                            $extension = $request->file('invoice_order')->getClientOriginalExtension();
//                        }
//                        $sha = 'rra' . md5(time().date("y"));
//                        if(isset($extension)){
//                            $filename = date('Y').$sha . "fghjm." . $extension;
//                        }
//                        $shipping_line_path = 'tax_clearance/';
//                        if (empty($invoice_order_file)) {
//                            File::makeDirectory($invoice_order_file, 0775, true, true);
//                        }
//                        if(isset($filename)){
//                            $track->invoice_order = $filename;
//                            $request->file('invoice_order')->move($shipping_line_path, $filename);
//                            $track->save();
//                            $all_data =  $request->all();
//
//                            $all_data['invoice_order']      = !empty($filename1) ? $filename1                : $track->invoice_order;
//                            $track->update($all_data);
//                        }
//                    }
//                }
//                else
//                {
//                    return JsonResponse::create(['error' => 'bill of lading exist this order can not be created'],401);
//                }
//
//                if ($lading->save())
//                {
//                    $cont_no  = '';
//                    $request->merge(['user_id' => $user_id , 'ladding_id' => $lading->id, 'cont_no' =>$cont_no]);
//                    $container =  Container::create($request->all());
//                    $container->save();
//                }
//                Session::flash('message', 'Order created successfully');
//                return redirect('/my_orders');

            }
            else
            {
                return JsonResponse::create(['error' => 'access-denied'],401);
            }

        }



    }
    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * Change status of order
     */

    public function add_status(Request $request)
    {
        $user_id        = Auth::id();
        $request->merge(['user_id' => $user_id ]);
        $order =  Trucking::create($request->all());
        $order->save();
        Session::flash('delete', 'Order created successfully');
        return back();

    }

    /**
     * @param $id
     */
    public function show($id)
    {
        //
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     *
     */

    public function trackingAPi()
    {
        $from = '2021';
        $to = '2030';

        $user = JWTAuth::parseToken()->authenticate();


        if($user->hasRole('admin'))
        {


            $ladding = DB::table('laddings')
                ->select('laddings.id','laddings.bill_lading')
                ->join('tracks','tracks.id','=','laddings.track_id')
                ->whereBetween(DB::raw('YEAR(laddings.created_at)'),[$from, $to])
                ->latest('laddings.created_at')
                ->get();

            $container = DB::table('containers')
                ->select('id','ladding_id','cont_no')
                ->whereBetween(DB::raw('YEAR(containers.created_at)'),[$from, $to])
                ->orderByDesc("containers.created_at")
                ->get();


            return response()->json(['bl'=>$ladding,'container'=>$container], 200);


        }
        else if($user->hasRole('documentation'))
        {


            $ladding = DB::table('laddings')
                ->select('laddings.id','laddings.bill_lading')
                ->join('tracks','tracks.id','=','laddings.track_id')
                ->whereBetween(DB::raw('YEAR(laddings.created_at)'),[$from, $to])
                ->latest('laddings.created_at')
                ->get();

            $container = DB::table('containers')
                ->select('id','ladding_id','cont_no')
                ->whereBetween(DB::raw('YEAR(containers.created_at)'),[$from, $to])
                ->orderByDesc("containers.created_at")
                ->get();


            return response()->json(['bl'=>$ladding,'container'=>$container], 200);

        }
        else if($user->hasRole('shipping'))
        {

            $ladding = DB::table('laddings')
                ->select('laddings.id','laddings.bill_lading')
                ->join('tracks','tracks.id','=','laddings.track_id')
                ->whereBetween(DB::raw('YEAR(laddings.created_at)'),[$from, $to])
                ->latest('laddings.created_at')
                ->get();

            $container = DB::table('containers')
                ->select('id','ladding_id','cont_no')
                ->whereBetween(DB::raw('YEAR(containers.created_at)'),[$from, $to])
                ->orderByDesc("containers.created_at")
                ->get();


            return response()->json(['bl'=>$ladding,'container'=>$container], 200);

        }
        else if($user->hasRole('declaration'))
        {

            $ladding = DB::table('laddings')
                ->select('laddings.id','laddings.bill_lading')
                ->join('tracks','tracks.id','=','laddings.track_id')
                ->whereBetween(DB::raw('YEAR(laddings.created_at)'),[$from, $to])
                ->latest('laddings.created_at')
                ->get();
            $container = DB::table('containers')
                ->select('id','ladding_id','cont_no')
                ->whereBetween(DB::raw('YEAR(containers.created_at)'),[$from, $to])
                ->orderByDesc("containers.created_at")
                ->get();



            return response()->json(['bl'=>$ladding,'container'=>$container], 200);

        }
        elseif($user->hasRole('super'))
        {

            $ladding = DB::table('laddings')
                ->select('laddings.id','laddings.bill_lading')
                ->join('tracks','tracks.id','=','laddings.track_id')
                ->whereBetween(DB::raw('YEAR(laddings.created_at)'),[$from, $to])
                ->latest('laddings.created_at')
                ->get();
            $container = DB::table('containers')
                ->select('id','ladding_id','cont_no')
                ->orderByDesc("containers.created_at")
                ->get();


            return response()->json(['bl'=>$ladding,'container'=>$container], 200);

        }
        elseif($user->hasRole('sales'))
        {

            $ladding = DB::table('laddings')
                ->select('laddings.id','laddings.bill_lading')
                ->join('tracks','tracks.id','=','laddings.track_id')
                ->whereBetween(DB::raw('YEAR(laddings.created_at)'),[$from, $to])
                ->latest('laddings.created_at')
                ->get();
            $container = DB::table('containers')
                ->select('id','ladding_id','cont_no')
                ->whereBetween(DB::raw('YEAR(containers.created_at)'),[$from, $to])
                ->orderByDesc("containers.created_at")
                ->get();


            return response()->json(['bl'=>$ladding,'container'=>$container], 200);

        }
        elseif($user->hasRole('finance'))
        {
            $ladding = DB::table('laddings')
                ->select('laddings.id','laddings.bill_lading')
                ->join('tracks','tracks.id','=','laddings.track_id')
                ->whereBetween(DB::raw('YEAR(laddings.created_at)'),[$from, $to])
                ->latest('laddings.created_at')
                ->get();
            $container = DB::table('containers')
                ->select('id','ladding_id','cont_no')
                ->whereBetween(DB::raw('YEAR(containers.created_at)'),[$from, $to])
                ->orderByDesc("containers.created_at")
                ->get();



            return response()->json(['bl'=>$ladding,'container'=>$container], 200);

        }
        else
        {
            return  JsonResponse::create(['error' => 'access-denied'],401);
        }

    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|static
     *
     */
    public  function  tracking()
    {
        $from = '2021';
        $to = '2030';
        $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
        $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

        $user =Auth::user();

        if($user->hasRole('admin'))
        {

            $container =Container::whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->get();
            $ladding = Ladding::select('laddings.*')
                ->join('tracks','tracks.id','=','laddings.track_id')
                ->latest('laddings.created_at')
                ->whereBetween(DB::raw('YEAR(laddings.created_at)'),[$from, $to])
                ->pluck("laddings.bill_lading","laddings.id");



            $track   = Track::select('tracks.*','bill_lading')
                ->join('laddings','laddings.track_id','=','tracks.id')
                ->orderBy("tracks.created_at")
                ->whereBetween(DB::raw('YEAR(tracks.created_at)'),[$from, $to])
                ->get();

            return view('admin.admin_truck',compact('expenses','normal','track','ladding','container'));
        }
        else if($user->hasRole('documentation'))
        {

            $container =Container::whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->get();
            $ladding = Ladding::select('laddings.*')
                ->join('tracks','tracks.id','=','laddings.track_id')
                ->latest('laddings.created_at')
                ->whereBetween(DB::raw('YEAR(laddings.created_at)'),[$from, $to])
                ->pluck("laddings.bill_lading","laddings.id");



            $track   = Track::select('tracks.*','bill_lading')
                ->join('laddings','laddings.track_id','=','tracks.id')
                ->orderBy("tracks.created_at")
                ->whereBetween(DB::raw('YEAR(tracks.created_at)'),[$from, $to])
                ->get();


            return view('documentation.documentation_truck',compact('expenses','normal','track','ladding','container'));
        }
        else if($user->hasRole('shipping'))
        {

            $container =Container::whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->get();
            $ladding = Ladding::select('laddings.*')
                ->join('tracks','tracks.id','=','laddings.track_id')
                ->latest('laddings.created_at')
                ->whereBetween(DB::raw('YEAR(laddings.created_at)'),[$from, $to])
                ->pluck("laddings.bill_lading","laddings.id");



            $track   = Track::select('tracks.*','bill_lading')
                ->join('laddings','laddings.track_id','=','tracks.id')
                ->orderBy("tracks.created_at")
                ->whereBetween(DB::raw('YEAR(tracks.created_at)'),[$from, $to])
                ->get();

            return view('shipping.shipping_truck',compact('expenses','normal','track','ladding','container'));
        }
        else if($user->hasRole('declaration'))
        {

            $container =Container::whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->get();
            $ladding = Ladding::select('laddings.*')
                ->join('tracks','tracks.id','=','laddings.track_id')
                ->latest('laddings.created_at')
                ->whereBetween(DB::raw('YEAR(laddings.created_at)'),[$from, $to])
                ->pluck("laddings.bill_lading","laddings.id");



            $track   = Track::select('tracks.*','bill_lading')
                ->join('laddings','laddings.track_id','=','tracks.id')
                ->orderBy("tracks.created_at")
                ->whereBetween(DB::raw('YEAR(tracks.created_at)'),[$from, $to])
                ->get();


            return view('declaration.declaration_truck',compact('expenses','normal','track','ladding','container'));
        }
        elseif($user->hasRole('super'))
        {
            $expenses = Expenses::with('container')->where('finance_status','LIKE','pending')->count();

            $normal = NormalExpenses::where('finance_status','LIKE','pending')->count();

            $container =Container::all();
            $ladding = Ladding::select('laddings.*')
                ->join('tracks','tracks.id','=','laddings.track_id')
                ->latest('laddings.created_at')
                ->pluck("laddings.bill_lading","laddings.id");

            $track   = Track::select('tracks.*','bill_lading')
                ->join('laddings','laddings.track_id','=','tracks.id')
                ->orderBy("tracks.created_at")
                ->get();

            return view('admin.super_admin_truck',compact('expenses','normal','track','ladding','container'));
        }
        elseif($user->hasRole('agent'))
        {

            $user_id =Auth::id();
            $profile = User::where('id','=',$user_id)->get();
            $container = Container::Select('containers.*')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->join('tracks','tracks.id','=','laddings.track_id')
                ->join('agents', 'agents.id', '=', 'tracks.agent_id')
                ->where('agents.user_id','=',$user_id)
                ->latest('laddings.created_at')
                ->get();

            $user_id =Auth::id();
            $ladding = Ladding::select('laddings.*')
                ->join('tracks','tracks.id','=','laddings.track_id')
                ->join('agents','agents.id','=','tracks.agent_id')
                ->where('agents.user_id','=',$user_id)
                ->latest('laddings.created_at')
                ->pluck("laddings.bill_lading","laddings.id");

            $track   = Track::select('tracks.*','bill_lading')
                ->join('laddings','laddings.track_id','=','tracks.id')
                ->join('agents','agents.id','=','tracks.agent_id')
                ->latest("tracks.created_at")
                ->get();

            return view('agent.agent_truck',compact('track','ladding','container','profile'));
        }
        elseif($user->hasRole('sales'))
        {
            $container =Container::whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->get();
            $ladding = Ladding::select('laddings.*')
                ->join('tracks','tracks.id','=','laddings.track_id')
                ->latest('laddings.created_at')
                ->whereBetween(DB::raw('YEAR(laddings.created_at)'),[$from, $to])
                ->pluck("laddings.bill_lading","laddings.id");



            $track   = Track::select('tracks.*','bill_lading')
                ->join('laddings','laddings.track_id','=','tracks.id')
                ->orderBy("tracks.created_at")
                ->whereBetween(DB::raw('YEAR(tracks.created_at)'),[$from, $to])
                ->get();

            return view('staff.sales_truck',compact('expenses','normal','track','ladding','container'));
        }
        elseif($user->hasRole('finance'))
        {
            $container =Container::whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->get();
            $ladding = Ladding::select('laddings.*')
                ->join('tracks','tracks.id','=','laddings.track_id')
                ->latest('laddings.created_at')
                ->whereBetween(DB::raw('YEAR(laddings.created_at)'),[$from, $to])
                ->pluck("laddings.bill_lading","laddings.id");



            $track   = Track::select('tracks.*','bill_lading')
                ->join('laddings','laddings.track_id','=','tracks.id')
                ->orderBy("tracks.created_at")
                ->whereBetween(DB::raw('YEAR(tracks.created_at)'),[$from, $to])
                ->get();


            return view('finance.finance_truck',compact('expenses','normal','track','ladding','container'));
        }
        else
        {
            return  JsonResponse::create(['error' => 'access-denied'],401);
        }
    }


    /**
     * @param $id
     * @return string
     *
     */
    public function trackingAjax($id)
    {
        $cities = DB::table("containers")
            ->where("ladding_id",$id)
            ->pluck("cont_no","id");
        return json_encode($cities);
    }
    /**
     * @param $id
     * @return string
     *
     */

    public function myformAjax($id)
    {
        $cities = DB::table("demo_cities")
            ->where("state_id",$id)
            ->pluck("name","id");

        $containers = Container::all()->where('ladding_id',$id)->pluck('cont_no','id');
        return json_encode($containers);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|static
     *
     * Order Details
     */
    public function  order_details(Request $request , $id)
    {
        $expenses = Expenses::with('container')->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
        $normal = NormalExpenses::where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

        $user   =Auth::user();
        if($user->hasRole('admin'))
        {
            $track   = Track::select('tracks.*','transporter_name','trucking_number','drop_name','driver_name','driver_contact','cont_no','bill_lading')
                ->join('laddings','laddings.track_id','=','tracks.id')
                ->join('containers','containers.ladding_id','=','containers.id')
                ->join('transporters','transporters.container_id','=','containers.id')
                ->join('drivers','drivers.container_id','=','containers.id')
                ->join('drops','drops.container_id','=','containers.id')
                ->where('tracks.id','=',$id)
                ->get();

            return view('admin.order_details',compact('normal','expenses','','track'));
        }
        elseif($user->hasRole('agent'))
        {
            $user_id =Auth::id();
            $track   = Track::select('tracks.*','transporter_name','trucking_number','drop_name','driver_name','driver_contact','cont_no','bill_lading')
                ->join('laddings','laddings.track_id','=','tracks.id')
                ->join('containers','containers.ladding_id','=','containers.id')
                ->join('transporters','transporters.container_id','=','containers.id')
                ->join('drivers','drivers.container_id','=','containers.id')
                ->join('drops','drops.container_id','=','containers.id')
                ->join('agents','agents.id','=','tracks.consignee_id')
                ->where('tracks.id','=',$id)
                ->where('agents.user_id','=',$user_id)
                ->get();

            return view('agent.agent_view_more',compact('track'));
        }
        else
        {
            return  JsonResponse::create(['error' => 'access-denied'],401);
        }

    }


    /**
     * @param $id
     * @return string
     *
     */
    public function admin_add_statusAjax($id)
    {


        $containers = Container::all()->where('ladding_id',$id)->pluck('cont_no','id');
        return json_encode($containers);

    }

    /**
     * @param $id
     * @return string
     *
     */
    public function findStatus($id)
    {
//        $tracks = Track::select('tracks.*')
//                    ->join('laddings','laddings.track_id','=','tracks.id')
//                    ->where('laddings.id',$id)
//                    ->pluck('tracks.current_status','tracks.id');
//
        $tracks = Ladding::select('laddings.*','current_status')
            ->join('tracks','tracks.id','=','laddings.track_id')
            ->where('laddings.id',$id)
            ->pluck('tracks.current_status','laddings.id');

        return json_encode($tracks);

    }

    /**
     * @param $id
     * @return string
     */
    public function admin_drop_statusAjax($id)
    {

        $containers = Container::all()->where('ladding_id',$id)->pluck('cont_no','id');

        return json_encode($containers);

    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|static
     *
     */
    public  function  admin_add_status()
    {

        $expenses = Expenses::with('container')->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
        $normal = NormalExpenses::where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

        $user   =Auth::user();
        if($user->hasRole('admin'))
        {
            $ladding = DB::table("laddings")
                ->join('tracks','tracks.id','=','laddings.track_id')
                ->latest('laddings.created_at')->pluck("bill_lading","laddings.id");


            return view('admin.admin_status',compact('expenses','normal','ladding'));
        }
        elseif($user->hasRole('super'))
        {
            $expenses = Expenses::with('container')->where('finance_status','LIKE','pending')->count();

            $normal = NormalExpenses::where('finance_status','LIKE','pending')->count();

            $ladding = DB::table("laddings")->latest('laddings.created_at')->pluck("bill_lading","id");

            return view('admin.super_admin_status',compact('expenses','normal','ladding'));
        }
        elseif($user->hasRole('documentation'))
        {
            $ladding = DB::table("laddings")->latest('laddings.created_at')->pluck("bill_lading","id");

            return view('documentation.documentation_status',compact('expenses','normal','ladding'));
        }
        elseif($user->hasRole('declaration'))
        {
            $ladding = DB::table("laddings")->latest('laddings.created_at')->pluck("bill_lading","id");

            return view('declaration.declaration_status',compact('expenses','normal','ladding'));
        }
        elseif($user->hasRole('shipping'))
        {
            $ladding = DB::table("laddings")->latest('laddings.created_at')->pluck("bill_lading","id");

            return view('shipping.shipping_status',compact('expenses','normal','ladding'));
        }
        elseif($user->hasRole('sales'))
        {
            $ladding = DB::table("laddings")->latest('laddings.created_at')->pluck("bill_lading","id");
            return view('staff.sales_admin_status',compact('expenses','normal','ladding'));
        }
        elseif($user->hasRole('finance'))
        {
            $expenses = Expenses::with('container')->where('finance_status','LIKE','pending')->count();

            $normal = NormalExpenses::where('finance_status','LIKE','pending')->count();

            $ladding = DB::table("laddings")->latest('laddings.created_at')->pluck("bill_lading","id");

            return view('finance.finance_status',compact('expenses','normal','ladding'));
        }
        elseif($user->hasRole('agent'))
        {

            $user_id     =Auth::id();
            $profile = User::where('id','=',$user_id)->get();
            $ladding = Ladding::select('laddings.*')
                ->join('tracks','tracks.id','=','laddings.track_id')
                ->join('agents','agents.id','=','tracks.agent_id')
                ->where('agents.user_id','=',$user_id)
                ->latest('laddings.created_at')
                ->pluck("bill_lading","id");
            return view('agent.agent_status',compact('ladding','profile'));
        }
        else
        {
            return  JsonResponse::create(['error' => 'access-denied'],401);
        }

    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     *  Admin edit order and agents
     */
    public  function  admin_edit_order(Request $request , $id)
    {
        $expenses = Expenses::with('container')->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
        $normal = NormalExpenses::where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

        $user   =Auth::user();
        if($user->hasRole('admin'))
        {
            $agent  =Agent::all();
            $tracks   = Track::select('tracks.*','bill_lading','organization_name')
                ->join('laddings','laddings.track_id','=','tracks.id')
                ->join('agents','agents.id','=','tracks.agent_id')
                ->where('tracks.id', '=', $id)
                ->get();
            $lad_id = Ladding::with('track')->where('track_id','=',$id)->first()->id;
            $declaration_type = BlDeclaration::with('lad','declaration')->where('ladding_id','=',$lad_id)->get();


            $container_loop  = Container::with('lad')
                ->where('ladding_id','=',$lad_id)
                ->get();

            $tracks_loop   = Track::select('tracks.*','bill_lading')
                ->join('laddings','laddings.track_id','=','tracks.id')
                ->where('tracks.id', '=', $id)
                ->get();
            $destination = Destination::all();
            $country    = Country::all();

            return view('admin.admin_edit_order', compact('country','destination','expenses','normal','tracks_loop','tracks','agent','container_loop','declaration_type'));
        }
        elseif($user->hasRole('agent'))
        {
            $agent  =Agent::all();
            $tracks   = Track::select('tracks.*','bill_lading','organization_name')
                ->join('laddings','laddings.track_id','=','tracks.id')
                ->join('agents','agents.id','=','tracks.agent_id')
                ->where('tracks.id', '=', $id)
                ->get();
            $lad_id = Ladding::with('track')->where('track_id','=',$id)->first()->id;
            $declaration_type = BlDeclaration::with('lad','declaration')->where('ladding_id','=',$lad_id)->get();
            $container_loop  = Container::with('lad')
                ->where('ladding_id','=',$lad_id)
                ->get();

            $tracks_loop   = Track::select('tracks.*','bill_lading')
                ->join('laddings','laddings.track_id','=','tracks.id')
                ->where('tracks.id', '=', $id)
                ->get();

            $user_id = Auth::id();
            $profile = User::where('id','=',$user_id)->get();
            $destination = Destination::all();
            return view('agent.agent_edit_order', compact('destination','expenses','normal','profile','tracks_loop','tracks','agent','container_loop','declaration_type'));
        }
        elseif($user->hasRole('super'))
        {
            $expenses = Expenses::with('container')->where('finance_status','LIKE','pending')->count();

            $normal = NormalExpenses::where('finance_status','LIKE','pending')->count();

            $agent  =Agent::all();
            $tracks   = Track::select('tracks.*','bill_lading','organization_name')
                ->join('laddings','laddings.track_id','=','tracks.id')
                ->join('agents','agents.id','=','tracks.agent_id')
                ->where('tracks.id', '=', $id)
                ->get();
            $lad_id = Ladding::with('track')->where('track_id','=',$id)->first()->id;
            $declaration_type = BlDeclaration::with('lad','declaration')->where('ladding_id','=',$lad_id)->get();
            $container_loop  = Container::with('lad')
                ->where('ladding_id','=',$lad_id)
                ->get();

            $tracks_loop   = Track::select('tracks.*','bill_lading')
                ->join('laddings','laddings.track_id','=','tracks.id')
                ->where('tracks.id', '=', $id)
                ->get();
            $destination = Destination::all();

            return view('admin.super_admin_edit_order', compact('destination','expenses','normal','tracks_loop','tracks','agent','container_loop','declaration_type'));
        }
        elseif($user->hasRole('documentation'))
        {
            $agent  =Agent::all();
            $tracks   = Track::select('tracks.*','bill_lading','organization_name')
                ->join('laddings','laddings.track_id','=','tracks.id')
                ->join('agents','agents.id','=','tracks.agent_id')
                ->where('tracks.id', '=', $id)
                ->get();
            $lad_id = Ladding::with('track')->where('track_id','=',$id)->first()->id;
            $declaration_type = BlDeclaration::with('lad','declaration')->where('ladding_id','=',$lad_id)->get();
            $container_loop  = Container::with('lad')
                ->where('ladding_id','=',$lad_id)
                ->get();

            $tracks_loop   = Track::select('tracks.*','bill_lading')
                ->join('laddings','laddings.track_id','=','tracks.id')
                ->where('tracks.id', '=', $id)
                ->get();
            $destination = Destination::all();

            return view('documentation.documentation_edit_order', compact('destination','expenses','normal','tracks_loop','tracks','agent','container_loop','declaration_type'));
        }
        elseif($user->hasRole('declaration'))
        {
            $agent  =Agent::all();
            $tracks   = Track::select('tracks.*','bill_lading','organization_name')
                ->join('laddings','laddings.track_id','=','tracks.id')
                ->join('agents','agents.id','=','tracks.agent_id')
                ->where('tracks.id', '=', $id)
                ->get();
            $lad_id = Ladding::with('track')->where('track_id','=',$id)->first()->id;
            $declaration_type = BlDeclaration::with('lad','declaration')->where('ladding_id','=',$lad_id)->get();
            $container_loop  = Container::with('lad')
                ->where('ladding_id','=',$lad_id)
                ->get();

            $tracks_loop   = Track::select('tracks.*','bill_lading')
                ->join('laddings','laddings.track_id','=','tracks.id')
                ->where('tracks.id', '=', $id)
                ->get();
            $destination = Destination::all();

            return view('declaration.declaration_edit_order', compact('destination','expenses','normal','tracks_loop','tracks','agent','container_loop','declaration_type'));
        }
        elseif($user->hasRole('shipping'))
        {
            $agent  =Agent::all();
            $tracks   = Track::select('tracks.*','bill_lading','organization_name')
                ->join('laddings','laddings.track_id','=','tracks.id')
                ->join('agents','agents.id','=','tracks.agent_id')
                ->where('tracks.id', '=', $id)
                ->get();
            $lad_id = Ladding::with('track')->where('track_id','=',$id)->first()->id;
            $declaration_type = BlDeclaration::with('lad','declaration')->where('ladding_id','=',$lad_id)->get();
            $container_loop  = Container::with('lad')
                ->where('ladding_id','=',$lad_id)
                ->get();

            $tracks_loop   = Track::select('tracks.*','bill_lading')
                ->join('laddings','laddings.track_id','=','tracks.id')
                ->where('tracks.id', '=', $id)
                ->get();

            $destination = Destination::all();
            return view('shipping.shpping_edit_order', compact('destination','expenses','normal','tracks_loop','tracks','agent','container_loop','declaration_type'));
        }
        elseif($user->hasRole('sales'))
        {
            $agent  =Agent::all();
            $tracks   = Track::select('tracks.*','bill_lading','organization_name')
                ->join('laddings','laddings.track_id','=','tracks.id')
                ->join('agents','agents.id','=','tracks.agent_id')
                ->where('tracks.id', '=', $id)
                ->get();
            $lad_id = Ladding::with('track')->where('track_id','=',$id)->first()->id;
            $declaration_type = BlDeclaration::with('lad','declaration')->where('ladding_id','=',$lad_id)->get();
            $container_loop  = Container::with('lad')
                ->where('ladding_id','=',$lad_id)
                ->get();

            $tracks_loop   = Track::select('tracks.*','bill_lading')
                ->join('laddings','laddings.track_id','=','tracks.id')
                ->where('tracks.id', '=', $id)
                ->get();

            $destination = Destination::all();
            return view('staff.staff_edit_order', compact('destination','expenses','normal','tracks_loop','agent','tracks','container_loop','declaration_type'));
        }
        elseif($user->hasRole('finance'))
        {
            $expenses = Expenses::with('container')->where('finance_status','LIKE','pending')->count();

            $normal = NormalExpenses::where('finance_status','LIKE','pending')->count();

            $agent  =Agent::all();
            $tracks   = Track::select('tracks.*','bill_lading','organization_name')
                ->join('laddings','laddings.track_id','=','tracks.id')
                ->join('agents','agents.id','=','tracks.agent_id')
                ->where('tracks.id', '=', $id)
                ->get();
            $lad_id = Ladding::with('track')->where('track_id','=',$id)->first()->id;
            $declaration_type = BlDeclaration::with('lad','declaration')->where('ladding_id','=',$lad_id)->get();
            $container_loop  = Container::with('lad')
                ->where('ladding_id','=',$lad_id)
                ->get();

            $tracks_loop   = Track::select('tracks.*','bill_lading')
                ->join('laddings','laddings.track_id','=','tracks.id')
                ->where('tracks.id', '=', $id)
                ->get();

            $destination = Destination::all();
            return view('finance.finance_edit_order', compact('destination','expenses','normal','tracks_loop','agent','tracks','container_loop','declaration_type'));
        }
        else
        {
            return  JsonResponse::create(['error' => 'access-denied'],401);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public  function now(Request $request)
    {

        $ladding= $request->get('ladding_id');
        $ladding_id = Ladding::where('track_id','=',$ladding)->first()->id;

        $user_id        = Auth::id();
        $count = count($request->input('cont_no'));
        for ($i=0; $i<$count; $i++)
        {
            $data[] = array('cont_no' => $request->input('cont_no')[$i],
                'user_id'       => $user_id,
                'ladding_id' =>$ladding_id,
                'created_at' =>Carbon::now());
        }
        DB::table('containers')->insert($data);
        Session::flash('message', 'Order created successfully');
        return back();
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|static
     */
    public function  save_add_status_api(Request $request)
    {
        $user = JWTAuth::parseToken()->authenticate();
        try {
            if($user->hasRole('admin') || $user->hasRole('sales') ||  $user->hasRole('super') || $user->hasRole('documentation') || $user->hasRole('shipping') || $user->hasRole('declaration') ||  $user->hasRole('finance') )
            {

                $cont_no = $request->get('cont_no');
                $containers = Container::where('cont_no','=',$cont_no)->first();


                $rules = [
                    'ladding_id'        => 'required',
                    'cont_no'           => 'required',
                    'processing_date'   => 'required',
                    'processing_status' => 'required',
                    'other_comment'     => 'required'
                ];

                $validator = Validator::make($request->all(), $rules);
                if ($validator->fails()) {
                    return response()->json($validator->messages(), 422);
                }

                $ladding_id = $request->get('ladding_id');
                $ladding = Ladding::where('id','=',$ladding_id)->first()->track_id;

                $user_id =Auth::id();

                $request->merge(['user_id' => $user_id , 'cont_id' =>$containers->id]);


                $order =  Trucking::create($request->all());
                if ($order->save());

                $request->merge(['current_status' => $order->processing_status,'other_comment' =>$order->other_comment]);
                $order = Track::findOrFail($ladding);
                $data = $request->all();
                $order->update($data);

                return response()->json(['status'=>200,'response' => 'order updated successful']);



            }

            elseif($user->hasRole('agent'))
            {
                $ladding_id = $request->get('ladding_id');
                $ladding = Ladding::where('id','=',$ladding_id)->first()->track_id;


                $user_id =Auth::id();
                $cont_id = $request->get('cont_no');
                $request->merge(['user_id' => $user_id , 'cont_id' =>$cont_id]);
                $order =  Trucking::create($request->all());
                if($order->save());
                $request->merge(['current_status' => $order->processing_status,'other_comment' =>$order->other_comment]);
                $order = Track::findOrFail($ladding);
                $data = $request->all();
                $order->update($data);
                Session::flash('delete', 'Order created successfully');
                return redirect('/my_orders');
            }
            else
            {
                return  JsonResponse::create(['error' => 'access-denied'],401);
            }

        }
        catch (Exception $e)
        {
            {
                return response()
                    ->json([
                        'status' => 500,
                        'response' => 'Bl id invalid or cont id '
                    ]);
            }
        }


    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     *
     * Request all and save add status
     */
    public function  save_add_status(Request $request)
    {
        $user =Auth::user();


        if($user->hasRole('admin') || $user->hasRole('sales') ||  $user->hasRole('super') || $user->hasRole('documentation') || $user->hasRole('shipping') || $user->hasRole('declaration') ||  $user->hasRole('finance') )
        {

            $ladding_id = $request->get('ladding_id');
            $ladding = Ladding::where('id','=',$ladding_id)->first()->track_id;

            $user_id =Auth::id();
            $cont_id = $request->get('cont_no');
            $request->merge(['user_id' => $user_id , 'cont_id' =>$cont_id]);
            $order =  Trucking::create($request->all());
            if ($order->save());

            $request->merge(['current_status' => $order->processing_status,'other_comment' =>$order->other_comment]);
            $order = Track::findOrFail($ladding);
            $data = $request->all();
            $order->update($data);
            Session::flash('delete', 'Order  status created successfully');
            return redirect('/all_order');
        }

        elseif($user->hasRole('agent'))
        {
            $ladding_id = $request->get('ladding_id');
            $ladding = Ladding::where('id','=',$ladding_id)->first()->track_id;


            $user_id =Auth::id();
            $cont_id = $request->get('cont_no');
            $request->merge(['user_id' => $user_id , 'cont_id' =>$cont_id]);
            $order =  Trucking::create($request->all());
            if($order->save());
            $request->merge(['current_status' => $order->processing_status,'other_comment' =>$order->other_comment]);
            $order = Track::findOrFail($ladding);
            $data = $request->all();
            $order->update($data);
            Session::flash('delete', 'Order created successfully');
            return redirect('/my_orders');
        }
        else
        {
            return  JsonResponse::create(['error' => 'access-denied'],401);
        }

    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|static
     *
     * tracking number
     */

    /**
     * @param $id
     * @return string
     */
    public function getContainer($id)
    {
        $states = DB::table("laddings")
            ->join('containers','containers.ladding_id','=',$id)->get();

        return json_encode($states);

    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|static
     *
     * tracking number without login
     */
    public  function  tracking_form()
    {

        $track = Track::select('tracks.*', 'status','comment','order_track_number')
            ->join('order_tracks','order_tracks.track_id','=','tracks.id')
            ->get();
        return view('truck',compact('track'));

    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * Result of trucking without login
     */
    public function find_truck_form(Request $request )
    {
        $truck  =  $request->input('order_track_number');

        if(($truck != "") )
        {
            $track = Track::select('tracks.*', 'status', 'comment', 'order_track_number')
                ->join('order_tracks', 'order_tracks.track_id', '=', 'tracks.id')
                ->where('tracks.order_track_number', '=', $truck)
                ->get();

            return view('truck_result', compact('track'));
        }
    }

    /**
     * @param Request $request
     * @return static
     *
     */
    public function ListContainer( Request $request, $id)
    {
        $expenses = Expenses::with('container')->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
        $normal = NormalExpenses::where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

        $user   =Auth::user();
        if($user->hasRole('admin'))
        {


            $track = Track::select('tracks.*','bill_lading')->join('laddings','laddings.track_id','=','tracks.id')->where('tracks.id','=',$id)->get();


            $containers = Container::select('containers.*','cont_no', 'date','type','client_name','type'
                ,'weight','line_vessel','eta','document_date_received','destination','terminal','driver_name','driver_contact',
                'commodity','port_charge','remarks','weight','file_opened_by','special_instruction','voyager_number','shipper','ata', 'transporter_name','trucking_number','loading_date')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->join('tracks','tracks.id','=','laddings.track_id')
                ->leftjoin('transporters','transporters.container_id','=','containers.id')
                ->leftjoin('drivers','drivers.container_id','=','containers.id')
                ->where('tracks.id', '=', $id)
                ->where('transporters.deleted_at', NULL)
                ->where('drivers.deleted_at', NULL)
                ->get();


            $status = Trucking::select('truckings.*','name','bill_lading','cont_no')
                ->join('laddings','laddings.id','=','truckings.ladding_id')
                ->join('tracks','tracks.id','=','laddings.track_id')
                ->join('users','users.id','=','truckings.user_id')
                ->Join('containers','containers.id','=','truckings.cont_id')
                ->where('tracks.id', '=', $id)
                ->orderBy('created_at','desc')
                ->get();





            return view('admin.list_containers', compact('containers','expenses','normal','track','status'));
        }
        elseif($user->hasRole('documentation'))
        {
            $track = Track::select('tracks.*','bill_lading')->join('laddings','laddings.track_id','=','tracks.id')->where('tracks.id','=',$id)->get();


            $containers = Container::select('containers.*','cont_no', 'date','type','client_name'
                ,'weight','line_vessel','eta','document_date_received','destination','terminal','driver_name','driver_contact',
                'commodity','port_charge','remarks','weight','file_opened_by','special_instruction','voyager_number','shipper','ata', 'transporter_name','trucking_number','loading_date')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->join('tracks','tracks.id','=','laddings.track_id')
                ->join('transporters','transporters.container_id','=','containers.id')
                ->Join('drivers','drivers.container_id','=','containers.id')
                ->where('tracks.id', '=', $id)
                ->where('transporters.deleted_at', NULL)
                ->where('drivers.deleted_at', NULL)
                ->get();


            $status = Trucking::select('truckings.*','name','bill_lading','cont_no')
                ->join('laddings','laddings.id','=','truckings.ladding_id')
                ->join('tracks','tracks.id','=','laddings.track_id')
                ->join('users','users.id','=','truckings.user_id')
                ->Join('containers','containers.id','=','truckings.cont_id')
                ->where('tracks.id', '=', $id)
                ->orderBy('created_at','desc')
                ->get();




            return view('documentation.list_containers', compact('containers','expenses','normal','track','status'));
        }
        elseif($user->hasRole('super'))
        {
            $track = Track::select('tracks.*','bill_lading')->join('laddings','laddings.track_id','=','tracks.id')->where('tracks.id','=',$id)->get();


            $containers = Container::select('containers.*','cont_no', 'date','type','client_name','type'
                ,'weight','line_vessel','eta','document_date_received','destination','terminal','driver_name','driver_contact',
                'commodity','port_charge','remarks','weight','file_opened_by','special_instruction','voyager_number','shipper','ata', 'transporter_name','trucking_number','loading_date')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->join('tracks','tracks.id','=','laddings.track_id')
                ->leftjoin('transporters','transporters.container_id','=','containers.id')
                ->leftjoin('drivers','drivers.container_id','=','containers.id')
                ->where('tracks.id', '=', $id)
                ->where('transporters.deleted_at', NULL)
                ->where('drivers.deleted_at', NULL)
                ->get();


            $status = Trucking::select('truckings.*','name','bill_lading','cont_no')
                ->join('laddings','laddings.id','=','truckings.ladding_id')
                ->join('tracks','tracks.id','=','laddings.track_id')
                ->join('users','users.id','=','truckings.user_id')
                ->Join('containers','containers.id','=','truckings.cont_id')
                ->where('tracks.id', '=', $id)
                ->orderBy('created_at','desc')
                ->get();




            return view('admin.super_list_containers', compact('containers','expenses','normal','track','status'));
        }
        elseif($user->hasRole('shipping'))
        {
            $track = Track::select('tracks.*','bill_lading')->join('laddings','laddings.track_id','=','tracks.id')->where('tracks.id','=',$id)->get();


            $containers = Container::select('containers.*','cont_no', 'date','type','client_name'
                ,'weight','line_vessel','eta','document_date_received','destination','terminal','driver_name','driver_contact',
                'commodity','port_charge','remarks','weight','file_opened_by','special_instruction','voyager_number','shipper','ata', 'transporter_name','trucking_number','loading_date')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->join('tracks','tracks.id','=','laddings.track_id')
                ->leftjoin('transporters','transporters.container_id','=','containers.id')
                ->leftjoin('drivers','drivers.container_id','=','containers.id')
                ->where('tracks.id', '=', $id)
                ->where('transporters.deleted_at', NULL)
                ->where('drivers.deleted_at', NULL)
                ->get();


            $status = Trucking::select('truckings.*','name','bill_lading','cont_no')
                ->join('laddings','laddings.id','=','truckings.ladding_id')
                ->join('tracks','tracks.id','=','laddings.track_id')
                ->join('users','users.id','=','truckings.user_id')
                ->Join('containers','containers.id','=','truckings.cont_id')
                ->where('tracks.id', '=', $id)
                ->orderBy('created_at','desc')
                ->get();



            return view('shipping.list_containers', compact('containers','expenses','normal','track','status'));
        }
        elseif($user->hasRole('declaration'))
        {
            $track = Track::select('tracks.*','bill_lading')->join('laddings','laddings.track_id','=','tracks.id')->where('tracks.id','=',$id)->get();


            $containers = Container::select('containers.*','cont_no', 'date','type','client_name'
                ,'weight','line_vessel','eta','document_date_received','destination','terminal','driver_name','driver_contact',
                'commodity','port_charge','remarks','weight','file_opened_by','special_instruction','voyager_number','shipper','ata', 'transporter_name','trucking_number','loading_date')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->join('tracks','tracks.id','=','laddings.track_id')
                ->leftjoin('transporters','transporters.container_id','=','containers.id')
                ->leftjoin('drivers','drivers.container_id','=','containers.id')
                ->where('tracks.id', '=', $id)
                ->where('transporters.deleted_at', NULL)
                ->where('drivers.deleted_at', NULL)
                ->get();


            $status = Trucking::select('truckings.*','name','bill_lading','cont_no')
                ->join('laddings','laddings.id','=','truckings.ladding_id')
                ->join('tracks','tracks.id','=','laddings.track_id')
                ->join('users','users.id','=','truckings.user_id')
                ->Join('containers','containers.id','=','truckings.cont_id')
                ->where('tracks.id', '=', $id)
                ->orderBy('created_at','desc')
                ->get();



            return view('declaration.list_containers', compact('containers','expenses','normal','track','status'));
        }
        elseif($user->hasRole('finance'))
        {
            $track = Track::select('tracks.*','bill_lading')->join('laddings','laddings.track_id','=','tracks.id')->where('tracks.id','=',$id)->get();


            $containers = Container::select('containers.*','cont_no', 'date','type','client_name'
                ,'weight','line_vessel','eta','document_date_received','destination','terminal','driver_name','driver_contact',
                'commodity','port_charge','remarks','weight','file_opened_by','special_instruction','voyager_number','shipper','ata', 'transporter_name','trucking_number','loading_date')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->join('tracks','tracks.id','=','laddings.track_id')
                ->leftjoin('transporters','transporters.container_id','=','containers.id')
                ->leftjoin('drivers','drivers.container_id','=','containers.id')
                ->where('tracks.id', '=', $id)
                ->where('transporters.deleted_at', NULL)
                ->where('drivers.deleted_at', NULL)
                ->get();


            $status = Trucking::select('truckings.*','name','bill_lading','cont_no')
                ->join('laddings','laddings.id','=','truckings.ladding_id')
                ->join('tracks','tracks.id','=','laddings.track_id')
                ->join('users','users.id','=','truckings.user_id')
                ->Join('containers','containers.id','=','truckings.cont_id')
                ->where('tracks.id', '=', $id)
                ->orderBy('created_at','desc')
                ->get();



            return view('finance.list_containers', compact('containers','expenses','normal','track','status'));
        }
        else
        {
            return  JsonResponse::create(['error' => 'access-denied'],401);
        }
    }


    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|static
     */
    public function find_truck_Api(Request $request )
    {
        $user = JWTAuth::parseToken()->authenticate();
        $expenses = Expenses::with('container')->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
        $normal = NormalExpenses::where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

        $truck          =  $request->input('bill_lading');
        $container_no   =  $request->input('cont_no');
        if($user->hasRole('admin'))
        {
            if(($truck != "") )
            {
                $track = Track::select('file_no','bill_lading','client_name','client_name','type','weight','frequency','eta','ata','destination','terminal','commodity','voyager_number','shipper','shipping_line','current_status')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->where('laddings.bill_lading', '=', $truck)
                    ->get();

                $get_bl_id = Ladding::where('bill_lading','=',$truck)->first();
                $containers = DB::table('containers')
                    ->select('containers.id','ladding_id','cont_no','driver_name','driver_contact',
                        'transporter_name','trucking_number','loading_date')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->leftjoin('transporters','transporters.container_id','=','containers.id')
                    ->leftjoin('drivers','drivers.container_id','=','containers.id')
                    ->where('containers.ladding_id','=',$get_bl_id->id)
                    ->orderByDesc("containers.created_at")
                    ->get();

                $status = Trucking::select('name as updated_by','bill_lading','cont_no','processing_date as updated_at','processing_status','other_comment')
                    ->join('laddings','laddings.id','=','truckings.ladding_id')
                    ->join('users','users.id','=','truckings.user_id')
                    ->Join('containers','containers.id','=','truckings.cont_id')
                    ->where('truckings.ladding_id', '=', $get_bl_id->id)
                    ->orderBy('truckings.created_at','desc')
                    ->get();


                return response()->json(['order'=>$track,'containers'=>$containers,'status_loop'=>$status], 200);
            }
            elseif (($container_no != ""))
            {


                $track = Track::select('file_no','bill_lading','client_name','client_name','type','weight','frequency','eta','ata','destination','terminal','commodity','voyager_number','shipper','shipping_line','current_status')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('containers','containers.ladding_id','=','laddings.id')
                    ->where('containers.cont_no', '=', $container_no)
                    ->get();


                $containers = Container::select('cont_no','type'
                    ,'weight as container_weight','transporter_name','driver_name','driver_contact',
                    'trucking_number','loading_date')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->join('tracks','tracks.id','=','laddings.track_id')
                    ->join('transporters','transporters.container_id','=','containers.id')
                    ->Join('drivers','drivers.container_id','=','containers.id')
                    ->where('containers.cont_no', '=', $container_no)
                    ->where('transporters.deleted_at', NULL)
                    ->where('drivers.deleted_at', NULL)
                    ->get();

                $driver = Driver::select('drivers.*')
                    ->join('containers','containers.id','=','drivers.container_id')
                    ->where('containers.cont_no', '=', $container_no)
                    ->get();

                $status = Trucking::select('name as updated_by','bill_lading','cont_no','processing_date as updated_at','processing_status','other_comment')
                    ->join('laddings','laddings.id','=','truckings.ladding_id')
                    ->join('users','users.id','=','truckings.user_id')
                    ->join('containers','containers.id','=','truckings.cont_id')
                    ->where('containers.cont_no', '=', $container_no)
                    ->latest('truckings.created_at','desc')
                    ->get();


                return response()->json(['order'=>$track,'containers'=>$containers,'status_loop'=>$status], 200);

            }
            elseif (($container_no == ""))
            {
                return  view('errors.404');
            }
            elseif (($truck == ""))
            {
                return  view('errors.404');
            }
        }
        elseif($user->hasRole('super'))
        {
            if(($truck != "") )
            {
                $track = Track::select('file_no','bill_lading','client_name','client_name','type','weight','frequency','eta','ata','destination','terminal','commodity','voyager_number','shipper','shipping_line','current_status')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->where('laddings.bill_lading', '=', $truck)
                    ->get();

                $get_bl_id = Ladding::where('bill_lading','=',$truck)->first();

                $containers = DB::table('containers')
                    ->select('containers.id','ladding_id','cont_no','driver_name','driver_contact',
                        'transporter_name','trucking_number','loading_date')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->leftjoin('transporters','transporters.container_id','=','containers.id')
                    ->leftjoin('drivers','drivers.container_id','=','containers.id')
                    ->where('containers.ladding_id','=',$get_bl_id->id)
                    ->orderByDesc("containers.created_at")
                    ->get();


                $status = Trucking::select('name as updated_by','bill_lading','cont_no','processing_date as updated_at','processing_status','other_comment')
                    ->join('laddings','laddings.id','=','truckings.ladding_id')
                    ->join('users','users.id','=','truckings.user_id')
                    ->Join('containers','containers.id','=','truckings.cont_id')
                    ->where('truckings.ladding_id', '=', $get_bl_id->id)
                    ->orderBy('truckings.created_at','desc')
                    ->get();


                return response()->json(['order'=>$track,'containers'=>$containers,'status_loop'=>$status], 200);
            }
            elseif (($container_no != ""))
            {


                $track = Track::select('file_no','bill_lading','client_name','client_name','type','weight','frequency','eta','ata','destination','terminal','commodity','voyager_number','shipper','shipping_line','current_status')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('containers','containers.ladding_id','=','laddings.id')
                    ->where('containers.cont_no', '=', $container_no)
                    ->get();


                $containers = Container::select('cont_no','type'
                    ,'weight as container_weight','transporter_name','driver_name','driver_contact',
                    'trucking_number','loading_date')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->join('tracks','tracks.id','=','laddings.track_id')
                    ->join('transporters','transporters.container_id','=','containers.id')
                    ->Join('drivers','drivers.container_id','=','containers.id')
                    ->where('containers.cont_no', '=', $container_no)
                    ->where('transporters.deleted_at', NULL)
                    ->where('drivers.deleted_at', NULL)
                    ->get();

                $driver = Driver::select('drivers.*')
                    ->join('containers','containers.id','=','drivers.container_id')
                    ->where('containers.cont_no', '=', $container_no)
                    ->get();

                $status = Trucking::select('name as updated_by','bill_lading','cont_no','processing_date as updated_at','processing_status','other_comment')
                    ->join('laddings','laddings.id','=','truckings.ladding_id')
                    ->join('users','users.id','=','truckings.user_id')
                    ->join('containers','containers.id','=','truckings.cont_id')
                    ->where('containers.cont_no', '=', $container_no)
                    ->latest('truckings.created_at','desc')
                    ->get();


                return response()->json(['order'=>$track,'containers'=>$containers,'status_loop'=>$status], 200);

            }
            elseif (($container_no == ""))
            {
                return  view('errors.404');
            }
            elseif (($truck == ""))
            {
                return  view('errors.404');
            }
        }
        elseif($user->hasRole('documentation'))
        {
            if(($truck != "") )
            {
                $track = Track::select('file_no','bill_lading','client_name','client_name','type','weight','frequency','eta','ata','destination','terminal','commodity','voyager_number','shipper','shipping_line','current_status')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->where('laddings.bill_lading', '=', $truck)
                    ->get();

                $get_bl_id = Ladding::where('bill_lading','=',$truck)->first();

                $containers = DB::table('containers')
                    ->select('containers.id','ladding_id','cont_no','driver_name','driver_contact',
                        'transporter_name','trucking_number','loading_date')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->leftjoin('transporters','transporters.container_id','=','containers.id')
                    ->leftjoin('drivers','drivers.container_id','=','containers.id')
                    ->where('containers.ladding_id','=',$get_bl_id->id)
                    ->orderByDesc("containers.created_at")
                    ->get();

                $status = Trucking::select('name as updated_by','bill_lading','cont_no','processing_date as updated_at','processing_status','other_comment')
                    ->join('laddings','laddings.id','=','truckings.ladding_id')
                    ->join('users','users.id','=','truckings.user_id')
                    ->Join('containers','containers.id','=','truckings.cont_id')
                    ->where('truckings.ladding_id', '=', $get_bl_id->id)
                    ->orderBy('truckings.created_at','desc')
                    ->get();


                return response()->json(['order'=>$track,'containers'=>$containers,'status_loop'=>$status], 200);
            }
            elseif (($container_no != ""))
            {


                $track = Track::select('file_no','bill_lading','client_name','client_name','type','weight','frequency','eta','ata','destination','terminal','commodity','voyager_number','shipper','shipping_line','current_status')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('containers','containers.ladding_id','=','laddings.id')
                    ->where('containers.cont_no', '=', $container_no)
                    ->get();


                $containers = Container::select('cont_no','type'
                    ,'weight as container_weight','transporter_name','driver_name','driver_contact',
                    'trucking_number','loading_date')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->join('tracks','tracks.id','=','laddings.track_id')
                    ->join('transporters','transporters.container_id','=','containers.id')
                    ->Join('drivers','drivers.container_id','=','containers.id')
                    ->where('containers.cont_no', '=', $container_no)
                    ->where('transporters.deleted_at', NULL)
                    ->where('drivers.deleted_at', NULL)
                    ->get();

                $driver = Driver::select('drivers.*')
                    ->join('containers','containers.id','=','drivers.container_id')
                    ->where('containers.cont_no', '=', $container_no)
                    ->get();

                $status = Trucking::select('name as updated_by','bill_lading','cont_no','processing_date as updated_at','processing_status','other_comment')
                    ->join('laddings','laddings.id','=','truckings.ladding_id')
                    ->join('users','users.id','=','truckings.user_id')
                    ->join('containers','containers.id','=','truckings.cont_id')
                    ->where('containers.cont_no', '=', $container_no)
                    ->latest('truckings.created_at','desc')
                    ->get();


                return response()->json(['order'=>$track,'containers'=>$containers,'status_loop'=>$status], 200);

            }
            elseif (($container_no == ""))
            {
                return  view('errors.404');
            }
            elseif (($truck == ""))
            {
                return  view('errors.404');
            }
        }
        elseif($user->hasRole('declaration'))
        {
            if(($truck != "") )
            {
                $track = Track::select('file_no','bill_lading','client_name','client_name','type','weight','frequency','eta','ata','destination','terminal','commodity','voyager_number','shipper','shipping_line','current_status')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->where('laddings.bill_lading', '=', $truck)
                    ->get();

                $get_bl_id = Ladding::where('bill_lading','=',$truck)->first();

                $containers = DB::table('containers')
                    ->select('containers.id','ladding_id','cont_no','driver_name','driver_contact',
                        'transporter_name','trucking_number','loading_date')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->leftjoin('transporters','transporters.container_id','=','containers.id')
                    ->leftjoin('drivers','drivers.container_id','=','containers.id')
                    ->where('containers.ladding_id','=',$get_bl_id->id)
                    ->orderByDesc("containers.created_at")
                    ->get();

                $status = Trucking::select('name as updated_by','bill_lading','cont_no','processing_date as updated_at','processing_status','other_comment')
                    ->join('laddings','laddings.id','=','truckings.ladding_id')
                    ->join('users','users.id','=','truckings.user_id')
                    ->Join('containers','containers.id','=','truckings.cont_id')
                    ->where('truckings.ladding_id', '=', $get_bl_id->id)
                    ->orderBy('truckings.created_at','desc')
                    ->get();


                return response()->json(['order'=>$track,'containers'=>$containers,'status_loop'=>$status], 200);
            }
            elseif (($container_no != ""))
            {


                $track = Track::select('file_no','bill_lading','client_name','client_name','type','weight','frequency','eta','ata','destination','terminal','commodity','voyager_number','shipper','shipping_line','current_status')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('containers','containers.ladding_id','=','laddings.id')
                    ->where('containers.cont_no', '=', $container_no)
                    ->get();


                $containers = Container::select('cont_no','type'
                    ,'weight as container_weight','transporter_name','driver_name','driver_contact',
                    'trucking_number','loading_date')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->join('tracks','tracks.id','=','laddings.track_id')
                    ->join('transporters','transporters.container_id','=','containers.id')
                    ->Join('drivers','drivers.container_id','=','containers.id')
                    ->where('containers.cont_no', '=', $container_no)
                    ->where('transporters.deleted_at', NULL)
                    ->where('drivers.deleted_at', NULL)
                    ->get();

                $driver = Driver::select('drivers.*')
                    ->join('containers','containers.id','=','drivers.container_id')
                    ->where('containers.cont_no', '=', $container_no)
                    ->get();

                $status = Trucking::select('name as updated_by','bill_lading','cont_no','processing_date as updated_at','processing_status','other_comment')
                    ->join('laddings','laddings.id','=','truckings.ladding_id')
                    ->join('users','users.id','=','truckings.user_id')
                    ->join('containers','containers.id','=','truckings.cont_id')
                    ->where('containers.cont_no', '=', $container_no)
                    ->latest('truckings.created_at','desc')
                    ->get();


                return response()->json(['order'=>$track,'containers'=>$containers,'status_loop'=>$status], 200);

            }
            elseif (($container_no == ""))
            {
                return  view('errors.404');
            }
            elseif (($truck == ""))
            {
                return  view('errors.404');
            }
        }
        elseif($user->hasRole('shipping'))
        {
            if(($truck != "") )
            {
                $track = Track::select('file_no','bill_lading','client_name','client_name','type','weight','frequency','eta','ata','destination','terminal','commodity','voyager_number','shipper','shipping_line','current_status')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->where('laddings.bill_lading', '=', $truck)
                    ->get();

                $get_bl_id = Ladding::where('bill_lading','=',$truck)->first();

                $containers = DB::table('containers')
                    ->select('containers.id','ladding_id','cont_no','driver_name','driver_contact',
                        'transporter_name','trucking_number','loading_date')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->leftjoin('transporters','transporters.container_id','=','containers.id')
                    ->leftjoin('drivers','drivers.container_id','=','containers.id')
                    ->where('containers.ladding_id','=',$get_bl_id->id)
                    ->orderByDesc("containers.created_at")
                    ->get();

                $status = Trucking::select('name as updated_by','bill_lading','cont_no','processing_date as updated_at','processing_status','other_comment')
                    ->join('laddings','laddings.id','=','truckings.ladding_id')
                    ->join('users','users.id','=','truckings.user_id')
                    ->Join('containers','containers.id','=','truckings.cont_id')
                    ->where('truckings.ladding_id', '=', $get_bl_id->id)
                    ->orderBy('truckings.created_at','desc')
                    ->get();


                return response()->json(['order'=>$track,'containers'=>$containers,'status_loop'=>$status], 200);
            }
            elseif (($container_no != ""))
            {


                $track = Track::select('file_no','bill_lading','client_name','client_name','type','weight','frequency','eta','ata','destination','terminal','commodity','voyager_number','shipper','shipping_line','current_status')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('containers','containers.ladding_id','=','laddings.id')
                    ->where('containers.cont_no', '=', $container_no)
                    ->get();


                $containers = Container::select('cont_no','type'
                    ,'weight as container_weight','transporter_name','driver_name','driver_contact',
                    'trucking_number','loading_date')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->join('tracks','tracks.id','=','laddings.track_id')
                    ->join('transporters','transporters.container_id','=','containers.id')
                    ->Join('drivers','drivers.container_id','=','containers.id')
                    ->where('containers.cont_no', '=', $container_no)
                    ->where('transporters.deleted_at', NULL)
                    ->where('drivers.deleted_at', NULL)
                    ->get();

                $driver = Driver::select('drivers.*')
                    ->join('containers','containers.id','=','drivers.container_id')
                    ->where('containers.cont_no', '=', $container_no)
                    ->get();

                $status = Trucking::select('name as updated_by','bill_lading','cont_no','processing_date as updated_at','processing_status','other_comment')
                    ->join('laddings','laddings.id','=','truckings.ladding_id')
                    ->join('users','users.id','=','truckings.user_id')
                    ->join('containers','containers.id','=','truckings.cont_id')
                    ->where('containers.cont_no', '=', $container_no)
                    ->latest('truckings.created_at','desc')
                    ->get();


                return response()->json(['order'=>$track,'containers'=>$containers,'status_loop'=>$status], 200);

            }
            elseif (($container_no == ""))
            {
                return  view('errors.404');
            }
            elseif (($truck == ""))
            {
                return  view('errors.404');
            }
        }
        elseif($user->hasRole('finance'))
        {
            if(($truck != "") )
            {
                $track = Track::select('file_no','bill_lading','client_name','client_name','type','weight','frequency','eta','ata','destination','terminal','commodity','voyager_number','shipper','shipping_line','current_status')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->where('laddings.bill_lading', '=', $truck)
                    ->get();

                $get_bl_id = Ladding::where('bill_lading','=',$truck)->first();

                $containers = DB::table('containers')
                    ->select('containers.id','ladding_id','cont_no','driver_name','driver_contact',
                        'transporter_name','trucking_number','loading_date')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->leftjoin('transporters','transporters.container_id','=','containers.id')
                    ->leftjoin('drivers','drivers.container_id','=','containers.id')
                    ->where('containers.ladding_id','=',$get_bl_id->id)
                    ->orderByDesc("containers.created_at")
                    ->get();


                $status = Trucking::select('name as updated_by','bill_lading','cont_no','processing_date as updated_at','processing_status','other_comment')
                    ->join('laddings','laddings.id','=','truckings.ladding_id')
                    ->join('users','users.id','=','truckings.user_id')
                    ->Join('containers','containers.id','=','truckings.cont_id')
                    ->where('truckings.ladding_id', '=', $get_bl_id->id)
                    ->orderBy('truckings.created_at','desc')
                    ->get();


                return response()->json(['order'=>$track,'containers'=>$containers,'status_loop'=>$status], 200);
            }
            elseif (($container_no != ""))
            {


                $track = Track::select('file_no','bill_lading','client_name','client_name','type','weight','frequency','eta','ata','destination','terminal','commodity','voyager_number','shipper','shipping_line','current_status')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('containers','containers.ladding_id','=','laddings.id')
                    ->where('containers.cont_no', '=', $container_no)
                    ->get();


                $containers = Container::select('cont_no','type'
                    ,'weight as container_weight','transporter_name','driver_name','driver_contact',
                    'trucking_number','loading_date')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->join('tracks','tracks.id','=','laddings.track_id')
                    ->join('transporters','transporters.container_id','=','containers.id')
                    ->Join('drivers','drivers.container_id','=','containers.id')
                    ->where('containers.cont_no', '=', $container_no)
                    ->where('transporters.deleted_at', NULL)
                    ->where('drivers.deleted_at', NULL)
                    ->get();

                $driver = Driver::select('drivers.*')
                    ->join('containers','containers.id','=','drivers.container_id')
                    ->where('containers.cont_no', '=', $container_no)
                    ->get();

                $status = Trucking::select('name as updated_by','bill_lading','cont_no','processing_date as updated_at','processing_status','other_comment')
                    ->join('laddings','laddings.id','=','truckings.ladding_id')
                    ->join('users','users.id','=','truckings.user_id')
                    ->join('containers','containers.id','=','truckings.cont_id')
                    ->where('containers.cont_no', '=', $container_no)
                    ->latest('truckings.created_at','desc')
                    ->get();


                return response()->json(['order'=>$track,'containers'=>$containers,'status_loop'=>$status], 200);

            }
            elseif (($container_no == ""))
            {
                return  view('errors.404');
            }
            elseif (($truck == ""))
            {
                return  view('errors.404');
            }
        }
        else
        {
            return  JsonResponse::create(['error' => 'access-denied'],401);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * Result of trucking
     */
    public function find_truck(Request $request )
    {
        $user =Auth::user();
        $expenses = Expenses::with('container')->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
        $normal = NormalExpenses::where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

        $truck          =  $request->input('bill_lading');
        $container_no =  $request->input('cont_no');
        if($user->hasRole('admin'))
        {
            if(($truck != "") )
            {
                $track = Track::select('tracks.*', 'bill_lading')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->where('laddings.id', '=', $truck)
                    ->get();

                $containers = Container::select('containers.*','cont_no', 'date','type','client_name'
                    ,'weight','line_vessel','eta','document_date_received','destination','terminal','driver_name','driver_contact',
                    'commodity','port_charge','remarks','weight','file_opened_by','special_instruction','voyager_number','shipper','ata', 'transporter_name','trucking_number','loading_date')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->join('tracks','tracks.id','=','laddings.track_id')
                    ->leftjoin('transporters','transporters.container_id','=','containers.id')
                    ->leftJoin('drivers','drivers.container_id','=','containers.id')
                    ->where('containers.ladding_id', '=', $truck)
                    ->where('transporters.deleted_at', NULL)
                    ->where('drivers.deleted_at', NULL)
                    ->get();

                $status = Trucking::select('truckings.*','name','bill_lading','cont_no')
                    ->join('laddings','laddings.id','=','truckings.ladding_id')
                    ->join('users','users.id','=','truckings.user_id')
                    ->Join('containers','containers.id','=','truckings.cont_id')
                    ->where('truckings.ladding_id', '=', $truck)
                    ->orderBy('created_at','desc')
                    ->get();


                return view('admin.order_details', compact('expenses','normal','track','containers','status'));
            }
            elseif (($container_no != ""))
            {


                $track = Track::select('tracks.*', 'bill_lading')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('containers','containers.ladding_id','=','laddings.id')
                    ->where('containers.cont_no', '=', $container_no)
                    ->get();


                $containers = Container::select('containers.*','cont_no', 'date','type','client_name'
                    ,'weight','line_vessel','eta','document_date_received','destination','terminal','driver_name','driver_contact',
                    'commodity','port_charge','remarks','weight','file_opened_by','special_instruction','voyager_number','shipper','ata', 'transporter_name','trucking_number','loading_date')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->join('tracks','tracks.id','=','laddings.track_id')
                    ->leftjoin('transporters','transporters.container_id','=','containers.id')
                    ->leftjoin('drivers','drivers.container_id','=','containers.id')
                    ->where('containers.cont_no', '=', $container_no)
                    ->where('transporters.deleted_at', NULL)
                    ->where('drivers.deleted_at', NULL)
                    ->get();

                $driver = Driver::select('drivers.*')
                    ->join('containers','containers.id','=','drivers.container_id')
                    ->where('containers.cont_no', '=', $container_no)
                    ->get();

                $status = Trucking::select('truckings.*','name','bill_lading','cont_no')
                    ->join('laddings','laddings.id','=','truckings.ladding_id')
                    ->join('users','users.id','=','truckings.user_id')
                    ->join('containers','containers.id','=','truckings.cont_id')
                    ->where('containers.cont_no', '=', $container_no)
                    ->latest('created_at','desc')
                    ->get();



                return view('admin.order_details', compact('expenses','normal','track','containers','driver','status'));
            }
            elseif (($container_no == ""))
            {
                return  view('errors.404');
            }
            elseif (($truck == ""))
            {
                return  view('errors.404');
            }
        }
        elseif($user->hasRole('super'))
        {
            $expenses = Expenses::with('container')->where('finance_status','LIKE','pending')->count();

            $normal = NormalExpenses::where('finance_status','LIKE','pending')->count();

            if(($truck != ""))
            {
                $track = Track::select('tracks.*', 'bill_lading')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->where('laddings.id', '=', $truck)
                    ->get();

                $containers = Container::select('containers.*','cont_no', 'date','type','client_name'
                    ,'weight','line_vessel','eta','document_date_received','destination','terminal','driver_name','driver_contact',
                    'commodity','port_charge','remarks','weight','file_opened_by','special_instruction','voyager_number','shipper','ata', 'transporter_name','trucking_number','loading_date')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->join('tracks','tracks.id','=','laddings.track_id')
                    ->leftjoin('transporters','transporters.container_id','=','containers.id')
                    ->leftjoin('drivers','drivers.container_id','=','containers.id')
                    ->where('containers.ladding_id', '=', $truck)
                    ->where('transporters.deleted_at', NULL)
                    ->where('drivers.deleted_at', NULL)
                    ->get();

                $driver = Driver::select('drivers.*')
                    ->join('containers','containers.id','=','drivers.container_id')
                    ->where('containers.ladding_id', '=', $truck)
                    ->get();

                $status = Trucking::select('truckings.*','name','bill_lading','cont_no')
                    ->join('laddings','laddings.id','=','truckings.ladding_id')
                    ->join('users','users.id','=','truckings.user_id')
                    ->leftJoin('containers','containers.id','=','truckings.cont_id')
                    ->where('truckings.ladding_id', '=', $truck)
                    ->orderBy('created_at','desc')
                    ->get();


                return view('admin.super_order_details', compact('expenses','normal','track','containers','driver','status'));
            }
            elseif (($container_no != ""))
            {


                $track = Track::select('tracks.*', 'bill_lading')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('containers','containers.ladding_id','=','laddings.id')
                    ->where('containers.cont_no', '=', $container_no)
                    ->get();


                $containers = Container::select('containers.*','cont_no', 'date','type','client_name'
                    ,'weight','line_vessel','eta','document_date_received','destination','terminal','driver_name','driver_contact',
                    'commodity','port_charge','remarks','weight','file_opened_by','special_instruction','voyager_number','shipper','ata', 'transporter_name','trucking_number','loading_date')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->join('tracks','tracks.id','=','laddings.track_id')
                    ->leftjoin('transporters','transporters.container_id','=','containers.id')
                    ->leftjoin('drivers','drivers.container_id','=','containers.id')
                    ->where('containers.cont_no', '=', $container_no)
                    ->where('transporters.deleted_at', NULL)
                    ->where('drivers.deleted_at', NULL)
                    ->get();


                $driver = Driver::select('drivers.*')
                    ->join('containers','containers.id','=','drivers.container_id')
                    ->where('containers.cont_no', '=', $container_no)
                    ->get();

                $status = Trucking::select('truckings.*','name','bill_lading','cont_no')
                    ->join('laddings','laddings.id','=','truckings.ladding_id')
                    ->join('users','users.id','=','truckings.user_id')
                    ->join('containers','containers.id','=','truckings.cont_id')
                    ->where('containers.cont_no', '=', $container_no)
                    ->latest('created_at','desc')
                    ->get();



                return view('admin.super_order_details', compact('expenses','normal','track','containers','driver','status'));
            }
            elseif (($container_no == ""))
            {
                return  view('errors.404');
            }
            elseif (($truck == ""))
            {
                return  view('errors.404');
            }
        }
        elseif($user->hasRole('sales'))
        {
            $truck = $request->input('bill_lading');
            if(($truck != ""))
            {
                $track = Track::select('tracks.*', 'bill_lading')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->where('laddings.id', '=', $truck)
                    ->get();

                $containers = Container::select('containers.*','cont_no', 'date','type','client_name'
                    ,'weight','line_vessel','eta','document_date_received','destination','terminal','driver_name','driver_contact',
                    'commodity','port_charge','remarks','weight','file_opened_by','special_instruction','voyager_number','shipper','ata', 'transporter_name','trucking_number','loading_date')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->join('tracks','tracks.id','=','laddings.track_id')
                    ->leftjoin('transporters','transporters.container_id','=','containers.id')
                    ->leftjoin('drivers','drivers.container_id','=','containers.id')
                    ->where('containers.ladding_id', '=', $truck)
                    ->where('transporters.deleted_at', NULL)
                    ->where('drivers.deleted_at', NULL)
                    ->get();

                $driver = Driver::select('drivers.*')
                    ->join('containers','containers.id','=','drivers.container_id')
                    ->where('containers.ladding_id', '=', $truck)
                    ->get();

                $status = Trucking::select('truckings.*','name','bill_lading','cont_no')
                    ->join('laddings','laddings.id','=','truckings.ladding_id')
                    ->join('users','users.id','=','truckings.user_id')
                    ->leftJoin('containers','containers.id','=','truckings.cont_id')
                    ->where('truckings.ladding_id', '=', $truck)
                    ->orderBy('created_at','desc')
                    ->get();


                return view('staff.sales_truck_result', compact('expenses','normal','track', 'containers', 'driver', 'status'));

            }
            elseif (($container_no != ""))
            {
                $track = Track::select('tracks.*', 'bill_lading')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('containers','containers.ladding_id','=','laddings.id')
                    ->where('containers.cont_no', '=', $container_no)
                    ->get();


                $containers = Container::select('containers.*','cont_no', 'date','type','client_name'
                    ,'weight','line_vessel','eta','document_date_received','destination','terminal','driver_name','driver_contact',
                    'commodity','port_charge','remarks','weight','file_opened_by','special_instruction','voyager_number','shipper','ata', 'transporter_name','trucking_number','loading_date')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->join('tracks','tracks.id','=','laddings.track_id')
                    ->leftjoin('transporters','transporters.container_id','=','containers.id')
                    ->leftjoin('drivers','drivers.container_id','=','containers.id')
                    ->where('containers.cont_no', '=', $container_no)
                    ->where('transporters.deleted_at', NULL)
                    ->where('drivers.deleted_at', NULL)
                    ->get();
                $driver = Driver::select('drivers.*')
                    ->join('containers','containers.id','=','drivers.container_id')
                    ->where('containers.cont_no', '=', $container_no)
                    ->get();

                $status = Trucking::select('truckings.*','name','bill_lading','cont_no')
                    ->join('laddings','laddings.id','=','truckings.ladding_id')
                    ->join('users','users.id','=','truckings.user_id')
                    ->join('containers','containers.id','=','truckings.cont_id')
                    ->where('containers.cont_no', '=', $container_no)
                    ->latest('created_at','desc')
                    ->get();


                return view('staff.sales_truck_result', compact('expenses','normal','track', 'containers', 'driver', 'status'));

            }
            elseif (($container_no == ""))
            {
                return  view('errors.404');
            }
            elseif (($truck == ""))
            {
                return  view('errors.404');
            }
        }
        elseif($user->hasRole('agent'))
        {
            $truck  =  $request->input('bill_lading');
            if(($truck != "") ) {
                $user_id = Auth::id();
                $profile = User::where('id','=',$user_id)->get();

                $track = Track::select('tracks.*', 'bill_lading')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->where('laddings.id', '=', $truck)
                    ->get();

                $containers = Container::select('containers.*','cont_no', 'date','type','client_name'
                    ,'weight','line_vessel','eta','document_date_received','destination','terminal',
                    'commodity','port_charge','remarks','weight','file_opened_by','special_instruction','voyager_number','shipper','ata', 'transporter_name','trucking_number','loading_date')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->join('tracks','tracks.id','=','laddings.track_id')
                    ->leftjoin('transporters','transporters.container_id','=','containers.id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('containers.ladding_id', '=', $truck)
                    ->get();

                $driver = Driver::select('drivers.*')
                    ->leftjoin('containers','containers.id','=','drivers.container_id')
                    ->where('containers.ladding_id', '=', $truck)
                    ->get();

                $status = Trucking::select('truckings.*','name','bill_lading','cont_no')
                    ->join('laddings','laddings.id','=','truckings.ladding_id')
                    ->join('users','users.id','=','truckings.user_id')
                    ->leftjoin('containers','containers.id','=','truckings.cont_id')
                    ->where('truckings.ladding_id', '=', $truck)
                    ->orderBy('created_at','desc')
                    ->get();

                return view('agent.agent_truck_result', compact('expenses','normal','track','containers','driver','status','profile'));
            }
            elseif (($container_no != ""))
            {
                $user_id = Auth::id();
                $track = Track::select('tracks.*', 'bill_lading')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('containers','containers.ladding_id','=','laddings.id')
                    ->where('containers.cont_no', '=', $container_no)
                    ->get();


                $containers = Container::select('containers.*','cont_no', 'date','type','client_name'
                    ,'weight','line_vessel','eta','document_date_received','destination','terminal','driver_name','driver_contact',
                    'commodity','port_charge','remarks','weight','file_opened_by','special_instruction','voyager_number','shipper','ata', 'transporter_name','trucking_number','loading_date')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->join('tracks','tracks.id','=','laddings.track_id')
                    ->leftjoin('drivers','drivers.container_id','=','containers.id')
                    ->leftjoin('transporters','transporters.container_id','=','containers.id')
                    ->where('containers.cont_no','=',$container_no)
                    ->get();

                $driver = Driver::select('drivers.*')
                    ->leftjoin('containers','containers.id','=','drivers.container_id')
                    ->where('containers.cont_no', '=', $container_no)
                    ->get();

                $status = Trucking::select('truckings.*','name','bill_lading','cont_no')
                    ->join('laddings','laddings.id','=','truckings.ladding_id')
                    ->join('users','users.id','=','truckings.user_id')
                    ->join('containers','containers.id','=','truckings.cont_id')
                    ->where('containers.cont_no', '=', $container_no)
                    ->orderBy('created_at','desc')
                    ->get();
                $profile = User::where('id','=',$user_id)->get();

                return view('agent.agent_truck_result', compact('expenses','normal','track','containers','driver','status','profile'));
            }
            elseif (($truck == ""))
            {
                return  view('errors.404');
            }
            elseif (($container_no == ""))
            {
                return  view('errors.404');
            }

        }
        else if($user->hasRole('documentation'))
        {
            if(($truck != ""))
            {
                $track = Track::select('tracks.*', 'bill_lading')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->where('laddings.id', '=', $truck)
                    ->get();

                $containers = Container::select('containers.*','cont_no', 'date','type','client_name'
                    ,'weight','line_vessel','eta','document_date_received','destination','terminal','driver_name','driver_contact',
                    'commodity','port_charge','remarks','weight','file_opened_by','special_instruction','voyager_number','shipper','ata', 'transporter_name','trucking_number','loading_date')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->join('tracks','tracks.id','=','laddings.track_id')
                    ->leftjoin('transporters','transporters.container_id','=','containers.id')
                    ->leftjoin('drivers','drivers.container_id','=','containers.id')
                    ->where('containers.ladding_id', '=', $truck)
                    ->where('transporters.deleted_at', NULL)
                    ->where('drivers.deleted_at', NULL)
                    ->get();

                $driver = Driver::select('drivers.*')
                    ->join('containers','containers.id','=','drivers.container_id')
                    ->where('containers.ladding_id', '=', $truck)
                    ->get();

                $status = Trucking::select('truckings.*','name','bill_lading','cont_no')
                    ->join('laddings','laddings.id','=','truckings.ladding_id')
                    ->join('users','users.id','=','truckings.user_id')
                    ->leftJoin('containers','containers.id','=','truckings.cont_id')
                    ->where('truckings.ladding_id', '=', $truck)
                    ->orderBy('created_at','desc')
                    ->get();


                return view('documentation.order_details', compact('expenses','normal','track','containers','driver','status'));
            }
            elseif (($container_no != ""))
            {


                $track = Track::select('tracks.*', 'bill_lading')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('containers','containers.ladding_id','=','laddings.id')
                    ->where('containers.cont_no', '=', $container_no)
                    ->get();


                $containers = Container::select('containers.*','cont_no', 'date','type','client_name'
                    ,'weight','line_vessel','eta','document_date_received','destination','terminal','driver_name','driver_contact',
                    'commodity','port_charge','remarks','weight','file_opened_by','special_instruction','voyager_number','shipper','ata', 'transporter_name','trucking_number','loading_date')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->join('tracks','tracks.id','=','laddings.track_id')
                    ->leftjoin('transporters','transporters.container_id','=','containers.id')
                    ->leftjoin('drivers','drivers.container_id','=','containers.id')
                    ->where('containers.cont_no', '=', $container_no)
                    ->where('transporters.deleted_at', NULL)
                    ->where('drivers.deleted_at', NULL)
                    ->get();

                $driver = Driver::select('drivers.*')
                    ->join('containers','containers.id','=','drivers.container_id')
                    ->where('containers.cont_no', '=', $container_no)
                    ->get();

                $status = Trucking::select('truckings.*','name','bill_lading','cont_no')
                    ->join('laddings','laddings.id','=','truckings.ladding_id')
                    ->join('users','users.id','=','truckings.user_id')
                    ->join('containers','containers.id','=','truckings.cont_id')
                    ->where('containers.cont_no', '=', $container_no)
                    ->latest('created_at','desc')
                    ->get();



                return view('documentation.order_details', compact('expenses','normal','track','containers','driver','status'));
            }
            elseif (($container_no == ""))
            {
                return  view('errors.404');
            }
            elseif (($truck == ""))
            {
                return  view('errors.404');
            }
        }
        else if($user->hasRole('declaration'))
        {
            if(($truck != ""))
            {
                $track = Track::select('tracks.*', 'bill_lading')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->where('laddings.id', '=', $truck)
                    ->get();

                $containers = Container::select('containers.*','cont_no', 'date','type','client_name'
                    ,'weight','line_vessel','eta','document_date_received','destination','terminal','driver_name','driver_contact',
                    'commodity','port_charge','remarks','weight','file_opened_by','special_instruction','voyager_number','shipper','ata', 'transporter_name','trucking_number','loading_date')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->join('tracks','tracks.id','=','laddings.track_id')
                    ->leftjoin('transporters','transporters.container_id','=','containers.id')
                    ->leftjoin('drivers','drivers.container_id','=','containers.id')
                    ->where('containers.ladding_id', '=', $truck)
                    ->where('transporters.deleted_at', NULL)
                    ->where('drivers.deleted_at', NULL)
                    ->get();


                $driver = Driver::select('drivers.*')
                    ->join('containers','containers.id','=','drivers.container_id')
                    ->where('containers.ladding_id', '=', $truck)
                    ->get();

                $status = Trucking::select('truckings.*','name','bill_lading','cont_no')
                    ->join('laddings','laddings.id','=','truckings.ladding_id')
                    ->join('users','users.id','=','truckings.user_id')
                    ->leftJoin('containers','containers.id','=','truckings.cont_id')
                    ->where('truckings.ladding_id', '=', $truck)
                    ->orderBy('created_at','desc')
                    ->get();


                return view('declaration.order_details', compact('expenses','normal','track','containers','driver','status'));
            }
            elseif (($container_no != ""))
            {


                $track = Track::select('tracks.*', 'bill_lading')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('containers','containers.ladding_id','=','laddings.id')
                    ->where('containers.cont_no', '=', $container_no)
                    ->get();


                $containers = Container::select('containers.*','cont_no', 'date','type','client_name'
                    ,'weight','line_vessel','eta','document_date_received','destination','terminal','driver_name','driver_contact',
                    'commodity','port_charge','remarks','weight','file_opened_by','special_instruction','voyager_number','shipper','ata', 'transporter_name','trucking_number','loading_date')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->join('tracks','tracks.id','=','laddings.track_id')
                    ->leftjoin('transporters','transporters.container_id','=','containers.id')
                    ->leftjoin('drivers','drivers.container_id','=','containers.id')
                    ->where('containers.cont_no', '=', $container_no)
                    ->where('transporters.deleted_at', NULL)
                    ->where('drivers.deleted_at', NULL)
                    ->get();

                $driver = Driver::select('drivers.*')
                    ->join('containers','containers.id','=','drivers.container_id')
                    ->where('containers.cont_no', '=', $container_no)
                    ->get();

                $status = Trucking::select('truckings.*','name','bill_lading','cont_no')
                    ->join('laddings','laddings.id','=','truckings.ladding_id')
                    ->join('users','users.id','=','truckings.user_id')
                    ->join('containers','containers.id','=','truckings.cont_id')
                    ->where('containers.cont_no', '=', $container_no)
                    ->latest('created_at','desc')
                    ->get();



                return view('documentation.order_details', compact('expenses','normal','track','containers','driver','status'));
            }
            elseif (($container_no == ""))
            {
                return  view('errors.404');
            }
            elseif (($truck == ""))
            {
                return  view('errors.404');
            }
        }
        else if($user->hasRole('shipping'))
        {
            if(($truck != ""))
            {
                $track = Track::select('tracks.*', 'bill_lading')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->where('laddings.id', '=', $truck)
                    ->get();

                $containers = Container::select('containers.*','cont_no', 'date','type','client_name'
                    ,'weight','line_vessel','eta','document_date_received','destination','terminal','driver_name','driver_contact',
                    'commodity','port_charge','remarks','weight','file_opened_by','special_instruction','voyager_number','shipper','ata', 'transporter_name','trucking_number','loading_date')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->join('tracks','tracks.id','=','laddings.track_id')
                    ->leftjoin('transporters','transporters.container_id','=','containers.id')
                    ->leftjoin('drivers','drivers.container_id','=','containers.id')
                    ->where('containers.ladding_id', '=', $truck)
                    ->where('transporters.deleted_at', NULL)
                    ->where('drivers.deleted_at', NULL)
                    ->get();


                $driver = Driver::select('drivers.*')
                    ->join('containers','containers.id','=','drivers.container_id')
                    ->where('containers.ladding_id', '=', $truck)
                    ->get();

                $status = Trucking::select('truckings.*','name','bill_lading','cont_no')
                    ->join('laddings','laddings.id','=','truckings.ladding_id')
                    ->join('users','users.id','=','truckings.user_id')
                    ->leftJoin('containers','containers.id','=','truckings.cont_id')
                    ->where('truckings.ladding_id', '=', $truck)
                    ->orderBy('created_at','desc')
                    ->get();


                return view('shipping.order_details', compact('expenses','normal','track','containers','driver','status'));
            }
            elseif (($container_no != ""))
            {


                $track = Track::select('tracks.*', 'bill_lading')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('containers','containers.ladding_id','=','laddings.id')
                    ->where('containers.cont_no', '=', $container_no)
                    ->get();


                $containers = Container::select('containers.*','cont_no', 'date','type','client_name'
                    ,'weight','line_vessel','eta','document_date_received','destination','terminal','driver_name','driver_contact',
                    'commodity','port_charge','remarks','weight','file_opened_by','special_instruction','voyager_number','shipper','ata', 'transporter_name','trucking_number','loading_date')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->join('tracks','tracks.id','=','laddings.track_id')
                    ->leftjoin('transporters','transporters.container_id','=','containers.id')
                    ->leftjoin('drivers','drivers.container_id','=','containers.id')
                    ->where('containers.cont_no', '=', $container_no)
                    ->where('transporters.deleted_at', NULL)
                    ->where('drivers.deleted_at', NULL)
                    ->get();
                $driver = Driver::select('drivers.*')
                    ->join('containers','containers.id','=','drivers.container_id')
                    ->where('containers.cont_no', '=', $container_no)
                    ->get();

                $status = Trucking::select('truckings.*','name','bill_lading','cont_no')
                    ->join('laddings','laddings.id','=','truckings.ladding_id')
                    ->join('users','users.id','=','truckings.user_id')
                    ->join('containers','containers.id','=','truckings.cont_id')
                    ->where('containers.cont_no', '=', $container_no)
                    ->latest('created_at','desc')
                    ->get();



                return view('shipping.order_details', compact('expenses','normal','track','containers','driver','status'));
            }
            elseif (($container_no == ""))
            {
                return  view('errors.404');
            }
            elseif (($truck == ""))
            {
                return  view('errors.404');
            }
        }
        else if($user->hasRole('finance'))
        {
            $expenses = Expenses::with('container')->where('finance_status','LIKE','pending')->count();

            $normal = NormalExpenses::where('finance_status','LIKE','pending')->count();

            if(($truck != ""))
            {
                $track = Track::select('tracks.*', 'bill_lading')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->where('laddings.id', '=', $truck)
                    ->get();

                $containers = Container::select('containers.*','cont_no', 'date','type','client_name'
                    ,'weight','line_vessel','eta','document_date_received','destination','terminal','driver_name','driver_contact',
                    'commodity','port_charge','remarks','weight','file_opened_by','special_instruction','voyager_number','shipper','ata', 'transporter_name','trucking_number','loading_date')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->join('tracks','tracks.id','=','laddings.track_id')
                    ->leftjoin('transporters','transporters.container_id','=','containers.id')
                    ->leftjoin('drivers','drivers.container_id','=','containers.id')
                    ->where('containers.ladding_id', '=', $truck)
                    ->where('transporters.deleted_at', NULL)
                    ->where('drivers.deleted_at', NULL)
                    ->get();

                $driver = Driver::select('drivers.*')
                    ->join('containers','containers.id','=','drivers.container_id')
                    ->where('containers.ladding_id', '=', $truck)
                    ->get();

                $status = Trucking::select('truckings.*','name','bill_lading','cont_no')
                    ->join('laddings','laddings.id','=','truckings.ladding_id')
                    ->join('users','users.id','=','truckings.user_id')
                    ->leftJoin('containers','containers.id','=','truckings.cont_id')
                    ->where('truckings.ladding_id', '=', $truck)
                    ->orderBy('created_at','desc')
                    ->get();


                return view('finance.order_details', compact('expenses','normal','track','containers','driver','status'));
            }
            elseif (($container_no != ""))
            {


                $track = Track::select('tracks.*', 'bill_lading')
                    ->join('laddings','laddings.track_id','=','tracks.id')
                    ->join('containers','containers.ladding_id','=','laddings.id')
                    ->where('containers.cont_no', '=', $container_no)
                    ->get();


                $containers = Container::select('containers.*','cont_no', 'date','type','client_name'
                    ,'weight','line_vessel','eta','document_date_received','destination','terminal','driver_name','driver_contact',
                    'commodity','port_charge','remarks','weight','file_opened_by','special_instruction','voyager_number','shipper','ata', 'transporter_name','trucking_number','loading_date')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->join('tracks','tracks.id','=','laddings.track_id')
                    ->leftjoin('transporters','transporters.container_id','=','containers.id')
                    ->leftjoin('drivers','drivers.container_id','=','containers.id')
                    ->where('containers.cont_no', '=', $container_no)
                    ->where('transporters.deleted_at', NULL)
                    ->where('drivers.deleted_at', NULL)
                    ->get();
                $driver = Driver::select('drivers.*')
                    ->join('containers','containers.id','=','drivers.container_id')
                    ->where('containers.cont_no', '=', $container_no)
                    ->get();

                $status = Trucking::select('truckings.*','name','bill_lading','cont_no')
                    ->join('laddings','laddings.id','=','truckings.ladding_id')
                    ->join('users','users.id','=','truckings.user_id')
                    ->join('containers','containers.id','=','truckings.cont_id')
                    ->where('containers.cont_no', '=', $container_no)
                    ->latest('created_at','desc')
                    ->get();



                return view('finance.order_details', compact('expenses','normal','track','containers','driver','status'));
            }
            elseif (($container_no == ""))
            {
                return  view('errors.404');
            }
            elseif (($truck == ""))
            {
                return  view('errors.404');
            }
        }
        else
        {
            return  JsonResponse::create(['error' => 'access-denied'],401);
        }
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function update_add_status(Request $request, $id)
    {
        $user =Auth::user();

        if($user->hasRole('admin'))
        {
            $user_id    =Auth::id();
            $request->merge(['user_id' => $user_id ]);
            $order =  Track::findOrFail($id);
            $data = $request->all();
            $order->update($data);
            Session::flash('update', 'Order  status created successfully');
            return redirect('/all_order');
        }
        else
        {
            return  JsonResponse::create(['error' => 'access-denied'],401);
        }
    }

    /***
     * @param Request $request
     * @param $id
     * @throws \Exception
     */
    public  function delete_add_status(Request $request, $id)
    {
        $area =Trucking::findOrfail($id);
        $area->delete();
        Session::flash('delete', 'Order Deleted successfully');
        dd($area);
    }

    /**
     * @param $id
     *
     */

    public function edit($id)
    {
        //
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updateStatus(Request $request, $id)
    {
        $user_id    =Auth::id();
        $request->merge(['user_id' => $user_id ]);
        $order =  Trucking::findOrFail($id);
        $track   = Track::select('tracks.*','bill_lading')
            ->join('laddings','laddings.track_id','=','tracks.id')
            ->join('truckings','truckings.ladding_id','=','laddings.id')
            ->where('truckings.id','=',$id)
            ->first()->id;


        $data = $request->all();
        if($order->update($data));
        $track_data =  Track::findOrFail($track);
        $track = $request->all();
        $track_data->update($track);

        Session::flash('updated', 'Order Status Updated successfully');
        return redirect('/tracking');

    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function deleteStatus(Request $request, $id)
    {

        $area =Trucking::findOrfail($id);
        $area->delete();
        Session::flash('delete', 'Status Deleted successfully');
        return redirect('/tracking');


    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user_id    =Auth::id();
        $request->merge(['user_id' => $user_id ]);
        $order =  Track::findOrFail($id);
        $data = $request->all();
        $order->update($data);
        Session::flash('updated', 'Order created successfully');
        return back();
    }


    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     *
     * Edit Order
     */

    public function admin_update_order(Request $request, $id)
    {
        $cont =  $request['cont_no'];
        $user   =Auth::user();
        if($user->hasRole('admin') || $user->hasRole('documentation') || $user->hasRole('super') || $user->hasRole('shipping') || $user->hasRole('declaration') || $user->hasRole('finance') )
        {

            $contract_doc           = $request->file('delivery_order');
            $port_charge_invoice    = $request->file('port_charge_invoice');
            $bl_doc                 = $request->file('bl_doc');
            $package_list_doc       = $request->file('package_list_doc');
            $invoice_order          = $request->file('invoice_order');

            $order = Track::findOrFail($id);

            if (isset($invoice_order)) {
                if (isset($invoice_order)) {
                    $extension1 = $request->file('invoice_order')->getClientOriginalExtension();
                }
                $sha = 'invoice_order' . md5(time());
                if (isset($extension1)) {
                    $filename6 = date('Y') . $sha . "sgfs." . $extension1;
                }
                $destination_path = 'OrderDoc/';
                if (empty($destination_path)) {
                    File::makeDirectory($destination_path, 0775, true, true);
                }
                if (isset($filename6)) {
                    $order->invoice_order = $destination_path . $filename6;
                    $request->file('invoice_order')->move($destination_path, $filename6);
                    $order->save();
                }
            }

            if (isset($package_list_doc)) {
                if (isset($package_list_doc)) {
                    $extension1 = $request->file('package_list_doc')->getClientOriginalExtension();
                }
                $sha = 'package_list_doc' . md5(time());
                if (isset($extension1)) {
                    $filename2 = date('Y') . $sha . "sgfs." . $extension1;
                }
                $destination_path = 'OrderDoc/';
                if (empty($destination_path)) {
                    File::makeDirectory($destination_path, 0775, true, true);
                    // File::makeDirectory($destination_path, $mode = 0777, true, true);
                }
                if (isset($filename2)) {
                    $order->package_list_doc = $destination_path . $filename2;
                    $request->file('package_list_doc')->move($destination_path, $filename2);
                    $order->save();
                }

            }

            if (isset($bl_doc)) {
                if (isset($bl_doc)) {
                    $extension1 = $request->file('bl_doc')->getClientOriginalExtension();
                }
                $sha = 'bl_doc' . md5(time());
                if (isset($extension1)) {
                    $filename3 = date('Y') . $sha . "sgfs." . $extension1;
                }
                $destination_path = 'OrderDoc/';
                if (empty($destination_path)) {
                    File::makeDirectory($destination_path, 0775, true, true);
                    // File::makeDirectory($destination_path, $mode = 0777, true, true);
                }
                if (isset($filename3)) {
                    $order->bl_doc = $destination_path . $filename3;
                    $request->file('bl_doc')->move($destination_path, $filename3);
                    $order->save();
                }

            }


            if (isset($contract_doc)) {
                if (isset($contract_doc)) {
                    $extension1 = $request->file('delivery_order')->getClientOriginalExtension();
                }
                $sha = 'rec_file' . md5(time());
                if (isset($extension1)) {
                    $filename4 = date('Y') . $sha . "sgfs." . $extension1;
                }
                $destination_path = 'DeliveryOrder/';
                if (empty($destination_path)) {
                    File::makeDirectory($destination_path, 0775, true, true);
                    // File::makeDirectory($destination_path, $mode = 0777, true, true);
                }
                if (isset($filename4)) {
                    $order->delivery_order = $destination_path . $filename4;
                    $request->file('delivery_order')->move($destination_path, $filename4);
                    $order->save();
                }

            }

            if (isset($port_charge_invoice)) {
                if (isset($port_charge_invoice)) {
                    $extension1 = $request->file('port_charge_invoice')->getClientOriginalExtension();
                }
                $sha = 'rec_file' . md5(time());
                if (isset($extension1)) {
                    $filename5 = date('Y') . $sha . "sgfs." . $extension1;
                }
                $destination_path = 'PortCharges/';
                if (empty($destination_path)) {
                    File::makeDirectory($destination_path, 0775, true, true);
                    // File::makeDirectory($destination_path, $mode = 0777, true, true);
                }
                if (isset($filename5)) {
                    $order->port_charge_invoice = $destination_path . $filename5;
                    $request->file('port_charge_invoice')->move($destination_path, $filename5);
                    $order->save();
                }

            }
            $data = $request->all();
            $data['port_charge_invoice']    = !empty($filename5) ? $filename5       : $order->port_charge_invoice;
            $data['delivery_order']         = !empty($filename4) ? $filename4       : $order->delivery_order;
            $data['bl_doc']                 = !empty($filename3) ? $filename3       : $order->bl_doc;
            $data['package_list_doc']       = !empty($filename2) ? $filename2       : $order->package_list_doc;
            $data['invoice_order']          = !empty($filename6) ? $filename6       : $order->invoice_order;
            $order->update($data);
            $track_id = $order->id;

            if($order->update($data))

                $user_id        = Auth::id();
            $count = count($request->get('cont_no'));
            $get_lading_id   = Ladding::wheretrack_id($track_id)->first()->id;

            $bill = Ladding::findOrFail($get_lading_id);
            $data = $request->all();

            if ($bill->update($data))

                $container = Container::where('ladding_id', $get_lading_id)->first();

            $xxx = (array_combine($request['cont_no_id'], $request->get('cont_no')));

            foreach ($xxx as $key => $value){
                $contain = Container::find($key);
                $contain->cont_no = $value;

                $contain->save();

            }
            for ($i=0; $i<$count; $i++) {
                $container_data[] = array('cont_no' => $request->input('cont_no')[$i],
                    'user_id' => $user_id,
                    'ladding_id' => $get_lading_id);
            }

            Session::flash('updated', 'Order Updated successfully');
            return redirect('/all_order');
        }

        else   if($user->hasRole('sales'))
        {
            $user_id = Auth::id();
            $request->merge(['user_id' => $user_id]);
            $order = Track::findOrFail($id);
            $data = $request->all();
            $track_id = $order->id;

            if($order->update($data))
                $user_id        = Auth::id();
            $count = count($request->get('cont_no'));
            $get_lading_id   = Ladding::wheretrack_id($track_id)->first()->id;

            $bill = Ladding::findOrFail($get_lading_id);
            $data = $request->all();

            if ($bill->update($data))
                $container = Container::where('ladding_id', $get_lading_id)->first();

            $xxx = (array_combine($request['cont_no_id'], $request->get('cont_no')));

            foreach ($xxx as $key => $value){
                $contain = Container::find($key);
                $contain->cont_no = $value;

                $contain->save();

            }
            for ($i=0; $i<$count; $i++) {
                $container_data[] = array('cont_no' => $request->input('cont_no')[$i],
                    'user_id' => $user_id,
                    'ladding_id' => $get_lading_id);
            }

            Session::flash('updated', 'Order created successfully');
            return redirect('/all_order');
        }
        elseif($user->hasRole('agent'))
        {
            $contract_doc           = $request->file('delivery_order');
            $port_charge_invoice    = $request->file('port_charge_invoice');
            $bl_doc                 = $request->file('bl_doc');
            $package_list_doc       = $request->file('package_list_doc');
            $invoice_order          = $request->file('invoice_order');

            $order = Track::findOrFail($id);

            if (isset($invoice_order)) {
                if (isset($invoice_order)) {
                    $extension1 = $request->file('invoice_order')->getClientOriginalExtension();
                }
                $sha = 'invoice_order' . md5(time());
                if (isset($extension1)) {
                    $filename6 = date('Y') . $sha . "sgfs." . $extension1;
                }
                $destination_path = 'OrderDoc/';
                if (empty($destination_path)) {
                    File::makeDirectory($destination_path, 0775, true, true);
                }
                if (isset($filename6)) {
                    $order->invoice_order = $destination_path . $filename6;
                    $request->file('invoice_order')->move($destination_path, $filename6);
                    $order->save();
                }
            }

            if (isset($package_list_doc)) {
                if (isset($package_list_doc)) {
                    $extension1 = $request->file('package_list_doc')->getClientOriginalExtension();
                }
                $sha = 'package_list_doc' . md5(time());
                if (isset($extension1)) {
                    $filename2 = date('Y') . $sha . "sgfs." . $extension1;
                }
                $destination_path = 'OrderDoc/';
                if (empty($destination_path)) {
                    File::makeDirectory($destination_path, 0775, true, true);
                    // File::makeDirectory($destination_path, $mode = 0777, true, true);
                }
                if (isset($filename2)) {
                    $order->package_list_doc = $destination_path . $filename2;
                    $request->file('package_list_doc')->move($destination_path, $filename2);
                    $order->save();
                }

            }

            if (isset($bl_doc)) {
                if (isset($bl_doc)) {
                    $extension1 = $request->file('bl_doc')->getClientOriginalExtension();
                }
                $sha = 'bl_doc' . md5(time());
                if (isset($extension1)) {
                    $filename3 = date('Y') . $sha . "sgfs." . $extension1;
                }
                $destination_path = 'OrderDoc/';
                if (empty($destination_path)) {
                    File::makeDirectory($destination_path, 0775, true, true);
                    // File::makeDirectory($destination_path, $mode = 0777, true, true);
                }
                if (isset($filename3)) {
                    $order->bl_doc = $destination_path . $filename3;
                    $request->file('bl_doc')->move($destination_path, $filename3);
                    $order->save();
                }

            }


            if (isset($contract_doc)) {
                if (isset($contract_doc)) {
                    $extension1 = $request->file('delivery_order')->getClientOriginalExtension();
                }
                $sha = 'rec_file' . md5(time());
                if (isset($extension1)) {
                    $filename4 = date('Y') . $sha . "sgfs." . $extension1;
                }
                $destination_path = 'DeliveryOrder/';
                if (empty($destination_path)) {
                    File::makeDirectory($destination_path, 0775, true, true);
                    // File::makeDirectory($destination_path, $mode = 0777, true, true);
                }
                if (isset($filename4)) {
                    $order->delivery_order = $destination_path . $filename4;
                    $request->file('delivery_order')->move($destination_path, $filename4);
                    $order->save();
                }

            }

            if (isset($port_charge_invoice)) {
                if (isset($port_charge_invoice)) {
                    $extension1 = $request->file('port_charge_invoice')->getClientOriginalExtension();
                }
                $sha = 'rec_file' . md5(time());
                if (isset($extension1)) {
                    $filename5 = date('Y') . $sha . "sgfs." . $extension1;
                }
                $destination_path = 'PortCharges/';
                if (empty($destination_path)) {
                    File::makeDirectory($destination_path, 0775, true, true);
                    // File::makeDirectory($destination_path, $mode = 0777, true, true);
                }
                if (isset($filename5)) {
                    $order->port_charge_invoice = $destination_path . $filename5;
                    $request->file('port_charge_invoice')->move($destination_path, $filename5);
                    $order->save();
                }

            }
            $data = $request->all();
            $data['port_charge_invoice']    = !empty($filename5) ? $filename5       : $order->port_charge_invoice;
            $data['delivery_order']         = !empty($filename4) ? $filename4       : $order->delivery_order;
            $data['bl_doc']                 = !empty($filename3) ? $filename3       : $order->bl_doc;
            $data['package_list_doc']       = !empty($filename2) ? $filename2       : $order->package_list_doc;
            $data['invoice_order']          = !empty($filename6) ? $filename6       : $order->invoice_order;
            $order->update($data);
            $track_id = $order->id;
            $order->update($data);


            Session::flash('updated', 'Order created successfully');
            return redirect('/my_orders');
        }
        else
        {
            return  JsonResponse::create(['error' => 'access-denied'],401);
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy($id)
    {
        $area =Track::findOrfail($id);
        $area->lad()->delete();
        $area->delete();
        Session::flash('delete', 'Order Deleted successfully');
        return back();
    }
}
