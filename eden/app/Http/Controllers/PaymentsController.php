<?php

namespace App\Http\Controllers;

use App\Archive;
use App\Clearance;
use App\Ladding;
use App\Payment_doc;
use Auth;
use DB;
use Illuminate\Support\Facades\File;

use Session;
use Illuminate\Http\Request;

class PaymentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user   =Auth::user();
        if($user->hasRole('sales'))
        {
            $ladding  = Ladding::all();
            $archive  = Payment_doc::select('payment_docs.*','bill_lading')
                        ->join('laddings','laddings.id','=','payment_docs.ladding_id')
                        ->join('tracks','tracks.id','=','laddings.track_id')
                        ->get();

            $expenses = Expenses::with('container')->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
            $normal = NormalExpenses::where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();


            return view('staff.payments_doc',compact('normal','expenses','archive','ladding'));
        }
        else
        {
            return JsonResponse::create(['error' => 'access-denied'],401);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user_id = Auth::id();
        $request->merge(['user_id' => $user_id]);
        $order =  Payment_doc::create($request->all());
        $invoice_doc                = $request->file('invoice_doc');
        $shipping_line_charges      = $request->file('delivery_note');


        if(isset($invoice_doc)) {
            if (isset($invoice_doc)) {
                $extension1 = $request->file('invoice_doc')->getClientOriginalExtension();
            }
            $sha = 'invoice_doc' . md5(time());
            if(isset($extension1)){
                $filename1 = date('Y').$sha . "sgfs." . $extension1;
            }
            $destination_path = 'payment_doc/';
            if (empty($invoice_doc)) {
                File::makeDirectory($invoice_doc, 0775, true, true);
                // File::makeDirectory($destination_path, $mode = 0777, true, true);
            }
            if(isset($filename1)){
                $order->invoice_doc =  $filename1;
                $request->file('invoice_doc')->move($destination_path, $filename1);
                $order->save();
            }

        }
        if(isset($shipping_line_charges)) {
            if (isset($shipping_line_charges)) {
                $shipping_line_ex = $request->file('delivery_note')->getClientOriginalExtension();
            }
            $sha = 'delivery_note' . md5(time().date("y"));
            if(isset($shipping_line_charges)){
                $filename5 = date('Y').$sha . "fghjm." . $shipping_line_ex;
            }
            $shipping_line_path = 'payment_doc/';
            if (empty($shipping_line_charges)) {
                File::makeDirectory($shipping_line_charges, 0775, true, true);
            }
            if(isset($filename5)){
                $order->delivery_note = $filename5;
                $request->file('delivery_note')->move($shipping_line_path, $filename5);
                $order->save();
            }
        }

        $all_data =  $request->all();

        $all_data['invoice_doc']      = !empty($filename1) ? $filename1                : $order->invoice_doc;
        $all_data['delivery_note']      = !empty($filename5) ? $filename5                : $order->delivery_note;

        $order->update($all_data);
        Session::flash('message', 'Document  Uploaded Successful  ');
        return  redirect('/payments_app');
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user_id = Auth::id();
        $request->merge(['user_id' => $user_id]);
        $order =  Payment_doc::findOrFail($id);


        $invoice_doc                = $request->file('invoice_doc');
        $shipping_line_charges      = $request->file('delivery_note');


        if(isset($invoice_doc)) {
            if (isset($invoice_doc)) {
                $extension1 = $request->file('invoice_doc')->getClientOriginalExtension();
            }
            $sha = 'invoice_doc' . md5(time());
            if(isset($extension1)){
                $filename1 = date('Y').$sha . "sgfs." . $extension1;
            }
            $destination_path = 'payment_doc/';
            if (empty($invoice_doc)) {
                File::makeDirectory($invoice_doc, 0775, true, true);
                // File::makeDirectory($destination_path, $mode = 0777, true, true);
            }
            if(isset($filename1)){
                $order->invoice_doc =  $filename1;
                $request->file('invoice_doc')->move($destination_path, $filename1);
                $order->save();
            }

        }
        if(isset($shipping_line_charges)) {
            if (isset($shipping_line_charges)) {
                $shipping_line_ex = $request->file('delivery_note')->getClientOriginalExtension();
            }
            $sha = 'delivery_note' . md5(time().date("y"));
            if(isset($shipping_line_charges)){
                $filename5 = date('Y').$sha . "fghjm." . $shipping_line_ex;
            }
            $shipping_line_path = 'payment_doc/';
            if (empty($shipping_line_charges)) {
                File::makeDirectory($shipping_line_charges, 0775, true, true);
            }
            if(isset($filename5)){
                $order->delivery_note = $filename5;
                $request->file('delivery_note')->move($shipping_line_path, $filename5);
                $order->save();
            }
        }

        $all_data =  $request->all();

        $all_data['invoice_doc']      = !empty($filename1) ? $filename1                : $order->invoice_doc;
        $all_data['delivery_note']      = !empty($filename5) ? $filename5                : $order->delivery_note;

        $order->update($all_data);
        Session::flash('message', 'Document  Uploaded Successful  ');
        return  redirect('/payments_app');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $training =Payment_doc::findOrfail($id);
        $training->delete();

        Session::flash('delete', 'Documents  Deleted  Successful ');
        return redirect('/clearance');
    }
}