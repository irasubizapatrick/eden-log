<?php

namespace App\Http\Controllers;

use App\Country;
use App\Destination;
use App\Expenses;
use App\NormalExpenses;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Session;

class CountryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user   =Auth::user();
        if($user->hasRole('admin'))
        {
            $expenses = Expenses::with('container')->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
            $normal = NormalExpenses::where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
            $destination = Country::all();
            return view('destination.country_admin',compact('destination','expenses','normal'));
        }
        else if($user->hasRole('sales'))
        {
            $expenses = Expenses::with('container')->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
            $normal = NormalExpenses::where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
            $destination = Country::all();
            return view('destination.country_sales',compact('destination','normal','expenses'));
        }
        else if($user->hasRole('documentation'))
        {
            $expenses = Expenses::with('container')->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
            $normal = NormalExpenses::where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
            $destination = Country::all();
            return view('destination.country_documentation',compact('destination','normal','expenses'));
        }
        else if($user->hasRole('declaration'))
        {
            $expenses = Expenses::with('container')->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
            $normal = NormalExpenses::where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
            $destination = Country::all();
            return view('destination.country_declaration',compact('destination','normal','expenses'));
        }
        else if($user->hasRole('shipping'))
        {
            $expenses = Expenses::with('container')->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
            $normal = NormalExpenses::where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
            $destination = Country::all();
            return view('destination.country_shipping',compact('destination','expenses','normal'));
        }
        elseif($user->hasRole('agent'))
        {
            $expenses = Expenses::with('container')->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
            $normal = NormalExpenses::where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
            $destination = Country::all();
            return view('destination.country_agent',compact('destination','normal','expenses'));
        }
        else
        {
            return JsonResponse::create(['error' => 'access-denied'],401);
        }
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {

        $status =  Country::create($request->all());
        $status->save();
        Session::flash('message', 'Destination Country created Successful');
        return redirect('/manage/destination/country');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $department = Country::findOrFail($id);
        $data = $request->all();
        $department->update($data);
        Session::flash('message', 'Destination Country Updated  Successful ');
        return redirect('/manage/destination/country');
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Exception
     */
    public function destroy($id)
    {
        $department  =Country::findOrfail($id);
        $department->delete();
        Session::flash('message', 'Destination  Country Deleted  Successful ');
        return redirect('/manage/destination/country');
    }
}
