<?php

namespace App\Http\Controllers;

use App\Container;
use App\Trucking;
use Illuminate\Http\Request;

class CmsController extends Controller
{
    /**
     *
     */
    public function index()
    {
        //
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function track_page_results(Request $request)
    {
        $truck  =  $request->input('track');
        $container = $request->get('cont_no');

        if(($truck != "") && $container !='')
        {

            $track = Trucking::select('truckings.*','bill_lading','cont_no')
                ->join('laddings', 'laddings.id', '=', 'truckings.ladding_id')
                ->join('containers','containers.id','=','truckings.cont_id')
                ->where('laddings.bill_lading', '=', $truck)
                ->where('containers.cont_no','=',$container)
                ->orderBy('created_at', 'desc')
                ->get();

            $cont = Container::select('containers.*')
                ->join('laddings', 'laddings.id', '=', 'containers.ladding_id')
                ->where('containers.cont_no', '=', $container)
                ->get();

            return view('tracking_res',compact('track','cont'));
        }
        elseif(( $truck !='' ||$container == ""))
        {
            $track = Trucking::select('truckings.*','bill_lading')
                ->join('laddings', 'laddings.id', '=', 'truckings.ladding_id')
                ->where('laddings.bill_lading', '=', $truck)
                ->orderBy('created_at', 'desc')
                ->get();

            $cont = Container::select('containers.*')
                ->join('laddings', 'laddings.id', '=', 'containers.ladding_id')
                ->where('laddings.bill_lading', '=', $truck)
                ->get();

            return view('track_page_res',compact('track','cont'));
        }
        elseif (( $truck =='' ||$container != ""))
        {
            $track = Trucking::select('truckings.*','bill_lading','cont_no')
                ->join('laddings', 'laddings.id', '=', 'truckings.ladding_id')
                ->join('containers','containers.id','=','truckings.cont_id')
                ->where('containers.cont_no','=',$container)
                ->orderBy('created_at', 'desc')
                ->get();


            $cont = Container::select('containers.*')
                ->join('laddings', 'laddings.id', '=', 'containers.ladding_id')
                ->where('containers.cont_no', '=', $container)
                ->get();

            return view('track_page_res',compact('track','cont'));
        }

        else
        {
            return view('track_page');
        }

    }
    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */

    public function tracking_cargo(Request $request)
    {
        $truck  =  $request->input('track');
        $container = $request->get('cont_no');

        if(($truck != "") && $container !='')
        {

            $track = Trucking::select('truckings.*','bill_lading','cont_no')
                            ->join('laddings', 'laddings.id', '=', 'truckings.ladding_id')
                            ->join('containers','containers.id','=','truckings.cont_id')
                            ->where('laddings.bill_lading', '=', $truck)
                            ->where('containers.cont_no','=',$container)
                            ->orderBy('created_at', 'desc')
                            ->get();

            $cont = Container::select('containers.*')
                            ->join('laddings', 'laddings.id', '=', 'containers.ladding_id')
                            ->where('containers.cont_no', '=', $container)
                            ->get();

            return view('tracking_res',compact('track','cont'));
        }
        elseif(( $truck !='' ||$container == ""))
        {
            $track = Trucking::select('truckings.*','bill_lading')
                        ->join('laddings', 'laddings.id', '=', 'truckings.ladding_id')
                        ->where('laddings.bill_lading', '=', $truck)
                        ->orderBy('created_at', 'desc')
                        ->get();

            $cont = Container::select('containers.*')
                            ->join('laddings', 'laddings.id', '=', 'containers.ladding_id')
                            ->where('laddings.bill_lading', '=', $truck)
                            ->get();

            return view('tracking_res',compact('track','cont'));
        }
        elseif (( $truck =='' ||$container != ""))
        {
            $track = Trucking::select('truckings.*','bill_lading','cont_no')
                            ->join('laddings', 'laddings.id', '=', 'truckings.ladding_id')
                            ->join('containers','containers.id','=','truckings.cont_id')
                            ->where('containers.cont_no','=',$container)
                            ->orderBy('created_at', 'desc')
                            ->get();


            $cont = Container::select('containers.*')
                        ->join('laddings', 'laddings.id', '=', 'containers.ladding_id')
                        ->where('containers.cont_no', '=', $container)
                        ->get();

            return view('tracking_res',compact('track','cont'));
        }

        else
        {
            return view('tracking');
        }

    }
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public  function about_us()
    {
        return view('about_us');
    }
/**
 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
 */
    public  function our_service()
    {
        return view('services');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public  function tracking()
    {
            return view('tracking');
    }


    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function track_page()
    {
        return view('track_page');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public  function blog()
    {
        return view('blog');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public  function contact_us()
    {
        return view('contact_us');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
