<?php

namespace App\Http\Controllers;

use App\Agent;
use App\Expenses;
use App\NormalExpenses;
use App\Sales;
use App\Track;
use App\Trucking;
use Carbon\Carbon;
use App\User;
use Hash;
use Illuminate\Support\Facades\DB;
use Session;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use Tymon\JWTAuth\Facades\JWTAuth;

class DashboardController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * Dashboard function for all users
     */
    public function index()
    {
        $user   =Auth::user();
        if($user->hasRole('admin')) {

            $agent  =Agent::all();

            $agents         = Agent::all();
            $agent_count    =Agent::count();


            $from = '2021';
            $to = '2030';
            $order  = Track::whereBetween(DB::raw('YEAR(tracks.created_at)'), [$from, $to])->get();

            $order_count = Track::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->get();

            $order_count    =Track::where( DB::raw('YEAR(created_at)'), '=', '2021')->count();

            $expense_count= Track::whereDate('tracks.created_at', '>=', Carbon::now()->subDays(30))->count();

            $track   = Track::select('tracks.*','bill_lading','organization_name')
                ->join('laddings','laddings.track_id','=','tracks.id')
                ->join('agents','agents.id','=','tracks.agent_id')
                ->whereBetween(DB::raw('YEAR(tracks.created_at)'), [$from, $to])
                ->take(10)
                ->latest('tracks.created_at')
                ->get();


            $track_received_doc = Track::select(DB::raw('count(tracks.id) as bar_received'))
                                    ->where('current_status','LIKE','Receiving the documents')
                                    ->whereBetween(DB::raw('YEAR(tracks.created_at)'), [$from, $to])
                                    ->get()->toArray();
            $bar_received = array_column($track_received_doc, 'bar_received');


            $track_under_line_doc = Track::select(DB::raw('count(tracks.id) as bar_under'))
                ->where('current_status','LIKE','Under shipping line procedures')
                ->whereBetween(DB::raw('YEAR(tracks.created_at)'), [$from, $to])
                ->get()->toArray();
            $bar_under_line = array_column($track_under_line_doc, 'bar_under');



            $bar_customs = Track::select(DB::raw('count(tracks.id) as bar_customs'))
                ->where('current_status','LIKE','Under customs procedures')
                ->whereBetween(DB::raw('YEAR(tracks.created_at)'), [$from, $to])
                ->value('bar_customs');


            $track_bar_releases_doc = Track::select(DB::raw('count(tracks.id) as bar_releases'))
                ->where('current_status','LIKE','Release Order, Delivery Order Received')
                ->whereBetween(DB::raw('YEAR(tracks.created_at)'), [$from, $to])
                ->get()->toArray();
            $bar_release = array_column($track_bar_releases_doc, 'bar_releases');




            $file_closed = Track::select(DB::raw('count(tracks.id) as file_closed'))
                ->where('current_status','LIKE','File Closed')
                ->whereBetween(DB::raw('YEAR(tracks.created_at)'), [$from, $to])
                ->value('file_closed');

            $bar_t1 = Track::select(DB::raw('count(tracks.id) as bar_t1'))
                ->where('current_status','LIKE','Prepare T1. / IM4 Documents')
                ->whereBetween(DB::raw('YEAR(tracks.created_at)'), [$from, $to])
                ->value('bar_t1');

            $track_port_doc = Track::select(DB::raw('count(tracks.id) as bar_port'))
                ->where('current_status','LIKE','Lodge Port Charges')
                ->whereBetween(DB::raw('YEAR(tracks.created_at)'), [$from, $to])
                ->get()->toArray();
            $bar_port = array_column($track_port_doc, 'bar_port');

            $track_pay_port_doc = Track::select(DB::raw('count(tracks.id) as bar_pay_port'))
                ->where('current_status','LIKE','Pay Port Charges')
                ->whereBetween(DB::raw('YEAR(tracks.created_at)'), [$from, $to])
                ->get()->toArray();
            $bar_pay_port = array_column($track_pay_port_doc, 'bar_pay_port');


            $collum_loading = Track::select(DB::raw('count(tracks.id) as collumn_loading'))
                                ->where('current_status','LIKE','Loading')
                                ->whereBetween(DB::raw('YEAR(tracks.created_at)'), [$from, $to])
                                ->get()->toArray();
            $row_collumn = array_column($collum_loading, 'collumn_loading');


            $bar_loading = Track::select(DB::raw('count(tracks.id) as bar_loading'))
                ->where('current_status','LIKE','Loading')
                ->whereBetween(DB::raw('YEAR(tracks.created_at)'), [$from, $to])
                ->value('bar_loading');


            $track_truck_doc = Track::select(DB::raw('count(tracks.id) as bar_truck'))
                ->where('current_status','LIKE','Truck/ Cargo in route Position')
                ->whereBetween(DB::raw('YEAR(tracks.created_at)'), [$from, $to])
                ->get()->toArray();
            $bar_truck = array_column($track_truck_doc, 'bar_truck');


            $bar_crossed = Track::select(DB::raw('count(tracks.id) as bar_crossed'))
                ->orWhere('current_status', 'like', 'Crossed Border')
                ->whereBetween(DB::raw('YEAR(tracks.created_at)'), [$from, $to])
                ->value('bar_crossed');

            $receiving_doc = Track::select(DB::raw('count(tracks.id) as receiving_doc'))
                            ->orWhere('current_status', 'like', '%' . 'Receiving the documents' . '%')
                            ->whereBetween(DB::raw('YEAR(tracks.created_at)'), [$from, $to])
                            ->value('receiving_doc');

            $track_bar_other_doc = Track::select(DB::raw('count(tracks.id) as bar_other'))
                ->where('current_status','LIKE','Other')
                ->whereBetween(DB::raw('YEAR(tracks.created_at)'), [$from, $to])
                ->get()->toArray();

            $bar_other = array_column($track_bar_other_doc, 'bar_other');


            $track_unpacking_doc = Track::select(DB::raw('count(tracks.id) as bar_unpacking'))
                ->where('current_status','LIKE','Unpacking')
                ->whereBetween(DB::raw('YEAR(tracks.created_at)'), [$from, $to])
                ->get()->toArray();

            $bar_unpacking = array_column($track_unpacking_doc, 'bar_unpacking');

            $bar_cargo_arrived = Track::select(DB::raw('count(tracks.id) as bar_cargo_arrived'))
                ->where('current_status','LIKE','Cargo arrived')
                ->whereBetween(DB::raw('YEAR(tracks.created_at)'), [$from, $to])
                ->value('bar_cargo_arrived');

            $bar_warehouse_doc = Track::select(DB::raw('count(tracks.id) as bar_warehouse'))
                ->where('current_status','LIKE','Cargo offloaded')
                ->whereBetween(DB::raw('YEAR(tracks.created_at)'), [$from, $to])
                ->get()->toArray();

            $bar_warehouse = array_column($bar_warehouse_doc, 'bar_warehouse');


            $bar_crossed_border = Track::select(DB::raw('count(tracks.id) as bar_warehouse'))
                                ->where('current_status','LIKE','Crossed_Border')
                                ->whereBetween(DB::raw('YEAR(tracks.created_at)'), [$from, $to])
                                ->get()->toArray();

            $bar_cross = array_column($bar_crossed_border, 'bar_warehouse');


            $track_bar_free = Track::select(DB::raw('count(tracks.id) as bar_free'))
                ->where('current_status','LIKE','Container Free')
                ->whereBetween(DB::raw('YEAR(tracks.created_at)'), [$from, $to])
                ->get()->toArray();

            $bar_free = array_column($track_bar_free, 'bar_free');

            $crossed_count =Track::where('current_status','LIKE','Crossed Border')
                ->whereBetween(DB::raw('YEAR(tracks.created_at)'), [$from, $to])
                ->count();

            $staff = Sales::whereBetween(DB::raw('YEAR(sales.created_at)'), [$from, $to])->count();

            $expenses = Expenses::with('container')
                ->where('user_id','=',Auth::id())
                ->where('finance_status','LIKE','pending')
                ->whereBetween(DB::raw('YEAR(expenses.created_at)'), [$from, $to])->count();
            $normal = NormalExpenses::where('user_id','=',Auth::id())
                ->where('finance_status','LIKE','pending')
                ->whereBetween(DB::raw('YEAR(normal_expenses.created_at)'), [$from, $to])
                ->count();


            $close_file   = DB::table('tracks')
                ->join('laddings','laddings.track_id','=','tracks.id')
                ->join('agents','agents.id','=','tracks.agent_id')
                ->select('organization_name','contact_person','telephone','country','address')
                ->selectRaw('count(tracks.id) as most')
                ->groupby('agents.id','organization_name','contact_person','telephone','country','address')
                ->orderBy('most', 'desc')
                ->whereNull('tracks.deleted_at')
                ->where('tracks.current_status', 'like', '%' . 'File Closed' . '%')
                ->whereBetween(DB::raw('YEAR(tracks.created_at)'), [$from, $to])
                ->get();

            $not_close_file   = DB::table('tracks')
                ->join('laddings','laddings.track_id','=','tracks.id')
                ->join('agents','agents.id','=','tracks.agent_id')
                ->select('organization_name','contact_person','telephone','country','address')
                ->selectRaw('count(tracks.id) as most')
                ->groupby('agents.id','organization_name','contact_person','telephone','country','address')
                ->orderBy('most', 'desc')
                ->whereNull('tracks.deleted_at')
                ->where('tracks.current_status', 'not like', '%' . 'File Closed' . '%')
                ->whereBetween(DB::raw('YEAR(tracks.created_at)'), [$from, $to])
                ->get();

            return view('dashboard.admin_dashboard',compact('bar_cross','row_collumn','receiving_doc','close_file','not_close_file','file_closed','normal','expenses','expense_count','crossed_count','staff','agents','agent','order','agent_count','order_count','track',
                'bar_free','bar_warehouse','bar_cargo_arrived','bar_unpacking','bar_crossed','bar_customs','bar_other','bar_pay_port','bar_truck','bar_t1','bar_release','bar_port','bar_pay_port','bar_loading','bar_received','bar_under_line'));

        }
        elseif($user->hasRole('finance')) {

            $agent  =Agent::all();

            $agents         = Agent::all();
            $agent_count    =Agent::count();


            $from = '2021';
            $to = '2030';
            $order  = Track::whereBetween(DB::raw('YEAR(tracks.created_at)'), [$from, $to])->get();

            $order_count = Track::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->get();

            $order_count    =Track::where( DB::raw('YEAR(created_at)'), '=', '2021')->count();

            $expense_count= Track::whereDate('tracks.created_at', '>=', Carbon::now()->subDays(30))->count();

            $track   = Track::select('tracks.*','bill_lading','organization_name')
                ->join('laddings','laddings.track_id','=','tracks.id')
                ->join('agents','agents.id','=','tracks.agent_id')
                ->whereBetween(DB::raw('YEAR(tracks.created_at)'), [$from, $to])
                ->take(10)
                ->latest('tracks.created_at')
                ->get();


            $track_received_doc = Track::select(DB::raw('count(tracks.id) as bar_received'))
                ->where('current_status','LIKE','Receiving the documents')
                ->whereBetween(DB::raw('YEAR(tracks.created_at)'), [$from, $to])
                ->get()->toArray();
            $bar_received = array_column($track_received_doc, 'bar_received');


            $track_under_line_doc = Track::select(DB::raw('count(tracks.id) as bar_under'))
                ->where('current_status','LIKE','Under shipping line procedures')
                ->whereBetween(DB::raw('YEAR(tracks.created_at)'), [$from, $to])
                ->get()->toArray();
            $bar_under_line = array_column($track_under_line_doc, 'bar_under');



            $bar_customs = Track::select(DB::raw('count(tracks.id) as bar_customs'))
                ->where('current_status','LIKE','Under customs procedures')
                ->whereBetween(DB::raw('YEAR(tracks.created_at)'), [$from, $to])
                ->value('bar_customs');


            $track_bar_releases_doc = Track::select(DB::raw('count(tracks.id) as bar_releases'))
                ->where('current_status','LIKE','Release Order, Delivery Order Received')
                ->whereBetween(DB::raw('YEAR(tracks.created_at)'), [$from, $to])
                ->get()->toArray();
            $bar_release = array_column($track_bar_releases_doc, 'bar_releases');




            $file_closed = Track::select(DB::raw('count(tracks.id) as file_closed'))
                ->where('current_status','LIKE','File Closed')
                ->whereBetween(DB::raw('YEAR(tracks.created_at)'), [$from, $to])
                ->value('file_closed');

            $bar_t1 = Track::select(DB::raw('count(tracks.id) as bar_t1'))
                ->where('current_status','LIKE','Prepare T1. / IM4 Documents')
                ->whereBetween(DB::raw('YEAR(tracks.created_at)'), [$from, $to])
                ->value('bar_t1');

            $track_port_doc = Track::select(DB::raw('count(tracks.id) as bar_port'))
                ->where('current_status','LIKE','Lodge Port Charges')
                ->whereBetween(DB::raw('YEAR(tracks.created_at)'), [$from, $to])
                ->get()->toArray();
            $bar_port = array_column($track_port_doc, 'bar_port');

            $track_pay_port_doc = Track::select(DB::raw('count(tracks.id) as bar_pay_port'))
                ->where('current_status','LIKE','Pay Port Charges')
                ->whereBetween(DB::raw('YEAR(tracks.created_at)'), [$from, $to])
                ->get()->toArray();
            $bar_pay_port = array_column($track_pay_port_doc, 'bar_pay_port');


            $collum_loading = Track::select(DB::raw('count(tracks.id) as collumn_loading'))
                ->where('current_status','LIKE','Loading')
                ->whereBetween(DB::raw('YEAR(tracks.created_at)'), [$from, $to])
                ->get()->toArray();
            $row_collumn = array_column($collum_loading, 'collumn_loading');


            $bar_loading = Track::select(DB::raw('count(tracks.id) as bar_loading'))
                ->where('current_status','LIKE','Loading')
                ->whereBetween(DB::raw('YEAR(tracks.created_at)'), [$from, $to])
                ->value('bar_loading');


            $track_truck_doc = Track::select(DB::raw('count(tracks.id) as bar_truck'))
                ->where('current_status','LIKE','Truck/ Cargo in route Position')
                ->whereBetween(DB::raw('YEAR(tracks.created_at)'), [$from, $to])
                ->get()->toArray();
            $bar_truck = array_column($track_truck_doc, 'bar_truck');


            $bar_crossed = Track::select(DB::raw('count(tracks.id) as bar_crossed'))
                ->orWhere('current_status', 'like', 'Crossed Border')
                ->whereBetween(DB::raw('YEAR(tracks.created_at)'), [$from, $to])
                ->value('bar_crossed');

            $receiving_doc = Track::select(DB::raw('count(tracks.id) as receiving_doc'))
                ->orWhere('current_status', 'like', '%' . 'Receiving the documents' . '%')
                ->whereBetween(DB::raw('YEAR(tracks.created_at)'), [$from, $to])
                ->value('receiving_doc');

            $track_bar_other_doc = Track::select(DB::raw('count(tracks.id) as bar_other'))
                ->where('current_status','LIKE','Other')
                ->whereBetween(DB::raw('YEAR(tracks.created_at)'), [$from, $to])
                ->get()->toArray();

            $bar_other = array_column($track_bar_other_doc, 'bar_other');


            $track_unpacking_doc = Track::select(DB::raw('count(tracks.id) as bar_unpacking'))
                ->where('current_status','LIKE','Unpacking')
                ->whereBetween(DB::raw('YEAR(tracks.created_at)'), [$from, $to])
                ->get()->toArray();

            $bar_unpacking = array_column($track_unpacking_doc, 'bar_unpacking');

            $bar_cargo_arrived = Track::select(DB::raw('count(tracks.id) as bar_cargo_arrived'))
                ->where('current_status','LIKE','Cargo arrived')
                ->whereBetween(DB::raw('YEAR(tracks.created_at)'), [$from, $to])
                ->value('bar_cargo_arrived');

            $bar_warehouse_doc = Track::select(DB::raw('count(tracks.id) as bar_warehouse'))
                ->where('current_status','LIKE','Cargo offloaded')
                ->whereBetween(DB::raw('YEAR(tracks.created_at)'), [$from, $to])
                ->get()->toArray();

            $bar_warehouse = array_column($bar_warehouse_doc, 'bar_warehouse');


            $bar_crossed_border = Track::select(DB::raw('count(tracks.id) as bar_warehouse'))
                ->where('current_status','LIKE','Crossed_Border')
                ->whereBetween(DB::raw('YEAR(tracks.created_at)'), [$from, $to])
                ->get()->toArray();

            $bar_cross = array_column($bar_crossed_border, 'bar_warehouse');


            $track_bar_free = Track::select(DB::raw('count(tracks.id) as bar_free'))
                ->where('current_status','LIKE','Container Free')
                ->whereBetween(DB::raw('YEAR(tracks.created_at)'), [$from, $to])
                ->get()->toArray();

            $bar_free = array_column($track_bar_free, 'bar_free');

            $crossed_count =Track::where('current_status','LIKE','Crossed Border')
                ->whereBetween(DB::raw('YEAR(tracks.created_at)'), [$from, $to])
                ->count();

            $staff = Sales::whereBetween(DB::raw('YEAR(sales.created_at)'), [$from, $to])->count();

            $expenses = Expenses::with('container')
                ->where('user_id','=',Auth::id())
                ->where('finance_status','LIKE','pending')
                ->whereBetween(DB::raw('YEAR(expenses.created_at)'), [$from, $to])->count();
            $normal = NormalExpenses::where('user_id','=',Auth::id())
                ->where('finance_status','LIKE','pending')
                ->whereBetween(DB::raw('YEAR(normal_expenses.created_at)'), [$from, $to])
                ->count();


            $close_file   = DB::table('tracks')
                ->join('laddings','laddings.track_id','=','tracks.id')
                ->join('agents','agents.id','=','tracks.agent_id')
                ->select('organization_name','contact_person','telephone','country','address')
                ->selectRaw('count(tracks.id) as most')
                ->groupby('agents.id','organization_name','contact_person','telephone','country','address')
                ->orderBy('most', 'desc')
                ->whereNull('tracks.deleted_at')
                ->where('tracks.current_status', 'like', '%' . 'File Closed' . '%')
                ->whereBetween(DB::raw('YEAR(tracks.created_at)'), [$from, $to])
                ->get();

            $not_close_file   = DB::table('tracks')
                ->join('laddings','laddings.track_id','=','tracks.id')
                ->join('agents','agents.id','=','tracks.agent_id')
                ->select('organization_name','contact_person','telephone','country','address')
                ->selectRaw('count(tracks.id) as most')
                ->groupby('agents.id','organization_name','contact_person','telephone','country','address')
                ->orderBy('most', 'desc')
                ->whereNull('tracks.deleted_at')
                ->where('tracks.current_status', 'not like', '%' . 'File Closed' . '%')
                ->whereBetween(DB::raw('YEAR(tracks.created_at)'), [$from, $to])
                ->get();
            return view('dashboard.finance_dashboard',compact('bar_cross','row_collumn','receiving_doc','close_file','not_close_file','file_closed','normal','expenses','expense_count','crossed_count','staff','agents','agent','order','agent_count','order_count','track',
                'bar_free','bar_warehouse','bar_cargo_arrived','bar_unpacking','bar_crossed','bar_customs','bar_other','bar_pay_port','bar_truck','bar_t1','bar_release','bar_port','bar_pay_port','bar_loading','bar_received','bar_under_line'));

        }

        elseif($user->hasRole('super')) {

            $collum_loading = Track::select(DB::raw('count(tracks.id) as collumn_loading'))
                ->where('current_status','LIKE','Loading')
                ->get()->toArray();
            $row_collumn = array_column($collum_loading, 'collumn_loading');


            $bar_crossed_border = Track::select(DB::raw('count(tracks.id) as bar_warehouse'))
                ->where('current_status','LIKE','Crossed_Border')
                ->get()->toArray();

            $bar_cross = array_column($bar_crossed_border, 'bar_warehouse');

            $receiving_doc = Track::select(DB::raw('count(tracks.id) as receiving_doc'))
                ->orWhere('current_status', 'like', '%' . 'Receiving the documents' . '%')
                ->value('receiving_doc');

            $agent  =Agent::all();
            $order  = Track::all();
            $expenses = Expenses::with('container')->where('finance_status','LIKE','pending')->count();

            $normal = NormalExpenses::where('finance_status','LIKE','pending')->count();

            $agents         = Agent::all();
            $agent_count    =Agent::count();
            $order_count    =Track::count();

            $track   = Track::select('tracks.*','bill_lading','organization_name')
                ->join('laddings','laddings.track_id','=','tracks.id')
                ->join('agents','agents.id','=','tracks.agent_id')
                ->take(10)
                ->latest('tracks.created_at')
                ->get();
            $expense_count= Track::whereDate('tracks.created_at', '>=', Carbon::now()->subDays(30))->count();

            $file_closed = Track::select(DB::raw('count(tracks.id) as file_closed'))
                ->where('current_status','LIKE','File Closed')
                ->value('file_closed');
            $track_received_doc = Track::select(DB::raw('count(tracks.id) as bar_received'))
                ->where('current_status','LIKE','Receiving the documents')
                ->get()->toArray();
            $bar_received = array_column($track_received_doc, 'bar_received');


            $track_under_line_doc = Track::select(DB::raw('count(tracks.id) as bar_under'))
                ->where('current_status','LIKE','Under shipping line procedures')
                ->get()->toArray();
            $bar_under_line = array_column($track_under_line_doc, 'bar_under');



            $bar_customs = Track::select(DB::raw('count(tracks.id) as bar_customs'))
                ->where('current_status','LIKE','Under Customs Procedures')
                ->value('bar_customs');


            $track_bar_releases_doc = Track::select(DB::raw('count(tracks.id) as bar_releases'))
                ->where('current_status','LIKE','Release Order, Delivery Order Received')
                ->get()->toArray();
            $bar_release = array_column($track_bar_releases_doc, 'bar_releases');




            $bar_t1 = Track::select(DB::raw('count(tracks.id) as bar_t1'))
                ->where('current_status','LIKE','Prepare for T1')
                ->value('bar_t1');

            $track_port_doc = Track::select(DB::raw('count(tracks.id) as bar_port'))
                ->where('current_status','LIKE','Lodge Port charges')
                ->get()->toArray();
            $bar_port = array_column($track_port_doc, 'bar_port');

            $track_pay_port_doc = Track::select(DB::raw('count(tracks.id) as bar_pay_port'))
                ->where('current_status','LIKE','Pay Port Charges')
                ->get()->toArray();
            $bar_pay_port = array_column($track_pay_port_doc, 'bar_pay_port');

            $bar_loading = Track::select(DB::raw('count(tracks.id) as bar_loading'))
                ->where('current_status','LIKE','Loading')
                ->value('bar_loading');


            $track_truck_doc = Track::select(DB::raw('count(tracks.id) as bar_truck'))
                ->where('current_status','LIKE','Truck/ Cargo in route Position')
                ->get()->toArray();
            $bar_truck = array_column($track_truck_doc, 'bar_truck');


            $bar_crossed = Track::select(DB::raw('count(tracks.id) as bar_crossed'))
                ->where('current_status','LIKE','Crossed Border')
                ->value('bar_crossed');


            $track_bar_other_doc = Track::select(DB::raw('count(tracks.id) as bar_other'))
                ->where('current_status','LIKE','Other')
                ->get()->toArray();

            $bar_other = array_column($track_bar_other_doc, 'bar_other');


            $track_unpacking_doc = Track::select(DB::raw('count(tracks.id) as bar_unpacking'))
                ->where('current_status','LIKE','Unpacking')
                ->get()->toArray();

            $bar_unpacking = array_column($track_unpacking_doc, 'bar_unpacking');

            $bar_cargo_arrived = Track::select(DB::raw('count(tracks.id) as bar_cargo_arrived'))
                ->where('current_status','LIKE','Cargo Arrived')
                ->value('bar_cargo_arrived');

            $bar_warehouse_doc = Track::select(DB::raw('count(tracks.id) as bar_warehouse'))
                ->where('current_status','LIKE','Unloaded ( Warehouse)')
                ->get()->toArray();

            $bar_warehouse = array_column($bar_warehouse_doc, 'bar_warehouse');

            $track_bar_free = Track::select(DB::raw('count(tracks.id) as bar_free'))
                ->where('current_status','LIKE','Container Free')
                ->get()->toArray();

            $bar_free = array_column($track_bar_free, 'bar_free');

            $crossed_count =Track::where('current_status','LIKE','Crossed Border')->count();

            $staff = Sales::count();



            $close_file   = DB::table('tracks')
                ->join('laddings','laddings.track_id','=','tracks.id')
                ->join('agents','agents.id','=','tracks.agent_id')
                ->select('organization_name','contact_person','telephone','country','address')
                ->selectRaw('count(tracks.id) as most')
                ->groupby('agents.id','organization_name','contact_person','telephone','country','address')
                ->orderBy('most', 'desc')
                ->whereNull('tracks.deleted_at')
                ->where('tracks.current_status', 'like', '%' . 'File Closed' . '%')
                ->get();

            $not_close_file   = DB::table('tracks')
                ->join('laddings','laddings.track_id','=','tracks.id')
                ->join('agents','agents.id','=','tracks.agent_id')
                ->select('organization_name','contact_person','telephone','country','address')
                ->selectRaw('count(tracks.id) as most')
                ->groupby('agents.id','organization_name','contact_person','telephone','country','address')
                ->orderBy('most', 'desc')
                ->whereNull('tracks.deleted_at')
                ->where('tracks.current_status', 'not like', '%' . 'File Closed' . '%')
                ->get();
            return view('dashboard.super_dashboard',compact('receiving_doc','bar_cross','row_collumn','close_file','not_close_file','file_closed','normal','expenses','expense_count','crossed_count','staff','agents','agent','order','agent_count','order_count','track',
                'bar_free','bar_warehouse','bar_cargo_arrived','bar_unpacking','bar_crossed','bar_customs','bar_other','bar_pay_port','bar_truck','bar_t1','bar_release','bar_port','bar_pay_port','bar_loading','bar_received','bar_under_line'));

        }
        elseif ($user->hasRole('agent')){

            $from = '2021';
            $to = '2030';
            $user_id = Auth::id();
            $agent  =Agent::all();
            $order  = Track::all();
            $profile = User::where('id','=',$user_id)->get();
            $name = Agent::where('user_id','=',$user_id)->first()->organization_name;

            $agent_count    =Agent::select(DB::raw('count(id) as agent_count'))
                ->value('agent_count');

            $order_count    =Track::select(DB::raw('count(tracks.id) as track_count'))
                                ->join('laddings','laddings.track_id','=','tracks.id')
                                ->join('agents','agents.id','=','tracks.agent_id')
                                ->where('agents.user_id','=',$user_id)
                                ->whereBetween(DB::raw('YEAR(tracks.created_at)'),[$from, $to])
                                ->value('track_count');

            $user_id =Auth::id();

            $track   = Track::select('tracks.*','bill_lading','organization_name')
                        ->join('laddings','laddings.track_id','=','tracks.id')
                        ->join('agents','agents.id','=','tracks.agent_id')
                        ->where('agents.user_id','=',$user_id)
                        ->whereBetween(DB::raw('YEAR(tracks.created_at)'),[$from, $to])
                        ->take(10)
                        ->latest('tracks.created_at')
                        ->get();
            $track_t1   = Track::select('tracks.*','bill_lading','organization_name')
                        ->join('laddings','laddings.track_id','=','tracks.id')
                        ->join('agents','agents.id','=','tracks.agent_id')
                        ->where('agents.user_id','=',$user_id)
                        ->where('current_status','LIKE','Prepare for T1')
                        ->whereBetween(DB::raw('YEAR(tracks.created_at)'),[$from, $to])
                        ->take(10)
                        ->latest('tracks.created_at')
                        ->get();

            $track_loading   = Track::select('tracks.*','bill_lading','organization_name')
                                ->join('laddings','laddings.track_id','=','tracks.id')
                                ->join('agents','agents.id','=','tracks.agent_id')
                                ->whereBetween(DB::raw('YEAR(tracks.created_at)'),[$from, $to])
                                ->where('agents.user_id','=',$user_id)
                                ->where('current_status','LIKE','Loading')
                                ->latest('tracks.created_at')
                                ->get();
            $track_cargo   = Track::select('tracks.*','bill_lading','organization_name')
                                ->join('laddings','laddings.track_id','=','tracks.id')
                                ->join('agents','agents.id','=','tracks.agent_id')
                                ->whereBetween(DB::raw('YEAR(tracks.created_at)'),[$from, $to])
                                ->where('agents.user_id','=',$user_id)
                                ->where('current_status','LIKE','Crossed Border')
                                ->take(10)
                                ->latest('tracks.created_at')
                                ->get();

            $track_cargo_count   = Track::select('tracks.*','bill_lading','organization_name')
                                    ->join('laddings','laddings.track_id','=','tracks.id')
                                    ->join('agents','agents.id','=','tracks.agent_id')
                                    ->whereBetween(DB::raw('YEAR(tracks.created_at)'),[$from, $to])
                                    ->where('agents.user_id','=',$user_id)
                                    ->where('current_status','LIKE','Crossed Border')
                                    ->latest('tracks.created_at')
                                    ->count();

            $track_loading_count   = Track::select('tracks.*','bill_lading','organization_name')
                                        ->join('laddings','laddings.track_id','=','tracks.id')
                                        ->join('agents','agents.id','=','tracks.agent_id')
                                        ->whereBetween(DB::raw('YEAR(tracks.created_at)'),[$from, $to])
                                        ->where('agents.user_id','=',$user_id)
                                        ->where('current_status','LIKE','Loading')
                                        ->latest('tracks.created_at')
                                        ->count();


            $crossed_count = $track->where('current_status','LIKE','Crossed Border')->count();
            $order_loading_count = $track->where('current_status','LIKE','Loading')->count();
            $prepare_t1_count = $track->where('current_status','LIKE','Prepare for T1')->count();

            return view('dashboard.agent_dashboard',compact('track_loading_count','track_t1','track_cargo_count','track_loading','track_cargo','prepare_t1_count','order_loading_count','crossed_count','profile','agent','order','agent_count','order_count','track','name'));

        }
        elseif ($user->hasRole('sales'))
        {

            $from = '2021';
            $to = '2030';
            $agent_count    =Agent::select(DB::raw('count(id) as agent_count'))
                ->whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])
                ->value('agent_count');

            $order_count    =Track::select(DB::raw('count(id) as track_count'))
                ->whereBetween(DB::raw('YEAR(tracks.created_at)'),[$from, $to])
                ->value('track_count');

            $track   = Track::select('tracks.*','bill_lading','organization_name')
                ->join('laddings','laddings.track_id','=','tracks.id')
                ->join('agents','agents.id','=','tracks.agent_id')
                ->whereBetween(DB::raw('YEAR(tracks.created_at)'),[$from, $to])
                ->take(10)
                ->latest('tracks.created_at')
                ->get();
            $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
            $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

            return  view('dashboard.sales_dashboard',compact('normal','expenses','track','order_count','agent_count'));

        }
        elseif ($user->hasRole('documentation'))
        {

            $from = '2021';
            $to = '2030';
            $agent_count    =Agent::select(DB::raw('count(id) as agent_count'))
                ->whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])
                ->value('agent_count');

            $order_count    =Track::select(DB::raw('count(id) as track_count'))
                ->whereBetween(DB::raw('YEAR(tracks.created_at)'),[$from, $to])
                ->value('track_count');

            $track   = Track::select('tracks.*','bill_lading','organization_name')
                ->join('laddings','laddings.track_id','=','tracks.id')
                ->join('agents','agents.id','=','tracks.agent_id')
                ->whereBetween(DB::raw('YEAR(tracks.created_at)'),[$from, $to])
                ->take(10)
                ->latest('tracks.created_at')
                ->get();
            $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
            $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

            return  view('dashboard.documentation_dashboard',compact('normal','expenses','track','order_count','agent_count'));
        }
        elseif ($user->hasRole('declaration'))
        {

            $from = '2021';
            $to = '2030';
            $agent_count    =Agent::select(DB::raw('count(id) as agent_count'))
                ->whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])
                ->value('agent_count');

            $order_count    =Track::select(DB::raw('count(id) as track_count'))
                ->whereBetween(DB::raw('YEAR(tracks.created_at)'),[$from, $to])
                ->value('track_count');

            $track   = Track::select('tracks.*','bill_lading','organization_name')
                ->join('laddings','laddings.track_id','=','tracks.id')
                ->join('agents','agents.id','=','tracks.agent_id')
                ->whereBetween(DB::raw('YEAR(tracks.created_at)'),[$from, $to])
                ->take(10)
                ->latest('tracks.created_at')
                ->get();
            $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
            $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

            return  view('dashboard.declaration_dashboard',compact('normal','expenses','track','order_count','agent_count'));
        }
        elseif ($user->hasRole('shipping'))
        {

            $from = '2021';
            $to = '2030';
            $agent_count    =Agent::select(DB::raw('count(id) as agent_count'))
                ->whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])
                ->value('agent_count');

            $order_count    =Track::select(DB::raw('count(id) as track_count'))
                ->whereBetween(DB::raw('YEAR(tracks.created_at)'),[$from, $to])
                ->value('track_count');

            $track   = Track::select('tracks.*','bill_lading','organization_name')
                ->join('laddings','laddings.track_id','=','tracks.id')
                ->join('agents','agents.id','=','tracks.agent_id')
                ->whereBetween(DB::raw('YEAR(tracks.created_at)'),[$from, $to])
                ->take(10)
                ->latest('tracks.created_at')
                ->get();
            $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
            $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

            return  view('dashboard.shipping_dashboard',compact('normal','expenses','track','order_count','agent_count'));
        }


        else
        {
            return JsonResponse::create(['error' => 'access-denied'],401);
        }

    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */

    public function CrossedBorder()
    {
        $user   =Auth::user();
        if($user->hasRole('admin')) {

            $agent  =Agent::all();
            $order  = Track::all();

            $agents         = Agent::all();
            $agent_count    =Agent::count();
            $order_count    =Track::count();


            $track   = Track::select('tracks.*','bill_lading','organization_name')
                ->join('laddings','laddings.track_id','=','tracks.id')
                ->join('agents','agents.id','=','tracks.agent_id')
                ->where('current_status','LIKE','Crossed Border')
                ->latest('tracks.created_at')
                ->get();

            $staff = Sales::count();
            $expenses = Expenses::with('container')->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

            $normal = NormalExpenses::where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

            return view('admin.crossed_border',compact('normal','expenses','staff','agents','agent','order','agent_count','order_count','track'));

        }
        elseif($user->hasRole('finance')) {

            $agent  =Agent::all();
            $order  = Track::all();

            $agents         = Agent::all();
            $agent_count    =Agent::count();
            $order_count    =Track::count();


            $track   = Track::select('tracks.*','bill_lading','organization_name')
                ->join('laddings','laddings.track_id','=','tracks.id')
                ->join('agents','agents.id','=','tracks.agent_id')
                ->where('current_status','LIKE','Crossed Border')
                ->latest('tracks.created_at')
                ->get();

            $staff = Sales::count();
            $expenses = Expenses::with('container')->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

            $normal = NormalExpenses::where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

            return view('finance.crossed_border',compact('normal','expenses','staff','agents','agent','order','agent_count','order_count','track'));

        }
        elseif($user->hasRole('super')) {

            $agent  =Agent::all();
            $order  = Track::all();

            $agents         = Agent::all();
            $agent_count    =Agent::count();
            $order_count    =Track::count();


            $track   = Track::select('tracks.*','bill_lading','organization_name')
                ->join('laddings','laddings.track_id','=','tracks.id')
                ->join('agents','agents.id','=','tracks.agent_id')
                ->where('current_status','LIKE','Crossed Border')
                ->latest('tracks.created_at')
                ->get();

            $staff = Sales::count();
            $expenses = Expenses::with('container')->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

            $normal = NormalExpenses::where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

            return view('admin.super_crossed_border',compact('normal','expenses','staff','agents','agent','order','agent_count','order_count','track'));

        }
        else
        {
            return JsonResponse::create(['error' => 'access-denied'],401);
        }

    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|static
     *
     * Change password
     */
    public function  change_pass()
    {
        $user   =Auth::user();
        if($user->hasRole('admin')) {

            $user_id    =Auth::id();
            $password   =User::select('users.*')
                ->where('id','=',$user_id)
                ->get();
            $expenses = Expenses::with('container')->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

            $normal = NormalExpenses::where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

            return view('auth.admin_reset_pass',compact('normal','expenses','password'));
        }
        elseif($user->hasRole('super')) {

            $user_id    =Auth::id();
            $password   =User::select('users.*')
                ->where('id','=',$user_id)
                ->get();
            $expenses = Expenses::with('container')->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

            $normal = NormalExpenses::where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

            return view('auth.super_reset_pass',compact('normal','expenses','password'));
        }
        elseif ($user->hasRole('sales'))
        {
            $user_id    =Auth::id();
            $password   =User::select('users.*')
                ->where('id','=',$user_id)
                ->get();
            $expenses = Expenses::with('container')->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

            $normal = NormalExpenses::where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

            return view('auth.sales_reset_pass',compact('normal','expenses','password'));
        }
        elseif ($user->hasRole('finance'))
        {
            $user_id    =Auth::id();
            $password   =User::select('users.*')
                ->where('id','=',$user_id)
                ->get();
            $expenses = Expenses::with('container')->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

            $normal = NormalExpenses::where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

            return view('auth.finance_reset_pass',compact('normal','expenses','password'));
        }
        elseif ($user->hasRole('documentation'))
        {
            $user_id    =Auth::id();
            $password   =User::select('users.*')
                ->where('id','=',$user_id)
                ->get();
            $expenses = Expenses::with('container')->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

            $normal = NormalExpenses::where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

            return view('auth.documentation_reset_pass',compact('normal','expenses','password'));
        }
        elseif ($user->hasRole('declaration'))
        {
            $user_id    =Auth::id();
            $password   =User::select('users.*')
                ->where('id','=',$user_id)
                ->get();
            $expenses = Expenses::with('container')->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

            $normal = NormalExpenses::where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

            return view('auth.declaration_reset_pass',compact('normal','expenses','password'));
        }
        elseif ($user->hasRole('shipping'))
        {
            $user_id    =Auth::id();
            $password   =User::select('users.*')
                ->where('id','=',$user_id)
                ->get();
            $expenses = Expenses::with('container')->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

            $normal = NormalExpenses::where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

            return view('auth.shipping_reset_pass',compact('normal','expenses','password'));
        }
        elseif ($user->hasRole('agent'))
        {

            $user_id    =Auth::id();
            $password   =User::select('users.*')
                ->where('id','=',$user_id)
                ->get();
            $expenses = Expenses::with('container')->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

            $normal = NormalExpenses::where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

            $profile = User::where('id','=',$user_id)->get();
            return view('auth.agent_reset_pass',compact('normal','expenses','password','profile'));

        }
        else
        {
            return JsonResponse::create(['error' => 'access-denied'],401);
        }
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update_password(Request $request, $id)
    {
        $user                         = User::find($id);
        $user->email                  = $request->get('email');
        $user->password               = Hash::make($request->get('password'));
        $user->update();
        Session::flash('updated', 'Password  Changed successfully');

        return redirect('/logout');
    }

    /**
     * @return static
     */
    public function track_t1()
    {
        $user_id = Auth::id();
        $user = Auth::user();
        if ($user->hasRole('agent'))
        {
            $profile = User::where('id','=',$user_id)->get();
            $track   = Track::select('tracks.*','bill_lading','organization_name')
                ->join('laddings','laddings.track_id','=','tracks.id')
                ->join('agents','agents.id','=','tracks.agent_id')
                ->where('agents.user_id','=',$user_id)
                ->where('current_status','LIKE','Prepare for T1')
                ->take(10)
                ->latest('tracks.created_at')
                ->get();

            return view('agent.track1',compact('track','profile'));

        } else
        {

            return JsonResponse::create(['error' => 'access-denied'], 401);
        }
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|static
     */
    public function track_loading()
    {
        $user_id = Auth::id();
        $user = Auth::user();
        if ($user->hasRole('agent'))
        {

            $profile = User::where('id','=',$user_id)->get();
            $track   = Track::select('tracks.*','bill_lading','organization_name')
                ->join('laddings','laddings.track_id','=','tracks.id')
                ->join('agents','agents.id','=','tracks.agent_id')
                ->where('agents.user_id','=',$user_id)
                ->where('current_status','LIKE','Loading')
                ->latest('tracks.created_at')
                ->get();


            return view('agent.track_loading',compact('track','profile'));

        } else
        {

            return JsonResponse::create(['error' => 'access-denied'], 401);
        }
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|static
     */
    public function track_cargo()
    {
        $user_id = Auth::id();
        $user = Auth::user();
        if ($user->hasRole('agent'))
        {

            $profile = User::where('id','=',$user_id)->get();
            $track   = Track::select('tracks.*','bill_lading','organization_name')
                ->join('laddings','laddings.track_id','=','tracks.id')
                ->join('agents','agents.id','=','tracks.agent_id')
                ->where('agents.user_id','=',$user_id)
                ->where('current_status','LIKE','Crossed Border')
                ->take(10)
                ->latest('tracks.created_at')
                ->get();

            return view('agent.track_cargo',compact('track','profile'));

        } else
        {

            return JsonResponse::create(['error' => 'access-denied'], 401);
        }
    }

    /**
     * @return JsonResponse
     */
    public function testNow()
    {
        $user = JWTAuth::parseToken()->authenticate();
        if($user->hasRole('admin')){

            $user = User::with('roles')->where('email','NOT LIKE','admin@log.com')->get();
            return response()->json($user);

        }
        else
        {
            return response()->json(['error' => 'access-denied'], 401);
        }

    }

    /**
     * @return JsonResponse
     */
    public function GetUser()
    {
        $user = JWTAuth::parseToken()->authenticate();

        if($user->hasRole('admin')){


            $staff = Sales::select('sales.*', 'display_name')
                ->join('users', 'users.id', '=', 'sales.user_id')
                ->join('role_user', 'role_user.user_id', '=', 'sales.user_id')
                ->join('roles', 'roles.id', '=', 'role_user.role_id')
                ->where('users.id','=',$user->id)->get();

            return response()->json(['user'=>$staff], 200);

        }
        elseif ($user->hasRole('super'))
        {

            $staff = Sales::select('sales.*', 'display_name')
                ->join('users', 'users.id', '=', 'sales.user_id')
                ->join('role_user', 'role_user.user_id', '=', 'sales.user_id')
                ->join('roles', 'roles.id', '=', 'role_user.role_id')
                ->where('users.id','=',$user->id)->get();

            return response()->json(['user'=>$staff], 200);
        }
        elseif($user->hasRole('finance'))
        {
            $staff = Sales::select('sales.*', 'display_name')
                ->join('users', 'users.id', '=', 'sales.user_id')
                ->join('role_user', 'role_user.user_id', '=', 'sales.user_id')
                ->join('roles', 'roles.id', '=', 'role_user.role_id')
                ->where('users.id','=',$user->id)->get();

            return response()->json(['user'=>$staff], 200);

        }
        elseif ($user->hasRole('agent'))
        {
            $staff = Sales::select('sales.*', 'display_name')
                ->join('users', 'users.id', '=', 'sales.user_id')
                ->join('role_user', 'role_user.user_id', '=', 'sales.user_id')
                ->join('roles', 'roles.id', '=', 'role_user.role_id')
                ->where('users.id','=',$user->id)->get();

            return response()->json(['user'=>$staff], 200);

        }
        elseif ($user->hasRole('sales'))
        {
            $staff = Sales::select('sales.*', 'display_name')
                ->join('users', 'users.id', '=', 'sales.user_id')
                ->join('role_user', 'role_user.user_id', '=', 'sales.user_id')
                ->join('roles', 'roles.id', '=', 'role_user.role_id')
                ->where('users.id','=',$user->id)->get();

            return response()->json(['user'=>$staff], 200);

        }
        elseif ($user->hasRole('documentation'))
        {
            $staff = Sales::select('sales.*', 'display_name')
                ->join('users', 'users.id', '=', 'sales.user_id')
                ->join('role_user', 'role_user.user_id', '=', 'sales.user_id')
                ->join('roles', 'roles.id', '=', 'role_user.role_id')
                ->where('users.id','=',$user->id)->get();

            return response()->json(['user'=>$staff], 200);

        }
        elseif ($user->hasRole('shipping'))
        {
            $staff = Sales::select('sales.*', 'display_name')
                ->join('users', 'users.id', '=', 'sales.user_id')
                ->join('role_user', 'role_user.user_id', '=', 'sales.user_id')
                ->join('roles', 'roles.id', '=', 'role_user.role_id')
                ->where('users.id','=',$user->id)->get();

            return response()->json(['user'=>$staff], 200);

        }
        elseif ($user->hasRole('declaration'))
        {
            $staff = Sales::select('sales.*', 'display_name')
                ->join('users', 'users.id', '=', 'sales.user_id')
                ->join('role_user', 'role_user.user_id', '=', 'sales.user_id')
                ->join('roles', 'roles.id', '=', 'role_user.role_id')
                ->where('users.id','=',$user->id)->get();

            return response()->json(['user'=>$staff], 200);

        }
        else
        {
            return response()->json(['error' => 'access-denied'], 401);
        }
    }

}
