<?php

namespace App\Http\Controllers;

use App\Agent;
use App\Expenses;
use App\NormalExpenses;
use App\Role;
use App\Track;
use App\User;
use DeepCopy\f002\A;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class AgentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user   =Auth::user();

        if($user->hasRole('admin'))
        {
            $expenses = Expenses::with('container')->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
            $normal = NormalExpenses::where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

            $agent = Agent::with('user')->latest('agents.created_at')->get();


            return view('agent.list_agent', compact('expenses','normal','agent'));
        }
        elseif($user->hasRole('super'))
        {
            $expenses = Expenses::with('container')->where('finance_status','LIKE','pending')->count();

            $normal = NormalExpenses::where('finance_status','LIKE','pending')->count();

            $agent = Agent::with('user')->latest('agents.created_at')->get();
            return view('agent.super_list_agent', compact('expenses','normal','agent'));
        }
        elseif($user->hasRole('sales'))
        {
            $expenses = Expenses::with('container')->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
            $normal = NormalExpenses::where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

            $agent = Agent::with('user')->latest('agents.created_at')->get();
            return view('staff.sales_list_agent', compact('expenses','normal','agent'));
        }
        elseif($user->hasRole('finance'))
        {
            $expenses = Expenses::with('container')->where('finance_status','LIKE','pending')->count();

            $normal = NormalExpenses::where('finance_status','LIKE','pending')->count();

            $agent = Agent::with('user')->latest('agents.created_at')->get();
            return view('finance.list_agent', compact('expenses','normal','agent'));
        }
        elseif($user->hasRole('documentation'))
        {
            $expenses = Expenses::with('container')->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
            $normal = NormalExpenses::where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

            $agent = Agent::with('user')->latest('agents.created_at')->get();
            return view('documentation.list_agent', compact('expenses','normal','agent'));
        }
        elseif($user->hasRole('shipping'))
        {
            $expenses = Expenses::with('container')->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
            $normal = NormalExpenses::where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

            $agent = Agent::with('user')->latest('agents.created_at')->get();
            return view('shipping.list_agent', compact('expenses','normal','agent'));
        }
        else
        {
            return JsonResponse::create(['error' => 'access-denied'],401);
        }
    }

    /**
     *
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $role = Role::where('display_name', $request['role'])->first();
        $other = $request->file('agent_logo');

        $user_password =   Hash::Make($request->get("password"));
        $request->merge(['password' => $user_password ]);
        $user = User::create($request->all());
        if(isset($other)) {
            if (isset($other)) {
                $extension2 = $request->file('agent_logo')->getClientOriginalExtension();
            }
            $sha = 'agent_logo' . md5(time());
            if(isset($extension2)){
                $filename4 = date('Y').$sha . "gfgsgf." . $extension2;
            }
            $destination_path1 = 'agent/';
            if (empty($destination_path1)) {
                File::makeDirectory($destination_path1, 0775, true, true);
            }
            if(isset($filename4)){
                $user->agent_logo = $filename4;
                $request->file('agent_logo')->move($destination_path1, $filename4);
                $user->save();
            }

        }

        $all_data =  $request->all();
        $all_data['agent_logo']                     = !empty($filename4) ? $filename4       : $user->agent_logo;

        $user->attachRole($role);
        if($user->save())
        $user_id    =Auth::id();
        $agent_id   =$user->id;
        $request->merge(['company_id' => $user_id ,'user_id' => $agent_id]);
        $order =  Agent::create($request->all());
        $all_data =  $request->all();
        $order->update($all_data);
        Session::flash('message', 'Agent  Account Created successfully');
        return redirect("/all_agent");
    }

    /**
     * @param $id
     */
    public function show($id)
    {
        //
    }

    /**
     * @param $id
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $other                     = $request->file('agent_logo');

        $order = Agent::findOrFail($id);
        $get_user_id = $request->get('user_id');
        $user_id = User::where('id','=',$get_user_id)->first()->id;

        $all_data =  $request->all();


        if ($order->update($all_data));
        if(isset($other)) {
            if (isset($other)) {
                $extension6 = $request->file('agent_logo')->getClientOriginalExtension();
            }
            $sha = 'agent_logo' . md5(time());
            if(isset($extension6)){
                $filename4 = date('Y').$sha . "gfgsgf." . $extension6;
            }
            $port_charges_path = 'agent/';
            if (empty($other)) {
                File::makeDirectory($other, 0775, true, true);
            }
            if(isset($filename4)){
                $order->agent_logo = $filename4;
                $request->file('agent_logo')->move($port_charges_path, $filename4);
                $order->save();
            }

        }
        $user_data = User::findOrFail($user_id);
        $all_data = $request->all();


        $all_data['agent_logo']  = !empty($filename4) ? $filename4  : $order->agent_logo;
        $user_data->update($all_data);

        Session::flash('message', 'Agent  Updated  successfully');
        return redirect("/all_agent");
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Exception
     */
    public function destroy($id)
    {
        $area   =Agent::findOrfail($id);
        $area->delete();

        Session::flash('delete', 'Agent  Account Deleted successfully');
        return redirect('/all_agent');
    }
}