<?php

namespace App\Http\Controllers;

use App\Agent;
use App\Container;
use App\Destination;
use App\Driver;
use App\Drop;
use App\Expenses;
use App\Ladding;
use App\NormalExpenses;
use App\Track;
use App\Transporter;
use App\Trucking;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Session;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DropController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user   =Auth::user();

        if($user->hasRole('admin'))
        {

            $from = '2021';
            $to = '2030';

            $ladding = DB::table("laddings")->whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->pluck("bill_lading","id");

            $cont_bill = Container::select('containers.*')
                            ->join('transporters','transporters.container_id','=','containers.id')
                            ->join('laddings','laddings.id','=','containers.ladding_id')
                            ->whereBetween(DB::raw('YEAR(containers.created_at)'), [$from, $to])
                            ->latest()
                            ->get();

            $bill   =Container::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->get();

            $drop = Drop::select('drops.*', 'bill_lading','cont_no')
                        ->join('containers','containers.id','=','drops.container_id')
                        ->join('laddings','laddings.id','=','containers.ladding_id')
                        ->join('transporters','transporters.container_id','=','containers.id')
                        ->whereBetween(DB::raw('YEAR(drops.created_at)'), [$from, $to])
                        ->orderByDesc("created_at")
                        ->paginate(10);
            $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
            $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

            return view('drop.drop',compact('expenses','normal','bill','drop','ladding','cont_bill'));
        }
        else if($user->hasRole('super'))
        {
            $ladding = DB::table("laddings")->pluck("bill_lading","id");

            $cont_bill = Container::select('containers.*')
                ->join('transporters','transporters.container_id','=','containers.id')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->latest()
                ->get();

            $bill   =Container::all();

            $drop = Drop::select('drops.*', 'bill_lading','cont_no')
                ->join('containers','containers.id','=','drops.container_id')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->join('transporters','transporters.container_id','=','containers.id')
                ->orderByDesc("created_at")
                ->paginate(10);
            $expenses = Expenses::with('container')->where('finance_status','LIKE','pending')->count();

            $normal = NormalExpenses::where('finance_status','LIKE','pending')->count();

            return view('drop.super_drop',compact('expenses','normal','bill','drop','ladding','cont_bill'));
        }
        else if($user->hasRole('documentation'))
        {
            $from = '2021';
            $to = '2030';
            $ladding = DB::table("laddings")->whereBetween(DB::raw('YEAR(laddings.created_at)'), [$from, $to])->pluck("bill_lading","id");

            $cont_bill = Container::select('containers.*')
                ->join('transporters','transporters.container_id','=','containers.id')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->whereBetween(DB::raw('YEAR(containers.created_at)'), [$from, $to])
                ->latest()
                ->get();

            $bill   =Container::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->get();

            $drop = Drop::select('drops.*', 'bill_lading','cont_no')
                ->join('containers','containers.id','=','drops.container_id')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->join('transporters','transporters.container_id','=','containers.id')
                ->whereBetween(DB::raw('YEAR(drops.created_at)'), [$from, $to])
                ->orderByDesc("created_at")
                ->paginate(10);
            $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
            $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

            return view('drop.documentation_drop',compact('expenses','normal','bill','drop','ladding','cont_bill'));
        }
        else if($user->hasRole('shipping'))
        {
            $from = '2021';
            $to = '2030';
            $ladding = DB::table("laddings")->whereBetween(DB::raw('YEAR(laddings.created_at)'), [$from, $to])->pluck("bill_lading","id");

            $cont_bill = Container::select('containers.*')
                ->join('transporters','transporters.container_id','=','containers.id')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->whereBetween(DB::raw('YEAR(containers.created_at)'), [$from, $to])
                ->latest()
                ->get();

            $bill   =Container::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->get();

            $drop = Drop::select('drops.*', 'bill_lading','cont_no')
                ->join('containers','containers.id','=','drops.container_id')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->join('transporters','transporters.container_id','=','containers.id')
                ->whereBetween(DB::raw('YEAR(drops.created_at)'), [$from, $to])
                ->orderByDesc("created_at")
                ->paginate(10);
            $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
            $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

            return view('drop.shipping_drop',compact('expenses','normal','bill','drop','ladding','cont_bill'));
        }
        else if($user->hasRole('sales'))
        {
            $from = '2021';
            $to = '2030';
            $ladding = DB::table("laddings")->whereBetween(DB::raw('YEAR(laddings.created_at)'), [$from, $to])->pluck("bill_lading","id");

            $cont_bill = Container::select('containers.*')
                ->join('transporters','transporters.container_id','=','containers.id')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->whereBetween(DB::raw('YEAR(containers.created_at)'), [$from, $to])
                ->latest()
                ->get();

            $bill   =Container::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->get();

            $drop = Drop::select('drops.*', 'bill_lading','cont_no')
                ->join('containers','containers.id','=','drops.container_id')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->join('transporters','transporters.container_id','=','containers.id')
                ->whereBetween(DB::raw('YEAR(drops.created_at)'), [$from, $to])
                ->orderByDesc("created_at")
                ->paginate(10);
            $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
            $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

            return view('drop.sales_drop',compact('expenses','normal','bill','drop','ladding','cont_bill'));
        }
        else if($user->hasRole('finance'))
        {
            $from = '2021';
            $to = '2030';
            $ladding = DB::table("laddings")->whereBetween(DB::raw('YEAR(laddings.created_at)'), [$from, $to])->pluck("bill_lading","id");

            $cont_bill = Container::select('containers.*')
                ->join('transporters','transporters.container_id','=','containers.id')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->whereBetween(DB::raw('YEAR(containers.created_at)'), [$from, $to])
                ->latest()
                ->get();

            $bill   =Container::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->get();

            $drop = Drop::select('drops.*', 'bill_lading','cont_no')
                ->join('containers','containers.id','=','drops.container_id')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->join('transporters','transporters.container_id','=','containers.id')
                ->whereBetween(DB::raw('YEAR(drops.created_at)'), [$from, $to])
                ->orderByDesc("created_at")
                ->paginate(10);
            $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
            $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

            return view('drop.finance_drop',compact('expenses','normal','bill','drop','ladding','cont_bill'));
        }
        else if($user->hasRole('declaration'))
        {
            $from = '2021';
            $to = '2030';
            $ladding = DB::table("laddings")->whereBetween(DB::raw('YEAR(laddings.created_at)'), [$from, $to])->pluck("bill_lading","id");

            $cont_bill = Container::select('containers.*')
                ->join('transporters','transporters.container_id','=','containers.id')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->whereBetween(DB::raw('YEAR(containers.created_at)'), [$from, $to])
                ->latest()
                ->get();

            $bill   =Container::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->get();

            $drop = Drop::select('drops.*', 'bill_lading','cont_no')
                ->join('containers','containers.id','=','drops.container_id')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->join('transporters','transporters.container_id','=','containers.id')
                ->whereBetween(DB::raw('YEAR(drops.created_at)'), [$from, $to])
                ->orderByDesc("created_at")
                ->paginate(10);
            $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
            $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

            return view('drop.declaration',compact('expenses','normal','bill','drop','ladding','cont_bill'));
        }
        else
        {
            return JsonResponse::create(['error' => 'access-denied'],401);
        }

    }


    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|static
     *
     */
    public function PendingContainer()
    {
        $user   =Auth::user();
        $from = '2021';
        $to = '2030';
        if($user->hasRole('admin'))
        {

            $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
            $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

            $ladding = DB::table("laddings")->whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->pluck("bill_lading","id");

            $cont_bill = Container::select('containers.*')
                            ->join('transporters','transporters.container_id','=','containers.id')
                            ->join('laddings','laddings.id','=','containers.ladding_id')
                            ->whereBetween(DB::raw('YEAR(containers.created_at)'), [$from, $to])
                            ->get();

            $destination = Destination::all();
            $country    = DB::table("countries")->pluck("country_name","id");

            $bill   =Container::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->get();

            $agent  = Agent::all();


            $pending    = Container::select('containers.*','bill_lading','current_status','tracks.updated_at as updateDate','organization_name','destination')
                            ->join('laddings','laddings.id','=','containers.ladding_id')
                            ->join('tracks','tracks.id','=','laddings.track_id')
                            ->join('agents','agents.id','=','tracks.agent_id')
                            ->where('tracks.current_status', 'not like', '%' . 'Receiving the documents' . '%')
                            ->where('tracks.current_status', 'not like', '%' . 'Under shipping line procedures' . '%')
                            ->where('tracks.current_status', 'not like', '%' . 'Under customs procedures' . '%')
                            ->where('tracks.current_status', 'not like', '%' . 'Release order, Delivery order received' . '%')
                            ->where('tracks.current_status', 'not like', '%' . 'Release order/delivery order' . '%')
                            ->where('tracks.current_status', 'not like', '%' . 'Prepare T1. / IM4 Documents' . '%')
                            ->where('tracks.current_status', 'not like', '%' . 'Release / Exit documents' . '%')
                            ->where('tracks.current_status', 'not like', '%' . 'Lodge Port Charges' . '%')
                            ->where('tracks.current_status', 'not like', '%' . 'Pay Port Charges' . '%')
                            ->where('tracks.current_status', 'not like', '%' . 'Loading' . '%')
                            ->where('tracks.current_status', 'not like', '%' . 'Container drop off' . '%')
                            ->where('tracks.current_status', 'not like', '%' . 'File Closed' . '%')
                            ->whereBetween(DB::raw('YEAR(tracks.created_at)'), [$from, $to])
                            ->orderByDesc("created_at")
                            ->paginate(10);


            return view('pendingContainer.admin',compact('agent','destination','country','expenses','normal','bill','pending','ladding','cont_bill'));

        }
        else if($user->hasRole('super'))
        {
            $ladding = DB::table("laddings")->pluck("bill_lading","id");

            $cont_bill = Container::select('containers.*')
                ->join('transporters','transporters.container_id','=','containers.id')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->get();

            $bill   =Container::all();

            $pending    = Container::select('containers.*','bill_lading','current_status','tracks.updated_at as updateDate','organization_name','destination')
                            ->join('laddings','laddings.id','=','containers.ladding_id')
                            ->join('tracks','tracks.id','=','laddings.track_id')
                            ->join('agents','agents.id','=','tracks.agent_id')
                            ->where('tracks.current_status', 'not like', '%' . 'Receiving the documents' . '%')
                            ->where('tracks.current_status', 'not like', '%' . 'Under shipping line procedures' . '%')
                            ->where('tracks.current_status', 'not like', '%' . 'Under customs procedures' . '%')
                            ->where('tracks.current_status', 'not like', '%' . 'Release order, Delivery order received' . '%')
                            ->where('tracks.current_status', 'not like', '%' . 'Release order/delivery order' . '%')
                            ->where('tracks.current_status', 'not like', '%' . 'Prepare T1. / IM4 Documents' . '%')
                            ->where('tracks.current_status', 'not like', '%' . 'Release / Exit documents' . '%')
                            ->where('tracks.current_status', 'not like', '%' . 'Lodge Port Charges' . '%')
                            ->where('tracks.current_status', 'not like', '%' . 'Pay Port Charges' . '%')
                            ->where('tracks.current_status', 'not like', '%' . 'Loading' . '%')
                            ->where('tracks.current_status', 'not like', '%' . 'Container drop off' . '%')
                            ->where('tracks.current_status', 'not like', '%' . 'File Closed' . '%')
                            ->whereBetween(DB::raw('YEAR(tracks.created_at)'), [$from, $to])
                            ->orderByDesc("created_at")
                            ->paginate(10);
            $agent  = Agent::all();
            $destination = Destination::all();
            $country    = DB::table("countries")->pluck("country_name","id");

            $expenses = Expenses::with('container')->where('finance_status','LIKE','pending')->count();

            $normal = NormalExpenses::where('finance_status','LIKE','pending')->count();

            return view('pendingContainer.super',compact('country','destination','agent','expenses','normal','bill','pending','ladding','cont_bill'));
        }
        else if($user->hasRole('documentation'))
        {
            $from = '2021';
            $to = '2030';
            $ladding = DB::table("laddings")->whereBetween(DB::raw('YEAR(laddings.created_at)'), [$from, $to])->pluck("bill_lading","id");

            $cont_bill = Container::select('containers.*')
                            ->join('transporters','transporters.container_id','=','containers.id')
                            ->join('laddings','laddings.id','=','containers.ladding_id')
                            ->whereBetween(DB::raw('YEAR(containers.created_at)'), [$from, $to])
                            ->get();

            $bill       =Container::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->get();

            $expenses   = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
            $normal     = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
            $agent  = Agent::all();
            $destination = Destination::all();
            $country    = DB::table("countries")->pluck("country_name","id");

            $pending    = Container::select('containers.*','bill_lading','current_status','tracks.updated_at as updateDate','organization_name','destination')
                                ->join('laddings','laddings.id','=','containers.ladding_id')
                                ->join('tracks','tracks.id','=','laddings.track_id')
                                ->join('agents','agents.id','=','tracks.agent_id')
                                ->where('tracks.current_status', 'not like', '%' . 'Receiving the documents' . '%')
                                ->where('tracks.current_status', 'not like', '%' . 'Under shipping line procedures' . '%')
                                ->where('tracks.current_status', 'not like', '%' . 'Under customs procedures' . '%')
                                ->where('tracks.current_status', 'not like', '%' . 'Release order, Delivery order received' . '%')
                                ->where('tracks.current_status', 'not like', '%' . 'Release order/delivery order' . '%')
                                ->where('tracks.current_status', 'not like', '%' . 'Prepare T1. / IM4 Documents' . '%')
                                ->where('tracks.current_status', 'not like', '%' . 'Release / Exit documents' . '%')
                                ->where('tracks.current_status', 'not like', '%' . 'Lodge Port Charges' . '%')
                                ->where('tracks.current_status', 'not like', '%' . 'Pay Port Charges' . '%')
                                ->where('tracks.current_status', 'not like', '%' . 'Loading' . '%')
                                ->where('tracks.current_status', 'not like', '%' . 'Container drop off' . '%')
                                ->where('tracks.current_status', 'not like', '%' . 'File Closed' . '%')
                                ->whereBetween(DB::raw('YEAR(tracks.created_at)'), [$from, $to])
                                ->orderByDesc("created_at")
                                ->paginate(10);
            return view('pendingContainer.documentation',compact('destination','country','agent','expenses','normal','bill','pending','ladding','cont_bill'));
        }
        else if($user->hasRole('shipping'))
        {
            $from = '2021';
            $to = '2030';
            $ladding = DB::table("laddings")->whereBetween(DB::raw('YEAR(laddings.created_at)'), [$from, $to])->pluck("bill_lading","id");

            $cont_bill = Container::select('containers.*')
                            ->join('transporters','transporters.container_id','=','containers.id')
                            ->join('laddings','laddings.id','=','containers.ladding_id')
                            ->whereBetween(DB::raw('YEAR(containers.created_at)'), [$from, $to])
                            ->get();

            $bill       =Container::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->get();

            $expenses   = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

            $normal     = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
            $agent  = Agent::all();
            $destination = Destination::all();
            $country    = DB::table("countries")->pluck("country_name","id");

            $pending    = Container::select('containers.*','bill_lading','current_status','tracks.updated_at as updateDate','organization_name','destination')
                                ->join('laddings','laddings.id','=','containers.ladding_id')
                                ->join('tracks','tracks.id','=','laddings.track_id')
                                ->join('agents','agents.id','=','tracks.agent_id')
                                ->where('tracks.current_status', 'not like', '%' . 'Receiving the documents' . '%')
                                ->where('tracks.current_status', 'not like', '%' . 'Under shipping line procedures' . '%')
                                ->where('tracks.current_status', 'not like', '%' . 'Under customs procedures' . '%')
                                ->where('tracks.current_status', 'not like', '%' . 'Release order, Delivery order received' . '%')
                                ->where('tracks.current_status', 'not like', '%' . 'Release order/delivery order' . '%')
                                ->where('tracks.current_status', 'not like', '%' . 'Prepare T1. / IM4 Documents' . '%')
                                ->where('tracks.current_status', 'not like', '%' . 'Release / Exit documents' . '%')
                                ->where('tracks.current_status', 'not like', '%' . 'Lodge Port Charges' . '%')
                                ->where('tracks.current_status', 'not like', '%' . 'Pay Port Charges' . '%')
                                ->where('tracks.current_status', 'not like', '%' . 'Loading' . '%')
                                ->where('tracks.current_status', 'not like', '%' . 'Container drop off' . '%')
                                ->where('tracks.current_status', 'not like', '%' . 'File Closed' . '%')
                                ->whereBetween(DB::raw('YEAR(tracks.created_at)'), [$from, $to])
                                ->orderByDesc("created_at")
                                ->paginate(10);
            return view('pendingContainer.shipping',compact('destination','country','agent','expenses','normal','bill','pending','ladding','cont_bill'));
        }
        else if($user->hasRole('sales'))
        {
            $from = '2021';
            $to = '2030';
            $ladding = DB::table("laddings")->whereBetween(DB::raw('YEAR(laddings.created_at)'), [$from, $to])->pluck("bill_lading","id");

            $cont_bill = Container::select('containers.*')
                            ->join('transporters','transporters.container_id','=','containers.id')
                            ->join('laddings','laddings.id','=','containers.ladding_id')
                            ->whereBetween(DB::raw('YEAR(containers.created_at)'), [$from, $to])
                            ->get();

            $bill   =Container::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->get();
            $agent  = Agent::all();
            $destination = Destination::all();
            $country    = DB::table("countries")->pluck("country_name","id");

            $pending    = Container::select('containers.*','bill_lading','current_status','tracks.updated_at as updateDate','organization_name','destination')
                                ->join('laddings','laddings.id','=','containers.ladding_id')
                                ->join('tracks','tracks.id','=','laddings.track_id')
                                ->join('agents','agents.id','=','tracks.agent_id')
                                ->where('tracks.current_status', 'not like', '%' . 'Receiving the documents' . '%')
                                ->where('tracks.current_status', 'not like', '%' . 'Under shipping line procedures' . '%')
                                ->where('tracks.current_status', 'not like', '%' . 'Under customs procedures' . '%')
                                ->where('tracks.current_status', 'not like', '%' . 'Release order, Delivery order received' . '%')
                                ->where('tracks.current_status', 'not like', '%' . 'Release order/delivery order' . '%')
                                ->where('tracks.current_status', 'not like', '%' . 'Prepare T1. / IM4 Documents' . '%')
                                ->where('tracks.current_status', 'not like', '%' . 'Release / Exit documents' . '%')
                                ->where('tracks.current_status', 'not like', '%' . 'Lodge Port Charges' . '%')
                                ->where('tracks.current_status', 'not like', '%' . 'Pay Port Charges' . '%')
                                ->where('tracks.current_status', 'not like', '%' . 'Loading' . '%')
                                ->where('tracks.current_status', 'not like', '%' . 'Container drop off' . '%')
                                ->where('tracks.current_status', 'not like', '%' . 'File Closed' . '%')
                                ->whereBetween(DB::raw('YEAR(tracks.created_at)'), [$from, $to])
                                ->orderByDesc("created_at")
                                ->paginate(10);

            $expenses   = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
            $normal     = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

            return view('pendingContainer.sales',compact('destination','country','agent','expenses','normal','bill','pending','ladding','cont_bill'));
        }
        else if($user->hasRole('finance'))
        {
            $from = '2021';
            $to = '2030';
            $ladding = DB::table("laddings")->whereBetween(DB::raw('YEAR(laddings.created_at)'), [$from, $to])->pluck("bill_lading","id");

            $cont_bill = Container::select('containers.*')
                            ->join('transporters','transporters.container_id','=','containers.id')
                            ->join('laddings','laddings.id','=','containers.ladding_id')
                            ->whereBetween(DB::raw('YEAR(containers.created_at)'), [$from, $to])
                            ->get();

            $bill   =Container::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->get();

            $expenses   = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
            $normal     = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
            $agent  = Agent::all();
            $destination = Destination::all();
            $country    = DB::table("countries")->pluck("country_name","id");

            $pending    = Container::select('containers.*','bill_lading','current_status','tracks.updated_at as updateDate','organization_name','destination')
                                ->join('laddings','laddings.id','=','containers.ladding_id')
                                ->join('tracks','tracks.id','=','laddings.track_id')
                                ->join('agents','agents.id','=','tracks.agent_id')
                                ->where('tracks.current_status', 'not like', '%' . 'Receiving the documents' . '%')
                                ->where('tracks.current_status', 'not like', '%' . 'Under shipping line procedures' . '%')
                                ->where('tracks.current_status', 'not like', '%' . 'Under customs procedures' . '%')
                                ->where('tracks.current_status', 'not like', '%' . 'Release order, Delivery order received' . '%')
                                ->where('tracks.current_status', 'not like', '%' . 'Release order/delivery order' . '%')
                                ->where('tracks.current_status', 'not like', '%' . 'Prepare T1. / IM4 Documents' . '%')
                                ->where('tracks.current_status', 'not like', '%' . 'Release / Exit documents' . '%')
                                ->where('tracks.current_status', 'not like', '%' . 'Lodge Port Charges' . '%')
                                ->where('tracks.current_status', 'not like', '%' . 'Pay Port Charges' . '%')
                                ->where('tracks.current_status', 'not like', '%' . 'Loading' . '%')
                                ->where('tracks.current_status', 'not like', '%' . 'Container drop off' . '%')
                                ->where('tracks.current_status', 'not like', '%' . 'File Closed' . '%')
                                ->whereBetween(DB::raw('YEAR(tracks.created_at)'), [$from, $to])
                                ->orderByDesc("created_at")
                                ->paginate(10);
            return view('pendingContainer.finance',compact('destination','country','agent','expenses','normal','bill','pending','ladding','cont_bill'));
        }
        else if($user->hasRole('declaration'))
        {
            $from = '2021';
            $to = '2030';
            $ladding = DB::table("laddings")->whereBetween(DB::raw('YEAR(laddings.created_at)'), [$from, $to])->pluck("bill_lading","id");

            $cont_bill = Container::select('containers.*')
                            ->join('transporters','transporters.container_id','=','containers.id')
                            ->join('laddings','laddings.id','=','containers.ladding_id')
                            ->whereBetween(DB::raw('YEAR(containers.created_at)'), [$from, $to])
                            ->get();

            $bill   =Container::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->get();
            $agent  = Agent::all();

            $pending    = Container::select('containers.*','bill_lading','current_status','tracks.updated_at as updateDate','organization_name','destination')
                                ->join('laddings','laddings.id','=','containers.ladding_id')
                                ->join('tracks','tracks.id','=','laddings.track_id')
                                ->join('agents','agents.id','=','tracks.agent_id')
                                ->where('tracks.current_status', 'not like', '%' . 'Receiving the documents' . '%')
                                ->where('tracks.current_status', 'not like', '%' . 'Under shipping line procedures' . '%')
                                ->where('tracks.current_status', 'not like', '%' . 'Under customs procedures' . '%')
                                ->where('tracks.current_status', 'not like', '%' . 'Release order, Delivery order received' . '%')
                                ->where('tracks.current_status', 'not like', '%' . 'Release order/delivery order' . '%')
                                ->where('tracks.current_status', 'not like', '%' . 'Prepare T1. / IM4 Documents' . '%')
                                ->where('tracks.current_status', 'not like', '%' . 'Release / Exit documents' . '%')
                                ->where('tracks.current_status', 'not like', '%' . 'Lodge Port Charges' . '%')
                                ->where('tracks.current_status', 'not like', '%' . 'Pay Port Charges' . '%')
                                ->where('tracks.current_status', 'not like', '%' . 'Loading' . '%')
                                ->where('tracks.current_status', 'not like', '%' . 'Container drop off' . '%')
                                ->where('tracks.current_status', 'not like', '%' . 'File Closed' . '%')
                                ->whereBetween(DB::raw('YEAR(tracks.created_at)'), [$from, $to])
                                ->orderByDesc("created_at")
                                ->paginate(10);
            $destination = Destination::all();
            $country    = DB::table("countries")->pluck("country_name","id");

            $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
            $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

            return view('pendingContainer.declaration',compact('country','destination','agent','expenses','normal','bill','pending','ladding','cont_bill'));
        }
        else
        {
            return JsonResponse::create(['error' => 'access-denied'],401);
        }

    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function SearchPendingC(Request $request)
    {
        $containers =   $request->get('container_id');
        $cf         =   $request->get('cf');
        $location   =   $request->get('destination');


        $user   =Auth::user();

        if($user->hasRole('admin'))
        {
            if ($containers !='' AND $cf !='' AND $location !='')
            {


                $from = '2021';
                $to = '2030';
                $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
                $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

                $ladding = DB::table("laddings")->whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->pluck("bill_lading","id");

                $cont_bill = Container::select('containers.*')
                    ->join('transporters','transporters.container_id','=','containers.id')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->whereBetween(DB::raw('YEAR(containers.created_at)'), [$from, $to])
                    ->get();
                $destination = Destination::all();
                $country    = DB::table("countries")->pluck("country_name","id");

                $bill   =Container::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->get();
                $agent  = Agent::all();


                $pending    = Container::select('containers.*','bill_lading','current_status','tracks.updated_at as updateDate','organization_name','destination')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->join('tracks','tracks.id','=','laddings.track_id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('tracks.current_status', 'not like', '%' . 'Receiving the documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Under shipping line procedures' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Under customs procedures' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release order, Delivery order received' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release order/delivery order' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Prepare T1. / IM4 Documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release / Exit documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Lodge Port Charges' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Pay Port Charges' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Loading' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Container drop off' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'File Closed' . '%')
                    ->whereBetween(DB::raw('YEAR(containers.created_at)'), [$from, $to])
                    ->whereBetween(DB::raw('YEAR(tracks.updated_at)'), [$from, $to])
                    ->where('containers.id','=',$containers)
                    ->where('tracks.agent_id','=',$cf)
                    ->where('tracks.destination', 'like', '%' . $location . '%')
                    ->orderByDesc("created_at")
                    ->paginate(10);


                return view('pendingContainer.admin',compact('agent','destination','country','expenses','normal','bill','pending','ladding','cont_bill'));

            }
            elseif ($containers !='' AND $cf !='' AND $location =='')
            {
                $from = '2021';
                $to = '2030';
                $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
                $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

                $ladding = DB::table("laddings")->whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->pluck("bill_lading","id");

                $cont_bill = Container::select('containers.*')
                    ->join('transporters','transporters.container_id','=','containers.id')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->whereBetween(DB::raw('YEAR(containers.created_at)'), [$from, $to])
                    ->get();
                $destination = Destination::all();
                $country    = DB::table("countries")->pluck("country_name","id");

                $bill   =Container::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->get();
                $agent  = Agent::all();


                $pending    = Container::select('containers.*','bill_lading','current_status','tracks.updated_at as updateDate','organization_name','destination')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->join('tracks','tracks.id','=','laddings.track_id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('tracks.current_status', 'not like', '%' . 'Receiving the documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Under shipping line procedures' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Under customs procedures' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release order, Delivery order received' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release order/delivery order' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Prepare T1. / IM4 Documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release / Exit documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Lodge Port Charges' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Pay Port Charges' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Loading' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Container drop off' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'File Closed' . '%')
                    ->whereBetween(DB::raw('YEAR(containers.created_at)'), [$from, $to])
                    ->whereBetween(DB::raw('YEAR(tracks.updated_at)'), [$from, $to])
                    ->where('containers.id','=',$containers)
                    ->where('tracks.agent_id','=',$cf)
                    ->orderByDesc("created_at")
                    ->paginate(10);


                return view('pendingContainer.admin',compact('agent','destination','country','expenses','normal','bill','pending','ladding','cont_bill'));

            }
            elseif ($containers !='' AND $cf =='' AND $location !='')
            {
                $from = '2021';
                $to = '2030';
                $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
                $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

                $ladding = DB::table("laddings")->whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->pluck("bill_lading","id");

                $cont_bill = Container::select('containers.*')
                    ->join('transporters','transporters.container_id','=','containers.id')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->whereBetween(DB::raw('YEAR(containers.created_at)'), [$from, $to])
                    ->get();
                $destination = Destination::all();
                $country    = DB::table("countries")->pluck("country_name","id");

                $bill   =Container::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->get();
                $agent  = Agent::all();


                $pending    = Container::select('containers.*','bill_lading','current_status','tracks.updated_at as updateDate','organization_name','destination')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->join('tracks','tracks.id','=','laddings.track_id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('tracks.current_status', 'not like', '%' . 'Receiving the documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Under shipping line procedures' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Under customs procedures' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release order, Delivery order received' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release order/delivery order' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Prepare T1. / IM4 Documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release / Exit documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Lodge Port Charges' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Pay Port Charges' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Loading' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Container drop off' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'File Closed' . '%')
                    ->whereBetween(DB::raw('YEAR(containers.created_at)'), [$from, $to])
                    ->whereBetween(DB::raw('YEAR(tracks.updated_at)'), [$from, $to])
                    ->where('containers.id','=',$containers)
                    ->where('tracks.destination', 'like', '%' . $location . '%')
                    ->orderByDesc("created_at")
                    ->paginate(10);


                return view('pendingContainer.admin',compact('agent','destination','country','expenses','normal','bill','pending','ladding','cont_bill'));

            }
            elseif ($containers !='' AND $cf =='' AND $location =='')
            {
                $from = '2021';
                $to = '2030';
                $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
                $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

                $ladding = DB::table("laddings")->whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->pluck("bill_lading","id");

                $cont_bill = Container::select('containers.*')
                    ->join('transporters','transporters.container_id','=','containers.id')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->whereBetween(DB::raw('YEAR(containers.created_at)'), [$from, $to])
                    ->get();
                $destination = Destination::all();
                $country    = DB::table("countries")->pluck("country_name","id");

                $bill   =Container::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->get();
                $agent  = Agent::all();


                $pending    = Container::select('containers.*','bill_lading','current_status','tracks.updated_at as updateDate','organization_name','destination')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->join('tracks','tracks.id','=','laddings.track_id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('tracks.current_status', 'not like', '%' . 'Receiving the documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Under shipping line procedures' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Under customs procedures' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release order, Delivery order received' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release order/delivery order' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Prepare T1. / IM4 Documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release / Exit documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Lodge Port Charges' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Pay Port Charges' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Loading' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Container drop off' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'File Closed' . '%')
                    ->whereBetween(DB::raw('YEAR(containers.created_at)'), [$from, $to])
                    ->whereBetween(DB::raw('YEAR(tracks.updated_at)'), [$from, $to])
                    ->where('containers.id','=',$containers)
                    ->orderByDesc("created_at")
                    ->paginate(10);


                return view('pendingContainer.admin',compact('agent','destination','country','expenses','normal','bill','pending','ladding','cont_bill'));

            }
            elseif ($containers =='' AND $cf !='' AND $location !='')
            {
                $from = '2021';
                $to = '2030';
                $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
                $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

                $ladding = DB::table("laddings")->whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->pluck("bill_lading","id");

                $cont_bill = Container::select('containers.*')
                    ->join('transporters','transporters.container_id','=','containers.id')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->whereBetween(DB::raw('YEAR(containers.created_at)'), [$from, $to])
                    ->get();
                $destination = Destination::all();
                $country    = DB::table("countries")->pluck("country_name","id");

                $bill   =Container::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->get();
                $agent  = Agent::all();


                $pending    = Container::select('containers.*','bill_lading','current_status','tracks.updated_at as updateDate','organization_name','destination')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->join('tracks','tracks.id','=','laddings.track_id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('tracks.current_status', 'not like', '%' . 'Receiving the documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Under shipping line procedures' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Under customs procedures' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release order, Delivery order received' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release order/delivery order' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Prepare T1. / IM4 Documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release / Exit documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Lodge Port Charges' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Pay Port Charges' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Loading' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Container drop off' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'File Closed' . '%')
                    ->whereBetween(DB::raw('YEAR(containers.created_at)'), [$from, $to])
                    ->whereBetween(DB::raw('YEAR(tracks.updated_at)'), [$from, $to])
                    ->where('tracks.agent_id','=',$cf)
                    ->where('tracks.destination', 'like', '%' . $location . '%')
                    ->orderByDesc("created_at")
                    ->paginate(10);


                return view('pendingContainer.admin',compact('agent','destination','country','expenses','normal','bill','pending','ladding','cont_bill'));

            }
            elseif ($containers =='' AND $cf =='' AND $location !='')
            {
                $from = '2021';
                $to = '2030';
                $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
                $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

                $ladding = DB::table("laddings")->whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->pluck("bill_lading","id");

                $cont_bill = Container::select('containers.*')
                    ->join('transporters','transporters.container_id','=','containers.id')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->whereBetween(DB::raw('YEAR(containers.created_at)'), [$from, $to])
                    ->get();
                $destination = Destination::all();
                $country    = DB::table("countries")->pluck("country_name","id");

                $bill   =Container::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->get();
                $agent  = Agent::all();


                $pending    = Container::select('containers.*','bill_lading','current_status','tracks.updated_at as updateDate','organization_name','destination')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->join('tracks','tracks.id','=','laddings.track_id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('tracks.current_status', 'not like', '%' . 'Receiving the documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Under shipping line procedures' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Under customs procedures' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release order, Delivery order received' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release order/delivery order' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Prepare T1. / IM4 Documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release / Exit documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Lodge Port Charges' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Pay Port Charges' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Loading' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Container drop off' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'File Closed' . '%')
                    ->whereBetween(DB::raw('YEAR(containers.created_at)'), [$from, $to])
                    ->whereBetween(DB::raw('YEAR(tracks.updated_at)'), [$from, $to])
                    ->where('tracks.destination', 'like', '%' . $location . '%')
                    ->orderByDesc("created_at")
                    ->paginate(10);


                return view('pendingContainer.admin',compact('agent','destination','country','expenses','normal','bill','pending','ladding','cont_bill'));

            }
            elseif ($containers =='' AND $cf !='' AND $location =='')
            {
                $from = '2021';
                $to = '2030';
                $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
                $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

                $ladding = DB::table("laddings")->whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->pluck("bill_lading","id");

                $cont_bill = Container::select('containers.*')
                    ->join('transporters','transporters.container_id','=','containers.id')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->whereBetween(DB::raw('YEAR(containers.created_at)'), [$from, $to])
                    ->get();
                $destination = Destination::all();
                $country    = DB::table("countries")->pluck("country_name","id");

                $bill   =Container::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->get();
                $agent  = Agent::all();


                $pending    = Container::select('containers.*','bill_lading','current_status','tracks.updated_at as updateDate','organization_name','destination')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->join('tracks','tracks.id','=','laddings.track_id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('tracks.current_status', 'not like', '%' . 'Receiving the documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Under shipping line procedures' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Under customs procedures' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release order, Delivery order received' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release order/delivery order' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Prepare T1. / IM4 Documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release / Exit documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Lodge Port Charges' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Pay Port Charges' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Loading' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Container drop off' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'File Closed' . '%')
                    ->whereBetween(DB::raw('YEAR(containers.created_at)'), [$from, $to])
                    ->whereBetween(DB::raw('YEAR(tracks.updated_at)'), [$from, $to])
                    ->where('tracks.agent_id','=',$cf)
                    ->orderByDesc("created_at")
                    ->paginate(10);

                return view('pendingContainer.admin',compact('agent','destination','country','expenses','normal','bill','pending','ladding','cont_bill'));

            }
            else
            {
                $from = '2021';
                $to = '2030';
                $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
                $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

                $ladding = DB::table("laddings")->whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->pluck("bill_lading","id");

                $cont_bill = Container::select('containers.*')
                    ->join('transporters','transporters.container_id','=','containers.id')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->whereBetween(DB::raw('YEAR(containers.created_at)'), [$from, $to])
                    ->get();
                $destination = Destination::all();
                $country    = DB::table("countries")->pluck("country_name","id");

                $bill   =Container::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->get();
                $agent  = Agent::all();


                $pending    = Container::select('containers.*','bill_lading','current_status','tracks.updated_at as updateDate','organization_name','destination')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->join('tracks','tracks.id','=','laddings.track_id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('tracks.current_status', 'not like', '%' . 'Receiving the documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Under shipping line procedures' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Under customs procedures' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release order, Delivery order received' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release order/delivery order' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Prepare T1. / IM4 Documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release / Exit documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Lodge Port Charges' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Pay Port Charges' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Loading' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Container drop off' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'File Closed' . '%')
                    ->whereBetween(DB::raw('YEAR(containers.created_at)'), [$from, $to])
                    ->whereBetween(DB::raw('YEAR(tracks.updated_at)'), [$from, $to])
                    ->orderByDesc("created_at")
                    ->paginate(10);


                return view('pendingContainer.admin',compact('agent','destination','country','expenses','normal','bill','pending','ladding','cont_bill'));


            }
        }
        else if($user->hasRole('super'))
        {
            if ($containers !='' AND $cf !='' AND $location !='')
            {


                $from = '2021';
                $to = '2030';
                $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
                $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

                $ladding = DB::table("laddings")->whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->pluck("bill_lading","id");

                $cont_bill = Container::select('containers.*')
                    ->join('transporters','transporters.container_id','=','containers.id')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->whereBetween(DB::raw('YEAR(containers.created_at)'), [$from, $to])
                    ->get();
                $destination = Destination::all();
                $country    = DB::table("countries")->pluck("country_name","id");

                $bill   =Container::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->get();
                $agent  = Agent::all();


                $pending    = Container::select('containers.*','bill_lading','current_status','tracks.updated_at as updateDate','organization_name','destination')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->join('tracks','tracks.id','=','laddings.track_id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('tracks.current_status', 'not like', '%' . 'Receiving the documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Under shipping line procedures' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Under customs procedures' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release order, Delivery order received' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release order/delivery order' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Prepare T1. / IM4 Documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release / Exit documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Lodge Port Charges' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Pay Port Charges' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Loading' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Container drop off' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'File Closed' . '%')
                    ->whereBetween(DB::raw('YEAR(containers.created_at)'), [$from, $to])
                    ->whereBetween(DB::raw('YEAR(tracks.updated_at)'), [$from, $to])
                    ->where('containers.id','=',$containers)
                    ->where('tracks.agent_id','=',$cf)
                    ->where('tracks.destination', 'like', '%' . $location . '%')
                    ->orderByDesc("created_at")
                    ->paginate(10);


                return view('pendingContainer.super',compact('agent','destination','country','expenses','normal','bill','pending','ladding','cont_bill'));

            }
            elseif ($containers !='' AND $cf !='' AND $location =='')
            {
                $from = '2021';
                $to = '2030';
                $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
                $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

                $ladding = DB::table("laddings")->whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->pluck("bill_lading","id");

                $cont_bill = Container::select('containers.*')
                    ->join('transporters','transporters.container_id','=','containers.id')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->whereBetween(DB::raw('YEAR(containers.created_at)'), [$from, $to])
                    ->get();
                $destination = Destination::all();
                $country    = DB::table("countries")->pluck("country_name","id");

                $bill   =Container::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->get();
                $agent  = Agent::all();


                $pending    = Container::select('containers.*','bill_lading','current_status','tracks.updated_at as updateDate','organization_name','destination')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->join('tracks','tracks.id','=','laddings.track_id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('tracks.current_status', 'not like', '%' . 'Receiving the documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Under shipping line procedures' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Under customs procedures' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release order, Delivery order received' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release order/delivery order' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Prepare T1. / IM4 Documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release / Exit documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Lodge Port Charges' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Pay Port Charges' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Loading' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Container drop off' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'File Closed' . '%')
                    ->whereBetween(DB::raw('YEAR(containers.created_at)'), [$from, $to])
                    ->whereBetween(DB::raw('YEAR(tracks.updated_at)'), [$from, $to])
                    ->where('containers.id','=',$containers)
                    ->where('tracks.agent_id','=',$cf)
                    ->orderByDesc("created_at")
                    ->paginate(10);


                return view('pendingContainer.super',compact('agent','destination','country','expenses','normal','bill','pending','ladding','cont_bill'));

            }
            elseif ($containers !='' AND $cf =='' AND $location !='')
            {
                $from = '2021';
                $to = '2030';
                $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
                $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

                $ladding = DB::table("laddings")->whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->pluck("bill_lading","id");

                $cont_bill = Container::select('containers.*')
                    ->join('transporters','transporters.container_id','=','containers.id')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->whereBetween(DB::raw('YEAR(containers.created_at)'), [$from, $to])
                    ->get();
                $destination = Destination::all();
                $country    = DB::table("countries")->pluck("country_name","id");

                $bill   =Container::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->get();
                $agent  = Agent::all();


                $pending    = Container::select('containers.*','bill_lading','current_status','tracks.updated_at as updateDate','organization_name','destination')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->join('tracks','tracks.id','=','laddings.track_id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('tracks.current_status', 'not like', '%' . 'Receiving the documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Under shipping line procedures' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Under customs procedures' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release order, Delivery order received' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release order/delivery order' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Prepare T1. / IM4 Documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release / Exit documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Lodge Port Charges' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Pay Port Charges' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Loading' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Container drop off' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'File Closed' . '%')
                    ->whereBetween(DB::raw('YEAR(containers.created_at)'), [$from, $to])
                    ->whereBetween(DB::raw('YEAR(tracks.updated_at)'), [$from, $to])
                    ->where('containers.id','=',$containers)
                    ->where('tracks.destination', 'like', '%' . $location . '%')
                    ->orderByDesc("created_at")
                    ->paginate(10);


                return view('pendingContainer.super',compact('agent','destination','country','expenses','normal','bill','pending','ladding','cont_bill'));

            }
            elseif ($containers !='' AND $cf =='' AND $location =='')
            {
                $from = '2021';
                $to = '2030';
                $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
                $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

                $ladding = DB::table("laddings")->whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->pluck("bill_lading","id");

                $cont_bill = Container::select('containers.*')
                    ->join('transporters','transporters.container_id','=','containers.id')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->whereBetween(DB::raw('YEAR(containers.created_at)'), [$from, $to])
                    ->get();
                $destination = Destination::all();
                $country    = DB::table("countries")->pluck("country_name","id");

                $bill   =Container::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->get();
                $agent  = Agent::all();


                $pending    = Container::select('containers.*','bill_lading','current_status','tracks.updated_at as updateDate','organization_name','destination')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->join('tracks','tracks.id','=','laddings.track_id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('tracks.current_status', 'not like', '%' . 'Receiving the documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Under shipping line procedures' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Under customs procedures' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release order, Delivery order received' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release order/delivery order' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Prepare T1. / IM4 Documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release / Exit documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Lodge Port Charges' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Pay Port Charges' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Loading' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Container drop off' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'File Closed' . '%')
                    ->whereBetween(DB::raw('YEAR(containers.created_at)'), [$from, $to])
                    ->whereBetween(DB::raw('YEAR(tracks.updated_at)'), [$from, $to])
                    ->where('containers.id','=',$containers)
                    ->orderByDesc("created_at")
                    ->paginate(10);


                return view('pendingContainer.super',compact('agent','destination','country','expenses','normal','bill','pending','ladding','cont_bill'));

            }
            elseif ($containers =='' AND $cf !='' AND $location !='')
            {
                $from = '2021';
                $to = '2030';
                $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
                $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

                $ladding = DB::table("laddings")->whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->pluck("bill_lading","id");

                $cont_bill = Container::select('containers.*')
                    ->join('transporters','transporters.container_id','=','containers.id')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->whereBetween(DB::raw('YEAR(containers.created_at)'), [$from, $to])
                    ->get();
                $destination = Destination::all();
                $country    = DB::table("countries")->pluck("country_name","id");

                $bill   =Container::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->get();
                $agent  = Agent::all();


                $pending    = Container::select('containers.*','bill_lading','current_status','tracks.updated_at as updateDate','organization_name','destination')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->join('tracks','tracks.id','=','laddings.track_id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('tracks.current_status', 'not like', '%' . 'Receiving the documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Under shipping line procedures' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Under customs procedures' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release order, Delivery order received' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release order/delivery order' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Prepare T1. / IM4 Documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release / Exit documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Lodge Port Charges' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Pay Port Charges' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Loading' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Container drop off' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'File Closed' . '%')
                    ->whereBetween(DB::raw('YEAR(containers.created_at)'), [$from, $to])
                    ->whereBetween(DB::raw('YEAR(tracks.updated_at)'), [$from, $to])
                    ->where('tracks.agent_id','=',$cf)
                    ->where('tracks.destination', 'like', '%' . $location . '%')
                    ->orderByDesc("created_at")
                    ->paginate(10);


                return view('pendingContainer.super',compact('agent','destination','country','expenses','normal','bill','pending','ladding','cont_bill'));

            }
            elseif ($containers =='' AND $cf =='' AND $location !='')
            {
                $from = '2021';
                $to = '2030';
                $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
                $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

                $ladding = DB::table("laddings")->whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->pluck("bill_lading","id");

                $cont_bill = Container::select('containers.*')
                    ->join('transporters','transporters.container_id','=','containers.id')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->whereBetween(DB::raw('YEAR(containers.created_at)'), [$from, $to])
                    ->get();
                $destination = Destination::all();
                $country    = DB::table("countries")->pluck("country_name","id");

                $bill   =Container::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->get();
                $agent  = Agent::all();


                $pending    = Container::select('containers.*','bill_lading','current_status','tracks.updated_at as updateDate','organization_name','destination')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->join('tracks','tracks.id','=','laddings.track_id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('tracks.current_status', 'not like', '%' . 'Receiving the documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Under shipping line procedures' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Under customs procedures' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release order, Delivery order received' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release order/delivery order' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Prepare T1. / IM4 Documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release / Exit documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Lodge Port Charges' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Pay Port Charges' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Loading' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Container drop off' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'File Closed' . '%')
                    ->whereBetween(DB::raw('YEAR(containers.created_at)'), [$from, $to])
                    ->whereBetween(DB::raw('YEAR(tracks.updated_at)'), [$from, $to])
                    ->where('tracks.destination', 'like', '%' . $location . '%')
                    ->orderByDesc("created_at")
                    ->paginate(10);


                return view('pendingContainer.super',compact('agent','destination','country','expenses','normal','bill','pending','ladding','cont_bill'));

            }
            elseif ($containers =='' AND $cf !='' AND $location =='')
            {
                $from = '2021';
                $to = '2030';
                $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
                $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

                $ladding = DB::table("laddings")->whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->pluck("bill_lading","id");

                $cont_bill = Container::select('containers.*')
                    ->join('transporters','transporters.container_id','=','containers.id')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->whereBetween(DB::raw('YEAR(containers.created_at)'), [$from, $to])
                    ->get();
                $destination = Destination::all();
                $country    = DB::table("countries")->pluck("country_name","id");

                $bill   =Container::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->get();
                $agent  = Agent::all();


                $pending    = Container::select('containers.*','bill_lading','current_status','tracks.updated_at as updateDate','organization_name','destination')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->join('tracks','tracks.id','=','laddings.track_id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('tracks.current_status', 'not like', '%' . 'Receiving the documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Under shipping line procedures' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Under customs procedures' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release order, Delivery order received' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release order/delivery order' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Prepare T1. / IM4 Documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release / Exit documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Lodge Port Charges' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Pay Port Charges' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Loading' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Container drop off' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'File Closed' . '%')
                    ->whereBetween(DB::raw('YEAR(containers.created_at)'), [$from, $to])
                    ->whereBetween(DB::raw('YEAR(tracks.updated_at)'), [$from, $to])
                    ->where('tracks.agent_id','=',$cf)
                    ->orderByDesc("created_at")
                    ->paginate(10);


                return view('pendingContainer.super',compact('agent','destination','country','expenses','normal','bill','pending','ladding','cont_bill'));

            }
            else
            {
                $from = '2021';
                $to = '2030';
                $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
                $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

                $ladding = DB::table("laddings")->whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->pluck("bill_lading","id");

                $cont_bill = Container::select('containers.*')
                    ->join('transporters','transporters.container_id','=','containers.id')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->whereBetween(DB::raw('YEAR(containers.created_at)'), [$from, $to])
                    ->get();
                $destination = Destination::all();
                $country    = DB::table("countries")->pluck("country_name","id");

                $bill   =Container::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->get();
                $agent  = Agent::all();


                $pending    = Container::select('containers.*','bill_lading','current_status','tracks.updated_at as updateDate','organization_name','destination')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->join('tracks','tracks.id','=','laddings.track_id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('tracks.current_status', 'not like', '%' . 'Receiving the documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Under shipping line procedures' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Under customs procedures' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release order, Delivery order received' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release order/delivery order' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Prepare T1. / IM4 Documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release / Exit documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Lodge Port Charges' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Pay Port Charges' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Loading' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Container drop off' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'File Closed' . '%')
                    ->whereBetween(DB::raw('YEAR(containers.created_at)'), [$from, $to])
                    ->whereBetween(DB::raw('YEAR(tracks.updated_at)'), [$from, $to])
                    ->orderByDesc("created_at")
                    ->paginate(10);


                return view('pendingContainer.super',compact('agent','destination','country','expenses','normal','bill','pending','ladding','cont_bill'));


            }
        }
        else if($user->hasRole('documentation'))
        {
            if ($containers !='' AND $cf !='' AND $location !='')
            {


                $from = '2021';
                $to = '2030';
                $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
                $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

                $ladding = DB::table("laddings")->whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->pluck("bill_lading","id");

                $cont_bill = Container::select('containers.*')
                    ->join('transporters','transporters.container_id','=','containers.id')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->whereBetween(DB::raw('YEAR(containers.created_at)'), [$from, $to])
                    ->get();
                $destination = Destination::all();
                $country    = DB::table("countries")->pluck("country_name","id");

                $bill   =Container::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->get();
                $agent  = Agent::all();


                $pending    = Container::select('containers.*','bill_lading','current_status','tracks.updated_at as updateDate','organization_name','destination')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->join('tracks','tracks.id','=','laddings.track_id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('tracks.current_status', 'not like', '%' . 'Receiving the documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Under shipping line procedures' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Under customs procedures' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release order, Delivery order received' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release order/delivery order' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Prepare T1. / IM4 Documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release / Exit documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Lodge Port Charges' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Pay Port Charges' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Loading' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Container drop off' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'File Closed' . '%')
                    ->whereBetween(DB::raw('YEAR(containers.created_at)'), [$from, $to])
                    ->where('containers.id','=',$containers)
                    ->where('tracks.agent_id','=',$cf)
                    ->where('tracks.destination', 'like', '%' . $location . '%')
                    ->whereBetween(DB::raw('YEAR(tracks.updated_at)'), [$from, $to])
                    ->orderByDesc("created_at")
                    ->paginate(10);


                return view('pendingContainer.documentation',compact('agent','destination','country','expenses','normal','bill','pending','ladding','cont_bill'));

            }
            elseif ($containers !='' AND $cf !='' AND $location =='')
            {
                $from = '2021';
                $to = '2030';
                $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
                $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

                $ladding = DB::table("laddings")->whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->pluck("bill_lading","id");

                $cont_bill = Container::select('containers.*')
                    ->join('transporters','transporters.container_id','=','containers.id')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->whereBetween(DB::raw('YEAR(containers.created_at)'), [$from, $to])
                    ->get();
                $destination = Destination::all();
                $country    = DB::table("countries")->pluck("country_name","id");

                $bill   =Container::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->get();
                $agent  = Agent::all();


                $pending    = Container::select('containers.*','bill_lading','current_status','tracks.updated_at as updateDate','organization_name','destination')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->join('tracks','tracks.id','=','laddings.track_id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('tracks.current_status', 'not like', '%' . 'Receiving the documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Under shipping line procedures' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Under customs procedures' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release order, Delivery order received' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release order/delivery order' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Prepare T1. / IM4 Documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release / Exit documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Lodge Port Charges' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Pay Port Charges' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Loading' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Container drop off' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'File Closed' . '%')
                    ->whereBetween(DB::raw('YEAR(containers.created_at)'), [$from, $to])
                    ->whereBetween(DB::raw('YEAR(tracks.updated_at)'), [$from, $to])
                    ->where('containers.id','=',$containers)
                    ->where('tracks.agent_id','=',$cf)
                    ->orderByDesc("created_at")
                    ->paginate(10);


                return view('pendingContainer.documentation',compact('agent','destination','country','expenses','normal','bill','pending','ladding','cont_bill'));

            }
            elseif ($containers !='' AND $cf =='' AND $location !='')
            {
                $from = '2021';
                $to = '2030';
                $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
                $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

                $ladding = DB::table("laddings")->whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->pluck("bill_lading","id");

                $cont_bill = Container::select('containers.*')
                    ->join('transporters','transporters.container_id','=','containers.id')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->whereBetween(DB::raw('YEAR(containers.created_at)'), [$from, $to])
                    ->get();
                $destination = Destination::all();
                $country    = DB::table("countries")->pluck("country_name","id");

                $bill   =Container::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->get();
                $agent  = Agent::all();


                $pending    = Container::select('containers.*','bill_lading','current_status','tracks.updated_at as updateDate','organization_name','destination')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->join('tracks','tracks.id','=','laddings.track_id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('tracks.current_status', 'not like', '%' . 'Receiving the documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Under shipping line procedures' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Under customs procedures' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release order, Delivery order received' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release order/delivery order' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Prepare T1. / IM4 Documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release / Exit documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Lodge Port Charges' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Pay Port Charges' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Loading' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Container drop off' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'File Closed' . '%')
                    ->whereBetween(DB::raw('YEAR(containers.created_at)'), [$from, $to])
                    ->whereBetween(DB::raw('YEAR(tracks.updated_at)'), [$from, $to])
                    ->where('containers.id','=',$containers)
                    ->where('tracks.destination', 'like', '%' . $location . '%')
                    ->orderByDesc("created_at")
                    ->paginate(10);


                return view('pendingContainer.documentation',compact('agent','destination','country','expenses','normal','bill','pending','ladding','cont_bill'));

            }
            elseif ($containers !='' AND $cf =='' AND $location =='')
            {
                $from = '2021';
                $to = '2030';
                $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
                $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

                $ladding = DB::table("laddings")->whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->pluck("bill_lading","id");

                $cont_bill = Container::select('containers.*')
                    ->join('transporters','transporters.container_id','=','containers.id')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->whereBetween(DB::raw('YEAR(containers.created_at)'), [$from, $to])
                    ->get();
                $destination = Destination::all();
                $country    = DB::table("countries")->pluck("country_name","id");

                $bill   =Container::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->get();
                $agent  = Agent::all();


                $pending    = Container::select('containers.*','bill_lading','current_status','tracks.updated_at as updateDate','organization_name','destination')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->join('tracks','tracks.id','=','laddings.track_id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('tracks.current_status', 'not like', '%' . 'Receiving the documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Under shipping line procedures' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Under customs procedures' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release order, Delivery order received' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release order/delivery order' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Prepare T1. / IM4 Documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release / Exit documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Lodge Port Charges' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Pay Port Charges' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Loading' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Container drop off' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'File Closed' . '%')
                    ->whereBetween(DB::raw('YEAR(containers.created_at)'), [$from, $to])
                    ->whereBetween(DB::raw('YEAR(tracks.updated_at)'), [$from, $to])
                    ->where('containers.id','=',$containers)
                    ->orderByDesc("created_at")
                    ->paginate(10);


                return view('pendingContainer.documentation',compact('agent','destination','country','expenses','normal','bill','pending','ladding','cont_bill'));

            }
            elseif ($containers =='' AND $cf !='' AND $location !='')
            {
                $from = '2021';
                $to = '2030';
                $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
                $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

                $ladding = DB::table("laddings")->whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->pluck("bill_lading","id");

                $cont_bill = Container::select('containers.*')
                    ->join('transporters','transporters.container_id','=','containers.id')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->whereBetween(DB::raw('YEAR(containers.created_at)'), [$from, $to])
                    ->get();
                $destination = Destination::all();
                $country    = DB::table("countries")->pluck("country_name","id");

                $bill   =Container::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->get();
                $agent  = Agent::all();


                $pending    = Container::select('containers.*','bill_lading','current_status','tracks.updated_at as updateDate','organization_name','destination')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->join('tracks','tracks.id','=','laddings.track_id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('tracks.current_status', 'not like', '%' . 'Receiving the documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Under shipping line procedures' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Under customs procedures' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release order, Delivery order received' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release order/delivery order' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Prepare T1. / IM4 Documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release / Exit documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Lodge Port Charges' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Pay Port Charges' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Loading' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Container drop off' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'File Closed' . '%')
                    ->whereBetween(DB::raw('YEAR(containers.created_at)'), [$from, $to])
                    ->where('tracks.agent_id','=',$cf)
                    ->where('tracks.destination', 'like', '%' . $location . '%')
                    ->whereBetween(DB::raw('YEAR(tracks.updated_at)'), [$from, $to])
                    ->orderByDesc("created_at")
                    ->paginate(10);


                return view('pendingContainer.documentation',compact('agent','destination','country','expenses','normal','bill','pending','ladding','cont_bill'));

            }
            elseif ($containers =='' AND $cf =='' AND $location !='')
            {
                $from = '2021';
                $to = '2030';
                $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
                $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

                $ladding = DB::table("laddings")->whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->pluck("bill_lading","id");

                $cont_bill = Container::select('containers.*')
                    ->join('transporters','transporters.container_id','=','containers.id')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->whereBetween(DB::raw('YEAR(containers.created_at)'), [$from, $to])
                    ->get();
                $destination = Destination::all();
                $country    = DB::table("countries")->pluck("country_name","id");

                $bill   =Container::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->get();
                $agent  = Agent::all();


                $pending    = Container::select('containers.*','bill_lading','current_status','tracks.updated_at as updateDate','organization_name','destination')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->join('tracks','tracks.id','=','laddings.track_id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('tracks.current_status', 'not like', '%' . 'Receiving the documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Under shipping line procedures' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Under customs procedures' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release order, Delivery order received' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release order/delivery order' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Prepare T1. / IM4 Documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release / Exit documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Lodge Port Charges' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Pay Port Charges' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Loading' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Container drop off' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'File Closed' . '%')
                    ->whereBetween(DB::raw('YEAR(containers.created_at)'), [$from, $to])
                    ->whereBetween(DB::raw('YEAR(tracks.updated_at)'), [$from, $to])
                    ->where('tracks.destination', 'like', '%' . $location . '%')
                    ->orderByDesc("created_at")
                    ->paginate(10);


                return view('pendingContainer.documentation',compact('agent','destination','country','expenses','normal','bill','pending','ladding','cont_bill'));

            }
            elseif ($containers =='' AND $cf !='' AND $location =='')
            {
                $from = '2021';
                $to = '2030';
                $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
                $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

                $ladding = DB::table("laddings")->whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->pluck("bill_lading","id");

                $cont_bill = Container::select('containers.*')
                    ->join('transporters','transporters.container_id','=','containers.id')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->whereBetween(DB::raw('YEAR(containers.created_at)'), [$from, $to])
                    ->get();
                $destination = Destination::all();
                $country    = DB::table("countries")->pluck("country_name","id");

                $bill   =Container::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->get();
                $agent  = Agent::all();


                $pending    = Container::select('containers.*','bill_lading','current_status','tracks.updated_at as updateDate','organization_name','destination')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->join('tracks','tracks.id','=','laddings.track_id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('tracks.current_status', 'not like', '%' . 'Receiving the documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Under shipping line procedures' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Under customs procedures' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release order, Delivery order received' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release order/delivery order' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Prepare T1. / IM4 Documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release / Exit documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Lodge Port Charges' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Pay Port Charges' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Loading' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Container drop off' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'File Closed' . '%')
                    ->whereBetween(DB::raw('YEAR(containers.created_at)'), [$from, $to])
                    ->whereBetween(DB::raw('YEAR(tracks.updated_at)'), [$from, $to])
                    ->where('tracks.agent_id','=',$cf)
                    ->orderByDesc("created_at")
                    ->paginate(10);


                return view('pendingContainer.documentation',compact('agent','destination','country','expenses','normal','bill','pending','ladding','cont_bill'));

            }
            else
            {
                $from = '2021';
                $to = '2030';
                $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
                $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

                $ladding = DB::table("laddings")->whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->pluck("bill_lading","id");

                $cont_bill = Container::select('containers.*')
                    ->join('transporters','transporters.container_id','=','containers.id')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->whereBetween(DB::raw('YEAR(containers.created_at)'), [$from, $to])
                    ->get();
                $destination = Destination::all();
                $country    = DB::table("countries")->pluck("country_name","id");

                $bill   =Container::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->get();
                $agent  = Agent::all();


                $pending    = Container::select('containers.*','bill_lading','current_status','tracks.updated_at as updateDate','organization_name','destination')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->join('tracks','tracks.id','=','laddings.track_id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('tracks.current_status', 'not like', '%' . 'Receiving the documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Under shipping line procedures' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Under customs procedures' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release order, Delivery order received' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release order/delivery order' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Prepare T1. / IM4 Documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release / Exit documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Lodge Port Charges' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Pay Port Charges' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Loading' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Container drop off' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'File Closed' . '%')
                    ->whereBetween(DB::raw('YEAR(containers.created_at)'), [$from, $to])
                    ->whereBetween(DB::raw('YEAR(tracks.updated_at)'), [$from, $to])
                    ->orderByDesc("created_at")
                    ->paginate(10);


                return view('pendingContainer.documentation',compact('agent','destination','country','expenses','normal','bill','pending','ladding','cont_bill'));


            }
        }
        else if($user->hasRole('shipping'))
        {
            if ($containers !='' AND $cf !='' AND $location !='')
            {


                $from = '2021';
                $to = '2030';
                $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
                $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

                $ladding = DB::table("laddings")->whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->pluck("bill_lading","id");

                $cont_bill = Container::select('containers.*')
                    ->join('transporters','transporters.container_id','=','containers.id')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->whereBetween(DB::raw('YEAR(containers.created_at)'), [$from, $to])
                    ->get();
                $destination = Destination::all();
                $country    = DB::table("countries")->pluck("country_name","id");

                $bill   =Container::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->get();
                $agent  = Agent::all();


                $pending    = Container::select('containers.*','bill_lading','current_status','tracks.updated_at as updateDate','organization_name','destination')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->join('tracks','tracks.id','=','laddings.track_id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('tracks.current_status', 'not like', '%' . 'Receiving the documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Under shipping line procedures' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Under customs procedures' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release order, Delivery order received' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release order/delivery order' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Prepare T1. / IM4 Documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release / Exit documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Lodge Port Charges' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Pay Port Charges' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Loading' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Container drop off' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'File Closed' . '%')
                    ->whereBetween(DB::raw('YEAR(containers.created_at)'), [$from, $to])
                    ->whereBetween(DB::raw('YEAR(tracks.updated_at)'), [$from, $to])
                    ->where('containers.id','=',$containers)
                    ->where('tracks.agent_id','=',$cf)
                    ->where('tracks.destination', 'like', '%' . $location . '%')
                    ->orderByDesc("created_at")
                    ->paginate(10);


                return view('pendingContainer.shipping',compact('agent','destination','country','expenses','normal','bill','pending','ladding','cont_bill'));

            }
            elseif ($containers !='' AND $cf !='' AND $location =='')
            {
                $from = '2021';
                $to = '2030';
                $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
                $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

                $ladding = DB::table("laddings")->whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->pluck("bill_lading","id");

                $cont_bill = Container::select('containers.*')
                    ->join('transporters','transporters.container_id','=','containers.id')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->whereBetween(DB::raw('YEAR(containers.created_at)'), [$from, $to])
                    ->get();
                $destination = Destination::all();
                $country    = DB::table("countries")->pluck("country_name","id");

                $bill   =Container::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->get();
                $agent  = Agent::all();


                $pending    = Container::select('containers.*','bill_lading','current_status','tracks.updated_at as updateDate','organization_name','destination')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->join('tracks','tracks.id','=','laddings.track_id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('tracks.current_status', 'not like', '%' . 'Receiving the documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Under shipping line procedures' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Under customs procedures' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release order, Delivery order received' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release order/delivery order' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Prepare T1. / IM4 Documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release / Exit documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Lodge Port Charges' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Pay Port Charges' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Loading' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Container drop off' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'File Closed' . '%')
                    ->whereBetween(DB::raw('YEAR(containers.created_at)'), [$from, $to])
                    ->whereBetween(DB::raw('YEAR(tracks.updated_at)'), [$from, $to])
                    ->where('containers.id','=',$containers)
                    ->where('tracks.agent_id','=',$cf)
                    ->orderByDesc("created_at")
                    ->paginate(10);


                return view('pendingContainer.shipping',compact('agent','destination','country','expenses','normal','bill','pending','ladding','cont_bill'));

            }
            elseif ($containers !='' AND $cf =='' AND $location !='')
            {
                $from = '2021';
                $to = '2030';
                $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
                $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

                $ladding = DB::table("laddings")->whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->pluck("bill_lading","id");

                $cont_bill = Container::select('containers.*')
                    ->join('transporters','transporters.container_id','=','containers.id')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->whereBetween(DB::raw('YEAR(containers.created_at)'), [$from, $to])
                    ->get();
                $destination = Destination::all();
                $country    = DB::table("countries")->pluck("country_name","id");

                $bill   =Container::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->get();
                $agent  = Agent::all();


                $pending    = Container::select('containers.*','bill_lading','current_status','tracks.updated_at as updateDate','organization_name','destination')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->join('tracks','tracks.id','=','laddings.track_id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('tracks.current_status', 'not like', '%' . 'Receiving the documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Under shipping line procedures' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Under customs procedures' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release order, Delivery order received' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release order/delivery order' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Prepare T1. / IM4 Documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release / Exit documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Lodge Port Charges' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Pay Port Charges' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Loading' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Container drop off' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'File Closed' . '%')
                    ->whereBetween(DB::raw('YEAR(containers.created_at)'), [$from, $to])
                    ->whereBetween(DB::raw('YEAR(tracks.updated_at)'), [$from, $to])
                    ->where('containers.id','=',$containers)
                    ->where('tracks.destination', 'like', '%' . $location . '%')
                    ->orderByDesc("created_at")
                    ->paginate(10);


                return view('pendingContainer.shipping',compact('agent','destination','country','expenses','normal','bill','pending','ladding','cont_bill'));

            }
            elseif ($containers !='' AND $cf =='' AND $location =='')
            {
                $from = '2021';
                $to = '2030';
                $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
                $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

                $ladding = DB::table("laddings")->whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->pluck("bill_lading","id");

                $cont_bill = Container::select('containers.*')
                    ->join('transporters','transporters.container_id','=','containers.id')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->whereBetween(DB::raw('YEAR(containers.created_at)'), [$from, $to])
                    ->get();
                $destination = Destination::all();
                $country    = DB::table("countries")->pluck("country_name","id");

                $bill   =Container::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->get();
                $agent  = Agent::all();


                $pending    = Container::select('containers.*','bill_lading','current_status','tracks.updated_at as updateDate','organization_name','destination')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->join('tracks','tracks.id','=','laddings.track_id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('tracks.current_status', 'not like', '%' . 'Receiving the documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Under shipping line procedures' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Under customs procedures' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release order, Delivery order received' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release order/delivery order' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Prepare T1. / IM4 Documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release / Exit documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Lodge Port Charges' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Pay Port Charges' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Loading' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Container drop off' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'File Closed' . '%')
                    ->whereBetween(DB::raw('YEAR(containers.created_at)'), [$from, $to])
                    ->whereBetween(DB::raw('YEAR(tracks.updated_at)'), [$from, $to])
                    ->where('containers.id','=',$containers)
                    ->orderByDesc("created_at")
                    ->paginate(10);


                return view('pendingContainer.shipping',compact('agent','destination','country','expenses','normal','bill','pending','ladding','cont_bill'));

            }
            elseif ($containers =='' AND $cf !='' AND $location !='')
            {
                $from = '2021';
                $to = '2030';
                $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
                $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

                $ladding = DB::table("laddings")->whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->pluck("bill_lading","id");

                $cont_bill = Container::select('containers.*')
                    ->join('transporters','transporters.container_id','=','containers.id')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->whereBetween(DB::raw('YEAR(containers.created_at)'), [$from, $to])
                    ->get();
                $destination = Destination::all();
                $country    = DB::table("countries")->pluck("country_name","id");

                $bill   =Container::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->get();
                $agent  = Agent::all();


                $pending    = Container::select('containers.*','bill_lading','current_status','tracks.updated_at as updateDate','organization_name','destination')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->join('tracks','tracks.id','=','laddings.track_id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('tracks.current_status', 'not like', '%' . 'Receiving the documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Under shipping line procedures' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Under customs procedures' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release order, Delivery order received' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release order/delivery order' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Prepare T1. / IM4 Documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release / Exit documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Lodge Port Charges' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Pay Port Charges' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Loading' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Container drop off' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'File Closed' . '%')
                    ->whereBetween(DB::raw('YEAR(containers.created_at)'), [$from, $to])
                    ->whereBetween(DB::raw('YEAR(tracks.updated_at)'), [$from, $to])
                    ->where('tracks.agent_id','=',$cf)
                    ->where('tracks.destination', 'like', '%' . $location . '%')
                    ->orderByDesc("created_at")
                    ->paginate(10);


                return view('pendingContainer.shipping',compact('agent','destination','country','expenses','normal','bill','pending','ladding','cont_bill'));

            }
            elseif ($containers =='' AND $cf =='' AND $location !='')
            {
                $from = '2021';
                $to = '2030';
                $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
                $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

                $ladding = DB::table("laddings")->whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->pluck("bill_lading","id");

                $cont_bill = Container::select('containers.*')
                    ->join('transporters','transporters.container_id','=','containers.id')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->whereBetween(DB::raw('YEAR(containers.created_at)'), [$from, $to])
                    ->get();
                $destination = Destination::all();
                $country    = DB::table("countries")->pluck("country_name","id");

                $bill   =Container::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->get();
                $agent  = Agent::all();


                $pending    = Container::select('containers.*','bill_lading','current_status','tracks.updated_at as updateDate','organization_name','destination')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->join('tracks','tracks.id','=','laddings.track_id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('tracks.current_status', 'not like', '%' . 'Receiving the documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Under shipping line procedures' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Under customs procedures' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release order, Delivery order received' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release order/delivery order' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Prepare T1. / IM4 Documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release / Exit documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Lodge Port Charges' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Pay Port Charges' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Loading' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Container drop off' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'File Closed' . '%')
                    ->whereBetween(DB::raw('YEAR(containers.created_at)'), [$from, $to])
                    ->whereBetween(DB::raw('YEAR(tracks.updated_at)'), [$from, $to])
                    ->where('tracks.destination', 'like', '%' . $location . '%')
                    ->orderByDesc("created_at")
                    ->paginate(10);


                return view('pendingContainer.shipping',compact('agent','destination','country','expenses','normal','bill','pending','ladding','cont_bill'));

            }
            elseif ($containers =='' AND $cf !='' AND $location =='')
            {
                $from = '2021';
                $to = '2030';
                $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
                $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

                $ladding = DB::table("laddings")->whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->pluck("bill_lading","id");

                $cont_bill = Container::select('containers.*')
                    ->join('transporters','transporters.container_id','=','containers.id')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->whereBetween(DB::raw('YEAR(containers.created_at)'), [$from, $to])
                    ->get();
                $destination = Destination::all();
                $country    = DB::table("countries")->pluck("country_name","id");

                $bill   =Container::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->get();
                $agent  = Agent::all();


                $pending    = Container::select('containers.*','bill_lading','current_status','tracks.updated_at as updateDate','organization_name','destination')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->join('tracks','tracks.id','=','laddings.track_id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('tracks.current_status', 'not like', '%' . 'Receiving the documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Under shipping line procedures' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Under customs procedures' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release order, Delivery order received' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release order/delivery order' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Prepare T1. / IM4 Documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release / Exit documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Lodge Port Charges' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Pay Port Charges' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Loading' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Container drop off' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'File Closed' . '%')
                    ->whereBetween(DB::raw('YEAR(containers.created_at)'), [$from, $to])
                    ->whereBetween(DB::raw('YEAR(tracks.updated_at)'), [$from, $to])
                    ->where('tracks.agent_id','=',$cf)
                    ->orderByDesc("created_at")
                    ->paginate(10);


                return view('pendingContainer.shipping',compact('agent','destination','country','expenses','normal','bill','pending','ladding','cont_bill'));

            }
            else
            {
                $from = '2021';
                $to = '2030';
                $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
                $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

                $ladding = DB::table("laddings")->whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->pluck("bill_lading","id");

                $cont_bill = Container::select('containers.*')
                    ->join('transporters','transporters.container_id','=','containers.id')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->whereBetween(DB::raw('YEAR(containers.created_at)'), [$from, $to])
                    ->get();
                $destination = Destination::all();
                $country    = DB::table("countries")->pluck("country_name","id");

                $bill   =Container::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->get();
                $agent  = Agent::all();


                $pending    = Container::select('containers.*','bill_lading','current_status','tracks.updated_at as updateDate','organization_name','destination')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->join('tracks','tracks.id','=','laddings.track_id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('tracks.current_status', 'not like', '%' . 'Receiving the documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Under shipping line procedures' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Under customs procedures' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release order, Delivery order received' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release order/delivery order' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Prepare T1. / IM4 Documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release / Exit documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Lodge Port Charges' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Pay Port Charges' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Loading' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Container drop off' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'File Closed' . '%')
                    ->whereBetween(DB::raw('YEAR(containers.created_at)'), [$from, $to])
                    ->whereBetween(DB::raw('YEAR(tracks.updated_at)'), [$from, $to])
                    ->orderByDesc("created_at")
                    ->paginate(10);


                return view('pendingContainer.shipping',compact('agent','destination','country','expenses','normal','bill','pending','ladding','cont_bill'));


            }
        }
        else if($user->hasRole('sales'))
        {
            if ($containers !='' AND $cf !='' AND $location !='')
            {


                $from = '2021';
                $to = '2030';
                $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
                $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

                $ladding = DB::table("laddings")->whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->pluck("bill_lading","id");

                $cont_bill = Container::select('containers.*')
                    ->join('transporters','transporters.container_id','=','containers.id')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->whereBetween(DB::raw('YEAR(containers.created_at)'), [$from, $to])
                    ->get();
                $destination = Destination::all();
                $country    = DB::table("countries")->pluck("country_name","id");

                $bill   =Container::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->get();
                $agent  = Agent::all();


                $pending    = Container::select('containers.*','bill_lading','current_status','tracks.updated_at as updateDate','organization_name','destination')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->join('tracks','tracks.id','=','laddings.track_id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('tracks.current_status', 'not like', '%' . 'Receiving the documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Under shipping line procedures' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Under customs procedures' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release order, Delivery order received' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release order/delivery order' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Prepare T1. / IM4 Documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release / Exit documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Lodge Port Charges' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Pay Port Charges' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Loading' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Container drop off' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'File Closed' . '%')
                    ->whereBetween(DB::raw('YEAR(containers.created_at)'), [$from, $to])
                    ->whereBetween(DB::raw('YEAR(tracks.updated_at)'), [$from, $to])
                    ->where('containers.id','=',$containers)
                    ->where('tracks.agent_id','=',$cf)
                    ->where('tracks.destination', 'like', '%' . $location . '%')
                    ->orderByDesc("created_at")
                    ->paginate(10);


                return view('pendingContainer.sales',compact('agent','destination','country','expenses','normal','bill','pending','ladding','cont_bill'));

            }
            elseif ($containers !='' AND $cf !='' AND $location =='')
            {
                $from = '2021';
                $to = '2030';
                $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
                $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

                $ladding = DB::table("laddings")->whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->pluck("bill_lading","id");

                $cont_bill = Container::select('containers.*')
                    ->join('transporters','transporters.container_id','=','containers.id')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->whereBetween(DB::raw('YEAR(containers.created_at)'), [$from, $to])
                    ->get();
                $destination = Destination::all();
                $country    = DB::table("countries")->pluck("country_name","id");

                $bill   =Container::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->get();
                $agent  = Agent::all();


                $pending    = Container::select('containers.*','bill_lading','current_status','tracks.updated_at as updateDate','organization_name','destination')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->join('tracks','tracks.id','=','laddings.track_id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('tracks.current_status', 'not like', '%' . 'Receiving the documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Under shipping line procedures' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Under customs procedures' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release order, Delivery order received' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release order/delivery order' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Prepare T1. / IM4 Documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release / Exit documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Lodge Port Charges' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Pay Port Charges' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Loading' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Container drop off' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'File Closed' . '%')
                    ->whereBetween(DB::raw('YEAR(containers.created_at)'), [$from, $to])
                    ->whereBetween(DB::raw('YEAR(tracks.updated_at)'), [$from, $to])
                    ->where('containers.id','=',$containers)
                    ->where('tracks.agent_id','=',$cf)
                    ->orderByDesc("created_at")
                    ->paginate(10);


                return view('pendingContainer.sales',compact('agent','destination','country','expenses','normal','bill','pending','ladding','cont_bill'));

            }
            elseif ($containers !='' AND $cf =='' AND $location !='')
            {
                $from = '2021';
                $to = '2030';
                $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
                $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

                $ladding = DB::table("laddings")->whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->pluck("bill_lading","id");

                $cont_bill = Container::select('containers.*')
                    ->join('transporters','transporters.container_id','=','containers.id')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->whereBetween(DB::raw('YEAR(containers.created_at)'), [$from, $to])
                    ->get();
                $destination = Destination::all();
                $country    = DB::table("countries")->pluck("country_name","id");

                $bill   =Container::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->get();
                $agent  = Agent::all();


                $pending    = Container::select('containers.*','bill_lading','current_status','tracks.updated_at as updateDate','organization_name','destination')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->join('tracks','tracks.id','=','laddings.track_id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('tracks.current_status', 'not like', '%' . 'Receiving the documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Under shipping line procedures' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Under customs procedures' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release order, Delivery order received' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release order/delivery order' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Prepare T1. / IM4 Documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release / Exit documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Lodge Port Charges' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Pay Port Charges' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Loading' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Container drop off' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'File Closed' . '%')
                    ->whereBetween(DB::raw('YEAR(containers.created_at)'), [$from, $to])
                    ->whereBetween(DB::raw('YEAR(tracks.updated_at)'), [$from, $to])
                    ->where('containers.id','=',$containers)
                    ->where('tracks.destination', 'like', '%' . $location . '%')
                    ->orderByDesc("created_at")
                    ->paginate(10);


                return view('pendingContainer.sales',compact('agent','destination','country','expenses','normal','bill','pending','ladding','cont_bill'));

            }
            elseif ($containers !='' AND $cf =='' AND $location =='')
            {
                $from = '2021';
                $to = '2030';
                $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
                $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

                $ladding = DB::table("laddings")->whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->pluck("bill_lading","id");

                $cont_bill = Container::select('containers.*')
                    ->join('transporters','transporters.container_id','=','containers.id')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->whereBetween(DB::raw('YEAR(containers.created_at)'), [$from, $to])
                    ->get();
                $destination = Destination::all();
                $country    = DB::table("countries")->pluck("country_name","id");

                $bill   =Container::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->get();
                $agent  = Agent::all();


                $pending    = Container::select('containers.*','bill_lading','current_status','tracks.updated_at as updateDate','organization_name','destination')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->join('tracks','tracks.id','=','laddings.track_id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('tracks.current_status', 'not like', '%' . 'Receiving the documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Under shipping line procedures' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Under customs procedures' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release order, Delivery order received' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release order/delivery order' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Prepare T1. / IM4 Documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release / Exit documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Lodge Port Charges' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Pay Port Charges' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Loading' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Container drop off' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'File Closed' . '%')
                    ->whereBetween(DB::raw('YEAR(containers.created_at)'), [$from, $to])
                    ->whereBetween(DB::raw('YEAR(tracks.updated_at)'), [$from, $to])
                    ->where('containers.id','=',$containers)
                    ->orderByDesc("created_at")
                    ->paginate(10);


                return view('pendingContainer.sales',compact('agent','destination','country','expenses','normal','bill','pending','ladding','cont_bill'));

            }
            elseif ($containers =='' AND $cf !='' AND $location !='')
            {
                $from = '2021';
                $to = '2030';
                $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
                $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

                $ladding = DB::table("laddings")->whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->pluck("bill_lading","id");

                $cont_bill = Container::select('containers.*')
                    ->join('transporters','transporters.container_id','=','containers.id')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->whereBetween(DB::raw('YEAR(containers.created_at)'), [$from, $to])
                    ->get();
                $destination = Destination::all();
                $country    = DB::table("countries")->pluck("country_name","id");

                $bill   =Container::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->get();
                $agent  = Agent::all();


                $pending    = Container::select('containers.*','bill_lading','current_status','tracks.updated_at as updateDate','organization_name','destination')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->join('tracks','tracks.id','=','laddings.track_id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('tracks.current_status', 'not like', '%' . 'Receiving the documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Under shipping line procedures' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Under customs procedures' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release order, Delivery order received' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release order/delivery order' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Prepare T1. / IM4 Documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release / Exit documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Lodge Port Charges' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Pay Port Charges' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Loading' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Container drop off' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'File Closed' . '%')
                    ->whereBetween(DB::raw('YEAR(containers.created_at)'), [$from, $to])
                    ->whereBetween(DB::raw('YEAR(tracks.updated_at)'), [$from, $to])
                    ->where('tracks.agent_id','=',$cf)
                    ->where('tracks.destination', 'like', '%' . $location . '%')
                    ->orderByDesc("created_at")
                    ->paginate(10);


                return view('pendingContainer.sales',compact('agent','destination','country','expenses','normal','bill','pending','ladding','cont_bill'));

            }
            elseif ($containers =='' AND $cf =='' AND $location !='')
            {
                $from = '2021';
                $to = '2030';
                $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
                $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

                $ladding = DB::table("laddings")->whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->pluck("bill_lading","id");

                $cont_bill = Container::select('containers.*')
                    ->join('transporters','transporters.container_id','=','containers.id')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->whereBetween(DB::raw('YEAR(containers.created_at)'), [$from, $to])
                    ->get();
                $destination = Destination::all();
                $country    = DB::table("countries")->pluck("country_name","id");

                $bill   =Container::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->get();
                $agent  = Agent::all();


                $pending    = Container::select('containers.*','bill_lading','current_status','tracks.updated_at as updateDate','organization_name','destination')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->join('tracks','tracks.id','=','laddings.track_id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('tracks.current_status', 'not like', '%' . 'Receiving the documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Under shipping line procedures' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Under customs procedures' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release order, Delivery order received' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release order/delivery order' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Prepare T1. / IM4 Documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release / Exit documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Lodge Port Charges' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Pay Port Charges' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Loading' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Container drop off' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'File Closed' . '%')
                    ->whereBetween(DB::raw('YEAR(containers.created_at)'), [$from, $to])
                    ->whereBetween(DB::raw('YEAR(tracks.updated_at)'), [$from, $to])
                    ->where('tracks.destination', 'like', '%' . $location . '%')
                    ->orderByDesc("created_at")
                    ->paginate(10);


                return view('pendingContainer.sales',compact('agent','destination','country','expenses','normal','bill','pending','ladding','cont_bill'));

            }
            elseif ($containers =='' AND $cf !='' AND $location =='')
            {
                $from = '2021';
                $to = '2030';
                $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
                $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

                $ladding = DB::table("laddings")->whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->pluck("bill_lading","id");

                $cont_bill = Container::select('containers.*')
                    ->join('transporters','transporters.container_id','=','containers.id')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->whereBetween(DB::raw('YEAR(containers.created_at)'), [$from, $to])
                    ->get();
                $destination = Destination::all();
                $country    = DB::table("countries")->pluck("country_name","id");

                $bill   =Container::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->get();
                $agent  = Agent::all();


                $pending    = Container::select('containers.*','bill_lading','current_status','tracks.updated_at as updateDate','organization_name','destination')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->join('tracks','tracks.id','=','laddings.track_id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('tracks.current_status', 'not like', '%' . 'Receiving the documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Under shipping line procedures' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Under customs procedures' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release order, Delivery order received' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release order/delivery order' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Prepare T1. / IM4 Documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release / Exit documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Lodge Port Charges' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Pay Port Charges' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Loading' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Container drop off' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'File Closed' . '%')
                    ->whereBetween(DB::raw('YEAR(containers.created_at)'), [$from, $to])
                    ->whereBetween(DB::raw('YEAR(tracks.updated_at)'), [$from, $to])
                    ->where('tracks.agent_id','=',$cf)
                    ->orderByDesc("created_at")
                    ->paginate(10);


                return view('pendingContainer.sales',compact('agent','destination','country','expenses','normal','bill','pending','ladding','cont_bill'));

            }
            else
            {
                $from = '2021';
                $to = '2030';
                $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
                $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

                $ladding = DB::table("laddings")->whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->pluck("bill_lading","id");

                $cont_bill = Container::select('containers.*')
                    ->join('transporters','transporters.container_id','=','containers.id')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->whereBetween(DB::raw('YEAR(containers.created_at)'), [$from, $to])
                    ->get();
                $destination = Destination::all();
                $country    = DB::table("countries")->pluck("country_name","id");

                $bill   =Container::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->get();
                $agent  = Agent::all();


                $pending    = Container::select('containers.*','bill_lading','current_status','tracks.updated_at as updateDate','organization_name','destination')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->join('tracks','tracks.id','=','laddings.track_id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('tracks.current_status', 'not like', '%' . 'Receiving the documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Under shipping line procedures' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Under customs procedures' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release order, Delivery order received' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release order/delivery order' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Prepare T1. / IM4 Documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release / Exit documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Lodge Port Charges' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Pay Port Charges' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Loading' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Container drop off' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'File Closed' . '%')
                    ->whereBetween(DB::raw('YEAR(containers.created_at)'), [$from, $to])
                    ->whereBetween(DB::raw('YEAR(tracks.updated_at)'), [$from, $to])
                    ->orderByDesc("created_at")
                    ->paginate(10);


                return view('pendingContainer.sales',compact('agent','destination','country','expenses','normal','bill','pending','ladding','cont_bill'));


            }
        }
        else if($user->hasRole('finance'))
        {
            if ($containers !='' AND $cf !='' AND $location !='')
            {


                $from = '2021';
                $to = '2030';
                $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
                $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

                $ladding = DB::table("laddings")->whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->pluck("bill_lading","id");

                $cont_bill = Container::select('containers.*')
                    ->join('transporters','transporters.container_id','=','containers.id')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->whereBetween(DB::raw('YEAR(containers.created_at)'), [$from, $to])
                    ->get();
                $destination = Destination::all();
                $country    = DB::table("countries")->pluck("country_name","id");

                $bill   =Container::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->get();
                $agent  = Agent::all();


                $pending    = Container::select('containers.*','bill_lading','current_status','tracks.updated_at as updateDate','organization_name','destination')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->join('tracks','tracks.id','=','laddings.track_id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('tracks.current_status', 'not like', '%' . 'Receiving the documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Under shipping line procedures' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Under customs procedures' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release order, Delivery order received' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release order/delivery order' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Prepare T1. / IM4 Documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release / Exit documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Lodge Port Charges' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Pay Port Charges' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Loading' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Container drop off' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'File Closed' . '%')
                    ->whereBetween(DB::raw('YEAR(containers.created_at)'), [$from, $to])
                    ->whereBetween(DB::raw('YEAR(tracks.updated_at)'), [$from, $to])
                    ->where('containers.id','=',$containers)
                    ->where('tracks.agent_id','=',$cf)
                    ->where('tracks.destination', 'like', '%' . $location . '%')
                    ->orderByDesc("created_at")
                    ->paginate(10);


                return view('pendingContainer.finance',compact('agent','destination','country','expenses','normal','bill','pending','ladding','cont_bill'));

            }
            elseif ($containers !='' AND $cf !='' AND $location =='')
            {
                $from = '2021';
                $to = '2030';
                $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
                $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

                $ladding = DB::table("laddings")->whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->pluck("bill_lading","id");

                $cont_bill = Container::select('containers.*')
                    ->join('transporters','transporters.container_id','=','containers.id')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->whereBetween(DB::raw('YEAR(containers.created_at)'), [$from, $to])
                    ->get();
                $destination = Destination::all();
                $country    = DB::table("countries")->pluck("country_name","id");

                $bill   =Container::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->get();
                $agent  = Agent::all();


                $pending    = Container::select('containers.*','bill_lading','current_status','tracks.updated_at as updateDate','organization_name','destination')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->join('tracks','tracks.id','=','laddings.track_id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('tracks.current_status', 'not like', '%' . 'Receiving the documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Under shipping line procedures' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Under customs procedures' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release order, Delivery order received' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release order/delivery order' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Prepare T1. / IM4 Documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release / Exit documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Lodge Port Charges' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Pay Port Charges' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Loading' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Container drop off' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'File Closed' . '%')
                    ->whereBetween(DB::raw('YEAR(containers.created_at)'), [$from, $to])
                    ->whereBetween(DB::raw('YEAR(tracks.updated_at)'), [$from, $to])
                    ->where('containers.id','=',$containers)
                    ->where('tracks.agent_id','=',$cf)
                    ->orderByDesc("created_at")
                    ->paginate(10);


                return view('pendingContainer.finance',compact('agent','destination','country','expenses','normal','bill','pending','ladding','cont_bill'));

            }
            elseif ($containers !='' AND $cf =='' AND $location !='')
            {
                $from = '2021';
                $to = '2030';
                $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
                $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

                $ladding = DB::table("laddings")->whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->pluck("bill_lading","id");

                $cont_bill = Container::select('containers.*')
                    ->join('transporters','transporters.container_id','=','containers.id')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->whereBetween(DB::raw('YEAR(containers.created_at)'), [$from, $to])
                    ->get();
                $destination = Destination::all();
                $country    = DB::table("countries")->pluck("country_name","id");

                $bill   =Container::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->get();
                $agent  = Agent::all();


                $pending    = Container::select('containers.*','bill_lading','current_status','tracks.updated_at as updateDate','organization_name','destination')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->join('tracks','tracks.id','=','laddings.track_id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('tracks.current_status', 'not like', '%' . 'Receiving the documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Under shipping line procedures' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Under customs procedures' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release order, Delivery order received' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release order/delivery order' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Prepare T1. / IM4 Documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release / Exit documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Lodge Port Charges' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Pay Port Charges' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Loading' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Container drop off' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'File Closed' . '%')
                    ->whereBetween(DB::raw('YEAR(containers.created_at)'), [$from, $to])
                    ->whereBetween(DB::raw('YEAR(tracks.updated_at)'), [$from, $to])
                    ->where('containers.id','=',$containers)
                    ->where('tracks.destination', 'like', '%' . $location . '%')
                    ->orderByDesc("created_at")
                    ->paginate(10);


                return view('pendingContainer.finance',compact('agent','destination','country','expenses','normal','bill','pending','ladding','cont_bill'));

            }
            elseif ($containers !='' AND $cf =='' AND $location =='')
            {
                $from = '2021';
                $to = '2030';
                $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
                $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

                $ladding = DB::table("laddings")->whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->pluck("bill_lading","id");

                $cont_bill = Container::select('containers.*')
                    ->join('transporters','transporters.container_id','=','containers.id')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->whereBetween(DB::raw('YEAR(containers.created_at)'), [$from, $to])
                    ->get();
                $destination = Destination::all();
                $country    = DB::table("countries")->pluck("country_name","id");

                $bill   =Container::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->get();
                $agent  = Agent::all();


                $pending    = Container::select('containers.*','bill_lading','current_status','tracks.updated_at as updateDate','organization_name','destination')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->join('tracks','tracks.id','=','laddings.track_id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('tracks.current_status', 'not like', '%' . 'Receiving the documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Under shipping line procedures' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Under customs procedures' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release order, Delivery order received' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release order/delivery order' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Prepare T1. / IM4 Documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release / Exit documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Lodge Port Charges' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Pay Port Charges' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Loading' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Container drop off' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'File Closed' . '%')
                    ->whereBetween(DB::raw('YEAR(containers.created_at)'), [$from, $to])
                    ->whereBetween(DB::raw('YEAR(tracks.updated_at)'), [$from, $to])
                    ->where('containers.id','=',$containers)
                    ->orderByDesc("created_at")
                    ->paginate(10);


                return view('pendingContainer.finance',compact('agent','destination','country','expenses','normal','bill','pending','ladding','cont_bill'));

            }
            elseif ($containers =='' AND $cf !='' AND $location !='')
            {
                $from = '2021';
                $to = '2030';
                $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
                $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

                $ladding = DB::table("laddings")->whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->pluck("bill_lading","id");

                $cont_bill = Container::select('containers.*')
                    ->join('transporters','transporters.container_id','=','containers.id')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->whereBetween(DB::raw('YEAR(containers.created_at)'), [$from, $to])
                    ->get();
                $destination = Destination::all();
                $country    = DB::table("countries")->pluck("country_name","id");

                $bill   =Container::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->get();
                $agent  = Agent::all();


                $pending    = Container::select('containers.*','bill_lading','current_status','tracks.updated_at as updateDate','organization_name','destination')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->join('tracks','tracks.id','=','laddings.track_id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('tracks.current_status', 'not like', '%' . 'Receiving the documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Under shipping line procedures' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Under customs procedures' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release order, Delivery order received' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release order/delivery order' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Prepare T1. / IM4 Documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release / Exit documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Lodge Port Charges' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Pay Port Charges' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Loading' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Container drop off' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'File Closed' . '%')
                    ->whereBetween(DB::raw('YEAR(containers.created_at)'), [$from, $to])
                    ->whereBetween(DB::raw('YEAR(tracks.updated_at)'), [$from, $to])
                    ->where('tracks.agent_id','=',$cf)
                    ->where('tracks.destination', 'like', '%' . $location . '%')
                    ->orderByDesc("created_at")
                    ->paginate(10);


                return view('pendingContainer.finance',compact('agent','destination','country','expenses','normal','bill','pending','ladding','cont_bill'));

            }
            elseif ($containers =='' AND $cf =='' AND $location !='')
            {
                $from = '2021';
                $to = '2030';
                $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
                $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

                $ladding = DB::table("laddings")->whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->pluck("bill_lading","id");

                $cont_bill = Container::select('containers.*')
                    ->join('transporters','transporters.container_id','=','containers.id')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->whereBetween(DB::raw('YEAR(containers.created_at)'), [$from, $to])
                    ->get();
                $destination = Destination::all();
                $country    = DB::table("countries")->pluck("country_name","id");

                $bill   =Container::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->get();
                $agent  = Agent::all();


                $pending    = Container::select('containers.*','bill_lading','current_status','tracks.updated_at as updateDate','organization_name','destination')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->join('tracks','tracks.id','=','laddings.track_id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('tracks.current_status', 'not like', '%' . 'Receiving the documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Under shipping line procedures' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Under customs procedures' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release order, Delivery order received' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release order/delivery order' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Prepare T1. / IM4 Documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release / Exit documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Lodge Port Charges' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Pay Port Charges' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Loading' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Container drop off' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'File Closed' . '%')
                    ->whereBetween(DB::raw('YEAR(containers.created_at)'), [$from, $to])
                    ->whereBetween(DB::raw('YEAR(tracks.updated_at)'), [$from, $to])
                    ->where('tracks.destination', 'like', '%' . $location . '%')
                    ->orderByDesc("created_at")
                    ->paginate(10);


                return view('pendingContainer.finance',compact('agent','destination','country','expenses','normal','bill','pending','ladding','cont_bill'));

            }
            elseif ($containers =='' AND $cf !='' AND $location =='')
            {
                $from = '2021';
                $to = '2030';
                $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
                $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

                $ladding = DB::table("laddings")->whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->pluck("bill_lading","id");

                $cont_bill = Container::select('containers.*')
                    ->join('transporters','transporters.container_id','=','containers.id')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->whereBetween(DB::raw('YEAR(containers.created_at)'), [$from, $to])
                    ->get();
                $destination = Destination::all();
                $country    = DB::table("countries")->pluck("country_name","id");

                $bill   =Container::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->get();
                $agent  = Agent::all();


                $pending    = Container::select('containers.*','bill_lading','current_status','tracks.updated_at as updateDate','organization_name','destination')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->join('tracks','tracks.id','=','laddings.track_id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('tracks.current_status', 'not like', '%' . 'Receiving the documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Under shipping line procedures' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Under customs procedures' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release order, Delivery order received' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release order/delivery order' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Prepare T1. / IM4 Documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release / Exit documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Lodge Port Charges' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Pay Port Charges' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Loading' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Container drop off' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'File Closed' . '%')
                    ->whereBetween(DB::raw('YEAR(containers.created_at)'), [$from, $to])
                    ->where('tracks.agent_id','=',$cf)
                    ->orderByDesc("created_at")
                    ->paginate(10);


                return view('pendingContainer.finance',compact('agent','destination','country','expenses','normal','bill','pending','ladding','cont_bill'));

            }
            else
            {
                $from = '2021';
                $to = '2030';
                $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
                $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

                $ladding = DB::table("laddings")->whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->pluck("bill_lading","id");

                $cont_bill = Container::select('containers.*')
                    ->join('transporters','transporters.container_id','=','containers.id')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->whereBetween(DB::raw('YEAR(containers.created_at)'), [$from, $to])
                    ->get();
                $destination = Destination::all();
                $country    = DB::table("countries")->pluck("country_name","id");

                $bill   =Container::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->get();
                $agent  = Agent::all();


                $pending    = Container::select('containers.*','bill_lading','current_status','tracks.updated_at as updateDate','organization_name','destination')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->join('tracks','tracks.id','=','laddings.track_id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('tracks.current_status', 'not like', '%' . 'Receiving the documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Under shipping line procedures' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Under customs procedures' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release order, Delivery order received' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release order/delivery order' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Prepare T1. / IM4 Documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release / Exit documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Lodge Port Charges' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Pay Port Charges' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Loading' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Container drop off' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'File Closed' . '%')
                    ->whereBetween(DB::raw('YEAR(containers.created_at)'), [$from, $to])
                    ->whereBetween(DB::raw('YEAR(tracks.updated_at)'), [$from, $to])
                    ->orderByDesc("created_at")
                    ->paginate(10);


                return view('pendingContainer.finance',compact('agent','destination','country','expenses','normal','bill','pending','ladding','cont_bill'));


            }
        }
        else if($user->hasRole('declaration'))
        {
            if ($containers !='' AND $cf !='' AND $location !='')
            {


                $from = '2021';
                $to = '2030';
                $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
                $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

                $ladding = DB::table("laddings")->whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->pluck("bill_lading","id");

                $cont_bill = Container::select('containers.*')
                    ->join('transporters','transporters.container_id','=','containers.id')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->whereBetween(DB::raw('YEAR(containers.created_at)'), [$from, $to])
                    ->get();
                $destination = Destination::all();
                $country    = DB::table("countries")->pluck("country_name","id");

                $bill   =Container::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->get();
                $agent  = Agent::all();


                $pending    = Container::select('containers.*','bill_lading','current_status','tracks.updated_at as updateDate','organization_name','destination')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->join('tracks','tracks.id','=','laddings.track_id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('tracks.current_status', 'not like', '%' . 'Receiving the documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Under shipping line procedures' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Under customs procedures' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release order, Delivery order received' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release order/delivery order' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Prepare T1. / IM4 Documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release / Exit documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Lodge Port Charges' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Pay Port Charges' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Loading' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Container drop off' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'File Closed' . '%')
                    ->whereBetween(DB::raw('YEAR(containers.created_at)'), [$from, $to])
                    ->whereBetween(DB::raw('YEAR(tracks.updated_at)'), [$from, $to])
                    ->where('containers.id','=',$containers)
                    ->where('tracks.agent_id','=',$cf)
                    ->where('tracks.destination', 'like', '%' . $location . '%')
                    ->orderByDesc("created_at")
                    ->paginate(10);


                return view('pendingContainer.declaration',compact('agent','destination','country','expenses','normal','bill','pending','ladding','cont_bill'));

            }
            elseif ($containers !='' AND $cf !='' AND $location =='')
            {
                $from = '2021';
                $to = '2030';
                $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
                $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

                $ladding = DB::table("laddings")->whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->pluck("bill_lading","id");

                $cont_bill = Container::select('containers.*')
                    ->join('transporters','transporters.container_id','=','containers.id')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->whereBetween(DB::raw('YEAR(containers.created_at)'), [$from, $to])
                    ->get();
                $destination = Destination::all();
                $country    = DB::table("countries")->pluck("country_name","id");

                $bill   =Container::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->get();
                $agent  = Agent::all();


                $pending    = Container::select('containers.*','bill_lading','current_status','tracks.updated_at as updateDate','organization_name','destination')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->join('tracks','tracks.id','=','laddings.track_id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('tracks.current_status', 'not like', '%' . 'Receiving the documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Under shipping line procedures' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Under customs procedures' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release order, Delivery order received' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release order/delivery order' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Prepare T1. / IM4 Documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release / Exit documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Lodge Port Charges' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Pay Port Charges' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Loading' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Container drop off' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'File Closed' . '%')
                    ->whereBetween(DB::raw('YEAR(containers.created_at)'), [$from, $to])
                    ->whereBetween(DB::raw('YEAR(tracks.updated_at)'), [$from, $to])
                    ->where('containers.id','=',$containers)
                    ->where('tracks.agent_id','=',$cf)
                    ->orderByDesc("created_at")
                    ->paginate(10);


                return view('pendingContainer.declaration',compact('agent','destination','country','expenses','normal','bill','pending','ladding','cont_bill'));

            }
            elseif ($containers !='' AND $cf =='' AND $location !='')
            {
                $from = '2021';
                $to = '2030';
                $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
                $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

                $ladding = DB::table("laddings")->whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->pluck("bill_lading","id");

                $cont_bill = Container::select('containers.*')
                    ->join('transporters','transporters.container_id','=','containers.id')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->whereBetween(DB::raw('YEAR(containers.created_at)'), [$from, $to])
                    ->get();
                $destination = Destination::all();
                $country    = DB::table("countries")->pluck("country_name","id");

                $bill   =Container::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->get();
                $agent  = Agent::all();


                $pending    = Container::select('containers.*','bill_lading','current_status','tracks.updated_at as updateDate','organization_name','destination')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->join('tracks','tracks.id','=','laddings.track_id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('tracks.current_status', 'not like', '%' . 'Receiving the documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Under shipping line procedures' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Under customs procedures' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release order, Delivery order received' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release order/delivery order' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Prepare T1. / IM4 Documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release / Exit documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Lodge Port Charges' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Pay Port Charges' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Loading' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Container drop off' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'File Closed' . '%')
                    ->whereBetween(DB::raw('YEAR(containers.created_at)'), [$from, $to])
                    ->whereBetween(DB::raw('YEAR(tracks.updated_at)'), [$from, $to])
                    ->where('containers.id','=',$containers)
                    ->where('tracks.destination', 'like', '%' . $location . '%')
                    ->orderByDesc("created_at")
                    ->paginate(10);


                return view('pendingContainer.declaration',compact('agent','destination','country','expenses','normal','bill','pending','ladding','cont_bill'));

            }
            elseif ($containers !='' AND $cf =='' AND $location =='')
            {
                $from = '2021';
                $to = '2030';
                $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
                $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

                $ladding = DB::table("laddings")->whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->pluck("bill_lading","id");

                $cont_bill = Container::select('containers.*')
                    ->join('transporters','transporters.container_id','=','containers.id')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->whereBetween(DB::raw('YEAR(containers.created_at)'), [$from, $to])
                    ->get();
                $destination = Destination::all();
                $country    = DB::table("countries")->pluck("country_name","id");

                $bill   =Container::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->get();
                $agent  = Agent::all();


                $pending    = Container::select('containers.*','bill_lading','current_status','tracks.updated_at as updateDate','organization_name','destination')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->join('tracks','tracks.id','=','laddings.track_id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('tracks.current_status', 'not like', '%' . 'Receiving the documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Under shipping line procedures' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Under customs procedures' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release order, Delivery order received' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release order/delivery order' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Prepare T1. / IM4 Documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release / Exit documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Lodge Port Charges' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Pay Port Charges' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Loading' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Container drop off' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'File Closed' . '%')
                    ->whereBetween(DB::raw('YEAR(containers.created_at)'), [$from, $to])
                    ->whereBetween(DB::raw('YEAR(tracks.updated_at)'), [$from, $to])
                    ->where('containers.id','=',$containers)
                    ->orderByDesc("created_at")
                    ->paginate(10);


                return view('pendingContainer.declaration',compact('agent','destination','country','expenses','normal','bill','pending','ladding','cont_bill'));

            }
            elseif ($containers =='' AND $cf !='' AND $location !='')
            {
                $from = '2021';
                $to = '2030';
                $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
                $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

                $ladding = DB::table("laddings")->whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->pluck("bill_lading","id");

                $cont_bill = Container::select('containers.*')
                    ->join('transporters','transporters.container_id','=','containers.id')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->whereBetween(DB::raw('YEAR(containers.created_at)'), [$from, $to])
                    ->get();
                $destination = Destination::all();
                $country    = DB::table("countries")->pluck("country_name","id");

                $bill   =Container::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->get();
                $agent  = Agent::all();


                $pending    = Container::select('containers.*','bill_lading','current_status','tracks.updated_at as updateDate','organization_name','destination')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->join('tracks','tracks.id','=','laddings.track_id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('tracks.current_status', 'not like', '%' . 'Receiving the documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Under shipping line procedures' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Under customs procedures' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release order, Delivery order received' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release order/delivery order' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Prepare T1. / IM4 Documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release / Exit documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Lodge Port Charges' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Pay Port Charges' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Loading' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Container drop off' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'File Closed' . '%')
                    ->whereBetween(DB::raw('YEAR(containers.created_at)'), [$from, $to])
                    ->whereBetween(DB::raw('YEAR(tracks.updated_at)'), [$from, $to])
                    ->where('tracks.agent_id','=',$cf)
                    ->where('tracks.destination', 'like', '%' . $location . '%')
                    ->orderByDesc("created_at")
                    ->paginate(10);


                return view('pendingContainer.declaration',compact('agent','destination','country','expenses','normal','bill','pending','ladding','cont_bill'));

            }
            elseif ($containers =='' AND $cf =='' AND $location !='')
            {
                $from = '2021';
                $to = '2030';
                $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
                $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

                $ladding = DB::table("laddings")->whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->pluck("bill_lading","id");

                $cont_bill = Container::select('containers.*')
                    ->join('transporters','transporters.container_id','=','containers.id')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->whereBetween(DB::raw('YEAR(containers.created_at)'), [$from, $to])
                    ->get();
                $destination = Destination::all();
                $country    = DB::table("countries")->pluck("country_name","id");

                $bill   =Container::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->get();
                $agent  = Agent::all();


                $pending    = Container::select('containers.*','bill_lading','current_status','tracks.updated_at as updateDate','organization_name','destination')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->join('tracks','tracks.id','=','laddings.track_id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('tracks.current_status', 'not like', '%' . 'Receiving the documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Under shipping line procedures' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Under customs procedures' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release order, Delivery order received' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release order/delivery order' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Prepare T1. / IM4 Documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release / Exit documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Lodge Port Charges' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Pay Port Charges' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Loading' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Container drop off' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'File Closed' . '%')
                    ->whereBetween(DB::raw('YEAR(containers.created_at)'), [$from, $to])
                    ->whereBetween(DB::raw('YEAR(tracks.updated_at)'), [$from, $to])
                    ->where('tracks.destination', 'like', '%' . $location . '%')
                    ->orderByDesc("created_at")
                    ->paginate(10);


                return view('pendingContainer.declaration',compact('agent','destination','country','expenses','normal','bill','pending','ladding','cont_bill'));

            }
            elseif ($containers =='' AND $cf !='' AND $location =='')
            {
                $from = '2021';
                $to = '2030';
                $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
                $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

                $ladding = DB::table("laddings")->whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->pluck("bill_lading","id");

                $cont_bill = Container::select('containers.*')
                    ->join('transporters','transporters.container_id','=','containers.id')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->whereBetween(DB::raw('YEAR(containers.created_at)'), [$from, $to])
                    ->get();
                $destination = Destination::all();
                $country    = DB::table("countries")->pluck("country_name","id");

                $bill   =Container::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->get();
                $agent  = Agent::all();


                $pending    = Container::select('containers.*','bill_lading','current_status','tracks.updated_at as updateDate','organization_name','destination')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->join('tracks','tracks.id','=','laddings.track_id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('tracks.current_status', 'not like', '%' . 'Receiving the documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Under shipping line procedures' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Under customs procedures' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release order, Delivery order received' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release order/delivery order' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Prepare T1. / IM4 Documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release / Exit documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Lodge Port Charges' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Pay Port Charges' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Loading' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Container drop off' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'File Closed' . '%')
                    ->whereBetween(DB::raw('YEAR(containers.created_at)'), [$from, $to])
                    ->whereBetween(DB::raw('YEAR(tracks.updated_at)'), [$from, $to])
                    ->where('tracks.agent_id','=',$cf)
                    ->orderByDesc("created_at")
                    ->paginate(10);


                return view('pendingContainer.declaration',compact('agent','destination','country','expenses','normal','bill','pending','ladding','cont_bill'));

            }
            else
            {
                $from = '2021';
                $to = '2030';
                $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
                $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

                $ladding = DB::table("laddings")->whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->pluck("bill_lading","id");

                $cont_bill = Container::select('containers.*')
                    ->join('transporters','transporters.container_id','=','containers.id')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->whereBetween(DB::raw('YEAR(containers.created_at)'), [$from, $to])
                    ->get();
                $destination = Destination::all();
                $country    = DB::table("countries")->pluck("country_name","id");

                $bill   =Container::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->get();
                $agent  = Agent::all();


                $pending    = Container::select('containers.*','bill_lading','current_status','tracks.updated_at as updateDate','organization_name','destination')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->join('tracks','tracks.id','=','laddings.track_id')
                    ->join('agents','agents.id','=','tracks.agent_id')
                    ->where('tracks.current_status', 'not like', '%' . 'Receiving the documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Under shipping line procedures' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Under customs procedures' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release order, Delivery order received' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release order/delivery order' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Prepare T1. / IM4 Documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Release / Exit documents' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Lodge Port Charges' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Pay Port Charges' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Loading' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'Container drop off' . '%')
                    ->where('tracks.current_status', 'not like', '%' . 'File Closed' . '%')
                    ->whereBetween(DB::raw('YEAR(containers.created_at)'), [$from, $to])
                    ->whereBetween(DB::raw('YEAR(tracks.updated_at)'), [$from, $to])
                    ->orderByDesc("created_at")
                    ->paginate(10);


                return view('pendingContainer.declaration',compact('agent','destination','country','expenses','normal','bill','pending','ladding','cont_bill'));


            }
        }
        else
        {
            return JsonResponse::create(['error' => 'access-denied'],401);
        }


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * @param $id
     * @return \Illuminate\Database\Eloquent\Model|null|static
     */
    public function DropDetails($id)
    {
        $from = '2021';
        $to = '2030';
        return  Container::select('containers.*','trucking_number','transporter_name','bill_lading')
                    ->join('transporters','transporters.container_id','=','containers.id')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
//                    ->join('drivers','drivers.container_id','=','containers.id')
                    ->whereBetween(DB::raw('YEAR(containers.created_at)'), [$from, $to])
                    ->where('containers.id','=',$id)
                    ->first();
    }


    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function searchBlDrop(Request $request )
    {
        $user = Auth::user();
        $from = '2021';
        $to = '2030';
        if ($user->hasRole('admin')) {


            $get_bl = $request->get('container_id');
            $ladding = DB::table("laddings")->whereBetween(DB::raw('YEAR(laddings.created_at)'), [$from, $to])->pluck("bill_lading", "id");

            $cont_bill = Container::select('containers.*')
                ->join('transporters', 'transporters.container_id', '=', 'containers.id')
                ->join('laddings', 'laddings.id', '=', 'containers.ladding_id')
                ->whereBetween(DB::raw('YEAR(containers.created_at)'), [$from, $to])
                ->get();

            $bill = Container::all();

            $drop = Drop::select('drops.*', 'bill_lading', 'cont_no')
                ->join('containers', 'containers.id', '=', 'drops.container_id')
                ->join('laddings', 'laddings.id', '=', 'containers.ladding_id')
                ->join('transporters', 'transporters.container_id', '=', 'containers.id')
                ->whereBetween(DB::raw('YEAR(drops.created_at)'), [$from, $to])
                ->where('containers.id', '=', $get_bl)
                ->paginate(10);

            $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
            $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

            return view('drop.drop', compact('expenses','normal','bill', 'drop', 'ladding', 'cont_bill'));
        }
        elseif ($user->hasRole('super')) {

            $get_bl = $request->get('container_id');
            $ladding = DB::table("laddings")->pluck("bill_lading", "id");

            $cont_bill = Container::select('containers.*')
                ->join('transporters', 'transporters.container_id', '=', 'containers.id')
                ->join('laddings', 'laddings.id', '=', 'containers.ladding_id')
                ->get();

            $bill = Container::all();

            $drop = Drop::select('drops.*', 'bill_lading', 'cont_no')
                ->join('containers', 'containers.id', '=', 'drops.container_id')
                ->join('laddings', 'laddings.id', '=', 'containers.ladding_id')
                ->join('transporters', 'transporters.container_id', '=', 'containers.id')
                ->where('containers.id', '=', $get_bl)
                ->paginate(10);

            $expenses = Expenses::with('container')->where('finance_status','LIKE','pending')->count();

            $normal = NormalExpenses::where('finance_status','LIKE','pending')->count();

            return view('drop.super_drop', compact('expenses','normal','bill', 'drop', 'ladding', 'cont_bill'));
        }
        elseif ($user->hasRole('documentation'))
        {

            $get_bl = $request->get('container_id');
            $ladding = DB::table("laddings")->whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->pluck("bill_lading", "id");

            $cont_bill = Container::select('containers.*')
                ->join('transporters', 'transporters.container_id', '=', 'containers.id')
                ->join('laddings', 'laddings.id', '=', 'containers.ladding_id')
                ->whereBetween(DB::raw('YEAR(containers.created_at)'), [$from, $to])
                ->get();

            $bill = Container::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->get();

            $drop = Drop::select('drops.*', 'bill_lading', 'cont_no')
                ->join('containers', 'containers.id', '=', 'drops.container_id')
                ->join('laddings', 'laddings.id', '=', 'containers.ladding_id')
                ->join('transporters', 'transporters.container_id', '=', 'containers.id')
                ->whereBetween(DB::raw('YEAR(drops.created_at)'), [$from, $to])
                ->where('containers.id', '=', $get_bl)
                ->paginate(10);
            $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
            $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

            return view('drop.documentation_drop', compact('expenses','normal','bill', 'drop', 'ladding', 'cont_bill'));
        }
        elseif ($user->hasRole('shipping'))
        {

            $get_bl = $request->get('container_id');
            $ladding = DB::table("laddings")->whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->pluck("bill_lading", "id");

            $cont_bill = Container::select('containers.*')
                ->join('transporters', 'transporters.container_id', '=', 'containers.id')
                ->join('laddings', 'laddings.id', '=', 'containers.ladding_id')
                ->whereBetween(DB::raw('YEAR(containers.created_at)'), [$from, $to])
                ->get();

            $bill = Container::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->get();

            $drop = Drop::select('drops.*', 'bill_lading', 'cont_no')
                ->join('containers', 'containers.id', '=', 'drops.container_id')
                ->join('laddings', 'laddings.id', '=', 'containers.ladding_id')
                ->join('transporters', 'transporters.container_id', '=', 'containers.id')
                ->whereBetween(DB::raw('YEAR(drops.created_at)'), [$from, $to])
                ->where('containers.id', '=', $get_bl)
                ->paginate(10);
            $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
            $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

            return view('drop.shipping_drop', compact('expenses','normal','bill', 'drop', 'ladding', 'cont_bill'));
        }
        elseif ($user->hasRole('sales'))
        {

            $get_bl = $request->get('container_id');
            $ladding = DB::table("laddings")->whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->pluck("bill_lading", "id");

            $cont_bill = Container::select('containers.*')
                ->join('transporters', 'transporters.container_id', '=', 'containers.id')
                ->join('laddings', 'laddings.id', '=', 'containers.ladding_id')
                ->whereBetween(DB::raw('YEAR(containers.created_at)'), [$from, $to])
                ->get();

            $bill = Container::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->get();

            $drop = Drop::select('drops.*', 'bill_lading', 'cont_no')
                ->join('containers', 'containers.id', '=', 'drops.container_id')
                ->join('laddings', 'laddings.id', '=', 'containers.ladding_id')
                ->join('transporters', 'transporters.container_id', '=', 'containers.id')
                ->whereBetween(DB::raw('YEAR(drops.created_at)'), [$from, $to])
                ->where('containers.id', '=', $get_bl)
                ->paginate(10);
            $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
            $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

            return view('drop.sales_drop', compact('expenses','normal','bill', 'drop', 'ladding', 'cont_bill'));
        }
        elseif ($user->hasRole('finance'))
        {

            $get_bl = $request->get('container_id');
            $ladding = DB::table("laddings")->whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->pluck("bill_lading", "id");

            $cont_bill = Container::select('containers.*')
                ->join('transporters', 'transporters.container_id', '=', 'containers.id')
                ->join('laddings', 'laddings.id', '=', 'containers.ladding_id')
                ->whereBetween(DB::raw('YEAR(containers.created_at)'), [$from, $to])
                ->get();

            $bill = Container::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->get();

            $drop = Drop::select('drops.*', 'bill_lading', 'cont_no')
                ->join('containers', 'containers.id', '=', 'drops.container_id')
                ->join('laddings', 'laddings.id', '=', 'containers.ladding_id')
                ->join('transporters', 'transporters.container_id', '=', 'containers.id')
                ->whereBetween(DB::raw('YEAR(drops.created_at)'), [$from, $to])
                ->where('containers.id', '=', $get_bl)
                ->paginate(10);
            $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
            $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

            return view('drop.finance_drop', compact('expenses','normal','bill', 'drop', 'ladding',  'cont_bill'));
        }
        elseif ($user->hasRole('declaration'))
        {

            $get_bl = $request->get('container_id');
            $ladding = DB::table("laddings")->whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->pluck("bill_lading", "id");

            $cont_bill = Container::select('containers.*')
                ->join('transporters', 'transporters.container_id', '=', 'containers.id')
                ->join('laddings', 'laddings.id', '=', 'containers.ladding_id')
                ->whereBetween(DB::raw('YEAR(containers.created_at)'), [$from, $to])
                ->get();

            $bill = Container::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->get();

            $drop = Drop::select('drops.*', 'bill_lading', 'cont_no')
                ->join('containers', 'containers.id', '=', 'drops.container_id')
                ->join('laddings', 'laddings.id', '=', 'containers.ladding_id')
                ->join('transporters', 'transporters.container_id', '=', 'containers.id')
                ->whereBetween(DB::raw('YEAR(drops.created_at)'), [$from, $to])
                ->where('containers.id', '=', $get_bl)
                ->paginate(10);
            $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
            $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

            return view('drop.declaration', compact('expenses','normal','bill', 'drop', 'ladding',  'cont_bill'));
        }
        else
        {
            return JsonResponse::create(['error' => 'access-denied'],401);
        }
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user   =Auth::user();
        if($user->hasRole('admin') || $user->hasRole('super') || $user->hasRole('documentation') ||  $user->hasRole('sales') || $user->hasRole('shipping') || $user->hasRole('finance') || ($user->hasRole('declaration')))
        {
            $user_id            = Auth::id();
            $request->merge(['user_id' => $user_id]);
            $order = Drop::create($request->all());
            $contract_file = $request->file('drop_document');
            $outWord        =$request->file('outWord');

            if (isset($contract_file)) {
                if (isset($contract_file)) {
                    $extension1 = $request->file('drop_document')->getClientOriginalExtension();
                }
                $sha = 'drop_document' . md5(time());
                if (isset($extension1)) {
                    $filename1 = date('Y') . $sha . "sgfs." . $extension1;
                }
                $destination_path = 'dropOffDocument/';
                if (empty($contract_file)) {
                    File::makeDirectory($contract_file, 0775, true, true);
                    // File::makeDirectory($destination_path, $mode = 0777, true, true);
                }
                if (isset($filename1)) {
                    $order->drop_document = $filename1;
                    $request->file('drop_document')->move($destination_path, $filename1);
                    $order->save();
                }
            }


            if (isset($outWord)) {
                if (isset($outWord)) {
                    $extension2 = $request->file('outWord')->getClientOriginalExtension();
                }
                $sha = 'outWord' . md5(time());
                if (isset($extension2)) {
                    $filename2 = date('Y') . $sha . "sgfs." . $extension2;
                }
                $destination_path = 'dropOffDocument/';
                if (empty($outWord)) {
                    File::makeDirectory($outWord, 0775, true, true);
                    // File::makeDirectory($destination_path, $mode = 0777, true, true);
                }
                if (isset($filename2)) {
                    $order->outWord = $filename2;
                    $request->file('outWord')->move($destination_path, $filename2);
                    $order->save();
                }
            }
            $all_data = $request->all();
            $all_data['drop_document'] = !empty($filename1) ? $filename1 : $order->drop_document;
            $all_data['outWord']        = !empty($filename2) ? $filename2 : $order->outWord;
            $order->save($all_data);
            Session::flash('message', 'Drop off Assigned to Container   Successful ');
            return redirect('/all_drop');
        }

        else
        {
            return JsonResponse::create(['error' => 'access-denied'],401);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user   =Auth::user();
        if($user->hasRole('admin') || $user->hasRole('super') ||  $user->hasRole('documentation') || $user->hasRole('sales') || $user->hasRole('shipping') || $user->hasRole('finance') || $user->hasRole('declaration'))
        {
            $user_id            = Auth::id();
            $request->merge(['user_id' => $user_id]);
            $contract_doc = $request->file('drop_document');
            $outWord        =$request->file('outWord');

            $order = Drop::findOrFail($id);
            if (isset($contract_doc)) {
                if (isset($contract_doc)) {
                    $extension1 = $request->file('drop_document')->getClientOriginalExtension();
                }
                $sha = 'drop_document' . md5(time());
                if (isset($extension1)) {
                    $filename1 = date('Y') . $sha . "sgfs." . $extension1;
                }
                $destination_path = 'dropOffDocument/';
                if (empty($destination_path)) {
                    File::makeDirectory($destination_path, 0775, true, true);
                    // File::makeDirectory($destination_path, $mode = 0777, true, true);
                }
                if (isset($filename1)) {
                    $order->drop_document = $destination_path . $filename1;
                    $request->file('drop_document')->move($destination_path, $filename1);
                    $order->save();
                }

            }
            if (isset($outWord)) {
                if (isset($outWord)) {
                    $extension2 = $request->file('outWord')->getClientOriginalExtension();
                }
                $sha = 'outWord' . md5(time());
                if (isset($extension2)) {
                    $filename2 = date('Y') . $sha . "sgfs." . $extension2;
                }
                $destination_path = 'dropOffDocument/';
                if (empty($destination_path)) {
                    File::makeDirectory($destination_path, 0775, true, true);
                    // File::makeDirectory($destination_path, $mode = 0777, true, true);
                }
                if (isset($filename2)) {
                    $order->outWord = $destination_path . $filename2;
                    $request->file('outWord')->move($destination_path, $filename2);
                    $order->save();
                }

            }
            $data = $request->all();
            $data['drop_document']  = !empty($filename1) ? $filename1 : $order->drop_document;
            $data['outWord']        = !empty($filename2) ? $filename2 : $order->outWord;
            $order->update($data);
            Session::flash('updated', 'Drop Off Updated  Successful ');
            return redirect('/all_drop');
        }

        else
        {
            return JsonResponse::create(['error' => 'access-denied'],401);
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|static
     * @throws \Exception
     */
    public function destroy($id)
    {

        $user   =Auth::user();
        if($user->hasRole('admin') || $user->hasRole('super') || $user->hasRole('sales') || $user->hasRole('documentation') || $user->hasRole('shipping') || $user->hasRole('finance') || $user->hasRole('declaration'))
        {
            $training =Drop::findOrfail($id);
            $training->delete();

            Session::flash('delete', 'Drop Off Deleted  Successful ');
            return redirect('/all_drop');
        }

        else
        {
            return JsonResponse::create(['error' => 'access-denied'],401);
        }
    }
}