<?php

namespace App\Http\Controllers;

use App\Container;
use App\Driver;
use App\Expenses;
use App\NormalExpenses;
use App\Track;
use App\Transporter;
use App\Trucking;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TransporterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user   =Auth::user();
        $from = '2021';
        $to = '2030';
        $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
        $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

        if($user->hasRole('admin'))
        {

            $ladding = DB::table("laddings")->whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->latest('laddings.created_at')->pluck("bill_lading","id");
            $truck_no   = Transporter::whereBetween(DB::raw('YEAR(transporters.created_at)'), [$from, $to])->orderBy('created_at', 'DESC')->get();
            $bill   =Container::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->get();


            $transporter = Transporter::Select('transporters.*','bill_lading','cont_no')
                ->join('containers','containers.id','=','transporters.container_id')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->whereBetween(DB::raw('YEAR(transporters.created_at)'), [$from, $to])
                ->latest("transporters.created_at")->paginate(10);

            return view('transporter.all_trans',compact('expenses','normal','bill','transporter','ladding','truck_no'));
        }
        elseif($user->hasRole('super'))
        {
            $ladding = DB::table("laddings")->latest('laddings.created_at')->pluck("bill_lading","id");
            $bill   =Container::all();
            $truck_no   = Transporter::orderBy('created_at', 'DESC')->get();
            $transporter = Transporter::Select('transporters.*','bill_lading','cont_no')
                ->join('containers','containers.id','=','transporters.container_id')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->latest("transporters.created_at")->paginate(10);
            return view('transporter.super_all_trans',compact('expenses','normal','bill','transporter','ladding','truck_no'));
        }
        elseif($user->hasRole('shipping'))
        {
            $ladding = DB::table("laddings")->whereBetween(DB::raw('YEAR(laddings.created_at)'), [$from, $to])->latest('laddings.created_at')->pluck("bill_lading","id");
            $bill   =Container::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->get();
            $truck_no   = Transporter::whereBetween(DB::raw('YEAR(transporters.created_at)'), [$from, $to])->orderBy('created_at', 'DESC')->get();
            $transporter = Transporter::Select('transporters.*','bill_lading','cont_no')
                            ->join('containers','containers.id','=','transporters.container_id')
                            ->join('laddings','laddings.id','=','containers.ladding_id')
                            ->whereBetween(DB::raw('YEAR(transporters.created_at)'), [$from, $to])
                            ->latest("transporters.created_at")->paginate(10);

            return view('transporter.shipping_all_trans',compact('expenses','normal','bill','transporter','ladding','truck_no'));
        }
        elseif($user->hasRole('declaration'))
        {
            $ladding = DB::table("laddings")->whereBetween(DB::raw('YEAR(laddings.created_at)'), [$from, $to])->latest('laddings.created_at')->pluck("bill_lading","id");
            $bill   =Container::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->get();
            $truck_no   = Transporter::whereBetween(DB::raw('YEAR(transporters.created_at)'), [$from, $to])->orderBy('created_at', 'DESC')->get();
            $transporter = Transporter::Select('transporters.*','bill_lading','cont_no')
                ->join('containers','containers.id','=','transporters.container_id')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->whereBetween(DB::raw('YEAR(transporters.created_at)'), [$from, $to])
                ->latest("transporters.created_at")->paginate(10);
            return view('transporter.declaration_all_trans',compact('expenses','normal','bill','transporter','ladding','truck_no'));
        }
        elseif($user->hasRole('finance'))
        {
            $ladding = DB::table("laddings")->whereBetween(DB::raw('YEAR(laddings.created_at)'), [$from, $to])->latest('laddings.created_at')->pluck("bill_lading","id");
            $bill   =Container::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->get();
            $truck_no   = Transporter::whereBetween(DB::raw('YEAR(transporters.created_at)'), [$from, $to])->orderBy('created_at', 'DESC')->get();
            $transporter = Transporter::Select('transporters.*','bill_lading','cont_no')
                ->join('containers','containers.id','=','transporters.container_id')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->whereBetween(DB::raw('YEAR(transporters.created_at)'), [$from, $to])
                ->latest("transporters.created_at")->paginate(10);
            return view('transporter.finance_all_trans',compact('expenses','normal','bill','transporter','ladding','truck_no'));
        }
        elseif($user->hasRole('sales'))
        {
            $ladding = DB::table("laddings")->whereBetween(DB::raw('YEAR(laddings.created_at)'), [$from, $to])->latest('laddings.created_at')->pluck("bill_lading","id");
            $bill   =Container::whereBetween(DB::raw('YEAR(created_at)'), [$from, $to])->get();
            $truck_no   = Transporter::whereBetween(DB::raw('YEAR(transporters.created_at)'), [$from, $to])->orderBy('created_at', 'DESC')->get();
            $transporter = Transporter::Select('transporters.*','bill_lading','cont_no')
                ->join('containers','containers.id','=','transporters.container_id')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->whereBetween(DB::raw('YEAR(transporters.created_at)'), [$from, $to])
                ->latest("transporters.created_at")->paginate(10);
            return view('transporter.operations_list',compact('expenses','normal','bill','transporter','ladding','truck_no'));
        }
        {
            return JsonResponse::create(['error' => 'access-denied'],401);
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {

        $user   =Auth::user();
        if($user->hasRole('admin') ||  $user->hasRole('sales') || $user->hasRole('finance') || $user->hasRole('shipping') ||  $user->hasRole('declaration'))
        {
            $user_id = Auth::id();
            $request->merge(['user_id' => $user_id]);
            $order =  Transporter::create($request->all());

            $driving_copy          = $request->file('truck_horse_copy');
            $passport_copy         = $request->file('truck_trailer_copy');



            if(isset($driving_copy)) {
                if (isset($driving_copy)) {
                    $extension2 = $request->file('truck_horse_copy')->getClientOriginalExtension();
                }
                $sha = 'truck_horse_copy' . md5(time());
                if(isset($extension2)){
                    $yellow_card_file = date('Y').$sha . "gfgsgf." . $extension2;
                }
                $destination_path1 = 'archives/';
                if (empty($destination_path1)) {
                    File::makeDirectory($destination_path1, 0775, true, true);
                }
                if(isset($yellow_card_file)){
                    $order->truck_horse_copy = $yellow_card_file;
                    $request->file('truck_horse_copy')->move($destination_path1, $yellow_card_file);
                    $order->save();
                }
            }

            if(isset($passport_copy)) {
                if (isset($passport_copy)) {
                    $extension2 = $request->file('truck_trailer_copy')->getClientOriginalExtension();
                }
                $sha = 'truck_trailer_copy' . md5(time());
                if(isset($extension2)){
                    $filename4 = date('Y').$sha . "gfgsgf." . $extension2;
                }
                $destination_path1 = 'archives/';
                if (empty($destination_path1)) {
                    File::makeDirectory($destination_path1, 0775, true, true);
                }
                if(isset($filename4)){
                    $order->truck_trailer_copy = $filename4;
                    $request->file('truck_trailer_copy')->move($destination_path1, $filename4);
                    $order->save();
                }

            }

            $all_data =  $request->all();

            $all_data['truck_horse_copy']         = !empty($yellow_card_file) ? $yellow_card_file   : $order->truck_horse_copy;
            $all_data['truck_trailer_copy']       = !empty($filename4) ? $filename4                 : $order->truck_trailer_copy;
            $order->update($all_data);
            Session::flash('message', 'Transporter Assigned to Container   Successful ');
            return redirect('/all_trans');
        }
        else
        {
            return JsonResponse::create(['error' => 'access-denied'],401);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|static
     *
     */

    public function searchBl(Request $request )
    {
        $date_from = '2021';
        $date_to = '2030';
        $bl_ladding = $request->get('ladding_id');
        $truck_number   = $request->get('truck_id');
        $container_id   = $request->get('container_id');
        $load_date      = $request->get('loading_date');

        $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'), [$date_from, $date_to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
        $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'), [$date_from, $date_to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();



        $user   =Auth::user();
        if($user->hasRole('admin'))
        {
            if ($bl_ladding != '')
            {
                $ladding = DB::table("laddings")->whereBetween(DB::raw('YEAR(laddings.created_at)'),  [$date_from, $date_to])->pluck("bill_lading","id");
                $truck_no   = Transporter::whereBetween(DB::raw('YEAR(created_at)'),  [$date_from, $date_to])->orderBy('created_at', 'DESC')->get();
                $bill   =Container::whereBetween(DB::raw('YEAR(containers.created_at)'),  [$date_from, $date_to])->get();
                $transporter = Transporter::select('transporters.*','cont_no','bill_lading')
                                ->join('containers','containers.id','=','transporters.container_id')
                                ->join('laddings','laddings.id','=','containers.ladding_id')
                                ->whereBetween(DB::raw('YEAR(transporters.created_at)'),  [$date_from, $date_to])
                                ->where('laddings.id','=',$bl_ladding)
                                ->paginate(10);
                return view('transporter.all_trans',compact('expenses','normal','bill','transporter','ladding','truck_no'));
            }
            elseif($truck_number != '')
            {
                $ladding = DB::table("laddings")->whereBetween(DB::raw('YEAR(laddings.created_at)'), [$date_from, $date_to])->pluck("bill_lading","id");
                $truck_no   = Transporter::whereBetween(DB::raw('YEAR(created_at)'),  [$date_from, $date_to])->orderBy('created_at', 'DESC')->get();
                $bill   =Container::whereBetween(DB::raw('YEAR(containers.created_at)'),  [$date_from, $date_to])->get();
                $transporter =  Transporter::select('transporters.*','cont_no','bill_lading')
                                    ->join('containers','containers.id','=','transporters.container_id')
                                    ->join('laddings','laddings.id','=','containers.ladding_id')
                                    ->whereBetween(DB::raw('YEAR(transporters.created_at)'),  [$date_from, $date_to])
                                    ->where('transporters.trucking_number','=',$truck_number)
                                    ->paginate(10);

                return view('transporter.all_trans',compact('expenses','normal','bill','transporter','ladding','truck_no'));
            }
            elseif($load_date != '')
            {
                $ladding = DB::table("laddings")->whereBetween(DB::raw('YEAR(laddings.created_at)'),  [$date_from, $date_to])->pluck("bill_lading","id");
                $truck_no   = Transporter::whereBetween(DB::raw('YEAR(created_at)'),  [$date_from, $date_to])->orderBy('created_at', 'DESC')->get();
                $bill   =Container::whereBetween(DB::raw('YEAR(containers.created_at)'),  [$date_from, $date_to])->get();
                $transporter = Transporter::select('transporters.*','cont_no','bill_lading')
                    ->join('containers','containers.id','=','transporters.container_id')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->whereBetween(DB::raw('YEAR(transporters.created_at)'),  [$date_from, $date_to])
                    ->where('transporters.loading_date','=',$load_date)
                    ->paginate(10);

                return view('transporter.all_trans',compact('expenses','normal','bill','transporter','ladding','truck_no'));

            }
            elseif($container_id != '')
            {
                $ladding = DB::table("laddings")->whereBetween(DB::raw('YEAR(laddings.created_at)'), [$date_from, $date_to])->pluck("bill_lading","id");
                $truck_no   = Transporter::whereBetween(DB::raw('YEAR(created_at)'),  [$date_from, $date_to])->orderBy('created_at', 'DESC')->get();
                $bill   =Container::whereBetween(DB::raw('YEAR(containers.created_at)'),  [$date_from, $date_to])->get();
                $transporter = Transporter::select('transporters.*','cont_no','bill_lading')
                    ->join('containers','containers.id','=','transporters.container_id')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->whereBetween(DB::raw('YEAR(transporters.created_at)'),  [$date_from, $date_to])
                    ->where('transporters.container_id','=',$container_id)
                    ->paginate(10);

                return view('transporter.all_trans',compact('expenses','normal','bill','transporter','ladding','truck_no'));
            }
        }
        elseif($user->hasRole('super'))
        {
            if ($bl_ladding != '')
            {
                $ladding = DB::table("laddings")->pluck("bill_lading","id");
                $truck_no   = Transporter::orderBy('created_at', 'DESC')->get();
                $bill   =Container::all();
                $transporter = Transporter::select('transporters.*','cont_no','bill_lading')
                    ->join('containers','containers.id','=','transporters.container_id')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->where('laddings.id','=',$bl_ladding)
                    ->paginate(10);
                return view('transporter.super_all_trans',compact('expenses','normal','bill','transporter','ladding','truck_no'));
            }
            elseif($truck_number != '')
            {
                $truck_no   = Transporter::orderBy('created_at', 'DESC')->get();
                $ladding    =   DB::table("laddings")->pluck("bill_lading","id");
                $bill       =   Container::all();
                $transporter =  Transporter::select('transporters.*','cont_no','bill_lading')
                    ->join('containers','containers.id','=','transporters.container_id')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->where('transporters.trucking_number','=',$truck_number)
                    ->paginate(10);

                return view('transporter.super_all_trans',compact('expenses','normal','bill','transporter','ladding','truck_no'));
            }
            elseif($load_date != '')
            {
                $ladding    = DB::table("laddings")->pluck("bill_lading","id");
                $truck_no   = Transporter::orderBy('created_at', 'DESC')->get();
                $bill        =Container::all();
                $transporter = Transporter::select('transporters.*','cont_no','bill_lading')
                    ->join('containers','containers.id','=','transporters.container_id')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->where('transporters.loading_date','=',$load_date)
                    ->paginate(10);

                return view('transporter.super_all_trans',compact('expenses','normal','bill','transporter','ladding','truck_no'));

            }
            elseif($container_id != '')
            {
                $ladding = DB::table("laddings")->pluck("bill_lading","id");

                $bill   =Container::all();
                $transporter = Transporter::select('transporters.*','cont_no','bill_lading')
                    ->join('containers','containers.id','=','transporters.container_id')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->where('transporters.container_id','=',$container_id)
                    ->paginate(10);

                return view('transporter.super_all_trans',compact('expenses','normal','bill','transporter','ladding','truck_no'));
            }




        }
        elseif($user->hasRole('shipping'))
        {
            if ($bl_ladding != '')
            {
                $ladding = DB::table("laddings")->whereBetween(DB::raw('YEAR(laddings.created_at)'),  [$date_from, $date_to])->pluck("bill_lading","id");
                $truck_no   = Transporter::whereBetween(DB::raw('YEAR(created_at)'),  [$date_from, $date_to])->orderBy('created_at', 'DESC')->get();
                $bill   =Container::whereBetween(DB::raw('YEAR(containers.created_at)'),  [$date_from, $date_to])->get();
                $transporter = Transporter::select('transporters.*','cont_no','bill_lading')
                    ->join('containers','containers.id','=','transporters.container_id')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->whereBetween(DB::raw('YEAR(transporters.created_at)'),  [$date_from, $date_to])
                    ->where('laddings.id','=',$bl_ladding)
                    ->paginate(10);
                return view('transporter.shipping_all_trans',compact('expenses','normal','bill','transporter','ladding','truck_no'));
            }
            elseif($truck_number != '')
            {
                $ladding = DB::table("laddings")->whereBetween(DB::raw('YEAR(laddings.created_at)'),  [$date_from, $date_to])->pluck("bill_lading","id");
                $truck_no   = Transporter::whereBetween(DB::raw('YEAR(created_at)'),  [$date_from, $date_to])->orderBy('created_at', 'DESC')->get();
                $bill   =Container::whereBetween(DB::raw('YEAR(containers.created_at)'),  [$date_from, $date_to])->get();
                $transporter =  Transporter::select('transporters.*','cont_no','bill_lading')
                    ->join('containers','containers.id','=','transporters.container_id')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->whereBetween(DB::raw('YEAR(transporters.created_at)'),  [$date_from, $date_to])
                    ->where('transporters.trucking_number','=',$truck_number)
                    ->paginate(10);

                return view('transporter.shipping_all_trans',compact('expenses','normal','bill','transporter','ladding','truck_no'));
            }
            elseif($load_date != '')
            {
                $ladding = DB::table("laddings")->whereBetween(DB::raw('YEAR(laddings.created_at)'),  [$date_from, $date_to])->pluck("bill_lading","id");
                $truck_no   = Transporter::whereBetween(DB::raw('YEAR(created_at)'),  [$date_from, $date_to])->orderBy('created_at', 'DESC')->get();
                $bill   =Container::whereBetween(DB::raw('YEAR(containers.created_at)'),  [$date_from, $date_to])->get();
                $transporter = Transporter::select('transporters.*','cont_no','bill_lading')
                    ->join('containers','containers.id','=','transporters.container_id')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->whereBetween(DB::raw('YEAR(transporters.created_at)'),  [$date_from, $date_to])
                    ->where('transporters.loading_date','=',$load_date)
                    ->paginate(10);

                return view('transporter.shipping_all_trans',compact('expenses','normal','bill','transporter','ladding','truck_no'));

            }
            elseif($container_id != '')
            {
                $ladding = DB::table("laddings")->whereBetween(DB::raw('YEAR(laddings.created_at)'), [$date_from, $date_to])->pluck("bill_lading","id");
                $truck_no   = Transporter::whereBetween(DB::raw('YEAR(created_at)'),  [$date_from, $date_to])->orderBy('created_at', 'DESC')->get();
                $bill   =Container::whereBetween(DB::raw('YEAR(containers.created_at)'),  [$date_from, $date_to])->get();
                $transporter = Transporter::select('transporters.*','cont_no','bill_lading')
                    ->join('containers','containers.id','=','transporters.container_id')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->whereBetween(DB::raw('YEAR(transporters.created_at)'),  [$date_from, $date_to])
                    ->where('transporters.container_id','=',$container_id)
                    ->paginate(10);

                return view('transporter.shipping_all_trans',compact('expenses','normal','bill','transporter','ladding','truck_no'));
            }




        }
        elseif($user->hasRole('declaration'))
        {
            if ($bl_ladding != '')
            {
                $ladding = DB::table("laddings")->whereBetween(DB::raw('YEAR(laddings.created_at)'), [$date_from, $date_to])->pluck("bill_lading","id");
                $truck_no   = Transporter::whereBetween(DB::raw('YEAR(created_at)'),  [$date_from, $date_to])->orderBy('created_at', 'DESC')->get();
                $bill   =Container::whereBetween(DB::raw('YEAR(containers.created_at)'),  [$date_from, $date_to])->get();
                $transporter = Transporter::select('transporters.*','cont_no','bill_lading')
                    ->join('containers','containers.id','=','transporters.container_id')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->whereBetween(DB::raw('YEAR(transporters.created_at)'),  [$date_from, $date_to])
                    ->where('laddings.id','=',$bl_ladding)
                    ->paginate(10);
                return view('transporter.declaration_all_trans',compact('expenses','normal','bill','transporter','ladding','truck_no'));
            }
            elseif($truck_number != '')
            {
                $ladding = DB::table("laddings")->whereBetween(DB::raw('YEAR(laddings.created_at)'), [$date_from, $date_to])->pluck("bill_lading","id");
                $truck_no   = Transporter::whereBetween(DB::raw('YEAR(created_at)'),  [$date_from, $date_to])->orderBy('created_at', 'DESC')->get();
                $bill   =Container::whereBetween(DB::raw('YEAR(containers.created_at)'),  [$date_from, $date_to])->get();
                $transporter =  Transporter::select('transporters.*','cont_no','bill_lading')
                    ->join('containers','containers.id','=','transporters.container_id')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->whereBetween(DB::raw('YEAR(transporters.created_at)'),  [$date_from, $date_to])
                    ->where('transporters.trucking_number','=',$truck_number)
                    ->paginate(10);

                return view('transporter.declaration_all_trans',compact('expenses','normal','bill','transporter','ladding','truck_no'));
            }
            elseif($load_date != '')
            {
                $ladding = DB::table("laddings")->whereBetween(DB::raw('YEAR(laddings.created_at)'),  [$date_from, $date_to])->pluck("bill_lading","id");
                $truck_no   = Transporter::whereBetween(DB::raw('YEAR(created_at)'),  [$date_from, $date_to])->orderBy('created_at', 'DESC')->get();
                $bill   =Container::whereBetween(DB::raw('YEAR(containers.created_at)'),  [$date_from, $date_to])->get();
                $transporter = Transporter::select('transporters.*','cont_no','bill_lading')
                    ->join('containers','containers.id','=','transporters.container_id')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->whereBetween(DB::raw('YEAR(transporters.created_at)'),  [$date_from, $date_to])
                    ->where('transporters.loading_date','=',$load_date)
                    ->paginate(10);

                return view('transporter.declaration_all_trans',compact('expenses','normal','bill','transporter','ladding','truck_no'));

            }
            elseif($container_id != '')
            {
                $ladding = DB::table("laddings")->whereBetween(DB::raw('YEAR(laddings.created_at)'),  [$date_from, $date_to])->pluck("bill_lading","id");
                $truck_no   = Transporter::whereBetween(DB::raw('YEAR(created_at)'),  [$date_from, $date_to])->orderBy('created_at', 'DESC')->get();
                $bill   =Container::whereBetween(DB::raw('YEAR(containers.created_at)'),  [$date_from, $date_to])->get();
                $transporter = Transporter::select('transporters.*','cont_no','bill_lading')
                    ->join('containers','containers.id','=','transporters.container_id')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->whereBetween(DB::raw('YEAR(transporters.created_at)'),  [$date_from, $date_to])
                    ->where('transporters.container_id','=',$container_id)
                    ->paginate(10);

                return view('transporter.declaration_all_trans',compact('expenses','normal','bill','transporter','ladding','truck_no'));
            }




        }
        elseif($user->hasRole('finance'))
        {
            if ($bl_ladding != '')
            {
                $ladding = DB::table("laddings")->whereBetween(DB::raw('YEAR(laddings.created_at)'),  [$date_from, $date_to])->pluck("bill_lading","id");
                $truck_no   = Transporter::whereBetween(DB::raw('YEAR(created_at)'), [$date_from, $date_to])->orderBy('created_at', 'DESC')->get();
                $bill   =Container::whereBetween(DB::raw('YEAR(containers.created_at)'),  [$date_from, $date_to])->get();
                $transporter = Transporter::select('transporters.*','cont_no','bill_lading')
                    ->join('containers','containers.id','=','transporters.container_id')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->whereBetween(DB::raw('YEAR(transporters.created_at)'),  [$date_from, $date_to])
                    ->where('laddings.id','=',$bl_ladding)
                    ->paginate(10);
                return view('transporter.finance_all_trans',compact('expenses','normal','bill','transporter','ladding','truck_no'));
            }
            elseif($truck_number != '')
            {
                $ladding = DB::table("laddings")->whereBetween(DB::raw('YEAR(laddings.created_at)'),  [$date_from, $date_to])->pluck("bill_lading","id");
                $truck_no   = Transporter::whereBetween(DB::raw('YEAR(created_at)'),  [$date_from, $date_to])->orderBy('created_at', 'DESC')->get();
                $bill   =Container::whereBetween(DB::raw('YEAR(containers.created_at)'),  [$date_from, $date_to])->get();
                $transporter =  Transporter::select('transporters.*','cont_no','bill_lading')
                    ->join('containers','containers.id','=','transporters.container_id')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->whereBetween(DB::raw('YEAR(transporters.created_at)'),  [$date_from, $date_to])
                    ->where('transporters.trucking_number','=',$truck_number)
                    ->paginate(10);

                return view('transporter.finance_all_trans',compact('expenses','normal','bill','transporter','ladding','truck_no'));
            }
            elseif($load_date != '')
            {
                $ladding = DB::table("laddings")->whereBetween(DB::raw('YEAR(laddings.created_at)'),  [$date_from, $date_to])->pluck("bill_lading","id");
                $truck_no   = Transporter::whereBetween(DB::raw('YEAR(created_at)'),  [$date_from, $date_to])->orderBy('created_at', 'DESC')->get();
                $bill   =Container::whereBetween(DB::raw('YEAR(containers.created_at)'),  [$date_from, $date_to])->get();
                $transporter = Transporter::select('transporters.*','cont_no','bill_lading')
                    ->join('containers','containers.id','=','transporters.container_id')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->whereBetween(DB::raw('YEAR(transporters.created_at)'),  [$date_from, $date_to])
                    ->where('transporters.loading_date','=',$load_date)
                    ->paginate(10);
                return view('transporter.finance_all_trans',compact('expenses','normal','bill','transporter','ladding','truck_no'));

            }
            elseif($container_id != '')
            {
                $ladding = DB::table("laddings")->whereBetween(DB::raw('YEAR(laddings.created_at)'),  [$date_from, $date_to])->pluck("bill_lading","id");
                $truck_no   = Transporter::whereBetween(DB::raw('YEAR(created_at)'),  [$date_from, $date_to])->orderBy('created_at', 'DESC')->get();
                $bill   =Container::whereBetween(DB::raw('YEAR(containers.created_at)'),  [$date_from, $date_to])->get();
                $transporter = Transporter::select('transporters.*','cont_no','bill_lading')
                    ->join('containers','containers.id','=','transporters.container_id')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->whereBetween(DB::raw('YEAR(transporters.created_at)'),  [$date_from, $date_to])
                    ->where('transporters.container_id','=',$container_id)
                    ->paginate(10);

                return view('transporter.finance_all_trans',compact('expenses','normal','bill','transporter','ladding','truck_no'));
            }




        }
        elseif($user->hasRole('sales'))
        {
            if ($bl_ladding != '')
            {
                $ladding = DB::table("laddings")->whereBetween(DB::raw('YEAR(laddings.created_at)'),  [$date_from, $date_to])->pluck("bill_lading","id");
                $truck_no   = Transporter::whereBetween(DB::raw('YEAR(created_at)'),  [$date_from, $date_to])->orderBy('created_at', 'DESC')->get();
                $bill   =Container::whereBetween(DB::raw('YEAR(containers.created_at)'),  [$date_from, $date_to])->get();
                $transporter = Transporter::select('transporters.*','cont_no','bill_lading')
                    ->join('containers','containers.id','=','transporters.container_id')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->whereBetween(DB::raw('YEAR(transporters.created_at)'),  [$date_from, $date_to])
                    ->where('laddings.id','=',$bl_ladding)
                    ->paginate(10);
                return view('transporter.operations_list',compact('expenses','normal','bill','transporter','ladding','truck_no'));
            }
            elseif($truck_number != '')
            {
                $ladding = DB::table("laddings")->whereBetween(DB::raw('YEAR(laddings.created_at)'), [$date_from, $date_to])->pluck("bill_lading","id");
                $truck_no   = Transporter::whereBetween(DB::raw('YEAR(created_at)'),  [$date_from, $date_to])->orderBy('created_at', 'DESC')->get();
                $bill   =Container::whereBetween(DB::raw('YEAR(containers.created_at)'),  [$date_from, $date_to])->get();
                $transporter =  Transporter::select('transporters.*','cont_no','bill_lading')
                    ->join('containers','containers.id','=','transporters.container_id')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->whereBetween(DB::raw('YEAR(transporters.created_at)'),  [$date_from, $date_to])
                    ->where('transporters.trucking_number','=',$truck_number)
                    ->paginate(10);

                return view('transporter.operations_list',compact('expenses','normal','bill','transporter','ladding','truck_no'));
            }
            elseif($load_date != '')
            {
                $ladding = DB::table("laddings")->whereBetween(DB::raw('YEAR(laddings.created_at)'), [$date_from, $date_to])->pluck("bill_lading","id");
                $truck_no   = Transporter::whereBetween(DB::raw('YEAR(created_at)'),  [$date_from, $date_to])->orderBy('created_at', 'DESC')->get();
                $bill   =Container::whereBetween(DB::raw('YEAR(containers.created_at)'),  [$date_from, $date_to])->get();
                $transporter = Transporter::select('transporters.*','cont_no','bill_lading')
                    ->join('containers','containers.id','=','transporters.container_id')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->whereBetween(DB::raw('YEAR(transporters.created_at)'),  [$date_from, $date_to])
                    ->where('transporters.loading_date','=',$load_date)
                    ->paginate(10);

                return view('transporter.operations_list',compact('expenses','normal','bill','transporter','ladding','truck_no'));

            }
            elseif($container_id != '')
            {
                $ladding = DB::table("laddings")->whereBetween(DB::raw('YEAR(laddings.created_at)'),  [$date_from, $date_to])->pluck("bill_lading","id");
                $truck_no   = Transporter::whereBetween(DB::raw('YEAR(created_at)'),  [$date_from, $date_to])->orderBy('created_at', 'DESC')->get();
                $bill   =Container::whereBetween(DB::raw('YEAR(containers.created_at)'),  [$date_from, $date_to])->get();
                $transporter = Transporter::select('transporters.*','cont_no','bill_lading')
                    ->join('containers','containers.id','=','transporters.container_id')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->whereBetween(DB::raw('YEAR(transporters.created_at)'),  [$date_from, $date_to])
                    ->where('transporters.container_id','=',$container_id)
                    ->paginate(10);

                return view('transporter.operations_list',compact('expenses','normal','bill','transporter','ladding','truck_no'));
            }

        }
        else
        {
            return JsonResponse::create(['error' => 'access-denied'],401);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request, $id)
    {
        $user   =Auth::user();
        if($user->hasRole('admin') || $user->hasRole('sales') || $user->hasRole('super') || $user->hasRole('shipping') || $user->hasRole('declaration') )
        {
            $user_id = Auth::id();

            $order =  Transporter::findOrFail($id);

            $customer_payments         = $request->file('truck_horse_copy');
            $other                     = $request->file('truck_trailer_copy');


            if(isset($customer_payments)) {
                if (isset($customer_payments)) {
                    $extension5 = $request->file('truck_horse_copy')->getClientOriginalExtension();
                }
                $sha = 'truck_horse_copy' . md5(time());
                if(isset($extension5)){
                    $filename6 = date('Y').$sha . "gfgsgf." . $extension5;
                }
                $port_charges_path = 'archives/';
                if (empty($customer_payments)) {
                    File::makeDirectory($customer_payments, 0775, true, true);
                }
                if(isset($filename6)){
                    $order->truck_horse_copy = $filename6;
                    $request->file('truck_horse_copy')->move($port_charges_path, $filename6);
                    $order->save();
                }
            }

            if(isset($other)) {
                if (isset($other)) {
                    $extension6 = $request->file('truck_trailer_copy')->getClientOriginalExtension();
                }
                $sha = 'truck_trailer_copy' . md5(time());
                if(isset($extension6)){
                    $filename4 = date('Y').$sha . "gfgsgf." . $extension6;
                }
                $port_charges_path = 'archives/';
                if (empty($other)) {
                    File::makeDirectory($other, 0775, true, true);
                }
                if(isset($filename4)){
                    $order->truck_trailer_copy = $filename4;
                    $request->file('truck_trailer_copy')->move($port_charges_path, $filename4);
                    $order->save();
                }

            }
            $request->merge(['user_id' => $user_id]);
            $all_data =  $request->all();

            $all_data['truck_horse_copy']          = !empty($filename6) ? $filename6  : $order->truck_horse_copy;
            $all_data['truck_trailer_copy']         = !empty($filename4) ? $filename4  : $order->truck_trailer_copy;

            $order->update($all_data);
            Session::flash('updated', 'Transporter Updated  Successful ');
            return redirect('/all_trans');
        }
        else
        {
            return JsonResponse::create(['error' => 'access-denied'],401);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user   =Auth::user();
        if($user->hasRole('admin') || ($user->hasRole('sales') ||  $user->hasRole('shipping') || $user->hasRole('declaration')))
        {
            $training =Transporter::findOrfail($id);
            $training->delete();

            Session::flash('delete', 'Transporter Deleted  Successful ');
            return redirect('/all_trans');
        }

        else
        {
            return JsonResponse::create(['error' => 'access-denied'],401);
        }
    }
}