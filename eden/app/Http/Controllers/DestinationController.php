<?php

namespace App\Http\Controllers;

use App\Country;
use App\Destination;
use App\Expenses;
use App\NormalExpenses;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Session;

class DestinationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user   =Auth::user();
        if($user->hasRole('admin'))
        {
            $country    = Country::all();
            $expenses = Expenses::with('container')->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
            $normal = NormalExpenses::where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
            $destination = Destination::with('country')->get();


            return view('destination.admin',compact('country','destination','expenses','normal'));
        }
        else if($user->hasRole('sales'))
        {
            $country    = Country::all();
            $expenses = Expenses::with('container')->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
            $normal = NormalExpenses::where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
            $destination = Destination::with('country')->get();            return view('destination.sales',compact('country','destination','normal','expenses'));
        }
        else if($user->hasRole('documentation'))
        {
            $country    = Country::all();
            $expenses = Expenses::with('container')->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
            $normal = NormalExpenses::where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
            $destination = Destination::with('country')->get();
            return view('destination.documentation',compact('country','destination','normal','expenses'));
        }
        else if($user->hasRole('declaration'))
        {
            $country    = Country::all();
            $expenses = Expenses::with('container')->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
            $normal = NormalExpenses::where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
            $destination = Destination::with('country')->get();
            return view('destination.declaration',compact('country','destination','normal','expenses'));
        }
        else if($user->hasRole('shipping'))
        {
            $country    = Country::all();
            $expenses = Expenses::with('container')->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
            $normal = NormalExpenses::where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
            $destination = Destination::with('country')->get();
            return view('destination.shipping',compact('country','destination','expenses','normal'));
        }
        elseif($user->hasRole('agent'))
        {
            $country    = Country::all();
            $expenses = Expenses::with('container')->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
            $normal = NormalExpenses::where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
            $destination = Destination::with('country')->get();
            return view('destination.agent',compact('country','destination','normal','expenses'));
        }
        else
        {
            return JsonResponse::create(['error' => 'access-denied'],401);
        }
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {

        $status =  Destination::create($request->all());
        $status->save();
        Session::flash('message', 'Destination created Successful');
        return redirect('/manage/destination');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $department = Destination::findOrFail($id);
        $data = $request->all();
        $department->update($data);
        Session::flash('message', 'Destination Updated  Successful ');
        return redirect('/manage/destination');
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Exception
     */
    public function destroy($id)
    {
        $department  =Destination::findOrfail($id);
        $department->delete();
        Session::flash('message', 'Destination Deleted  Successful ');
        return redirect('/manage/destination');
    }
}
