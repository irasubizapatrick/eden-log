<?php

namespace App\Http\Controllers;

use App\Container;
use App\Expenses;
use App\Ladding;
use App\NormalExpenses;
use App\Track;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Auth;
use Session;
use App\User;
use Illuminate\Support\Facades\DB;

class VoucherController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user   =Auth::user();
        if($user->hasRole('super'))
        {
            $cont_bill = Container::select('containers.*','bill_lading')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->get();
            $ladding = DB::table("laddings")->pluck("bill_lading","id");
            $expenses_data = Expenses::select('expenses.*','bill_lading','cont_no','client_name')
                ->join('containers','containers.id','=','expenses.container_id')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->join('tracks','tracks.id','=','laddings.track_id')->latest()->paginate(10);
            $data = Ladding::latest()->get();
            $track = Track::latest()->get();
            $expenses = Expenses::with('container')->where('finance_status','LIKE','pending')->count();

            $normal = NormalExpenses::where('finance_status','LIKE','pending')->count();



            return view('expenses.super',compact('normal','expenses_data','ladding','expenses','cont_bill','data','track'));

        }
        elseif($user->hasRole('finance'))
        {
            $expenses = Expenses::with('container')->where('finance_status','LIKE','pending')->count();

            $normal = NormalExpenses::where('finance_status','LIKE','pending')->count();


            $cont_bill = Container::select('containers.*','bill_lading')
                            ->join('laddings','laddings.id','=','containers.ladding_id')
                            ->get();

            $ladding        = DB::table("laddings")->pluck("bill_lading","id");
            $expenses_data  = Expenses::select('expenses.*','bill_lading','cont_no','client_name')
                                ->join('containers','containers.id','=','expenses.container_id')
                                ->join('laddings','laddings.id','=','containers.ladding_id')
                                ->join('tracks','tracks.id','=','laddings.track_id')
                                ->latest()->paginate(10);


            $data = Ladding::latest()->get();
            $track = Track::latest()->get();
            return view('expenses.finance',compact('normal','expenses_data','ladding','expenses','cont_bill','data','track'));

        }
        else
        {
            return JsonResponse::create(['error' => 'access-denied'],401);
        }

    }




    /**
     * @param Request $request
     * @return static
     */

    public function ExpensesSort(Request $request)
    {
        $expenses = Expenses::with('container')->where('finance_status','LIKE','pending')->count();
        $normal = NormalExpenses::where('finance_status','LIKE','pending')->count();


        $user   =Auth::user();

        $data   = Ladding::latest()->get();
        $track  = Track::latest()->get();

        if($user->hasRole('finance'))
        {
            $from = $request->get('date1');
            $to = $request->get('date2');
            $service    = $request->get('activity');

            /**
             *
             * If you select From one date  to another Date
             **/

            if(($from  !='') AND  ($to !='') AND ($service !=''))
            {


                $expenses_data = Expenses::select('expenses.*','bill_lading','cont_no','client_name')
                    ->join('containers','containers.id','=','expenses.container_id')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->join('tracks','tracks.id','=','laddings.track_id')
                    ->where('expenses.expenses_date' ,'>=',$from)
                    ->where('expenses.expenses_date' ,'<=',$to)
                    ->where('expenses.reason', 'like', '%' . $service . '%')
                    ->orderby('laddings.id')
                    ->paginate(10);

                $cont_bill = Container::select('containers.*','bill_lading')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->get();

                $ladding = DB::table("laddings")->pluck("bill_lading","id");
                return view('expenses.finance',compact('expenses','ladding','expenses_data','normal','cont_bill','data','track'));
            }

            elseif(($from  =='') AND  ($to =='') AND ($service !=''))
            {




                $expenses_data = Expenses::select('expenses.*','bill_lading','cont_no','client_name')
                    ->join('containers','containers.id','=','expenses.container_id')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->join('tracks','tracks.id','=','laddings.track_id')
                    ->where('expenses.reason', 'like', '%' . $service . '%')
                    ->orderby('laddings.id')
                    ->paginate(10);

                $cont_bill = Container::select('containers.*','bill_lading')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->get();

                $ladding = DB::table("laddings")->pluck("bill_lading","id");
                return view('expenses.finance',compact('expenses','ladding','expenses_data','normal','cont_bill','data','track'));
            }
            /**
             *
             * If you select From one date only
             **/

            else  if(($from  !=null) AND ($to ==null))
            {

                $expenses_data = Expenses::select('expenses.*','bill_lading','cont_no','client_name')
                    ->join('containers','containers.id','=','expenses.container_id')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->join('tracks','tracks.id','=','laddings.track_id')
                    ->where('expenses.expenses_date' ,'>=',$from)
                    ->orderby('laddings.id')
                    ->paginate(10);

                $cont_bill = Container::select('containers.*','bill_lading')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->get();
                $ladding = DB::table("laddings")->pluck("bill_lading","id");
                return view('expenses.finance',compact('expenses','expenses_data','normal','ladding','cont_bill','data','track'));
            }

            /**
             *
             * If you select To the last date
             *
             **/

            else   if(($from  =='') AND ($to !=''))
            {


                $expenses_data = Expenses::select('expenses.*','bill_lading','cont_no','client_name')
                    ->join('containers','containers.id','=','expenses.container_id')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->join('tracks','tracks.id','=','laddings.track_id')
                    ->where('expenses_date' ,'<=',$to)
                    ->orderby('laddings.id')
                    ->paginate(10);

                $cont_bill = Container::select('containers.*','bill_lading')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->get();

                $ladding = DB::table("laddings")->pluck("bill_lading","id");
                return view('expenses.finance',compact('expenses_data','normal','expenses','ladding','cont_bill','data','track'));
            }
            /**
             *
             * If you don't select anything
             *
             **/

            else
            {

                $ladding = DB::table("laddings")->pluck("bill_lading","id");

                $expenses_data = Expenses::select('expenses.*','bill_lading','cont_no','client_name')
                    ->join('containers','containers.id','=','expenses.container_id')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->join('tracks','tracks.id','=','laddings.track_id')
                    ->orderby('laddings.id')
                    ->paginate(10);

                $cont_bill = Container::select('containers.*','bill_lading')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->get();
                return view('expenses.finance',compact('expenses_data','normal','ladding','expenses','cont_bill','data','track'));
            }
        }
        else if($user->hasRole('super'))
        {

            $from = $request->get('date1');
            $to = $request->get('date2');
            $service    = $request->get('activity');


            /**
             *
             * If you select From one date  to another Date
             **/

            if(($from  !='') AND  ($to !='') AND ($service !=''))
            {



                $expenses_data = Expenses::select('expenses.*','bill_lading','cont_no','client_name')
                    ->join('containers','containers.id','=','expenses.container_id')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->join('tracks','tracks.id','=','laddings.track_id')
                    ->where('expenses.expenses_date' ,'>=',$from)
                    ->where('expenses.expenses_date' ,'<=',$to)
                    ->where('expenses.reason', 'like', '%' . $service . '%')
                    ->orderby('laddings.id')
                    ->paginate(10);

                $cont_bill = Container::select('containers.*','bill_lading')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->get();
                $ladding = DB::table("laddings")->pluck("bill_lading","id");
                return view('expenses.super',compact('expenses_data','normal','expenses','ladding','cont_bill','data','track'));
            }
            elseif(($from  =='') AND  ($to =='') AND ($service !=''))
            {



                $expenses_data = Expenses::select('expenses.*','bill_lading','cont_no','client_name')
                    ->join('containers','containers.id','=','expenses.container_id')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->join('tracks','tracks.id','=','laddings.track_id')
                    ->where('expenses.reason', 'like', '%' . $service . '%')
                    ->orderby('laddings.id')
                    ->paginate(10);

                $cont_bill = Container::select('containers.*','bill_lading')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->get();
                $ladding = DB::table("laddings")->pluck("bill_lading","id");
                return view('expenses.super',compact('expenses_data','normal','expenses','ladding','cont_bill','data','track'));
            }
            /**
             *
             * If you select From one date only
             **/

            else  if(($from  !=null) AND ($to ==null))
            {


                $expenses_data = Expenses::select('expenses.*','bill_lading','cont_no','client_name')
                    ->join('containers','containers.id','=','expenses.container_id')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->join('tracks','tracks.id','=','laddings.track_id')
                    ->where('expenses.expenses_date' ,'>=',$from)
                    ->orderby('laddings.id')
                    ->paginate(10);

                $cont_bill = Container::select('containers.*','bill_lading')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->get();
                $ladding = DB::table("laddings")->pluck("bill_lading","id");
                return view('expenses.super',compact('expenses_data','normal','expenses','ladding','cont_bill','data','track'));
            }

            /**
             *
             * If you select To the last date
             *
             **/

            else   if(($from  =='') AND ($to !=''))
            {

                $expenses_data = Expenses::select('expenses.*','bill_lading','cont_no','client_name')
                    ->join('containers','containers.id','=','expenses.container_id')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->join('tracks','tracks.id','=','laddings.track_id')
                    ->where('expenses.expenses_date' ,'<=',$to)
                    ->orderby('laddings.id')
                    ->paginate(10);

                $ladding = DB::table("laddings")->pluck("bill_lading","id");
                $cont_bill = Container::select('containers.*','bill_lading')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->get();
                return view('expenses.super',compact('expenses_data','normal','expenses','ladding','cont_bill','data','track'));
            }
            /**
             *
             * If you don't select anything
             *
             **/

            else
            {

                $ladding    = DB::table("laddings")->pluck("bill_lading","id");
                $expenses_data = Expenses::select('expenses.*','bill_lading','cont_no','client_name')
                    ->join('containers','containers.id','=','expenses.container_id')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->join('tracks','tracks.id','=','laddings.track_id')
                    ->orderby('laddings.id')
                    ->paginate(10);

                $cont_bill = Container::select('containers.*','bill_lading')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->get();
                return view('expenses.super',compact('expenses_data','normal','ladding','expenses','cont_bill','data','track'));
            }
        }
        else
        {
            return JsonResponse::create(['error' => 'access-denied'],401);
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }


    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|static
     *
     */
    public function ExpenseConsign(Request $request)
    {

        $container         = $request->get('container_id');
        $bl                 = $request->get('bill_lading');
        $client_name        = $request->get('client_name');

        $expenses = Expenses::with('container')->where('finance_status','LIKE','pending')->count();

        $normal = NormalExpenses::where('finance_status','LIKE','pending')->count();

        $user   =Auth::user();

        if($user->hasRole('super'))
        {

            if ($container != "")
            {
                $cont_bill  = Container::select('containers.*','bill_lading')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->get();
                $ladding    = DB::table("laddings")->pluck("bill_lading","id");

                $expenses_data = Expenses::select('expenses.*','bill_lading','cont_no','client_name')
                    ->join('containers','containers.id','=','expenses.container_id')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->join('tracks','tracks.id','=','laddings.track_id')
                    ->where('containers.id','=',$container)
                    ->latest()->paginate(10);
                $data = Ladding::latest()->get();
                $track = Track::latest()->get();
                return view('expenses.super',compact('ladding','expenses_data','normal','expenses','cont_bill','data','track'));


            }
            elseif ($bl !="")
            {
                $cont_bill  = Container::select('containers.*','bill_lading')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->get();
                $ladding    = DB::table("laddings")->pluck("bill_lading","id");
                $expenses_data = Expenses::select('expenses.*','bill_lading','cont_no','client_name')
                    ->join('containers','containers.id','=','expenses.container_id')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->join('tracks','tracks.id','=','laddings.track_id')
                    ->where('laddings.id','=',$bl)
                    ->latest()->paginate(10);
                $data = Ladding::latest()->get();
                $track = Track::latest()->get();
                return view('expenses.super',compact('ladding','expenses_data','normal','expenses','cont_bill','data','track'));
            }
            elseif ($client_name !=="")
            {
                $cont_bill  = Container::select('containers.*','bill_lading')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->get();
                $ladding    = DB::table("laddings")->pluck("bill_lading","id");
                $expenses_data = Expenses::select('expenses.*','bill_lading','cont_no','client_name')
                    ->join('containers','containers.id','=','expenses.container_id')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->join('tracks','tracks.id','=','laddings.track_id')
                    ->where('tracks.client_name','LIKE',$client_name)
                    ->latest()->paginate(10);
                $data = Ladding::latest()->get();
                $track = Track::latest()->get();
                return view('expenses.super',compact('ladding','expenses_data','normal','expenses','cont_bill','data','track'));
            }
            else
            {
                $cont_bill = Container::select('containers.*','bill_lading')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->get();
                $ladding = DB::table("laddings")->pluck("bill_lading","id");
                $expenses_data = Expenses::select('expenses.*','bill_lading','cont_no','client_name')
                    ->join('containers','containers.id','=','expenses.container_id')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->join('tracks','tracks.id','=','laddings.track_id')->latest()->paginate(10);
                $data = Ladding::latest()->get();
                $track = Track::latest()->get();
                return view('expenses.super',compact('ladding','expenses_data','normal','expenses','cont_bill','data','track'));

            }


            return JsonResponse::create(['error' => 'access-denied'],401);

        }
        elseif($user->hasRole('finance'))
        {

            if ($container != "")
            {
                $cont_bill  = Container::select('containers.*','bill_lading')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->get();
                $ladding    = DB::table("laddings")->pluck("bill_lading","id");

                $expenses_data = Expenses::select('expenses.*','bill_lading','cont_no','client_name')
                    ->join('containers','containers.id','=','expenses.container_id')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->join('tracks','tracks.id','=','laddings.track_id')
                    ->where('containers.id','=',$container)
                    ->latest()->paginate(10);
                $data = Ladding::latest()->get();
                $track = Track::latest()->get();
                return view('expenses.finance',compact('ladding','expenses_data','normal','expenses','cont_bill','data','track'));


            }
            elseif ($bl !="")
            {
                $cont_bill  = Container::select('containers.*','bill_lading')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->get();
                $ladding    = DB::table("laddings")->pluck("bill_lading","id");
                $expenses_data = Expenses::select('expenses.*','bill_lading','cont_no','client_name')
                    ->join('containers','containers.id','=','expenses.container_id')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->join('tracks','tracks.id','=','laddings.track_id')
                    ->where('laddings.id','=',$bl)
                    ->latest()->paginate(10);
                $data = Ladding::latest()->get();
                $track = Track::latest()->get();
                return view('expenses.finance',compact('ladding','expenses_data','normal','expenses','cont_bill','data','track'));
            }
            elseif ($client_name !=="")
            {
                $cont_bill  = Container::select('containers.*','bill_lading')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->get();
                $ladding    = DB::table("laddings")->pluck("bill_lading","id");
                $expenses_data = Expenses::select('expenses.*','bill_lading','cont_no','client_name')
                    ->join('containers','containers.id','=','expenses.container_id')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->join('tracks','tracks.id','=','laddings.track_id')
                    ->where('tracks.client_name','LIKE',$client_name)
                    ->latest()->paginate(10);
                $data = Ladding::latest()->get();
                $track = Track::latest()->get();
                return view('expenses.finance',compact('ladding','expenses_data','normal','expenses','cont_bill','data','track'));
            }
            else
            {
                $cont_bill = Container::select('containers.*','bill_lading')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->get();

                $ladding = DB::table("laddings")->pluck("bill_lading","id");
                $expenses_data = Expenses::select('expenses.*','bill_lading','cont_no','client_name')
                    ->join('containers','containers.id','=','expenses.container_id')
                    ->join('laddings','laddings.id','=','containers.ladding_id')
                    ->join('tracks','tracks.id','=','laddings.track_id')
                    ->latest()->paginate(10);
                $data = Ladding::latest()->get();
                $track = Track::latest()->get();
                return view('expenses.finance',compact('ladding','expenses_data','normal','expenses','cont_bill','data','track'));

            }


            return JsonResponse::create(['error' => 'access-denied'],401);

        }
        else
        {
            return JsonResponse::create(['error' => 'access-denied'],401);
        }
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|static
     */
    public function ViewRequest(Request $request ,$id)
    {
        $user   =Auth::user();
        if($user->hasRole('finance') || ($user->hasRole('super')))
        {

            $ladding = DB::table("laddings")->pluck("bill_lading","id");
            $expenses = Expenses::with('container','user','staff')->where('id','=',$id)->latest()->get();

            return view('expenses.request_view',compact('ladding','expenses'));
        }
        else
        {
            return JsonResponse::create(['error' => 'access-denied'],401);
        }
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|static
     */
    public function RequestAmount()

    {
        $from = '2019';
        $to = '2030';
        $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
        $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

        $user   =Auth::user();
        if($user->hasRole('admin')) {

            $user_id  = Auth::id();
            $expenses_data = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',$user_id)->latest()->get();
            $ladding = DB::table("laddings")->whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->pluck("bill_lading","id");
            return view('expenses.admin',compact('ladding','expenses','normal','expenses_data'));
        }
        elseif ($user->hasRole('sales'))
        {
            $user_id  = Auth::id();
            $expenses_data = Expenses::select('expenses.*','bill_lading','cont_no','client_name')
                ->join('containers','containers.id','=','expenses.container_id')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->join('tracks','tracks.id','=','laddings.track_id')
                ->where('expenses.user_id','=',$user_id)->latest()->get();
//            $expenses = Expenses::with('container')->where('user_id','=',$user_id)->latest()->get();
            $ladding = DB::table("laddings")->pluck("bill_lading","id");
            return view('expenses.sales',compact('ladding','expenses_data','normal','expenses'));
        }
        elseif ($user->hasRole('documentation'))
        {
            $user_id  = Auth::id();
            $expenses_data = Expenses::select('expenses.*','bill_lading','cont_no','client_name')
                ->join('containers','containers.id','=','expenses.container_id')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->join('tracks','tracks.id','=','laddings.track_id')
                ->where('expenses.user_id','=',$user_id)->latest()->get();
//            $expenses = Expenses::with('container')->where('user_id','=',$user_id)->latest()->get();
            $ladding = DB::table("laddings")->pluck("bill_lading","id");
            return view('expenses.documentation',compact('expenses_data','normal','ladding','expenses'));
        }
        elseif ($user->hasRole('declaration'))
        {
            $user_id  = Auth::id();
            $expenses_data = Expenses::select('expenses.*','bill_lading','cont_no','client_name')
                ->join('containers','containers.id','=','expenses.container_id')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->join('tracks','tracks.id','=','laddings.track_id')
                ->where('expenses.user_id','=',$user_id)->latest()->get();
//            $expenses = Expenses::with('container')->where('user_id','=',$user_id)->latest()->get();
            $ladding = DB::table("laddings")->pluck("bill_lading","id");
            return view('expenses.declaration',compact('expenses_data','normal','ladding','expenses'));
        }
        elseif ($user->hasRole('shipping'))

        {
            $user_id  = Auth::id();
            $expenses_data = Expenses::select('expenses.*','bill_lading','cont_no','client_name')
                ->join('containers','containers.id','=','expenses.container_id')
                ->join('laddings','laddings.id','=','containers.ladding_id')
                ->join('tracks','tracks.id','=','laddings.track_id')
                ->where('expenses.user_id','=',$user_id)->latest()->get();
//            $expenses = Expenses::with('container')->where('user_id','=',$user_id)->latest()->get();
            $ladding = DB::table("laddings")->pluck("bill_lading","id");
            return view('expenses.shipping',compact('expenses_data','normal','ladding','expenses'));
        }
        else
        {
            return JsonResponse::create(['error' => 'access-denied'],401);
        }
    }
    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */

    public function ExpensesForm (Request $request)
    {

        $user_id =Auth::id();
        $name   =Auth::user()->name;
        $container  = $request->get('cont_no');

        $container_full = Container::where('id','=',$container)->first()->cont_no;

        $service    = $request->get('reason');
        $amount     = $request->get('amount_requested');
        $currency   = $request->get('amount_currency');

        if($currency =='USD')
        {

            $date = $request->get('expenses_date');
            $newDateFormat = Carbon::parse($date)->format('Y-m-d');
            $cont_id = $request->get('cont_no');
            $request->merge(['user_id' => $user_id , 'container_id' =>$cont_id,'staff_id'=> $user_id,'expenses_date' =>$newDateFormat,'currency_usd'=>$amount]);
            $order =  Expenses::create($request->all());
            ($order->save());
            Session::flash('message', 'Request   successful sent');
            return back();

        }

        elseif($currency == 'TZS')
        {

            $date = $request->get('expenses_date');
            $newDateFormat = Carbon::parse($date)->format('Y-m-d');
            $cont_id = $request->get('cont_no');
            $request->merge(['user_id' => $user_id , 'container_id' =>$cont_id,'staff_id'=> $user_id,'expenses_date' =>$newDateFormat,'currency_tzs'=>$amount]);
            $order =  Expenses::create($request->all());
            ($order->save());

            Session::flash('message', 'Request   successful sent');
            return back();

        }
        else
        {
            return JsonResponse::create(['error' => 'access-denied'], 401);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $user = Auth::user();
        if ($user->hasRole('super') || $user->hasRole('finance'))
        {

            $super_status = $request->get('finance_status');


            if ($super_status === 'voucher closed')
            {
                return JsonResponse::create(['error' => 'voucher has been authorized '], 401);
            }
            elseif ($super_status ===   'canceled by super admin')
            {
                return JsonResponse::create(['error' => 'voucher has been rejected by super admin '], 401);
            }
            else
            {

                $currency   = $request->get('amount_currency');
                $amount     = $request->get('amount_requested');

                if($currency =='USD')
                {


                    $date = $request->get('expenses_date');
                    $newDateFormat = Carbon::parse($date)->format('Y-m-d');
                    $user_id = Auth::id();
                    $request->merge(['staff_id' => $user_id ,'expenses_date' =>$newDateFormat,'amount_requested'=>$amount,'currency_usd'=>$amount,'currency_tzs'=>'0']);
                    $order = Expenses::findOrFail($id);
                    $all_data = $request->all();
                    $order->update($all_data);
                    Session::flash('updated', ' Updated Successful ');
                    return redirect('/expenses');

                }

                elseif($currency == 'TZS')
                {

                    $date = $request->get('expenses_date');
                    $newDateFormat = Carbon::parse($date)->format('Y-m-d');
                    $user_id = Auth::id();
                    $request->merge(['staff_id' => $user_id ,'expenses_date' =>$newDateFormat,'amount_requested'=>$amount,'currency_tzs'=>$amount,'currency_usd'=>'0']);
                    $order = Expenses::findOrFail($id);
                    $all_data = $request->all();
                    $order->update($all_data);
                    Session::flash('updated', ' Updated Successful ');
                    return redirect('/expenses');

                }
                else
                {
                    return JsonResponse::create(['error' => 'access-denied'], 401);
                }
            }

        }
        else
        {
            return JsonResponse::create(['error' => 'access-denied'], 401);
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Exception
     */
    public function destroy($id)
    {

        $user = Auth::user();
        if ($user->hasRole('super') ||  $user->hasRole('finance')) {
            $training = Expenses::findOrfail($id);
            $training->delete();

            Session::flash('delete', ' Deleted Successful');
            return redirect('/expenses');
        }
        else
        {
            return JsonResponse::create(['error' => 'access-denied'], 401);
        }

    }
}
