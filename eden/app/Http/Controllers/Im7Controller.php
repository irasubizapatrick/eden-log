<?php

namespace App\Http\Controllers;

use App\BlDeclaration;
use App\Expenses;
use App\Ladding;
use App\NormalExpenses;
use App\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\DB;
use Session;

class Im7Controller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user   =Auth::user();
        if($user->hasRole('admin')) {

            $from = '2021';
            $to = '2030';
            $ladding = DB::table("laddings")->whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->latest()->pluck("bill_lading","id");
            $declaration = BlDeclaration::Select('bl_declarations.*','bill_lading')
                ->join('declaration_types','declaration_types.id','LIKE','bl_declarations.declaration_id')
                ->join('laddings','laddings.id','=','bl_declarations.ladding_id')
                ->where('declaration_types.cargo_declaration_type','LIKE','IM7')
                ->whereBetween(DB::raw('YEAR(bl_declarations.created_at)'),[$from, $to])
                ->get();
            $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
            $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

            return view('declaration.im7', compact('expenses','normal','declaration','ladding'));
        }
        elseif($user->hasRole('super')) {

            $ladding = DB::table("laddings")->latest()->pluck("bill_lading","id");
            $declaration = BlDeclaration::Select('bl_declarations.*','bill_lading')
                ->join('declaration_types','declaration_types.id','LIKE','bl_declarations.declaration_id')
                ->join('laddings','laddings.id','=','bl_declarations.ladding_id')
                ->where('declaration_types.cargo_declaration_type','LIKE','IM7')
                ->get();
            $expenses = Expenses::with('container')->where('finance_status','LIKE','pending')->count();

            $normal = NormalExpenses::where('finance_status','LIKE','pending')->count();

            return view('declaration.super_im7', compact('expenses','normal','declaration','ladding'));
        }
        elseif($user->hasRole('declaration')) {

            $ladding = DB::table("laddings")->latest()->pluck("bill_lading","id");
            $declaration = BlDeclaration::Select('bl_declarations.*','bill_lading')
                ->join('declaration_types','declaration_types.id','LIKE','bl_declarations.declaration_id')
                ->join('laddings','laddings.id','=','bl_declarations.ladding_id')
                ->where('declaration_types.cargo_declaration_type','LIKE','IM7')
                ->get();

            $expenses = Expenses::with('container')->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
            $normal = NormalExpenses::where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

            return view('declaration.declaration_im7', compact('expenses','normal','declaration','ladding'));
        }
        elseif($user->hasRole('finance')) {

            $ladding = DB::table("laddings")->latest()->pluck("bill_lading","id");
            $declaration = BlDeclaration::Select('bl_declarations.*','bill_lading')
                ->join('declaration_types','declaration_types.id','LIKE','bl_declarations.declaration_id')
                ->join('laddings','laddings.id','=','bl_declarations.ladding_id')
                ->where('declaration_types.cargo_declaration_type','LIKE','IM7')
                ->get();
            $expenses = Expenses::with('container')->where('finance_status','LIKE','pending')->count();

            $normal = NormalExpenses::where('finance_status','LIKE','pending')->count();

            return view('declaration.finance_im7', compact('expenses','normal','declaration','ladding'));
        }

        else  if($user->hasRole('agent')) {

            $ladding = DB::table("laddings")->join('tracks','tracks.id','=','laddings.track_id')
                ->join('agents','agents.id','=','tracks.agent_id')
                ->where('agents.user_id','=',Auth::id())
                ->pluck("bill_lading","laddings.id");
            $profile = User::where('id','=',Auth::id())->get();
            $lading      = Ladding::all();
            $declaration = BlDeclaration::Select('bl_declarations.*','bill_lading')
                ->join('declaration_types','declaration_types.id','LIKE','bl_declarations.declaration_id')
                ->join('laddings','laddings.id','=','bl_declarations.ladding_id')
                ->join('tracks','tracks.id','=','laddings.track_id')
                ->join('agents','agents.id','=','tracks.agent_id')
                ->where('agents.user_id','=',Auth::id())
                ->where('declaration_types.cargo_declaration_type','LIKE','IM7')
                ->latest()
                ->get();
            $expenses = Expenses::with('container')->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
            $normal = NormalExpenses::where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

            return view('declaration.agent_im7', compact('expenses','normal','declaration','lading','profile','ladding'));
        }
        else
        {
            return JsonResponse::create(['error' => 'access-denied'],401);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|static
     */

    public function searchBlIM7(Request $request )
    {

        $user   =Auth::user();
        $get_bl = $request->get('ladding_id');
        if($user->hasRole('admin')) {

            $ladding = DB::table("laddings")->latest()->pluck("bill_lading","id");
            $lading      = Ladding::all();
            $declaration = BlDeclaration::Select('bl_declarations.*','bill_lading')
                ->join('declaration_types','declaration_types.id','LIKE','bl_declarations.declaration_id')
                ->join('laddings','laddings.id','=','bl_declarations.ladding_id')
                ->where('declaration_types.cargo_declaration_type','LIKE','IM7')
                ->where('laddings.id','=',$get_bl)
                ->get();
            $expenses = Expenses::with('container')->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
            $normal = NormalExpenses::where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

            return view('declaration.im7', compact('expenses','normal','declaration','lading','ladding'));
        }
        elseif($user->hasRole('super'))
        {

            $ladding = DB::table("laddings")->latest()->pluck("bill_lading","id");
            $lading      = Ladding::all();
            $declaration = BlDeclaration::Select('bl_declarations.*','bill_lading')
                ->join('declaration_types','declaration_types.id','LIKE','bl_declarations.declaration_id')
                ->join('laddings','laddings.id','=','bl_declarations.ladding_id')
                ->where('declaration_types.cargo_declaration_type','LIKE','IM7')
                ->where('laddings.id','=',$get_bl)
                ->get();
            $expenses = Expenses::with('container')->where('finance_status','LIKE','pending')->count();

            $normal = NormalExpenses::where('finance_status','LIKE','pending')->count();

            return view('declaration.super_im7', compact('expenses','normal','declaration','lading','ladding'));
        }
        elseif($user->hasRole('finance'))
        {
            $ladding = DB::table("laddings")->latest()->pluck("bill_lading","id");
            $lading      = Ladding::all();
            $declaration = BlDeclaration::Select('bl_declarations.*','bill_lading')
                ->join('declaration_types','declaration_types.id','LIKE','bl_declarations.declaration_id')
                ->join('laddings','laddings.id','=','bl_declarations.ladding_id')
                ->where('declaration_types.cargo_declaration_type','LIKE','IM7')
                ->where('laddings.id','=',$get_bl)
                ->get();
            $expenses = Expenses::with('container')->where('finance_status','LIKE','pending')->count();

            $normal = NormalExpenses::where('finance_status','LIKE','pending')->count();

            return view('declaration.finance_im7', compact('expenses','normal','declaration','lading','ladding'));
        }
        elseif($user->hasRole('declaration'))
        {
            $ladding = DB::table("laddings")->latest()->pluck("bill_lading","id");
            $lading      = Ladding::all();
            $declaration = BlDeclaration::Select('bl_declarations.*','bill_lading')
                ->join('declaration_types','declaration_types.id','LIKE','bl_declarations.declaration_id')
                ->join('laddings','laddings.id','=','bl_declarations.ladding_id')
                ->where('declaration_types.cargo_declaration_type','LIKE','IM7')
                ->where('laddings.id','=',$get_bl)
                ->get();
            $expenses = Expenses::with('container')->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
            $normal = NormalExpenses::where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

            return view('declaration.declaration_im7', compact('expenses','normal','declaration','lading','ladding'));
        }
        elseif($user->hasRole('agent'))
        {

            $ladding = DB::table("laddings")->join('tracks','tracks.id','=','laddings.track_id')
                ->join('agents','agents.id','=','tracks.agent_id')
                ->where('agents.user_id','=',Auth::id())
                ->pluck("bill_lading","laddings.id");
            $profile = User::where('id','=',Auth::id())->get();
            $lading      = Ladding::all();
            $declaration = BlDeclaration::Select('bl_declarations.*','bill_lading')
                ->join('declaration_types','declaration_types.id','LIKE','bl_declarations.declaration_id')
                ->join('laddings','laddings.id','=','bl_declarations.ladding_id')
                ->join('tracks','tracks.id','=','laddings.track_id')
                ->join('agents','agents.id','=','tracks.agent_id')
                ->where('agents.user_id','=',Auth::id())
                ->where('declaration_types.cargo_declaration_type','LIKE','IM7')
                ->where('laddings.id','=',$get_bl)
                ->latest()
                ->get();
            $expenses = Expenses::with('container')->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
            $normal = NormalExpenses::where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

            return view('declaration.agent_im7', compact('expenses','normal','declaration','lading','profile','ladding'));
        }


        else
        {
            return JsonResponse::create(['error' => 'access-denied'],401);
        }
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user_id = Auth::id();
        $request->merge(['user_id' => $user_id]);
        $order =  BlDeclaration::findOrFail($id);


        $c2                         = $request->file('c2');
        $t1_verified                = $request->file('t1_verified');
        $t1                         = $request->file('t1');
        $whz                        = $request->file('whz');


        if(isset($c2)) {
            if (isset($c2)) {
                $extension = $request->file('c2')->getClientOriginalExtension();
            }
            $sha = 'c2' . md5(time());
            if(isset($extension)){
                $filename1 = date('Y').$sha . "sgfs." . $extension;
            }
            $destination_path = 'Declaration/';
            if (empty($c2)) {
                File::makeDirectory($c2, 0775, true, true);
            }
            if(isset($filename1)){
                $order->c2 =  $filename1;
                $request->file('c2')->move($destination_path, $filename1);
                $order->save();
            }
        }

        if(isset($t1_verified)) {
            if (isset($t1_verified)) {
                $extension = $request->file('t1_verified')->getClientOriginalExtension();
            }
            $sha = 't1_verified' . md5(time());
            if(isset($extension)){
                $filename2 = date('Y').$sha . "sgfs." . $extension;
            }
            $destination_path = 'Declaration/';
            if (empty($t1_verified)) {
                File::makeDirectory($t1_verified, 0775, true, true);
            }
            if(isset($filename2)){
                $order->t1_verified =  $filename2;
                $request->file('t1_verified')->move($destination_path, $filename2);
                $order->save();
            }
        }

        if(isset($t1)) {
            if (isset($t1)) {
                $extension = $request->file('t1')->getClientOriginalExtension();
            }
            $sha = 't1' . md5(time());
            if(isset($extension)){
                $filename3 = date('Y').$sha . "sgfs." . $extension;
            }
            $destination_path = 'Declaration/';
            if (empty($t1)) {
                File::makeDirectory($t1, 0775, true, true);
            }
            if(isset($filename3)){
                $order->t1 =  $filename3;
                $request->file('t1')->move($destination_path, $filename3);
                $order->save();
            }
        }

        if(isset($whz)) {
            if (isset($whz)) {
                $extension = $request->file('whz')->getClientOriginalExtension();
            }
            $sha = 'whz' . md5(time());
            if(isset($extension)){
                $filename4 = date('Y').$sha . "sgfs." . $extension;
            }
            $destination_path = 'Declaration/';
            if (empty($whz)) {
                File::makeDirectory($whz, 0775, true, true);
            }
            if(isset($filename4)){
                $order->whz =  $filename4;
                $request->file('whz')->move($destination_path, $filename4);
                $order->save();
            }
        }

        if(isset($other_clearance_doc)) {
            if (isset($other_clearance_doc)) {
                $shipping_line_ex = $request->file('other_clearance_doc')->getClientOriginalExtension();
            }
            $sha = 'other_clearance_doc' . md5(time().date("y"));
            if(isset($other_clearance_doc)){
                $filename6 = date('Y').$sha . "fghjm." . $shipping_line_ex;
            }
            $shipping_line_path = 'tax_clearance/';
            if (empty($other_clearance_doc)) {
                File::makeDirectory($other_clearance_doc, 0775, true, true);
            }
            if(isset($filename6)){
                $order->other_clearance_doc = $filename6;
                $request->file('other_clearance_doc')->move($shipping_line_path, $filename6);
                $order->save();
            }
        }
        $all_data =  $request->all();

        $all_data['c2']                        = !empty($filename1) ? $filename1               : $order->c2;
        $all_data['t1_verified']               = !empty($filename2) ? $filename2               : $order->t1_verified;
        $all_data['t1']                        = !empty($filename3) ? $filename3               : $order->t1;
        $all_data['whz']                       = !empty($filename4) ? $filename4               : $order->whz;
        $order->update($all_data);
        Session::flash('message', 'Declaration');
        return  redirect('/declaration/im7');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
