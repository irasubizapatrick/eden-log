<?php

namespace App\Http\Controllers;

use App\Agent;
use App\Expenses;
use App\NormalExpenses;
use App\Role;
use App\Sales;
use App\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class SalesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user   =Auth::user();
        if($user->hasRole('admin'))
        {
            $staff = Sales::select('sales.*', 'display_name')
                    ->join('users', 'users.id', '=', 'sales.user_id')
                    ->join('role_user', 'role_user.user_id', '=', 'sales.user_id')
                    ->join('roles', 'roles.id', '=', 'role_user.role_id')
                    ->where('users.email','NOT LIKE','demo@mik.com')
                    ->where('users.email','NOT LIKE','admin@log.com')
                    ->latest()
                    ->paginate(10);
            $expenses = Expenses::with('container')->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
            $normal = NormalExpenses::where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

            return view('staff.list_staff', compact('normal','expenses','staff'));
        }
        else if($user->hasRole('super'))
        {
            $expenses = Expenses::with('container')->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
            $normal = NormalExpenses::where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

            $staff = Sales::select('sales.*', 'display_name')
                ->join('users', 'users.id', '=', 'sales.user_id')
                ->join('role_user', 'role_user.user_id', '=', 'sales.user_id')
                ->join('roles', 'roles.id', '=', 'role_user.role_id')
                ->where('users.email','NOT LIKE','demo@mik.com')
                ->where('users.email','NOT LIKE','admin@log.com')
                ->latest()
                ->paginate(10);

            return view('staff.super_list_staff', compact('expenses','normal','staff'));
        }
        else
        {
            return JsonResponse::create(['error' => 'access-denied'],401);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $role = Role::where('display_name', $request['role'])->first();

        $user               =   new User;
        $user->name         =   $request->get('name');
        $user->email        =   $request->get('employee_email');
        $user->password     =   Hash::Make($request->get("password"));
        $user->save();

        $user->attachRole($role);
        if($user->save())

        $user_id    =Auth::id();
        $sales_id   =$user->id;
        $request->merge(['company_id' => $user_id ,'user_id' => $sales_id]);
        $agent =  Sales::create($request->all());
        $agent->save();

        Session::flash('message', 'Staff  Account Created successfully');
        return redirect("/staff");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $get_user_id = Sales::where('id','=',$id)->first()->user_id;
        $role = Role::where('display_name', $request['role'])->first();
        $user_password     =   Hash::Make($request->get("password"));
        $user_email        =   $request->get('employee_email');
        $name = $request->get('employee_name');
        $order = Sales::findOrFail($id);
        $data = $request->all();
        if($order->update($data));

            $user = User::findOrFail($get_user_id);
            $user->roles()->sync($role);
            $request->merge(['email' =>$user_email,'name' =>$name]);
            $user_data =$request->all();
            $user->update($user_data);


        Session::flash('updated', 'Staff created successfully');
        return redirect('/staff');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $area   =Sales::findOrfail($id);
        $area->delete();
        Session::flash('delete', 'Staff  Account Deleted successfully');
        return redirect('/staff');
    }
}