<?php

namespace App\Http\Controllers;

use App\Agent;
use App\Expenses;
use App\Message;
use App\MessageStatus;
use App\NormalExpenses;
use App\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Session;

class MessageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user   =Auth::user();
        if($user->hasRole('admin'))
        {
            $agent    = Agent::all();
            $message  = Message::with('agent')->get();
            $expenses = Expenses::with('container')->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
            $normal = NormalExpenses::where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

            return view('conv.admin_message',compact('expenses','normal','message','agent'));
        }
        else if($user->hasRole('sales'))
        {
            $agent    = Agent::all();
            $message  = Message::with('agent')->get();
            $expenses = Expenses::with('container')->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
            $normal = NormalExpenses::where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

            return view('conv.sales_message',compact('expenses','normal','message','agent'));
        }
        else if($user->hasRole('documentation'))
        {
            $agent    = Agent::all();
            $message  = Message::with('agent')->get();
            $expenses = Expenses::with('container')->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
            $normal = NormalExpenses::where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

            return view('conv.documentation_message',compact('expenses','normal','message','agent'));
        }
        else if($user->hasRole('declaration'))
        {
            $agent    = Agent::all();
            $message  = Message::with('agent')->get();
            $expenses = Expenses::with('container')->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
            $normal = NormalExpenses::where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

            return view('conv.declaration_message',compact('expenses','normal','message','agent'));
        }
        else if($user->hasRole('shipping'))
        {
            $agent    = Agent::all();
            $message  = Message::with('agent')->get();
            $expenses = Expenses::with('container')->where('finance_status','LIKE','pending')->count();

            $normal = NormalExpenses::where('finance_status','LIKE','pending')->count();

            return view('conv.shipping_message',compact('expenses','normal','message','agent'));
        }
        elseif($user->hasRole('agent'))
        {
            $expenses = Expenses::with('container')->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
            $normal = NormalExpenses::where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

            $user_id  = Auth::id();
            $profile = User::where('id','=',$user_id)->get();
            $agent    = Agent::all()->where('user_id','',$user_id);
            $message     = Message::Select('messages.*')
                        ->join('agents','agents.id','=','messages.agent_id')
                        ->where('agents.user_id','=',$user_id)->get();
            return view('conv.agent_message',compact('expenses','normal','message','agent','profile'));
        }
        else
        {
            return JsonResponse::create(['error' => 'access-denied'],401);
        }
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }


    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|static
     */
    public function ViewConversation($id)
    {
        $user   =Auth::user();
        if($user->hasRole('admin'))
        {

            $message_status = MessageStatus::with('message','user')->where('message_id','=',$id)->get();
            return view('conv.admin_view',compact('message_status'));
        }
        else if($user->hasRole('sales'))
        {

            $message_status = MessageStatus::with('message','user')->where('message_id','=',$id)->get();
            return view('conv.sales_view',compact('message_status'));
        }
        elseif ($user->hasRole('agent'))
        {
            $user_id = Auth::id();
            $profile = User::where('id','=',$user_id)->get();
            $message_status = MessageStatus::with('message','user')->where('message_id','=',$id)->get();
            return view('conv.agent_view',compact('message_status','profile'));
        }
        elseif ($user->hasRole('documentation'))
        {
            $message_status = MessageStatus::with('message','user')->where('message_id','=',$id)->get();
            return view('conv.documentation_view',compact('message_status'));
        }
        elseif ($user->hasRole('declaration'))
        {
            $message_status = MessageStatus::with('message','user')->where('message_id','=',$id)->get();
            return view('conv.declaration_view',compact('message_status'));
        }
        elseif ($user->hasRole('shipping'))
        {
            $message_status = MessageStatus::with('message','user')->where('message_id','=',$id)->get();
            return view('conv.shipping_view',compact('message_status'));
        }
        else
        {
            return JsonResponse::create(['error' => 'access-denied'],401);
        }
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */


    public function reply(Request $request){
        $user_id = Auth::id();
        $request->merge(['user_id' => $user_id]);
        $agent =  MessageStatus::create($request->all());
        $agent->save();
        Session::flash('message', ' Successful');
        return redirect('/conversation');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $user_id    =Auth::id();
        $agent_id   = $request->get('agent_id');
        $request->merge(['user_id' => $user_id,'agent_id' =>$agent_id]);
        $agent =  Message::create($request->all());
        if ($agent->save());
        $request->merge(['user_id' => $user_id,'message_id' =>$agent->id,'message_responses' =>$agent->message_description]);
        $status =  MessageStatus::create($request->all());
        $status->save();
        Session::flash('message', 'Message created Successful');
        return redirect('/conversation');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $status  ='closed';
        $request->merge(['ticket_status' => $status]);
        $department = Message::findOrFail($id);
        $data = $request->all();
        $department->update($data);
        Session::flash('message', 'Message Closed  Successful ');
        return redirect('/conversation');
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Exception
     */
    public function destroy($id)
    {
        $department  =Message::findOrfail($id);
        $department->delete();
        Session::flash('message', 'Message Deleted  Successful ');
        return redirect('/conversation');
    }
}
