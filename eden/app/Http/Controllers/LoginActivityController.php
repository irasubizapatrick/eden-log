<?php

namespace App\Http\Controllers;

use App\Expenses;
use App\LoginActivity;
use App\NormalExpenses;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Auth;

class LoginActivityController extends Controller
{
    public function index()
    {
        $user   =Auth::user();
        if($user->hasRole('admin'))
        {
            $normal = NormalExpenses::where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

            $expenses = Expenses::with('container')->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
            $loginActivities = LoginActivity::with('user')->latest()->get();
            return view('login-activity', compact('loginActivities','expenses','normal'));
        }
        else
        {
            return JsonResponse::create(['error' => 'access-denied'],401);
        }
    }
}
