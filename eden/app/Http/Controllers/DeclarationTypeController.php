<?php

namespace App\Http\Controllers;

use App\DeclarationType;
use App\Expenses;
use App\Ladding;
use App\NormalExpenses;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Auth;
use Session;
class DeclarationTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user   =Auth::user();
        if($user->hasRole('admin')) {

            $lading = Ladding::all();
            $declaration = DeclarationType::with('user')->get();
            $expenses = Expenses::with('container')->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
            $normal = NormalExpenses::where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

            return view('admin.declaration_type', compact('expenses','normal','declaration','lading'));
        }
        else
        {
            return JsonResponse::create(['error' => 'access-denied'],401);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user_id    =Auth::id();
        $request->merge(['user_id' => $user_id]);
        $status =  DeclarationType::create($request->all());
        $status->save();
        Session::flash('message', 'Declaration type created Successful ');
        return redirect('/declaration/type');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $declaration = DeclarationType::findOrFail($id);
        $data = $request->all();
        $declaration->update($data);
        Session::flash('updated', 'Declaration type updated Successful ');
        return redirect('/declaration/type');
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Exception
     */
    public function destroy($id)
    {
        $declaration  =DeclarationType::findOrfail($id);
        $declaration->delete();
        Session::flash('delete', 'Declaration type deleted  Successful ');
        return redirect('/declaration/type');
    }
}
