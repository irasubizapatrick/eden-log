<?php

namespace App\Http\Controllers;

use App\Archive;
use App\Expenses;
use App\Ladding;
use App\NormalExpenses;
use Auth;
use DB;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\File;

use Session;
use Illuminate\Http\Request;

class ArchiveController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user   =Auth::user();

        if($user->hasRole('admin'))
        {
            $from = '2021';
            $to = '2030';
            $lading  = Ladding::whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->get();
            $archive  = Archive::select('archives.*','bill_lading')
                            ->join('laddings','laddings.id','=','archives.ladding_id')
                            ->whereBetween(DB::raw('YEAR(archives.created_at)'),[$from, $to])
                            ->latest()
                            ->get();
            $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
            $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

            return view('admin.archive_list',compact('normal','expenses','archive','lading'));
        }
        elseif($user->hasRole('sales'))
        {

            $from = '2021';
            $to = '2030';
            $ladding  = Ladding::whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->get();
            $archive  = Archive::select('archives.*','bill_lading')
                ->join('laddings','laddings.id','=','archives.ladding_id')
                ->whereBetween(DB::raw('YEAR(archives.created_at)'),[$from, $to])
                ->latest()
                ->get();
            $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
            $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();


            return view('staff.sales_archive_list',compact('normal','expenses','archive','ladding'));
        }
        elseif($user->hasRole('documentation'))
        {
            $from = '2021';
            $to = '2030';
            $ladding  = Ladding::whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->get();
            $archive  = Archive::select('archives.*','bill_lading')
                ->join('laddings','laddings.id','=','archives.ladding_id')
                ->whereBetween(DB::raw('YEAR(archives.created_at)'),[$from, $to])
                ->latest()
                ->get();
            $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
            $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

            return view('documentation.documentation_archive_list',compact('normal','expenses','archive','ladding'));
        }
        elseif($user->hasRole('shipping'))
        {
            $from = '2021';
            $to = '2030';
            $ladding  = Ladding::whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->get();
            $archive  = Archive::select('archives.*','bill_lading')
                ->join('laddings','laddings.id','=','archives.ladding_id')
                ->whereBetween(DB::raw('YEAR(archives.created_at)'),[$from, $to])
                ->latest()
                ->get();
            $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
            $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();


            return view('shipping.shipping_archive_list',compact('normal','expenses','archive','ladding'));
        }
        elseif($user->hasRole('declaration'))
        {
            $from = '2021';
            $to = '2030';
            $ladding  = Ladding::whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->get();
            $archive  = Archive::select('archives.*','bill_lading')
                ->join('laddings','laddings.id','=','archives.ladding_id')
                ->whereBetween(DB::raw('YEAR(archives.created_at)'),[$from, $to])
                ->latest()
                ->get();
            $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
            $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();


            return view('declaration.declaration_archive_list',compact('normal','expenses','archive','ladding'));
        }
        elseif($user->hasRole('finance'))
        {
            $from = '2021';
            $to = '2030';
            $lading  = Ladding::whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->get();
            $archive  = Archive::select('archives.*','bill_lading')
                ->join('laddings','laddings.id','=','archives.ladding_id')
                ->whereBetween(DB::raw('YEAR(archives.created_at)'),[$from, $to])
                ->latest()
                ->get();
            $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
            $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

            return view('finance.finance_archive_list',compact('normal','expenses','archive','lading'));
        }
        else
        {
            return JsonResponse::create(['error' => 'access-denied'],401);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $user_id = Auth::id();
        $request->merge(['user_id' => $user_id]);
        $order =  Archive::create($request->all());


        $invoice_doc                = $request->file('invoice_doc');
        $shipping_line_charges      = $request->file('shipping_line_charges');
        $port_charges               = $request->file('port_charges');
        $customer_payments          = $request->file('customer_payments');
        $other                     = $request->file('other');


        if(isset($invoice_doc)) {
            if (isset($invoice_doc)) {
                $extension1 = $request->file('invoice_doc')->getClientOriginalExtension();
            }
            $sha = 'invoice_doc' . md5(time());
            if(isset($extension1)){
                $filename1 = date('Y').$sha . "sgfs." . $extension1;
            }
            $destination_path = 'archives/';
            if (empty($invoice_doc)) {
                File::makeDirectory($invoice_doc, 0775, true, true);
                // File::makeDirectory($destination_path, $mode = 0777, true, true);
            }
            if(isset($filename1)){
                $order->invoice_doc =  $filename1;
                $request->file('invoice_doc')->move($destination_path, $filename1);
                $order->save();
            }

        }
        if(isset($shipping_line_charges)) {
            if (isset($shipping_line_charges)) {
                $shipping_line_ex = $request->file('shipping_line_charges')->getClientOriginalExtension();
            }
            $sha = 'shipping_line_charges' . md5(time().date("y"));
            if(isset($shipping_line_charges)){
                $filename5 = date('Y').$sha . "fghjm." . $shipping_line_ex;
            }
            $shipping_line_path = 'archives/';
            if (empty($shipping_line_charges)) {
                File::makeDirectory($shipping_line_charges, 0775, true, true);
            }
            if(isset($filename5)){
                $order->shipping_line_charges = $filename5;
                $request->file('shipping_line_charges')->move($shipping_line_path, $filename5);
                $order->save();
            }
        }

        if(isset($port_charges)) {
            if (isset($port_charges)) {
                $insurance_ex = $request->file('port_charges')->getClientOriginalExtension();
            }
            $sha = 'port_charges' . md5("in".time());
            if(isset($insurance_ex)){
                $insurance_file = date('Y').$sha . "ertyui." . $insurance_ex;
            }
            $port_charges_path = 'archives/';
            if (empty($insurance_path)) {
                File::makeDirectory($port_charges_path, 0775, true, true);
            }
            if(isset($insurance_file)){
                $order->port_charges =  $insurance_file;
                $request->file('port_charges')->move($port_charges_path, $insurance_file);
                $order->save();
            }
        }

        if(isset($customer_payments)) {
            if (isset($customer_payments)) {
                $extension2 = $request->file('customer_payments')->getClientOriginalExtension();
            }
            $sha = 'customer_payments' . md5(time());
            if(isset($extension2)){
                $yellow_card_file = date('Y').$sha . "gfgsgf." . $extension2;
            }
            $destination_path1 = 'archives/';
            if (empty($destination_path1)) {
                File::makeDirectory($destination_path1, 0775, true, true);
            }
            if(isset($yellow_card_file)){
                $order->customer_payments = $yellow_card_file;
                $request->file('customer_payments')->move($destination_path1, $yellow_card_file);
                $order->save();
            }
        }

        if(isset($other)) {
            if (isset($other)) {
                $extension2 = $request->file('other')->getClientOriginalExtension();
            }
            $sha = 'other' . md5(time());
            if(isset($extension2)){
                $filename4 = date('Y').$sha . "gfgsgf." . $extension2;
            }
            $destination_path1 = 'archives/';
            if (empty($destination_path1)) {
                File::makeDirectory($destination_path1, 0775, true, true);
            }
            if(isset($filename4)){
                $order->other = $filename4;
                $request->file('other')->move($destination_path1, $filename4);
                $order->save();
            }

        }

        $all_data =  $request->all();

        $all_data['invoice_doc']                = !empty($filename1) ? $filename1                : $order->invoice_doc;
        $all_data['shipping_line_charges']      = !empty($filename5) ? $filename5                : $order->shipping_line_charges;
        $all_data['port_charges']               = !empty($insurance_file) ? $insurance_file      : $order->port_charges;
        $all_data['customer_payments']          = !empty($yellow_card_file) ? $yellow_card_file  : $order->customer_payments;
        $all_data['other']                     = !empty($filename4) ? $filename4                : $order->other;

        $order->update($all_data);
        Session::flash('message', 'Document  Uploaded Successful  ');
        return  redirect('/archive_doc');
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user_id = Auth::id();

        $order =  Archive::findOrFail($id);

        $invoice_doc                = $request->file('invoice_doc');
        $shipping_line_charges      = $request->file('shipping_line_charges');
        $port_charges               = $request->file('port_charges');
        $customer_payments          = $request->file('customer_payments');
        $other                     = $request->file('other');


        if(isset($invoice_doc)) {
            if (isset($invoice_doc)) {
                $extension1 = $request->file('invoice_doc')->getClientOriginalExtension();
            }
            $sha = 'invoice_doc' . md5(time());
            if(isset($extension1)){
                $filename1 = date('Y').$sha . "sgfs." . $extension1;
            }
            $port_charges_path = 'archives/';
            if (empty($invoice_doc)) {
                File::makeDirectory($invoice_doc, 0775, true, true);
            }
            if(isset($filename1)){
                $order->invoice_doc =  $filename1;
                $request->file('invoice_doc')->move($port_charges_path, $filename1);
                $order->save();
            }

        }
        if(isset($shipping_line_charges)) {
            if (isset($shipping_line_charges)) {
                $extension3 = $request->file('shipping_line_charges')->getClientOriginalExtension();
            }
            $sha = 'shipping_line_charges' . md5(time().date("y"));
            if(isset($extension3)){
                $filename2 = date('Y').$sha . "fghjm." . $extension3;
            }
            $port_charges_path = 'archives/';
            if (empty($shipping_line_charges)) {
                File::makeDirectory($shipping_line_charges, 0775, true, true);
            }
            if(isset($filename2)){
                $order->shipping_line_charges = $filename2;
                $request->file('shipping_line_charges')->move($port_charges_path, $filename2);
                $order->save();
            }
        }

        if(isset($port_charges)) {
            if (isset($port_charges)) {
                $extension4 = $request->file('port_charges')->getClientOriginalExtension();
            }
            $sha = 'port_charges' . md5("in".time());
            if(isset($extension4)){
                $filename3 = date('Y').$sha . "ertyui." . $extension4;
            }
            $port_charges_path = 'archives/';
            if (empty($port_charges)) {
                File::makeDirectory($port_charges, 0775, true, true);
            }
            if(isset($filename3)){
                $order->port_charges =  $filename3;
                $request->file('port_charges')->move($port_charges_path, $filename3);
                $order->save();
            }
        }

        if(isset($customer_payments)) {
            if (isset($customer_payments)) {
                $extension5 = $request->file('customer_payments')->getClientOriginalExtension();
            }
            $sha = 'customer_payments' . md5(time());
            if(isset($extension5)){
                $filename6 = date('Y').$sha . "gfgsgf." . $extension5;
            }
            $port_charges_path = 'archives/';
            if (empty($customer_payments)) {
                File::makeDirectory($customer_payments, 0775, true, true);
            }
            if(isset($filename6)){
                $order->customer_payments = $filename6;
                $request->file('customer_payments')->move($port_charges_path, $filename6);
                $order->save();
            }
        }

        if(isset($other)) {
            if (isset($other)) {
                $extension6 = $request->file('other')->getClientOriginalExtension();
            }
            $sha = 'other' . md5(time());
            if(isset($extension6)){
                $filename4 = date('Y').$sha . "gfgsgf." . $extension6;
            }
            $port_charges_path = 'archives/';
            if (empty($other)) {
                File::makeDirectory($other, 0775, true, true);
            }
            if(isset($filename4)){
                $order->other = $filename4;
                $request->file('other')->move($port_charges_path, $filename4);
                $order->save();
            }

        }
        $request->merge(['user_id' => $user_id]);
        $all_data =  $request->all();

        $all_data['invoice_doc']                = !empty($filename1) ? $filename1  : $order->invoice_doc;
        $all_data['shipping_line_charges']      = !empty($filename2) ? $filename2  : $order->shipping_line_charges;
        $all_data['port_charges']               = !empty($filename3) ? $filename3  : $order->port_charges;
        $all_data['customer_payments']          = !empty($filename6) ? $filename6  : $order->customer_payments;
        $all_data['other']                      = !empty($filename4) ? $filename4  : $order->other;

        $order->update($all_data);
        Session::flash('message', 'Document  Uploaded Successful  ');
        return  redirect('/archive_doc');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $training =Archive::findOrfail($id);
        $training->delete();

        Session::flash('delete', 'Documents  Deleted  Successful ');
        return redirect('/archive_doc');
    }
}