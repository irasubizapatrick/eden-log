<?php

namespace App\Http\Controllers;

use App\Expenses;
use App\NormalExpenses;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Auth;
use Session;
use App\User;
use Illuminate\Support\Facades\DB;

class NormalExpensesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user   =Auth::user();
        if($user->hasRole('super'))
        {
            $expenses = Expenses::with('container')->where('finance_status','LIKE','pending')->count();

            $normal = NormalExpenses::where('finance_status','LIKE','pending')->count();


            $ladding = DB::table("laddings")->pluck("bill_lading","id");
            $expenses_data = NormalExpenses::with('staff','user')->latest()->get();
            return view('expenses.super_normal',compact('ladding','expenses','normal','expenses_data'));

        }
        elseif($user->hasRole('finance'))
        {
            $expenses = Expenses::with('container')->where('finance_status','LIKE','pending')->count();

            $normal = NormalExpenses::where('finance_status','LIKE','pending')->count();

            $ladding = DB::table("laddings")->pluck("bill_lading","id");
            $expenses_data = NormalExpenses::with('staff','user')->latest()->get();
            return view('expenses.finance_normal',compact('ladding','expenses','normal','expenses_data'));
        }
        elseif ($user->hasRole('sales'))
        {
            $expenses = Expenses::with('container')->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

            $normal = NormalExpenses::where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

            $ladding = DB::table("laddings")->pluck("bill_lading","id");
            return view('expenses.sales',compact('ladding','expenses','normal'));
        }
        else
        {
            return JsonResponse::create(['error' => 'access-denied'],401);
        }

    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|static
     */
    public function ExpensesSortback(Request $request)
    {
        $expenses = Expenses::with('container')->where('finance_status','LIKE','pending')->count();
        $normal = NormalExpenses::where('finance_status','LIKE','pending')->count();
        $user   =Auth::user();
        $date1 = $request->get('date1');
        $date2 = $request->get('date2');
        if($user->hasRole('finance'))
        {

            $from   =  Carbon::parse($date1)->format('Y-m-d');
            $to     =   Carbon::parse($date2)->format('Y-m-d');
            $service    = $request->get('activity');


            /**
             *
             * If you select From one date  to another Date
             **/

            if(($from  !='') AND  ($to !='') AND ($service !=''))
            {


                $expenses_data = NormalExpenses::with('staff')
                            ->where('expenses_date' ,'>=',$from)
                            ->where('expenses_date' ,'<=',$to)
                            ->where('reason','LIKE',$service)
                            ->latest()->get();

                $ladding = DB::table("laddings")->pluck("bill_lading","id");
                return view('expenses.finance_normal',compact('expenses','ladding','normal','expenses_data'));
            }
            /**
             *
             * If you select From one date only
             **/

            else  if(($from  !=null) AND ($to ==null))
            {


                $expenses_data = NormalExpenses::with('staff')
                    ->where('expenses_date' ,'>=',$from)
                    ->latest()->get();

                $ladding = DB::table("laddings")->pluck("bill_lading","id");
                return view('expenses.finance_normal',compact('expenses','ladding','normal','expenses_data'));
            }

            /**
             *
             * If you select To the last date
             *
             **/

            else   if(($from  =='') AND ($to !=''))
            {


                $expenses_data = NormalExpenses::with('staff')
                    ->where('expenses_date' ,'<=',$to)
                    ->latest()->get();
                $ladding = DB::table("laddings")->pluck("bill_lading","id");
                return view('expenses.finance_normal',compact('expenses','ladding','normal','expenses_data'));
            }
            /**
             *
             * If you don't select anything
             *
             **/

            else
            {

                $ladding = DB::table("laddings")->pluck("bill_lading","id");
                $expenses_data = NormalExpenses::with('staff')->latest()->get();
                return view('expenses.finance_normal',compact('ladding','expenses','normal','expenses_data'));
            }
        }
        else if($user->hasRole('super'))
        {

            $from   =  Carbon::parse($date1)->format('Y-m-d');
            $to     =   Carbon::parse($date2)->format('Y-m-d');
            $service    = $request->get('activity');


            /**
             *
             * If you select From one date  to another Date
             **/

            if(($from  !='') AND  ($to !='') AND ($service !=''))
            {


                $expenses_data = NormalExpenses::with('staff')
                    ->where('expenses_date' ,'>=',$from)
                    ->where('expenses_date' ,'<=',$to)
                    ->where('reason','LIKE',$service)
                    ->latest()->get();

                $ladding = DB::table("laddings")->pluck("bill_lading","id");
                return view('expenses.super_normal',compact('expenses','ladding','normal','expenses_data'));
            }
            /**
             *
             * If you select From one date only
             **/

            else  if(($from  !=null) AND ($to ==null))
            {


                $expenses_data = NormalExpenses::with('staff')
                    ->where('expenses_date' ,'>=',$from)
                    ->latest()->get();

                $ladding = DB::table("laddings")->pluck("bill_lading","id");
                return view('expenses.super_normal',compact('expenses','ladding','normal','expenses_data'));
            }

            /**
             *
             * If you select To the last date
             *
             **/

            else   if(($from  =='') AND ($to !=''))
            {


                $expenses_data = NormalExpenses::with('staff')
                    ->where('expenses_date' ,'<=',$to)
                    ->latest()->get();
                $ladding = DB::table("laddings")->pluck("bill_lading","id");
                return view('expenses.super_normal',compact('expenses','ladding','normal','expenses_data'));
            }
            /**
             *
             * If you don't select anything
             *
             **/

            else
            {

                $ladding = DB::table("laddings")->pluck("bill_lading","id");
                $expenses_data = NormalExpenses::with('staff')->latest()->get();
                return view('expenses.super_normal',compact('ladding','expenses','normal','expenses_data'));
            }
        }
        else
        {
            return JsonResponse::create(['error' => 'access-denied'],401);
        }

    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }


    public function ViewRequestNormal(Request $request ,$id)
    {
        $user   =Auth::user();
        if($user->hasRole('finance') || ($user->hasRole('super')))
        {

            $ladding = DB::table("laddings")->pluck("bill_lading","id");
            $expenses = NormalExpenses::with('staff','user')->where('id','=',$id)->get();

            return view('expenses.normal_request_view',compact('ladding','expenses'));
        }
        else
        {
            return JsonResponse::create(['error' => 'access-denied'],401);
        }
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|static
     */
    public function RequestAmountNormal()
    {
        $user   =Auth::user();
        if($user->hasRole('admin')) {

            $from = '2021';
            $to = '2030';
            $user_id  = Auth::id();
            $expenses_data = NormalExpenses::where('user_id','=',$user_id)->whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->latest()->get();
            $ladding = DB::table("laddings")->whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->pluck("bill_lading","id");
            $expenses = Expenses::with('container')->whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
            $normal = NormalExpenses::whereBetween(DB::raw('YEAR(created_at)'),[$from, $to])->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

            return view('expenses.admin_normal',compact('expenses_data','normal','ladding','expenses'));
        }
        else  if($user->hasRole('super')) {

            $user_id  = Auth::id();
            $expenses_data = NormalExpenses::where('user_id','=',$user_id)->latest()->get();
            $ladding = DB::table("laddings")->pluck("bill_lading","id");

            $expenses = Expenses::with('container')->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
            $normal = NormalExpenses::where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

            return view('expenses.super_normal',compact('expenses_data','normal','ladding','expenses'));
        }
        elseif($user->hasRole('finance')) {

            $user_id  = Auth::id();
            $expenses_data = NormalExpenses::where('user_id','=',$user_id)->latest()->get();
            $ladding = DB::table("laddings")->pluck("bill_lading","id");
            $expenses = Expenses::with('container')->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
            $normal = NormalExpenses::where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

            return view('expenses.finance_normal',compact('expenses_data','normal','ladding','expenses'));
        }

        elseif ($user->hasRole('sales'))
        {
            $user_id  = Auth::id();
            $expenses_data = NormalExpenses::where('user_id','=',$user_id)->latest()->get();
            $ladding = DB::table("laddings")->pluck("bill_lading","id");
            $expenses = Expenses::with('container')->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
            $normal = NormalExpenses::where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

            return view('expenses.normal_sales',compact('expenses_data','normal','ladding','expenses'));
        }
        elseif ($user->hasRole('documentation'))
        {
            $user_id  = Auth::id();
            $expenses_data = NormalExpenses::where('user_id','=',$user_id)->latest()->get();
            $ladding = DB::table("laddings")->pluck("bill_lading","id");
            $expenses = Expenses::with('container')->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
            $normal = NormalExpenses::where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

            return view('expenses.normal_documentation',compact('expenses_data','normal','ladding','expenses'));
        }
        elseif ($user->hasRole('declaration'))
        {
            $user_id  = Auth::id();
            $expenses_data = NormalExpenses::where('user_id','=',$user_id)->latest()->get();
            $ladding = DB::table("laddings")->pluck("bill_lading","id");
            $expenses = Expenses::with('container')->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
            $normal = NormalExpenses::where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

            return view('expenses.declaration_normal',compact('expenses_data','normal','ladding','expenses'));
        }
        elseif ($user->hasRole('shipping'))
        {
            $user_id  = Auth::id();
            $expenses_data = NormalExpenses::where('user_id','=',$user_id)->latest()->get();
            $ladding = DB::table("laddings")->pluck("bill_lading","id");
            $expenses = Expenses::with('container')->where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();
            $normal = NormalExpenses::where('user_id','=',Auth::id())->where('finance_status','LIKE','pending')->count();

            return view('expenses.normal_shipping',compact('expenses_data','normal','ladding','expenses'));
        }
        else
        {
            return JsonResponse::create(['error' => 'access-denied'],401);
        }
    }
    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */

    public function ExpensesFormNormal (Request $request)
    {

        $name   =Auth::user()->name;
        $service    = $request->get('reason');
        $amount     = $request->get('amount_requested');
        $currency   = $request->get('amount_currency');

        $date = $request->get('expenses_date');
        $user_id =Auth::id();
        $request->merge(['staff_id'=> $user_id,'user_id' =>$user_id]);
        $order =  NormalExpenses::create($request->all());
        $order->save();
        if ($order->save());

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.mista.io/sms",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => false,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => array('action' => 'send-sms','to' => '+250786520477','from' => 'MIKUMI','sms' =>  " ".$name." Requested Money ". $amount. " ".$currency ." on " .$date. "  For  ".$service .""),
            CURLOPT_HTTPHEADER => array(
                "x-api-key:Rj13bndBTHh3ZGFNaUVpbUZ1SkI="
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            echo
            Session::flash('message', 'Request   successful sent');
            return back();
        } else {
            echo $response;
        }

        Session::flash('delete', 'Request   successful sent');
        return back();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {


        $user = Auth::user();
        if ($user->hasRole('super') || $user->hasRole('finance')){
            $super_status = $request->get('finance_status');


            if ($super_status === 'voucher closed')
            {
                return JsonResponse::create(['error' => 'voucher has been authorized '], 401);
            }
            elseif ($super_status ===   'canceled by super admin')
            {
                return JsonResponse::create(['error' => 'voucher has been rejected by super admin '], 401);
            }
            else
            {
                $user_id = Auth::id();
                $request->merge(['staff_id' => $user_id]);
                $order = NormalExpenses::findOrFail($id);
                $all_data = $request->all();
                $order->update($all_data);
                Session::flash('updated', ' Updated Successful');
                return redirect('/normal/expenses');
            }


        }
        else
        {
            return JsonResponse::create(['error' => 'access-denied'], 401);
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Exception
     */
    public function destroy($id)
    {

        $user = Auth::user();
        if ($user->hasRole('super') ||  $user->hasRole('finance')) {
            $training = NormalExpenses::findOrfail($id);
            $training->delete();

            Session::flash('delete', ' Deleted Successful');
            return redirect('/normal/expenses');
        }
        else
        {
            return JsonResponse::create(['error' => 'access-denied'], 401);
        }

    }
}
