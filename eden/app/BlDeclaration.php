<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BlDeclaration extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $table = 'bl_declarations';
    protected $fillable = ['id', 'user_id','ladding_id','declaration_id','manifest','t1',
                'whz','t1_verified','exist_note','assessment_doc','c2','authorization_doc',
        'movement_sheet','release','declaration_other','whz_reference','t1_im7_reference',
        'c2_im7_reference','dmc_im7_reference','exist_note_reference','release_reference','c2_im4_reference','dmc_doc'];
    public function lad(){
        return $this->belongsTo('App\Ladding','ladding_id');
    }

    public function declaration(){
        return $this->belongsTo('App\DeclarationType','declaration_id');
    }

}


