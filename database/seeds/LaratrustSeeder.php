<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class LaratrustSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return  void
     */
    public function run()
    {
        $this->command->info('Truncating User, Role and Permission tables');
        $this->truncateLaratrustTables();

        $config = config('laratrust_seeder.role_structure');
        $userPermission = config('laratrust_seeder.permission_structure');
        $mapPermission = collect(config('laratrust_seeder.permissions_map'));

        $Adminrole = \App\Role::create([
            'name' => 'admin',
            'display_name' => 'Admin',
            'description' => 'Admin'
        ]);


        $Clientrole = \App\Role::create([
            'name' => 'client',
            'display_name' => 'Client',
            'description' => 'Client'
        ]);

        $Agentrole = \App\Role::create([
            'name' => 'agent',
            'display_name' => 'Agent',
            'description' => 'Agent'
        ]);

        $SalesManagerrole = \App\Role::create([
            'name' => 'salesManager',
            'display_name' => 'SalesManager',
            'description' => 'SalesManager'
        ]);

        $Salesrole = \App\Role::create([
            'name' => 'sales',
            'display_name' => 'Sales',
            'description' => 'Sales'
        ]);


        $TechnicalManagerole = \App\Role::create([
            'name' => 'technicalManager',
            'display_name' => 'TechnicalManager',
            'description' => 'TechnicalManager'
        ]);

        $Financerole = \App\Role::create([
            'name' => 'finance',
            'display_name' => 'Finance',
            'description' => 'Finance'
        ]);

        $Technicalrole = \App\Role::create([
            'name' => 'technical',
            'display_name' => 'Technical',
            'description' => 'Technical'
        ]);


        $admin = \App\User::create([
            'name' => 'admin',
            'email' => 'admin@rev.rw',
            'password' => bcrypt('password')
        ]);

        $admin->attachRole($Adminrole);

    }

    /**
     * Truncates all the laratrust tables and the users table
     *
     * @return    void
     */
    public function truncateLaratrustTables()
    {
        Schema::disableForeignKeyConstraints();
        DB::table('permission_role')->truncate();
        DB::table('permission_user')->truncate();
        DB::table('role_user')->truncate();
        \App\User::truncate();
        \App\Role::truncate();
        \App\Permission::truncate();
        Schema::enableForeignKeyConstraints();
    }
}
