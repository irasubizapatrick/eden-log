<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Client extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $table = 'clients';
    protected $fillable = ['id', 'created_id','user_id','lead_id','client_name','client_address',
        'client_phone','client_email','package_id','client_id','device_number','tin_number','serial_number','mac_address','deleted_comment','acceptance_doc'];

    protected $with = 'lead';
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function lead()
    {
        return $this->belongsTo('App\Lead', 'lead_id');
    }
    public function user()
    {
        return $this->belongsTo('App\User', 'created_id');
    }
    public function package()
    {
        return $this->belongsTo('App\Package', 'package_id');
    }

}