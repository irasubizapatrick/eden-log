<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Fq extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $table = 'fqs';
    protected $fillable = ['id', 'user_id', 'fq_title','fq_description'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
}