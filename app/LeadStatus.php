<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LeadStatus extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $table = 'lead_statuses';
    protected $fillable = ['id', 'user_id', 'lead_id','status_lead','comment'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function lead()
    {
        return $this->belongsTo('App\Lead', 'lead_id');
    }
    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
}
