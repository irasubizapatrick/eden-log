<?php

namespace App\Http\Controllers;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DB;
use App\Product;
use App\Package;

use Session;
class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user   =Auth::user();

        if($user->hasRole('admin'))
        {
            $product = Product::with('user')->get();
            return view('product.admin_add_product',compact('product'));
        }
        elseif($user->hasRole('salesManager'))
        {
            $product = Product::with('user')->get();
            return view('product.sales_manager_ad_product',compact('product'));
        }
        elseif($user->hasRole('sales'))
        {
            $product = Product::with('user')->get();
            return view('product.sales_add_product',compact('product'));
        }
        else
        {
            return JsonResponse::create(['error' => 'access-denied'],401);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user_id = Auth::id();
        $request->merge(['user_id' => $user_id]);
        $agent =  Product::create($request->all());
        $agent->save();
        Session::flash('message', 'Product created Successful');
        return redirect('/product');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $user_id =Auth::id();
        $request->merge(['user_id' => $user_id]);
        $department = Product::findOrFail($id);
        $data = $request->all();
        $department->update($data);
        Session::flash('updated', 'Product Updated  Successful ');
        return redirect('/product');
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Exception
     */
    public function destroy($id)
    {
        $department  =Product::findOrfail($id);
        $department->delete();
        Session::flash('deleted', 'Product Deleted  Successful ');
        return redirect('/product');
    }
}
