<?php

namespace App\Http\Controllers;

use App\Department;
use App\Role;
use App\Staff;
use App\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Session;
use DB;
use Hash;
class StaffController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   $user   =Auth::user();
        if($user->hasRole('admin')) {
            $department = Department::all();
            $staff = User::select('users.*', 'display_name','full_name','telephone','staff_email','department_name')
                        ->join('staff','staff.user_id','=','users.id')
                        ->join('role_user', 'role_user.user_id', '=', 'users.id')
                        ->join('roles', 'roles.id', '=', 'role_user.role_id')
                        ->join('departments','departments.id','=','staff.department_id')
                        ->get();

            return view('staff.list_staff', compact('staff', 'department', 'users'));
        }

        else
        {
            return JsonResponse::create(['error' => 'access-denied'],401);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user_id    =   Auth::id();
        $email          =   $request->get('email');
        $full_name      =   $request->get('name');
        $role       =   Role::where('display_name', $request['role'])->first();
        $password   =  Hash::Make($request->get("password"));
        $request->merge(['user_id' => $user_id, 'password' =>$password]);
        $user      =  User::create($request->all());

        $user->attachRole($role);
        if($user->save())
        $request->merge(['user_id' => $user->id, 'staff_email' =>$email,'full_name'=> $full_name]);
        $company = Staff::create($request->all());
        $company->save();
        Session::flash('message', 'Staff Member Created successfully');
        return redirect("/staff");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user_id = Auth::id();
        $request->merge(['user_id' => $user_id]);
        $order = User::findOrFail($id);
        $password   =  Hash::Make($request->get("password"));
        $request->merge(['user_id' => $user_id, 'password' =>$password]);
        $data = $request->all();
        $order->update($data);
        Session::flash('updated', 'Staff Updated successfully');
        return redirect("/staff");
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Exception
     */
    public function destroy($id)
    {
        $staff = User::findOrfail($id);
        $staff->delete();
        Session::flash('deleted', 'Staff Deleted successfully');
        return redirect("/staff");
    }
}
