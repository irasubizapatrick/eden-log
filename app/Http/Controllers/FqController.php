<?php

namespace App\Http\Controllers;

use App\Fq;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Auth;
use Session;
use DB;


class FqController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {  $user = Auth::user();
        if ($user->hasRole('admin')) {
            $fq = Fq::all();
            return view('cms.fq',compact('fq'));
        }
        else
        {
            return JsonResponse::create(['error' => 'access-denied'],401);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return JsonResponse
     */
    public function store(Request $request)
    {
        $user   =Auth::user();
        if($user->hasRole('admin') || $user->hasRole('sales'))
        {
            $user_id = Auth::id();

            $request->merge(['user_id' => $user_id]);
            $all_data = Fq::create($request->all());
            $all_data->save();
            Session::flash('message', 'Fq us  Added  Successful ');
            return redirect('/fq');
        }
        else
        {
            return JsonResponse::create(['error' => 'access-denied'],401);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * @param $id
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user   =Auth::user();
        if($user->hasRole('admin'))
        {


            $user_id = Auth::id();
            $request->merge(['user_id' => $user_id]);
            $order = Fq::findOrFail($id);
            $data = $request->all();
            $order->update($data);
            Session::flash('message', 'Faq Updated  Successful ');
            return redirect('/fq');
        }
        else
        {
            return JsonResponse::create(['error' => 'access-denied'],401);
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Exception
     *
     */
    public function destroy($id)
    {
        $user   =Auth::user();
        if($user->hasRole('admin') || $user->hasRole('sales'))
        {
            $area   =Fq::findOrfail($id);
            $area->delete();
            Session::flash('message', 'Faq Deleted  Successful ');
            return redirect('/fq');
        }
        else
        {
            return JsonResponse::create(['error' => 'access-denied'],401);
        }
    }

}
