<?php

namespace App\Http\Controllers;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DB;
use App\Product;
use App\Package;

use Session;
class PackageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user   =Auth::user();

        if($user->hasRole('admin'))
        {
            $product = Product::all();
            $package = Package::with('product','user')->get();
            return view('package.admin_add_package',compact('package','product'));
        }
        elseif($user->hasRole('salesManager'))
        {
            $product = Product::all();
            $package = Package::with('product','user')->get();
            return view('package.sales_manager_add_package',compact('package','product'));
        }
        else
        {
            return JsonResponse::create(['error' => 'access-denied'],401);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user_id = Auth::id();
        $request->merge(['user_id' => $user_id]);
        $agent =  Package::create($request->all());
        $agent->save();
        Session::flash('message', 'Package created Successful');
        return redirect('/package');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $user_id =Auth::id();
        $request->merge(['user_id' => $user_id]);
        $department = Package::findOrFail($id);
        $data = $request->all();
        $department->update($data);
        Session::flash('updated', 'Package Updated  Successful ');
        return redirect('/package');
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Exception
     */
    public function destroy($id)
    {
        $department  =Package::findOrfail($id);
        $department->delete();
        Session::flash('deleted', 'Package Deleted  Successful ');
        return redirect('/package');
    }
}
