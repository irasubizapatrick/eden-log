<?php

namespace App\Http\Controllers;

use App\Agent;
use App\Department;
use App\Role;
use App\Staff;
use App\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Session;
use DB;
use Hash;
class AgentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user   =Auth::user();
        if($user->hasRole('salesManager'))
        {
        $department = Department::all();
        $staff = User::select('users.*', 'display_name','agent_name','agent_telephone','agent_email','address')
                        ->join('agents','agents.user_id','=','users.id')
                        ->join('role_user', 'role_user.user_id', '=', 'users.id')
                        ->join('roles', 'roles.id', '=', 'role_user.role_id')
                        ->get();
        return view('agent.list_agent',compact('staff','department','users'));
        }
        elseif ($user->hasRole('admin'))
        {
            $department = Department::all();
            $staff = User::select('users.*', 'display_name','agent_name','agent_telephone','agent_email','address')
                        ->join('agents','agents.user_id','=','users.id')
                        ->join('role_user', 'role_user.user_id', '=', 'users.id')
                        ->join('roles', 'roles.id', '=', 'role_user.role_id')
                        ->get();

//            return response()->json($staff);
            return view('agent.admin_list_agent',compact('staff','department','users'));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user   =Auth::user();
        if($user->hasRole('admin') || ($user->hasRole('salesManager')))
        {

            $email = $request->get('email');
            $full_name = $request->get('name');
            $role = Role::where('display_name', $request['role'])->first();
            $password = Hash::Make($request->get("password"));
            $request->merge(['password' => $password]);
            $user = User::create($request->all());

            $user->attachRole($role);
            $user_id = $user->id;
            $created_id = Auth::id();
            if ($user->save())
            $request->merge(['user_id' => $user_id, 'created_id' =>$created_id,'agent_email' => $email, 'agent_name' => $full_name]);
            $company = Agent::create($request->all());
            $company->save();
            Session::flash('message', 'agent  Created successfully');
            return redirect("/agent");
        }
        else
        {
            return JsonResponse::create(['error' => 'access-denied'],401);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $user_id = Auth::id();
        $request->merge(['user_id' => $user_id]);
        $order = User::findOrFail($id);
        $password   =  Hash::Make($request->get("password"));
        $request->merge(['user_id' => $user_id, 'password' =>$password]);
        $data = $request->all();
        $order->update($data);
        Session::flash('updated', 'Agent Updated successfully');
        return redirect("/agent");
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Exception
     */
    public function destroy($id)
    {
        $staff = Agent::findOrfail($id);
        $staff->delete();
        Session::flash('delete', 'Agent Deleted successfully');
        return redirect("/agent");
    }
}
