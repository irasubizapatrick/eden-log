<?php

namespace App\Http\Controllers;

use App\Ticket;
use App\TicketStatus;
use Illuminate\Http\Request;

use App\Client;
use App\Lead;
use App\LeadStatus;
use App\Package;
use App\Product;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use Session;


class TicketController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user   =Auth::user();
        if($user->hasRole('admin'))
        {

            $ticket = Ticket::with('user')->get();
            return view('ticket.admin_ticket_list',compact('ticket'));
        }
        elseif ($user->hasRole('salesManager'))
        {
            $ticket = Ticket::with('user')->get();
            return view('ticket.sales_manager_ticket',compact('ticket'));
        }
        elseif ($user->hasRole('sales'))
        {
            $ticket = Ticket::with('user')->get();
            return view('ticket.sales_ticket',compact('ticket'));
        }

        elseif ($user->hasRole('technicalManager'))
        {
            $ticket = Ticket::with('user')->get();
            return view('ticket.technical_ticket',compact('ticket'));
        }
        elseif ($user->hasRole('technical'))
        {
            $ticket = Ticket::with('user')->get();
            return view('ticket.technical_ticket',compact('ticket'));
        }

        elseif ($user->hasRole('client'))
        {
            $user_id    =Auth::id();
            $ticket = Ticket::with('user')->where('tickets.user_id','=',$user_id)->get();
            return view('ticket.client_ticket',compact('ticket'));
        }
        else
        {
            return JsonResponse::create(['error' => 'access-denied'],401);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }


    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|static
     */
    public function ViewConversation($id)
    {
        $user   =Auth::user();
        if($user->hasRole('admin'))
        {

            $ticket = TicketStatus::with('user','tick')->where('ticket_id','=',$id)->get();
            return view('ticket.admin_view_client_ticket',compact('ticket'));
        }
        elseif ($user->hasRole('salesManager'))
        {
            $ticket = TicketStatus::with('user','tick')->where('ticket_id','=',$id)->get();
            return view('ticket.sales_view_client_ticket',compact('ticket'));
        }


        elseif ($user->hasRole('technicalManager'))
        {

            $ticket = TicketStatus::with('user','tick')->where('ticket_id','=',$id)->get();
            return view('ticket.technical_view_ticket',compact('ticket'));

        }
        elseif ($user->hasRole('client'))
        {
            $ticket = TicketStatus::with('user','tick')->where('ticket_id','=',$id)->get();
            return view('ticket.view_client_ticket',compact('ticket'));
        }
        else
        {
            return JsonResponse::create(['error' => 'access-denied'],401);
        }
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */


    public function add_Answer(Request $request){
        $user_id = Auth::id();
        $ticket_id =$request->get('ticket_id');
        $request->merge(['user_id' => $user_id,'ticket_id' =>$ticket_id]);
        $agent =  TicketStatus::create($request->all());
        $agent->save();
        Session::flash('message', ' Successful');
        return back();
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $user_id    =Auth::id();
        $get_client_id = Client::where('user_id','=',$user_id)->first()->id;


        $request->merge(['user_id' => $user_id,'client_id'=>$get_client_id]);
        $ticket =  Ticket::create($request->all());
        if ($ticket->save());
        $request->merge(['user_id' => $user_id,'ticket_id' =>$ticket->id]);
        $agent =  TicketStatus::create($request->all());
        $agent->save();
        Session::flash('message', 'Ticket created Successful');
        return redirect('/manage/ticket');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $status  ='closed';
        $request->merge(['ticket_status' => $status]);
        $department = Ticket::findOrFail($id);
        $data = $request->all();
        $department->update($data);
        Session::flash('message', 'Ticket Closed  Successful ');
        return redirect('/manage/ticket');
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Exception
     */
    public function destroy($id)
    {
        $department  =Ticket::findOrfail($id);
        $department->delete();
        Session::flash('message', 'Ticket Deleted  Successful ');
        return redirect('/manage/ticket');
    }
}
