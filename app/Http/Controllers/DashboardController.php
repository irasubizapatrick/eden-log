<?php

namespace App\Http\Controllers;

use App\Client;
use App\Lead;
use Illuminate\Http\Request;
use Auth;

class DashboardController extends Controller
{
    /**
     *  Dasboard Redirections
     */
    public function index()
    {
      $user_email   = Auth::user()->email;
        $user   =Auth::user();
        if($user->hasRole('admin'))
        {
            $count_lead = Lead::where('lead_status','LIKE','pending')->count();
            $leads = Lead::with('user')->latest()->get();

            return view('dashboard.admin',compact('leads','count_lead'));
        }
        elseif ($user->hasRole('salesManager'))
        {
            $count_lead = Lead::where('lead_status','LIKE','pending')->count();
            $leads = Lead::with('user')->latest()->get();
            return view('dashboard.sales_manager',compact('leads','count_lead'));
        }

        elseif ($user->hasRole('agent'))
        {

            $user_id    = Auth::id();
            $count_lead = Lead::where('user_id','=',$user_id)->count();
            $leads = Lead::with('user')->latest()->where('user_id','=',$user_id)->get();
            return view('dashboard.agent',compact('leads','count_lead'));

        }

        elseif ($user->hasRole('technicalManager'))
        {
            $count_lead = Lead::where('lead_status','LIKE','Site Survey Request')->count();
            $leads = Lead::with('user')->latest()->where('lead_status','NOT LIKE','pending')->get();
            return view('dashboard.technical_manager',compact('leads','count_lead'));
        }
        elseif ($user->hasRole('technical'))
        {
            $count_lead = Lead::where('lead_status','LIKE','Site Survey Request')->count();
            $leads = Lead::with('user')->latest()->where('lead_status','NOT LIKE','pending')->get();
            return view('dashboard.technical',compact('leads','count_lead'));
        }


        elseif ($user->hasRole('client'))
        {
            $user_id = Auth::id();
            $count_lead = Lead::where('lead_status','LIKE','Site Survey Request')->count();
            $leads = Lead::with('user')->latest()->where('lead_status','NOT LIKE','pending')->get();
            $client =Client::with('package', 'user')->where('clients.user_id','=',$user_id)->get();
            return view('dashboard.client',compact('leads','count_lead','client'));
        }
        elseif ($user->hasRole('sales'))
        {
            $count_lead = Lead::where('lead_status','LIKE','pending')->count();
            $leads = Lead::with('user')->latest()->get();
            return view('dashboard.sales',compact('leads','count_lead'));
        }


        else
        {
            return 'no role';
        }


    }
}
