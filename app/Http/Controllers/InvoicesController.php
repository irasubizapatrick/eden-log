<?php

namespace App\Http\Controllers;

use App\Client;
use App\Contract;
use App\Invoice;
use App\Lead;
use App\LeadStatus;
use App\Package;
use App\Product;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Session;

class InvoicesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user   =Auth::user();

        if($user->hasRole('admin')) {
            $client = Client::all();
            $contract = Invoice::with('user','client')->latest()->get();
            return view('client.admin_list_invoice', compact('contract','client'));
        }
        elseif($user->hasRole('salesManager')) {
            $client = Client::all();
            $contract = Invoice::with('user','client')->latest()->get();
            return view('client.sales_manager_list_invoice', compact('contract','client'));
        } elseif($user->hasRole('sales')) {
            $client = Client::all();
            $contract = Invoice::with('user','client')->latest()->get();
            return view('client.sales_list_invoice', compact('contract','client'));
        }
        else
        {
            return JsonResponse::create(['error' => 'access-denied'],401);
        }
    }


    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|static
     */
    public function my_invoice()
    {
        $user   =Auth::user();

        if($user->hasRole('admin')) {
            $user_id = Auth::id();
            $client = Invoice::select('invoices.*', 'client_name')
                ->join('clients', 'clients.id', '=', 'invoices.client_id')
                ->where('clients.user_id', '=', $user_id)
                ->get();
            return view('client.my_invoice', compact('client'));
        }
        else
        {
            return JsonResponse::create(['error' => 'access-denied'],401);
        }
    }
    /**
     *
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user   =Auth::user();

        if($user->hasRole('admin') || $user->hasRole('salesManager') || $user->hasRole('sales')) {


            $user_id = Auth::id();
            $request->merge(['user_id' => $user_id]);
            $order = Invoice::create($request->all());
            $contract_file = $request->file('invoice_doc');

            if (isset($contract_file)) {
                if (isset($contract_file)) {
                    $extension1 = $request->file('invoice_doc')->getClientOriginalExtension();
                }
                $sha = 'invoice_doc' . md5(time());
                if (isset($extension1)) {
                    $filename1 = date('Y') . $sha . "sgfs." . $extension1;
                }
                $destination_path = 'invoice_file/';
                if (empty($contract_file)) {
                    File::makeDirectory($contract_file, 0775, true, true);
                    // File::makeDirectory($destination_path, $mode = 0777, true, true);
                }
                if (isset($filename1)) {
                    $order->invoice_doc = $filename1;
                    $request->file('invoice_doc')->move($destination_path, $filename1);
                    $order->save();
                }
            }
            $all_data = $request->all();
            $all_data['invoice_doc'] = !empty($filename1) ? $filename1 : $order->invoice_doc;
            $order->save($all_data);
            Session::flash('message', 'Invoice  Created successfully');
            return back();
        }
        else
        {
            return JsonResponse::create(['error' => 'access-denied'],401);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user   =Auth::user();

        if($user->hasRole('admin') || $user->hasRole('salesManager') || $user->hasRole('sales')) {

        $contract_doc = $request->file('invoice_doc');
        $order = Invoice::findOrFail($id);
        if (isset($contract_doc)) {
            if (isset($contract_doc)) {
                $extension1 = $request->file('invoice_doc')->getClientOriginalExtension();
            }
            $sha = 'contract_doc' . md5(time());
            if (isset($extension1)) {
                $filename1 = date('Y') . $sha . "sgfs." . $extension1;
            }
            $destination_path = 'invoice_file/';
            if (empty($destination_path)) {
                File::makeDirectory($destination_path, 0775, true, true);
                // File::makeDirectory($destination_path, $mode = 0777, true, true);
            }
            if (isset($filename1)) {
                $order->invoice_doc = $destination_path . $filename1;
                $request->file('invoice_doc')->move($destination_path, $filename1);
                $order->save();
            }

        }
        $data = $request->all();
        $data['invoice_doc'] = !empty($filename1) ? $filename1 : $order->invoice_doc;
        $order->update($data);
        Session::flash('message', 'Invoice Updated Successful');
        return back();
    }
    else
        {
            return JsonResponse::create(['error' => 'access-denied'],401);
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy($id)
    {
        $user   =Auth::user();

        if($user->hasRole('admin') || $user->hasRole('salesManager') || $user->hasRole('sales')) {
        $staff = Invoice::findOrfail($id);
        $staff->delete();
        Session::flash('message', 'Contract Deleted Successful');
        return back();
        }
        else
        {
            return JsonResponse::create(['error' => 'access-denied'],401);
        }
    }
}
