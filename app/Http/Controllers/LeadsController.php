<?php

namespace App\Http\Controllers;

use App\Client;
use App\Lead;
use App\LeadStatus;
use App\Package;
use App\Product;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Session;

class LeadsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user   =Auth::user();

        if($user->hasRole('admin')) {
            $leads = Lead::with('user')->latest()->get();
            return view('leads.admin_list_leads', compact('leads'));
        }
        elseif($user->hasRole('salesManager')) {
            $leads = Lead::with('user')->latest()->get();
            return view('leads.sales_manager_list_leads', compact('leads'));
        }
        elseif($user->hasRole('agent')) {
            $user_id    =Auth::id();
            $leads      = Lead::with('user')->latest()->where('user_id','=',$user_id)->get();
            return view('leads.agent_list_leads', compact('leads'));
        }
        elseif($user->hasRole('sales')) {
            $leads = Lead::with('user')->latest()->get();
            return view('leads.sales_leads', compact('leads'));
        }
        else
        {
            return JsonResponse::create(['error' => 'access-denied'],401);
        }
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     *
     */
    public function customerReg()
    {
        return view('website_lead_form');

    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function leadStatus($id)
    {
        $user   =Auth::user();

        if($user->hasRole('admin'))
        {
            $leads_status = LeadStatus::with('user','lead')->where('lead_statuses.lead_id','=',$id)->get();
            return view('leads.admin_list_leads_status',compact('leads_status'));
        }
        elseif($user->hasRole('salesManager'))
        {
            $leads_status = LeadStatus::with('user','lead')->where('lead_statuses.lead_id','=',$id)->get();
            return view('leads.sales_manager_leads_status',compact('leads_status'));
        }
        elseif($user->hasRole('sales'))
        {
            $leads_status = LeadStatus::with('user','lead')->where('lead_statuses.lead_id','=',$id)->get();
            return view('leads.sales_leads_status',compact('leads_status'));
        }
        elseif($user->hasRole('technicalManager'))
        {
            $leads_status = LeadStatus::with('user','lead')->where('lead_statuses.lead_id','=',$id)->get();
            return view('leads.technical_manager_leads_status',compact('leads_status'));
        }
        elseif($user->hasRole('agent'))
        {
            $leads_status = LeadStatus::with('user','lead')->where('lead_statuses.lead_id','=',$id)->get();
            return view('leads.agent_list_leads_status',compact('leads_status'));
        }
        else
        {
            return JsonResponse::create(['error' => 'access-denied'],401);
        }
    }

    /***
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */

     public function lead_reg( Request $request)
    {
        $user_id = 1;
        $lead_status = 'pending';
        $request->merge(['lead_status' => $lead_status ,'user_id' => $user_id]);
        $agent =  Lead::create($request->all());
        if($agent->save());
        $request->merge(['user_id' => $user_id , 'lead_id' =>$agent->id, ' status_lead' =>$lead_status]);
        $lead_status =  LeadStatus::create($request->all());
        $lead_status->save();
        Session::flash('message', 'Site Survey Request Sent Successful');
        return back();

    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|static
     */
    public function site_SUrvey()
    {
        $user   =Auth::user();

        if($user->hasRole('technicalManager') || ($user->hasRole('technical')))
        {
            $leads = Lead::with('user')->latest()->where('lead_status','NOT LIKE','pending')->get();
            return view('leads.technical_manager_list_leads', compact('leads'));
        }
        else
        {
            return JsonResponse::create(['error' => 'access-denied'],401);
        }
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|static
     */
    public function createAccount(Request $request, $id)
    {
        $user   =Auth::user();

        if($user->hasRole('admin'))
        {
            $product = Product::all();
            $package = Package::with('product','user')->get();
            $leads_status = Lead::with('user')->where('leads.id','=',$id)->get();
            return view('leads.admin_create_account',compact('leads_status','product','package'));
        }
        elseif($user->hasRole('salesManager'))
        {

            $product = Product::all();
            $package = Package::with('product','user')->get();
            $leads_status = Lead::with('user')->where('leads.id','=',$id)->get();
            return view('leads.sales_manager_create_account',compact('leads_status','package','product'));
        }
        elseif($user->hasRole('sales'))
        {

            $product = Product::all();
            $package = Package::with('product','user')->get();
            $leads_status = Lead::with('user')->where('leads.id','=',$id)->get();
            return view('leads.sales_create_account',compact('leads_status','package','product'));
        }
        else
        {
            return JsonResponse::create(['error' => 'access-denied'],401);
        }

    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user   =Auth::user();

        if($user->hasRole('admin') || $user->hasRole('salesManager') || $user->hasRole('sales')) {

            $user_id = Auth::id();
            $lead_status = 'pending';
            $request->merge(['lead_status' => $lead_status, 'user_id' => $user_id]);
            $agent = Lead::create($request->all());
            if ($agent->save()) ;
            $request->merge(['user_id' => $user_id, 'lead_id' => $agent->id, ' status_lead' => $lead_status]);
            $lead_status = LeadStatus::create($request->all());
            $lead_status->save();
            Session::flash('message', 'Lead Created Successful');
            return back();
        }
        else
        {
            return JsonResponse::create(['error' => 'access-denied'],401);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user   =Auth::user();

        if($user->hasRole('admin') || $user->hasRole('salesManager') || $user->hasRole('sales')|| $user->hasRole('technical') || $user->hasRole('finance'))  {

        $comment = $request->get('comment');
        $status = $request->get('lead_status');
        $acceptance_doc  = $request->file('acceptance_doc');

        $order = Lead::findOrFail($id);
        if(isset($acceptance_doc)) {
            if (isset($acceptance_doc)) {
                $extension1 = $request->file('acceptance_doc')->getClientOriginalExtension();
            }
            $sha = 'acceptance_doc' . md5(time());
            if(isset($extension1)){
                $filename1 = date('Y').$sha . "sgfs." . $extension1;
            }
            $destination_path = 'accept_letter/';
            if (empty($destination_path)) {
                File::makeDirectory($destination_path, 0775, true, true);
                // File::makeDirectory($destination_path, $mode = 0777, true, true);
            }
            if(isset($filename1)){
                $order->acceptance_doc = $destination_path . $filename1;
                $request->file('acceptance_doc')->move($destination_path, $filename1);
                $order->save();
            }

        }
        $data = $request->all();
        $data['acceptance_doc']   = !empty($filename1) ? $filename1                : $order->acceptance_doc;
        if($order->update($data));
        $user_id = Auth::id();
        $request->merge(['lead_id' =>$order->id, 'status_lead' =>$status ,'comment' =>$comment,'user_id'=>$user_id]);
        $lead_status =  LeadStatus::create($request->all());
        $lead_status->save();
        Session::flash('message', 'Installation Created Successful');
        return back();
        }
        else
        {
            return JsonResponse::create(['error' => 'access-denied'],401);
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy($id)
    {
        $user   =Auth::user();

        if($user->hasRole('admin') || $user->hasRole('salesManager') || $user->hasRole('sales')) {
        $staff = Lead::findOrfail($id);
        $staff->delete();
        Session::flash('message', 'Lead Created Successful');
        return back();
    }
    else
        {
        return JsonResponse::create(['error' => 'access-denied'],401);
        }
    }
}
