<?php

namespace App\Http\Controllers;

use App\Fq;
use App\Team;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Auth;
use Session;
use DB;


class TeamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {  $user = Auth::user();
        if ($user->hasRole('admin')) {
            $team = Team::all();
            return view('cms.team',compact('team'));
        }
        else
        {
            return JsonResponse::create(['error' => 'access-denied'],401);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return JsonResponse
     */
    public function store(Request $request)
    {
        $user   =Auth::user();
        if($user->hasRole('admin'))
        {
            $user_id = Auth::id();
            $contract_doc   = $request->file('team_photo');
            $request->merge(['user_id' => $user_id]);
            $agent =  Team::create($request->all());
            if(isset($contract_doc)) {
                if (isset($contract_doc)) {
                    $extension1 = $request->file('team_photo')->getClientOriginalExtension();
                }
                $sha = 'team_photo' . md5(time());
                if(isset($extension1)){
                    $filename1 = date('Y').$sha . "sgfs." . $extension1;
                }
                $destination_path = 'team_photo/';
                if (empty($destination_path)) {
                    File::makeDirectory($destination_path, 0775, true, true);
                    // File::makeDirectory($destination_path, $mode = 0777, true, true);
                }
                if(isset($filename1)){
                    $agent->team_photo = $destination_path . $filename1;
                    $request->file('team_photo')->move($destination_path, $filename1);
                    $agent->save();
                }
                $all_data =  $request->all();
                $all_data['team_photo']   = !empty($filename1) ? $filename1   : $agent->team_photo;
            }
            $agent->save();
            Session::flash('message', 'Team member  Added  Successful ');
            return redirect('/manage_team');
        }
        else
        {
            return JsonResponse::create(['error' => 'access-denied'],401);
        }


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * @param $id
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user   =Auth::user();
        if($user->hasRole('admin'))
        {
            $contract_doc  = $request->file('team_photo');

            $order = Team::findOrFail($id);
            if(isset($contract_doc)) {
                if (isset($contract_doc)) {
                    $extension1 = $request->file('team_photo')->getClientOriginalExtension();
                }
                $sha = 'team_photo' . md5(time());
                if(isset($extension1)){
                    $filename1 = date('Y').$sha . "sgfs." . $extension1;
                }
                $destination_path = 'team_photo/';
                if (empty($destination_path)) {
                    File::makeDirectory($destination_path, 0775, true, true);
                    // File::makeDirectory($destination_path, $mode = 0777, true, true);
                }
                if(isset($filename1)){
                    $order->team_photo = $destination_path . $filename1;
                    $request->file('team_photo')->move($destination_path, $filename1);
                    $order->save();
                }

            }
            $data = $request->all();
            $data['team_photo']   = !empty($filename1) ? $filename1                : $order->team_photo;
            $order->update($data);
            Session::flash('message', 'Team member Updated  Successful ');
            return redirect('/manage_team');

        }
        else
        {
            return JsonResponse::create(['error' => 'access-denied'],401);
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Exception
     *
     */
    public function destroy($id)
    {
        $user   =Auth::user();
        if($user->hasRole('admin'))
        {
            $area   =Team::findOrfail($id);
            $area->delete();
            Session::flash('message', 'Team member Deleted  Successful ');
            return redirect('/manage_team');
        }
        else
        {
            return JsonResponse::create(['error' => 'access-denied'],401);
        }
    }

}
