<?php

namespace App\Http\Controllers;

use App\Client;
use App\Lead;
use App\LeadStatus;
use App\Package;
use App\Product;
use App\Role;
use App\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Hash;
use Illuminate\Support\Facades\Auth;
use Session;

class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $user   =Auth::user();

        if($user->hasRole('admin')) {
            $product = Product::all();
            $package = Package::with('product', 'user')->get();
            $client = Client::with('package', 'user')->get();
            return view('client.admin_list_client', compact('client', 'package', 'product'));

        }
        elseif($user->hasRole('salesManager'))
        {
            $product = Product::all();
            $package = Package::with('product', 'user')->get();
            $client = Client::with('package', 'user')->get();
            return view('client.sales_manager_list_client', compact('client', 'package', 'product'));

        }
        elseif($user->hasRole('sales'))
        {
            $product = Product::all();
            $package = Package::with('product', 'user')->get();
            $client = Client::with('package', 'user')->get();
            return view('client.sales_list_client', compact('client', 'package', 'product'));

        }
        else
        {
            return JsonResponse::create(['error' => 'access-denied'],401);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function moreCustomerDetails($id)
    {
        $user   =Auth::user();

        if($user->hasRole('admin'))
        {
            $client = Client::with('package', 'user')->where('clients.id','=',$id)->get();
            return view('leads.admin_customer_more_details',compact('client'));
        }
        elseif($user->hasRole('salesManager'))
        {
            $client = Client::with('package', 'user')->where('clients.id','=',$id)->get();
            return view('leads.customer_more_details',compact('client'));
        }
        elseif($user->hasRole('sales'))
        {
            $client = Client::with('package', 'user')->where('clients.id','=',$id)->get();
            return view('leads.sales_customer_more_details',compact('client'));
        }
        else
        {
            return JsonResponse::create(['error' => 'access-denied'],401);
        }
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function save_customer(Request $request)
    {

        $user   =Auth::user();

        if($user->hasRole('admin') || $user->hasRole('salesManager') || $user->hasRole('sales'))  {

        $user_id    =   Auth::id();
        $lead_email          =   $request->get('client_email');
        $role       =  Role::where('display_name', $request['role'])->first();
        $password   =  Hash::Make($request->get("password"));
        $lead_id    = $request->get('lead_id');
        $lead_status = 'client';
        $request->merge(['email' => $lead_email, 'password' =>$password]);
        $user      =  User::create($request->all());
        if($user->save())
        $user->attachRole($role);

        $job = Lead::find($lead_id);
        $request->merge(['lead_status' => $lead_status ]);
        $job_data = $request->all();
        if ($job->update($job_data));

        $request->merge(['created_id' => $user_id,'user_id'=>$user->id]);
        $company = Client::create($request->all());
        $company->save();

        Session::flash('message', 'Client  Created successfully');
        return redirect("/manage/all/client");
        }
    else
        {
            return JsonResponse::create(['error' => 'access-denied'],401);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user   =Auth::user();

        if($user->hasRole('admin') || $user->hasRole('salesManager') || $user->hasRole('sales')) {
            $user_id =Auth::id();
            $request->merge(['created_id' => $user_id]);
            $department = Client::findOrFail($id);
            $data = $request->all();
            $department->update($data);
            Session::flash('updated', 'Client  Updated successfully');
            return redirect('/manage/all/client');
        }
        else
        {
            return JsonResponse::create(['error' => 'access-denied'],401);
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Exception
     */
    public function destroy($id)
    {
        $user   =Auth::user();

        if($user->hasRole('admin') || $user->hasRole('salesManager') || $user->hasRole('sales')) {
            $department  =Client::findOrfail($id);
            $department->delete();
            Session::flash('deleted', 'Client  Deleted successfully');
            return redirect("/manage/all/client");
        }
        else
        {
            return JsonResponse::create(['error' => 'access-denied'],401);
        }
    }
}
