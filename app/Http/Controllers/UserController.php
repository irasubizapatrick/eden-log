<?php

namespace App\Http\Controllers;

use App\User;
use Hash;
use Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     *
     */
    public function  change_pass()
    {
        $user   =Auth::user();
        if($user->hasRole('admin')) {

            $user_id    =Auth::id();
            $password   =User::select('users.*')->where('id','=',$user_id)->get();
            return view('dashboard.change_pass',compact('password'));
        }
        elseif($user->hasRole('salesManager'))
        {
            $user_id    =Auth::id();
            $password   =User::select('users.*')->where('id','=',$user_id)->get();
            return view('dashboard.sales_manager_change_pass',compact('password'));
        }
        elseif($user->hasRole('sales'))
        {
            $user_id    =Auth::id();
            $password   =User::select('users.*')->where('id','=',$user_id)->get();
            return view('dashboard.sales_change_pass',compact('password'));
        }
        elseif($user->hasRole('agent'))
        {
            $user_id    =Auth::id();
            $password   =User::select('users.*')->where('id','=',$user_id)->get();
            return view('dashboard.agent_change_pass',compact('password'));
        }
        elseif($user->hasRole('technicalManager') || ($user->hasRole('technical')))
        {
            $user_id    =Auth::id();
            $password   =User::select('users.*')->where('id','=',$user_id)->get();
            return view('dashboard.technical_manager_change_pass',compact('password'));
        }
        elseif($user->hasRole('client'))
        {
            $user_id    =Auth::id();
            $password   =User::select('users.*')->where('id','=',$user_id)->get();
            return view('dashboard.client_change_pass',compact('password'));
        }
        else
        {
            return JsonResponse::create(['error' => 'access-denied'],401);
        }
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update_password(Request $request, $id)
    {
        $user                         = User::find($id);
        $user->email                  = $request->get('email');
        $user->password               = Hash::make($request->get('password'));
        $user->update();
        Session::flash('updated', 'Password  Changed successfully');
        return redirect('/logout');
    }
}
