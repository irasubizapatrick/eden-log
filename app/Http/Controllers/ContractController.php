<?php

namespace App\Http\Controllers;

use App\Client;
use App\Contract;
use App\Lead;
use App\LeadStatus;
use App\Package;
use App\Product;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Session;

class ContractController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user   =Auth::user();

        if($user->hasRole('admin')) {
            $client = Client::all();
            $contract = Contract::with('user','client')->latest()->get();
            return view('client.admin_list_contract', compact('contract','client'));
        }
        elseif($user->hasRole('salesManager')) {
            $client = Client::all();
            $contract = Contract::with('user','client')->latest()->get();
            return view('client.sales_manager_list_contract', compact('contract','client'));
        }
        elseif($user->hasRole('sales')) {
            $client = Client::all();
            $contract = Contract::with('user','client')->latest()->get();
            return view('client.sales_list_contract', compact('contract','client'));
        }
        else
        {
            return JsonResponse::create(['error' => 'access-denied'],401);
        }
    }


    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $user   =Auth::user();

        if($user->hasRole('admin') || $user->hasRole('salesManager') || $user->hasRole('sales'))  {

        $user_id = Auth::id();
        $request->merge(['user_id' => $user_id]);
        $order =  Contract::create($request->all());
        $contract_file   = $request->file('contract_doc');

        if(isset($contract_file)) {
            if (isset($contract_file)) {
                $extension1 = $request->file('contract_doc')->getClientOriginalExtension();
            }
            $sha = 'contract_doc' . md5(time());
            if(isset($extension1)){
                $filename1 = date('Y').$sha . "sgfs." . $extension1;
            }
            $destination_path = 'contracts_file/';
            if (empty($contract_file)) {
                File::makeDirectory($contract_file, 0775, true, true);
                // File::makeDirectory($destination_path, $mode = 0777, true, true);
            }
            if(isset($filename1)){
                $order->contract_doc =  $filename1;
                $request->file('contract_doc')->move($destination_path, $filename1);
                $order->save();
            }
        }
        $all_data =  $request->all();
        $all_data['contract_doc']                = !empty($filename1) ? $filename1                : $order->contract_doc;
        $order->save($all_data);
        Session::flash('message', 'Contracts  Created successfully');
        return back();
        }
        else
        {
            return JsonResponse::create(['error' => 'access-denied'],401);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $user   =Auth::user();

        if($user->hasRole('admin') || $user->hasRole('salesManager') || $user->hasRole('sales'))  {

            $contract_doc  = $request->file('contract_doc');

        $order = Contract::findOrFail($id);
        if(isset($contract_doc)) {
            if (isset($contract_doc)) {
                $extension1 = $request->file('contract_doc')->getClientOriginalExtension();
            }
            $sha = 'contract_doc' . md5(time());
            if(isset($extension1)){
                $filename1 = date('Y').$sha . "sgfs." . $extension1;
            }
            $destination_path = 'contract_doc/';
            if (empty($destination_path)) {
                File::makeDirectory($destination_path, 0775, true, true);
                // File::makeDirectory($destination_path, $mode = 0777, true, true);
            }
            if(isset($filename1)){
                $order->contract_doc = $destination_path . $filename1;
                $request->file('contract_doc')->move($destination_path, $filename1);
                $order->save();
            }

        }
        $data = $request->all();
        $data['contract_doc']   = !empty($filename1) ? $filename1                : $order->contract_doc;
        $order->update($data);
        Session::flash('message', 'Contract Updated Successful');
        return back();
    }
        else
        {
        return JsonResponse::create(['error' => 'access-denied'],401);
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy($id)
    {
        $user   =Auth::user();

        if($user->hasRole('admin') || $user->hasRole('salesManager') || $user->hasRole('sales'))  {

        $staff = Contract::findOrfail($id);
        $staff->delete();
        Session::flash('message', 'Contract Deleted Successful');
        return back();
    }
        else
        {
            return JsonResponse::create(['error' => 'access-denied'],401);
        }
    }
}
