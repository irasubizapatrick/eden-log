<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TicketStatus extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $table = 'ticket_statuses';
    protected $fillable = ['id', 'user_id','ticket_id','solution'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function tick()
    {
        return $this->belongsTo('App\Ticket', 'ticket_id');
    }
}
