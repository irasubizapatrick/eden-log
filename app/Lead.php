<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Lead extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $table = 'leads';
    protected $fillable = ['id', 'user_id', 'lead_name','lead_address','lead_phone','lead_email','lead_status','client_id','device_number',
        'tin_number','serial_number','mac_address','acceptance_doc'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
}