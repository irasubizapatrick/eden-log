<!DOCTYPE html>
<html lang="en">
@include('layouts.web_head')

<body>
<!-- Main header -->
@include('layouts.web_header')
<!-- End of Main header -->

<!-- Page Title -->
<section class="page-title-wrap">
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="page-title" >
                    <h2>Our Packages</h2>
                    <ul class="list-unstyled m-0 d-flex">
                        <li><a href="/"><i class="fa fa-home"></i> Home</a></li>
                        <li><a href="#">Our Packages</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End of Page Title -->

<!-- Services -->
<section class="pt-120 pb-90">
    <div class="container">
        <div class="row pb-90">
            <div class="col-lg-4 col-sm-6">
                <div class="single-package text-center animated fadeInUp">
                    <h4>Iwacu Pack</h4>
                    <span>for Residential Tailored</span>
                    <hr>
                    <ul class="list-unstyled">
                        <li>Ranging from <span>5Mbps</span></li>
                        <li>to Dedicated to 30 Mbps Dedicated</li>
                        <li>One time Set up fee of 50,000 RWF</li>
                        <li>All Prices are VAT include</li>
                    </ul>
                    <p> 40,000 <sup>RWF</sup><span>/Monthly</span></p>
                    <a href="index.html#" class="btn">Order This Plan</a>
                </div>
            </div>
            <div class="col-lg-4 col-sm-6">
                <div class="single-package text-center animated fadeInUp">
                    <h4>Karibu Pack</h4>
                    <span>for SMEs Tailored</span>
                    <hr>
                    <ul class="list-unstyled">
                        <li>Ranging from <span>5Mbps</span></li>
                        <li>to Dedicated to 30Mbps Dedicated</li>
                        <li>One time Set up fee of 50,000 RWF</li>
                        <li>All Prices are VAT include</li>
                    </ul>
                    <p> 80,000 <sup>RWF</sup><span>/Monthly</span></p>
                    <a href="index.html#" class="btn">Order This Plan</a>
                </div>
            </div>
            <div class="col-lg-4 col-sm-6">
                <div class="single-package text-center animated fadeInUp">
                    <h4>Corporate Pack</h4>
                    <span>for Business user</span>
                    <hr>
                    <ul class="list-unstyled">
                        <li>Ranging from <span>3Mbps</span></li>
                        <li>Dedicated to 300Mbps Dedicated</li>
                        <li>One time Set up fee of 50,000 RWF</li>
                        <li>All Prices are VAT include</li>
                    </ul>
                    <p> 221,000 <sup>RWF</sup><span>/Monthly</span></p>
                    <a href="index.html#" class="btn">Order This Plan</a>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End of Services -->
<!-- Footer -->
@include('layouts.web_footer')
<!-- End of Footer -->

<!-- Back to top -->
<div class="back-to-top">
    <a href="index.html#"> <i class="fa fa-chevron-up"></i></a>
</div>

@include('layouts.web_js')
</body>
</html>