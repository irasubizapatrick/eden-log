<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    @include('layouts.dash_title')
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description" />
    <meta content="Revolution Air " name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <!-- App favicon -->
    <link rel="shortcut icon" type="/dash/image/png" href="/dash/img/logo.png">
    <!--Morris Chart-->
    <link rel="stylesheet" href="/dash/assets/libs/morris-js/morris.css" />
    <!-- App css -->
    <link href="/dash/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="/dash/assets/css/icons.min.css" rel="stylesheet" type="text/css" />
    <link href="/dash/assets/css/app.min.css" rel="stylesheet" type="text/css" />
</head>

<body>
<!-- Begin page -->
<div id="wrapper">
    <!-- Topbar Start -->
@include('layouts.top_bar')
    <!-- end Topbar -->

    <!-- ========== Left Sidebar Start ========== -->
    <div class="left-side-menu">

        <div class="slimscroll-menu">

            <!-- User box -->
            <div class="user-box text-center">
                <img src="{{Auth::user()->email}}" alt="user-img"  class=" img-thumbnail avatar-lg rounded-circle img-circle profile">
                <div class="dropdown">
                    <a href="/dashboard" class="text-dark dropdown-toggle h5 mt-2 mb-1 d-block" data-toggle="dropdown">{{Auth::user()->name}}</a>

                </div>
            </div>

            <!--- Sidemenu -->
        @include('layouts.client_sidebar')
        <!-- End Sidebar -->

            <div class="clearfix"></div>

        </div>
        <!-- Sidebar -left -->

    </div>
    <!-- Left Sidebar End -->

    <!-- ============================================================== -->
    <!-- Start Page Content here -->
    <!-- ============================================================== -->

    <div class="content-page">
        <div class="content">

            <!-- Start Content-->
            <div class="container-fluid">

                {{--<div class="row">--}}

                    {{--<div class="col-xl-4 col-md-6">--}}
                        {{--<div class="card-box">--}}

                            {{--<h4 class="header-title mt-0 mb-4">Service</h4>--}}
                            {{--<a href="/site/survey/request">--}}
                                {{--<div class="widget-chart-1">--}}
                                    {{--<div class="widget-chart-box-1 float-left" dir="ltr">--}}
                                        {{--<input data-plugin="knob" data-width="80" data-height="80" data-fgColor="#f05050 "--}}
                                               {{--data-bgColor="#F9B9B9" value="{{$count_lead}}"--}}
                                               {{--data-skin="tron" data-angleOffset="180" data-readOnly=true--}}
                                               {{--data-thickness=".15"/>--}}
                                    {{--</div>--}}
                                    {{--<div class="widget-detail-1 text-right">--}}
                                        {{--<h2 class="font-weight-normal pt-2 mb-1"> {{$count_lead}} </h2>--}}
                                        {{--<p class="text-muted mb-1">Revenue today</p>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</a>--}}
                        {{--</div>--}}

                    {{--</div><!-- end col -->--}}

                    {{--<div class="col-xl-4 col-md-6">--}}
                        {{--<div class="card-box">--}}
                            {{--<h4 class="header-title mt-0 mb-3"> My Usage </h4>--}}

                            {{--<div class="widget-box-2">--}}
                                {{--<div class="widget-detail-2 text-right">--}}
                                    {{--<span class="badge badge-success badge-pill float-left mt-3">0 <i class="mdi mdi-trending-up"></i> </span>--}}
                                    {{--<h2 class="font-weight-normal mb-1"> 0 </h2>--}}
                                    {{--<p class="text-muted mb-3">Revenue today</p>--}}
                                {{--</div>--}}
                                {{--<div class="progress progress-bar-alt-success progress-sm">--}}
                                    {{--<div class="progress-bar bg-" role="progressbar"--}}
                                         {{--aria-valuenow="77" aria-valuemin="0" aria-valuemax="100"--}}
                                         {{--style="width: 77%;">--}}
                                        {{--<span class="sr-only">77% Complete</span>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}

                    {{--</div><!-- end col -->--}}


                    {{--<div class="col-xl-4 col-md-6">--}}
                        {{--<div class="card-box">--}}

                            {{--<h4 class="header-title mt-0 mb-3">Documents</h4>--}}

                            {{--<div class="widget-box-2">--}}
                                {{--<div class="widget-detail-2 text-right">--}}
                                    {{--<span class="badge badge-pink badge-pill float-left mt-3">0 <i class="mdi mdi-trending-up"></i> </span>--}}
                                    {{--<h2 class="font-weight-normal mb-1"> 0 </h2>--}}
                                    {{--<p class="text-muted mb-3">Revenue today</p>--}}
                                {{--</div>--}}
                                {{--<div class="progress progress-bar-alt-pink progress-sm">--}}
                                    {{--<div class="progress-bar bg-pink" role="progressbar"--}}
                                         {{--aria-valuenow="77" aria-valuemin="0" aria-valuemax="100"--}}
                                         {{--style="width: 77%;">--}}
                                        {{--<span class="sr-only">77% Complete</span>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}

                    {{--</div><!-- end col -->--}}

                {{--</div>--}}
                <!-- end row -->
                <!-- end row -->

                <div class="row">
                    <div class="col-xl-12">
                        <div class="card-box">

                            <h4 class="header-title mt-0 mb-3">Service </h4>

                            <div class="table-responsive">
                                <table class="table table-hover mb-0">
                                    <thead>
                                    <tr style="color: black;">
                                        <th>#</th>
                                        <th>Date</th>
                                        <th>Product</th>
                                        <th>Package</th>
                                        <th>Name</th>
                                        <th>Address</th>
                                        <th>Phone</th>
                                        <th>Email</th>
                                        <th>Acceptance Letter</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    <?php $i=1;?>
                                    @foreach($client AS $value)
                                        <tr style="color: black;">
                                            <td>{{$i++}}</td>
                                            <td>{{$value->created_at}}</td>
                                            <td>{{$value->package->product->product_name}} </td>
                                            <td>{{$value->package->package_name}}</td>
                                            <td>{{$value->client_name}}</td>
                                            <td>{{$value->client_address}}</td>
                                            <td>{{$value->client_phone}}</td>
                                            <td>{{$value->client_email}}</td>
                                            <td>
                                            @if($value->lead->acceptance_doc !='')

                                                <a href="./accept_letter/{{$value->lead->acceptance_doc}}" target="_blank">File</a>
                                            @else
                                                <p>no file</p>
                                            @endif

                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div><!-- end col -->

                </div>
                <!-- end row -->

            </div> <!-- container -->

        </div> <!-- content -->



    </div>

    <!-- ============================================================== -->
    <!-- End Page content -->
    <!-- ============================================================== -->


</div>
<!-- END wrapper -->

<!-- Right Sidebar -->
<div class="right-bar">
    <div class="rightbar-title">
        <a href="javascript:void(0);" class="right-bar-toggle float-right">
            <i class="dripicons-cross noti-icon"></i>
        </a>
        <h5 class="m-0 text-white">Settings</h5>
    </div>
    <div class="slimscroll-menu">
        <!-- User box -->
        <div class="user-box">
            <div class="user-img">
                <img src="/dash/assets/images/users/user-1.jpg" alt="user-img" title="Mat Helme" class="rounded-circle img-fluid">
                <a href="javascript:void(0);" class="user-edit"><i class="mdi mdi-pencil"></i></a>
            </div>

            <h5><a href="javascript: void(0);">Username</a> </h5>
            <p class="text-muted mb-0"><small>Admin Head</small></p>
        </div>

        <!-- Settings -->
        <hr class="mt-0" />
        <h5 class="pl-3">Basic Settings</h5>
        <hr class="mb-0" />

        <div class="p-3">
            <div class="checkbox checkbox-primary mb-2">
                <input id="Rcheckbox1" type="checkbox" checked>
                <label for="Rcheckbox1">
                    Notifications
                </label>
            </div>
            <div class="checkbox checkbox-primary mb-2">
                <input id="Rcheckbox2" type="checkbox" checked>
                <label for="Rcheckbox2">
                    API Access
                </label>
            </div>
            <div class="checkbox checkbox-primary mb-2">
                <input id="Rcheckbox3" type="checkbox">
                <label for="Rcheckbox3">
                    Auto Updates
                </label>
            </div>
            <div class="checkbox checkbox-primary mb-2">
                <input id="Rcheckbox4" type="checkbox" checked>
                <label for="Rcheckbox4">
                    Online Status
                </label>
            </div>
            <div class="checkbox checkbox-primary mb-0">
                <input id="Rcheckbox5" type="checkbox" checked>
                <label for="Rcheckbox5">
                    Auto Payout
                </label>
            </div>
        </div>

        <!-- Timeline -->
        <hr class="mt-0" />
        <h5 class="pl-3 pr-3">Messages <span class="float-right badge badge-pill badge-danger">25</span></h5>
        <hr class="mb-0" />
        <div class="p-3">
            <div class="inbox-widget">
                <div class="inbox-item">
                    <div class="inbox-item-img"><img src="/dash/assets/images/users/user-2.jpg" class="rounded-circle" alt=""></div>
                    <p class="inbox-item-author"><a href="javascript: void(0);" class="text-dark">Tomaslau</a></p>
                    <p class="inbox-item-text">I've finished it! See you so...</p>
                </div>
                <div class="inbox-item">
                    <div class="inbox-item-img"><img src="/dash/assets/images/users/user-3.jpg" class="rounded-circle" alt=""></div>
                    <p class="inbox-item-author"><a href="javascript: void(0);" class="text-dark">Stillnotdavid</a></p>
                    <p class="inbox-item-text">This theme is awesome!</p>
                </div>
                <div class="inbox-item">
                    <div class="inbox-item-img"><img src="/dash/assets/images/users/user-4.jpg" class="rounded-circle" alt=""></div>
                    <p class="inbox-item-author"><a href="javascript: void(0);" class="text-dark">Kurafire</a></p>
                    <p class="inbox-item-text">Nice to meet you</p>
                </div>

                <div class="inbox-item">
                    <div class="inbox-item-img"><img src="/dash/assets/images/users/user-5.jpg" class="rounded-circle" alt=""></div>
                    <p class="inbox-item-author"><a href="javascript: void(0);" class="text-dark">Shahedk</a></p>
                    <p class="inbox-item-text">Hey! there I'm available...</p>
                </div>
                <div class="inbox-item">
                    <div class="inbox-item-img"><img src="/dash/assets/images/users/user-6.jpg" class="rounded-circle" alt=""></div>
                    <p class="inbox-item-author"><a href="javascript: void(0);" class="text-dark">Adhamdannaway</a></p>
                    <p class="inbox-item-text">This theme is awesome!</p>
                </div>
            </div> <!-- end inbox-widget -->
        </div> <!-- end .p-3-->

    </div> <!-- end slimscroll-menu-->
</div>
<!-- /Right-bar -->

<!-- Right bar overlay-->
<div class="rightbar-overlay"></div>

<!-- Vendor js -->
<script src="/dash/assets/js/vendor.min.js"></script>

<!-- knob plugin -->
<script src="/dash/assets/libs/jquery-knob/jquery.knob.min.js"></script>

<!--Morris Chart-->
<script src="/dash/assets/libs/morris-js/morris.min.js"></script>
<script src="/dash/assets/libs/raphael/raphael.min.js"></script>

<!-- Dashboard init js-->
<script src="/dash/assets/js/pages/dashboard.init.js"></script>

<!-- App js -->
<script src="/dash/assets/js/app.min.js"></script>

<script src="/dash/assets/js/initial.js-master/initial.js" type="text/javascript"></script>

<script>
    $('.profile').initial();
</script>

</body>
</html>