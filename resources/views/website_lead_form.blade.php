<!DOCTYPE html>
<html lang="en">
@include('layouts.web_head')

<style type="text/css">
    .note
    {
        text-align: center;
        height: 80px;
        background: #3377bc;
        color: #fff;
        font-weight: 500;
        line-height: 80px;
        font-size: 27px;
        -webkit-font-smoothing: antialiased;

    }
    .form-content
    {
        padding: 5%;
        border: 1px solid #ced4da;
        margin-bottom: 2%;
    }
    .form-control{
        border-radius:0px ;
    }
    .btnSubmit
    {
        border:none;
        border-radius:1.5rem;
        padding: 1%;
        width: 20%;
        cursor: pointer;
        background: #01a86c;
        color: #fff;
    }
</style>
<body>
<!-- Main header -->
@include('layouts.web_header')
<!-- End of Main header -->
<section class="page-title-wrap">
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="page-title" >
                    <h2>Site Survey Request</h2>
                    <ul class="list-unstyled m-0 d-flex">
                        <li><a href="/"><i class="fa fa-home"></i> Home</a></li>
                        <li><a href="#">Site  Survey</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Abut Us -->
<section class="pt-55 pb-55">
    <div class="container register-form">
        <div class="form">
            <div class="note">
              Site Survey Form
            </div>


            <div class="form-content">
                <div class="success ">
                    @if (Session::has('message'))
                        <div class="alert alert-success alert-dismissable">
                            <a href="" class="close" data-dismiss="alert" aria-label="close">
                                &times;

                            </a>
                            {{ Session::get('message') }}
                        </div>

                    @endif
                    @if (Session::has('delete'))
                        <div class="alert alert-success alert-dismissable">
                            <a href="" class="close" data-dismiss="alert" aria-label="close">
                                &times;

                            </a>
                            {{ Session::get('delete') }}
                        </div>

                    @endif
                    @if (Session::has('updated'))
                        <div class="alert alert-success alert-dismissable">
                            <a href="" class="close" data-dismiss="alert" aria-label="close">
                                &times;

                            </a>
                            {{ Session::get('updated') }}
                        </div>

                    @endif
                </div>
                <form class="form-horizontal" action="/lead_reg" method="POST" enctype="multipart/form-data">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Full Names</label>
                                <input type="text" class="form-control" name="lead_name" placeholder="Enter Your Name *" required value=""/>
                            </div>
                            <div class="form-group">
                                <label>Phone Number</label>
                                <input type="number" class="form-control"  name="lead_phone" placeholder=" Phone Number *"  required value=""/>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Email</label>
                                <input type="email" class="form-control" name="lead_email" placeholder="Enter your Email" value="" required/>
                            </div>
                            <div class="form-group">
                                <label>Address </label>
                                <input type="text" class="form-control" name="lead_address" placeholder="Ex: Gasabo *" value="" required/>
                            </div>
                        </div>
                    </div>
                    <button type="submit" class="btnSubmit">Submit</button>

                </form>
            </div>
        </div>
    </div>
</section>
<!-- End of About Us -->
<!-- Footer -->
@include('layouts.web_footer')
<!-- End of Footer -->

<!-- Back to top -->
<div class="back-to-top">
    <a href="index.html#"> <i class="fa fa-chevron-up"></i></a>
</div>

@include('layouts.web_js')
</body>
</html>