<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    @include('layouts.dash_title')
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description" />
    <meta content="Revolution Air " name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <!-- App favicon -->
    <link rel="shortcut icon" type="image/png" href="/dash/img/logo.png">

    <!-- third party css -->
    <link href="/dash/assets/libs/datatables/dataTables.bootstrap4.css" rel="stylesheet" type="text/css" />
    <link href="/dash/assets/libs/datatables/responsive.bootstrap4.css" rel="stylesheet" type="text/css" />
    <link href="/dash/assets/libs/datatables/buttons.bootstrap4.css" rel="stylesheet" type="text/css" />
    <link href="/dash/assets/libs/datatables/select.bootstrap4.css" rel="stylesheet" type="text/css" />
    <!-- third party css end -->


    <!-- App css -->
    <link href="/dash/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="/dash/assets/css/icons.min.css" rel="stylesheet" type="text/css" />
    <link href="/dash/assets/css/app.min.css" rel="stylesheet" type="text/css" />

</head>

<body>

<!-- Begin page -->
<div id="wrapper">

    <!-- Topbar Start -->
@include('layouts.top_bar')
    <!-- end Topbar -->

    <!-- ========== Left Sidebar Start ========== -->
    <div class="left-side-menu">

        <div class="slimscroll-menu">

            <div class="user-box text-center">
                <img src="{{Auth::user()->email}}" alt="user-img"  class=" img-thumbnail avatar-lg rounded-circle img-circle profile">
                <div class="dropdown">
                    <a href="/dashboard" class="text-dark dropdown-toggle h5 mt-2 mb-1 d-block" data-toggle="dropdown">{{Auth::user()->name}}</a>

                </div>
            </div>
            <!--- Sidemenu -->
        @include('layouts.sales_manager_sidebar')
        <!-- End Sidebar -->

            <div class="clearfix"></div>

        </div>
        <!-- Sidebar -left -->

    </div>
    <!-- Left Sidebar End -->

    <!-- ============================================================== -->
    <!-- Start Page Content here -->
    <!-- ============================================================== -->

    <div class="content-page">
        <div class="content">

            <!-- Start Content-->
            <div class="container-fluid">

                <div class="row">
                    <div class="col-12">
                        <div class="card-box">
                            <h4 class="header-title ">Leads  Status Details</h4>
                            <a  href="{{URL::previous()}}" role="button" class="btn btn-danger waves-effect waves-light float-right" ><span><i  class="fa fa-arrow-left"></i> Get Back</span></a>
                            <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap">
                                <thead>
                                <tr>
                                    <th class="d-flex">#</th>
                                    <th>Date</th>
                                    <th>Created From</th>
                                    <th>Name</th>
                                    <th>Address</th>
                                    <th>Phone</th>
                                    <th>Email</th>
                                    <th>Status</th>
                                    <th>Comment</th>
                                </tr>
                                </thead>
                                <tbody>

                                <?php $i=1;?>
                                @foreach($leads_status AS $value)
                                    <tr>
                                        <td class="d-flex">{{$i++}}</td>
                                        <td>{{$value->created_at}}</td>
                                        <td>
                                            @if($value->user_id =='1')
                                                <span>Website</span>
                                            @else
                                                <span>{{$value->user->name}}</span>
                                            @endif
                                        </td>
                                        <td>{{$value->lead->lead_name}}</td>
                                        <td>{{$value->lead->lead_address}}</td>
                                        <td>{{$value->lead->lead_phone}}</td>
                                        <td>{{$value->lead->lead_email}}</td>

                                        <td>
                                            @if($value->status_lead =='pending')
                                                <span class="badge badge-warning">{{$value->status_lead}}</span>
                                            @elseif($value->status_lead =='Site Survey Request')
                                                <span class="badge badge-primary">{{$value->status_lead}}</span>
                                            @elseif($value->status_lead =='Positive')
                                                <span class="badge badge-success">{{$value->status_lead}}</span>
                                            @elseif($value->status_lead =='Request Installation')
                                                <span class="badge badge-purple">{{$value->status_lead}}</span>
                                            @elseif($value->status_lead =='on going')
                                                <span class="badge badge-pink">{{$value->status_lead}}</span>
                                            @elseif($value->status_lead =='On hold')
                                                <span class="badge badge-warning">{{$value->status_lead}}</span>
                                            @elseif($value->status_lead =='Completed')
                                                <span class="badge badge-success">{{$value->status_lead}}</span>
                                            @else
                                                <span class="badge badge-danger">{{$value->status_lead}}</span>
                                            @endif

                                        </td>
                                        <td>
                                            @if($value->comment == '')
                                                <span>no comment</span>
                                            @else
                                                <span>{{$value->comment}}</span>
                                            @endif

                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- end row -->

            </div> <!-- container-fluid -->

        </div> <!-- content -->



    </div>

    <!-- ============================================================== -->
    <!-- End Page content -->
    <!-- ============================================================== -->


</div>
<!-- END wrapper -->


<!-- Right bar overlay-->
<div class="rightbar-overlay"></div>

<!-- Vendor js -->
<script src="/dash/assets/js/vendor.min.js"></script>

<!-- third party js -->
<script src="/dash/assets/libs/datatables/jquery.dataTables.min.js"></script>
<script src="/dash/assets/libs/datatables/dataTables.bootstrap4.js"></script>
<script src="/dash/assets/libs/datatables/dataTables.responsive.min.js"></script>
<script src="/dash/assets/libs/datatables/responsive.bootstrap4.min.js"></script>
<script src="/dash/assets/libs/datatables/dataTables.buttons.min.js"></script>
<script src="/dash/assets/libs/datatables/buttons.bootstrap4.min.js"></script>
<script src="/dash/assets/libs/datatables/buttons.html5.min.js"></script>
<script src="/dash/assets/libs/datatables/buttons.flash.min.js"></script>
<script src="/dash/assets/libs/datatables/buttons.print.min.js"></script>
<script src="/dash/assets/libs/datatables/dataTables.keyTable.min.js"></script>
<script src="/dash/assets/libs/datatables/dataTables.select.min.js"></script>
<script src="/dash/assets/libs/pdfmake/pdfmake.min.js"></script>
<script src="/dash/assets/libs/pdfmake/vfs_fonts.js"></script>
<!-- third party js ends -->

<!-- Datatables init -->
<script src="/dash/assets/js/pages/datatables.init.js"></script>

<!-- App js -->
<script src="/dash/assets/js/app.min.js"></script>
<script src="/dash/assets/js/initial.js-master/initial.js" type="text/javascript"></script>

<script>
    $('.profile').initial();
</script>
</body>
</html>