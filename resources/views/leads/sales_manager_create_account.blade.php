<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    @include('layouts.dash_title')
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description" />
    <meta content="Revolution Air " name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <!-- App favicon -->
    <link rel="shortcut icon" type="image/png" href="/dash/img/logo.png">

    <!-- third party css -->
    <link href="/dash/assets/libs/datatables/dataTables.bootstrap4.css" rel="stylesheet" type="text/css" />
    <link href="/dash/assets/libs/datatables/responsive.bootstrap4.css" rel="stylesheet" type="text/css" />
    <link href="/dash/assets/libs/datatables/buttons.bootstrap4.css" rel="stylesheet" type="text/css" />
    <link href="/dash/assets/libs/datatables/select.bootstrap4.css" rel="stylesheet" type="text/css" />
    <!-- third party css end -->


    <!-- App css -->
    <link href="/dash/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="/dash/assets/css/icons.min.css" rel="stylesheet" type="text/css" />
    <link href="/dash/assets/css/app.min.css" rel="stylesheet" type="text/css" />

</head>

<body>

<!-- Begin page -->
<div id="wrapper">

    <!-- Topbar Start -->
@include('layouts.top_bar')
    <!-- end Topbar -->

    <!-- ========== Left Sidebar Start ========== -->
    <div class="left-side-menu">

        <div class="slimscroll-menu">

            <div class="user-box text-center">
                <img src="{{Auth::user()->email}}" alt="user-img"  class=" img-thumbnail avatar-lg rounded-circle img-circle profile">
                <div class="dropdown">
                    <a href="/dashboard" class="text-dark dropdown-toggle h5 mt-2 mb-1 d-block" data-toggle="dropdown">{{Auth::user()->name}}</a>

                </div>
            </div>
            <!--- Sidemenu -->
        @include('layouts.sales_manager_sidebar')
        <!-- End Sidebar -->

            <div class="clearfix"></div>

        </div>
        <!-- Sidebar -left -->

    </div>
    <!-- Left Sidebar End -->

    <!-- ============================================================== -->
    <!-- Start Page Content here -->
    <!-- ============================================================== -->

    <div class="content-page">
        <div class="content">

            <!-- Start Content-->
            <div class="container-fluid">
                @foreach($leads_status  as $value)
                <div class="row">
                    <div class="col-12">
                        <div class="card-box">
                            <h4 class="header-title ">Create Client Account</h4>
                                <form class="form-horizontal" action="/manage/customer" method="POST" enctype="multipart/form-data">
                                <input type="hidden" name="_method" value="PUT" />
                                <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                <div class="modal-body">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label> Product </label>
                                                <select class="form-control" name="product_id" required>
                                                    @foreach($product as $products)
                                                        <option value="{{$products->id}}">{{$products->product_name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label> Package </label>
                                                <select class="form-control" name="package_id" required>
                                                    @foreach($package as $packages)
                                                        <option value="{{$packages->id}}">{{$packages->package_name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Full Names</label>

                                                <input type="hidden" class="form-control" value="{{$value->id}}" name="lead_id"  placeholder="Enter Your Name *" required />
                                                <input type="text" class="form-control" value="{{$value->lead_name}}" name="client_name"  placeholder="Enter Your Name *" required />
                                            </div>
                                            <div class="form-group">
                                                <label>Phone Number</label>
                                                <input type="text" class="form-control" value="{{$value->lead_phone}}"  name="client_phone" placeholder=" Phone Number *"  required />
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Email</label>
                                                <input type="email" class="form-control" value="{{$value->lead_email}}" name="client_email" placeholder="Enter your Email"  required/>
                                            </div>
                                            <div class="form-group">
                                                <label>Address </label>
                                                <input type="text" class="form-control" value="{{$value->lead_address}}" name="client_address" placeholder="Ex: Gasabo *"  required/>
                                                <input type="hidden" class="form-control" name="role" value="Client" />
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>ID Number</label>
                                                <input type="text" name="client_id" class="form-control"  value="{{$value->client_id}}" placeholder="Enter national ID or passport" required>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Device Number</label>
                                                <input type="text" name="device_number" value="{{$value->device_number}}" class="form-control" placeholder="Enter Device number" required>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Serial Number</label>
                                                <input type="text" name="serial_number" value="{{$value->serial_number}}" class="form-control"  placeholder="Enter serial number" required>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Tin Number  </label>
                                                <input type="text" name="tin_number" value="{{$value->tin_number}}" class="form-control"  placeholder="Enter tin number " >
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Mac address </label>
                                                <input type="text" name="mac_address"  value="{{$value->mac_address}}"class="form-control"  placeholder="Enter mac address" required>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>User Name</label>
                                                <input type="text" class="form-control" value="{{$value->lead_name}}"  name="name" placeholder="Enter your User Name"  required/>
                                                <input type="hidden" class="form-control" value="{{$value->acceptance_doc}}"  name="acceptance_doc" placeholder="Enter your User Name"  required/>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Password </label>
                                                <input type="password" class="form-control" value="password" name="password" placeholder="*********"  required/>
                                            </div>
                                        </div>
                                        <div class="col-md-12 mx-auto">
                                            <div class="form-group">
                                                <button type="submit" class="btn btn-success">Create  Client account</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>

                        </div>
                    </div>
                </div>
            @endforeach
                <!-- end row -->

            </div> <!-- container-fluid -->

        </div> <!-- content -->
    </div>

    <!-- ============================================================== -->
    <!-- End Page content -->
    <!-- ============================================================== -->


</div>
<!-- END wrapper -->


<!-- Right bar overlay-->
<div class="rightbar-overlay"></div>

<!-- Vendor js -->
<script src="/dash/assets/js/vendor.min.js"></script>

<!-- third party js -->
<script src="/dash/assets/libs/datatables/jquery.dataTables.min.js"></script>
<script src="/dash/assets/libs/datatables/dataTables.bootstrap4.js"></script>
<script src="/dash/assets/libs/datatables/dataTables.responsive.min.js"></script>
<script src="/dash/assets/libs/datatables/responsive.bootstrap4.min.js"></script>
<script src="/dash/assets/libs/datatables/dataTables.buttons.min.js"></script>
<script src="/dash/assets/libs/datatables/buttons.bootstrap4.min.js"></script>
<script src="/dash/assets/libs/datatables/buttons.html5.min.js"></script>
<script src="/dash/assets/libs/datatables/buttons.flash.min.js"></script>
<script src="/dash/assets/libs/datatables/buttons.print.min.js"></script>
<script src="/dash/assets/libs/datatables/dataTables.keyTable.min.js"></script>
<script src="/dash/assets/libs/datatables/dataTables.select.min.js"></script>
<script src="/dash/assets/libs/pdfmake/pdfmake.min.js"></script>
<script src="/dash/assets/libs/pdfmake/vfs_fonts.js"></script>
<!-- third party js ends -->

<!-- Datatables init -->
<script src="/dash/assets/js/pages/datatables.init.js"></script>

<!-- App js -->
<script src="/dash/assets/js/app.min.js"></script>
<script src="/dash/assets/js/initial.js-master/initial.js" type="text/javascript"></script>

<script>
    $('.profile').initial();
</script>
</body>
</html>