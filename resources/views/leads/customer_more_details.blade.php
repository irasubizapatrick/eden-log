<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    @include('layouts.dash_title')
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description" />
    <meta content="Revolution Air " name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <!-- App favicon -->
    <link rel="shortcut icon" type="image/png" href="/dash/img/logo.png">

    <!-- third party css -->
    <link href="/dash/assets/libs/datatables/dataTables.bootstrap4.css" rel="stylesheet" type="text/css" />
    <link href="/dash/assets/libs/datatables/responsive.bootstrap4.css" rel="stylesheet" type="text/css" />
    <link href="/dash/assets/libs/datatables/buttons.bootstrap4.css" rel="stylesheet" type="text/css" />
    <link href="/dash/assets/libs/datatables/select.bootstrap4.css" rel="stylesheet" type="text/css" />
    <!-- third party css end -->


    <!-- App css -->
    <link href="/dash/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="/dash/assets/css/icons.min.css" rel="stylesheet" type="text/css" />
    <link href="/dash/assets/css/app.min.css" rel="stylesheet" type="text/css" />

</head>

<body>

<!-- Begin page -->
<div id="wrapper">

    <!-- Topbar Start -->
@include('layouts.top_bar')
    <!-- end Topbar -->

    <!-- ========== Left Sidebar Start ========== -->
    <div class="left-side-menu">

        <div class="slimscroll-menu">

            <div class="user-box text-center">
                <img src="{{Auth::user()->email}}" alt="user-img"  class=" img-thumbnail avatar-lg rounded-circle img-circle profile">
                <div class="dropdown">
                    <a href="/dashboard" class="text-dark dropdown-toggle h5 mt-2 mb-1 d-block" data-toggle="dropdown">{{Auth::user()->name}}</a>

                </div>
            </div>
            <!--- Sidemenu -->
        @include('layouts.sales_manager_sidebar')
        <!-- End Sidebar -->

            <div class="clearfix"></div>

        </div>
        <!-- Sidebar -left -->

    </div>
    <!-- Left Sidebar End -->

    <!-- ============================================================== -->
    <!-- Start Page Content here -->
    <!-- ============================================================== -->

    <div class="content-page">
        <div class="content">

            <!-- Start Content-->
            <div class="container-fluid">

                <div class="row">
                    <div class="col-12">
                        <div class="card-box">
                            <h4 class="header-title ">CLient  More  Details</h4>
                            <a  href="{{URL::previous()}}" role="button" class="btn btn-danger waves-effect waves-light float-right" ><span><i  class="fa fa-arrow-left"></i> Get Back</span></a>
                            <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap">
                                <thead>
                                <tr>
                                    <th class="d-flex">#</th>
                                    <th>Date</th>
                                    <th>Name</th>
                                    <th>ID</th>
                                    <th>TIN Number</th>
                                    <th>Device Number</th>
                                    <th>Serial Number</th>
                                    <th>Mac Address</th>
                                    <th>Acceptance Letter</th>
                                </tr>
                                </thead>
                                <tbody>

                                <?php $i=1;?>
                                @foreach($client AS $value)
                                    <tr>
                                        <td class="d-flex">{{$i++}}</td>
                                        <td>{{$value->created_at}}</td>
                                        <td>{{$value->client_name}}</td>
                                        <td>{{$value->client_id}}</td>
                                        <td>{{$value->tin_number}}</td>
                                        <td>{{$value->device_number}}</td>
                                        <td>{{$value->serial_number}}</td>
                                        <td>{{$value->mac_address}}</td>
                                        <td>
                                            <a href="/acceptance/{{$value->acceptance_doc}}" target="_blank">File</a></td>

                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- end row -->

            </div> <!-- container-fluid -->

        </div> <!-- content -->



    </div>

    <!-- ============================================================== -->
    <!-- End Page content -->
    <!-- ============================================================== -->


</div>
<!-- END wrapper -->


<!-- Right bar overlay-->
<div class="rightbar-overlay"></div>

<!-- Vendor js -->
<script src="/dash/assets/js/vendor.min.js"></script>

<!-- third party js -->
<script src="/dash/assets/libs/datatables/jquery.dataTables.min.js"></script>
<script src="/dash/assets/libs/datatables/dataTables.bootstrap4.js"></script>
<script src="/dash/assets/libs/datatables/dataTables.responsive.min.js"></script>
<script src="/dash/assets/libs/datatables/responsive.bootstrap4.min.js"></script>
<script src="/dash/assets/libs/datatables/dataTables.buttons.min.js"></script>
<script src="/dash/assets/libs/datatables/buttons.bootstrap4.min.js"></script>
<script src="/dash/assets/libs/datatables/buttons.html5.min.js"></script>
<script src="/dash/assets/libs/datatables/buttons.flash.min.js"></script>
<script src="/dash/assets/libs/datatables/buttons.print.min.js"></script>
<script src="/dash/assets/libs/datatables/dataTables.keyTable.min.js"></script>
<script src="/dash/assets/libs/datatables/dataTables.select.min.js"></script>
<script src="/dash/assets/libs/pdfmake/pdfmake.min.js"></script>
<script src="/dash/assets/libs/pdfmake/vfs_fonts.js"></script>
<!-- third party js ends -->

<!-- Datatables init -->
<script src="/dash/assets/js/pages/datatables.init.js"></script>

<!-- App js -->
<script src="/dash/assets/js/app.min.js"></script>
<script src="/dash/assets/js/initial.js-master/initial.js" type="text/javascript"></script>

<script>
    $('.profile').initial();
</script>
</body>
</html>