<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    @include('layouts.dash_title')
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description" />
    <meta content="Revolution Air " name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <!-- App favicon -->
    <link rel="shortcut icon" type="image/png" href="/dash/img/logo.png">

    <!-- third party css -->
    <link href="/dash/assets/libs/datatables/dataTables.bootstrap4.css" rel="stylesheet" type="text/css" />
    <link href="/dash/assets/libs/datatables/responsive.bootstrap4.css" rel="stylesheet" type="text/css" />
    <link href="/dash/assets/libs/datatables/buttons.bootstrap4.css" rel="stylesheet" type="text/css" />
    <link href="/dash/assets/libs/datatables/select.bootstrap4.css" rel="stylesheet" type="text/css" />
    <!-- third party css end -->


    <!-- App css -->
    <link href="/dash/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="/dash/assets/css/icons.min.css" rel="stylesheet" type="text/css" />
    <link href="/dash/assets/css/app.min.css" rel="stylesheet" type="text/css" />

</head>

<body>

<!-- Begin page -->
<div id="wrapper">

    <!-- Topbar Start -->
@include('layouts.top_bar')
    <!-- end Topbar -->

    <!-- ========== Left Sidebar Start ========== -->
    <div class="left-side-menu">

        <div class="slimscroll-menu">

            <div class="user-box text-center">
                <img src="{{Auth::user()->email}}" alt="user-img"  class=" img-thumbnail avatar-lg rounded-circle img-circle profile">
                <div class="dropdown">
                    <a href="/dashboard" class="text-dark dropdown-toggle h5 mt-2 mb-1 d-block" data-toggle="dropdown">{{Auth::user()->name}}</a>

                </div>
            </div>
            <!--- Sidemenu -->
        @include('layouts.dash_sidebar')
        <!-- End Sidebar -->

            <div class="clearfix"></div>

        </div>
        <!-- Sidebar -left -->

    </div>
    <!-- Left Sidebar End -->

    <!-- ============================================================== -->
    <!-- Start Page Content here -->
    <!-- ============================================================== -->

    <div class="content-page">
        <div class="content">

            <!-- Start Content-->
            <div class="container-fluid">

                <div class="row">
                    <div class="col-12">
                        <div class="card-box">
                            <h4 class="header-title ">Ticket List</h4>
                            <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title" id="myLargeModalLabel">Generete Ticket</h4>
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                        </div>
                                        <div class="modal-body">
                                            <form class="form-horizontal" action="/manage/ticket" method="POST" enctype="multipart/form-data">
                                                <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label>Ticket Name </label>
                                                            <select class="form-control" name="ticket_name" required>
                                                                <option value="">Select </option>
                                                                <option value="Internet is slow">Internet is slow</option>
                                                                <option value="Modem is not working">Modem is not working</option>
                                                                <option value="Internet is not working">Internet is not working</option>
                                                                <option value="Other">Other</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label>Explain More </label>
                                                            <textarea type="text" class="form-control" name="explanation" required value=""></textarea>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                                            <button type="submit" class="btn btn-success">Send</button>
                                                        </div>
                                                    </div>
                                                </div>

                                            </form>
                                        </div>
                                    </div><!-- /.modal-content -->
                                </div><!-- /.modal-dialog -->
                            </div><!-- /.modal -->
                            <div class="row">
                                <div class="col-md-6 mx-auto">
                                    @if (Session::has('message'))
                                        <div class="alert alert-success alert-dismissable">
                                            <a href="" class="close" data-dismiss="alert" aria-label="close">
                                                &times;

                                            </a>
                                            {{ Session::get('message') }}
                                        </div>

                                    @endif
                                    @if (Session::has('delete'))
                                        <div class="alert alert-success alert-dismissable">
                                            <a href="" class="close" data-dismiss="alert" aria-label="close">
                                                &times;

                                            </a>
                                            {{ Session::get('delete') }}
                                        </div>

                                    @endif
                                    @if (Session::has('updated'))
                                        <div class="alert alert-success alert-dismissable">
                                            <a href="" class="close" data-dismiss="alert" aria-label="close">
                                                &times;

                                            </a>
                                            {{ Session::get('updated') }}
                                        </div>

                                    @endif
                                </div>
                            </div>
                            <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap">
                                <thead>
                                <tr >
                                    <th>#</th>
                                    <th>Subject</th>
                                    <th>More Details</th>
                                    <th>Last Reply </th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>

                                <?php $i=1;?>
                                @foreach($ticket AS $value)
                                    <tr >
                                        <td >{{$i++}}</td>
                                        <td>{{$value->ticket_name}}</td>
                                        <td>{{$value->explanation}}</td>
                                        <td>
                                            {{$value->user->name}}
                                        </td>
                                        <td>
                                            @if($value->ticket_status == 'pending')
                                                <span class="badge badge-warning">{{$value->ticket_status}}</span>

                                            @else
                                                <span class="badge badge-danger">{{$value->ticket_status}}</span>
                                            @endif

                                        </td>
                                        <td >
                                            <a href="{{route ('view/conservation',['id' =>$value->id])}}" type="button" class=" btn btn-info" style="float: none;"><span class="mdi mdi-comment"> View Conversation</span></a>
                                            <button type="button" data-toggle="modal" class="tabledit-edit-button btn btn-danger" data-target="#delete<?php echo $i;?>" ><span class="mdi mdi-contain-end">Close</span></button>
                                            <div class="modal fade" id="delete<?php echo $i;?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLarge01" style="display: none;" aria-hidden="true">
                                                <div class="modal-dialog modal-lg" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <label class="mx-2">Closed the ticket </label>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">×</span>
                                                            </button>
                                                        </div>
                                                        <form role="form-horizontal" action="/manage/ticket/{{$value->id}}" method="post" enctype="multipart/form-data">
                                                            <input type="hidden" name="_method" value="PUT" />
                                                            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                                            <div class="modal-footer">
                                                                <button type="submit" class="btn btn-success">Confirm</button>
                                                                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>

                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- end row -->

            </div> <!-- container-fluid -->

        </div> <!-- content -->



    </div>

    <!-- ============================================================== -->
    <!-- End Page content -->
    <!-- ============================================================== -->


</div>
<!-- END wrapper -->


<!-- Right bar overlay-->
<div class="rightbar-overlay"></div>

<!-- Vendor js -->
<script src="/dash/assets/js/vendor.min.js"></script>

<!-- third party js -->
<script src="/dash/assets/libs/datatables/jquery.dataTables.min.js"></script>
<script src="/dash/assets/libs/datatables/dataTables.bootstrap4.js"></script>
<script src="/dash/assets/libs/datatables/dataTables.responsive.min.js"></script>
<script src="/dash/assets/libs/datatables/responsive.bootstrap4.min.js"></script>
<script src="/dash/assets/libs/datatables/dataTables.buttons.min.js"></script>
<script src="/dash/assets/libs/datatables/buttons.bootstrap4.min.js"></script>
<script src="/dash/assets/libs/datatables/buttons.html5.min.js"></script>
<script src="/dash/assets/libs/datatables/buttons.flash.min.js"></script>
<script src="/dash/assets/libs/datatables/buttons.print.min.js"></script>
<script src="/dash/assets/libs/datatables/dataTables.keyTable.min.js"></script>
<script src="/dash/assets/libs/datatables/dataTables.select.min.js"></script>
<script src="/dash/assets/libs/pdfmake/pdfmake.min.js"></script>
<script src="/dash/assets/libs/pdfmake/vfs_fonts.js"></script>
<!-- third party js ends -->

<!-- Datatables init -->
<script src="/dash/assets/js/pages/datatables.init.js"></script>

<!-- App js -->
<script src="/dash/assets/js/app.min.js"></script>
<script src="/dash/assets/js/initial.js-master/initial.js" type="text/javascript"></script>

<script>
    $('.profile').initial();
</script>
</body>
</html>