<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    @include('layouts.dash_title')
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description" />
    <meta content="Revolution Air " name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <!-- App favicon -->
    <link rel="shortcut icon" type="image/png" href="/dash/img/logo.png">

    <!-- third party css -->
    <link href="/dash/assets/libs/datatables/dataTables.bootstrap4.css" rel="stylesheet" type="text/css" />
    <link href="/dash/assets/libs/datatables/responsive.bootstrap4.css" rel="stylesheet" type="text/css" />
    <link href="/dash/assets/libs/datatables/buttons.bootstrap4.css" rel="stylesheet" type="text/css" />
    <link href="/dash/assets/libs/datatables/select.bootstrap4.css" rel="stylesheet" type="text/css" />
    <!-- third party css end -->


    <!-- App css -->
    <link href="/dash/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="/dash/assets/css/icons.min.css" rel="stylesheet" type="text/css" />
    <link href="/dash/assets/css/app.min.css" rel="stylesheet" type="text/css" />

</head>

<body>

<!-- Begin page -->
<div id="wrapper">

    <!-- Topbar Start -->
@include('layouts.top_bar')
    <!-- end Topbar -->

    <!-- ========== Left Sidebar Start ========== -->
    <div class="left-side-menu">

        <div class="slimscroll-menu">

            <div class="user-box text-center">
                <img src="{{Auth::user()->email}}" alt="user-img"  class=" img-thumbnail avatar-lg rounded-circle img-circle profile">
                <div class="dropdown">
                    <a href="/dashboard" class="text-dark dropdown-toggle h5 mt-2 mb-1 d-block" data-toggle="dropdown">{{Auth::user()->name}}</a>

                </div>
            </div>
            <!--- Sidemenu -->
        @include('layouts.sales_manager_sidebar')
        <!-- End Sidebar -->

            <div class="clearfix"></div>

        </div>
        <!-- Sidebar -left -->

    </div>
    <!-- Left Sidebar End -->

    <!-- ============================================================== -->
    <!-- Start Page Content here -->
    <!-- ============================================================== -->

    <div class="content-page">
        <div class="content">

            <!-- Start Content-->
            <div class="container-fluid">

                <div class="row">
                    <div class="col-12">
                        <div class="card-box">

                            <a  href="{{URL::previous()}}" role="button" class="btn btn-danger waves-effect waves-light float-right" ><span><i  class="fa fa-arrow-left"></i> Get Back</span></a>

                            <button class="btn btn-success waves-effect waves-light float-right mx-3" data-toggle="modal" data-target=".bs-example-modal-lg"><span > <i  class="fa fa-comment"></i> Reply</span></button>
                            @foreach($ticket AS $value)
                                <div class="card-box task-detail" style="box-shadow: 0px 5px #2f3f4833;">
                                    <div class="media mb-3">

                                        <div class="media-body">
                                            <h4 class="media-heading mt-0">Client  Name :{{$value->tick->user->name}}</h4>
                                        </div>
                                    </div>

                                    <h4>Ticket Name : <span>{{$value->tick->ticket_name}}</span></h4>

                                    <h5 class="text-warning">Added By {{$value->user->name}}</h5>
                                    <p >
                                        Comments: {{$value->solution}}
                                    </p>

                                    <div class="row task-dates mb-0 mt-2">
                                        <div class="col-lg-6">
                                            <h5 class="font-600 m-b-5">Created Date</h5>
                                            <p> {{$value->tick->created_at}} </p>
                                        </div>

                                        <div class="col-lg-6">
                                            <h5 class="font-600 m-b-5">Reply Date</h5>
                                            <p> {{$value->created_at}} </p>

                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
                                    <div class="modal-dialog modal-lg">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title" id="myLargeModalLabel">Add Comments</h4>
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            </div>
                                            <div class="modal-body">
                                                <div class="col-md-12">
                                                    <form class="form-horizontal" method="POST" action="/add/manage/ticket" enctype="multipart/form-data">
                                                        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                                        <div class="col-sm-12">
                                                            <label>Add Comments</label>
                                                            <input type="hidden" name="ticket_id" value="{{$value->tick->id}}">
                                                            <textarea class="form-control" name="solution" ></textarea>
                                                        </div>
                                                        <br>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <button type="submit" class="btn btn-primary">Post Reply</button>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                                </form>
                                            </div>
                                        </div><!-- /.modal-content -->
                                    </div><!-- /.modal-dialog -->
                                </div><!-- /.modal -->
                            @endforeach
                        </div>
                    </div>
                </div>
                <!-- end row -->

            </div> <!-- container-fluid -->

        </div> <!-- content -->



    </div>

    <!-- ============================================================== -->
    <!-- End Page content -->
    <!-- ============================================================== -->


</div>
<!-- END wrapper -->


<!-- Right bar overlay-->
<div class="rightbar-overlay"></div>

<!-- Vendor js -->
<script src="/dash/assets/js/vendor.min.js"></script>

<!-- third party js -->
<script src="/dash/assets/libs/datatables/jquery.dataTables.min.js"></script>
<script src="/dash/assets/libs/datatables/dataTables.bootstrap4.js"></script>
<script src="/dash/assets/libs/datatables/dataTables.responsive.min.js"></script>
<script src="/dash/assets/libs/datatables/responsive.bootstrap4.min.js"></script>
<script src="/dash/assets/libs/datatables/dataTables.buttons.min.js"></script>
<script src="/dash/assets/libs/datatables/buttons.bootstrap4.min.js"></script>
<script src="/dash/assets/libs/datatables/buttons.html5.min.js"></script>
<script src="/dash/assets/libs/datatables/buttons.flash.min.js"></script>
<script src="/dash/assets/libs/datatables/buttons.print.min.js"></script>
<script src="/dash/assets/libs/datatables/dataTables.keyTable.min.js"></script>
<script src="/dash/assets/libs/datatables/dataTables.select.min.js"></script>
<script src="/dash/assets/libs/pdfmake/pdfmake.min.js"></script>
<script src="/dash/assets/libs/pdfmake/vfs_fonts.js"></script>
<!-- third party js ends -->

<!-- Datatables init -->
<script src="/dash/assets/js/pages/datatables.init.js"></script>

<!-- App js -->
<script src="/dash/assets/js/app.min.js"></script>
<script src="/dash/assets/js/initial.js-master/initial.js" type="text/javascript"></script>

<script>
    $('.profile').initial();
</script>
</body>
</html>