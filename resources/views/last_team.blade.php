<!DOCTYPE html>
<html lang="en">
@include('layouts.web_head')

<body>
<!-- Main header -->
@include('layouts.web_header')
<!-- End of Main header -->
<!-- Page Title -->
<section class="page-title-wrap">
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="page-title" >
                    <h2>Our Team </h2>
                    <ul class="list-unstyled m-0 d-flex">
                        <li><a href="/"><i class="fa fa-home"></i> Home</a></li>
                        <li><a href="/team">Team</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End of Page Title -->

<!-- Abut Us -->
<section class="pb-55">
    <div class="container">
        <div class="row py-5 my-3">
            <div class="col-lg-6">
                <img src="/landing/img/team/team.jpg" class="img img-responsive">
            </div>
            <div class="col-lg-6">
                <h2 class="text-blue">Our Amazing Team</h2>

                <p>We are a dedicated team of professionals at the service of our customers and community. Through relentless efforts, we are continuously solving connectivity challenges.</p>
                <div class="team-about">
                    <p>
                        “Our strength lies in our collaborative spirit,<span class="text-green">we believe nothing is impossible.</span>”
                    </p>
                </div>
                <div class="team-about">
                    <p>
                        “We are <span class="text-green">passionate</span> about delivering an <span class="text-green">exceptional</span> experience.”
                    </p>
                </div>
                <h2 class="text-blue">Tuko Pamoja<span class="text-yellow"> ! </span></h2>
            </div>
        </div>
        <div class="row align-items-center pb-4">
            @foreach($team as $value)
                <div class="col-sm-6 col-md-3 col-lg-3 card-custom-bottom">
                    <div class="card card-10">
                        <img src="/landing/img/team/team1.jpg" class="img img-responsive">
                        <div class="card-name">
                            <span class="name">{{$value->team_name}}</span>
                            <span class="title">{{$value->team_title}}</span>
                        </div>
                        <div class="card-icons">
                            <span class="email-txt">{{$value->team_email}}</span>
                        </div>
                    </div>
                </div>
            @endforeach
            {{--<div class="col-sm-6 col-md-3 col-lg-3 card-custom-bottom">--}}
            {{--<div class="card card-10">--}}
            {{--<img src="/landing/img/team/team2.jpg" class="img img-responsive">--}}
            {{--<div class="card-name">--}}
            {{--<span class="name">Selena Souah</span>--}}
            {{--<span class="title">Co-Founder/Chairman</span>--}}
            {{--</div>--}}
            {{--<div class="card-icons">--}}
            {{--<span class="email-txt">selena.souah@revair.rw</span>--}}
            {{--</div>--}}
            {{--</div>--}}
            {{--</div>--}}
            {{--<div class="col-sm-6 col-md-3 col-lg-3 card-custom-bottom">--}}
            {{--<div class="card card-10">--}}
            {{--<img src="/landing/img/team/team6.jpg" class="img img-responsive">--}}
            {{--<div class="card-name">--}}
            {{--<span class="name">Jean Pierre K. Ruzindana</span>--}}
            {{--<span class="title">Chief Technical Officer</span>--}}
            {{--</div>--}}
            {{--<div class="card-icons">--}}
            {{--<span class="email-txt">jeanpierre.kamanzi@revair.rw</span>--}}
            {{--</div>--}}
            {{--</div>--}}
            {{--</div>--}}
            {{--<div class="col-sm-6 col-md-3 col-lg-3 card-custom-bottom">--}}
            {{--<div class="card card-10">--}}
            {{--<img src="/landing/img/team/team5.jpg" class="img img-responsive">--}}
            {{--<div class="card-name">--}}
            {{--<span class="name">Ariane Rugumaho</span>--}}
            {{--<span class="title">Director of Finance</span>--}}
            {{--</div>--}}
            {{--<div class="card-icons">--}}
            {{--<span class="email-txt">ariane.rugumaho@revair.rw</span>--}}
            {{--</div>--}}
            {{--</div>--}}
            {{--</div>--}}
        </div>
    </div>
</section>
<!-- End of Abut Us -->

<!-- Footer -->
@include('layouts.web_footer')
<!-- End of Footer -->

<!-- Back to top -->
<div class="back-to-top">
    <a href="/#"> <i class="fa fa-chevron-up"></i></a>
</div>

@include('layouts.web_js')
</body>
</html>