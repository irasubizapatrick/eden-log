<!DOCTYPE html>
<html lang="en">
@include('layouts.web_head')

<body>
<!-- Main header -->
@include('layouts.web_header')
<!-- End of Main header -->

<!-- Page Title -->
<section class="page-title-wrap">
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="page-title" >
                    <h2>Contact</h2>
                    <ul class="list-unstyled m-0 d-flex">
                        <li><a href="/"><i class="fa fa-home"></i> Home</a></li>
                        <li><a href="/contact">Contact</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End of Page Title -->

<!-- Contacts -->
<section class="pt-120 pb-120">
    <div class="container">
        <div class="row">
            <div class="col-lg-8">
                <!-- Map -->
                <div class="map"  data-trigger="map" data-map-options='{"latitude": "-1.946808", "longitude": "30.127850", "zoom": "16", "api_key": "AIzaSyCjkssBA3hMeFtClgslO2clWFR6bRraGz0"}'></div>
            </div>
            <div class="col-lg-4">
                <!-- Map description -->
                <div class="map-description light-bg d-flex align-items-center" >
                    <p>" Committed to connect Africa with Internet solutions backed by a dedicated team of professionals in order to satisfy the growing needs of connectivity. "</p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4">
                <div class="page-contacts-widget-wrapper light-bg d-flex align-items-center" >
                    <!-- Contact Info -->
                    <div class="page-contacts-widget">
                        <h3 class="h4">Contact Us</h3>
                        <div class="contact-widget-content">
                            <p>KG 11 Avenue, Gasabo Kigali, Rwanda, Triumph Building</p>
                            <ul class="list-unstyled">
                                <li>
                                    <i class="fa fa-phone"></i>
                                    <a href="tel:+1234567890">(+250) 788-129-500</a>
                                </li>
                                <li>
                                    <i class="fa fa-envelope-o"></i>
                                    <a href="mailto:info@revair.rw">info@revair.rw</a>
                                </li>
                                <li>
                                    <i class="fa fa-map-marker"></i>
                                    <span>KG 11 Avenue, Gasabo Kigali, Rwanda, Triumph Building</span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-8">
                <!-- Contact Form -->
                <div class="contact-form parsley-validate-wrap mt-60" >
                    <h3 class="bordered-title">Get In Touch</h3>
                    <div class="form-response"></div>
                    <form method="post" action="sendmail.php">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-field">
                                    <input type="text" name="name" class="theme-input-style" placeholder="Your Name" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-field">
                                    <input type="email" name="email" class="theme-input-style" placeholder="E-mail address" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-field">
                                    <input type="text" name="phone" class="theme-input-style" placeholder="Telephone" data-parsley-pattern="^[\d\+\-\.\(\)\/\s]*$">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-field">
                                    <input type="text" name="subject" class="theme-input-style" placeholder="Subject">
                                </div>
                            </div>
                        </div>
                        <div class="form-field">
                            <textarea name="message" class="theme-input-style" placeholder="Write your message" required></textarea>
                        </div>
                        <button type="submit" class="btn">Send Message</button>
                    </form>
                </div>
                <!-- End of Contact Form -->
            </div>
        </div>
    </div>
</section>
<!-- End of Contacts -->

<!-- Footer -->
@include('layouts.web_footer')
<!-- End of Footer -->

<!-- Back to top -->
<div class="back-to-top">
    <a href="/#"> <i class="fa fa-chevron-up"></i></a>
</div>

@include('layouts.web_js')
</body>
</html>