<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    @include('layouts.dash_title')
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description" />
    <meta content="Revolution Air " name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <!-- App favicon -->
    <link rel="shortcut icon" type="image/png" href="/dash/img/logo.png">

    <!-- third party css -->
    <link href="/dash/assets/libs/datatables/dataTables.bootstrap4.css" rel="stylesheet" type="text/css" />
    <link href="/dash/assets/libs/datatables/responsive.bootstrap4.css" rel="stylesheet" type="text/css" />
    <link href="/dash/assets/libs/datatables/buttons.bootstrap4.css" rel="stylesheet" type="text/css" />
    <link href="/dash/assets/libs/datatables/select.bootstrap4.css" rel="stylesheet" type="text/css" />
    <!-- third party css end -->


    <!-- App css -->
    <link href="/dash/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="/dash/assets/css/icons.min.css" rel="stylesheet" type="text/css" />
    <link href="/dash/assets/css/app.min.css" rel="stylesheet" type="text/css" />

</head>

<body>

<!-- Begin page -->
<div id="wrapper">

    <!-- Topbar Start -->
    <div class="navbar-custom">
        <ul class="list-unstyled topnav-menu float-right mb-0">
            <li class="dropdown notification-list">
                <a class="nav-link dropdown-toggle nav-user mr-0 waves-effect" data-toggle="dropdown" href="/dashboard" role="button" aria-haspopup="false" aria-expanded="false">
                    <img src="{{Auth::user()->name}}" alt="user-image" class="rounded-circle profile">
                    <span class="pro-user-name ml-1">
                                {{Auth::user()->email}} <i class="mdi mdi-chevron-down"></i>
                            </span>
                </a>
                <div class="dropdown-menu dropdown-menu-right profile-dropdown ">
                    <!-- item-->
                    <div class="dropdown-header noti-title">
                        <h6 class="text-overflow m-0">Welcome !</h6>
                    </div>

                    <!-- item-->
                    <a href="javascript:void(0);" class="dropdown-item notify-item">
                        <i class="fe-user"></i>
                        <span>My Account</span>
                    </a>

                    <!-- item-->
                    <a href="javascript:void(0);" class="dropdown-item notify-item">
                        <i class="fe-settings"></i>
                        <span>Settings</span>
                    </a>

                    <div class="dropdown-divider"></div>

                    <!-- item-->

                    <a  href="{{ url('/logout') }}"onclick="event.preventDefault();document.getElementById('logout-form').submit();" class="mega-menu dropdown-item notify-item" data-close="true">
                        <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                        </form>
                        <i class="fe-log-out"></i>
                        <span>Logout</span>
                    </a>

                </div>
            </li>

        </ul>


        <!-- LOGO -->
        <div class="logo-box">
            <a href="/dashboard" class="logo text-center">
                <span class="logo-lg">
                    <img src="/dash/assets/images/logo.png" alt="">
                    <!-- <span class="logo-lg-text-light">Xeria</span> -->
                </span>
                <span class="logo-sm">
                    <!-- <span class="logo-sm-text-dark">X</span> -->
                    <img src="/dash/assets/images/logo-sm.png" alt="" height="24">
                </span>
            </a>
        </div>

        <ul class="list-unstyled topnav-menu topnav-menu-left m-0">
            <li>
                <button class="button-menu-mobile disable-btn waves-effect">
                    <i class="fe-menu"></i>
                </button>
            </li>

            <li>
                <h4 class="page-title-main">FQS </h4>
            </li>

        </ul>
    </div>
    <!-- end Topbar -->

    <!-- ========== Left Sidebar Start ========== -->
    <div class="left-side-menu">

        <div class="slimscroll-menu">

            <div class="user-box text-center">
                <img src="{{Auth::user()->email}}" alt="user-img"  class=" img-thumbnail avatar-lg rounded-circle img-circle profile">
                <div class="dropdown">
                    <a href="/dashboard" class="text-dark dropdown-toggle h5 mt-2 mb-1 d-block" data-toggle="dropdown">{{Auth::user()->name}}</a>

                </div>
            </div>
            <!--- Sidemenu -->
        @include('layouts.dash_sidebar')
        <!-- End Sidebar -->

            <div class="clearfix"></div>

        </div>
        <!-- Sidebar -left -->

    </div>
    <!-- Left Sidebar End -->

    <!-- ============================================================== -->
    <!-- Start Page Content here -->
    <!-- ============================================================== -->

    <div class="content-page">
        <div class="content">

            <!-- Start Content-->
            <div class="container-fluid">

                <div class="row">
                    <div class="col-12">
                        <div class="card-box">
                            <h4 class="header-title ">FQS</h4>
                            <button class="btn btn-success waves-effect waves-light float-right" data-toggle="modal" data-target=".bs-example-modal-lg"><span class="fa fa-plus"></span>Add new</button>
                            <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title" id="myLargeModalLabel">Add FAQ</h4>
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                        </div>
                                        <div class="modal-body">
                                            <form class="form-horizontal" action="/fq" method="POST" enctype="multipart/form-data">
                                                <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label>FAQ Title </label>
                                                            <input type="text" class="form-control" name="fq_title" placeholder="Enter  Name " required value="" />
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label>FAQ Description </label>
                                                            <textarea class="form-control" name="fq_description" >

                                                            </textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <button type="submit" class="btn btn-success">Submit</button>
                                            </form>
                                        </div>
                                    </div><!-- /.modal-content -->
                                </div><!-- /.modal-dialog -->
                            </div><!-- /.modal -->
                            <div class="row">
                                <div class="col-md-6 mx-auto">
                                    @if (Session::has('message'))
                                        <div class="alert alert-success alert-dismissable">
                                            <a href="" class="close" data-dismiss="alert" aria-label="close">
                                                &times;

                                            </a>
                                            {{ Session::get('message') }}
                                        </div>

                                    @endif
                                    @if (Session::has('delete'))
                                        <div class="alert alert-success alert-dismissable">
                                            <a href="" class="close" data-dismiss="alert" aria-label="close">
                                                &times;

                                            </a>
                                            {{ Session::get('delete') }}
                                        </div>

                                    @endif
                                    @if (Session::has('updated'))
                                        <div class="alert alert-success alert-dismissable">
                                            <a href="" class="close" data-dismiss="alert" aria-label="close">
                                                &times;

                                            </a>
                                            {{ Session::get('updated') }}
                                        </div>

                                    @endif
                                </div>
                            </div>

                            <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap">
                                <thead>
                                <tr>
                                    <th class="d-flex">#</th>
                                    <th>Created by </th>
                                    <th>Title</th>
                                    <th>Description</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>

                                <?php $i=1;?>
                                @foreach($fq AS $value)
                                    <tr>
                                        <td class="d-flex">{{$i++}}</td>
                                        <td>{{$value->user->name}}</td>
                                        <td>{{$value->fq_title}}</td>
                                        <td>{{$value->fq_description}}</td>
                                        <td>
                                            <button type="button" data-toggle="modal" class="tabledit-edit-button btn btn-success" data-target="#editall<?php echo $i;?>" style="float: none;"><span class="mdi mdi-pencil"></span></button>
                                            <button type="button" data-toggle="modal" class="tabledit-edit-button btn btn-danger" data-target="#delete<?php echo $i;?>" style="float: none;"><span class="mdi mdi-trash-can"></span></button>

                                            <div class="modal fade" id="delete<?php echo $i;?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLarge01" style="display: none;" aria-hidden="true">
                                                <div class="modal-dialog modal-lg" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title">Delete </h5>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">×</span>
                                                            </button>
                                                        </div>
                                                        <form   class="form-horizontal mt-1" action="/fq/{{$value->id}}" method="POST">
                                                            <label class="mx-2">Are you sure you want to delete</label>
                                                            <input type="hidden" name="_method" value="DELETE" />
                                                            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                                            <div class="modal-footer">
                                                                <button type="submit" class="btn btn-success">Confirm</button>
                                                                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal fade" id="editall<?php echo $i;?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLarge01" style="display: none;" aria-hidden="true">
                                                <div class="modal-dialog modal-lg" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title">Edit Department </h5>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">×</span>
                                                            </button>
                                                        </div>
                                                        <form role="form-horizontal" action="/fq/{{$value->id}}" method="post" enctype="multipart/form-data">
                                                            <input type="hidden" name="_method" value="PUT" />
                                                            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                                            <div class="modal-body">
                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        <div class="form-group">
                                                                            <label>FAQ Title </label>
                                                                            <input type="text" value="{{$value->fq_title}}" class="form-control" name="fq_title" placeholder="Enter  Name " required value="" />
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-12">
                                                                        <div class="form-group">
                                                                            <label>FAQ Description </label>
                                                                            <textarea class="form-control" name="fq_description" >

                                                                            </textarea>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-12">
                                                                        <div class="form-group">
                                                                            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                                                            <button type="submit" class="btn btn-success">Save changes</button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>

                                        </td>
                                    </tr>

                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- end row -->

            </div> <!-- container-fluid -->

        </div> <!-- content -->



    </div>

    <!-- ============================================================== -->
    <!-- End Page content -->
    <!-- ============================================================== -->


</div>
<!-- END wrapper -->

<!-- Right Sidebar -->
<div class="right-bar">
    <div class="rightbar-title">
        <a href="javascript:void(0);" class="right-bar-toggle float-right">
            <i class="dripicons-cross noti-icon"></i>
        </a>
        <h5 class="m-0 text-white">Settings</h5>
    </div>
    <div class="slimscroll-menu">
        <!-- User box -->
        <div class="user-box">
            <div class="user-img">
                <img src="/dash/assets/images/users/user-1.jpg" alt="user-img" title="Mat Helme" class="rounded-circle img-fluid">
                <a href="javascript:void(0);" class="user-edit"><i class="mdi mdi-pencil"></i></a>
            </div>

            <h5><a href="javascript: void(0);">Username</a> </h5>
            <p class="text-muted mb-0"><small>Admin Head</small></p>
        </div>

        <!-- Settings -->
        <hr class="mt-0" />
        <h5 class="pl-3">Basic Settings</h5>
        <hr class="mb-0" />

        <div class="p-3">
            <div class="checkbox checkbox-primary mb-2">
                <input id="Rcheckbox1" type="checkbox" checked>
                <label for="Rcheckbox1">
                    Notifications
                </label>
            </div>
            <div class="checkbox checkbox-primary mb-2">
                <input id="Rcheckbox2" type="checkbox" checked>
                <label for="Rcheckbox2">
                    API Access
                </label>
            </div>
            <div class="checkbox checkbox-primary mb-2">
                <input id="Rcheckbox3" type="checkbox">
                <label for="Rcheckbox3">
                    Auto Updates
                </label>
            </div>
            <div class="checkbox checkbox-primary mb-2">
                <input id="Rcheckbox4" type="checkbox" checked>
                <label for="Rcheckbox4">
                    Online Status
                </label>
            </div>
            <div class="checkbox checkbox-primary mb-0">
                <input id="Rcheckbox5" type="checkbox" checked>
                <label for="Rcheckbox5">
                    Auto Payout
                </label>
            </div>
        </div>

        <!-- Timeline -->
        <hr class="mt-0" />
        <h5 class="pl-3 pr-3">Messages <span class="float-right badge badge-pill badge-danger">25</span></h5>
        <hr class="mb-0" />
        <div class="p-3">
            <div class="inbox-widget">
                <div class="inbox-item">
                    <div class="inbox-item-img"><img src="/dash/assets/images/users/user-2.jpg" class="rounded-circle" alt=""></div>
                    <p class="inbox-item-author"><a href="javascript: void(0);" class="text-dark">Tomaslau</a></p>
                    <p class="inbox-item-text">I've finished it! See you so...</p>
                </div>
                <div class="inbox-item">
                    <div class="inbox-item-img"><img src="/dash/assets/images/users/user-3.jpg" class="rounded-circle" alt=""></div>
                    <p class="inbox-item-author"><a href="javascript: void(0);" class="text-dark">Stillnotdavid</a></p>
                    <p class="inbox-item-text">This theme is awesome!</p>
                </div>
                <div class="inbox-item">
                    <div class="inbox-item-img"><img src="/dash/assets/images/users/user-4.jpg" class="rounded-circle" alt=""></div>
                    <p class="inbox-item-author"><a href="javascript: void(0);" class="text-dark">Kurafire</a></p>
                    <p class="inbox-item-text">Nice to meet you</p>
                </div>

                <div class="inbox-item">
                    <div class="inbox-item-img"><img src="/dash/assets/images/users/user-5.jpg" class="rounded-circle" alt=""></div>
                    <p class="inbox-item-author"><a href="javascript: void(0);" class="text-dark">Shahedk</a></p>
                    <p class="inbox-item-text">Hey! there I'm available...</p>
                </div>
                <div class="inbox-item">
                    <div class="inbox-item-img"><img src="/dash/assets/images/users/user-6.jpg" class="rounded-circle" alt=""></div>
                    <p class="inbox-item-author"><a href="javascript: void(0);" class="text-dark">Adhamdannaway</a></p>
                    <p class="inbox-item-text">This theme is awesome!</p>
                </div>
            </div> <!-- end inbox-widget -->
        </div> <!-- end .p-3-->

    </div> <!-- end slimscroll-menu-->
</div>
<!-- /Right-bar -->

<!-- Right bar overlay-->
<div class="rightbar-overlay"></div>

<!-- Vendor js -->
<script src="/dash/assets/js/vendor.min.js"></script>

<!-- third party js -->
<script src="/dash/assets/libs/datatables/jquery.dataTables.min.js"></script>
<script src="/dash/assets/libs/datatables/dataTables.bootstrap4.js"></script>
<script src="/dash/assets/libs/datatables/dataTables.responsive.min.js"></script>
<script src="/dash/assets/libs/datatables/responsive.bootstrap4.min.js"></script>
<script src="/dash/assets/libs/datatables/dataTables.buttons.min.js"></script>
<script src="/dash/assets/libs/datatables/buttons.bootstrap4.min.js"></script>
<script src="/dash/assets/libs/datatables/buttons.html5.min.js"></script>
<script src="/dash/assets/libs/datatables/buttons.flash.min.js"></script>
<script src="/dash/assets/libs/datatables/buttons.print.min.js"></script>
<script src="/dash/assets/libs/datatables/dataTables.keyTable.min.js"></script>
<script src="/dash/assets/libs/datatables/dataTables.select.min.js"></script>
<script src="/dash/assets/libs/pdfmake/pdfmake.min.js"></script>
<script src="/dash/assets/libs/pdfmake/vfs_fonts.js"></script>
<!-- third party js ends -->

<!-- Datatables init -->
<script src="/dash/assets/js/pages/datatables.init.js"></script>

<!-- App js -->
<script src="/dash/assets/js/app.min.js"></script>
<script src="/dash/assets/js/initial.js-master/initial.js" type="text/javascript"></script>

<script>
    $('.profile').initial();
</script>
</body>
</html>