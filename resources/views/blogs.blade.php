<!DOCTYPE html>
<html lang="en">
@include('layouts.web_head')

<body>
<!-- Main header -->
@include('layouts.web_header')
<!-- End of Main header -->

<!-- Page Title -->
<section class="page-title-wrap">
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="page-title" >
                    <h2>Blog</h2>
                    <ul class="list-unstyled m-0 d-flex">
                        <li><a href="/"><i class="fa fa-home"></i> Home</a></li>
                        <li><a href="/">Blog</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End of Page Title -->

<!-- Blog -->
<section class="pt-120 pb-65">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="blog">
                    <h2 class="py-5 pb-5 mb-5 text-center">Coming soon</h2>
                    <div class="row isotope">
                        <!-- Single Post -->
                        <!--<div class="col-lg-4 col-md-6">-->
                        <!--    <div class="single-news mb-55" >-->
                        <!--        <a class="tip" href="blog-3-columns.html#">News</a>-->
                        <!--        <img src="img/posts/post.jpg" data-rjs="2" alt="">-->
                        <!--        <h3 class="h5"><a href="blog-details.html">Everything you need to know to cut the cord and ditch cable to order now</a></h3>-->
                        <!--        <a href="blog-details.html">Continue Reading <i class="fa fa-angle-right"></i></a>-->
                        <!--    </div>-->
                        <!--</div>-->
                        <!-- End of Single Post -->

                        <!--<div class="col-lg-4 col-md-6">-->
                        <!--    <div class="single-news mb-55" >-->
                        <!--        <a class="tip" href="blog-3-columns.html#">News</a>-->
                        <!--        <img src="img/posts/post.jpg" data-rjs="2" alt="">-->
                        <!--        <h3 class="h5"><a href="blog-details.html">Why the FCC's latest net neutrality defense is hollow on the flow</a></h3>-->
                        <!--        <a href="blog-details.html">Continue Reading <i class="fa fa-angle-right"></i></a>-->
                        <!--    </div>-->
                        <!--</div>-->
                        <!--<div class="col-lg-4 col-md-6">-->
                        <!--    <div class="single-news mb-55" >-->
                        <!--        <a class="tip" href="blog-3-columns.html#">News</a>-->
                        <!--        <img src="img/posts/post.jpg" data-rjs="2" alt="">-->
                        <!--        <h3 class="h5"><a href="blog-details.html">Why the FCC's latest net neutrality defense is hollow on the flow</a></h3>-->
                        <!--        <a href="blog-details.html">Continue Reading <i class="fa fa-angle-right"></i></a>-->
                        <!--    </div>-->
                        <!--</div>-->
                        <!--<div class="col-lg-4 col-md-6">-->
                        <!--    <div class="single-news mb-55" >-->
                        <!--        <a class="tip" href="blog-3-columns.html#">News</a>-->
                        <!--        <img src="img/posts/post.jpg" data-rjs="2" alt="">-->
                        <!--        <h3 class="h5"><a href="blog-details.html">Three privacy tools that block your Internet provider from tracking your computer</a></h3>-->
                        <!--        <a href="blog-details.html">Continue Reading <i class="fa fa-angle-right"></i></a>-->
                        <!--    </div>-->
                        <!--</div>-->
                        <!--<div class="col-lg-4 col-md-6">-->
                        <!--    <div class="single-news mb-55" >-->
                        <!--        <a class="tip" href="blog-3-columns.html#">News</a>-->
                        <!--        <img src="img/posts/post.jpg" data-rjs="2" alt="">-->

                        <!--        <h3 class="h5"><a href="blog-details.html">Three privacy tools that block your Internet provider from tracking your computer</a></h3>-->
                        <!--        <a href="blog-details.html">Continue Reading <i class="fa fa-angle-right"></i></a>-->
                        <!--    </div>-->
                        <!--</div>-->
                        <!--<div class="col-lg-4 col-md-6">-->
                        <!--    <div class="single-news mb-55" >-->
                        <!--        <a class="tip" href="blog-3-columns.html#">News</a>-->
                        <!--        <img src="img/posts/post.jpg" data-rjs="2" alt="">-->

                        <!--        <h3 class="h5"><a href="blog-details.html">Powered Enterprise IT: Cloud implementation built for the future</a></h3>-->
                        <!--        <a href="blog-details.html">Continue Reading <i class="fa fa-angle-right"></i></a>-->
                        <!--    </div>-->
                        <!--</div>-->
                        <!--<div class="col-lg-4 col-md-6">-->
                        <!--    <div class="single-news mb-55" >-->
                        <!--        <a class="tip" href="blog-3-columns.html#">News</a>-->
                        <!--        <img src="img/posts/post.jpg" data-rjs="2" alt="">-->

                        <!--        <h3 class="h5"><a href="blog-details.html">Everything you need to know to cut the cord and ditch cable to order now</a></h3>-->
                        <!--        <a href="blog-details.html">Continue Reading <i class="fa fa-angle-right"></i></a>-->
                        <!--    </div>-->
                        <!--</div>-->
                        <!--<div class="col-lg-4 col-md-6">-->
                        <!--    <div class="single-news mb-55" >-->
                        <!--        <a class="tip" href="blog-3-columns.html#">News</a>-->
                        <!--        <img src="img/posts/post.jpg" data-rjs="2" alt="">-->

                        <!--        <h3 class="h5"><a href="blog-details.html">Powered Enterprise IT: Cloud implementation built for the future</a></h3>-->
                        <!--        <a href="blog-details.html">Continue Reading <i class="fa fa-angle-right"></i></a>-->
                        <!--    </div>-->
                        <!--</div>-->
                        <!--<div class="col-lg-4 col-md-6">-->
                        <!--    <div class="single-news mb-55" >-->
                        <!--        <a class="tip" href="blog-3-columns.html#">News</a>-->
                        <!--        <img src="img/posts/post.jpg" data-rjs="2" alt="">-->

                        <!--        <h3 class="h5"><a href="blog-details.html">Three privacy tools that block your Internet provider from tracking your computer</a></h3>-->
                        <!--        <a href="blog-details.html">Continue Reading <i class="fa fa-angle-right"></i></a>-->
                        <!--    </div>-->
                        <!--</div>-->
                        <!--<div class="col-lg-4 col-md-6">-->
                        <!--    <div class="single-news mb-55" >-->
                        <!--        <a class="tip" href="blog-3-columns.html#">News</a>-->
                        <!--        <img src="img/posts/post.jpg" data-rjs="2" alt="">-->

                        <!--        <h3 class="h5"><a href="blog-details.html">Three privacy tools that block your Internet provider from tracking your computer</a></h3>-->
                        <!--        <a href="blog-details.html">Continue Reading <i class="fa fa-angle-right"></i></a>-->
                        <!--    </div>-->
                        <!--</div>-->
                        <!--<div class="col-lg-4 col-md-6">-->
                        <!--    <div class="single-news mb-55" >-->
                        <!--        <a class="tip" href="blog-3-columns.html#">News</a>-->
                        <!--        <img src="img/posts/post.jpg" data-rjs="2" alt="">-->

                        <!--        <h3 class="h5"><a href="blog-details.html">Three privacy tools that block your Internet provider from tracking your computer</a></h3>-->
                        <!--        <a href="blog-details.html">Continue Reading <i class="fa fa-angle-right"></i></a>-->
                        <!--    </div>-->
                        <!--</div>-->
                        <!--<div class="col-lg-4 col-md-6">-->
                        <!--    <div class="single-news mb-55" >-->
                        <!--        <a class="tip" href="blog-3-columns.html#">News</a>-->
                        <!--        <img src="img/posts/post.jpg" data-rjs="2" alt="">-->
                        <!--        <h3 class="h5"><a href="blog-details.html">Powered Enterprise IT: Cloud implementation built for the future</a></h3>-->
                        <!--        <a href="blog-details.html">Continue Reading <i class="fa fa-angle-right"></i></a>-->
                        <!--    </div>-->
                        <!--</div>-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End of Blog -->

<!-- Footer -->
@include('layouts.web_footer')
<!-- End of Footer -->

<!-- Back to top -->
<div class="back-to-top">
    <a href="/#"> <i class="fa fa-chevron-up"></i></a>
</div>

@include('layouts.web_js')
</body>
</html>