<!DOCTYPE html>
<html lang="en">
@include('layouts.web_head')

<body>
<!-- Main header -->
@include('layouts.web_header')
<!-- End of Main header -->
<!-- Page Title -->
<section class="page-title-wrap">
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="page-title" >
                    <h2>About Us</h2>
                    <ul class="list-unstyled m-0 d-flex">
                        <li><a href="/"><i class="fa fa-home"></i> Home</a></li>
                        <li><a href="/about">About us</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End of Page Title -->

<!-- Abut Us -->
<section class="pt-120 pb-55">
    <div class="container">
        <div class="row align-items-center pb-4">
            <div class="col-lg-6">
                <div class="text-center" >
                    <img src="/landing/img/team/both.jpg" alt="" data-rjs="2">
                </div>
            </div>
            <div class="col-lg-6 pt-5">
                <div class="number-one-content" data-animate="fadeInUp" data-delay=".3">
                    <h2 class="mb-3">Revolution Air Internet is Here To  Get You Connected.</h2>
                    <blockquote style="font-size: 1rem;line-height: 1.5;font-style: italic;color: #202e39;margin: 0;">
                        <p class="about-content">“
                            It is a fact!<br>
                            Africa is the second most populated continent, with the youngest population and yet it is by far the least connected continent.<br>
                            The Internet Revolution has brought unprecedented economic and social opportunities worldwide.<br>
                            After spending more than a decade overseas, we have set to build a<br> Pan African Broadband Internet Network that will enable people on our continent to benefit from the internet.”<br></p>
                        <h4>Feissal & Selena</h4>
                    </blockquote>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="single-about-us" >
                    <h3 class="h4">Our Mission</h3>
                    <p>To provide innovative and relevant solutions to connect our customers efficiently</p>
                </div>
            </div>
            <div class="col-md-4">
                <div class="single-about-us" >
                    <h3 class="h4">Our Vision</h3>
                    <p>To create a strong, secure and reliable pan African broadband Internet network.</p>
                </div>
            </div>
            <div class="col-md-4">
                <div class="single-about-us" >
                    <h3 class="h4">Our Values</h3>
                    <li>Excellence</li>
                    <li>Integrity</li>
                    <li>Technological Innovation</li>
                    <li>Passion</li>
                    <li>Teamwork</li>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End of Abut Us -->
<!-- Footer -->
@include('layouts.web_footer')
<!-- End of Footer -->

<!-- Back to top -->
<div class="back-to-top">
    <a href="index.html#"> <i class="fa fa-chevron-up"></i></a>
</div>

@include('layouts.web_js')
</body>
</html>