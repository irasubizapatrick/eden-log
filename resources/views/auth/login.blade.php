{{--@extends('layouts.app')--}}

{{--@section('content')--}}
{{--<div class="container">--}}
    {{--<div class="row">--}}
        {{--<div class="col-md-8 col-md-offset-2">--}}
            {{--<div class="panel panel-default">--}}
                {{--<div class="panel-heading">Login</div>--}}

                {{--<div class="panel-body">--}}
                    {{--<form class="form-horizontal" method="POST" action="{{ route('login') }}">--}}
                        {{--{{ csrf_field() }}--}}

                        {{--<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">--}}
                            {{--<label for="email" class="col-md-4 control-label">E-Mail Address</label>--}}

                            {{--<div class="col-md-6">--}}
                                {{--<input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>--}}

                                {{--@if ($errors->has('email'))--}}
                                    {{--<span class="help-block">--}}
                                        {{--<strong>{{ $errors->first('email') }}</strong>--}}
                                    {{--</span>--}}
                                {{--@endif--}}
                            {{--</div>--}}
                        {{--</div>--}}

                        {{--<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">--}}
                            {{--<label for="password" class="col-md-4 control-label">Password</label>--}}

                            {{--<div class="col-md-6">--}}
                                {{--<input id="password" type="password" class="form-control" name="password" required>--}}

                                {{--@if ($errors->has('password'))--}}
                                    {{--<span class="help-block">--}}
                                        {{--<strong>{{ $errors->first('password') }}</strong>--}}
                                    {{--</span>--}}
                                {{--@endif--}}
                            {{--</div>--}}
                        {{--</div>--}}

                        {{--<div class="form-group">--}}
                            {{--<div class="col-md-6 col-md-offset-4">--}}
                                {{--<div class="checkbox">--}}
                                    {{--<label>--}}
                                        {{--<input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me--}}
                                    {{--</label>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}

                        {{--<div class="form-group">--}}
                            {{--<div class="col-md-8 col-md-offset-4">--}}
                                {{--<button type="submit" class="btn btn-primary">--}}
                                    {{--Login--}}
                                {{--</button>--}}

                                {{--<a class="btn btn-link" href="{{ route('password.request') }}">--}}
                                    {{--Forgot Your Password?--}}
                                {{--</a>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</form>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
{{--</div>--}}
{{--@endsection--}}

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Document Title -->
    <title>Revolution Air | Internet</title>
    <!-- Favicon -->
    <link rel="shortcut icon" type="image/png" href="/landing/img/logo.png">
    <!-- CSS Files -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Rubik:400,500,700%7CSource+Sans+Pro:300i,400,400i,600,700">
    <link rel="stylesheet" href="/landing/css/bootstrap.min.css">
    <link rel="stylesheet" href="/landing/css/font-awesome.min.css">
    <link rel="stylesheet" href="/landing/plugins/swiper/swiper.min.css">
    <link rel="stylesheet" href="/landing/css/style.css">
    <link rel="stylesheet" href="/landing/css/responsive.css">
    <link rel="stylesheet" href="/landing/css/custom.css">
</head>

<style type="text/css">
    .note
    {
        text-align: center;
        height: 80px;
        background: #3377bc;
        color: #fff;
        font-weight: 500;
        line-height: 80px;
        font-size: 27px;
        -webkit-font-smoothing: antialiased;

    }
    .form-content
    {
        padding: 5%;
        border: 1px solid #ced4da;
        margin-bottom: 2%;
    }
    .form-control{
        border-radius:0px ;
    }
    .btnSubmit
    {
        border:none;
        border-radius:1.5rem;
        padding: 1%;
        width: 20%;
        cursor: pointer;
        background: #01a86c;
        color: #fff;
    }
</style>
<body>
<!-- Main header -->
<header class="header">
    <div class="header-top">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-6 col-md-7">
                    <div class="header-info text-center text-md-left">
                        <span>Welcome to Revolution Air Ltd</span>
                    </div>
                </div>
                <div class="col-lg-6 col-md-5">
                    <div class="header-top-right d-flex align-items-center justify-content-center justify-content-md-end">
                        <form class="parsley-validate d-flex position-relative" action="#">
                            <input type="text" placeholder="I am looking for" required>
                            <button type="submit"><i class="fa fa-search"></i></button>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="main-header" style="box-shadow: 1px 1px #0000001c;">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-4 col-9">
                    <!-- Logo -->
                    <div class="logo">
                        <a href="/">
                            <img src="/landing/img/logo.png" data-rjs="2" alt="REVAIR">
                        </a>
                    </div>
                </div>
                <div class="col-xl-8 col-lg-8 col-md-5 col-sm-2 col-3">
                    <nav>
                        <!-- Header-menu -->
                        <div class="header-menu">
                            <ul>
                                <li class="active"><a href="/">Home</a></li>
                                <li><a href="/about">About</a></li>
                                <li><a href="/#package-list">Products/Services</a></li>
                                <li><a href="/team">Our Team</a></li>
                                <li><a href="/#faq">Our FAQ's</a></li>
                                <li><a href="/blog">Our Blog</a></li>
                                <li><a href="/contact">Contact</a></li>
                            </ul>
                        </div>
                        <!-- End of Header-menu -->
                    </nav>
                </div>
                <div class="col-xl-2 col-lg-2 col-md-4 col-sm-6 d-none d-sm-block">
                    <!-- Header Call -->
                    <div class="header-call text-right">
                        <span>Call Now</span>
                        <a href="tel:+1234567890">(+250) 784-807-640</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
<!-- End of Main header -->

<!-- Abut Us -->
<section class="pt-55 pb-55">
    <div class="container register-form">
        <div class="col-md-6 mx-auto">
            <div class="form">
                <div class="note">
                    Login
                </div>
                <div class="form-content">
                    <div class="success ">
                        @if (Session::has('message'))
                            <div class="alert alert-success alert-dismissable">
                                <a href="" class="close" data-dismiss="alert" aria-label="close">
                                    &times;

                                </a>
                                {{ Session::get('message') }}
                            </div>

                        @endif
                        @if (Session::has('delete'))
                            <div class="alert alert-success alert-dismissable">
                                <a href="" class="close" data-dismiss="alert" aria-label="close">
                                    &times;

                                </a>
                                {{ Session::get('delete') }}
                            </div>

                        @endif
                        @if (Session::has('updated'))
                            <div class="alert alert-success alert-dismissable">
                                <a href="" class="close" data-dismiss="alert" aria-label="close">
                                    &times;

                                </a>
                                {{ Session::get('updated') }}
                            </div>

                        @endif
                    </div>
                    <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                                    <label>Email</label>
                                    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="example@gmail.com" required autofocus>

                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                        <strong style="color: red;">{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                                    <label>Password</label>
                                    <input id="password" type="password" class="form-control" name="password" placeholder="**********" required>

                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                        <strong style="color: red;">{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <button type="submit" style="border-radius: unset !important;" class="btnSubmit">Login</button>
                    </form>
                </div>
            </div>
        </div>

    </div>
</section>
<!-- End of About Us -->
<!-- Footer -->
@include('layouts.web_footer')
<!-- End of Footer -->

<!-- Back to top -->
<div class="back-to-top">
    <a href="index.html#"> <i class="fa fa-chevron-up"></i></a>
</div>

<!-- JS Files -->
<script src="/landing/js/jquery-3.3.1.min.js"></script>
<script src="/landing/js/bootstrap.bundle.min.js"></script>
<script src="/landing/plugins/waypoints/jquery.waypoints.min.js"></script>
<script src="/landing/plugins/waypoints/sticky.min.js"></script>
<script src="/landing/plugins/swiper/swiper.min.js"></script>
<script src="/landing/plugins/magnific-popup/jquery.magnific-popup.min.js"></script>
<script src="/landing/plugins/parsley/parsley.min.js"></script>
<script src="/landing/plugins/retinajs/retina.min.js"></script>
<script src="/landing/plugins/isotope/isotope.pkgd.min.js"></script>
<script src="/landing/js/menu.min.js"></script>
<script src="/landing/js/scripts.js"></script>
<script src="/landing/js/custom.js"></script>
</body>
</html>