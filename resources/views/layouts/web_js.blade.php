<!-- JS Files -->
<script src="/landing/js/jquery-3.3.1.min.js"></script>
<script src="/landing/js/bootstrap.bundle.min.js"></script>
<script src="/landing/plugins/waypoints/jquery.waypoints.min.js"></script>
<script src="/landing/plugins/waypoints/sticky.min.js"></script>
<script src="/landing/plugins/swiper/swiper.min.js"></script>
<script src="/landing/plugins/magnific-popup/jquery.magnific-popup.min.js"></script>
<script src="/landing/plugins/parsley/parsley.min.js"></script>
<script src="/landing/plugins/retinajs/retina.min.js"></script>
<script src="/landing/plugins/isotope/isotope.pkgd.min.js"></script>
<script src="/landing/js/menu.min.js"></script>
<script src="/landing/js/scripts.js"></script>
<script src="/landing/js/custom.js"></script>
<script type="text/javascript">
    $.fn.carousel.Constructor.prototype.cycle = function (event) {
        if (!event) {
            this._isPaused = false;
        }

        if (this._interval) {
            clearInterval(this._interval)
            this._interval = null;
        }

        if (this._config.interval && !this._isPaused) {

            var $ele = $('.carousel-item-next');
            var newInterval = $ele.data('interval') || this._config.interval;
            this._interval = setInterval(
                (document.visibilityState ? this.nextWhenVisible : this.next).bind(this),
                newInterval
            );
        }
    };
</script>