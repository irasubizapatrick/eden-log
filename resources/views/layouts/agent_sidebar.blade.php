<div id="sidebar-menu">

    <ul class="metismenu" id="side-menu">

        <li>
            <a href="/dashboard">
                <i class="fa fa-home"></i>
                <span> Dashboard </span>
            </a>
        </li>
        <li>
            <a href="/leads">
                <i class="fa fa-users"></i>
                <span> My Leads </span>
            </a>
        </li>
        <li>
            <a href="#">
                <i class="fa fa-file"></i>
                <span> Document </span>
            </a>
        </li>
    </ul>

</div>