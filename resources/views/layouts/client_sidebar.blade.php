<div id="sidebar-menu">

    <ul class="metismenu" id="side-menu">

        <li>
            <a href="/dashboard">
                <i class="fa fa-home"></i>
                <span> Dashboard </span>
            </a>
        </li>
        <li class="">
            <a href="javascript: void(0);" aria-expanded="false">
                <i class="mdi mdi-file-account"></i>
                <span> Tickets </span>
                <span class="menu-arrow"></span>
            </a>
            <ul class="nav-second-level collapse" aria-expanded="false" style="height: 0px;">
                <li><a href="/manage/ticket">List</a></li>
            </ul>
        </li>


        <li>
            <a href="/my/invoice">
                <i class="fa fa-file"></i>
                <span>My  Invoices </span>
            </a>
        </li>
    </ul>

</div>