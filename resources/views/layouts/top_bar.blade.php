<div class="navbar-custom">
    <ul class="list-unstyled topnav-menu float-right mb-0">
        <li class="dropdown notification-list">
            <a class="nav-link dropdown-toggle nav-user mr-0 waves-effect" data-toggle="dropdown" href="/dashboard" role="button" aria-haspopup="false" aria-expanded="false">
                <img src="{{Auth::user()->name}}" alt="user-image" class="rounded-circle profile">
                <span class="pro-user-name ml-1">
                                {{Auth::user()->email}} <i class="mdi mdi-chevron-down"></i>
                            </span>
            </a>
            <div class="dropdown-menu dropdown-menu-right profile-dropdown ">
                <!-- item-->
                <div class="dropdown-header noti-title">
                    <h6 class="text-overflow m-0">Welcome !</h6>
                </div>


                <!-- item-->
                <a href="/change_password" class="dropdown-item notify-item">
                    <i class="fe-settings"></i>
                    <span>Settings</span>
                </a>

                <div class="dropdown-divider"></div>

                <!-- item-->

                <a  href="{{ url('/logout') }}"onclick="event.preventDefault();document.getElementById('logout-form').submit();" class="mega-menu dropdown-item notify-item" data-close="true">
                    <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                    </form>
                    <i class="fe-log-out"></i>
                    <span>Logout</span>
                </a>

            </div>
        </li>




    </ul>


    <!-- LOGO -->
    <div class="logo-box">
        <a href="/dashboard" class="logo text-center">
                        <span class="logo-lg">
                            <img src="/dash/assets/images/logo.png" alt="">
                            <!-- <span class="logo-lg-text-light">Xeria</span> -->
                        </span>
            <span class="logo-sm">
                            <!-- <span class="logo-sm-text-dark">X</span> -->
                            <img src="/dash/assets/images/logo-sm.png" alt="" height="24">
                        </span>
        </a>
    </div>

    <ul class="list-unstyled topnav-menu topnav-menu-left m-0">
        <li>
            <button class="button-menu-mobile disable-btn waves-effect">
                <i class="fe-menu"></i>
            </button>
        </li>

        <li>
            <h4 class="page-title-main">Revair CRM</h4>
        </li>

    </ul>
</div>