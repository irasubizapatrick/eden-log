<footer class="main-footer">
    <div class="footer-widgets border-top pt-80 pb-50">
        <div class="container">
            <div class="row">
                <!-- Footer Widget -->
                <div class="col-lg-6 col-sm-6">
                    <div class="footer-widget mb-30">
                        <h3 class="h4">Contact Us</h3>
                        <div class="contact-widget-content">
                            <p>KG 11 Avenue,<br>  Gasabo Kigali, Rwanda</p>
                            <ul class="list-unstyled">
                                <li>
                                    <i class="fa fa-phone"></i>
                                    <a href="tel:+1234567890">(+250) 788-129-500 </a>
                                </li>
                                <li>
                                    <i class="fa fa-envelope-o"></i>
                                    <a href="mailto:info@revair.rw">info@revair.rw</a>
                                </li>
                                <li>
                                    <i class="fa fa-map-marker"></i>
                                    <span>KG 11 Avenue, Gasabo Kigali, Rwanda, Triumph Building</span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- End of Footer Widget -->

                <!-- Footer Widget -->
                <div class="col-lg-4 col-sm-6">
                    <div class="footer-widget mb-30">
                        <h3 class="h4">Company</h3>
                        <div class="menu-wrap">
                            <ul class="menu">
                                <li><a href="/about">About Us</a></li>
                                <li><a href="/team">Our Team</a></li>
                                <li><a href="#">Privacy</a></li>
                                <li><a href="#">Terms & condition</a></li>
                                <li><a href="/login">Customer Account</a> </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- End of Footer Widget -->

                <!-- Footer Widget -->
                {{--<div class="col-lg-3 col-sm-6">--}}
                    {{--<div class="footer-widget mb-30">--}}
                        {{--<h3 class="h4">Support Links</h3>--}}
                        {{--<div class="menu-wrap">--}}
                            {{--<ul class="menu">--}}
                                {{--<li><a href="#">Help</a></li>--}}
                                {{--<li><a href="#">support</a></li>--}}
                                {{--<li><a href="#">Client</a></li>--}}
                                {{--<li><a href="/customer/registration">Request site Survey</a></li>--}}

                            {{--</ul>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
                <!-- End of Footer Widget -->

                <!-- Footer Widget -->
                <div class="col-lg-2 col-sm-6">
                    <div class="footer-widget mb-30">
                        <h3 class="h4">Let's Connect</h3>
                        <div class="menu-wrap contact-widget-content">
                            <ul class="list-unstyled">
                                <li>
                                    <i class="fa fa-facebook"></i>
                                    <a href="https://www.facebook.com/RevAirRw/">Facebook</a>
                                </li>
                                <li>
                                    <i class="fa fa-twitter"></i>
                                    <a href="https://twitter.com/RevAirRw">Twitter</a>
                                </li>
                                <li>
                                    <i class="fa fa-linkedin"></i>
                                    <a href="https://www.linkedin.com/company/revolution-air-internet/">Linkedin</a>
                                </li>
                                <li>
                                    <i class="fa fa-instagram"></i>
                                    <a href="https://www.instagram.com/revair_rw/?hl=en">Instagram</a>
                                </li>
                                <li>
                                    <i class="fa fa-youtube"></i>
                                    <a href="https://www.youtube.com/channel/UCRSpl6cSB81DwnE9HhKQxUw?view_as=subscriber">Youtube</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- End of Footer Widget -->
            </div>
        </div>
    </div>

    <div class="bottom-footer dark-bg">
        <div class="container">
            <div class="row align-items-center border-top py-4">
                <!-- Copyright -->
                <div class="col-md-12">
                    <div class="copyright-text text-center">
                        <p class="mb-md-0">&copy; 2019 Revolution Air. All rights reserved.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>