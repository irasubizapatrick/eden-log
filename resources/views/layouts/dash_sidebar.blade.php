<div id="sidebar-menu">

    <ul class="metismenu" id="side-menu">

        <li>
            <a href="/dashboard">
                <i class="fa fa-home"></i>
                <span> Dashboard </span>
            </a>
        </li>

        <li>
            <a href="/leads">
                <i class="fa fa-users"></i>
                <span> Manage Leads </span>
            </a>
        </li>
        <li>
            <a href="/department">
                <i class="fa fa-briefcase"></i>
                <span> Department</span>
            </a>
        </li>
        <li>
            <a href="/agent">
                <i class="fa fa-user"></i>
                <span> Agent </span>
            </a>
        </li>
        <li>
            <a href="/product">
                <i class="fa fa-briefcase"></i>
                <span> Product </span>
            </a>
        </li>
        <li>
            <a href="/package">
                <i class="fa fa-search-plus"></i>
                <span> Package </span>
            </a>
        </li>
        <li>
            <a href="/manage/all/client">
                <i class="fa fa-user"></i>
                <span> Customers </span>
            </a>
        </li>
        <li>
            <a href="#">
                <i class="fa fa-file"></i>
                <span>Invoices </span>
            </a>
        </li>
        <li>
            <a href="/contracts">
                <i class="fa fa-file"></i>
                <span> Contracts </span>
            </a>
        </li>


        <li>
            <a href="/staff">
                <i class="fa fa-user"></i>
                <span> Staff </span>
            </a>
        </li>

        <li class="">
            <a href="javascript: void(0);" aria-expanded="false">
                <i class="mdi mdi-lock-question"></i>
                <span> Tickets </span>
                <span class="menu-arrow"></span>
            </a>
            <ul class="nav-second-level collapse" aria-expanded="false" style="height: 0px;">
                <li><a href="/manage/ticket">List</a></li>
            </ul>
        </li>
        <li class="">
            <a href="javascript: void(0);" aria-expanded="false">
                <i class="mdi mdi-file"></i>
                <span> CMS </span>
                <span class="menu-arrow"></span>
            </a>
            <ul class="nav-second-level collapse" aria-expanded="false" style="height: 0px;">
                <li><a href="/fq">FAQ</a></li>
                <li><a href="/manage_team">Team</a></li>
            </ul>
        </li>



    </ul>

</div>