<header>
    <div class="header-top">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-6 col-md-7">
                    <div class="header-info text-center text-md-left">
                        <span>Welcome to Revolution Air Ltd</span>
                    </div>
                </div>
                <div class="col-lg-6 col-md-5">
                    <div class="header-top-right d-flex align-items-center justify-content-center justify-content-md-end">
                        <form class="parsley-validate d-flex position-relative" action="#">
                            <input type="text" placeholder="I am looking for" required>
                            <button type="submit"><i class="fa fa-search"></i></button>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="sticky-wrapper">
        <nav class="navbar navbar-expand-lg navbar-dark">
            <div class="container">
                <div class="logo">
                    <a href="/">
                        <img src="/landing/img/logo.png" alt="REVAIR">
                    </a>
                </div>
                <button class="navbar-toggler navbar-toggler-right collapsed" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                    <i class="fa fa-bars"></i>
                </button>
                <div class="navbar-collapse collapse" id="navbarResponsive" style="">
                    <ul class="navbar-nav text-uppercase mx-auto">
                        <li class="nav-item active">
                            <a class="nav-link" href="/">Home</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/about">About</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/#package-list">Products/Services</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/team">Our Team</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/#faq">Our FAQ's</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/blog">Our Blog</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="contact">Contact</a>
                        </li>
                    </ul>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6 d-none d-sm-block">
                    <!-- Header Call -->
                    <div class="header-call text-right">
                        <span>Call Now</span>
                        <a href="tel:+1234567890">(+250) 788-129-500</a>
                    </div>
                </div>
            </div>
        </nav>
    </div>
</header>