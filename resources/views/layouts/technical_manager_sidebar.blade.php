<div id="sidebar-menu">

    <ul class="metismenu" id="side-menu">

        <li>
            <a href="/dashboard">
                <i class="fa fa-home"></i>
                <span> Dashboard </span>
            </a>
        </li>
        <li>
            <a href="/site/survey/request">
                <i class="fa fa-file"></i>
                <span>Site Survey </span>
            </a>
        </li> <li class="">
            <a href="javascript: void(0);" aria-expanded="false">
                <i class="mdi mdi-lock-question"></i>
                <span> Tickets </span>
                <span class="menu-arrow"></span>
            </a>
            <ul class="nav-second-level collapse" aria-expanded="false" style="height: 0px;">
                <li><a href="/manage/ticket">List</a></li>
            </ul>
        </li>

    </ul>


</div>