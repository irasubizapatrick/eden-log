<!DOCTYPE html>
<html lang="en">
@include('layouts.web_head')
<body>
<!-- Main header -->
@include('layouts.web_header')

<!-- Banner -->
<section>
    <div id="home-slides" class="carousel slide" data-pause="none" data-ride="carousel">
        <!-- Indicators -->
        <ul class="carousel-indicators">
            <li data-target="#home-slides" data-slide-to="0" class="active"></li>
            <li data-target="#home-slides" data-slide-to="1"></li>
            <li data-target="#home-slides" data-slide-to="2"></li>
        </ul>
        <div class="carousel-inner" role="listbox">
            <div class="carousel-item active" data-interval="21000">
                <img class="img-full" src="/landing/img/revbox.gif" alt="First slide" >
            </div>
            <div class="carousel-item " data-interval="10000">
                <img class="img-full" src="/landing/img/fita.gif" alt="Second slide">
            </div>
            <div class="carousel-item" data-interval="15000">
                <img class="img-full" src="/landing/img/africamap.gif" alt="Third slide">
            </div>

        </div>
        <!-- Left and right controls -->
        <a class="carousel-control-prev" href="#home-slides" data-slide="prev">
            <span class="carousel-control-prev-icon"></span>
        </a>
        <a class="carousel-control-next" href="#home-slides" data-slide="next">
            <span class="carousel-control-next-icon"></span>
        </a>
    </div>
</section>
<!-- End of Banner -->
<section id="bellow-slider">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 grid-icon col-4">
                <div class="row">
                    <div class="col-lg-3">
                        <img src="/landing/img/excellence.svg" class="img-icon">
                    </div>
                    <div class="col-lg-9">
                        <div class="content">
                            <h2>Excellence</h2>
                            <p>Through relentless efforts and technological innovations.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 grid-icon col-4">
                <div class="row">
                    <div class="col-lg-3">
                        <img src="/landing/img/integration.svg" class="img-icon">
                    </div>
                    <div class="col-lg-9">
                        <div class="content">
                            <h2>Integrity</h2>
                            <p>create a strong, secure and reliable pan African broadband Internet network.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 grid-icon col-4">
                <div class="row">
                    <div class="col-lg-3">
                        <img src="/landing/img/team.svg" class="img-icon">
                    </div>
                    <div class="col-lg-9">
                        <div class="content">
                            <h2>Team Work</h2>
                            <p>Our strength lies in our collaborative spirit,we believe nothing is impossible.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Abut Us -->
<section class="pt-55  after-slider">
    <div class="container">
        <div class="row align-items-center pb-80">
            <div class="col-lg-6 pb-xs-5 mx-auto text-center">
                <h2 class="mb-3">Coverage Map</h2>
                <div class="number-one-content">
                    <p>Service activation is based on <a href="#faq" style="color: #3377bc">line of sight</a> and requires a <a href="#faq" style="color: #3377bc"> site surveyLine of sight </a></p>
                    <p>
                        <a href="/customer/registration" class="btn">Get Connected</a>
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End of About Us -->
<section class="pack-section pt-50" id="package-list">
    <img src="/landing/img/product.png" class="watermarker-product-img">
    <div class="custom-section-header">
        <h2 class="text-center"><img src="/landing/img/r-logo.png" class="logo-with-words">evairBox Shared Plans</h2>
    </div>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-3 col-md-6 col-6">
                <div class="box-1">
                    <div class="mx-auto">
                        <div class="circle-box">
                            <h5 class="mbs-text">
                                <span class="capacity">5</span>
                                <span class="mbps-text">Mbps</span>
                            </h5>
                            <span class="amount-text">40,000 Rwfs</span>
                        </div>
                    </div>
                    <ul class="includes-list">
                        <li>Super-Fast Internet</li>
                        <li>Installation Fee: 50,000 RWF VAT Inclusive</li>
                        <li>Service activated in one (1) Day</li>
                        <li>98% SLA (Network Availabilty)</li>
                        <li>24/7 ticketing system for support</li>
                    </ul>
                </div>
            </div>
            <div class="col-xl-3 col-md-6 col-6">
                <div class="box-2">
                    <div class="mx-auto">
                        <div class="circle-box">
                            <h5 class="mbs-text">
                                <span class="capacity">10</span>
                                <span class="mbps-text">Mbps</span>
                            </h5>
                            <span class="amount-text">100,000 Rwfs</span>
                        </div>
                    </div>
                    <ul class="includes-list">
                        <li>Super-Fast Internet</li>
                        <li>Installation Fee: 50,000 RWF VAT Inclusive</li>
                        <li>Service activated in one (1) Day</li>
                        <li>98% SLA (Network Availabilty)</li>
                        <li>24/7 ticketing system for support</li>
                    </ul>
                </div>
            </div>
            <div class="col-xl-3 col-md-6 col-6">
                <div class="box-3">
                    <div class="mx-auto">
                        <div class="circle-box">
                            <h5 class="mbs-text">
                                <span class="capacity">15</span>
                                <span class="mbps-text">Mbps</span>
                            </h5>
                            <span class="amount-text">185,000 Rwfs</span>
                        </div>
                    </div>
                    <ul class="includes-list">
                        <li>Super-Fast Internet</li>
                        <li>Installation Fee: 50,000 RWF VAT Inclusive</li>
                        <li>Service activated in one (1) Day</li>
                        <li>98% SLA (Network Availabilty)</li>
                        <li>24/7 ticketing system for support</li>
                    </ul>
                    <img src="/landing/img/motari.gif" class="img-moving-bike">
                </div>
            </div>
            <div class="col-xl-3 col-md-6 col-6">
                <div class="box-4">
                    <div class="mx-auto">
                        <div class="circle-box">
                            <h5 class="mbs-text">
                                <span class="capacity">20</span>
                                <span class="mbps-text">Mbps</span>
                            </h5>
                            <span class="amount-text">250,000 Rwfs</span>
                        </div>
                    </div>
                    <ul class="includes-list">
                        <li>Super-Fast Internet</li>
                        <li>Installation Fee: 50,000 RWF VAT Inclusive</li>
                        <li>Service activated in one (1) Day</li>
                        <li>98% SLA (Network Availabilty)</li>
                        <li>24/7 ticketing system for support</li>
                    </ul>
                </div>
            </div>
        </div>
        <h5 class="package-bottom-text">For Dedicated Plans, Please Contact Us</h5>
    </div>
</section>
<!-- End of Services -->
<!-- Latest news -->
<section class="light-bg pt-120 pb-120" id="faq">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-6 col-lg-8 col-md-10">
                <div class="section-title text-center">
                    <h2>FAQ'S</h2>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6 mx-auto">
                <div class="panel-group faq-side" id="accordion" role="tablist">
                    @foreach($faq as $value)
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingOne">
                                <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#{{$value->id}}" aria-expanded="false" aria-controls="collapseOne">
                                        {{$value->fq_title}}
                                    </a>
                                </h4>
                            </div>
                            <div id="{{$value->id}}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                <div class="panel-body">
                                    <p>{{$value->fq_description}}</p>
                                </div>
                            </div>
                        </div>
                    @endforeach
                    {{--<div class="panel panel-default">--}}
                    {{--<div class="panel-heading" role="tab" id="headingOne">--}}
                    {{--<h4 class="panel-title">--}}
                    {{--<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">--}}
                    {{--WHAT IS LINE OF SIGHT?--}}
                    {{--</a>--}}
                    {{--</h4>--}}
                    {{--</div>--}}
                    {{--<div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">--}}
                    {{--<div class="panel-body">--}}
                    {{--<p>FITA Internet operates through wireless networks which require a clear view of the human eye (Line of Sight) from where the antenna is installed (ideally top of the client’s premises) to where our Tower is located. In cases where the view to the tower is not clear (tall buildings or geographical obstacles in between), client’s location is reported as a No Line of Sight area.--}}
                    {{--</p>--}}
                    {{--</div>--}}
                    {{--</div>--}}
                    {{--</div>--}}
                    {{--<div id="learn-more">--}}
                    {{--<div class="panel panel-default">--}}
                    {{--<div class="panel-heading" role="tab" id="headingThree">--}}
                    {{--<h4 class="panel-title">--}}
                    {{--<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">--}}
                    {{--WHAT IS SHARED/DEDICATED BANDWIDTH ?--}}
                    {{--</a>--}}
                    {{--</h4>--}}
                    {{--</div>--}}
                    {{--<div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">--}}
                    {{--<div class="panel-body">--}}
                    {{--<p>Shared Bandwidth (IWACU Pack in this case) involves a set bandwidth shared among users, on a shared bandwidth connection. All bandwidth (5 Mbps, 10 Mbps, etc.) is split among all allocated clients. Download and upload speeds on a shared package are “up to” a particular limit.  If usage on the shared bandwidth is light, clients will experience faster download speeds. If all clients on the network are streaming videos or uploading large files, speeds will decrease. This package is usually recommended for homes and small businesses with limited internet use.</p>--}}
                    {{--<p class="mt-2">Dedicated Bandwidth (KARIBU Pack in this case) is bandwidth that is dedicated to the client. With a dedicated plan, you are the only subscriber to a specified amount of bandwidth, that bandwidth allocated is for your use only. Those assigned dedicated bandwidth have access to more reliable services, like having consistent download and upload speeds. This package is usually recommended for Businesses that rely on providing online services to their customers and which depend on uploading and downloading content as well as operating heavy platforms.--}}
                    {{--</p>--}}
                    {{--</div>--}}
                    {{--</div>--}}
                    {{--</div>--}}
                    {{--</div>--}}

                    {{--<div class="panel panel-default">--}}
                    {{--<div class="panel-heading" role="tab" id="headingFour">--}}
                    {{--<h4 class="panel-title">--}}
                    {{--<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">--}}
                    {{--HOW DO I PAY FOR THE SERVICE ?--}}
                    {{--</a>--}}
                    {{--</h4>--}}
                    {{--</div>--}}
                    {{--<div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">--}}
                    {{--<div class="panel-body">--}}
                    {{--<p>After full service implementation as per signed contract, the client signs a service acceptance form and billing starts as per the day of acceptance. Service is post-paid (used and payed later) hence previous months’ invoices are generated (through email) between 1st-5th day of current month and the client is obliged to clear the invoice not later than the current month either by cheque or deposit on Revolution Air Ltd’ accounts in Equity or BK banks</p>--}}
                    {{--</div>--}}
                    {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="panel panel-default">--}}
                    {{--<div class="panel-heading" role="tab" id="headingFive">--}}
                    {{--<h4 class="panel-title">--}}
                    {{--<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFive" aria-expanded="false" aria-controls="collapseFive">--}}
                    {{--HOW DO I CHECK MY INTERNET USAGE ?--}}
                    {{--</a>--}}
                    {{--</h4>--}}
                    {{--</div>--}}
                    {{--<div id="collapseFive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFive">--}}
                    {{--<div class="panel-body">--}}
                    {{--<p>After signing the service acceptance form, our clients are given access to a monitoring platform where they have visibility of their internet usage, peak hours and through which other recommendations are made if necessary.</p>--}}
                    {{--</div>--}}
                    {{--</div>--}}
                    {{--</div>--}}

                    {{--<div class="panel panel-default">--}}
                    {{--<div class="panel-heading" role="tab" id="headingSix">--}}
                    {{--<h4 class="panel-title">--}}
                    {{--<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSix" aria-expanded="false" aria-controls="collapseSix">--}}
                    {{--HOW DO I GET SUPPORT?--}}
                    {{--</a>--}}
                    {{--</h4>--}}
                    {{--</div>--}}
                    {{--<div id="collapseSix" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSix">--}}
                    {{--<div class="panel-body">--}}
                    {{--<p>After signing the service acceptance form, a dedicated account manager is assigned to you for dedicated relationship with us. We strongly recommend that any support request be addressed directly to them as they will know they right channel for your request. Also an escalation matrix is shared just in case.</p>--}}
                    {{--</div>--}}
                    {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="panel panel-default">--}}
                    {{--<div class="panel-heading" role="tab" id="headingSeven">--}}
                    {{--<h4 class="panel-title">--}}
                    {{--<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">--}}
                    {{--CAN I CHANGE MY INTERNET SUBSCRIPTION ?--}}
                    {{--</a>--}}
                    {{--</h4>--}}
                    {{--</div>--}}
                    {{--<div id="collapseSeven" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSeven">--}}
                    {{--<div class="panel-body">--}}
                    {{--<p>Yes. Revolution Air has engineered the network to accommodate great flexibility and scalability. Maybe it’s something faster. Perhaps it’s downshifting to a lesser bandwidth for a while. Whichever the course, we can help you out.Make the request during office hours, and we’ll adjust your billing and make the internet bandwidth change effective within minutes.</p>--}}
                    {{--</div>--}}
                    {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="panel panel-default">--}}
                    {{--<div class="panel-heading" role="tab" id="headingEight">--}}
                    {{--<h4 class="panel-title">--}}
                    {{--<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseEight" aria-expanded="false" aria-controls="collapseEight">--}}
                    {{--HOW LONG DOES IT TAKE TO BE CONNECTED?--}}
                    {{--</a>--}}
                    {{--</h4>--}}
                    {{--</div>--}}
                    {{--<div id="collapseEight" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingEight">--}}
                    {{--<div class="panel-body">--}}
                    {{--<p>A client is connected within 48 hours of placing an order and having a contract signed.</p>--}}
                    {{--</div>--}}
                    {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="panel panel-default">--}}
                    {{--<div class="panel-heading" role="tab" id="headingSept">--}}
                    {{--<h4 class="panel-title">--}}
                    {{--<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSept" aria-expanded="false" aria-controls="collapseSept">--}}
                    {{--MY INTERNET IS NOT WORKING,WHAT DO I DO ?--}}
                    {{--</a>--}}
                    {{--</h4>--}}
                    {{--</div>--}}
                    {{--<div id="collapseSept" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSept">--}}
                    {{--<div class="panel-body">--}}
                    {{--<p>Please check if the wireless router is well connected to the power socket and if not, please connected it and test again. If already connected and the issue persists, please contact us through the account manager or escalation matrix and we shall have the service restored.--}}
                    {{--</p>--}}
                    {{--</div>--}}
                    {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="panel panel-default">--}}
                    {{--<div class="panel-heading" role="tab" id="headingNine">--}}
                    {{--<h4 class="panel-title">--}}
                    {{--<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseNine" aria-expanded="false" aria-controls="collapseNine">--}}
                    {{--I WANT TO CANCEL MY SUBSCRIPTION WHAT DO I DO  ?--}}
                    {{--</a>--}}
                    {{--</h4>--}}
                    {{--</div>--}}
                    {{--<div id="collapseNine" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingNine">--}}
                    {{--<div class="panel-body">--}}
                    {{--<p>A client intending to cancel subscription is required to give a 30 days’ notice within which we attempt to understand our client’s reason and attend to it where possible.Should you choose to cancel your subscription for reasons other than failure to deliver agreed services, an early termination fee of 50,000 RWF is applied.</p>--}}
                    {{--</div>--}}
                    {{--</div>--}}
                    {{--</div>--}}
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End of Latest news -->

<!-- Footer -->
@include('layouts.web_footer')
<!-- End of Footer -->

<!-- Back to top -->
<div class="back-to-top">
    <a href="/#"> <i class="fa fa-chevron-up"></i></a>
</div>

@include('layouts.web_js')
</body>
</html>