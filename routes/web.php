<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/','LandingController@index');
Route::get('/about','LandingController@about');
Route::get('/blog','LandingController@blog');
Route::get('/contact','LandingController@contact');
Route::get('/service','LandingController@service');
Route::get('/team','LandingController@team');
Route::get('/customer/registration','LeadsController@customerReg')->name('customer/registration');
Route::post('/lead_reg','LeadsController@lead_reg')->name('lead_reg');
Route::get('/dashboard', ['middleware' => 'please_login',  function (){
    return view('auth.login');
}]);


Route::get('/logout ',function (){
    Auth::logout();
    return redirect('/login');
});
Auth::routes();


Route::group(['middleware'=> 'auth'], function() {

    Route::get('/dashboard', 'DashboardController@index');
    Route::resource('/analytics', 'AnalyticsController');

    /*
    |--------------------------------------------------------------------------
    |   Leads Controller
    |--------------------------------------------------------------------------
    */

    Route::resource('/leads', 'LeadsController');
    Route::any('/view_lead_status/{id}', 'LeadsController@leadStatus')->name('view_lead_status');
    Route::any('/create_account/{id}', 'LeadsController@createAccount')->name('create_account');
    Route::get('/site/survey/request', 'LeadsController@site_SUrvey')->name('site/survey/request');

    /*
   |--------------------------------------------------------------------------
   |   Department Controller
   |--------------------------------------------------------------------------
   */

    Route::resource('/department', 'DepartmentController');

    /*
    |--------------------------------------------------------------------------
    |   Staff Controller
    |--------------------------------------------------------------------------
    */

    Route::resource('/staff', 'StaffController');


    /*
    |--------------------------------------------------------------------------
    |   Agent Controller
    |--------------------------------------------------------------------------
    */

    Route::resource('/agent', 'AgentController');

    /*
     |--------------------------------------------------------------------------
     |   Client Controller
     |--------------------------------------------------------------------------
     */

    Route::resource('/manage/all/client', 'ClientController');
    Route::any('/manage/customer', 'ClientController@save_customer')->name('manage/customer');
    Route::get('/moreCustomerDetails/{id}', 'ClientController@moreCustomerDetails')->name('moreCustomerDetails');


    /*
    |--------------------------------------------------------------------------
    |   Product Controller
    |--------------------------------------------------------------------------
    */

    Route::resource('/product', 'ProductController');


    /*
    |--------------------------------------------------------------------------
    |   Package Controller
    |--------------------------------------------------------------------------
    */

    Route::resource('/package', 'PackageController');

    /*
    |--------------------------------------------------------------------------
    |   Ticket Management Controller
    |--------------------------------------------------------------------------
    */

    Route::resource('/manage/ticket', 'TicketController');
    Route::get('/view/conservation/{id}', 'TicketController@ViewConversation')->name('view/conservation');
    Route::post('/add/manage/ticket', 'TicketController@add_Answer')->name('/add/manage/ticket');

    /*
    |--------------------------------------------------------------------------
    |   Contract Management Controller
    |--------------------------------------------------------------------------
    */

    Route::resource('/contracts', 'ContractController');

    /*
    |--------------------------------------------------------------------------
    |   Content Management System
    |--------------------------------------------------------------------------
    */


    Route::resource('/fq','FqController');
    Route::resource('/manage_team','TeamController');

    /*
    |--------------------------------------------------------------------------
    |   Invoice Management Controller
    |--------------------------------------------------------------------------
    */

    Route::resource('/invoice', 'InvoicesController');
    Route::get('/my/invoice','InvoicesController@my_invoice')->name('my/invoice');

    /*
    |--------------------------------------------------------------------------
    |  User Controller
    |--------------------------------------------------------------------------
    */

    Route::get('/change_password','UserController@change_pass')->name('change_password');
    Route::any('/update_password/{id}','UserController@update_password')->name('update_password');
});